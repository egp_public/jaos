модуль FoxOberonFrontend; (**  AUTHOR "fof"; PURPOSE "Oberon FoxCompiler: Oberon frontend module"; Копипаста с FoxOberonFrontend.Mod **)

использует
	Потоки, Diagnostics,  SyntaxTree := FoxSyntaxTree, Parser := FoxParser, Scanner := FoxScanner, SomeFrontend := FoxFrontend;

тип

	Frontend* = окласс (SomeFrontend.Frontend)
	перем
		scanner: Scanner.Лексер;
		parser: Parser.Parser;

		проц {перекрыта}Initialize*(вхДиагностика: Diagnostics.Diagnostics; l0flags: мнвоНаБитахМЗ; reader: Потоки.Чтец; 
			конст вхИмяИсходногоФайла, definitions, вхДиректорияДляМакрорасширенногоФайла: массив из симв8; вхpos: цел32);
		нач
			(** параметр diagnostics не используется в конструкторе базового класса почему-то *)
			Initialize^(вхДиагностика, l0flags, reader, вхИмяИсходногоФайла, definitions, вхДиректорияДляМакрорасширенногоФайла, вхpos);
			scanner := Scanner.NewScanner(вхИмяИсходногоФайла, reader, вхpos, вхДиагностика);
			parser := Parser.NewParser( scanner, вхДиагностика, definitions );
		кон Initialize;

		проц {перекрыта}Parse*(): SyntaxTree.ДеревоМодуля;
		нач
			возврат parser.Module();
		кон Parse;

		проц {перекрыта}Error*(): булево;
		нач
			возврат parser.error;
		кон Error;

		проц {перекрыта}Done*(): булево;
		нач
			возврат ~parser.NextModule()
		кон Done;

	кон Frontend;

	проц Get*():SomeFrontend.Frontend;
	перем frontend: Frontend;
	нач
		нов(frontend);
		возврат frontend;
	кон Get;

кон FoxOberonFrontend.
