(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль CalcD2;   (** AUTHOR "adf"; PURPOSE "Computes second-order derivatives"; *)

использует NbrInt, NbrRe, NbrCplx, MathRe, CalcFn;

конст
	(** Admissible parameters to be passed for establishing the differencing scheme used to compute a derivative. *)
	Forward* = 9;  Central* = 10;  Backward* = 11;

перем
	epsilon, zero: NbrRe.Real;

	(* Force the argument in and out of addressable memory to minimize round-off error. *)
	проц DoNothing( x: NbrRe.Real );
	кон DoNothing;

	проц DoCplxNothing( z: NbrCplx.Complex );
	кон DoCplxNothing;

	(** Computes  d2f(x)/dx2 *)
	проц Solve*( f: CalcFn.ReArg;  atX: NbrRe.Real;  differencing: NbrInt.Integer ): NbrRe.Real;
	перем h, h2, hOpt, hMin, power, result, temp: NbrRe.Real;
	нач
		(*  Select an optimum step size.  See v5.7 on Numerical Derivatives in Press et al., Numerical Recipes. *)
		power := 4 / 5;  hMin := MathRe.Power( NbrRe.Epsilon, power );  power := 1 / 4;
		hOpt := NbrRe.Abs( atX ) * MathRe.Power( epsilon, power );  h := NbrRe.Max( hOpt, hMin );
		(* Refine h so that  x + h and x differ by an exactly representable number in memory. *)
		temp := atX + h;  DoNothing( temp );  h := temp - atX;  h2 := h * h;
		(* Compute an approximate value for the derivative. *)
		если differencing = Forward то
			result := f( atX + 2 * h );
			result := result - 2 * f( atX + h );
			result := (result + f( atX )) / h2
		аесли differencing = Backward то
			result := f( atX );
			result := result - 2 * f( atX - h );
			result := (result + f( atX - 2 * h )) / h2
		иначе  (* differencing = Central *)
			result := f( atX + h );
			result := result - 2 * f( atX );
			result := (result + f( atX - h )) / h2
		всё;
		возврат result
	кон Solve;

	(** Computes  d2f(z)/dz2 *)
	проц SolveCplx*( f: CalcFn.CplxArg;  atZ: NbrCplx.Complex;  differencing: NbrInt.Integer ): NbrCplx.Complex;
	перем h, hOpt, hMin, power: NbrRe.Real;  ch, ch2, result, temp: NbrCplx.Complex;
	нач
		(*  Select an optimum step size. *)
		power := 4 / 5;  hMin := MathRe.Power( NbrRe.Epsilon, power );  power := 1 / 4;
		hOpt := NbrCplx.Abs( atZ ) * MathRe.Power( epsilon, power );  h := NbrRe.Max( hOpt, hMin );
		NbrCplx.Set( h, h, ch );
		(* Refine h so that  z + ch and z differ by an exactly representable number in memory. *)
		temp := atZ + ch;  DoCplxNothing( temp );  ch := temp - atZ;  ch2 := ch * ch;
		(* Compute an approximate value for the derivative. *)
		если differencing = Forward то
			result := f( atZ + 2 * ch );
			result := result - 2 * f( atZ + ch );
			result := (result + f( atZ )) / ch2
		аесли differencing = Backward то
			result := f( atZ );
			result := result - 2 * f( atZ - ch );
			result := (result + f( atZ - 2 * ch )) / ch2
		иначе  (* differencing = Central *)
			result := f( atZ + ch );
			result := result - 2 * f( atZ );
			result := (result + f( atZ - ch )) / ch2
		всё;
		возврат result
	кон SolveCplx;

	(** Computes 62f(z)/6x2,  z = x + i y  *)
	проц SolveCplxRe*( f: CalcFn.CplxArg;  atZ: NbrCplx.Complex;  differencing: NbrInt.Integer ): NbrCplx.Complex;
	перем h, hOpt, hMin, power: NbrRe.Real;  ch, ch2, result, temp: NbrCplx.Complex;
	нач
		(*  Select an optimum step size. *)
		power := 4 / 5;  hMin := MathRe.Power( NbrRe.Epsilon, power );  power := 1 / 4;
		hOpt := NbrCplx.Abs( atZ ) * MathRe.Power( epsilon, power );  h := NbrRe.Max( hOpt, hMin );
		NbrCplx.Set( h, zero, ch );
		(* Refine h so that  z + ch and z differ by an exactly representable number in memory. *)
		temp := atZ + ch;  DoCplxNothing( temp );  ch := temp - atZ;  ch2 := ch * ch;
		(* Compute an approximate value for the derivative. *)
		если differencing = Forward то
			result := f( atZ + 2 * ch );
			result := result - 2 * f( atZ + ch );
			result := (result + f( atZ )) / ch2
		аесли differencing = Backward то
			result := f( atZ );
			result := result - 2 * f( atZ - ch );
			result := (result + f( atZ - 2 * ch )) / ch2
		иначе  (* differencing = Central *)
			result := f( atZ + ch );
			result := result - 2 * f( atZ );
			result := (result + f( atZ - ch )) / ch2
		всё;
		возврат result
	кон SolveCplxRe;

	(** Computes  62f(z)/6y2,  z = x + i y  *)
	проц SolveCplxIm*( f: CalcFn.CplxArg;  atZ: NbrCplx.Complex;  differencing: NbrInt.Integer ): NbrCplx.Complex;
	перем h, hOpt, hMin, power: NbrRe.Real;  ch, ch2, result, temp: NbrCplx.Complex;
	нач
		(*  Select an optimum step size. *)
		power := 4 / 5;  hMin := MathRe.Power( NbrRe.Epsilon, power );  power := 1 / 4;
		hOpt := NbrCplx.Abs( atZ ) * MathRe.Power( epsilon, power );  h := NbrRe.Max( hOpt, hMin );
		NbrCplx.Set( zero, h, ch );
		(* Refine h so that  z + ch and z differ by an exactly representable number in memory. *)
		temp := atZ + ch;  DoCplxNothing( temp );  ch := temp - atZ;  ch2 := ch * ch;
		(* Compute an approximate value for the derivative. *)
		если differencing = Forward то
			result := f( atZ + 2 * ch );
			result := result - 2 * f( atZ + ch );
			result := (result + f( atZ )) / ch2
		аесли differencing = Backward то
			result := f( atZ );
			result := result - 2 * f( atZ - ch );
			result := (result + f( atZ - 2 * ch )) / ch2
		иначе  (* differencing = Central *)
			result := f( atZ + ch );
			result := result - 2 * f( atZ );
			result := (result + f( atZ - ch )) / ch2
		всё;
		возврат result
	кон SolveCplxIm;

	(** Computes  62f(z)/6r2,  z = r exp( i f )  *)
	проц SolveCplxAbs*( f: CalcFn.CplxArg;  atZ: NbrCplx.Complex;  differencing: NbrInt.Integer ): NbrCplx.Complex;
	перем h, hOpt, hMin, power: NbrRe.Real;  ch, ch2, result, temp: NbrCplx.Complex;
	нач
		(*  Select an optimum step size. *)
		power := 4 / 5;  hMin := MathRe.Power( NbrRe.Epsilon, power );  power := 1 / 4;
		hOpt := NbrCplx.Abs( atZ ) * MathRe.Power( epsilon, power );  h := NbrRe.Max( hOpt, hMin );
		NbrCplx.SetPolar( h, zero, ch );
		(* Refine h so that  z + ch and z differ by an exactly representable number in memory. *)
		temp := atZ + ch;  DoCplxNothing( temp );  ch := temp - atZ;  ch2 := ch * ch;
		(* Compute an approximate value for the derivative. *)
		если differencing = Forward то
			result := f( atZ + 2 * ch );
			result := result - 2 * f( atZ + ch );
			result := (result + f( atZ )) / ch2
		аесли differencing = Backward то
			result := f( atZ );
			result := result - 2 * f( atZ - ch );
			result := (result + f( atZ - 2 * ch )) / ch2
		иначе  (* differencing = Central *)
			result := f( atZ + ch );
			result := result - 2 * f( atZ );
			result := (result + f( atZ - ch )) / ch2
		всё;
		возврат result
	кон SolveCplxAbs;

	(** Computes  62f(z)/6f2,  z = r exp( i f )  *)
	проц SolveCplxArg*( f: CalcFn.CplxArg;  atZ: NbrCplx.Complex;  differencing: NbrInt.Integer ): NbrCplx.Complex;
	перем h, hOpt, hMin, power: NbrRe.Real;  ch, ch2, result, temp: NbrCplx.Complex;
	нач
		(*  Select an optimum step size. *)
		power := 4 / 5;  hMin := MathRe.Power( NbrRe.Epsilon, power );  power := 1 / 4;
		hOpt := NbrCplx.Arg( atZ ) * MathRe.Power( epsilon, power );  h := NbrRe.Max( hOpt, hMin );
		NbrCplx.SetPolar( zero, h, ch );
		(* Refine h so that  z + ch and z differ by an exactly representable number in memory. *)
		temp := atZ + ch;  DoCplxNothing( temp );  ch := temp - atZ;  ch2 := ch * ch;
		(* Compute an approximate value for the derivative. *)
		если differencing = Forward то
			result := f( atZ + 2 * ch );
			result := result - 2 * f( atZ + ch );
			result := (result + f( atZ )) / ch2
		аесли differencing = Backward то
			result := f( atZ );
			result := result - 2 * f( atZ - ch );
			result := (result + f( atZ - 2 * ch )) / ch2
		иначе  (* differencing = Central *)
			result := f( atZ + ch );
			result := result - 2 * f( atZ );
			result := (result + f( atZ - ch )) / ch2
		всё;
		возврат result
	кон SolveCplxArg;

нач
	epsilon := 100 * NbrRe.Epsilon;  zero := 0
кон CalcD2.
