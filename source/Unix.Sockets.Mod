модуль Sockets;	(** AUTHOR "G.F.";  PURPOSE "Interface to Unix sockets" *)

(* derived from NetBase.Mod,  BD / 13.2.96 *)

(*	1998.04.27	g.f.	Linux PPC version	*)
(*	1099.05.17	g.f.	adapted to Threads	*)
(*	1999.11.22	g.f.	Solaris x86 version	*)
(*	2000.02.18	g.f.	adapted to Solaris 8	*)
(*	2001.01.06	g.f.	[c] - flag for new compiler	*)
(*	2007.07.06	g.f.	IP address format converted to IP.Adr *)

использует S := НИЗКОУР, Unix, Трассировка, IP;

конст

	(*listen *)
	Backlog = 5;		(* max number of pending connections *)

тип
	SocketAdr* = укль на запись
		family*		: цел16;
		port*		: цел16;	(* in network byte order! *)
	кон;

	SocketAdrV4* = укль на запись (SocketAdr)
		v4Adr*		: цел32;
		zero*		: массив 8 из симв8
	кон;

	SocketAdrV6* = укль на запись (SocketAdr)
		flowinfo*	: цел32;
		v6Adr*		: массив 16 из симв8;
		scopeId*	: цел32;
		srcId*		: цел32
	кон;

	NameBuf = укль на запись
		buf: массив 64 из симв8
	кон;

	SocketOption = запись кон;
	Length = целМЗ;

	Linger = запись (SocketOption)
		onoff	: цел32;
		linger	: цел32;
	кон;

	Switch = запись (SocketOption)
		onoff	: цел32
	кон;

перем
	socket		: проц {C} ( af, typ, protocol: целМЗ ): целМЗ;
	setsockopt	: проц {C} ( s: целМЗ; level, optname: целМЗ; перем opt: SocketOption; optlen: Length): целМЗ;
	accept		: проц {C} ( s: целМЗ; adrPtr: адресВПамяти; перем adrlen: Length ): целМЗ;
	bind		: проц {C} ( s: целМЗ; adr: SocketAdr; adrlen: Length ): целМЗ;
	connect		: проц {C} ( s: целМЗ; adr: SocketAdr; adrlen: Length ): целМЗ;
	listen		: проц {C} ( s: целМЗ; backlog: целМЗ ): целМЗ;
	recv		: проц {C} ( s: целМЗ; buf: адресВПамяти; len: размерМЗ; flags: целМЗ ): размерМЗ;
	send		: проц {C} ( s: целМЗ; buf: адресВПамяти; len: размерМЗ; flags: целМЗ ): размерМЗ;
	recvfrom	: проц {C} ( s: целМЗ; buf: адресВПамяти; len: размерМЗ; flags: целМЗ; from: NameBuf; перем flen: Length ): размерМЗ;
	sendto		: проц {C} ( s: целМЗ; buf: адресВПамяти; len: размерМЗ; flags: целМЗ; to: SocketAdr; tolen: Length ): размерМЗ;
	shutdown	: проц {C} ( s: целМЗ; how: целМЗ ): целМЗ;

	getpeername	: проц {C} ( s: целМЗ; adr: NameBuf; перем adrlen: Length ): целМЗ;
	getsockname	: проц {C} ( s: целМЗ; adr: NameBuf; перем adrlen: Length ): целМЗ;

	htonl	: проц {C} ( hostlong	: бцел32 ): бцел32;
	htons	: проц {C} ( hostshort	: бцел16 ): бцел16;
	ntohl	: проц {C} ( netlong	: бцел32 ): бцел32;
	ntohs	: проц {C} ( netshort	: бцел16 ): бцел16;

	проц NewSocketAdr*( ip: IP.Adr; port: цел32 ): SocketAdr;
	перем sadr4: SocketAdrV4;  sadr6: SocketAdrV6; i: размерМЗ;
	нач
		просей ip.usedProtocol из
		| -1:
			нов( sadr4 );
				sadr4.family := Unix.AFINET;
				sadr4.port := IntToNet( устарПреобразуйКБолееУзкомуЦел( port ) );
				sadr4.v4Adr := 0;
			возврат sadr4
		| IP.IPv4:
			нов( sadr4 );
				sadr4.family := Unix.AFINET;
				sadr4.port := IntToNet( устарПреобразуйКБолееУзкомуЦел( port ) );
				sadr4.v4Adr := ip.ipv4Adr;
			возврат sadr4
		| IP.IPv6:
			нов( sadr6 );
				sadr6.family := Unix.AFINET6;
				sadr6.port := IntToNet( устарПреобразуйКБолееУзкомуЦел( port ) );
				sadr6.flowinfo := 0;
				нцДля i := 0 до 15 делай  sadr6.v6Adr[i] := ip.ipv6Adr[i]  кц;
				sadr6.scopeId := 0;
				sadr6.srcId := 0;
			возврат sadr6
		иначе
			СТОП( 99 )
		всё
	кон NewSocketAdr;

	проц SockAdrToIPAdr*( sadr: SocketAdr ): IP.Adr;
	перем ip: IP.Adr;  i: размерМЗ;
	нач
		если sadr суть SocketAdrV4 то
			ip.usedProtocol := IP.IPv4;
			ip.ipv4Adr := sadr(SocketAdrV4).v4Adr;
			ip.ipv6Adr := ""
		иначе
			ip.usedProtocol := IP.IPv6;
			ip.ipv4Adr := 0;
			нцДля i := 0 до 15 делай
				ip.ipv6Adr[i] := sadr(SocketAdrV6).v6Adr[i]
			кц
		всё;
		возврат ip
	кон SockAdrToIPAdr;

	проц GetPortNumber*( sadr: SocketAdr ): цел32;
	перем port: цел32;
	нач
		port := NetToInt( sadr.port );
		если port < 0 то port := port + 10000H всё;
		возврат port
	кон GetPortNumber;

	проц BufToSocketAdr( конст buf: массив из симв8; len: целМЗ ): SocketAdr;
	перем adr4: SocketAdrV4; adr6: SocketAdrV6;
	нач
		если len = Unix.SockAddrSizeV4 то
			нов( adr4 );
			S.копируйПамять( адресОт( buf ), адресОт( adr4^), len );
			возврат adr4
		иначе
			нов( adr6 );
			S.копируйПамять( адресОт( buf ), адресОт( adr6^), len );
			возврат adr6
		всё
	кон BufToSocketAdr;

	проц Accept*( s: целМЗ ): целМЗ;
	перем len: Length; err, new: целМЗ;
	нач
		len := 0;
		нцДо
			new := accept( s, 0, len );
			если new < 0 то  err := Unix.errno()  всё
		кцПри (new > 0) или (err # Unix.EINTR);
		если new < 0 то  Unix.Perror( "Sockets.Accept" )  всё;
		возврат new
	кон Accept;

	проц Bind*( s: целМЗ; addr: SocketAdr): булево;
	перем err: целМЗ; len: Length;
	нач
		если addr.family = Unix.AFINET то  len := Unix.SockAddrSizeV4  иначе  len := Unix.SockAddrSizeV6  всё;
		err:= bind( s, addr, len );
		возврат err = 0
	кон Bind;

	проц Close*( s: целМЗ );
	нач
		неважно shutdown( s, Unix.ShutRDWR );
		неважно Unix.close( s );
	кон Close;

	проц Connect*( s: целМЗ; addr: SocketAdr ): булево;
	перем err: целМЗ; len: Length;
	нач
		если addr.family = Unix.AFINET то  len := Unix.SockAddrSizeV4  иначе  len := Unix.SockAddrSizeV6  всё;
		err:= connect( s, addr, len );
		если err = 0 то
			возврат истина
		иначе
			Unix.Perror( "Sockets.Connect: " );
			возврат ложь
		всё;
		возврат err = 0
	кон Connect;

	проц GetSockName*( s: целМЗ ): SocketAdr;
	перем len: Length; err: целМЗ; buf: NameBuf;
	нач
		нов( buf );  len := 64;
		err := getsockname( s, buf, len );
		если err = 0 то
			возврат BufToSocketAdr( buf.buf, len )
		иначе
			Unix.Perror( "Sockets.GetSockName" );
			возврат НУЛЬ
		всё
	кон GetSockName;

	проц GetPeerName*( s: целМЗ ): SocketAdr;
	перем err: целМЗ; len: Length; buf: NameBuf;
	нач
		нов( buf );  len := 64;
		err:= getpeername( s, buf, len );
		если err = 0 то
			возврат BufToSocketAdr( buf.buf, len )
		иначе
			Unix.Perror( "Sockets.GetPeerName" );
			возврат НУЛЬ
		всё
	кон GetPeerName;

	проц Listen*( s: целМЗ ): булево;
	перем err: целМЗ;
	нач
		err := listen( s, Backlog );
		возврат err = 0
	кон Listen;

	проц Recv*( s: целМЗ; перем buf: массив из симв8; pos: размерМЗ; перем len: размерМЗ; flags: целМЗ ): булево;
	перем res: размерМЗ; err: целМЗ;
	нач
		нцДо
			res := recv( s, адресОт( buf[pos] ), len, flags );
			если res < 0 то  err := Unix.errno()  всё
		кцПри (res >= 0) или (err # Unix.EINTR);
		если err >= 0 то
			len:= res;  возврат истина
		иначе
			Unix.Perror( "Sockets.Recv" );
			len:= 0;  возврат ложь
		всё
	кон Recv;

	проц Send*( s: целМЗ; конст buf: массив из симв8; pos: размерМЗ; перем len: размерМЗ ): булево;
	перем err: размерМЗ;
	нач
		утв( длинаМассива(buf)-pos >= len );
		err := send( s, адресОт( buf[pos] ), len, 0 );
		если err >= 0 то
			len := err;  возврат истина
		иначе
			Unix.Perror( "Sockets.Send" );
			len := 0;  возврат ложь
		всё
	кон Send;

	проц RecvFrom*( s: целМЗ; перем from: SocketAdr;
						перем buf: массив из симв8; pos: размерМЗ;  перем len: размерМЗ ): булево;
	перем res: размерМЗ; err, size: целМЗ; nbuf: NameBuf;
	нач
		нов( nbuf ); size := 64;
		нцДо
			res := recvfrom( s, адресОт(buf[pos]), длинаМассива( buf ) - pos, 0, nbuf, size );
			если res < 0 то  err := Unix.errno()  всё
		кцПри (res >= 0) или (err # Unix.EINTR);
		если res >= 0 то
			from := BufToSocketAdr( nbuf.buf, size );
			len := res;  возврат истина
		иначе
			Unix.Perror( "Sockets.RecvFrom" );
			len := 0;  возврат ложь
		всё
	кон RecvFrom;

	проц SendTo*( s: целМЗ;  dest: SocketAdr;  конст buf: массив из симв8;  pos, len: размерМЗ ): булево;
	перем err: размерМЗ; size: Length;
	нач
		утв( длинаМассива(buf) - pos >= len );
		если dest.family = Unix.AFINET то  size := Unix.SockAddrSizeV4  иначе  size := Unix.SockAddrSizeV6  всё;
		err:= sendto( s, адресОт( buf[pos] ), len, 0, dest, size );
		если err >= 0 то
			возврат истина
		иначе
			Unix.Perror( "Sockets.SendTo" );
			возврат ложь
		всё
	кон SendTo;

	проц Socket* ( af, typ, protocol: целМЗ ): целМЗ;
	перем s: целМЗ;
	нач
		s := socket( af, typ, protocol );
		возврат s
	кон Socket;

	проц Available*( s: целМЗ ): цел32;
	перем available, err: целМЗ;
	нач
		available := 0;
		err := Unix.ioctl( s, Unix.FioNRead, адресОт( available ) );
		если err = 0 то
			возврат available
		иначе
			Unix.Perror( "Sockets.Available (ioctl)" );
			возврат -1
		всё
	кон Available;

	проц Requested*( s: целМЗ ): булево;
	конст
		SLen = Unix.FdSetLen;
		SetBits = размер16_от( мнвоНаБитах32 ) * 8;
	перем
		res, i: целМЗ;
		readfds: Unix.FdSet;
		timeout: Unix.Timeval;
	нач
		timeout.sec := 0; timeout.usec := 0;
		нцДля i := 0 до SLen - 1  делай readfds[i] := {} кц;
		включиВоМнвоНаБитах( readfds[s DIV SetBits],  s остОтДеленияНа SetBits );
		res := Unix.select( s+1, адресОт( readfds ), 0, 0, timeout );
		возврат res > 0
	кон Requested;

	проц AwaitPacket*( s: целМЗ; ms: цел32 ): булево;
	конст
		SLen = Unix.FdSetLen;
		SetBits = размер16_от( мнвоНаБитах32 ) * 8;
	перем
		res, err, i: целМЗ;
		readfds: Unix.FdSet;
		timeout: Unix.Timeval;
	нач
		timeout.sec := ms DIV 1000;  ms := ms остОтДеленияНа 1000;
		timeout.usec := 1000*ms;
		нцДля i := 0 до SLen - 1  делай readfds[i] := {} кц;
		включиВоМнвоНаБитах( readfds[s DIV SetBits],  s остОтДеленияНа SetBits );
		нцДо
			res := Unix.select( s+1, адресОт( readfds ), 0, 0, timeout );
			если res < 0 то  err := Unix.errno()  всё
		кцПри (res >= 0) или (err # Unix.EINTR);
		возврат res > 0
	кон AwaitPacket;

	проц SetLinger* ( s: целМЗ ): булево;
	перем
		linger: Linger;
		err: целМЗ;
	нач
		linger.onoff := 1;  linger.linger := 1;
		err := setsockopt( s, Unix.SoLSocket, Unix.SoLinger, linger, размерОт Linger);
		если err # 0 то  Unix.Perror(  "Sockets.SetLinger (setsockopt)" )  всё;
		возврат err = 0
	кон SetLinger;

	проц KeepAlive* ( s: целМЗ; enable: булево ): булево;
	перем
		opt: Switch;
		err: целМЗ;
	нач
		если enable то  opt.onoff := 1  иначе  opt.onoff := 0  всё;
		err := setsockopt( s, Unix.SoLSocket, Unix.SoKeepAlive, opt, размерОт Switch );
		если err # 0 то  Unix.Perror(  "Sockets.KeepAlive (setsockopt)" )  всё;
		возврат err = 0
	кон KeepAlive;

	проц NoDelay* ( s: целМЗ; enable: булево ): булево;
	перем
		opt: Switch;
		err: целМЗ;
	нач
		если enable то  opt.onoff := 1  иначе  opt.onoff := 0  всё;
		err := setsockopt( s, Unix.IpProtoTCP, Unix.SoNoDelay, opt, размерОт Switch );
		если err # 0 то  Unix.Perror(  "Sockets.NoDelay (setsockopt)" )  всё;
		возврат err = 0
	кон NoDelay;

	проц NetToInt* (x: цел16): цел16;
	нач
		возврат цел16(ntohs(x))
	кон NetToInt;

	проц IntToNet* (x: цел16): цел16;
	нач
		возврат цел16(htons(x))
	кон IntToNet;

	проц NetToLInt* (x: цел32): цел32;
	нач
		возврат цел32(ntohl(x))
	кон NetToLInt;

	проц LIntToNet* (x: цел32): цел32;
	нач
		возврат цел32(htonl(x))
	кон LIntToNet;

	проц Init;
	перем slib: адресВПамяти;
	нач
		если Unix.Version = "Solaris" то
			slib := Unix.Dlopen( "libsocket.so.1", 2 );
			если slib = 0 то  slib := Unix.Dlopen( "libsocket.so", 2 )  всё;
			если slib = 0 то  Трассировка.StringLn( "Unix.Dlopen( 'libsocket.so' ) failed")  всё;
		иначе
			slib := Unix.libc
		всё;
		Unix.Dlsym( slib, "accept", адресОт( accept ) );
		Unix.Dlsym( slib, "bind", адресОт( bind ) );
		Unix.Dlsym( slib, "connect", адресОт( connect ) );
		Unix.Dlsym( slib, "shutdown", адресОт( shutdown ) );
		Unix.Dlsym( slib, "getpeername", адресОт( getpeername ) );
		Unix.Dlsym( slib, "htonl", адресОт( htonl ) );
		Unix.Dlsym( slib, "htons", адресОт( htons ) );
		Unix.Dlsym( slib, "listen", адресОт( listen ) );
		Unix.Dlsym( slib, "ntohl", адресОт( ntohl ) );
		Unix.Dlsym( slib, "ntohs", адресОт( ntohs ) );
		Unix.Dlsym( slib, "recv", адресОт( recv ) );
		Unix.Dlsym( slib, "recvfrom", адресОт( recvfrom ) );
		Unix.Dlsym( slib, "send", адресОт( send ) );
		Unix.Dlsym( slib, "sendto", адресОт( sendto ) );
		Unix.Dlsym( slib, "setsockopt", адресОт( setsockopt ) );
		Unix.Dlsym( slib, "socket", адресОт( socket ) );
		Unix.Dlsym( slib, "getsockname", адресОт( getsockname ) );
	кон Init;

нач
	Init
кон Sockets.
