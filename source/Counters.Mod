(* Atomic counters *)
(* Copyright (C) Florian Negele *)

(** Provides a counter type with the following atomic operations. *)
модуль Counters;

использует CPU;

(** Represents an atomic counter. *)
тип Counter* = запись value := 0: размерМЗ кон;

(** Represents an atomic counter aligned for optimal cache behavior. *)
тип AlignedCounter* = запись {выровнять (CPU.CacheLineSize)} (Counter) кон;

(** Returns the current value of an atomic counter. *)
проц Read- (перем counter: Counter): размерМЗ;
нач {безКооперации, безОбычныхДинПроверок} возврат сравнениеСОбменом (counter.value, 0, 0);
кон Read;

(** Atomically increments the value of an atomic counter by the specified amount and returns its previous value. *)
проц Increment- (перем counter: Counter; amount: размерМЗ): размерМЗ;
перем value: размерМЗ;
нач {безКооперации, безОбычныхДинПроверок}
	нц
		value := сравнениеСОбменом (counter.value, 0, 0);
		если сравнениеСОбменом (counter.value, value, value + amount) = value то прервиЦикл всё;
		CPU.Backoff;
	кц;
	возврат value;
кон Increment;

(** Atomically decrements the value of an atomic counter by the specified amount and returns either its previous value or zero if the counter cannot be decremented. *)
проц Decrement- (перем counter: Counter; amount: размерМЗ): размерМЗ;
перем value: размерМЗ;
нач {безКооперации, безОбычныхДинПроверок}
	нц
		value := сравнениеСОбменом (counter.value, 0, 0);
		если value < amount то прервиЦикл всё;
		если сравнениеСОбменом (counter.value, value, value - amount) = value то прервиЦикл всё;
		CPU.Backoff;
	кц;
	возврат value;
кон Decrement;

(** Atomically increments the value of an atomic counter. *)
проц Inc- (перем counter: Counter);
перем value: размерМЗ;
нач {безКооперации, безОбычныхДинПроверок}
	нц
		value := сравнениеСОбменом (counter.value, 0, 0);
		утв (value # матМаксимум (размерМЗ));
		если сравнениеСОбменом (counter.value, value, value + 1) = value то прервиЦикл всё;
		CPU.Backoff;
	кц;
кон Inc;

(** Atomically decrements the value of an atomic counter. *)
проц Dec- (перем counter: Counter);
перем value: размерМЗ;
нач {безКооперации, безОбычныхДинПроверок}
	нц
		value := сравнениеСОбменом (counter.value, 0, 0);
		утв (value # матМинимум (размерМЗ));
		если сравнениеСОбменом (counter.value, value, value - 1) = value то прервиЦикл всё;
		CPU.Backoff;
	кц;
кон Dec;

кон Counters.
