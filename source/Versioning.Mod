(**
	@author fof
	@date 2012.07.06

	collection of tools to do tagging of files/logs/documents that make an identification of all relevant state possible
**)
модуль Versioning;

использует Dates, Strings, Потоки, Modules, Files;

	проц WriteModules*(w: Потоки.Писарь);
	перем m: Modules.Module;
	нач
		m := Modules.root;
		нцПока m # НУЛЬ делай w.пСтроку8(m.name); w.пСтроку8(":"); w.п16ричное(m.crc,-8); w.пСтроку8(" "); m := m.next кц;
	кон WriteModules;

	проц WriteDate*(w: Потоки.Писарь);
	перем s: массив 32 из симв8;
	нач
		Strings.FormatDateTime("yyyy.mm.dd hh:nn:ss", Dates.Now(), s); w.пСтроку8(s);
	кон WriteDate;

	проц WriteAuthor*(w: Потоки.Писарь);
	нач
	кон WriteAuthor;

	проц TagFileName*(fileName: массив из симв8; перем result: массив из симв8);
	перем extension: Files.FileName; s: массив 32 из симв8;
	нач
		Files.SplitExtension(fileName, result, extension);
		Strings.FormatDateTime("_yyyymmdd_hhnnss",Dates.Now(), s);
		Strings.Append(result,s);
		Files.JoinExtension(result, extension, result);
	кон TagFileName;

	проц NewLogWriter*(перем logFileName: массив из симв8; конст purpose,name: массив из симв8): Потоки.Писарь;
	нач
		возврат NewLogWriterInner(logFileName, истина, purpose, name) кон NewLogWriter;

	(* Polite означает, что имя файла не содержит даты, а равно заданному. Дата и время
		вполне хорошо себя чувствуют и внутри файла *)
	проц NewPoliteLogWriter*(перем logFileName: массив из симв8; конст purpose,name: массив из симв8): Потоки.Писарь;
	нач
		возврат NewLogWriterInner(logFileName, ложь, purpose, name) кон NewPoliteLogWriter;



	проц NewLogWriterInner(перем logFileName: массив из симв8; tagFileName: булево; конст purpose,name: массив из симв8): Потоки.Писарь;
	перем logFile: Files.File; log: Files.Writer;
	нач
		если tagFileName то 
			TagFileName(logFileName, logFileName) всё;
		logFile := Files.New(logFileName);
		если logFile = НУЛЬ то возврат НУЛЬ всё;
		Files.Register(logFile);
		нов(log,logFile,0);
		log.пСтроку8(purpose); log.пСтроку8(":"); log.пСтроку8(name); log.пВК_ПС;
		WriteDate(log); log.пВК_ПС;
		WriteAuthor(log); log.пВК_ПС;
		WriteModules(log); log.пВК_ПС;
		возврат log
	кон NewLogWriterInner;


кон Versioning.
