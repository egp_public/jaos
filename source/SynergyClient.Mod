модуль SynergyClient; (** AUTHOR "thomas.frey@alumni.ethz.ch"; PURPOSE "Synergy Client"; *)
(* 2007.08.26
	Limitations:
		* Only supports Mouse and Keyboard for now
		* Not all key combinations are understood correctly
		* I took the protocol information from looking at the network traffic with Ethereal because i did not find a clean spec and was
			too lazy to dig through the source code at http://synergy2.sourceforge.net/ . So dont take it as "according to spec".
*)
использует
	Modules, Objects, Commands, Потоки, IP, TCP, DNS, ЛогЯдра, Strings, Inputs,  WMWindowManager, WMMessages;

конст
	DebugKeyboard = ложь;

тип
	SynergyClient = окласс
	перем in : Потоки.Чтец;
		out : Потоки.Писарь;
		connection : TCP.Connection;
		packet : массив 2048 из симв8;
		errors : булево;
		manager : WMWindowManager.WindowManager;
		originator : WMWindowManager.ViewPort;
		mouseKeys : мнвоНаБитахМЗ;
		mouseX, mouseY : цел32;
		running : булево;
		screenName : массив 128 из симв8;

		lastKeysym, lastUcs : цел32;

		проц &New*(conn : TCP.Connection; sName : массив из симв8);
		нач
			running := истина;
			connection := conn;
			копируйСтрокуДо0(sName, screenName);
			нов(in, connection.ПрочтиИзПотока, 1024);
			нов(out, connection.ЗапишиВПоток, 1024);
			manager := WMWindowManager.GetDefaultManager();
			originator := WMWindowManager.GetDefaultView(); (* make synergy events appear as coming from the default view. *)
		кон New;

		проц GetPacket16(pos : цел32) : цел32;
		перем int16 : цел16;
		нач
			int16 := кодСимв8(packet[pos]) * 256 + кодСимв8(packet[pos + 1]);
			возврат int16
		кон GetPacket16;

		проц GetPacket32(pos : цел32) : цел32;
		перем int32 : цел32;
		нач
			int32 := кодСимв8(packet[pos]) * 1000000H + кодСимв8(packet[pos + 1]) * 10000H + кодСимв8(packet[pos + 2]) * 100H + кодСимв8(packet[pos + 3]);
			возврат int32
		кон GetPacket32;

		проц SendClientHello(screenName : массив из симв8);
		перем strLen : цел32;
		нач {единолично}
			strLen := Strings.Length(screenName)(цел32);
			out.пЦел32_сп(11 + 4 + strLen);
			out.пСтроку8("Synergy");
			out.п2МладшихБайта_сп(1);
			out.п2МладшихБайта_сп(3);
			out.пЦел32_сп(strLen);
			out.пСтроку8(screenName);
			out.ПротолкниБуферВПоток
		кон SendClientHello;

		проц SendDINF(left, top, width, height, wrap, pointerX, pointerY : цел32);
		нач  {единолично}
			out.пЦел32_сп(4 + 7 * 2);
			out.пСтроку8("DINF");
			out.п2МладшихБайта_сп(left); out.п2МладшихБайта_сп(top); out.п2МладшихБайта_сп(width); out.п2МладшихБайта_сп(height);
			out.п2МладшихБайта_сп(wrap);
			out.п2МладшихБайта_сп(pointerX); out.п2МладшихБайта_сп(pointerY);
			out.ПротолкниБуферВПоток
		кон SendDINF;

		проц SendNOP;
		нач  {единолично}
			out.пЦел32_сп(4);
			out.пСтроку8("CNOP");
			out.ПротолкниБуферВПоток
		кон SendNOP;

		проц MouseEvent(x, y, dz: цел32; keys : мнвоНаБитахМЗ);
		перем msg : WMMessages.Message;
		нач
			msg.originator := originator;
			msg.msgType := WMMessages.MsgPointer;
			msg.x := x; msg.y := y;
			msg.dz := dz;
			msg.flags := keys;
			если manager # НУЛЬ то если manager.sequencer.Add(msg) то всё всё;
		кон MouseEvent;

		проц KeyEvent(ucs: цел32; flags : мнвоНаБитахМЗ; keysym : цел32);
		перем msg : WMMessages.Message;
		нач
			msg.originator := originator;
			msg.msgType := WMMessages.MsgKey;
			msg.x := ucs;
			msg.y := keysym;
			msg.flags := flags;
			если manager.sequencer.Add(msg) то всё;
		кон KeyEvent;

		проц ConvertKey(keyId, keyMask, keyButton : цел32; перем ucs: цел32; перем flags : мнвоНаБитахМЗ; перем keysym : цел32; down : булево);
		нач
			если down то flags := {} иначе flags := {Inputs.Release} всё;
			если keyMask остОтДеленияНа 2 = 1 то (* shift is pressed *)
				flags := flags + Inputs.Shift
			всё;
			если (keyMask DIV 2)  остОтДеленияНа 2 = 1 то (* ctrl is pressed *)
				flags := flags + Inputs.Ctrl
			всё;
			если (keyMask DIV 4)  остОтДеленияНа 2 = 1 то (* alt is pressed *)
				flags := flags + Inputs.Alt
			всё;

			если (keyMask DIV 16)  остОтДеленияНа 2 = 1 то (* meta is pressed *)
				flags := flags + Inputs.Meta
			всё;

			если keyId > 0 то
				ucs := keyId; keysym := keyId;
				если Inputs.Ctrl * flags # {}  то
					если (ASCII_вЗаглавную(симв8ИзКода(ucs)) >= "A") и (ASCII_вЗаглавную(симв8ИзКода(ucs)) <= "Z") то keysym := кодСимв8(ASCII_вЗаглавную(симв8ИзКода(ucs))) - 64; ucs := 0 всё
				всё

			иначе
				просей keyButton из
					|1 : keysym := lastKeysym; ucs := lastUcs
					|14 : keysym := 0FF08H; ucs := 07FH
					|15 : keysym := 0FF09H; ucs := 09H; (* tab *)
					|28 : keysym := 0FF0DH; ucs := 0DH;
					|42 : keysym := 0FFE1H; ucs := 0H; (* shift *)
					|29 : keysym := 0FFE3H; ucs := 0H; (* ctrl *)
					|56 : keysym := 0FFFFH; ucs := 0H; (* alt *)
					|58 : keysym := 0FFFFH; ucs := 0H; (* Scroll Lock *)
					|331: keysym := 0FF51H; ucs := 0C4H; (* cursor left *)
					|333: keysym := 0FF53H; ucs := 0C3H; (* cursor right *)
					|328: keysym := 0FF52H; ucs := 0C1H; (* cursor up *)
					|336: keysym := 0FF54H; ucs := 0C2H; (* cursor down *)
					|327: keysym := 0FF50H; ucs := 0A8H; (* cursor home *)
					|329: keysym := 0FF55H; ucs := 0A2H; (* cursor PgUp *)
					|337: keysym := 0FF56H; ucs := 0A3H; (* cursor PgDn *)
					|335: keysym := 0FF57H; ucs := 0A9H; (* cursor End *)
					|339: keysym := 0FFFFH; ucs := 0A1H; (* Delete *)
					|349: keysym := 0FF67H; ucs := 0; (* meta ? menu *)
					|347: keysym := 0FF67H; ucs := 0; (* meta ? real *)
				иначе
					если DebugKeyboard то
						ЛогЯдра.пСтроку8("keyId= "); ЛогЯдра.пЦел64(keyId, 0);
						ЛогЯдра.пСтроку8("keyMask= "); ЛогЯдра.пЦел64(keyMask, 0);
						ЛогЯдра.пСтроку8("keyButton= "); ЛогЯдра.пЦел64(keyButton, 0); ЛогЯдра.пВК_ПС;
					всё;
					keysym := 0H; ucs := 0H
				всё
			всё;
			lastKeysym := keysym; lastUcs := ucs
		кон ConvertKey;

		проц Loop;
		перем
			packetLength, i : цел32;
			packetType : массив 5 из симв8;

			len: размерМЗ;
			x, dz, w, h : цел32;
			keyId, keyMask, keyButton : цел32;

			ucs, keysym : цел32;
			flags : мнвоНаБитахМЗ;

		нач
			ЛогЯдра.пСтроку8("Synergy Client : connected to server."); ЛогЯдра.пВК_ПС;
			errors := ложь;
			нцПока ~errors и (in.кодВозвратаПоследнейОперации = 0) делай
				packetLength := in.чЦел32_сп();
				in.чБайты(packet, 0, packetLength, len);
				если len # packetLength то errors := истина
				иначе
					нцДля i := 0 до 3 делай packetType[i] := packet[i] кц; packetType[4] := 0X;
					если packetType = "Syne" то
						SendClientHello(screenName)
					аесли packetType = "QINF" то (* request for screen information ? *)
						w := округлиВниз(originator.range.r - originator.range.l);
						h := округлиВниз(originator.range.b - originator.range.t);
						SendDINF(0, 0, w, h, 0, 100, 100)
					аесли packetType = "EUNK" то (* probably the screen is not known on the server... add it and try again*)
						ЛогЯдра.пСтроку8("probably the screen is not known on the synergy server... add it and try again"); ЛогЯдра.пВК_ПС;
					аесли packetType = "CROP" то (* client reset options... what options ?? *)
					аесли packetType = "DSOP" то (* set some options *)
					аесли packetType = "CIAK" то (* ack the resolution settings *)
					аесли packetType = "CALV" то (* Is  the client still alive ?  say CNOP(E) ;-) *)
						SendNOP
					аесли packetType = "CINN" то (* client enter... we got the pointer *)
						mouseX := GetPacket16(4); mouseY := GetPacket16(6);
						SendNOP
					аесли packetType = "COUT" то (* ... we lost the pointer *)
					аесли packetType = "DCLP" то (* Something with the clipboard *)
					аесли packetType = "DMMV" то (* Mouse move *)
						mouseX := GetPacket16(4); mouseY := GetPacket16(6);
						MouseEvent(mouseX, mouseY, 0, mouseKeys);
						SendNOP
					аесли packetType = "DMDN" то (* Mouse down *)
						x := кодСимв8(packet[4]);
						если (x >= 1) и (x <= 3) то включиВоМнвоНаБитах(mouseKeys, x - 1) всё;
						MouseEvent(mouseX, mouseY, 0, mouseKeys);
						SendNOP
					аесли packetType = "DMUP" то (* Mouse up *)
						x := кодСимв8(packet[4]);
						если (x >= 1) и (x <= 3) то исключиИзМнваНаБитах(mouseKeys, x - 1) всё;
						MouseEvent(mouseX, mouseY, 0, mouseKeys);
						SendNOP
					аесли packetType = "DMWM" то (* Scroll wheel *)
						x := GetPacket32(4);
						если x < 0 то dz := 1 иначе dz := -1 всё;
						MouseEvent(mouseX, mouseY, dz, mouseKeys);
						SendNOP
					аесли (packetType = "DKDN") или (packetType = "DKRP")  то (* KeyDown *)
						keyId := GetPacket16(4); keyMask := GetPacket16(6); keyButton := GetPacket16(8);
						flags := {};
						ConvertKey(keyId, keyMask, keyButton, ucs, flags, keysym, истина);
						KeyEvent(ucs, flags, keysym);
						SendNOP
					аесли packetType = "DKUP" то (* KeyUp *)
						keyId := GetPacket16(4); keyMask := GetPacket16(6); keyButton := GetPacket16(8);
						ConvertKey(keyId, keyMask, keyButton, ucs, flags, keysym, ложь);
						keysym := 0FFFFH; ucs := 0;
						KeyEvent(ucs, flags, keysym);
						SendNOP
					иначе
						ЛогЯдра.пСтроку8("packetType= "); ЛогЯдра.пСтроку8(packetType); ЛогЯдра.пВК_ПС;
					всё
				всё
			кц;

			running := ложь;
			ЛогЯдра.пСтроку8("Synergy client stopped"); ЛогЯдра.пВК_ПС
		кон Loop;

	нач {активное}
		Loop;
	кон SynergyClient;

перем
	client : SynergyClient;

проц Connect*(context : Commands.Context);
перем
	serverIP : IP.Adr;
	res : целМЗ;
	connection : TCP.Connection;
	server, screenName : массив 128 из симв8;
нач
	(* server *)
	если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(server) или ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(screenName) то
		context.out.пСтроку8('Start with SynergyClient.Connect "ServerName" "ScreenName" ~'); context.out.пВК_ПС;
		возврат;
	всё;

	если client = НУЛЬ то
		DNS.HostByName(server, serverIP, res);
		нов(connection);
		connection.Open(TCP.NilPort, serverIP, 24800, res);
		если res = 0 то
			нов(client, connection, screenName);
			context.out.пСтроку8("Connection established"); context.out.пВК_ПС;
		иначе
			context.out.пСтроку8("Could not connect to server."); context.out.пВК_ПС
		всё
	иначе
		context.out.пСтроку8("Already connected."); context.out.пВК_ПС;
	всё;
кон Connect;

проц Close*(context : Commands.Context);
нач
	если client # НУЛЬ то
		client.connection.Закрой();
		context.out.пСтроку8("Connection closed"); context.out.пВК_ПС;
		client := НУЛЬ;
	всё;
кон Close;

проц Cleanup;
перем i : цел32;
нач
	если client # НУЛЬ то
		client.connection.Закрой();
		i := 0; нцПока (i < 1000) и client.running делай Objects.Yield; увел(i) кц
	всё
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон SynergyClient.

System.Free SynergyClient ~
SynergyClient.Connect "192.168.0.3" "Bluebottle" ~
SynergyClient.Close ~
