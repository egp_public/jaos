модуль Serials; (** AUTHOR "afi"; PURPOSE "Generic serial communication ports driver"; *)
(**
 * Designed to support serial communication via the conventional RS232-type serial communication ports COM1 to COM8 and
 * via a USB port with a USB to serial adapter. Hot plug-in of the device is possible. For that reason, devices  are registered/unregistered
 * by procedures located in this module.
 *
 * Usage:
 *
 *	Serials.Show ~	displays a list of all available serial ports
 *
 *	Serials.CloseAllPorts ~ forces closing all serials ports
 *
 * History:
 *
 *	20.01.2006	First Release (afi)
 *	14.06.2006	Fixed Port.Send, introduced result value for Port.SendChar, implemented termination handler, cleanup (staubesv)
 *	26.06.2006	Added charactersSent, characterReceived for tracking (staubesv)
 *	04.08.2006	Removed SetPortState from Port interface (staubesv)
 *)

использует Потоки, Modules, ЛогЯдра, Commands, ЭВМ, Трассировка;

конст
	Verbose = истина;

	MaxPorts* = 64;

	(** Parity *)
	ParNo* = 0;  ParOdd* = 1;  ParEven* = 2;  ParMark* = 3;  ParSpace* = 4;

	(** Stop bits *)
	Stop1* = 1;  Stop2* = 2;  Stop1dot5* = 3;

	(* Serial port default settings *)
	DefaultBPS = 115200; DefaultDataBits = 8; DefaultParity = ParNo; DefaultStop = Stop1;

	(** Modem control lines *)
	DTR* = 0;  RTS* = 1;	(** output *)
	Break* = 2;	(** input/output - Bit 6 in LCR *)
	DSR* = 3;  CTS* = 4;  RI* = 5;  DCD* = 6;	(** input *)

	(** Receive error diagnostic *)
	OverrunError* = 10;
	ParityError* = 11;
	FramingError* = 12;
	BreakInterrupt* = 13;

	(** Error conditions *)
	Ok* =  0;
	Closed* = -1;
	TransportError* = -2;  (** Error on transport layer, e.g. USB error in RS-232 over USB *)
	TimeoutExpired* = -3; (** Timeout expired *)

	(** Errors for Port.Open procedure *)
	PortInUse* =  1; NoSuchPort* =  2; WrongBPS* =  3; WrongData* =  4; WrongParity* =  5; WrongStop* =  6;

тип

	Port* = окласс (Потоки.ФункциональныйИнтерфейсКПотоку);
	перем
		name- : массив 6 из симв8;
		description- : массив 128 из симв8;

		(** Characters sent/read since port has been opened. Consider these fields read-only! *)
		charactersSent*, charactersReceived* : Потоки.ТипМестоВПотоке;

		проц Open* (bps, data, parity, stop : цел32; перем res: целМЗ);
		кон Open;

		проц {перекрыта}Закрой* ;
		кон Закрой;

		проц SendChar* (ch: симв8; перем res : целМЗ);
		кон SendChar;

		(** Send len characters from buf to output, starting at ofs. res is non-zero on error. *)
		проц {перекрыта}ЗапишиВПоток*(конст buf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
		перем i : размерМЗ;
		нач
			i := 0;
			нцПока i < len делай
				SendChar(buf[ofs + i], res);
				если res # Ok то возврат всё;
				увел(i)
			кц
		кон ЗапишиВПоток;

		проц ReceiveChar* (перем ch: симв8; перем res: целМЗ);
		кон ReceiveChar;

		(** Receive size characters into buf, starting at ofs and return the effective number of bytes read in len.
			Wait until at least min bytes (possibly zero) are available. res is non-zero on error. *)
		проц {перекрыта}ПрочтиИзПотока*(перем buf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);
		перем ch: симв8;
		нач
			len := 0;
			res := Ok;
			нцПока (len < min) делай
				ReceiveChar(ch, res);
				если res # Ok то возврат всё;
				buf[ofs + len] := ch;
				увел(len);
			кц;
			нцПока (Available() > 0) и (len < size) делай
				ReceiveChar(ch, res);
				если res # Ok то возврат всё;
				buf[ofs + len] := ch;
				увел(len)
			кц;
		кон ПрочтиИзПотока;

		проц Available*(): размерМЗ;
		кон Available;

		(** Get the port state: state (open, closed), speed in bps, no. of data bits, parity, stop bit length. *)
		проц GetPortState*(перем openstat : булево; перем bps, data, parity, stop : цел32);
		кон GetPortState;

		(** Clear the specified modem control lines.  s may contain DTR, RTS & Break. *)
		проц ClearMC*(s: мнвоНаБитахМЗ);
		кон ClearMC;

		(** Set the specified modem control lines.  s may contain DTR, RTS & Break. *)
		проц SetMC*(s: мнвоНаБитахМЗ);
		кон SetMC;

		(** Return the state of the specified modem control lines. s contains
			the current state of DSR, CTS, RI, DCD & Break Interrupt. *)
		проц GetMC*(перем s: мнвоНаБитахМЗ);
		кон GetMC;

		(** Setup receive timeout (maximum time allowed to elapse before arrival of the next data byte) in ms.
			Use timeout <= 0 to disable receive timeout handling  *)
		проц SetReceiveTimeout*(timeout: цел32);
		кон SetReceiveTimeout;

		проц Show*;
		нач
			ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(" ("); ЛогЯдра.пСтроку8(description); ЛогЯдра.пСтроку8(")");
		кон Show;

	кон Port;

перем
	(* 	In this array the RS232-type COM ports are registered in the first 8 array elements.
		USB ports equipped with a USB to serial adapter are registered in the next 8 array elements. *)
	ports : массив MaxPorts из Port;

	tracePort: Port;
	traceChar0: Трассировка.ТипПроцедуры˛реализующейВыводСимв8ДляТрассировки;

(** At the disposal of the USB driver modules for hot plug-in of a device. *)
проц RegisterPort* (port : Port; конст description : массив из симв8);
перем name : массив 6 из симв8; portNumber : цел32;
нач {единолично}
	утв(port # НУЛЬ);
	portNumber := 9;
	нцПока (portNumber < длинаМассива(ports)) и (ports[portNumber-1] # НУЛЬ) делай увел(portNumber); кц;
	если portNumber < длинаМассива(ports) то
		ports[portNumber-1] := port;
		name := "COM";
		если portNumber < 10 то
			name[3] := симв8ИзКода(кодСимв8("0") + portNumber);
		иначе
			name[3] := симв8ИзКода(кодСимв8("0") + portNumber DIV 10);
			name[4] := симв8ИзКода(кодСимв8("0") + portNumber остОтДеленияНа 10);
		всё;
		копируйСтрокуДо0(name, port.name);
		копируйСтрокуДо0(description, port.description);
		если Verbose то ЛогЯдра.пСтроку8("Serials: "); port.Show; ЛогЯдра.пСтроку8(" is now available."); ЛогЯдра.пВК_ПС; всё;
	иначе
		ЛогЯдра.пСтроку8("Serials: Could not register port: No free slots."); ЛогЯдра.пВК_ПС;
	всё;
кон RegisterPort;

(** At the disposal of the USB driver modules for hot plug-in of a device. *)
проц UnRegisterPort* (port : Port);
перем i : цел32;
нач {единолично}
	i := 0; нцПока (i < длинаМассива(ports)) и (ports[i] # port) делай увел(i); кц;
	если i < длинаМассива(ports) то
		ports[i].Закрой;
		ports[i] := НУЛЬ;
		если Verbose то ЛогЯдра.пСтроку8("Serials: "); port.Show; ЛогЯдра.пСтроку8(" has been removed."); ЛогЯдра.пВК_ПС; всё;
	иначе
		ЛогЯдра.пСтроку8("Serials: Warning: UnRegisterPort: Port not found."); ЛогЯдра.пВК_ПС;
	всё;
кон UnRegisterPort;

(**	COM1 to COM8 are reserved for RS-232 / V24 communication ports. Other ports will be named COM9, COM9 and so on as needed.
	The idea is that the onboard COM ports get the same port numbers as in the BIOS *)
проц RegisterOnboardPort*(portNumber : цел32; port : Port; конст name, description : массив из симв8);
нач {единолично}
	если (portNumber >= 1) и (portNumber <= длинаМассива(ports)) и (ports[portNumber-1] = НУЛЬ) то
		ports[portNumber-1] := port;
		копируйСтрокуДо0(name, port.name);
		копируйСтрокуДо0(description, port.description);
		если Verbose то ЛогЯдра.пСтроку8("Serials: "); port.Show; ЛогЯдра.пСтроку8(" is now available."); ЛогЯдра.пВК_ПС; всё;
	иначе
		ЛогЯдра.пСтроку8("Serials: Warning; Could not register onboard port."); ЛогЯдра.пВК_ПС;
	всё;
кон RegisterOnboardPort;

проц GetPort* (portNumber : цел32) : Port;
перем port : Port;
нач {единолично}
	если (portNumber >= 1) и (portNumber <= длинаМассива(ports)) и (ports[portNumber-1] # НУЛЬ) то
		port := ports[portNumber-1];
	всё;
	возврат port;
кон GetPort;

проц TraceChar(ch: симв8);
перем res: целМЗ;
нач
	если tracePort # НУЛЬ то
		tracePort.SendChar(ch, res);
	всё;
кон TraceChar;

(**
	Setup serial port for kernel trace output

	portNumber: serial port number, or 0 to disable trace output, or a negative value for rolling back to the initial trace output configuration
	bps, data, parity, stop: serial port settings
*)
проц SetTracePort*(portNumber: цел32; bps, data, parity, stop: цел32; перем res: целМЗ);
перем
	isOpen0: булево;
	bps0, data0, parity0, stop0 : цел32;
	res1: целМЗ;
	port: Port;
нач{единолично}
	res := 0;
	если (portNumber >= 1) и (portNumber <= длинаМассива(ports)) и (ports[portNumber-1] # НУЛЬ) то

		port := ports[portNumber-1];

		port.GetPortState(isOpen0, bps0, data0, parity0, stop0);

		(* do not close the port if the current port settings match *)
		если ~isOpen0 или (bps0 # bps) или (data0 # data) или (parity0 # parity) или (stop0 # stop) то
			port.Закрой;
			port.Open(bps, data, parity, stop, res);
			если res # 0 то
				если isOpen0 то port.Open(bps0, data0, parity0, stop0, res1); всё;
				возврат;
			всё;
		всё;

		если Трассировка.пСимв8 # TraceChar то
			traceChar0 := Трассировка.пСимв8;
		всё;
		tracePort := port;
		Трассировка.пСимв8 := TraceChar;

	аесли portNumber = 0 то
		tracePort := НУЛЬ;
		если Трассировка.пСимв8 # TraceChar то
			traceChar0 := Трассировка.пСимв8;
		всё;
		Трассировка.пСимв8 := TraceChar;
	аесли portNumber < 0 то
		если traceChar0 # НУЛЬ то
			Трассировка.пСимв8 := traceChar0;
			tracePort := НУЛЬ;
			traceChar0 := НУЛЬ;
		всё;
	иначе
		res := NoSuchPort;
	всё;
кон SetTracePort;

(** Returns TRUE if a given port is currently used for kernel trace output *)
проц IsTracePort*(port: Port): булево;
нач
	возврат (tracePort # НУЛЬ) и (port = tracePort);
кон IsTracePort;

(**
	Get serial port parameters from an input stream

	The format of the stream is expected to be as [portNumber bps data parity stop] exactly in this predefined order;

	portNumber is obligatory parameter, the othe parameters are optional; if not specified they will be assigned to
	default values (DefaultBPS, DefaultDataBits, DefaultParity, DefaultStop)

	The format of parity and stop parameters is as follows:

		parity = "odd"|"even"|"mark"|"space"|"no"
		stop = "1"|"1.5"|"2"

	params: set of parameters specified by the user (0-bps, 1-data, 2-parity, 3-stop)
	res: error code, 0 in case of success
*)
проц GetPortParameters*(r: Потоки.Чтец; перем portNumber, bps, data, parity, stop: цел32; перем params: мнвоНаБитахМЗ; перем res: целМЗ);
перем
	str : массив 32 из симв8;
	d: цел32;
нач
	res := 0;

	если ~r.ПропустиБелоеПолеИЧитайЦел32(portNumber,ложь) или (GetPort(portNumber) = НУЛЬ) то
		res := NoSuchPort;
		возврат;
	всё;

	params := {};
	bps := DefaultBPS; data := DefaultDataBits; parity := DefaultParity; stop := DefaultStop;

	если r.ПропустиБелоеПолеИЧитайЦел32(d, ложь) то
		bps := d; включиВоМнвоНаБитах(params,0);
	иначе возврат;
	всё;

	если r.ПропустиБелоеПолеИЧитайЦел32(d, ложь) то
		data := d; включиВоМнвоНаБитах(params,1);
	иначе возврат;
	всё;

	если ~r.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(str) то
		возврат;
	всё;

	если str = "odd" то
		parity := ParOdd
	аесли str = "even" то
		parity := ParEven
	аесли str = "mark" то
		parity := ParMark
	аесли str = "space" то
		parity := ParSpace
	аесли str # "no" то
		res := WrongParity;
		возврат;
	всё;

	включиВоМнвоНаБитах(params,2);

	если ~r.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(str) то
		возврат;
	всё;

	если str = "1.5" то
		stop := Stop1dot5
	аесли str = "2" то
		stop := Stop2
	аесли str # "1" то
		res := WrongStop;
		возврат;
	всё;

	включиВоМнвоНаБитах(params,3);
кон GetPortParameters;

(**
	Setup trace over a given serial port

	SetTrace portNumber bps data parity stop~

	portNumber: serial port number, or 0 to disable trace output, or a negative value for rolling back to the initial trace output configuration
*)
проц SetTrace*(context: Commands.Context);
перем
	portNumber, bps, data, parity, stop: цел32; res: целМЗ; params: мнвоНаБитахМЗ;
нач
	GetPortParameters(context.arg, portNumber, bps, data, parity, stop, params, res);
	если (portNumber > 0) и (res # 0) то
		context.result := Commands.CommandError;
		context.error.пСтроку8("Invalid port settings, res="); context.error.пЦел64(res,0); context.error.пВК_ПС;
		возврат;
	всё;

	SetTracePort(portNumber, bps, data, parity, stop, res);
	если res # 0 то
		context.result := Commands.CommandError;
		context.error.пСтроку8("Failed to setup trace port, res="); context.error.пЦел64(res,0); context.error.пВК_ПС;
	всё;
кон SetTrace;

проц Show*(context : Commands.Context);
перем port : Port; noPortsAvailable : булево; i : цел32;
нач {единолично}
	noPortsAvailable := истина;
	context.out.пСтроку8("Serials: "); context.out.пВК_ПС;
	нцДля i := 0 до длинаМассива(ports)-1 делай
		port := ports[i];
		если port # НУЛЬ то
			noPortsAvailable := ложь;
			context.out.пСтроку8(port.name); context.out.пСимв8(9X); context.out.пСтроку8(port.description); context.out.пВК_ПС;
		всё;
	кц;
	если noPortsAvailable то context.out.пСтроку8("No serial ports found."); всё;
	context.out.пВК_ПС;
кон Show;

(** Test serial ports COM1 and if present COM2 with the generic driver *)
проц Test*(context : Commands.Context);
перем
	result : целМЗ;
	portgotten : Port;
нач
	context.out.пСтроку8 ("Testing availability of COM1 and COM2."); context.out.пВК_ПС;
	context.out.пСтроку8 ("Testing COM1: ");
	portgotten := GetPort (1);
	если portgotten # НУЛЬ то
		portgotten.Open (4800, 8, 2, 2, result);
		portgotten.Закрой ();
		context.out.пСтроку8 ("available, result="); context.out.пЦел64(result, 0); context.out.пВК_ПС;
		context.out.пСтроку8 ("Testing COM2: ");
		portgotten := GetPort (2);
		если portgotten # НУЛЬ то
			portgotten.Open (9600, 8, 2, 2, result);
			portgotten.Закрой ();
			context.out.пСтроку8 ("available, result="); context.out.пЦел64(result, 0); context.out.пВК_ПС
		иначе
			context.out.пСтроку8 ("Could not get port 2"); context.out.пВК_ПС
		всё
	иначе
		context.out.пСтроку8 ("Could not get port 1"); context.out.пВК_ПС;
		context.out.пСтроку8 ("Not testing COM2 as it is likely not to work either."); context.out.пВК_ПС;
	всё;
кон Test;

(** Close all serial ports *)
проц CloseAllPorts*(context : Commands.Context);
перем portNbr : цел32;
нач {единолично}
	нцДля portNbr := 0 до длинаМассива(ports)-1 делай
		если ports[portNbr] # НУЛЬ то
			ports[portNbr].Закрой;
		всё;
	кц;
	если (context # НУЛЬ) то context.out.пСтроку8("All serial ports closed"); context.out.пВК_ПС; всё;
кон CloseAllPorts;

проц Cleanup;
нач
	CloseAllPorts(НУЛЬ);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
кон Serials.

System.Free V24 Serials ~

V24.Install ~
Serials.Test ~
Serials.Show ~

