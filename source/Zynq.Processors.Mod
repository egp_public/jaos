(* Runtime support for Zynq *)
(* Copyright (C) Florian Negele *)

(** The Processors module represents all logical processors of the system. *)
модуль Processors;

использует НИЗКОУР, Counters, CPU;

(** Indicates the maximal number of logical processors that are supported by the system. *)
конст Maximum* = 2 + CPU.Interrupts;

(** Holds the actual number of processors in the system. *)
перем count-: размерМЗ;

перем running: Counters.AlignedCounter; (* counts the number of application processors currently running *)

(** Returns the unique index of the processor executing this procedure call. *)
проц GetCurrentIndex- отКомпоновщика "Activities.GetCurrentProcessorIndex" (): размерМЗ;

(** Suspends the execution of the current processor. *)
(** A suspended processor must be resumed by a call to the Processors.ResumeAnyProcessor procedure. *)
(** @topic Scheduling *)
проц SuspendCurrentProcessor-;
машКод
	WFE
кон SuspendCurrentProcessor;

(** Resumes the execution of a single suspended processor. *)
(** @topic Scheduling *)
проц ResumeAllProcessors-;
машКод
	SEV
кон ResumeAllProcessors;

(** Starts the execution of all available processors. *)
(** @topic Scheduling *)
проц StartAll-;
машКод
	SEV
кон StartAll;

проц {NORETURN, NOPAF} Boot;
проц Idle отКомпоновщика "Activities.Idle";
проц Execute отКомпоновщика "Activities.Execute" (procedure: проц);
нач {безКооперации, безОбычныхДинПроверок}
	Counters.Inc (running);
	SuspendCurrentProcessor;
	SuspendCurrentProcessor;
	Execute (Idle);
	Counters.Dec (running);
	CPU.Halt;
кон Boot;

(** Initializes the module by enumerating all available processors. *)
(** @topic Runtime Call *)
проц Initialize-;
нач {безКооперации, безОбычныхДинПроверок}
	ResumeAllProcessors;
	нцДо кцПри Counters.Read (running) = 1;
	count := 2;
кон Initialize;

(** Terminates the module and waits for all other processors to stop their execution. *)
(** @topic Runtime Call *)
проц Terminate-;
нач {безКооперации, безОбычныхДинПроверок}
	нцДо кцПри Counters.Read (running) = 0;
кон Terminate;

кон Processors.
