модуль RMSMTP; (** AUTHOR "retmeier"; PURPOSE "A SMTP client"; *)

использует
	SMTPClient, Commands, ЛогЯдра, Strings, Потоки, Classes := TFClasses, IMAPClient, IMAPUtilities;

конст
	DEBUG = истина;
	CR = 0DX; LF = 0AX;
	Port = 25;

	OK* = 0;
	CONNECTIONERROR* = 1;
	FROMERROR* = 2;
	TOERROR* = 3;
	CCERROR* = 4;
	BCCERROR* = 5;
	DATAERROR* = 6;
	FINISHERROR* = 7;

тип
	String* = Strings.String;


проц Send*(context : Commands.Context);
перем
	message: IMAPClient.Message;
	server, thisHost: массив 1024 из симв8;
	ret: цел32;
нач
	(* parse the message from the input string *)
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(server);
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(thisHost);

	нов(message);
	parse(context, message);

	message.header.date := IMAPUtilities.getRFC822Date();
	ret := SendMessage(message, server, thisHost);
кон Send;

проц SendMessage*(message: IMAPClient.Message; конст server, thisHost: массив из симв8): цел32;
перем
	smtp: SMTPClient.SMTPSession;
	i: цел32;
	res: целМЗ;
	returnValue: цел32;
	w: Потоки.Писарь;
	p: динамическиТипизированныйУкль;
	s: String;
	address: IMAPUtilities.Address;
нач
	(* start communication with server *)
	нов(smtp);
	smtp.Open(server, thisHost, Port, res);
	если res # SMTPClient.Ok то
		ЛогЯдра.пСтроку8("Failure: it wasn't possible to connect to server: ");	ЛогЯдра.пСтроку8(server); ЛогЯдра.пВК_ПС();
		возврат CONNECTIONERROR;
	всё;

	(* send MAIL FROM command *)
	если (message.header.from # НУЛЬ) и (message.header.from.GetCount() > 0) то
		p := message.header.from.GetItem(0);
		address := p(IMAPUtilities.Address);
		AddressToSMTPString(address, s);
	иначе
		нов(s, Strings.Length(thisHost)+1);
		Strings.Copy(thisHost, 0, Strings.Length(thisHost), s^);
	всё;

	если DEBUG то
		ЛогЯдра.пСтроку8("MAIL FROM: "); ЛогЯдра.пСтроку8(s^); ЛогЯдра.пВК_ПС();
	всё;

	если ~smtp.StartMailFrom(s^) то
		ЛогЯдра.пСтроку8("Error occured while trying to send the Command: MAIL FROM. Maybe no from header-field was specified or it was errorous"); ЛогЯдра.пВК_ПС();
		smtp.Close();
		возврат FROMERROR;
	всё;

	(* send RCPT TO Commands *)
	если message.header.to # НУЛЬ то
		i := 0;
		нцПока i < message.header.to.GetCount() делай
			p := message.header.to.GetItem(i);
			address := p(IMAPUtilities.Address);
			AddressToSMTPString(address, s);
			если DEBUG то
				ЛогЯдра.пСтроку8("RCPT: "); ЛогЯдра.пСтроку8(s^); ЛогЯдра.пВК_ПС();
			всё;

			если ~smtp.SendTo(s^) то
				ЛогЯдра.пСтроку8("Error occured while trying to send the Command: RCPT TO for the receivers specified in To"); ЛогЯдра.пВК_ПС();
				smtp.Close();
				возврат TOERROR;
			всё;
			увел(i);
		кц;
	всё;
	если message.header.cc # НУЛЬ то
		i := 0;
		нцПока i < message.header.cc.GetCount() делай
			p := message.header.cc.GetItem(i);
			address := p(IMAPUtilities.Address);
			AddressToSMTPString(address, s);

			если ~smtp.SendTo(s^) то
				ЛогЯдра.пСтроку8("Error occured while trying to send the Command: RCPT TO for the receivers specified in Cc"); ЛогЯдра.пВК_ПС();
				smtp.Close();
				возврат CCERROR;
			всё;
			увел(i);
		кц;
	всё;
	если message.header.bcc # НУЛЬ то
		i := 0;
		нцПока i < message.header.bcc.GetCount() делай
			p := message.header.bcc.GetItem(i);
			address := p(IMAPUtilities.Address);
			AddressToSMTPString(address, s);

			если ~smtp.SendTo(s^) то
				ЛогЯдра.пСтроку8("Error occured while trying to send the Command: RCPT TO for the receivers specified in Bcc"); ЛогЯдра.пВК_ПС();
				smtp.Close();
				возврат BCCERROR;
			всё;
			увел(i);
		кц;
	всё;

	(* start sending the message data *)
	если ~smtp.StartData() то
		ЛогЯдра.пСтроку8("Error occured while trying to send the Command: DATA"); ЛогЯдра.пВК_ПС();
		smtp.Close();
		возврат DATAERROR;
	всё;

	s := message.ToString();

	w := smtp.w;
	w.пСтроку8(s^);
	w.пВК_ПС(); w.пСтроку8("."); w.пВК_ПС();

	если smtp.FinishSendRaw() то
		если DEBUG то
			ЛогЯдра.пСтроку8("send was sucessful"); ЛогЯдра.пВК_ПС();
		всё;
		returnValue := OK;
	иначе
		ЛогЯдра.пСтроку8("send failed"); ЛогЯдра.пВК_ПС();
		returnValue := FINISHERROR;
	всё;

	smtp.Close();

	возврат returnValue;
кон SendMessage;

проц parse*(context : Commands.Context; перем message: IMAPClient.Message);
перем
	token: массив 128 из симв8;
	buffer: Strings.Buffer;
	string: String;
	w: Потоки.Писарь;
	c: симв8;
	headerDone : булево;
	pos: Потоки.ТипМестоВПотоке;
	addresses: Classes.List;
	header: IMAPClient.HeaderElement;
нач
	(* process header of the message *)
	нов(header);

	context.arg.ПропустиБелоеПоле();
	headerDone := ложь;
	нцПока ~headerDone делай
		pos := context.arg.МестоВПотоке();
		context.arg.чЦепочкуСимв8ДоБелогоПоля(token);

		Strings.UpperCase(token);

		если token = "TO:" то
			context.arg.ПропустиПробелыИТабуляции();
			string := readRestOfLine(context);
			IMAPUtilities.ParseAddresses(string, addresses);
			header.to := addresses;
		аесли token = "CC:" то
			context.arg.ПропустиПробелыИТабуляции();
			string := readRestOfLine(context);
			IMAPUtilities.ParseAddresses(string, addresses);
			header.cc := addresses;
		аесли token = "BCC:" то
			context.arg.ПропустиПробелыИТабуляции();
			string := readRestOfLine(context);
			IMAPUtilities.ParseAddresses(string, addresses);
			header.bcc := addresses;
		аесли token = "FROM:" то
			context.arg.ПропустиПробелыИТабуляции();
			string := readRestOfLine(context);
			IMAPUtilities.ParseAddresses(string, addresses);
			header.from := addresses;
		аесли token = "SENDER:" то
			context.arg.ПропустиПробелыИТабуляции();
			string := readRestOfLine(context);
			IMAPUtilities.ParseAddresses(string, addresses);
			header.sender := addresses;
		аесли token = "SUBJECT:" то
			context.arg.ПропустиПробелыИТабуляции();
			string := readRestOfLine(context);
			header.subject := string;
		аесли token = "DATE:" то
			context.arg.ПропустиПробелыИТабуляции();
			string := readRestOfLine(context);
			header.date := string;
		иначе
			headerDone := истина;
		всё;

	кц;
	message.header := header;

	(* process message Body *)
	context.arg.ПерейдиКМестуВПотоке(pos);

	нов(buffer, 16);
	w := buffer.GetWriter();

	context.arg.чСимв8(c);
	нцПока c # 0X делай
		w.пСимв8(c);
		context.arg.чСимв8(c);
	кц;

	string := buffer.GetString();

	message.message := string;
кон parse;

проц readRestOfLine*(context : Commands.Context): String;
перем
	string: String;
	buffer: Strings.Buffer;
	w: Потоки.Писарь;
	c: симв8;
нач
	нов(buffer, 16);
	w := buffer.GetWriter();
	context.arg.чСимв8(c);
	нцПока(c # 0X) и (c # LF) и (c # CR) делай
		w.пСимв8(c);
		context.arg.чСимв8(c);
	кц;
	если c = CR то
		c := context.arg.ПодглядиСимв8();
		если c = LF то
			c := context.arg.чИДайСимв8();
		всё;
	всё;
	string := buffer.GetString();
	возврат string;
кон readRestOfLine;

проц AddressToSMTPString(address: IMAPUtilities.Address; перем string: String);
перем buffer: Strings.Buffer; w: Потоки.Писарь;
нач
	нов(buffer, 16);
	w := buffer.GetWriter();
	если DEBUG то
		ЛогЯдра.пСтроку8("In AddressToSMPTString namePart: "); ЛогЯдра.пСтроку8(address.namePart^);
		ЛогЯдра.пСтроку8(" domainPart: "); ЛогЯдра.пСтроку8(address.domainPart^); ЛогЯдра.пВК_ПС();
	всё;
	w.пСтроку8(address.namePart^);
	w.пСтроку8("@");
	w.пСтроку8(address.domainPart^);

	string := buffer.GetString();
кон AddressToSMTPString;

кон RMSMTP.
