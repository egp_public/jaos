(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль MathReSeries;   (** AUTHOR "adf"; PURPOSE "Procedures for computing real series"; *)

использует NbrInt, NbrRe, DataErrors;

тип
	Coefficient* = окласс
	перем
		(** The index of a coefficient to be determined - supplied by the calling function, e.g., PowerSeries. *)
		n-: NbrInt.Integer;
		(** The argument of the function being evaluated - supplied by the calling function, e.g., PowerSeries. *)
		x-: NbrRe.Real;
		(** The End Of Series - to be updated by Evaluate. *)
		eos*: булево;
		(** The derived coefficinet - to be updated by Evaluate. *)
		coef*: NbrRe.Real;

		(** Abstract - must be extended. Used to evaluate coefficients in user-defined series. *)
		проц Evaluate*;
		нач
			DataErrors.Error( "This function is abstract. It must be extended." )
		кон Evaluate;

	кон Coefficient;

перем
	epsilon: NbrRe.Real;

	(**                   a1(x)
		y = b0(x) + ----------
		                  b1(x) + a2(x)
		                              ----------
		                              b2(x) + a3(x)
		                                          --------
		                                          b3(x) + ...
	 *)
	проц ContinuedFraction*( a, b: Coefficient;  x: NbrRe.Real ): NbrRe.Real;
	(* Based on an algorithm from: Lentz, W.J., Applied Optics, 15, 1976, 668-671. *)
	перем convergedLast, convergedThis: булево;  c, d, delta, f: NbrRe.Real;
	нач
		(* Get the leading coefficient, b0. *)
		a.n := 0;  a.x := x;  a.eos := ложь;  b.n := 0;  b.x := x;  b.eos := ложь;  b.Evaluate;
		(* Initialize Lentz's recursive algorithm. *)
		если b.coef = 0 то b.coef := 1/NbrRe.MaxNbr всё;
		f := b.coef;  c := f;  d := 0;
		(* The recursive algorithm of Lentz. *)
		convergedThis := ложь;
		нцДо
			NbrInt.Inc( a.n );  a.Evaluate;  NbrInt.Inc( b.n );  b.Evaluate;  c := b.coef + a.coef * x / c;
			d := 1 / (b.coef + a.coef * x * d);  delta := c * d;  f := delta * f;  delta := 1 - delta;
			convergedLast := convergedThis;  convergedThis := NbrRe.Abs( delta ) < epsilon
		кцПри (convergedLast и convergedThis) или (a.eos или b.eos);
		возврат f
	кон ContinuedFraction;

(** The lengths of the supplied arrays  a & b  must be the same. *)
	проц TruncatedContinuedFraction*( a, b: массив из NbrRe.Real;  x: NbrRe.Real ): NbrRe.Real;
	перем i, aLen, bLen: размерМЗ;  c, d, f: NbrRe.Real;
	нач
		aLen := длинаМассива( a );  bLen := длинаМассива( b );
		если aLen # bLen то DataErrors.Error( "Lengths of supplied arrays must be equal." );  f := 0;  возврат f всё;
		если b[0] = 0 то b[0] := 1/NbrRe.MaxNbr всё;
		f := b[0];  c := f;  d := 0;  i := 1;
		нцДо c := b[i] + a[i] * x / c;  d := 1 / (b[i] + a[i] * x * d);  f := c * d * f;  увел( i ) кцПри i = aLen;
		возврат f
	кон TruncatedContinuedFraction;

(** y = a0 + a1x + a2x2 + a3x3 + ... *)
	проц PowerSeries*( a: Coefficient;  x: NbrRe.Real ): NbrRe.Real;
	перем convergedLast, convergedThis: булево;  sum, update, xx: NbrRe.Real;
	нач
		a.x := x;  a.n := 0;  a.eos := ложь;  a.Evaluate;  sum := a.coef;  xx := 1;  convergedThis := ложь;
		нцДо
			NbrInt.Inc( a.n );  a.Evaluate;  xx := x * xx;  update := a.coef * xx;  sum := sum + update;
			convergedLast := convergedThis;  convergedThis := NbrRe.Abs( update ) < (epsilon * NbrRe.Abs( sum ))
		кцПри (convergedLast и convergedThis) или a.eos;
		возврат sum
	кон PowerSeries;

	проц TruncatedPowerSeries*( a: массив из NbrRe.Real;  x: NbrRe.Real ): NbrRe.Real;
	перем i, len: размерМЗ;  prod: NbrRe.Real;
	нач
		len := длинаМассива( a );  prod := a[len - 1] * x;
		нцДля i := len - 2 до 1 шаг -1 делай prod := (a[i] + prod) * x кц;
		prod := a[0] + prod;  возврат prod
	кон TruncatedPowerSeries;

(**         a0 + a1x + a2x2 + a3x3 + ...
	 	y = --------------------
		    b0 + b1x + b2x2 + b3x3 + ...
	 *)
	проц RationalFunction*( a, b: Coefficient;  x: NbrRe.Real ): NbrRe.Real;
	перем denom, num, ratio: NbrRe.Real;
	нач
		num := PowerSeries( a, x );  denom := PowerSeries( b, x );  ratio := num / denom;  возврат ratio
	кон RationalFunction;

	проц TruncatedRationalFunction*( a, b: массив из NbrRe.Real;  x: NbrRe.Real ): NbrRe.Real;
	перем denom, num, ratio: NbrRe.Real;
	нач
		num := TruncatedPowerSeries( a, x );  denom := TruncatedPowerSeries( b, x );  ratio := num / denom;
		возврат ratio
	кон TruncatedRationalFunction;

нач
	epsilon := 10 * NbrRe.Epsilon
кон MathReSeries.
