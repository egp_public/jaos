модуль UsbEhciPhy; (** AUTHOR "Timothée Martiel"; PURPOSE "TUSB1210 USB EHCI PHY Control"; *)

использует НИЗКОУР, Kernel, ЛогЯдра, Gpio, UsbDebug;

конст
	(* Ulpi Viewport bits *)
	UlpiWakeup = 31;
	UlpiRun = 30;
	UlpiWrite = 29;
	UlpiAddressMask = {16 .. 23};
	UlpiAddressOfs = 16;
	UlpiWriteDataMask = {0 .. 7};

	(* ULPI addresses *)
	FuncCtrl = 4H;
	FuncCtrlSet = 5H;
	FuncCtrlClr = 6H;
	IfcCtrlSet = 8H;
	IfcCtrlClr = 9H;
	OtgCtrlSet = 0BH;
	OtgCtrlClr = 0CH;

	(* Register bits in FuncCtrl *)
	Reset = 5;
	SuspendM = 6;
	Opmode = {3, 4};
	OpmodeNorm = {};
	XcvrSelect = {0, 1};
	XcvrHS = {};
	XcvrFS = {0};
	XcvrLS = {1};


	(* Register bits in OtgCtrl *)
	IdPullUp = 0;
	DmPullDown = 2;
	DpPullDown = 1;
	DrvVbus = 5;
	DrvVbusExt = 6;

	(* Default Timeout. Value comes from Linux implementation *)
	Timeout = 2000;

	(** Wakeup ULPI *)
	проц Wakeup (viewport: адресВПамяти): булево;
	перем
		timer: Kernel.MilliTimer;
		reg: мнвоНаБитахМЗ;
	нач
		Kernel.SetTimer(timer, Timeout);
		НИЗКОУР.запиши32битаПоАдресу(viewport, {UlpiWakeup});
		нцДо
			reg := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, НИЗКОУР.прочти32битаПоАдресу(viewport))
		кцПри ~(31 в reg) или Kernel.Expired(timer);
		если (31 в reg) и (UsbDebug.Level >= UsbDebug.Errors) то
			ЛогЯдра.пСтроку8("TUSB1210 UsbEhciPhy: could not wakeup PHY");
			ЛогЯдра.пВК_ПС
		всё;
		возврат ~(31 в reg)
	кон Wakeup;

	(** Write to ULPI register *)
	проц Write(viewport, address: адресВПамяти; value: мнвоНаБитахМЗ): булево;
	перем
		timer: Kernel.MilliTimer;
		reg: мнвоНаБитахМЗ;
	нач
		если ~Wakeup(viewport) то возврат ложь всё;
		Kernel.SetTimer(timer, Timeout);
		НИЗКОУР.запиши32битаПоАдресу(viewport, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, value) * UlpiWriteDataMask + НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, логСдвиг(address, UlpiAddressOfs)) * UlpiAddressMask + {UlpiWrite, UlpiRun});
		нцДо
			НИЗКОУР.прочтиОбъектПоАдресу(viewport, reg);
		кцПри ~(30 в reg) или Kernel.Expired(timer);
		если (30 в reg) и (UsbDebug.Level >= UsbDebug.Errors) то
			ЛогЯдра.пСтроку8("TUSB1210 UsbEhcuPhy: could not write to PHY");
			ЛогЯдра.пВК_ПС
		всё;
		возврат ~(30 в reg)
	кон Write;

	(**
	 * Inits the ULPI via the Viewport register of the EHCI controller.
	 * Has to be done when the controller is configured and running.
	 *
	 * 'viewport' is the address of the viewport register. 'reset' is the GPIO
	 * pin to which the full ULPI reset is wired (negative if not available).
	 *)
	проц Init * (viewport: адресВПамяти; reset: цел32): булево;
	перем
		i: цел32;
	нач
		если reset >= 0 то
			Gpio.SetDirection(reset, Gpio.Output);
			Gpio.EnableOutput(reset, истина);
			Gpio.SetData(reset, истина);
			Gpio.SetData(reset, ложь);
			(*! TODO: Wait 2 us *)
			нцДля i := 0 до 1000000 делай кц;
			Gpio.SetData(reset, истина)
		иначе
			ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Skipping GPIO USB reset"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
		всё;

		(* Reset *)
		если viewport # 0 то
			если ~Write(viewport, FuncCtrlSet, {Reset}) то возврат ложь всё;
			если ~Write(viewport, OtgCtrlSet, {DmPullDown, DpPullDown, IdPullUp}) то возврат ложь всё;
			если ~Write(viewport, FuncCtrlSet, {2, SuspendM} + OpmodeNorm + XcvrLS) то возврат ложь всё;
			если ~Write(viewport, FuncCtrlClr, {0 .. 6} - OpmodeNorm  - XcvrLS - {2, SuspendM}) то возврат ложь всё;
			если ~Write(viewport, OtgCtrlSet, {DrvVbus, DrvVbusExt}) то возврат ложь всё;
			если ~Write(viewport, FuncCtrlClr, {2}) то возврат ложь всё
		иначе
			ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Skipping USB Viewport reset"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
		всё;
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("USB PHY Initialized sucessfully"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
		возврат истина
	кон Init;
кон UsbEhciPhy.
