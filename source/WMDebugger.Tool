Visual debuger tool fo A2:

2. Compiler.Compile  -b=AMD
	FoxAMDBackend.Mod
	WMComboBox.Mod
	WMTextView2.Mod
	BtDecoder.Mod
	I386Decoder2.Mod
	BtDTraps.Mod
	BtMenus.Mod
	BtDbgPanel.Mod
	WMDebugger.Mod 
~

2. Unload from memory:
SystemTools.Free FoxAMDBackend~

3. Compile debugger test:

Compiler.Compile TestDbg.Mod ~

4. Open debuger, for example:

WMDebugger.Open WMDebugger/TestDbg.Mod~  
Shortcuts:
 look and customize in file WMDebugger/WMDebugger.XML

5.Unload if need:
SystemTools.Free WMDebugger BtMenus BtDTraps BtDbgPanel I386Decoder2 BtDecoder WMComboBox TestDbg ~

 
Best regards,
Bohdan