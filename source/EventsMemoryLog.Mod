модуль EventsMemoryLog; (** AUTHOR "staubesv"; PURPOSE "Log system events in memory"; *)
(**
 * History:
 *
 *	14.03.2007	First release (staubesv)
 *)

использует
	Modules, Events, EventsUtils;

конст

	(* Logsize = MaxNofWrapper * EventsPerWrapper events *)
	MaxNofWrapper = 16;
	EventsPerWrapper = 256;

тип

	EventLogger = окласс(Events.Sink)
	перем
		events : EventsUtils.EventContainer;

		проц {перекрыта}Handle*(event : Events.Event);
		нач
			если ~events.IsFull() то
				events.Handle(event);
			всё;
		кон Handle;

		проц &Init*;
		нач
			name := "EventLog";
			нов(events, MaxNofWrapper, EventsPerWrapper);
		кон Init;

	кон EventLogger;

перем
	eventLog- : EventLogger;

проц GetEvents*() : EventsUtils.EventContainer;
нач
	возврат eventLog.events;
кон GetEvents;

проц Clear*;
нач
	eventLog.events.Clear;
кон Clear;

проц Cleanup;
нач
	Events.Unregister(eventLog);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	нов(eventLog);
	Events.Register(eventLog);
кон EventsMemoryLog.
