модуль XModem;	(** AUTHOR "be"; PURPOSE "XModem protocol"; *)

(* following the "Modem Protocol Documentation by Ward Christensen, 1/1/1982 *)

использует НИЗКОУР, Kernel, Потоки, ЛогЯдра;

конст
	DebugS = ложь;	(* debug sender *)
	DebugR = ложь;	(* debug receiver *)

	(* result codes *)
	Ok* = 0;
	Timeout* = 1;
	Error* = 2;

	(* states *)
	SInit = 00X;
	SData = 01X;
	SEOT = 02X;
	RInitCRC = 03X;
	RInitChksum = 04X;
	RData = 05X;
	Abort = 06X;
	Exit = 07X;

	(* Timeouts ([ms]) *)
	SenderInitialTimeout = 60000;
	SenderACKTimeout = 60000;
	ReceiverInitTimeout = 3000;
	ReceiverDataTimeout = 1000;
	PurgeTimeout = 1000;

	MaxRetries = 10;
	MaxCRCThreshold = 3;

	HeaderSize = 3;
	BlockSize = 128;
	ChksumSize = 1;
	CRCSize = 2;
	PacketSizeChksum = HeaderSize + BlockSize + ChksumSize;
	PacketSizeCRC = HeaderSize + BlockSize + CRCSize;
	MaxPacketSize = PacketSizeCRC;

	(* symbols *)
	SOH = 01X;
	EOT = 04X;
	ACK = 06X;
	NAK = 15X;

тип
	XModem* = окласс
		перем
			state: симв8;	(* current state *)
			rx: Потоки.Чтец;	(* input channel *)
			tx: Потоки.Писарь;	(* output channel *)
			crc: булево;	(* TRUE iff in CRC mode, FALSE iff in checksum mode *)
			packetSize: цел32;	(* size of a packet, including header & checksum/CRC. Initialized by SendInit/ReceiveInit *)
			retries: цел32;	(* # of retries *)
			blockIndex: цел32;	(* # of block *)
			msg: массив 256 из симв8;	(* error message *)

		проц &InitXModem*(rx: Потоки.Чтец; tx: Потоки.Писарь);
		нач
			сам.rx := rx; сам.tx := tx
		кон InitXModem;

		проц Get(r: Потоки.Чтец; timeout: цел32; перем ch: симв8): цел32;
		перем milliTimer : Kernel.MilliTimer;
		нач
			если (r.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() = 0) то
				Kernel.SetTimer(milliTimer, timeout);
				нцДо
				кцПри (r.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() > 0) или Kernel.Expired(milliTimer);
				если (r.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() = 0) то
					ch := 0X; возврат Timeout
				всё
			всё;
			ch := r.чИДайСимв8();
			возврат Ok
		кон Get;

		проц Peek(r: Потоки.Чтец; timeout: цел32; перем ch: симв8): цел32;
		перем milliTimer : Kernel.MilliTimer;
		нач
			если (r.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() = 0) то
				Kernel.SetTimer(milliTimer, timeout);
				нцДо
				кцПри (r.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() > 0) или Kernel.Expired(milliTimer);
				если (r.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() = 0) то
					ch := 0X; возврат Timeout
				всё
			всё;
			ch := r.ПодглядиСимв8();
			возврат Ok
		кон Peek;

		проц Purge(r: Потоки.Чтец);
		перем ch: симв8;
		нач
			нцДо кцПри Get(r, PurgeTimeout, ch) = Timeout
		кон Purge;

		проц CalcCheckSum(перем buffer: массив из симв8; ofs, len: цел32): симв8;
		перем i, chksum: цел32;
		нач
			chksum := 0;
			нцДля i := 0 до len-1 делай chksum := chksum + кодСимв8(buffer[ofs+i]) кц;
			возврат симв8ИзКода(chksum остОтДеленияНа 100H)
		кон CalcCheckSum;

		проц CalcCRC(перем buffer: массив из симв8; ofs, len: цел32): цел32;
		перем i, k, crc: цел32;
		нач
			crc := 0;
			нцДля i := 0 до len-1 делай
				crc := цел32(мнвоНаБитахМЗ(crc) / мнвоНаБитахМЗ(кодСимв8(buffer[ofs+i])*100H));
				нцДля k := 0 до 7 делай
					если (15 в мнвоНаБитахМЗ(crc)) то
						crc := цел32(мнвоНаБитахМЗ(crc*2) /мнвоНаБитахМЗ(1021H))
					иначе
						crc := crc*2
					всё
				кц
			кц;
			возврат crc остОтДеленияНа 10000H
		кон CalcCRC;

		проц GetErrorMessage*(перем string: массив из симв8);
		нач
			копируйСтрокуДо0(msg, string)
		кон GetErrorMessage;
	кон XModem;

	XMSender* = окласс(XModem)
		перем
			data: Потоки.Чтец;
			buffer: массив MaxPacketSize из симв8;
			resend: булево;

		проц &Init*(data, rx: Потоки.Чтец; tx: Потоки.Писарь);
		нач
			InitXModem(rx, tx); сам.data := data; state := SInit;
			blockIndex := 1; retries := 0; resend := ложь
		кон Init;

		проц GetData(перем buffer: массив из симв8; ofs, len: цел32);
		перем i: цел32;
		нач
			i := 0;
			нцПока (data.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() > 0) и (i < len) делай
				buffer[ofs+i] := data.чИДайСимв8(); увел(i)
			кц;
			нцПока (i < len) делай buffer[ofs+i] := 0X; увел(i) кц
		кон GetData;

		проц SetHeader(перем buf: массив из симв8; index: цел32);
		нач
			утв(index < 100H);
			buf[0] := SOH;
			buf[1] := симв8ИзКода(index);
			buf[2] := симв8ИзКода(255-index)
		кон SetHeader;

		проц SendInit;
		перем res: целМЗ; ch: симв8;
		нач
			если DebugS то
				ЛогЯдра.пСтроку8("SendInit:"); ЛогЯдра.пВК_ПС; ЛогЯдра.пСтроку8("  waiting for 'C'/NAK...")
			всё;
			res := Get(rx, SenderInitialTimeout, ch);

			если (res = Ok) то
				если DebugS то ЛогЯдра.пСтроку8("got "); ЛогЯдра.п16ричное(кодСимв8(ch), -2); ЛогЯдра.пСимв8("X"); ЛогЯдра.пВК_ПС всё;
				если ((ch = NAK) или (ch = "C")) то
					crc := (ch = "C");
					если crc то packetSize := PacketSizeCRC иначе packetSize := PacketSizeChksum всё;
					если DebugS то
						если crc то ЛогЯдра.пСтроку8("  CRC") иначе ЛогЯдра.пСтроку8("  checksum") всё;
						ЛогЯдра.пСтроку8(" mode; new state = SData"); ЛогЯдра.пВК_ПС
					всё;
					state := SData
				иначе
					увел(retries); (* state stays the same (SInit) *)
					копируйСтрокуДо0("SendInit: wrong reply", msg);
					если DebugS то ЛогЯдра.пСтроку8("  retries = "); ЛогЯдра.пЦел64(retries, 0); ЛогЯдра.пВК_ПС всё
				всё
			иначе (* timeout *)
				копируйСтрокуДо0("SendInit timeout", msg);
				если DebugS то ЛогЯдра.пСтроку8("  new state = Abort"); ЛогЯдра.пВК_ПС всё;
				state := Abort
			всё
		кон SendInit;

		проц SendData;
		перем c, res: цел32; ch: симв8;
		нач
			если DebugS то ЛогЯдра.пСтроку8("SendData:"); ЛогЯдра.пВК_ПС всё;
			(* send block *)
			если ~resend то
				SetHeader(buffer, blockIndex остОтДеленияНа 100H);
				GetData(buffer, HeaderSize, BlockSize);
				если crc то
					c := CalcCRC(buffer, HeaderSize, BlockSize);
					buffer[HeaderSize+BlockSize] := симв8ИзКода(c DIV 100H);
					buffer[HeaderSize+BlockSize+1] := симв8ИзКода(c остОтДеленияНа 100H);
				иначе
					buffer[HeaderSize+BlockSize] := CalcCheckSum(buffer, HeaderSize, BlockSize)
				всё
			иначе resend := ложь
			всё;
			если DebugS то ЛогЯдра.пСтроку8("  sending "); ЛогЯдра.пЦел64(packetSize, 0); ЛогЯдра.пСтроку8(" bytes...") всё;
			tx.пБайты(buffer, 0, packetSize); tx.ПротолкниБуферВПоток;
			нцПока (rx.кодВозвратаПоследнейОперации = 0) и (rx.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() > 0) делай ch := rx.чИДайСимв8() кц;	(* toss any characters immediately upon completing sending a block *)
			если DebugS то ЛогЯдра.пСтроку8("done."); ЛогЯдра.пВК_ПС; ЛогЯдра.пСтроку8("  waiting for ACK...") всё;

			(* wait for ACK *)
			res := Get(rx, SenderACKTimeout, ch);
			если (res = Ok) то
				если (ch = ACK) то
					если DebugS то ЛогЯдра.пСтроку8("ok."); ЛогЯдра.пВК_ПС всё;
					увел(blockIndex);
					если (data.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() = 0) то state := SEOT всё;	(* ELSE state stays the same (SData) *)
					retries := 0	(* temp...*)
				иначе
					если DebugS то ЛогЯдра.пСтроку8("no (got "); ЛогЯдра.п16ричное(кодСимв8(ch),-2); ЛогЯдра.пСтроку8("X)"); ЛогЯдра.пВК_ПС всё;
					копируйСтрокуДо0("SendData: no ACK", msg);
					увел(retries);	(* state stays the same (SData) *)
					resend := истина	(* send same data again *)
				всё
			иначе (* timeout *)
				копируйСтрокуДо0("SendData: timeout", msg);
				если DebugS то ЛогЯдра.пСтроку8("  timeout; res = "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС всё;
				state := Abort
			всё
		кон SendData;

		проц SendEOT;
		перем res: целМЗ; ch: симв8;
		нач
			(* send EOT *)
			tx.пСимв8(EOT); tx.ПротолкниБуферВПоток;

			(* wait for ACK *)
			res := Get(rx, SenderACKTimeout, ch);
			если (res = Ok) то
				если (ch = ACK) то state := Exit
				иначе
					копируйСтрокуДо0("SendEOT: no ACK", msg);
					увел(retries) (* state stays the same (SEOT) *)
				всё
			иначе (* timeout *)
				копируйСтрокуДо0("SendEOT: timeout", msg);
				state := Abort
			всё
		кон SendEOT;

		(* completely receiver-driven *)
		проц Send*(): цел32;
		нач
			нцПока (state # Abort) и (state # Exit) и (retries < MaxRetries) делай
				просей state из
				| SInit: SendInit
				| SData: SendData
				| SEOT: SendEOT
				всё
			кц;
			если (state = Exit) то возврат Ok
			иначе возврат Error
			всё
		кон Send;
	кон XMSender;

	XMReceiver* = окласс(XModem)
		перем
			data: Потоки.Писарь;
			crcThreshold: цел32;

		проц &Init*(data: Потоки.Писарь; rx: Потоки.Чтец; tx: Потоки.Писарь);
		нач
			InitXModem(rx, tx); сам.data := data; state := RInitCRC;
			blockIndex := 1; retries := 0; crcThreshold := 0
		кон Init;

		проц PutData(перем buffer: массив из симв8; ofs, len: цел32);
		нач
			data.пБайты(buffer, ofs, len)
		кон PutData;

		проц GetHeader(перем buf: массив из симв8): цел32;
		перем index: цел32;
		нач
			если (buf[0] = SOH) то
				index := кодСимв8(buf[1]);
				если (index # 255-кодСимв8(buf[2])) то index := -1 всё
			иначе
				index := -1
			всё;
			возврат index
		кон GetHeader;

		проц ReceiveInitCRC;
		перем res: целМЗ; ch: симв8;
		нач
			если DebugR то
				ЛогЯдра.пСтроку8("ReceiveInitCRC:"); ЛогЯдра.пВК_ПС; ЛогЯдра.пСтроку8("  sending 'C'..."); ЛогЯдра.пВК_ПС
			всё;
			packetSize := PacketSizeCRC;
			(* send "C" *)
			crc := истина;
			tx.пСимв8("C"); tx.ПротолкниБуферВПоток;

			(* wait for ACK *)
			если DebugR то ЛогЯдра.пСтроку8("  waiting for reply...") всё;
			res := Peek(rx, ReceiverInitTimeout, ch);
			если (res = Ok) то
				если DebugR то ЛогЯдра.пСтроку8("got "); ЛогЯдра.п16ричное(кодСимв8(ch), -2); ЛогЯдра.пСимв8("X"); ЛогЯдра.пВК_ПС всё;
				просей ch из
				| SOH: если DebugR то ЛогЯдра.пСтроку8("  new state = RData"); ЛогЯдра.пВК_ПС всё; state := RData
				| EOT: если DebugR то ЛогЯдра.пСтроку8("  new state = Abort"); ЛогЯдра.пВК_ПС всё;
					копируйСтрокуДо0("ReceiveInitCRC: got EOT", msg); state := Abort
				иначе
					увел(retries); (* state stays the same (RInitCRC) *)
					копируйСтрокуДо0("ReceiveInitCRC: wrong reply", msg);
					если DebugR то ЛогЯдра.пСтроку8("  retries = "); ЛогЯдра.пЦел64(retries, 0); ЛогЯдра.пВК_ПС всё
				всё
			иначе (* timeout *)
				увел(crcThreshold);
				если DebugR то ЛогЯдра.пСтроку8("  timeout, CRC-threshold = "); ЛогЯдра.пЦел64(crcThreshold, 0); ЛогЯдра.пВК_ПС всё;
				если (crcThreshold = MaxCRCThreshold) то
					если DebugR то ЛогЯдра.пСтроку8("  switching to checksum-mode"); ЛогЯдра.пВК_ПС всё;
					crc := ложь; retries := 0; state := RInitChksum
				всё (* ELSE state stays the same (RInitCRC) *)
			всё
		кон ReceiveInitCRC;

		проц ReceiveInitChecksum;
		перем res: целМЗ; ch: симв8;
		нач
			packetSize := PacketSizeChksum;
			(* send NAK *)
			tx.пСимв8(NAK); tx.ПротолкниБуферВПоток;

			(* wait for transmission to begin *)
			res := Peek(rx, ReceiverInitTimeout, ch);
			если (res = Ok) то
				state := RData
			иначе (* timeout *)
				копируйСтрокуДо0("ReceiveInitChecksum: timeout", msg);
				увел(retries)	(* state stays the same (RInitChksum) *)
			всё
		кон ReceiveInitChecksum;

		проц ReceiveData;
		перем buffer: укль на массив из симв8; ch: симв8; res, i, idx, c, cc: цел32; ok: булево;
		нач
			нов(buffer,packetSize);
			если DebugR то
				ЛогЯдра.пСтроку8("ReceiveData:"); ЛогЯдра.пВК_ПС; ЛогЯдра.пСтроку8("  waiting for first byte...")
			всё;
			(* get first byte (SOH/EOT) *)
			res := Peek(rx, ReceiverDataTimeout, ch);

			если (res = 0) и ((ch = SOH) или (ch = EOT)) то
				если DebugR то ЛогЯдра.пСтроку8("got "); ЛогЯдра.п16ричное(кодСимв8(ch), -2); ЛогЯдра.пСимв8("X"); ЛогЯдра.пВК_ПС всё;
				если (ch = SOH) то
					если DebugR то ЛогЯдра.пСтроку8("  receiving "); ЛогЯдра.пЦел64(packetSize, 0); ЛогЯдра.пСтроку8(" bytes...") всё;
					(* receive packetSize bytes *)
					i := 0; res := 0;
					нцПока (i < packetSize) и (res = 0) делай
						res := Get(rx, ReceiverDataTimeout, buffer[i]);
						увел(i)
					кц;
					если DebugR то ЛогЯдра.пСтроку8("done (got "); ЛогЯдра.пЦел64(i, 0); ЛогЯдра.пСтроку8(" bytes)"); ЛогЯдра.пВК_ПС всё;

					если (res = 0) то
						idx := GetHeader(buffer^);
						если (idx = blockIndex остОтДеленияНа 100H) то	(* correct block number *)
							(* check checksum/CRC *)
							если crc то
								c := устарПреобразуйКБолееШирокомуЦел(кодСимв8(buffer[HeaderSize+BlockSize]))*100H+кодСимв8(buffer[HeaderSize+BlockSize+1]);
								cc := CalcCRC(buffer^, HeaderSize, BlockSize);
								если DebugR и (c # cc) то
									ЛогЯдра.пСтроку8("  wrong checksum: "); ЛогЯдра.п16ричное(cc, 8); ЛогЯдра.пСтроку8(", expected "); ЛогЯдра.п16ричное(c, 8); ЛогЯдра.пВК_ПС
								всё;
								ok := c = c
							иначе
								ok := CalcCheckSum(buffer^, HeaderSize, BlockSize) = buffer[HeaderSize+BlockSize]
							всё;

							если ok то
								если DebugR то
									ЛогЯдра.пСтроку8("  received block "); ЛогЯдра.пЦел64(blockIndex, 0); ЛогЯдра.пВК_ПС
								всё;
								PutData(buffer^, HeaderSize, BlockSize);
								увел(blockIndex);
								tx.пСимв8(ACK); tx.ПротолкниБуферВПоток
							иначе
								копируйСтрокуДо0("ReceiveData: checksum error", msg);
								если DebugR то ЛогЯдра.пСтроку8("  checksum error"); ЛогЯдра.пВК_ПС всё;
								увел(retries);
								tx.пСимв8(NAK); tx.ПротолкниБуферВПоток
							всё
						аесли (idx = (blockIndex-1) остОтДеленияНа 100H) то	(* maybe the sender lost our ACK *)
							копируйСтрокуДо0("ReceiveData: got block n-1", msg);
							если DebugR то ЛогЯдра.пСтроку8("  got block n-1"); ЛогЯдра.пВК_ПС всё;
							увел(retries); tx.пСимв8(ACK); tx.ПротолкниБуферВПоток
						иначе
							копируйСтрокуДо0("ReceiveData: wrong block number", msg);
							state := Abort;
							если DebugR то
								ЛогЯдра.пСтроку8("  wrong block number"); ЛогЯдра.пЦел64(idx, 5); ЛогЯдра.пСтроку8(", expected ");
								ЛогЯдра.пЦел64(blockIndex, 0); ЛогЯдра.пВК_ПС
							всё
						всё
					иначе
						копируйСтрокуДо0("ReceiveData: timeout while receiving block", msg);
						state := Abort
					всё
				иначе (* ch = EOT *)
					если (blockIndex = 1) то
						копируйСтрокуДо0("ReceiveData: got EOT instead of first block", msg);
						state := Abort
					иначе
						tx.пСимв8(ACK); tx.ПротолкниБуферВПоток; state := Exit
					всё
				всё
			иначе (* timeout/wrong character *)
				копируйСтрокуДо0("ReceiveData: timeout/wrong packet", msg);
				если DebugR то
					ЛогЯдра.пСтроку8("timeout/wrong packet; res = "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пСтроку8("; ch = ");
						ЛогЯдра.п16ричное(кодСимв8(ch), -2); ЛогЯдра.пВК_ПС
				всё;
				увел(retries);
				Purge(rx);
				tx.пСимв8(NAK); tx.ПротолкниБуферВПоток
			всё
		кон ReceiveData;

		проц Receive*(): цел32;
		нач
			Purge(rx);
			нцПока (state # Abort) и (state # Exit) и (retries < MaxRetries) делай
				просей state из
				| RInitCRC: ReceiveInitCRC
				| RInitChksum: ReceiveInitChecksum
				| RData: ReceiveData
				всё
			кц;
			data.ПротолкниБуферВПоток;
			если (state = Exit) то возврат Ok
			иначе возврат Error
			всё
		кон Receive;
	кон XMReceiver;

кон XModem.

ystem.Free TestXModem XModem.Mod ~
