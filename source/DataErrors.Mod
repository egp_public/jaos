(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль DataErrors;   (** AUTHOR "adf, fof"; PURPOSE "For reporting runtime warnings & errors into file Error.Log"; *)

(**  Error.Log is an error log file that can be viewed with any ASCII editor, and is overwritten with each session.
	If no errors or warnings were logged, the file will be empty.  File Error.Log is automatically opened/closed whenever
	module DataErrors.Mod is loaded-into/freed-from the system.  The Error.Log file from the previous session is saved in
	file Error.Log.Bak.

	Logging of the first error message of a session produces an audible SOS beep to inform the user to check this file.
	Logging of the first warning message of a session produces just 3 beeps (instead of 9) in the SOS tone sequence.

	Errors are for catostropic events, e.g., division by zero.
	Warnings are for non-optimal events, e.g., a series did not converge. *)

использует НИЗКОУР, ЭВМ, Kernel, Modules, Files, Beep, NbrInt, NbrRat, NbrRe, NbrCplx;

перем
	beepedError, beepedWarning: булево;  F: Files.File;  W: Files.Writer;


	(***! BEGIN part from Traps, todo: simplify and adjust to needs *)
тип
	Variable = запись
		adr, type, size, n, tdadr: цел32
	кон;   (* variable descriptor *)
конст
	MaxString = 64;  MaxArray = 8;  MaxCols = 70;  Sep = "  ";  SepLen = 2;

	(* Write the specified procedure name and returns parameters for use with NextVar and Variables. *)
	(* Find a procedure in the reference block.  Return index of name, or -1 if not found. *)
	проц FindProc( refs: Modules.Bytes;  modpc: цел32 ): цел32;
	перем i, m, t, proc: цел32;  ch: симв8;
	нач
		proc := -1;  i := 0;  m := длинаМассива( refs^ )(цел32);  ch := refs[i];  увел( i );
		нцПока (i < m) и ((ch = 0F8X) или (ch = 0F9X)) делай  (* proc *)
			GetNum( refs, i, t );   (* pofs *)
			если t > modpc то  (* previous procedure was the one *)
				ch := 0X (* stop search *)
			иначе  (* ~found *)
				если ch = 0F9X то
					GetNum( refs, i, t );   (* nofPars *)
					увел( i, 3 ) (* RetType, procLev, slFlag *)
				всё;
				proc := i;   (* remember this position, just before the name *)
				нцДо ch := refs[i];  увел( i ) кцПри ch = 0X;   (* pname *)
				если i < m то
					ch := refs[i];  увел( i );   (* 1X | 3X | 0F8X | 0F9X *)
					нцПока (i < m) и (ch >= 1X) и (ch <= 3X) делай  (* var *)
						ch := refs[i];  увел( i );   (* type *)
						если (ch >= 81X) или (ch = 16X) или (ch = 1DX) то
							GetNum( refs, i, t ) (* dim/tdadr *)
						всё;
						GetNum( refs, i, t );   (* vofs *)
						нцДо ch := refs[i];  увел( i ) кцПри ch = 0X;   (* vname *)
						если i < m то ch := refs[i];  увел( i ) всё  (* 1X | 3X | 0F8X | 0F9X *)
					кц
				всё
			всё
		кц;
		если (proc = -1) и (i # 0) то proc := i всё;   (* first procedure *)
		возврат proc
	кон FindProc;

	проц WriteProc0( mod: Modules.Module;  pc, fp: цел32;  перем refs: Modules.Bytes;  перем refpos, base: цел32 );
	перем ch: симв8;
	нач
		refpos := -1;
		если mod = НУЛЬ то
			если pc = 0 то W.пСтроку8( "NIL" ) иначе W.пСтроку8( "Unknown PC=" );  W.п16ричное( pc, 8 );  W.пСимв8( "H" ) всё;
			если fp # -1 то W.пСтроку8( " FP=" );  W.п16ричное( fp, 8 );  W.пСимв8( "H" ) всё
		иначе
			W.пСтроку8( mod.name );  умень( pc, цел32(адресОт( mod.code[0] )) );  refs := mod.refs;
			если (refs # НУЛЬ ) и (длинаМассива( refs ) # 0) то
				refpos := FindProc( refs, pc );
				если refpos # -1 то
					W.пСимв8( "." );  ch := refs[refpos];  увел( refpos );
					если ch = "$" то base := цел32(mod.sb) иначе base := fp всё;   (* for variables *)
					нцПока ch # 0X делай W.пСимв8( ch );  ch := refs[refpos];  увел( refpos ) кц
				всё
			всё;
			W.пСтроку8( " pc=" );  W.пЦел64( pc, 1 )
		всё
	кон WriteProc0;

(* Find procedure name and write it. *)
	проц WriteProc( pc: цел32 );
	перем refs: Modules.Bytes;  refpos, base: цел32;
	нач
		WriteProc0( Modules.ThisModuleByAdr( pc ), pc, -1, refs, refpos, base )
	кон WriteProc;
(* Write a simple variable value. *)

	проц WriteSimpleVar( adr, type, tdadr: цел32;  перем col: цел32 );
	перем ch: симв8;  sval: цел8;  ival: цел16;  lval: цел32;
	нач
		просей type из
		1, 3:  (* BYTE, CHAR *)
				НИЗКОУР.прочтиОбъектПоАдресу( adr, ch );
				если (ch > " ") и (ch <= "~") то W.пСимв8( ch );  увел( col ) иначе W.п16ричное( кодСимв8( ch ), -2 );  W.пСимв8( "X" );  увел( col, 3 ) всё
		| 2:    (* BOOLEAN *)
				НИЗКОУР.прочтиОбъектПоАдресу( adr, ch );
				если ch = 0X то W.пСтроку8( "FALSE" )
				аесли ch = 1X то W.пСтроку8( "TRUE" )
				иначе W.пЦел64( кодСимв8( ch ), 1 )
				всё;
				увел( col, 5 )
		| 4:    (* SIGNED8 *)
				НИЗКОУР.прочтиОбъектПоАдресу( adr, sval );
				W.пЦел64( sval, 1 );  увел( col, 4 )
				(*Streams.WriteString(w, " ("); Streams.WriteHex(w, sval, -3); Streams.WriteString(w, "H)")*)
		| 5:    (* SIGNED16 *)
				НИЗКОУР.прочтиОбъектПоАдресу( adr, ival );
				W.пЦел64( ival, 1 );  увел( col, 5 )
				(*Streams.WriteString(w, " ("); Streams.WriteHex(w, ival, 8); Streams.WriteString(w, "H)")*)
		| 6:    (* SIGNED32 *)
				НИЗКОУР.прочтиОбъектПоАдресу( adr, lval );
				W.пЦел64( lval, 1 );  увел( col, 5 );
				если матМодуль( lval ) >= 10000H то W.пСтроку8( " (" );  W.п16ричное( lval, 8 );  W.пСтроку8( "H)" );  увел( col, 12 ) всё
		| 7, 8, 13, 16, 29:  (* FLOAT32, FLOAT64, POINTER *)
				увел( col, 9 );
				если (type = 8) или (type = 16) то НИЗКОУР.прочтиОбъектПоАдресу( adr + 4, lval );  W.п16ричное( lval, 8 );  увел( col, 8 ) всё;
				НИЗКОУР.прочтиОбъектПоАдресу( adr, lval );  W.п16ричное( lval, 8 );  W.пСимв8( "H" )
		| 9:    (* SET *)
				НИЗКОУР.прочтиОбъектПоАдресу( adr, lval );
				W.пМнвоНаБитахМЗ( мнвоНаБитахМЗ(lval ) );  увел( col, 8 ) (* col is guess *)
		| 22:  (* RECORD *)
				W.пСтроку8( "Rec." );  W.п16ричное( tdadr, 8 );  W.пСимв8( "H" );  увел( col, 13 )
		| 14:  (* PROC *)
				НИЗКОУР.прочтиОбъектПоАдресу( adr, lval );  WriteProc( lval );  увел( col, 25 )
		всё
	кон WriteSimpleVar;

(* Write a variable value.  The v parameter is a variable descriptor obtained with NextVar.  Parameter col is incremented with

		the (approximate) number of characters written. *)
	проц WriteVar( v: Variable;  перем col: цел32 );
	перем ch: симв8;
	нач
		если v.type = 15 то
			W.пСимв8( 22X );
			нц
				если v.n = 0 то прервиЦикл всё;
				НИЗКОУР.прочтиОбъектПоАдресу( v.adr, ch );  увел( v.adr );
				если (ch < " ") или (ch > "~") то прервиЦикл всё;
				W.пСимв8( ch );  увел( col );  умень( v.n )
			кц;
			W.пСимв8( 22X );  увел( col, 2 );
			если ch # 0X то W.пСимв8( "!" ) всё
		иначе
			нцПока v.n > 0 делай
				WriteSimpleVar( v.adr, v.type, v.tdadr, col );  умень( v.n );  увел( v.adr, v.size );
				если v.n > 0 то W.пСтроку8( ", " );  увел( col, 2 ) всё
			кц
		всё
	кон WriteVar;

(* Get a compressed refblk number. *)

	проц GetNum( refs: Modules.Bytes;  перем i, num: цел32 );
	перем n, s: цел32;  x: симв8;
	нач
		s := 0;  n := 0;  x := refs[i];  увел( i );
		нцПока кодСимв8( x ) >= 128 делай увел( n, арифмСдвиг( кодСимв8( x ) - 128, s ) );  увел( s, 7 );  x := refs[i];  увел( i ) кц;
		num := n + арифмСдвиг( кодСимв8( x ) остОтДеленияНа 64 - кодСимв8( x ) DIV 64 * 64, s )
	кон GetNum;

(* Step to the next variable in the refs block.  The name parameter returns empty if no more variables are found.  The attributes

		are returned in v.  Parameter refpos is modified.  *)
	проц NextVar( refs: Modules.Bytes;  перем refpos: цел32;  base: цел32;  перем name: массив из симв8;  перем v: Variable );
	перем x: Variable;  j: цел32;  ch, mode: симв8;
	нач
		name[0] := 0X;   (* empty name signals end or error *)
		если refpos < длинаМассива( refs^ ) - 1 то
			mode := refs[refpos];  увел( refpos );
			если (mode >= 1X) и (mode <= 3X) то  (* var *)
				x.type := кодСимв8( refs[refpos] );  увел( refpos );
				если x.type > 80H то
					если x.type = 83H то x.type := 15 иначе умень( x.type, 80H ) всё;
					GetNum( refs, refpos, x.n )
				аесли (x.type = 16H) или (x.type = 1DH) то GetNum( refs, refpos, x.tdadr );  x.n := 1
				иначе
					если x.type = 15 то x.n := MaxString (* best guess *) иначе x.n := 1 всё
				всё;
				(* get address *)
				GetNum( refs, refpos, x.adr );
				увел( x.adr, base );   (* convert to absolute address *)
				если x.n = 0 то  (* open array (only on stack, not global variable) *)
					НИЗКОУР.прочтиОбъектПоАдресу( x.adr + 4, x.n ) (* real LEN from stack *)
				всё;
				если mode # 1X то НИЗКОУР.прочтиОбъектПоАдресу( x.adr, x.adr ) всё;   (* indirect *)
				(* get size *)
				просей x.type из
				1..4, 15:
						x.size := 1
				| 5:   x.size := 2
				| 6..7, 9, 13, 14, 29:
						x.size := 4
				| 8, 16:
						x.size := 8
				| 22:
						x.size := 0;
						утв ( x.n <= 1 )
				иначе x.size := -1
				всё;
				если x.size >= 0 то  (* ok, get name *)
					ch := refs[refpos];  увел( refpos );  j := 0;
					нцПока ch # 0X делай
						если j < длинаМассива( name ) - 1 то name[j] := ch;  увел( j ) всё;   (* truncate long names *)
						ch := refs[refpos];  увел( refpos )
					кц;
					name[j] := 0X;  v := x (* non-empty name *)
				всё
			всё
		всё
	кон NextVar;

(* Find the specified global variable and return its descriptor.  Returns TRUE iff the variable is found. *)
	проц Variables( refs: Modules.Bytes;  refpos, base: цел32 );
	перем v: Variable;  j, col: цел32;
		name: массив 64 из симв8;
		etc: булево;
	нач
		нц
			NextVar( refs, refpos, base, name, v );
			если name[0] = 0X то прервиЦикл всё;
			(* write name *)
			если (col # 0) и (v.n > 1) и (v.type # 15) то  (* Ln before array (except string) *)
				W.пВК_ПС();  col := 0
			всё;
			W.пСтроку8( Sep );  W.пСтроку8( name );  W.пСимв8( "=" );  j := 0;
			нцПока name[j] # 0X делай увел( j ) кц;
			увел( col, SepLen + 1 + j );
			(* write variable *)
			если (v.adr >= -4) и (v.adr < 4096) то  (* must be NIL VAR parameter *)
				W.пСтроку8( "NIL (" );  W.п16ричное( v.adr, 8 );  W.пСимв8( ")" );  увел( col, 14 )
			иначе
				etc := ложь;
				если v.type = 15 то
					если v.n > MaxString то etc := истина;  v.n := MaxString всё
				иначе
					если v.n > MaxArray то etc := истина;  v.n := MaxArray всё
				всё;
				WriteVar( v, col );   (* write value *)
				если etc то W.пСтроку8( "..." );  увел( col, 3 ) всё
			всё;
			если col > MaxCols то W.пВК_ПС();  col := 0 всё
		кц;
		если col # 0 то W.пВК_ПС() всё
	кон Variables;

	проц InitVar( mod: Modules.Module;  перем refs: Modules.Bytes;  перем refpos, base: цел32 );
	перем ch: симв8;
	нач
		refpos := -1;
		если mod # НУЛЬ то
			refs := mod.refs;  base := цел32(mod.sb);
			если (refs # НУЛЬ ) и (длинаМассива( refs ) # 0) то
				refpos := FindProc( refs, 0 );
				если refpos # -1 то
					ch := refs[refpos];  увел( refpos );
					нцПока ch # 0X делай ch := refs[refpos];  увел( refpos ) кц
				всё
			всё
		всё
	кон InitVar;

	проц ModuleState( mod: Modules.Module );
	перем refpos, base: цел32;  refs: Modules.Bytes;
	нач
		InitVar( mod, refs, refpos, base );
		если refpos # -1 то W.пСтроку8( "State " );  W.пСтроку8( mod.name );  W.пСимв8( ":" );  W.пВК_ПС();  Variables( refs, refpos, base ) всё
	кон ModuleState;

(* Display call trackback. *)

	проц StackTraceBack( eip, ebp: цел32;  long: булево );
	перем count, refpos, base: цел32;  m: Modules.Module;  refs: Modules.Bytes;
	конст MaxFrames = 16;
	нач
		count := 0;   (* frame count *)
		нцДо
			m := Modules.ThisModuleByAdr( eip );
			если (m # НУЛЬ ) или (count = 0) то
				WriteProc0( m, eip, ebp, refs, refpos, base );  W.пВК_ПС();
				если long и ((count > 0)) то  (* show variables *)
					если refpos # -1 то Variables( refs, refpos, base ) всё;
					если (m # НУЛЬ ) и (base # m.sb) и (count = 0) то ModuleState( m ) всё
				всё;
				НИЗКОУР.прочтиОбъектПоАдресу( ebp + 4, eip );   (* return addr from stack *)
				НИЗКОУР.прочтиОбъектПоАдресу( ebp, ebp );   (* follow dynamic link *)
				увел( count )
			иначе ebp := 0
			всё
		кцПри (ebp = 0) или (count = MaxFrames);
		если ebp # 0 то W.пСтроку8( "..." ) всё
	кон StackTraceBack;
	(***! END part from Traps *)

(* From antsPortability *)
	проц ErrorCaller( перем m: Modules.Module;  перем pc, ebp, eip: NbrInt.Integer );
	перем i, reg: NbrInt.Integer;  timer: Kernel.Timer;
	нач
		reg := цел32(НИЗКОУР.GetFramePointer ());
		НИЗКОУР.прочтиОбъектПоАдресу( reg, ebp );   (* stack frame of caller *)
		НИЗКОУР.прочтиОбъектПоАдресу( ebp + 4, eip );   (* return address from caller *)
		m := Modules.ThisModuleByAdr( eip );
		если m # НУЛЬ то pc := цел32(eip - адресОт( m.code[0] )) иначе pc := матМаксимум( цел32 ) всё;
		если ~beepedError то
			beepedError := истина;  нов( timer );
			нцДля i := 1 до 3 делай Beep.Beep( 125 );  timer.Sleep( 100 );  Beep.Beep( 0 );  timer.Sleep( 100 ) кц;
			нцДля i := 1 до 3 делай Beep.Beep( 100 );  timer.Sleep( 350 );  Beep.Beep( 0 );  timer.Sleep( 150 ) кц;
			нцДля i := 1 до 3 делай Beep.Beep( 125 );  timer.Sleep( 100 );  Beep.Beep( 0 );  timer.Sleep( 100 ) кц
		всё
	кон ErrorCaller;

	проц WarningCaller( перем m: Modules.Module;  перем pc, ebp, eip: NbrInt.Integer );
	перем reg: NbrInt.Integer;  timer: Kernel.Timer;
	нач
		reg := цел32(НИЗКОУР.GetFramePointer ());
		НИЗКОУР.прочтиОбъектПоАдресу( reg, ebp );   (* stack frame of caller *)
		НИЗКОУР.прочтиОбъектПоАдресу( ebp + 4, eip );   (* return address from caller *)
		m := Modules.ThisModuleByAdr( eip );
		если m # НУЛЬ то pc := цел32(eip - адресОт( m.code[0] )) иначе pc := матМаксимум( цел32 ) всё;
		если ~beepedWarning то
			beepedWarning := истина;  нов( timer );  Beep.Beep( 125 );  timer.Sleep( 100 );  Beep.Beep( 0 );
			timer.Sleep( 100 );  Beep.Beep( 100 );  timer.Sleep( 100 );  Beep.Beep( 0 );  timer.Sleep( 100 );
			Beep.Beep( 125 );  timer.Sleep( 100 );  Beep.Beep( 0 );  timer.Sleep( 100 )
		всё
	кон WarningCaller;

	проц IdentifyProcedure( перем m: Modules.Module;  pc: NbrInt.Integer;  перем module, type, proc: массив из симв8 );
	перем i: NbrInt.Integer;  ch: симв8;  refs: Modules.Bytes;  refpos: цел32;
	нач
		module[0] := 0X;  type[0] := 0X;  proc[0] := 0X;
		если m = НУЛЬ то
			если pc = 0 то копируйСтрокуДо0( "NIL", proc ) иначе копируйСтрокуДо0( "unknown pointer", proc ) всё
		иначе
			копируйСтрокуДо0( m.name, module );  refs := m.refs;  refpos := FindProc( refs, pc );
			если refpos # -1 то
				ch := refs[refpos];  NbrInt.Inc( refpos );  i := 0;
				нцПока (ch # 0X) делай
					proc[i] := ch;  ch := refs[refpos];  NbrInt.Inc( refpos );  NbrInt.Inc( i );
					если ch = "." то ch := refs[refpos];  NbrInt.Inc( refpos );  proc[i] := 0X;  копируйСтрокуДо0( proc, type );  i := 0 всё
				кц;
				proc[i] := 0X
			иначе копируйСтрокуДо0( "unknown", proc )
			всё
		всё
	кон IdentifyProcedure;

	проц Location( module, type, proc: массив из симв8 );
	нач
		если module[0] # 0X то W.пСтроку8( "   module: " );  W.пСтроку8( module );  W.пВК_ПС всё;
		если type[0] # 0X то
			W.пСтроку8( "      type: " );  W.пСтроку8( type );  W.пВК_ПС;
			если proc[0] # 0X то W.пСтроку8( "         method: " );  W.пСтроку8( proc );  W.пВК_ПС всё
		иначе
			если proc[0] # 0X то W.пСтроку8( "      procedure: " );  W.пСтроку8( proc );  W.пВК_ПС всё
		всё
	кон Location;

	проц DetailedErrorReport( перем m: Modules.Module;  pc: цел32;  eip, ebp: цел32 );
перем refs: Modules.Bytes; refpos: цел32; adr: цел32;
		нач
		если m # НУЛЬ то
	 refs := m.refs;  refpos := FindProc( refs, pc );
					GetNum( refs, refpos, adr );
				увел( adr, цел32(m.sb));   (* convert to absolute address *)
	W.п16ричное(adr,8);W.пВК_ПС;
		W.пСтроку8( "Detailed Error Report" );  W.пВК_ПС;
		если m# НУЛЬ то W.пСтроку8( "Module State:" );  W.пВК_ПС;  ModuleState( m );  всё;
		W.пСтроку8( "Stack trace back" );  W.пВК_ПС;
		если (eip # 0) и (ebp # 0) то StackTraceBack( eip, ebp, истина );  всё;
		всё;
	кон DetailedErrorReport;


(** Log an error message to file Error.Log. *)
	проц Error*( message: массив из симв8 );
	перем m: Modules.Module;  pc: NbrInt.Integer;
		module, type, proc: массив 64 из симв8;
		ebp, eip: цел32;
	нач {единолично}
		если W # НУЛЬ то
			ErrorCaller( m, pc, ebp, eip );  IdentifyProcedure( m, pc, module, type, proc );  W.пСтроку8( "An error of:" );  W.пВК_ПС;  W.пСтроку8( "   " );
			W.пСтроку8( message );  W.пВК_ПС;  W.пСтроку8( "occurred in:" );  W.пВК_ПС;  Location( module, type, proc );  W.пВК_ПС;
			DetailedErrorReport(m,pc,eip,ebp);
			W.ПротолкниБуферВПоток
		всё
	кон Error;

(** Log an error message to file Error.Log when an error arises from a passed parameter whose value was int. *)
	проц IntError*( int: NbrInt.Integer;  message: массив из симв8 );
	перем m: Modules.Module;  pc: NbrInt.Integer;
		module, type, proc, string: массив 64 из симв8;
		ebp, eip: цел32;
	нач {единолично}
		если W # НУЛЬ то
			ErrorCaller( m, pc, ebp, eip );  IdentifyProcedure( m, pc, module, type, proc );  W.пСтроку8( "An ERROR occurred in:" );  W.пВК_ПС;
			Location( module, type, proc );  W.пСтроку8( "The argument passed that caused this error was: " );  W.пВК_ПС;  W.пСтроку8( "   " );  NbrInt.IntToString( int, string );
			W.пСтроку8( string );  W.пВК_ПС;  W.пСтроку8( "resulting in the following error message:" );  W.пВК_ПС;  W.пСтроку8( "   " );  W.пСтроку8( message );  W.пВК_ПС;  W.пВК_ПС;
			DetailedErrorReport(m,pc,eip,ebp);
			W.ПротолкниБуферВПоток
		всё
	кон IntError;

(** Log an error message to file Error.Log when an error arises from a passed parameter whose value was rat. *)
	проц RatError*( rat: NbrRat.Rational;  message: массив из симв8 );
	перем m: Modules.Module;  pc: NbrInt.Integer;
		module, type, proc, string: массив 64 из симв8;
		ebp, eip: цел32;
	нач {единолично}
		если W # НУЛЬ то
			ErrorCaller( m, pc, ebp, eip );  IdentifyProcedure( m, pc, module, type, proc );  W.пСтроку8( "An ERROR occurred in:" );  W.пВК_ПС;
			Location( module, type, proc );  W.пСтроку8( "The argument passed that caused this error was: " );  W.пВК_ПС;  W.пСтроку8( "   " );  NbrRat.RatToString( rat, string );
			W.пСтроку8( string );  W.пВК_ПС;  W.пСтроку8( "resulting in the following error message:" );  W.пВК_ПС;  W.пСтроку8( "   " );  W.пСтроку8( message );  W.пВК_ПС;  W.пВК_ПС;
						DetailedErrorReport(m,pc,eip,ebp);
			W.ПротолкниБуферВПоток
		всё
	кон RatError;

(** Log an error message to file Error.Log when an error arises from a passed parameter whose value was re. *)
	проц ReError*( re: NbrRe.Real;  message: массив из симв8 );
	перем m: Modules.Module;  pc: NbrInt.Integer;
		module, type, proc, string: массив 64 из симв8;
		ebp, eip: цел32;
	нач {единолично}
		если W # НУЛЬ то
			ErrorCaller( m, pc, ebp, eip );  IdentifyProcedure( m, pc, module, type, proc );  W.пСтроку8( "An ERROR occurred in:" );  W.пВК_ПС;
			Location( module, type, proc );  W.пСтроку8( "The argument passed that caused this error was: " );  W.пВК_ПС;  W.пСтроку8( "   " );  NbrRe.ReToString( re, 15, string );
			W.пСтроку8( string );  W.пВК_ПС;  W.пСтроку8( "resulting in the following error message:" );  W.пВК_ПС;  W.пСтроку8( "   " );  W.пСтроку8( message );  W.пВК_ПС;  W.пВК_ПС;
						DetailedErrorReport(m,pc,eip,ebp);
			W.ПротолкниБуферВПоток
		всё
	кон ReError;

(** Log an error message to file Error.Log when an error arises from a passed parameter whose value was cplx. *)
	проц CplxError*( cplx: NbrCplx.Complex;  message: массив из симв8 );
	перем m: Modules.Module;  pc: NbrInt.Integer;
		module, type, proc, string: массив 64 из симв8;
		ebp, eip: цел32;
	нач {единолично}
		если W # НУЛЬ то
			ErrorCaller( m, pc, ebp, eip );  IdentifyProcedure( m, pc, module, type, proc );  W.пСтроку8( "An ERROR occurred in:" );  W.пВК_ПС;
			Location( module, type, proc );  W.пСтроку8( "The argument passed that caused this error was: " );  W.пВК_ПС;  W.пСтроку8( "   " );  NbrCplx.CplxToPolarString( cplx, 15, string );
			W.пСтроку8( string );  W.пВК_ПС;  W.пСтроку8( "resulting in the following error message:" );  W.пВК_ПС;  W.пСтроку8( "   " );  W.пСтроку8( message );  W.пВК_ПС;  W.пВК_ПС;
						DetailedErrorReport(m,pc,eip,ebp); W.ПротолкниБуферВПоток
		всё
	кон CplxError;

(** Log a warning message to file Error.Log. *)
	проц Warning*( message: массив из симв8 );
	перем m: Modules.Module;  pc: NbrInt.Integer;
		module, type, proc: массив 64 из симв8;
		ebp, eip: цел32;
	нач {единолично}
		если W # НУЛЬ то
			WarningCaller( m, pc, ebp, eip );  IdentifyProcedure( m, pc, module, type, proc );  W.пСтроку8( "A WARNING of:" );  W.пВК_ПС;  W.пСтроку8( "   " );
			W.пСтроку8( message );  W.пВК_ПС;  W.пСтроку8( "occurred in:" );  W.пВК_ПС;  Location( module, type, proc );  W.пВК_ПС;  W.ПротолкниБуферВПоток
		всё
	кон Warning;

(** Log a warning message to file Error.Log when a warning arises from a passed parameter whose value was int. *)
	проц IntWarning*( int: NbrInt.Integer;  message: массив из симв8 );
	перем m: Modules.Module;  pc: NbrInt.Integer;
		module, type, proc, string: массив 64 из симв8;
		ebp, eip: цел32;
	нач {единолично}
		если W # НУЛЬ то
			WarningCaller( m, pc, ebp, eip );  IdentifyProcedure( m, pc, module, type, proc );  W.пСтроку8( "A WARNING occurred in:" );  W.пВК_ПС;
			Location( module, type, proc );  W.пСтроку8( "The argument passed that caused this warning was: " );  W.пВК_ПС;  W.пСтроку8( "   " );  NbrInt.IntToString( int, string );
			W.пСтроку8( string );  W.пВК_ПС;  W.пСтроку8( "resulting in the following error message:" );  W.пВК_ПС;  W.пСтроку8( "   " );  W.пСтроку8( message );  W.пВК_ПС;  W.пВК_ПС;  W.ПротолкниБуферВПоток
		всё
	кон IntWarning;

(** Log a warning message to file Error.Log when a warning arises from a passed parameter whose value was rat. *)
	проц RatWarning*( rat: NbrRat.Rational;  message: массив из симв8 );
	перем m: Modules.Module;  pc: NbrInt.Integer;
		module, type, proc, string: массив 64 из симв8;
		ebp, eip: цел32;
	нач {единолично}
		если W # НУЛЬ то
			WarningCaller( m, pc, ebp, eip );  IdentifyProcedure( m, pc, module, type, proc );  W.пСтроку8( "A WARNING occurred in:" );  W.пВК_ПС;
			Location( module, type, proc );  W.пСтроку8( "The argument passed that caused this warning was: " );  W.пВК_ПС;  W.пСтроку8( "   " );  NbrRat.RatToString( rat, string );
			W.пСтроку8( string );  W.пВК_ПС;  W.пСтроку8( "resulting in the following error message:" );  W.пВК_ПС;  W.пСтроку8( "   " );  W.пСтроку8( message );  W.пВК_ПС;  W.пВК_ПС;  W.ПротолкниБуферВПоток
		всё
	кон RatWarning;

(** Log a warning message to file Error.Log when a warning arises from a passed parameter whose value was re. *)
	проц ReWarning*( re: NbrRe.Real;  message: массив из симв8 );
	перем m: Modules.Module;  pc: NbrInt.Integer;
		module, type, proc, string: массив 64 из симв8;
		ebp, eip: цел32;
	нач {единолично}
		если W # НУЛЬ то
			WarningCaller( m, pc, ebp, eip );  IdentifyProcedure( m, pc, module, type, proc );  W.пСтроку8( "A WARNING occurred in:" );  W.пВК_ПС;
			Location( module, type, proc );  W.пСтроку8( "The argument passed that caused this warning was: " );  W.пВК_ПС;  W.пСтроку8( "   " );  NbrRe.ReToString( re, 15, string );
			W.пСтроку8( string );  W.пВК_ПС;  W.пСтроку8( "resulting in the following error message:" );  W.пВК_ПС;  W.пСтроку8( "   " );  W.пСтроку8( message );  W.пВК_ПС;  W.пВК_ПС;  W.ПротолкниБуферВПоток
		всё
	кон ReWarning;

(** Log a warning message to file Error.Log when a warning arises from a passed parameter whose value was cplx. *)
	проц CplxWarning*( cplx: NbrCplx.Complex;  message: массив из симв8 );
	перем m: Modules.Module;  pc: NbrInt.Integer;
		module, type, proc, string: массив 64 из симв8;
		ebp, eip: цел32;
	нач {единолично}
		если W # НУЛЬ то
			WarningCaller( m, pc, ebp, eip );  IdentifyProcedure( m, pc, module, type, proc );  W.пСтроку8( "A WARNING occurred in:" );  W.пВК_ПС;
			Location( module, type, proc );  W.пСтроку8( "The argument passed that caused this warning was: " );  W.пВК_ПС;  W.пСтроку8( "   " );  NbrCplx.CplxToPolarString( cplx, 15, string );
			W.пСтроку8( string );  W.пВК_ПС;  W.пСтроку8( "resulting in the following error message:" );  W.пВК_ПС;  W.пСтроку8( "   " );  W.пСтроку8( message );  W.пВК_ПС;  W.пВК_ПС;  W.ПротолкниБуферВПоток
		всё
	кон CplxWarning;

(** Opens the file Error.Log, saving the prior log file to Error.Log.Bak.
		Error.Log is automatically opened whenever this module is loaded into memory,
		and it only needs to be reopened manually if you had previously closed it manually. *)
	проц Open*;
	перем ignor: целМЗ;  backup, current: Files.FileName;
	нач
		beepedError := ложь;  beepedWarning := ложь;  копируйСтрокуДо0( "Error.Log", current );  копируйСтрокуДо0( "Error.Log.Bak", backup );  Files.Delete( backup, ignor );
		Files.Rename( current, backup, ignor );  F := Files.New( current );  Files.OpenWriter( W, F, 0 )
	кон Open;

(** Closes the file Error.Log.
		Error.Log is automatically closed whenever this module is garbage collected.
		Executing Close forces the file to close immediately. *)
	проц Close*;
	нач
		если F # НУЛЬ то Files.Register( F );  W := НУЛЬ;  F := НУЛЬ всё
	кон Close;



нач
	Open;  Modules.InstallTermHandler( Close )
кон DataErrors.

DataErrors.Close ~
EditTools.OpenAscii  Error.Log ~
EditTools.OpenAscii  Error.Log.Bak ~

