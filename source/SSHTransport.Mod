модуль SSHTransport;  	(* GF	8.7.2002 / 10.12.2020 *)

использует TCP, IP, DNS, Strings, Log := ЛогЯдра,
	Ciphers := CryptoCiphers, B := CryptoBigNumbers, DH := CryptoDiffieHellman, CryptoHashes, CryptoHMAC,
	U := CryptoUtils, G := SSHGlobals, P := SSHPackets, SSHKeys;

конст
	ClientVersion = "SSH-2.0-A2 SSH-1.8";  SSHport = 22;
	(* KEXAlgorythms	= built from contents of  SSHConfiguration.Text *)
	SHKAlgorythms		= "ssh-rsa,ssh-dss";
	(* Ciphers list			=  built from contents of  SSHConfiguration.Text *)
	(* HMAC list			=  built from contents of  SSHConfiguration.Text *)
	ComprAlgorythms	= "none";
	Languages 			= "";

	CR = 0DX;  NL = 0AX;

	Closed* = 0;  Connected* = 1;  Keyexchange* = 2;  (** Connection states *)


тип
	Key = массив 64 из симв8;
	ModuleName = массив 32 из симв8;
	
	Packet* = P.Packet;	
	
	SSHMac = окласс 
	перем 
		size, keySize: размерМЗ;
		name: массив 64 из симв8;
		key: массив 64 из симв8;
		hmac: CryptoHMAC.HMac;
		
		проц &NewMac( конст modName: массив из симв8; macLen: размерМЗ );
		нач
			нов( hmac, modName );
			keySize := hmac.size;
			если macLen < hmac.size то  hmac.ShrinkLength( macLen ) всё;
			копируйСтрокуДо0( hmac.name, name );
			size := hmac.size
		кон NewMac;
		
		проц SetKey( конст macKey: массив из симв8 );
		перем i: размерМЗ;
		нач
			нцДля i := 0 до keySize-1 делай  key[i] := macKey[i]  кц;
		кон SetKey;
		
		проц Initialize( nr: размерМЗ );
		перем
			seqno: массив 4 из симв8;
		нач
			hmac.Initialize( key, keySize );
			Int2Chars( nr, seqno );
			hmac.Update( seqno, 0, 4 ); 
		кон Initialize;
		
		проц Update( конст buf: массив из симв8; pos, len: размерМЗ );
		нач
			hmac.Update( buf, pos, len )
		кон Update;
		
		проц GetMac( перем buf: массив из симв8; pos: размерМЗ );
		нач
			hmac.GetMac( buf, pos )
		кон GetMac;
		
	кон SSHMac;
	
	
	
	SecurityState = запись
		incipher, outcipher: Ciphers.Cipher;
		inkeybits, outkeybits: цел32;		(* cipher keybits *)
		inmode, outmode: цел8;				(* cipher modes *)
		inmac, outmac: SSHMac;
	кон;
	
	

	Connection* = окласс
	перем
		state-: целМЗ;
		tcp-: TCP.Connection;
		servername: массив 128 из симв8;
		cvers, svers: массив 260 из симв8;  (* string *)
		session_id-: Key;  session_id_size-: размерМЗ;
		session_key: Key;  session_key_size: размерМЗ;
		secret: массив 1024 из симв8;	(* common secret, (mpint format) *)
		incount, outcount: цел32;	(* packet counters *)
		incipher, outcipher: Ciphers.Cipher;
		inmac, outmac: SSHMac;
		
		cipherList, hmacList, kexList: массив 1024 из симв8;	(* client methods *)
		clientChannelNo: цел32;

		проц & Open*( конст hostname: массив из симв8 );
		перем
			res: целМЗ;  adr: IP.Adr;  p: размерМЗ;
		нач
			state := Closed;
			Log.пСтроку8( "connecting to " );  Log.пСтроку8( hostname );  Log.пСтроку8( " ... " );
			DNS.HostByName( hostname, adr, res );
			если res = DNS.Ok то
				нов( tcp ); tcp.Open( 0, adr, SSHport, res );
				если res = TCP.Ok то
					state := Connected;
					копируйСтрокуДо0( hostname, servername );
					Log.пСтроку8( "connected" );  Log.пВК_ПС;
					tcp.KeepAlive( истина );
					p := 0;  U.PutString( cvers, p, ClientVersion );
					incipher := Ciphers.NewCipher( "" );
					outcipher := Ciphers.NewCipher( "" );		(* empty ciphers, no encryption *)
					incount := 0; outcount := 0;
					inmac := НУЛЬ;  outmac := НУЛЬ;
					если GetServerVersion( ) то
						SendClientVersion;
						G.GetCipherList( cipherList );
						G.GetHMacList( hmacList );
						G.GetKexMethods( kexList );
						session_id_size := 0;
						NegotiateAlgorythms;
					всё
				иначе
					Log.пВК_ПС;  Log.пСтроку8( "connection failed" );  Log.пВК_ПС;
				всё
			иначе
				Log.пВК_ПС;  Log.пСтроку8( "DNS lookup failed" );  Log.пВК_ПС;
			всё;
		кон Open;


		проц GetServerVersion( ): булево;
		перем receivebuf: массив 2048 из симв8;  len, p1, p2: размерМЗ;
		нач
			нцДо
				len := ReceiveLine( tcp, receivebuf )
			кцПри Head( receivebuf, "SSH-" );

			если ~Head( receivebuf, "SSH-1.99" ) и ~Head( receivebuf, "SSH-2.0" ) то
				Log.пСтроку8( "remote host does not support SSH version 2.0" );  Log.пВК_ПС;
				tcp.Закрой( );
				возврат ложь
			иначе
				p1 := 0;  p2 := 0;
				U.PutArray( svers, p1, receivebuf, p2, len );
				возврат истина
			всё
		кон GetServerVersion;
				

		проц SendClientVersion;
		перем
			len, pos: размерМЗ; res: целМЗ;
			nl: массив 4 из симв8;
		нач
			pos := 0;
			U.GetLength( cvers, pos, len );
			tcp.ЗапишиВПоток( cvers, 4, len, истина, res );
			nl[0] := CR; nl[1] := NL;
			tcp.ЗапишиВПоток( nl, 0, 2, истина, res );
		кон SendClientVersion;
	

		(** Send packet p *)
		проц SendPacket*( p: Packet );
		перем
			i, trlen, payload, padsize, cbs: размерМЗ; res: целМЗ;
			trbuf: массив 8196 из симв8;
		нач {единолично}
			если state = Closed то  возврат  всё;
			утв( p.len > 0 );
			cbs := outcipher.blockSize;
			padsize := cbs - (p.len + 5) остОтДеленияНа cbs;
			если padsize < 4 то увел( padsize, cbs ) всё;
			payload := 1 + p.len + padsize;
			Int2Chars( payload, trbuf );  trbuf[4] := симв8ИзКода( padsize );
			trlen := 4 + payload;
			нцДля i := 0 до p.len - 1 делай trbuf[i+5] := p.buf[i]  кц;
			U.RandomBytes( trbuf, p.len + 5, padsize  );
			если outmac # НУЛЬ то
				outmac.Initialize( outcount );
				outmac.Update( trbuf, 0, trlen );
				outmac.GetMac( trbuf, trlen );
			всё;
			outcipher.Encrypt( trbuf, 0, trlen );
			если outmac # НУЛЬ то увел( trlen, outmac.size ) всё;
			tcp.ЗапишиВПоток( trbuf, 0, trlen, истина, res );
			увел( outcount );
			если G.debug то	 p.Show  всё;
		кон SendPacket;

				
		(** Receive SSH Packet *)
		проц GetPacket*( ): Packet;
		перем i, l, pos, trlen: размерМЗ; padsize, cbs: цел32; err, res: целМЗ;
			rmac, cmac: массив 64 из симв8;
			trbuf: массив 8196 из симв8;
			msg: массив 128 из симв8;
			size: размерМЗ;
			p: Packet;	
		нач
			утв( state > Closed );
			cbs := incipher.blockSize;
			tcp.ПрочтиИзПотока( trbuf, 0, cbs, cbs, l, res );
			если res # TCP.Ok то 
				Log.пСтроку8( "### Broken connection, closed by peer" ); Log.пВК_ПС;
				state := Closed;
				возврат  P.emptyPacket
			всё;
			incipher.Decrypt( trbuf, 0, cbs );
			pos := 0;
			U.GetLength( trbuf, pos, trlen );
			утв( (4 + trlen) остОтДеленияНа cbs = 0 );
			padsize := кодСимв8( trbuf[4] );  size := trlen - 1 - padsize;
			увел( trlen, 4 );	(* the len bytes itself *)
			tcp.ПрочтиИзПотока( trbuf, cbs, trlen - cbs, trlen - cbs, l, res );
			incipher.Decrypt( trbuf, cbs, trlen - cbs );	
			если inmac # НУЛЬ то
				tcp.ПрочтиИзПотока( rmac, 0, inmac.size, inmac.size, l, res );
				inmac.Initialize( incount );
				inmac.Update( trbuf, 0, trlen );
				inmac.GetMac( cmac, 0 );
				i := 0;
				нцПока (i < inmac.size) и (rmac[i] = cmac[i]) делай  увел( i )  кц;
				если i < inmac.size то
					Log.пСтроку8( "### received a packet with wrong MAC" ); Log.пВК_ПС;
					Disconnect( 1, "wrong mac" );
					возврат  P.emptyPacket
				всё
			всё;
			увел( incount );
			p := P.MakeReceivedPacket( trbuf, size );
			если G.debug то  p.Show  всё;
			
			если p.type = P.Ignore то  
				возврат GetPacket( )
			аесли p.type = P.Debug то  
				p.SetPos( 1 );
				если p.GetChar( ) = 1X то
					p.GetString( msg );
					Log.пСтроку8( msg ); Log.пВК_ПС;
				всё;
				возврат GetPacket( )
			аесли (p.type = P.KEXInit) и (state # Keyexchange) то
				Log.пСтроку8( "reexchanging keys:  not yet implemented" ); Log.пВК_ПС;
				СТОП( 99 );
				(* renegotiate( ssh, package )*)
			иначе
				если p.type = P.Disconn то
					p.SetPos( 1 );
					err := p.GetInteger( );
					p.GetString( msg );  
					Log.пСтроку8( "### remote host closed the connection: " );
					Log.пСтроку8( msg ); Log.пВК_ПС;
					state := Closed;
					p := P.emptyPacket
				всё;
			всё;
			возврат p
		кон GetPacket;	

		проц DebugMessage*( msg: массив из симв8 );
		перем 
			buf: массив 256 из симв8;
			sp: Packet;
		нач
			buf := "A2-SSH: ";  Strings.Append( buf, msg );
			нов( sp,  P.Debug, 512 );
				sp.AppChar( 1X );			(* TRUE, always display *)
				sp.AppString( buf );			(* message *)
				sp.AppString( "" );			(* language *)
			SendPacket( sp )
		кон DebugMessage;


		проц Disconnect*( reason: целМЗ;  конст msg: массив  из симв8 );
		перем sp: Packet;
		нач 
			если state > Closed то
				нов( sp, P.Disconn, 512 );
					sp.AppInteger( reason );
					sp.AppString( msg );
					sp.AppString( "" );	(* language *)
				SendPacket( sp );
				tcp.Закрой( );  state := Closed;
				Log.пСтроку8( "connection to " );  Log.пСтроку8( servername );  Log.пСтроку8( " closed "); Log.пВК_ПС;
			всё;
		кон Disconnect;

		проц GetChannelNo*( ): цел32;
		нач
			увел( clientChannelNo );
			возврат clientChannelNo
		кон GetChannelNo;

		проц PacketAvailable*(  ): булево;
		нач
			если state = Closed то  возврат ложь
			иначе  возврат tcp.Available( ) >= 16
			всё
		кон PacketAvailable;

				
		проц NegotiateAlgorythms;
		перем
			l, n, pos: размерМЗ;  kex: целМЗ;
			x, m: массив 512 из симв8;  lbuf: массив 4 из симв8;
			cipher: Ciphers.Cipher; keybits, maclen: цел32;
			modname: ModuleName;
			mode: цел8;
			sp, rp: Packet;
			hash: CryptoHashes.Hash;
			new: SecurityState;
		нач
			state := Keyexchange;
			
			sp := ClientAlgorythms( );
			SendPacket( sp );
			
			rp := GetPacket( );	
			если rp.type = P.KEXInit то	
				rp.SetPos( 17 ); (* skip serever random *)
				нцДля n := 1 до 8 делай
					rp.GetString( x );
					просей n из
					|1:
							AlgoMatch( "kex", kexList, x, m );
							если m = "diffie-hellman-group1-sha1" то kex := 1
							аесли m = "diffie-hellman-group14-sha1" то kex := 2
							аесли m = "diffie-hellman-group-exchange-sha1" то kex := 3
							аесли m = "diffie-hellman-group1-sha256" то kex := 4
							аесли m = "diffie-hellman-group14-sha256" то kex := 5
							аесли m = "diffie-hellman-group-exchange-sha256" то kex := 6
							иначе
								Disconnect( 2, "protocol error" );  возврат
							всё;
					|2:
							AlgoMatch( "hostkey", SHKAlgorythms, x, m );
							если m = "" то
								Disconnect( 2, "protocol error" );  возврат
							всё
					|3, 4:
							AlgoMatch( "cipher", cipherList, x, m );
							если m = "" то
								Disconnect( 2, "protocol error" );  возврат
							всё;
							G.GetCipherParams( m, modname, keybits, mode );
							cipher := Ciphers.NewCipher( modname );
							если n = 3 то
								new.outcipher := cipher;  new.outkeybits := keybits; new.outmode := mode
							иначе
								new.incipher := cipher;  new.inkeybits := keybits; new.inmode := mode
							всё;
					|5, 6:
							AlgoMatch( "hmac", hmacList, x, m );
							если m = "none" то
							аесли m # "" то
								G.GetHMacParams( m, modname, maclen );
							иначе
								Disconnect( 2, "protocol error" );  возврат
							всё;
							если n = 5 то  
								нов( new.outmac, modname, maclen );
							иначе  
								нов( new.inmac, modname, maclen );
							всё;
					|7, 8:
							AlgoMatch( "compr", ComprAlgorythms, x, m );
							если m # "none" то
								Disconnect( 2, "protocol error" );  возврат
							всё
					всё;
				кц;
			иначе 
				Disconnect( 2, "protocol error" );  возврат
			всё;
			если kex < 4 то
				hash := CryptoHashes.NewHash( "CryptoSHA1" )
			иначе
				hash := CryptoHashes.NewHash( "CryptoSHA256" );
				умень( kex, 3 )
			всё;
			hash.Initialize;
			pos := 0;  U.GetLength( cvers, pos, l );  hash.Update( cvers, 0, l + 4 );		(* VC *)
			pos := 0;  U.GetLength( svers, pos, l );  hash.Update( svers, 0, l + 4 );		(* VS *)
			Int2Chars( sp.len, lbuf );
			hash.Update( lbuf, 0, 4 );  hash.Update( sp.buf^, 0, sp.len );	(* IC *)
			Int2Chars( rp.len, lbuf );
			hash.Update( lbuf, 0, 4 );  hash.Update( rp.buf^, 0, rp.len );		(* IS *)

			если kex = 1 то  rp := Group1( hash )
			аесли kex = 2 то  rp := Group14( hash )
			аесли kex = 3 то  rp := GroupExchange( hash )
			всё;
			если state # Closed то  CheckSHK( rp )  всё;
			если state # Closed то
				hash.GetHash( session_key, 0 );  
				session_key_size := hash.size;
				если session_id_size = 0 то
					session_id := session_key;  
					session_id_size := session_key_size
				всё;
				ActivateNewKeys( hash, new );
				state := Connected;
			иначе
				Log.пСтроку8( "### key exchange failed" ); Log.пВК_ПС
			всё;
		кон NegotiateAlgorythms; 

		проц ClientAlgorythms(): Packet;
		перем 
			rand: массив 16 из симв8; i: целМЗ;
			sp: Packet;
		нач
			нов( sp, P.KEXInit, 2048 );
				U.RandomBytes( rand, 0, 16 );
				нцДля i := 0 до 15 делай  sp.AppChar( rand[i] )  кц;	
				sp.AppString( kexList );
				sp.AppString( SHKAlgorythms );
				sp.AppString( cipherList );  sp.AppString( cipherList );
				sp.AppString( hmacList );  sp.AppString( hmacList );
				sp.AppString( ComprAlgorythms );  sp.AppString( ComprAlgorythms );
				sp.AppString( Languages );  sp.AppString( Languages );
				sp.AppChar(  0X );	(* FALSE *)
				sp.AppInteger( 0 );
			возврат sp
		кон ClientAlgorythms;

		проц Group1( hash: CryptoHashes.Hash ): Packet;
		перем
			pos, size: размерМЗ;
			pub, serverpub, sec: B.BigNumber;  
			dh: DH.DH;  
			sp, rp: Packet;
		нач
			нов( dh, 0, "dh.ssh.group1" );
			pub := dh.GenPubKey( );

			нов( sp, P.DHInit, 1024 );
				sp.AppBigNumber( pub );
			SendPacket( sp );
			
			rp := GetPacket( );
			если rp.type = P.DHReply то
				rp.SetPos( 1 );
				rp.GetBlobInfo( pos, size );
				hash.Update( rp.buf^, pos, size );						(* KS *)
				hash.Update( sp.buf^, 1, sp.len - 1 );					(* e *)
				rp.GetBlobInfo( pos, size );
				hash.Update( rp.buf^, pos, size );						(*  f *)
				rp.SetPos( pos );
				serverpub := rp.GetBigNumber( );
				sec := dh.ComputeKey( serverpub );
				pos := 0;  U.PutBigNumber( secret, pos, sec );
				hash.Update( secret, 0, pos );							(* K *)
			иначе
				Disconnect( 2, "protocol error: 'DH REPLY' package expected" );
			всё;
			возврат rp
		кон Group1;

		проц Group14( hash: CryptoHashes.Hash ): Packet;
		перем
			pos, size: размерМЗ; 
			pub, serverpub, sec: B.BigNumber;  
			dh: DH.DH;  
			sp, rp: Packet;
		нач
			нов( dh, 0, "dh.ssh.group14" );
			pub := dh.GenPubKey( );

			нов( sp, P.DHInit, 2048 );  
				sp.AppBigNumber( pub );
			SendPacket( sp );
			
			rp := GetPacket( );
			если rp.type = P.DHReply то
				rp.SetPos( 1 );
				rp.GetBlobInfo( pos, size );
				hash.Update( rp.buf^, pos, size );					(* KS *)
				hash.Update( sp.buf^, 1, sp.len - 1 );				(* e *)
				rp.GetBlobInfo( pos, size );
				hash.Update( rp.buf^, pos, size );					(* f *)
				rp.SetPos( pos );
				serverpub := rp.GetBigNumber( );
				sec := dh.ComputeKey( serverpub );
				pos := 0;  U.PutBigNumber( secret, pos, sec );
				hash.Update( secret, 0, pos );						(* K *)
			иначе
				Disconnect( 2, "protocol error: 'DH REPLY' package expected" );
			всё;
			 возврат rp
		кон Group14;
				

		проц GroupExchange( hash: CryptoHashes.Hash ): Packet;
		перем
			pos, size: размерМЗ; 
			clientpub, serverpub, sec, prim, gen: B.BigNumber;  
			dh: DH.DH;
			sp1, sp2, rp1, rp2: Packet;
		нач
			нов( sp1, P.GEXRequest, 64 );
				sp1.AppInteger( 1024 );  
				sp1.AppInteger( 1024 );  
				sp1.AppInteger( 2048 );
			SendPacket( sp1 );
			
			rp1 := GetPacket( );
			если rp1.type = P.GEXGroup то
				rp1.SetPos( 1 );
				prim := rp1.GetBigNumber( );
				gen := rp1.GetBigNumber( );
				нов( dh, 512, "" );  dh.SetPrime( prim, gen );
				clientpub := dh.GenPubKey( );

				нов( sp2, P.GEXInit, 1024 );  
					sp2.AppBigNumber( clientpub );
				SendPacket( sp2 );
				
				rp2 := GetPacket( );
				если rp2.type = P.GEXReply то
					rp2.SetPos( 1 );
					rp2.GetBlobInfo( pos, size );
					hash.Update( rp2.buf^, pos, size );					(* KS *)
					hash.Update( sp1.buf^, 1, 12 );					(* min || n || max *)
					hash.Update( rp1.buf^, 1, rp1.len - 1 );			(* p || g *)
					hash.Update( sp2.buf^, 1, sp2.len - 1 );			(* e *)
					rp2.GetBlobInfo( pos, size );						
					hash.Update( rp2.buf^, pos, size );					(* f *)
					rp2.SetPos( pos );
					serverpub := rp2.GetBigNumber( );
					sec := dh.ComputeKey( serverpub );
					pos := 0;  U.PutBigNumber( secret, pos, sec );
					hash.Update( secret, 0, pos );						(* K *)
				иначе
					Disconnect( 2, "protocol error: 'DH GEX REPLY' package expected" );
				всё
			иначе
				Disconnect( 2, "protocol error: 'DH GEX GROUP' package expected" );
			всё;
			возврат rp2
		кон GroupExchange;


		проц CheckSHK( p: Packet );
		перем keyblob, signature: массив 2048 из симв8; len: размерМЗ;
		нач
			(* current position *)
			p.GetArray( signature, len );
			p.SetPos( 1 );
			p.GetArray( keyblob, len );
			если ~SSHKeys.VerifyHostkey(	keyblob, signature, servername, 
												session_key, session_key_size ) то
				Log.пСтроку8( "### Server host key verification failed" ); Log.пВК_ПС;
				Disconnect( 2, "protocol error" );
			всё;
		кон CheckSHK;
				
	
		проц ActivateNewKeys( hash: CryptoHashes.Hash; new: SecurityState );
		перем key: массив 512 из симв8; sp, rp: Packet;
		нач
			нов( sp, P.NewKeys, 64 );  
			SendPacket( sp );
			
			rp := GetPacket( );
			если rp.type = P.NewKeys то		
				outcipher := new.outcipher;  	
				DeriveKey( 'C', new.outkeybits DIV 8, hash, key );
				outcipher.InitKey( key, new.outkeybits );
				DeriveKey( 'A', outcipher.blockSize, hash, key );
				outcipher.SetIV( key, new.outmode );
				
				incipher := new.incipher;
				DeriveKey( 'D', new.inkeybits DIV 8, hash, key );
				incipher.InitKey( key, new.inkeybits );
				DeriveKey( 'B', incipher.blockSize, hash, key );
				incipher.SetIV( key, new.inmode );
			
				outmac := new.outmac;
				DeriveKey( 'E', outmac.keySize, hash, key );
				outmac.SetKey( key );
				
				inmac := new.inmac;
				DeriveKey( 'F', inmac.keySize, hash, key );
				inmac.SetKey( key );
			иначе
				Disconnect( 2, "protocol error:  'NEWKEYS' packet expected" )
			всё
		кон ActivateNewKeys;	


		проц DeriveKey( keykind: симв8;  len: размерМЗ;  hash: CryptoHashes.Hash; перем key: массив из симв8 );
		перем
			digest: массив 512 из симв8;  ch: массив 2 из симв8;
			i, have, secret_size: размерМЗ;
		нач
			i := 0;  U.GetLength( secret, i, secret_size ); увел( secret_size, 4 );
			ch[0] := keykind;  
			
			hash.Initialize;
			hash.Update( secret, 0, secret_size );
			hash.Update( session_key, 0, session_key_size );
			hash.Update( ch, 0, 1 );
			hash.Update( session_id, 0, session_id_size );
			hash.GetHash( digest, 0 );  have :=  hash.size;
			нцПока have < len делай
				hash.Initialize;
				hash.Update( secret, 0, secret_size );
				hash.Update( session_key, 0, session_key_size );
				hash.Update( digest, 0, have );
				hash.GetHash( digest, have );  увел( have,  hash.size );
			кц;
			нцДля i := 0 до len - 1 делай key[i] := digest[i] кц;
		кон DeriveKey;


	кон Connection;



	проц Int2Chars( v: размерМЗ;  перем buf: массив из симв8 );
	перем i: размерМЗ;
	нач
		нцДля i := 3 до 0 шаг -1 делай buf[i] := симв8ИзКода( v остОтДеленияНа 256 );  v := v DIV 256 кц;
	кон Int2Chars;

	проц Head( конст buf, s: массив из симв8 ): булево;
	перем i: размерМЗ;
	нач
		нцДля i := 0 до длинаМассива( s ) - 1 делай
			если (buf[i] # s[i]) и (s[i] # 0X) то  возврат ложь  всё
		кц;
		возврат истина
	кон Head;

	проц ReceiveLine( tcp: TCP.Connection;  перем buf: массив из симв8 ): размерМЗ;
	перем i, l: размерМЗ; res: целМЗ;
	нач
		i := 0;
		нцДо tcp.ПрочтиИзПотока( buf, i, 1, 1, l, res );  увел( i );
		кцПри buf[i - 1] = NL;
		если buf[i - 2] = CR то i := i - 2 иначе i := i - 1 всё;
		buf[i] := 0X;
		возврат i;
	кон ReceiveLine;


	проц AlgoMatch( конст algo, cstr, sstr: массив из симв8;  перем match: массив из симв8 );
	перем
		si, ci: цел16;  matched: булево;  tmp: массив 64 из симв8;

		проц nextSuit( конст buf: массив из симв8;  перем i: цел16;  перем suit: массив из симв8 );
		перем j: цел16;
		нач
			нцПока (i < длинаМассива( buf )) и (buf[i] # 0X) и ((buf[i] = ',') или (buf[i] = ' ')) делай  увел( i )  кц;
			j := 0;
			нцПока (i < длинаМассива( buf )) и (buf[i] # 0X) и ((buf[i] # ',') и (buf[i] # ' ')) делай
				suit[j]  := buf[i];  увел( i );  увел( j )
			кц;
			suit[j] := 0X;
		кон nextSuit;

	нач  ci := 0;
		нцДо  nextSuit( cstr, ci, match );  si := 0;
			нцДо
				nextSuit( sstr, si, tmp );
				matched := (tmp # "") и (tmp = match)
			кцПри matched или (tmp = "");
		кцПри matched или (match = "");
		если G.debug или (match = "") то
			Log.пСтроку8( algo ); Log.пСтроку8( " server: " ); Log.пСтроку8( sstr ); Log.пВК_ПС;
			Log.пСтроку8( algo ); Log.пСтроку8( " client: " ); Log.пСтроку8( cstr ); Log.пВК_ПС;
			Log.пСтроку8( algo ); Log.пСтроку8( " match:  " ); Log.пСтроку8( match ); Log.пВК_ПС;
		всё;
	кон AlgoMatch;

нач
	Log.пСтроку8( "A2-SSH, version 1.8" );  Log.пВК_ПС;
кон SSHTransport.
