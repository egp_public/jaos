(* Copyright 2005-2006, Markus Heule, ETH Zurich *)

модуль OSCNet;  (** AUTHOR "heulemar"; PURPOSE "OpenSoundControl networkplugins (TCP and UDP)"; *)

(*

This modue contains objecttypes for the TCP and UDP implementation of the OSC protocol. For each version, a server
and a client version is supplied.

The server version uses an OSCService as servicebackend. Upon creation of a serverobject, it will
start listening for connections or packets from the network. When it receives a packet, it parses it and upon successful
parsing, it will hand it over to the corresponding OSCService. They can also return packets to the sender of another
packet with the 'Return' function. The services can be stopped with the 'Stop' function.

Note: The TCP version uses the TCPServices framework to support multiple concurrent TCP connections.

Example of usage:

VAR
	net: OSCNet.OSCUDPServer or OSCNet.OSCTCPServer;
	service: OSCService.OSCService;
BEGIN
	...
	NEW(net, service, 57110, res); (* server listens now on port 57110 and delivers packets to service *)
	...
	net.Stop; (* stopps the networkplugin *)
	...


The client versions can send OSCPackets to a remote OSCServer. They can also receive replies from the remote OSC server.

Example:

VAR
	client: OSCTCPClient (or OSCUDPClient);
	p, newp: OSCPacket;
	res: INTEGER;
BEGIN
	NEW(client, fip, fport, TCP.NilPort, res);
	...
	res := client.Send(p);
	...
	res := client.Receive(newp);
*)

использует
	OSC, OSCService, IP, UDP, TCP, Network, TCPServices,
	Kernel, ЛогЯдра (* Testing *), Strings;

конст

	Ok* = 0;
	Timeout* = 4401;
	ParseError* = 4402;
	PacketTooBig* = 4403;
	BadReturnData* = 4404;

	MaxUDPPacketLength* = 10000H;
	MaxTCPPacketLength = MaxUDPPacketLength; (* TOOD: What value? *)
	ReceiveTimeout* = 1000; (* ms *)

	NotImplemented* = 101;

	Trace* = ложь;
	UDPHack = истина;


тип
	(* abstract class of all network clients *)
	OSCClient = окласс
		проц Send*(p: OSC.OSCPacket): целМЗ;
		нач СТОП(NotImplemented); кон Send;

		проц Receive*(перем p: OSC.OSCPacket): целМЗ;
		нач СТОП(NotImplemented); кон Receive;

		проц Close*;
		нач СТОП(NotImplemented); кон Close;
	кон OSCClient;

	(* This objecttype is used to store the IP and the Port of the remote client.
		This information is used when a packet should be returned to a sender. (See SetReturner(..) in OSCUDPServer) *)
	OSCUDPData = окласс
		перем
			fip*: IP.Adr;
			fport*: цел32;
	кон OSCUDPData;

	(* UDP Client *)
	OSCUDPClient* = окласс(OSCClient)
		перем
			s: UDP.Socket;
			fip: IP.Adr;
			fport: цел32;

		(* Creates a new UDPClient which sends packets to fip:fport.
			Supply UDP.NilPort for lport, if you don't want to specify a fixed local port for communication.
			In res the returnvalue of the UDP.Socket's creations is returned. If res doesn't equal to UDP.Ok, the
			client shouldn't be used *)
		проц &InitUDP*(fip: IP.Adr; fport, lport: цел32; перем res: целМЗ);
		нач
			сам.fip := fip;
			сам.fport := fport;
			нов(s, lport, res);
		кон InitUDP;

		(* sends an OSCMessage or an OSCBundle to fip:fport. Returns the statuscode of UDP.Socket.Send *)
		проц {перекрыта}Send*(p: OSC.OSCPacket): целМЗ;
		нач
			возврат SendUDP(s, fip, fport, p);
		кон Send;

		(* receives a packet from the network. Only UDP packets from our partner are considered.
			You can also supply a timeout in miliseconds. Use -1 for a infinite wait.
			Returns Ok, ParseError or an UDP returncode (eg:. UDP.Timeout) *)
		проц Recieve*(перем p: OSC.OSCPacket; timeout (* in ms *): цел32): целМЗ;
		перем
			fip2: IP.Adr; fport2: цел32;
			size: размерМЗ;
			buffer: Strings.String;
			got: размерМЗ;
			res: целМЗ;
			endticks: цел32;
			istimeout: булево;
		нач
			если timeout # -1 то
				(* timeout *)
				istimeout := истина;
				endticks := Kernel.GetTicks () + timeout;
			всё;
			нов(buffer, MaxUDPPacketLength);
			нцДо
				если istimeout то timeout := endticks - Kernel.GetTicks (); всё;
				s.Receive(buffer^, 0, MaxUDPPacketLength, timeout, fip2, fport2, got, res);
			кцПри (res # UDP.Ok) или (IP.AdrsEqual(fip, fip2) и (fport = fport2));
			если res # UDP.Ok то возврат res; всё;
			(* parse packet *)
			size := got;
			p := OSC.ParseOSCPacket(buffer^, size);
			если p = НУЛЬ то возврат ParseError; всё;
			возврат Ok;
		кон Recieve;

		(* closes the clientconnection *)
		проц {перекрыта}Close*;
		нач
			s.Close();
		кон Close;

	кон OSCUDPClient;


	(* UDP Server *)
	OSCUDPServer* = окласс
		перем
			s: UDP.Socket;
			serror: булево;
			oscservice: OSCService.OSCService;
			stopping: булево; (* flag to stop the service *)

			(* inernal variables of 'main'-procedure *)
			newPacket: OSC.OSCPacket;
			newUDPData: OSCUDPData;
			buffer: OSC.String; (* ARRAY MaxUDPPacketLength OF CHAR; *)
			receivefip: IP.Adr; receivefport: цел32;
			got: размерМЗ;
			res: целМЗ;

		(* Sets the signal to stop the service *)
		проц Stop*;
		нач { единолично }
			stopping := истина;
		кон Stop;

		(* Creates a new UDPServer listening on UDP port lport.. Sends received packets to service.
			If res is not UDP.Ok, then the server will immediately quit *)
		проц &InitUDPServer*(service: OSCService.OSCService; lport: цел32; перем res: целМЗ);
		нач
			утв(service # НУЛЬ);
			oscservice := service;
			нов(buffer, MaxUDPPacketLength);
			нов(s, lport, res);
			если(res # UDP.Ok) то serror := истина; иначе serror := ложь; всё;
			stopping := ложь;
		кон InitUDPServer;

		(* Returns an OSCMessage or an OSCBundle to the sender specified by data, which is indeed an instance of OSCUDPData *)
		проц return(p: OSC.OSCPacket; data: окласс): целМЗ;
		нач
			если data суть OSCUDPData то
				просейТип data: OSCUDPData делай
					если Trace то ЛогЯдра.пСтроку8('UDPServer.Return called'); ЛогЯдра.пВК_ПС;
						IP.OutAdr(data.fip); ЛогЯдра.пСтроку8(' Port: '); ЛогЯдра.пЦел64(data.fport, 10);
						ЛогЯдра.пВК_ПС; всё;
					возврат SendUDP(s, data.fip, data.fport, p);
				всё;
			иначе
				если Trace то ЛогЯдра.пСтроку8('UDPServer.Return: BadReturnData received'); ЛогЯдра.пВК_ПС; всё;
				возврат BadReturnData;
			всё;
		кон return;

	нач { активное }
		если (~serror) то
			нцДо
				(* receive packets and parse them *)
				s.Receive(buffer^, 0, MaxUDPPacketLength, ReceiveTimeout, receivefip, receivefport, got, res);
				если res = UDP.Ok то
					newPacket := OSC.ParseOSCPacket(buffer^, got);
					если newPacket # НУЛЬ то
						нов(newUDPData);
						если Trace то
							ЛогЯдра.пСтроку8('OSCUDPServer: Received Packet from: '); ЛогЯдра.п16ричное(receivefip.ipv4Adr, 10);
							ЛогЯдра.п16ричное(receivefip.usedProtocol, 10);
							ЛогЯдра.п16ричное(receivefip.data, 10);
							ЛогЯдра.пСтроку8(' port: '); ЛогЯдра.пЦел64(receivefport, 10); ЛогЯдра.пВК_ПС;
						всё;
						если UDPHack то
							newUDPData.fip := IP.StrToAdr('192.168.150.1');
						иначе
							newUDPData.fip := receivefip;
						всё;
						newUDPData.fport := receivefport;
						newPacket.SetReturner(return, newUDPData);
						oscservice.NewPacket(newPacket);
					всё;
				аесли res # UDP.Timeout то
					(* closing service *)
					нач { единолично }
						stopping := истина;
					кон;
				всё;
			кцПри stopping;
			(* cleanup *)
			s.Close();
		всё;
	кон OSCUDPServer;

	(* TCP Client *)
	OSCTCPClient* = окласс(OSCClient)
		перем
			connection: TCP.Connection;

		(* creates a new OSCTCPClient and connects to fip:fport. The user can also specify a local port to use for the outgoing
			connection. If TCP.NilPort is used, the operating system assigns a free local port number. If res doesn't euqal to
			TCP.Ok then this client shouldn't be used. *)
		проц &InitTCP*(fip: IP.Adr; fport, lport: цел32; перем res: целМЗ);
		нач
			нов(connection);
			connection.Open(lport, fip, fport, res);
		кон InitTCP;

		проц {перекрыта}Close*;
		нач
			connection.Закрой;
		кон Close;

		(* sends a packet to the connected OSCServer. Returns TCP.Ok if sent successfully, otherwise an TCP.* errorcode is
			returned. *)
		проц {перекрыта}Send*(p: OSC.OSCPacket): целМЗ;
		нач
			возврат SendTCP(connection, p);
		кон Send;

		(* receives a packet from the OSC Server. *)
		проц {перекрыта}Receive*(перем p: OSC.OSCPacket): целМЗ;
		нач
			возврат ReceiveTCP(connection, p);
		кон Receive;

	кон OSCTCPClient;


	(* An OSCTCPServer will create for each new connection an OSCTCPAgent object. This object handles all the communication
		with the connected client. It also responsible to return messages to the sender of an OSCPacket.
		Note: The registred return-handler also includes the current SELF-pointer. Therfore, a call to returner(...) in
		OSC.OSCPacket will always be delivered to the right agent object *)
	OSCTCPAgent = окласс(TCPServices.Agent);
		перем
			oscservice: OSCService.OSCService;
			newpacket: OSC.OSCPacket;
			res: целМЗ;

		проц &StartOSCAgent*(oscs: OSCService.OSCService; c: TCP.Connection; s: TCPServices.Service);
		нач
			утв(oscs # НУЛЬ);
			oscservice := oscs;
			Start(c,s);
		кон StartOSCAgent;

		(* returns a packet to the current client. data is ignored *)
		проц return*(p: OSC.OSCPacket; data: окласс): целМЗ;
		нач
			если Trace то ЛогЯдра.пСтроку8('TCPServer.Return called IP: ');
				IP.OutAdr(client.fip); ЛогЯдра.пСтроку8(' Port: '); ЛогЯдра.пЦел64(client.fport, 10);
				ЛогЯдра.пВК_ПС; всё;
			возврат SendTCP(client, p);
		кон return;

	нач { активное }
		нц
			res := ReceiveTCP(client, newpacket);
			если res = Ok то
				утв(newpacket # НУЛЬ);
				newpacket.SetReturner(return, НУЛЬ);
				oscservice.NewPacket(newpacket);
			аесли res # ParseError то прервиЦикл всё; (* Closing Connection on unrecoverableerror *)
		кц;
		Terminate;
	кон OSCTCPAgent;

	(* TCP Server *)
	OSCTCPServer* = окласс
		перем
			tcpservice: TCPServices.Service;
			service: OSCService.OSCService;

		(* starts the server: registers the OSCService s and creates the TCPServices.Service, which listens for connections *)
		проц &InitTCPServer*(s: OSCService.OSCService; lport: цел32; перем res: целМЗ);
		нач
			утв(s # НУЛЬ);
			service := s;
			нов(tcpservice, lport, newAgent, res);
		кон InitTCPServer;

		(* This function is called by tcpservice to create a new agent *)
		проц newAgent(c: TCP.Connection; s: TCPServices.Service): TCPServices.Agent;
			перем agent: OSCTCPAgent;
		нач
			нов(agent, service, c, s);
			возврат agent;
		кон newAgent;

		(* Stops the OSCTCPServer. Closes the listening socket and all established connections *)
		проц Stop*;
		нач
			tcpservice.Stop;
		кон Stop;

	кон OSCTCPServer;



	проц SendTCP(client: TCP.Connection; p: OSC.OSCPacket): целМЗ;
	перем
		buffer: OSC.String;
		size: массив 4 из симв8;
		res: целМЗ;
	нач
		утв(p # НУЛЬ);
		buffer := p.GetBytes();
		утв(buffer # НУЛЬ);
		(* TCP: <size || packet> *)
		Network.PutNet4(size, 0, p.GetSize()(цел32));
		client.ЗапишиВПоток(size, 0, 4, ложь, res);
		если(res # TCP.Ok) то возврат res; всё;
		client.ЗапишиВПоток(buffer^, 0, длинаМассива(buffer^), ложь, res);
		возврат res;
	кон SendTCP;

	проц ReceiveTCP(client: TCP.Connection; перем p: OSC.OSCPacket): целМЗ;
	перем
		res: целМЗ; len: размерМЗ;
		buffer: укль на массив из симв8;
		sizebuf: массив 4 из симв8;
		packetsize: цел32;
	нач
		client.ПрочтиИзПотока(sizebuf, 0, длинаМассива(sizebuf), 4, len, res);
		если res # TCP.Ok то возврат res всё;
		утв(len = 4);
		packetsize := Network.GetNet4(sizebuf, 0);
		(* allocate new buffer *)
		если (packetsize < 0) или (packetsize > MaxTCPPacketLength) то
			если Trace то ЛогЯдра.пСтроку8('OSCTCPAgent: Packet too big: '); ЛогЯдра.п16ричное(packetsize, 10); ЛогЯдра.пВК_ПС; всё;
			возврат PacketTooBig;
		всё;
		нов(buffer, packetsize);
		client.ПрочтиИзПотока(buffer^, 0, packetsize, packetsize, len, res);
		если res # TCP.Ok то возврат res; всё;
		утв(len = packetsize);
		p := OSC.ParseOSCPacket(buffer^, packetsize);
		если p = НУЛЬ то возврат ParseError; всё;
		возврат Ok;
	кон ReceiveTCP;

	проц SendUDP(s: UDP.Socket; fip: IP.Adr; fport: цел32; p: OSC.OSCPacket): целМЗ;
	перем
		buffer: OSC.String;
		res: целМЗ;
	нач
		утв(p # НУЛЬ);
		buffer := p.GetBytes();
		утв(buffer # НУЛЬ);
		s.Send(fip, fport, buffer^, 0, длинаМассива(buffer^), res);
		если Trace то ЛогЯдра.пСтроку8('SendUDP: buffer: '); ЛогЯдра.пСрезБуфера16рично(buffer^, 0, длинаМассива(buffer^)); ЛогЯдра.пСтроку8( ' fip '); IP.OutAdr(fip);
			ЛогЯдра.пСтроку8(' fport: '); ЛогЯдра.пЦел64(fport, 10); ЛогЯдра.пВК_ПС; всё;
		возврат res;
	кон SendUDP;
	(*
	PROCEDURE RecieveUDP(s: UDP.Socket; timeout (* in ms *): SIGNED32;
								VAR fip: IP.Adr; VAR fport: SIGNED32; VAR p: OSC.OSCPacket): SIGNED32;
		VAR
			fip2: IP.Adr; fport2: SIGNED32;
			size: SIGNED32;
			buffer: Strings.String;
			got, res: SIGNED32;
		BEGIN
			NEW(buffer, MaxUDPPacketLength);
			ASSERT(buffer # NIL);
			(* if fip = NILAdr, fport = 0 then recive from all, otherwise only from this port *)
			REPEAT
				s.Receive(buffer^, 0, MaxUDPPacketLength, timeout, gotfip, gotfport, res);
			UNTIL res # Ok

			(* Should we only receive from fip - if fip # NILAdr !!!! ???? *)
			s.Receive(buffer^, 0, MaxUDPPacketLength, timeout, fip2, fport2, got, res);
			IF res # UDP.Ok THEN RETURN res; END;
			size := got;
			(* parse packet *)
			p := OSC.ParseOSCPacket(buffer^, size);
			IF p = NIL THEN RETURN ParseError; END;
			RETURN Ok;
		END Recieve;
*)


	(* Testprocedures *)

	проц TestUDPSend*;
		перем
			socket: OSCUDPClient;
			p, p2: OSC.OSCMessage;
			attri: OSC.OSCParamInteger;
			attrs: OSC.OSCParamString;
			b: OSC.OSCBundle;
			tt: OSC.OSCTimeTag;
			ip: IP.Adr;
			res: целМЗ;
	нач
		ip := IP.StrToAdr('192.168.150.1'); ЛогЯдра.пЦел64(res, 4);
		нов(socket, ip, 57110, 57110, res); ЛогЯдра.пЦел64(res, 4);
		нов(p, Strings.NewString('/abc/def/ghi'));
		нов(attri, 01234H); p.AddArgument(attri);
		res := socket.Send(p);
		ЛогЯдра.пЦел64(res, 4); ЛогЯдра.пВК_ПС;
		нов(p2, Strings.NewString('/xyz'));
		нов(attrs, Strings.NewString('<== This is a stirng in a Message ==>'));
		p2.AddArgument(attrs);
		нов(tt); tt.SetLow(2005,12,26,18,12,15,999);
		нов(b, tt, НУЛЬ, 0); b.AddPacket(p); b.AddPacket(p2);
		res := socket.Send(b);
		socket.Close;
		ЛогЯдра.пСтроку8('TestUDPSend done'); ЛогЯдра.пВК_ПС;
	кон TestUDPSend;

	проц TestTCPSend*;
		перем
			c: OSCTCPClient;
			p, p2: OSC.OSCMessage;
			attri: OSC.OSCParamInteger;
			attrs: OSC.OSCParamString;
			b: OSC.OSCBundle;
			tt: OSC.OSCTimeTag;
			ip: IP.Adr;
			res: целМЗ;
	нач
		ip := IP.StrToAdr('192.168.150.1'); ЛогЯдра.пЦел64(res, 4);
		нов(c, ip, 2009, TCP.NilPort, res); ЛогЯдра.пЦел64(res, 4);
		нов(p, Strings.NewString('/abc/def/ghi'));
		нов(attri, 01234H); p.AddArgument(attri);
		res := c.Send(p);
		ЛогЯдра.пЦел64(res, 4); ЛогЯдра.пВК_ПС;
		нов(p2, Strings.NewString('/xyz'));
		нов(attrs, Strings.NewString('<== This is a stirng in a Message ==>'));
		p2.AddArgument(attrs);
		нов(tt); tt.SetLow(2005,12,26,18,12,15,999);
		нов(b, tt, НУЛЬ, 0); b.AddPacket(p); b.AddPacket(p2);
		res := c.Send(b);
		ЛогЯдра.пСтроку8('TestTCPSend done'); ЛогЯдра.пВК_ПС;
		c.Close;
	кон TestTCPSend;

кон OSCNet.

OSCNet.TestUDPSend ~
OSCNet.TestTCPSend ~
OSCNet.TestUDPReceive ~
*)
