модуль SkinEngine; (** AUTHOR "FN"; PURPOSE "Infrastructure for skin support"; *)

использует
	ЛогЯдра, Files, Потоки, XML, Objects := XMLObjects, Commands, Strings, BSL := SkinLanguage,
	Texts, Codecs, Pipes, Configuration, UTF8Strings,
	WM := WMWindowManager, WMComponents, WMProperties, Messages := WMMessages, Graphics := WMGraphics;

тип
	String = Strings.String;

	ReportError* = BSL.ReportError;

	Skin* = окласс
	перем xml- : XML.Document;
		filename- : массив 256 из симв8 (* for reloading skin from file *)
	кон Skin;

перем
	manager : WM.WindowManager;
	current -: Skin;

(* ----- internal functions ----------------------------------------------*)

(*load cursor bitmaps specified by xml-tree with root el *)
проц LoadCursors(el : XML.Element; manager : WM.WindowManager);
перем en: Objects.Enumerator; p : динамическиТипизированныйУкль; x : XML.Element; s : String;

	проц LoadPointerInfo(x : XML.Element; pi : WM.PointerInfo);
	перем hotX, hotY : цел32; s, bitmap : String;
		en : Objects.Enumerator; a : динамическиТипизированныйУкль; y : XML.Element;
	нач
		en := x.GetContents(); en.Reset();
		нцПока en.HasMoreElements() делай
			a := en.GetNext();
			если a суть XML.Element то
				y := a(XML.Element);
				s := y.GetName();
				если s^ = "Bitmap" то
					bitmap := GetCharContent(y)
				аесли s^ = "HotX" то
					s := GetCharContent(y); Strings.StrToInt(s^, hotX)
				аесли s^ = "HotY" то
					s := GetCharContent(y); Strings.StrToInt(s^, hotY)
				всё
			всё
		кц;
		WM.LoadCursor(bitmap^, hotX, hotY, pi);
	кон LoadPointerInfo;

нач
	en:= el.GetContents(); en.Reset();
	нцПока en.HasMoreElements() делай
		p := en.GetNext();
		если p суть XML.Element то
			x := p(XML.Element);
			s := x.GetName();
			если s^ = "Default" то
				LoadPointerInfo(x, manager.pointerStandard)
			аесли s^ = "Move" то
				LoadPointerInfo(x, manager.pointerMove)
			аесли s^ = "Text" то
				LoadPointerInfo(x, manager.pointerText)
			аесли s^ = "Crosshair" то
				LoadPointerInfo(x, manager.pointerCrosshair)
			аесли s^ = "Upleftdownright" то
				LoadPointerInfo(x, manager.pointerULDR)
			аесли s^ = "Uprightdownleft" то
				LoadPointerInfo(x, manager.pointerURDL)
			аесли s^ = "Updown" то
				LoadPointerInfo(x, manager.pointerUpDown)
			аесли s^ = "Leftright" то
				LoadPointerInfo(x, manager.pointerLeftRight)
			аесли s^ = "Link" то
				LoadPointerInfo(x, manager.pointerLink)
			всё
		всё
	кц
кон LoadCursors;

(* load window-style specified by xml-tree with root el *)
проц LoadWindow(el : XML.Element) : WM.WindowStyle;
перем contents, en : Objects.Enumerator; p : динамическиТипизированныйУкль; x, y : XML.Element;
	s, ts, ss : XML.String; desc : WM.WindowStyle; res : целМЗ;

		проц Error(конст x: массив из симв8);
		нач
			ЛогЯдра.пСтроку8("Style not completely defined, missing : ");	ЛогЯдра.пСтроку8(x); ЛогЯдра.пВК_ПС
		кон Error;

		проц LoadImg(конст name : массив из симв8; перем img : Graphics.Image);
		нач
			img := Graphics.LoadImage(name, истина)
		кон LoadImg;

нач
	нов(desc);
	contents := el.GetContents(); contents.Reset();
	нцПока contents.HasMoreElements() делай
		p := contents.GetNext();
		если p суть XML.Element то
			x := p(XML.Element);
			s := x.GetName();
			если s^ = "UseBitmaps" то
				en := x.GetContents();
				p := en.GetNext();
				если p суть XML.Chars то
					ss := p(XML.Chars).GetStr();
					если ss # НУЛЬ то
						Strings.Trim(ss^, " "); Strings.LowerCase(ss^);
						desc.useBitmaps := (ss^ = "true");
					всё
				всё;
			аесли s^ = "Title" то
				en := x.GetContents(); en.Reset();
				нцПока en.HasMoreElements() делай
					p := en.GetNext();
					если p суть XML.Element то
						y := p(XML.Element);
						ss := y.GetName();
						если ss^ = "ActiveCloseBitmap" то
							ts := GetCharContent(y);
							если ts # НУЛЬ то LoadImg(ts^, desc.ca) всё
						аесли ss^ = "InactiveCloseBitmap" то
							ts := GetCharContent(y);
							если ts # НУЛЬ то LoadImg(ts^, desc.ci) всё
						аесли ss^ = "HoverCloseBitmap" то
							ts := GetCharContent(y);
							если ts # НУЛЬ то LoadImg(ts^, desc.closeHover); всё;
						аесли ss^ = "ActiveMinimizeBitmap" то
							ts := GetCharContent(y);
							если ts # НУЛЬ то LoadImg(ts^, desc.ma); всё;
						аесли ss^ = "InactiveMinimizeBitmap" то
							ts := GetCharContent(y);
							если ts # НУЛЬ то LoadImg(ts^, desc.mi); всё;
						аесли ss^ = "HoverMinimizeBitmap" то
							ts := GetCharContent(y);
							если ts # НУЛЬ то LoadImg(ts^, desc.minimizeHover); всё;
						аесли ss^ = "ActiveTopMargin" то
							ts := GetCharContent(y);
							если ts # НУЛЬ то Strings.StrToInt(ts^, desc.atextY) всё
						аесли ss^ = "InactiveTopMargin" то
							ts := GetCharContent(y);
							если ts = НУЛЬ то desc.itextY := desc.atextY иначе Strings.StrToInt(ts^, desc.itextY) всё
						аесли ss^ = "ActiveLeftMargin" то
							ts := GetCharContent(y);
							если ts # НУЛЬ то Strings.StrToInt(ts^, desc.atextX) всё;
						аесли ss^ = "InactiveLeftMargin" то
							ts := GetCharContent(y);
							если ts = НУЛЬ то desc.itextY := desc.atextY иначе Strings.StrToInt(ts^, desc.itextX) всё
						аесли ss^ = "ActiveColor" то
							ts := GetCharContent(y);
							если ts # НУЛЬ то Strings.HexStrToInt(ts^, desc.atextColor, res) всё
						аесли ss^ = "InactiveColor" то
							ts := GetCharContent(y);
							если ts = НУЛЬ то desc.itextColor := desc.atextColor иначе Strings.HexStrToInt(ts^, desc.itextColor, res) всё;
						аесли ss^ = "MinimizeOffset" то
							ts := GetCharContent(y);
							если (ts = НУЛЬ) то desc.minimizeOffset := 0; иначе Strings.StrToInt(ts^, desc.minimizeOffset); всё;
						всё;
					всё
				кц
			аесли s^ = "Top" то
				en := x.GetContents(); en.Reset();
				нцПока en.HasMoreElements() делай
					p := en.GetNext();
					если p суть XML.Element то
						y := p(XML.Element);
						ss := y.GetName();
						если ss^ = "ActiveLeft" то
							ts := GetCharContent(y); если ts = НУЛЬ то Error("Top left active") иначе LoadImg(ts^, desc.taa) всё
						аесли ss^ = "InactiveLeft" то
							ts := GetCharContent(y); если ts = НУЛЬ то desc.tia := desc.taa иначе LoadImg(ts^, desc.tia) всё
						аесли ss^ = "ActiveMiddle" то
							ts := GetCharContent(y); если ts = НУЛЬ то Error("Top middle active") иначе LoadImg(ts^, desc.tab) всё
						аесли ss^ = "InactiveMiddle" то
							ts := GetCharContent(y); если ts = НУЛЬ то desc.tib := desc.tab иначе LoadImg(ts^, desc.tib) всё
						аесли ss^ = "ActiveRight" то
							ts := GetCharContent(y); если ts = НУЛЬ то Error("Top right active") иначе LoadImg(ts^, desc.tac) всё
						аесли ss^ = "InactiveRight" то
							ts := GetCharContent(y); если ts = НУЛЬ то desc.tic := desc.tac иначе LoadImg(ts^, desc.tic) всё
						аесли ss^ = "FocusThreshold" то
							ts := GetCharContent(y); 	если ts # НУЛЬ то Strings.StrToInt(ts^, desc.topFocusThreshold) всё
						аесли ss^ = "Threshold" то
							ts := GetCharContent(y); 	если ts # НУЛЬ то Strings.StrToInt(ts^, desc.topThreshold) всё
						всё
					всё
				кц
			аесли s^ = "Left" то
				en := x.GetContents(); en.Reset();
				нцПока en.HasMoreElements() делай
					p := en.GetNext();
					если p суть XML.Element то
						y := p(XML.Element);
						ss := y.GetName();
						если ss^ = "ActiveTop" то
							ts := GetCharContent(y); если ts # НУЛЬ то LoadImg(ts^, desc.laa) всё;
						аесли ss^ = "InactiveTop" то
							ts := GetCharContent(y); если ts # НУЛЬ то LoadImg(ts^, desc.lia) всё;
						аесли ss^ = "ActiveMiddle" то
							ts := GetCharContent(y); если ts = НУЛЬ то Error("Left middle active") иначе LoadImg(ts^, desc.lab) всё;
						аесли ss^ = "InactiveMiddle" то
							ts := GetCharContent(y); если ts # НУЛЬ то LoadImg(ts^, desc.lib) всё;
						аесли ss^ = "ActiveBottom" то
							ts := GetCharContent(y); если ts # НУЛЬ то LoadImg(ts^, desc.lac) всё;
						аесли ss^ = "InactiveBottom" то
							ts := GetCharContent(y); если ts # НУЛЬ то LoadImg(ts^, desc.lic) всё;
						аесли ss^ = "FocusThreshold" то
							ts := GetCharContent(y); 	если ts # НУЛЬ то Strings.StrToInt(ts^, desc.leftFocusThreshold) всё
						аесли ss^ = "Threshold" то
							ts := GetCharContent(y); 	если ts # НУЛЬ то Strings.StrToInt(ts^, desc.leftThreshold) всё
						всё
					всё
				кц
			аесли s^ = "Right" то
				en := x.GetContents(); en.Reset();
				нцПока en.HasMoreElements() делай
					p := en.GetNext();
					если p суть XML.Element то
						y := p(XML.Element);
						ss := y.GetName();
						если ss^ = "ActiveTop" то
							ts := GetCharContent(y); если ts # НУЛЬ то LoadImg(ts^, desc.raa) всё;
						аесли ss^ = "InactiveTop" то
							ts := GetCharContent(y); если ts # НУЛЬ то LoadImg(ts^, desc.ria) всё;
						аесли ss^ = "ActiveMiddle" то
							ts := GetCharContent(y); если ts = НУЛЬ то Error("Right middle active") иначе LoadImg(ts^, desc.rab) всё;
						аесли ss^ = "InactiveMiddle" то
							ts := GetCharContent(y); если ts # НУЛЬ то LoadImg(ts^, desc.rib) всё;
						аесли ss^ = "ActiveBottom" то
							ts := GetCharContent(y); если ts # НУЛЬ то LoadImg(ts^, desc.rac) всё;
						аесли ss^ = "InactiveBottom" то
							ts := GetCharContent(y); если ts # НУЛЬ то LoadImg(ts^, desc.ric) всё;
						аесли ss^ = "FocusThreshold" то
							ts := GetCharContent(y); 	если ts # НУЛЬ то Strings.StrToInt(ts^, desc.rightFocusThreshold) всё
						аесли ss^ = "Threshold" то
							ts := GetCharContent(y); 	если ts # НУЛЬ то Strings.StrToInt(ts^, desc.rightThreshold) всё
						всё
					всё
				кц
			аесли s^ = "Bottom" то
				en := x.GetContents(); en.Reset();
				нцПока en.HasMoreElements() делай
					p := en.GetNext();
					если p суть XML.Element то
						y := p(XML.Element);
						ss := y.GetName();
						если ss^ = "ActiveLeft" то
							ts := GetCharContent(y); если ts = НУЛЬ то Error("Bottom left active") иначе LoadImg(ts^, desc.baa) всё;
						аесли ss^ = "InactiveLeft" то
							ts := GetCharContent(y); если ts = НУЛЬ то desc.bia := desc.baa иначе LoadImg(ts^, desc.bia) всё;
						аесли ss^ = "ActiveMiddle" то
							ts := GetCharContent(y); если ts = НУЛЬ то Error("Bottom middle active") иначе LoadImg(ts^, desc.bab) всё;
						аесли ss^ = "InactiveMiddle" то
							ts := GetCharContent(y); если ts = НУЛЬ то desc.bib := desc.bab иначе LoadImg(ts^, desc.bib) всё;
						аесли ss^ = "ActiveRight" то
							ts := GetCharContent(y); если ts = НУЛЬ то Error("Bottom right active") иначе LoadImg(ts^, desc.bac) всё;
						аесли ss^ = "InactiveRight" то
							ts := GetCharContent(y); если ts = НУЛЬ то desc.bic := desc.bac иначе LoadImg(ts^, desc.bic) всё;
						аесли ss^ = "FocusThreshold" то
							ts := GetCharContent(y); 	если ts # НУЛЬ то Strings.StrToInt(ts^, desc.bottomFocusThreshold) всё
						аесли ss^ = "Threshold" то
							ts := GetCharContent(y); 	если ts # НУЛЬ то Strings.StrToInt(ts^, desc.bottomThreshold) всё
						всё
					всё
				кц
			аесли s^ = "Desktop" то
				en := x.GetContents(); en.Reset();
				нцПока en.HasMoreElements() делай
					p := en.GetNext();
					если p суть XML.Element то
						y := p(XML.Element);
						ss := y.GetName();
						если ss^ = "Color" то
							ts := GetCharContent(y);	если ts # НУЛЬ то Strings.HexStrToInt(ts^, desc.desktopColor, res) всё
						аесли ss^ = "FgColor" то
							ts := GetCharContent(y);	если ts # НУЛЬ то Strings.HexStrToInt(ts^, desc.fgColor, res) всё
						аесли ss^ = "BgColor" то
							ts := GetCharContent(y);	если ts # НУЛЬ то Strings.HexStrToInt(ts^, desc.bgColor, res) всё
						аесли ss^ = "SelectColor" то
							ts := GetCharContent(y);	если ts # НУЛЬ то Strings.HexStrToInt(ts^, desc.selectCol, res) всё
						всё
					всё
				кц
			аесли s^ = "Border" то
				en := x.GetContents(); en.Reset();
				нцПока en.HasMoreElements() делай
					p := en.GetNext();
					если p суть XML.Element то
						y := p(XML.Element);
						ss := y.GetName();
						если ss^ = "Left" то
							ts := GetCharContent(y); если ts # НУЛЬ то Strings.StrToSize(ts^, desc.lw) всё;
						аесли ss^ = "Right" то
							ts := GetCharContent(y); если ts # НУЛЬ то Strings.StrToSize(ts^, desc.rw) всё;
						аесли ss^ = "Top" то
							ts := GetCharContent(y); если ts # НУЛЬ то Strings.StrToSize(ts^, desc.th) всё;
						аесли ss^ = "Bottom" то
							ts := GetCharContent(y); если ts # НУЛЬ то иначе Strings.StrToSize(ts^, desc.bh) всё;
						аесли ss^ = "ActiveColor" то
							ts := GetCharContent(y); если ts # НУЛЬ то Strings.HexStrToInt(ts^, desc.baCol, res) всё;
						аесли ss^ = "InactiveColor" то
							ts := GetCharContent(y); если ts # НУЛЬ то Strings.HexStrToInt(ts^, desc.biCol, res) всё;
						аесли ss^ = "Active3d" то
							ts := GetCharContent(y); если ts # НУЛЬ то Strings.HexStrToInt(ts^, desc.basw, res) всё;
						аесли ss^ = "Inactive3d" то
							ts := GetCharContent(y); если ts # НУЛЬ то Strings.HexStrToInt(ts^, desc.bisw, res) всё
						всё
					всё
				кц
			всё
		всё
	кц;
	возврат desc
кон LoadWindow;


(* reset skin-engine i.e. install zero-skin *)
проц SetZeroSkin(broadcast: булево);
перем i, j : цел32; lists : WMComponents.ListArray;
	 properties : WMProperties.PropertyArray; msg : Messages.Message;
нач
	(* components *)
	lists := WMComponents.propertyListList.Enumerate();
	i := 0;
	нцПока i < длинаМассива(lists^) делай
		properties := lists[i].Enumerate();
		j := 0;
		нцПока j < длинаМассива(properties^) делай
			если properties[j].HasPrototype() то properties[j].Reset() всё;
			увел(j)
		кц;
		увел(i)
	кц;
	msg.msgType := Messages.MsgExt; msg.ext := WMComponents.componentStyleMsg;
	если broadcast то manager.Broadcast(msg) всё;
	(* default-window and cursors *)
	manager.ZeroSkin
кон SetZeroSkin;

(* install skin specified by xml-doc *)
проц SetXmlSkinInternal(doc : XML.Document);
перем p : динамическиТипизированныйУкль; cont : Objects.Enumerator; root: XML.Element;
	el : XML.Content; s : Strings.String; desc : WM.WindowStyle;
нач
	SetZeroSkin(ложь);
	root := doc.GetRoot();
	cont := root.GetContents(); cont.Reset();
	нцПока cont.HasMoreElements() делай
		p := cont.GetNext();
		если p суть XML.Element то
			el := p(XML.Element);
			s := el(XML.Element).GetName();
			LowerCase(s);
			если s^ = "window" то
				(* default window frame *)
				desc := LoadWindow(el(XML.Element))
			аесли s^ = "cursors" то
				(* cursors *)
				LoadCursors(el(XML.Element), manager)
			аесли s^ = "components" то
				(* Components *)
				WMComponents.SetStyle(p(XML.Element))
			всё
		всё
	кц;
	desc.Initialize;
	manager.SetStyle(desc);
кон SetXmlSkinInternal;

(* ----- api --------------------------------------------------------- *)

(** make SKIN the current skin, if SKIN is NIL, zeroskin will be installed *)
проц InstallSkin*(skin : Skin);
нач
	если skin # НУЛЬ то
		current := skin;
		если skin.xml # НУЛЬ то
			SetXmlSkinInternal(skin.xml)
		иначе
			SetZeroSkin(истина)
		всё
	всё
кон InstallSkin;

(* read skin from ascii-stream. the stream must provide a skin-definition in bsl (Bluebottle SkinLanguage) *)
проц GetSkinFromStream(конст filename : массив из симв8; r : Потоки.Чтец; reportError : ReportError; warnings : булево) : Skin;
перем scn : BSL.Scanner; prs : BSL.Parser; skin : Skin;
нач
	нов(scn, r);
	нов(prs, filename, scn);
	если reportError # НУЛЬ то prs.reportError := reportError всё;
	нов(skin);
	skin.xml := prs.Parse(warnings);
	если skin.xml # НУЛЬ то
		копируйСтрокуДо0(filename, skin.filename);
		возврат skin
	иначе
		возврат НУЛЬ
	всё
кон GetSkinFromStream;

(** parse T and return a skin-object defined by the bsl-definition provided in T *)
проц GetSkinFromText*(конст filename : массив из симв8; t : Texts.Text; re : ReportError; warnings : булево) : Skin;
перем encoder : Codecs.TextEncoder; pipe : Pipes.Pipe; w : Потоки.Писарь;
	r : Потоки.Чтец; res : целМЗ;
нач
	нов(pipe, 10000);
	Потоки.НастройПисаря(w, pipe.Send);
	Потоки.НастройЧтеца(r, pipe.Receive);
	encoder := Codecs.GetTextEncoder("ISO8859-1");
	если encoder = НУЛЬ то ЛогЯдра.пСтроку8("Could not open encoder ISO8859-1"); ЛогЯдра.пВК_ПС; возврат НУЛЬ всё;
	encoder.Open(w);
	encoder.WriteText(t, res);
	pipe.Close;
	возврат GetSkinFromStream(filename, r, re, warnings)
кон GetSkinFromText;

(** return a skin-object defined by the *.skin-file specified by FILENAME *)
проц GetSkinFromFile*(конст filename : массив из симв8; re : ReportError; warnings : булево) : Skin;
перем res : целМЗ; in : Потоки.Чтец; decoder : Codecs.TextDecoder; skin : Skin;
	description : массив 128 из симв8;
нач
	decoder := Codecs.GetTextDecoder("UTF-8");
	если decoder = НУЛЬ то
		ЛогЯдра.пСтроку8("Could not open decoder for UTF-8."); ЛогЯдра.пВК_ПС; возврат НУЛЬ
	всё;
	копируйСтрокуДо0(filename, description); Strings.Append(description, "://skin.bsl");
	in := Codecs.OpenInputStream(description);
	если in = НУЛЬ то
		ЛогЯдра.пСтроку8("Could not open stream on file : "); ЛогЯдра.пСтроку8(description); ЛогЯдра.пВК_ПС; возврат НУЛЬ
	всё;
	decoder.Open(in, res);
	skin := GetSkinFromText(filename, decoder.GetText(), re, warnings);
	возврат skin
кон GetSkinFromFile;

(* ----- commands ---------------------------------------------------*)

(** Set the currently installed skin as default i.e. register it in the autostart-section of Configuration.XML *)
проц SetCurrentAsDefault*(context  : Commands.Context);
перем val : массив 128 из симв8; res : целМЗ;
нач
	если current # НУЛЬ то
		val := "SkinEngine.Load "; Strings.Append(val, current.filename);
		Configuration.Put("Autostart.DefaultSkin", val, res);
		если (res = Configuration.Ok) то
			context.out.пСтроку8("Set "); context.out.пСтроку8(current.filename); context.out.пСтроку8(" as default."); context.out.пВК_ПС;
		иначе
			context.error.пСтроку8("Could not set "); context.error.пСтроку8(current.filename); context.error.пСтроку8(" as default, res: ");
			context.error.пЦел64(res, 0); context.error.пВК_ПС;
		всё;
	иначе (* ZeroSkin *)
		val := "SkinEngine.Unload";
		Configuration.Put("Autostart.DefaultSkin", val, res);
		если (res = Configuration.Ok) то
			context.out.пСтроку8("Set ZeroSkin as default."); context.out.пВК_ПС;
		иначе
			context.error.пСтроку8("Could not set ZeroSkin as default, res: "); context.error.пЦел64(res, 0); context.error.пВК_ПС;
		всё;
	всё;
кон SetCurrentAsDefault;

(** Set system to initial state (i.e. install ZeroSkin) *)
проц Unload*(context : Commands.Context);
нач
	если current # НУЛЬ то
		SetZeroSkin(истина);
		current := НУЛЬ;
	иначе
		context.out.пСтроку8("ZeroSkin already loaded"); context.out.пВК_ПС;
	всё;
кон Unload;

(** Install the skin specified by given filename (.skin)*)
проц Load*(context : Commands.Context);
перем skinfile : Files.FileName; skin : Skin;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(skinfile);
	если (current = НУЛЬ) или (UTF8Strings.Compare(skinfile, current.filename) # UTF8Strings.CmpEqual) то
		context.out.пСтроку8("SkinEngine : Loading "); context.out.пСтроку8(skinfile); context.out.пСтроку8("...");
		skin := GetSkinFromFile(skinfile, НУЛЬ, ложь);
		если skin # НУЛЬ то
			InstallSkin(skin);
			context.out.пСтроку8("ok"); context.out.пВК_ПС
		всё
	иначе
		context.out.пСтроку8("Skin "); context.out.пСтроку8(skinfile); context.out.пСтроку8(" already loaded"); context.out.пВК_ПС;
	всё;
кон Load;

(* ----- helpers ----------------------------------------------------- *)

(* convert s to lower case *)
проц LowerCase(s : String);
перем i : размерМЗ;
нач
	нцДля i := 0 до длинаМассива(s^)-1 делай s^[i] := Strings.LOW(s^[i]) кц
кон LowerCase;

(* return character-content of x's first child-node *)
проц GetCharContent(x : XML.Element) : String;
перем en : Objects.Enumerator; a : динамическиТипизированныйУкль;
нач
	en := x.GetContents(); a := en.GetNext();
	если a суть XML.Chars то возврат a(XML.Chars).GetStr()
	иначе возврат НУЛЬ всё
кон GetCharContent;


проц ВелиВсемуПерерисоваться;
перем msg : Messages.Message; перем i, j : цел32; lists : WMComponents.ListArray;
	 properties : WMProperties.PropertyArray; 
нач
	(* Скопировано с ZeroSkin, благодарность Сергею Дурманову *)
	lists := WMComponents.propertyListList.Enumerate();
	i := 0;
	нцПока i < длинаМассива(lists^) делай
		properties := lists[i].Enumerate();
		j := 0;
		нцПока j < длинаМассива(properties^) делай
			если properties[j].HasPrototype() то properties[j].Reset() всё;
			увел(j)
		кц;
		увел(i)
	кц;
	msg.msgType := Messages.MsgExt; msg.ext := WMComponents.componentStyleMsg;
	manager.Broadcast(msg);
кон ВелиВсемуПерерисоваться;

проц ВклВыклОпускЛат*;
нач
	Graphics.ОпускЛатиницу := ~ Graphics.ОпускЛатиницу;
	если Graphics.ОпускЛатиницу то 
		Graphics.ПодчЛатиницу := ложь всё;
	ВелиВсемуПерерисоваться(); кон ВклВыклОпускЛат;

проц ВклВыклПодчЛат*;
нач
	Graphics.ПодчЛатиницу := ~ Graphics.ПодчЛатиницу;
	если Graphics.ПодчЛатиницу то 
		Graphics.ОпускЛатиницу := ложь всё;
	ВелиВсемуПерерисоваться кон ВклВыклПодчЛат;

нач
	manager := WM.GetDefaultManager();
	current := НУЛЬ;
кон SkinEngine.



System.Free SkinEngine ~

