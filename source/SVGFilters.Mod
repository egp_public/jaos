модуль SVGFilters;

использует SVG, SVGColors, SVGUtilities, XML, XMLObjects, Raster, Math;

(* Constants that determine the input, the blend-mode and the type of color-matrix *)
конст
	InSourceGraphic*=0;
	InFilterElement*=1;
	BlendModeNormal=0;
	BlendModeMultiply=1;
	BlendModeScreen=2;
	BlendModeDarken=3;
	BlendModeLighten=4;
	ColorMatrixTypeMatrix=0;
	ColorMatrixTypeSaturate=1;
	ColorMatrixTypeHueRotate=2;
	ColorMatrixTypeLuminanceToAlpha=3;

тип
	Buffer*=SVG.Document;
	FilterWindow*=укль на FilterWindowDesc;
	FilterWindowDesc*=запись
		x*, y*, width*, height*: SVG.Length;
		modeBlend, modeCopy: Raster.Mode;
		sourceGraphic: Buffer;
	кон;
	In*=укль на InDesc;
	InDesc*=запись
		type*: цел8;
		fe*: FilterElement;
	кон;
	FilterElement*=окласс
		перем
			in*: In;
			x*,y*,width*,height*: SVG.Length;

		(* Apply this filter element to a new target *)
		проц Apply*(window: FilterWindow): Buffer;
		перем target: Buffer;
		нач
			target := SVG.NewDocument(округлиВниз(width),округлиВниз(height));
			ApplyFilter(window, target);
			возврат target;
		кон Apply;

		(* Apply this filter element to the specifies target *)
		проц ApplyFilter*(window: FilterWindow; target: Buffer);
		нач
			СТОП(99)
		кон ApplyFilter;

		(* Get the buffer of the processed input *)
		проц GetInBuffer(in: In; window: FilterWindow): Buffer;
		перем b:Buffer; fe: FilterElement;
		нач
			просей in.type из
				InSourceGraphic: возврат window.sourceGraphic;
				| InFilterElement: fe:=in.fe; b:=fe.Apply(window); возврат b;
			всё
		кон GetInBuffer;

		(* Get a pixel from the source buffer*)
		проц GetInPixel(x,y, tx0,ty0: размерМЗ; window: FilterWindow; in: In; source: Buffer; перем pix: Raster.Pixel);
		нач
			если in.type#InFilterElement то
				x:=x+tx0-округлиВниз(window.x);
				y:=y+ty0-округлиВниз(window.y);
			всё;
			если (0 <= x) и (x < source.width) и (0 <= y) и (y < source.height) то
				Raster.Get(source,x,y, pix, window.modeCopy);
			иначе
				Raster.SetRGBA(pix,0,0,0,0)
			всё
		кон GetInPixel;

	кон FilterElement;

	feBlend*=окласс(FilterElement)
		перем
			in2*: In;
			mode*: цел8;

		(* Apply this filter element *)
		проц {перекрыта}ApplyFilter*(window: FilterWindow; target: Buffer);
		перем tx,ty: размерМЗ;
			source,source2: Buffer;
			pix,pix2: Raster.Pixel;
		нач
			source := GetInBuffer(in,window);
			source2 := GetInBuffer(in2,window);
			нцДля ty:=0 до target.height-1 делай
			нцДля tx:=0 до target.width-1 делай
				GetInPixel(tx,ty, округлиВниз(x),округлиВниз(y), window, in,source, pix);
				GetInPixel(tx,ty, округлиВниз(x),округлиВниз(y), window, in2,source2, pix2);
				Blend(pix,pix2);
				Raster.Put(target, tx,ty, pix, window.modeCopy)
			кц; кц;
		кон ApplyFilter;

		(* Blend two pixels *)
		проц Blend(перем a, b: Raster.Pixel);
		перем fa, fb, ta, tb, i: размерМЗ;
		нач
			просей mode из
			| BlendModeNormal:	(* (1-qa)*cb+ca *)
				fa := 255;
				fb := 255-кодСимв8(a[3]);
				нцДля i := 0 до 2 делай
					a[i] := Raster.Clamp[200H + (fa * кодСимв8(a[i]) + fb * кодСимв8(b[i])) DIV 255]
				кц
			| BlendModeMultiply:	(* (1-qa)*cb+(1-qb)*ca+ca*cb *)
				fb := 255-кодСимв8(a[3]);
				нцДля i := 0 до 2 делай
					fa :=  255-кодСимв8(b[3])+кодСимв8(b[i]);
					a[i] := Raster.Clamp[200H + (fa * кодСимв8(a[i]) + fb * кодСимв8(b[i])) DIV 255]
				кц
			| BlendModeScreen:	(* cb+ca-ca*cb *)
				fb := 255;
				нцДля i := 0 до 2 делай
					fa := 255-кодСимв8(b[i]);
					a[i] := Raster.Clamp[200H + (fa * кодСимв8(a[i]) + fb * кодСимв8(b[i])) DIV 255]
				кц
			| BlendModeDarken:	(* Min((1-qa)*cb+ca,(1-qb)*ca+cb) *)
				fa := 255-кодСимв8(b[3]);
				fb := 255-кодСимв8(a[3]);
				нцДля i := 0 до 2 делай
					ta := fa * кодСимв8(a[i]) + 255 * устарПреобразуйКБолееШирокомуЦел(кодСимв8(b[i]));
					tb := 255 * устарПреобразуйКБолееШирокомуЦел(кодСимв8(a[i])) + fb * кодСимв8(b[i]);
					если ta<tb то
						a[i] := Raster.Clamp[200H + ta DIV 255]
					иначе
						a[i] := Raster.Clamp[200H + tb DIV 255]
					всё
				кц
			| BlendModeLighten:	(* Max((1-qa)*cb+ca,(1-qb)*ca+cb) *)
				fa := 255-кодСимв8(b[3]);
				fb := 255-кодСимв8(a[3]);
				нцДля i := 0 до 2 делай
					ta := fa * кодСимв8(a[i]) + 255 * устарПреобразуйКБолееШирокомуЦел(кодСимв8(b[i]));
					tb := 255 * устарПреобразуйКБолееШирокомуЦел(кодСимв8(a[i])) + fb * кодСимв8(b[i]);
					если ta>tb то
						a[i] := Raster.Clamp[200H + ta DIV 255]
					иначе
						a[i] := Raster.Clamp[200H + tb DIV 255]
					всё
				кц
			всё;

			(* 1- (1-qa)*(1-qb) *)
			fa := 255-кодСимв8(a[3]);
			fb := 255-кодСимв8(b[3]);
			a[3] := Raster.Clamp[200H + 255 - (fa*fb) DIV 255]
		кон Blend;
	кон feBlend;

	feOffset*=окласс(FilterElement)
		перем
			dx*,dy*: SVG.Length;

		(* Apply this filter element *)
		проц {перекрыта}ApplyFilter*(window: FilterWindow; target: Buffer);
		перем tx,ty, sx,sy: размерМЗ;
			source: Buffer;
			pix: Raster.Pixel;
		нач
			source := GetInBuffer(in,window);
			нцДля ty:=0 до target.height-1 делай
			нцДля tx:=0 до target.width-1 делай
				sx := округлиВниз(tx-dx);
				sy := округлиВниз(ty-dy);
				GetInPixel(sx,sy, округлиВниз(x),округлиВниз(y), window, in,source, pix);
				Raster.Put(target, tx,ty, pix, window.modeCopy)
			кц; кц;
		кон ApplyFilter;
	кон feOffset;

	ColorMatrix*=запись
		a: массив 5,4 из вещ64;
	кон;

	feColorMatrix*=окласс(FilterElement)
		перем
			type*: цел8;
			matrix*: ColorMatrix;

		(* Apply this filter element *)
		проц {перекрыта}ApplyFilter*(window: FilterWindow; target: Buffer);
		перем tx,ty: размерМЗ;
			source: Buffer;
			pix: Raster.Pixel;
		нач
			source := GetInBuffer(in,window);
			нцДля ty:=0 до target.height-1 делай
			нцДля tx:=0 до target.width-1 делай
				GetInPixel(tx,ty, округлиВниз(x),округлиВниз(y), window, in,source, pix);
				TransformByColorMatrix(pix,matrix);
				Raster.Put(target, tx,ty, pix, window.modeCopy)
			кц; кц;
		кон ApplyFilter;
	кон feColorMatrix;

	feGaussianBlur*=окласс(FilterElement)
		перем
			stdDeviationX*, stdDeviationY*: SVG.Length;

		(* Apply this filter element *)
		проц {перекрыта}ApplyFilter*(window: FilterWindow; target: Buffer);
		перем tx,ty, k: размерМЗ;
			source: Buffer;
			pix: Raster.Pixel;
			temp: Buffer;
			kernel: укль на массив из вещ64;
			kernelSizeX, kernelSizeY, kernelHalfX, kernelHalfY: размерМЗ;
			norm, sSquared2: вещ64;
			sum: SVGColors.ColorSum;
		нач
		(* Gaussian Kernel is seperable into a vertical part and a horizontal part. *)
		(* Prepare the buffers for both parts using the larger stdDeviation. *)
			kernelHalfX := 3*округлиВниз(stdDeviationX);
			kernelHalfY := 3*округлиВниз(stdDeviationY);
			kernelSizeX := 2*kernelHalfX+1;
			kernelSizeY := 2*kernelHalfY+1;
			если stdDeviationX>stdDeviationY то нов(kernel,kernelSizeX)
			иначе нов(kernel,kernelSizeY);
			всё;

			source := GetInBuffer(in,window);
			temp := SVG.NewDocument(source.width, source.height);

		(* horizontal part *)
			sSquared2 := 2*stdDeviationX*stdDeviationX;
			norm := Math.sqrt(устарПреобразуйКБолееУзкомуЦел(sSquared2)*Math.pi);
			нцДля k :=-kernelHalfX до kernelHalfX делай
				kernel[kernelHalfX+k] := Math.exp(устарПреобразуйКБолееУзкомуЦел(-k*k/sSquared2)) / norm;
			кц;

			нцДля ty:=0 до temp.height-1 делай
			нцДля tx:=0 до temp.width-1 делай
				sum[0] := 0; sum[1] := 0; sum[2] := 0; sum[3] := 0;
				нцДля k:=-kernelHalfX до kernelHalfX делай
					GetInPixel(tx+k,ty, округлиВниз(x),округлиВниз(y), window, in,source, pix);
					SVGColors.WeightedAdd(sum, kernel[kernelHalfX+k], pix);
				кц;
				SVGColors.ColorSumToPixel(sum, pix);
				Raster.Put(temp, tx,ty, pix, window.modeCopy)
			кц; кц;

		(* vertical part *)
			sSquared2 := 2*stdDeviationY*stdDeviationY;
			norm := Math.sqrt(устарПреобразуйКБолееУзкомуЦел(sSquared2)*Math.pi);
			нцДля k :=-kernelHalfY до kernelHalfY делай
				kernel[kernelHalfY+k] := Math.exp(устарПреобразуйКБолееУзкомуЦел(-k*k/sSquared2)) / norm;
			кц;

			нцДля ty:=0 до target.height-1 делай
			нцДля tx:=0 до target.width-1 делай
				sum[0] := 0; sum[1] := 0; sum[2] := 0; sum[3] := 0;
				нцДля k:=-kernelHalfY до kernelHalfY делай
					GetInPixel(tx,ty+k, округлиВниз(x),округлиВниз(y), window, in,temp, pix);
					SVGColors.WeightedAdd(sum, kernel[kernelHalfY+k], pix);
				кц;
				SVGColors.ColorSumToPixel(sum, pix);
				Raster.Put(target, tx,ty, pix, window.modeCopy)
			кц; кц
		кон ApplyFilter;
	кон feGaussianBlur;

	feMerge*=окласс(FilterElement)
		перем in2: XMLObjects.List;

		(* Apply this filter element *)
		проц {перекрыта}ApplyFilter*(window: FilterWindow; target: Buffer);
		перем tx,ty: размерМЗ;
			source: Buffer;
			pix: Raster.Pixel;
			enum: XMLObjects.Enumerator;
			next: In;
			nextPtr: динамическиТипизированныйУкль;
		нач
			если in#НУЛЬ то
				source := GetInBuffer(in,window);
				нцДля ty:=0 до target.height-1 делай
				нцДля tx:=0 до target.width-1 делай
					GetInPixel(tx,ty, округлиВниз(x),округлиВниз(y), window, in,source, pix);
					Raster.Put(target, tx,ty, pix, window.modeCopy)
				кц; кц;

				если in2#НУЛЬ то
					enum := in2.GetEnumerator();
					нцПока enum.HasMoreElements() делай
						nextPtr := enum.GetNext();
						next := nextPtr(In);
						source := GetInBuffer(next,window);
						нцДля ty:=0 до target.height-1 делай
						нцДля tx:=0 до target.width-1 делай
							GetInPixel(tx,ty, округлиВниз(x),округлиВниз(y), window, in,source, pix);
							Raster.Put(target, tx,ty, pix, window.modeBlend)
						кц; кц;
					кц
				всё
			всё
		кон ApplyFilter;

		(* Add a mergeNode *)
		проц AddNode*(in: In);
		нач
			если in2=НУЛЬ то нов(in2) всё;
			in2.Add(in);
		кон AddNode;
	кон feMerge;

	feFlood*=окласс(FilterElement)
		перем
			pix*: Raster.Pixel;

		(* Apply this filter element *)
		проц {перекрыта}ApplyFilter*(window: FilterWindow; target: Buffer);
		перем tx,ty: размерМЗ;
		нач
			нцДля ty:=0 до target.height-1 делай
			нцДля tx:=0 до target.width-1 делай
				Raster.Put(target, tx,ty, pix, window.modeCopy)
			кц; кц;
		кон ApplyFilter;
	кон feFlood;

	feImage*=окласс(FilterElement)
		перем image*: Buffer;

		(* Apply this filter element *)
		проц {перекрыта}ApplyFilter*(window: FilterWindow; target: Buffer);
		перем tx,ty, sx,sy: размерМЗ;
			pix: Raster.Pixel;
		нач
			нцДля ty:=0 до target.height-1 делай
			нцДля tx:=0 до target.width-1 делай
				sx := округлиВниз(tx*image.width/width);
				sy := округлиВниз(ty*image.height/height);
				если (sx <image.width) и (sy < image.height) то
					Raster.Get(image, sx,sy, pix, window.modeCopy);
				иначе
					Raster.SetRGBA(pix,0,0,0,0)
				всё;
				Raster.Put(target, tx,ty, pix, window.modeCopy)
			кц; кц;
		кон ApplyFilter;
	кон feImage;

	feTile*=окласс(FilterElement)

		(* Apply this filter element *)
		проц {перекрыта}ApplyFilter*(window: FilterWindow; target: Buffer);
		перем tx,ty, sx,sy: размерМЗ;
			source: Buffer;
			pix: Raster.Pixel;
		нач
			source := GetInBuffer(in,window);
			нцДля ty:=0 до target.height-1 делай
			нцДля tx:=0 до target.width-1 делай
				sx := tx остОтДеленияНа source.width;
				sy := ty остОтДеленияНа source.height;
				GetInPixel(sx,sy, округлиВниз(x),округлиВниз(y), window, in,source, pix);
				Raster.Put(target, tx,ty, pix, window.modeCopy)
			кц; кц;
		кон ApplyFilter;
	кон feTile;

	Filter*=окласс
		перем fElements: XMLObjects.ArrayDict;
			rootElement*: FilterElement;
			window*: FilterWindow;

		проц &New*;
		нач
			нов(fElements);
			нов(window);
			Raster.InitMode(window.modeBlend, Raster.srcOverDst);
			Raster.InitMode(window.modeCopy, Raster.srcCopy);
		кон New;

		(* Add a filter element *)
		проц AddFilterElement*(fElement: FilterElement; id: SVG.String);
		нач
			fElements.Remove(id^);
			fElements.Add(id^, fElement);
		кон AddFilterElement;

		(* Get a filter element with some specified id *)
		проц GetFilterElement*(id: SVG.String):FilterElement;
		перем p: динамическиТипизированныйУкль;
		нач
			p := fElements.Get(id^);
			если p = НУЛЬ то возврат НУЛЬ
			иначе возврат p(FilterElement) всё
		кон GetFilterElement;

		(* Apply this filter *)
		проц Apply*(source, target: Buffer);
		перем result: Buffer;
			minx, miny, maxx, maxy: размерМЗ;
		нач
			если rootElement#НУЛЬ то
				window.sourceGraphic := SVG.NewDocument(window.width,window.height);

				Raster.Copy(source, window.sourceGraphic,
					округлиВниз(window.x), округлиВниз(window.y), округлиВниз(window.x+window.width), округлиВниз(window.y+window.height),
					0, 0, window.modeCopy);

				result := rootElement.Apply(window);

				minx := округлиВниз(rootElement.x);
				miny := округлиВниз(rootElement.y);
				maxx := округлиВниз(rootElement.x+result.width);
				maxy := округлиВниз(rootElement.y+result.height);
				если minx<0 то minx := 0 всё;
				если miny<0 то miny := 0 всё;
				если maxx>target.width то maxx := target.width всё;
				если maxy>target.height то maxy := target.height всё;
				если (minx<=maxx) и (miny<=maxy) то
					Raster.Copy(result, target,
						minx-округлиВниз(rootElement.x), miny-округлиВниз(rootElement.y), maxx-округлиВниз(rootElement.x), maxy-округлиВниз(rootElement.y),
						minx, miny, window.modeBlend)
				всё
			иначе
				SVG.Log("Filter has no elements");
			всё
		кон Apply;

	кон Filter;

	FilterDict*=окласс
		перем filters: XMLObjects.ArrayDict;

		проц &New*;
		нач
			нов(filters)
		кон New;

		(* Add a filter *)
		проц AddFilter*(filter: Filter; id: SVG.String);
		нач
			filters.Add(id^, filter)
		кон AddFilter;

		(* Get a filter with some specified id *)
		проц GetFilter*(id: SVG.String):Filter;
		перем p: динамическиТипизированныйУкль;
		нач
			p := filters.Get(id^);
			если p = НУЛЬ то возврат НУЛЬ
			иначе возврат p(Filter) всё
		кон GetFilter;
	кон FilterDict;

	FilterStack*=окласс
		перем
			topFilter: Filter;
			next: FilterStack;

		(* Push a new filter onto the stack *)
		проц Push*(filter: Filter);
		перем pushed: FilterStack;
		нач
			нов(pushed);

			pushed^ := сам^;
			next := pushed;
			topFilter:=filter;
		кон Push;

		(* Pop the top filter from the stack *)
		проц Pop*(перем filter: Filter);
		нач
			filter := topFilter;
			сам^ := next^;
		кон Pop;
	кон FilterStack;

(* Parse some in attribute of a filter element *)
проц ParseIn*(value: SVG.String; перем in: цел8);
нач
	если value^ = "SourceGraphic" то in := InSourceGraphic
	иначе
		in := InFilterElement
	всё
кон ParseIn;

(* Parse the blendMode attribute of some feBlend element *)
проц ParseBlendMode*(value: SVG.String; перем mode: цел8);
нач
	если value^ = "normal" то mode := BlendModeNormal
	аесли value^ = "multiply" то mode := BlendModeMultiply
	аесли value^ = "screen" то mode := BlendModeScreen
	аесли value^ = "darken" то mode := BlendModeDarken
	аесли value^ = "lighten" то mode := BlendModeLighten
	иначе
		SVG.Error("mode attribute feBlend must be 'normal', 'multiply', 'screen', 'darken'  or 'lighten'");
		mode := BlendModeNormal
	всё
кон ParseBlendMode;

(* Parse some type attribute of some feColorMatrix element *)
проц ParseColorMatrixType*(value: XML.String; перем type: цел8);
нач
	если value^ = "matrix" то type := ColorMatrixTypeMatrix
	аесли value^ = "saturate" то type := ColorMatrixTypeSaturate
	аесли value^ = "hueRotate" то type := ColorMatrixTypeHueRotate
	аесли value^ = "luminanceToAlpha" то type := ColorMatrixTypeLuminanceToAlpha
	иначе
		SVG.Error("type attribute feColorMatrix must be 'matrix', 'saturate', 'hueRotate' or 'luminanceToAlpha'");
		type := ColorMatrixTypeMatrix
	всё
кон ParseColorMatrixType;

(* Parse some values attribute of some feColorMatrix element *)
проц ParseColorMatrixValues*(values: XML.String; type: цел8; перем matrix: ColorMatrix):булево;
перем pos: размерМЗ; i,j: размерМЗ;
	angle, s,c: вещ64;
нач
	(* Init to the zero matrix *)
	нцДля i := 0 до 4 делай
	нцДля j := 0 до 3 делай
		matrix.a[i,j] := 0;
	кц; кц;

	если type=ColorMatrixTypeLuminanceToAlpha то
		(* In this case the 'values' attribute is not applicable as the matrix is predefined: *)
		matrix.a[0,3] := 0.2125;
		matrix.a[1,3] := 0.7154;
		matrix.a[2,3] := 0.0721;
		возврат истина
	всё;

	(* Otherwise the default is the identity matrix *)
	matrix.a[0,0] := 1;
	matrix.a[1,1] := 1;
	matrix.a[2,2] := 1;
	matrix.a[3,3] := 1;
	если values=НУЛЬ то возврат истина всё;

	pos :=0;
	просей type из
	| ColorMatrixTypeMatrix:
		нцДля j := 0 до 3 делай
		нцДля i := 0 до 4 делай
			SVGUtilities.SkipCommaWhiteSpace(pos, values);
			SVGUtilities.StrToFloatPos(values^, matrix.a[i,j], pos);
		кц;кц;
		возврат истина
	| ColorMatrixTypeSaturate:
		SVGUtilities.SkipCommaWhiteSpace(pos, values);
		SVGUtilities.StrToFloatPos(values^, s, pos);
		matrix.a[0,0] := 0.213+0.787*s; matrix.a[1,0] := 0.715-0.715*s; matrix.a[2,0] := 0.072-0.072*s;
		matrix.a[0,1] := 0.213-0.213*s; matrix.a[1,1] := 0.715+0.285*s; matrix.a[2,1] := 0.072-0.072*s;
		matrix.a[0,2] := 0.213-0.213*s; matrix.a[1,2] := 0.715-0.715*s; matrix.a[2,2] := 0.072+0.928*s;
		возврат истина
	| ColorMatrixTypeHueRotate:
		SVGUtilities.SkipCommaWhiteSpace(pos, values);
		SVGUtilities.StrToFloatPos(values^, angle, pos);
		s := Math.sin(-устарПреобразуйКБолееУзкомуЦел(angle)/180.0*Math.pi);
		c := Math.cos(-устарПреобразуйКБолееУзкомуЦел(angle)/180.0*Math.pi);
		matrix.a[0,0] := 0.213+0.787*c-0.213*s; matrix.a[1,0] := 0.715-0.715*c-0.715*s; matrix.a[2,0] := 0.072-0.072*c+0.928*s;
		matrix.a[0,1] := 0.213-0.213*c+0.143*s; matrix.a[1,1] := 0.715+0.285*c+0.140*s; matrix.a[2,1] := 0.072-0.072*c-0.283*s;
		matrix.a[0,2] := 0.213-0.213*c-0.787*s; matrix.a[1,2] := 0.715-0.715*c+0.715*s; matrix.a[2,2] := 0.072+0.928*c+0.072*s;
		возврат истина
	всё;
	возврат ложь
кон ParseColorMatrixValues;

(* Transform a pixel by a matrix *)
проц TransformByColorMatrix(перем pix: Raster.Pixel; перем matrix: ColorMatrix);
перем p, result: массив 4 из вещ64;
нач
	p[0] := кодСимв8(pix[0])/255.0;
	p[1] := кодСимв8(pix[1])/255.0;
	p[2] := кодСимв8(pix[2])/255.0;
	p[3] := кодСимв8(pix[3])/255.0;
	result[0] := matrix.a[0,0]*p[0] + matrix.a[1,0]*p[1] + matrix.a[2,0]*p[2] + matrix.a[3,0]*p[3] + matrix.a[4,0];
	result[1] := matrix.a[0,1]*p[0] + matrix.a[1,1]*p[1] + matrix.a[2,1]*p[2] + matrix.a[3,1]*p[3] + matrix.a[4,1];
	result[2] := matrix.a[0,2]*p[0] + matrix.a[1,2]*p[1] + matrix.a[2,2]*p[2] + matrix.a[3,2]*p[3] + matrix.a[4,2];
	result[3] := matrix.a[0,3]*p[0] + matrix.a[1,3]*p[1] + matrix.a[2,3]*p[2] + matrix.a[3,3]*p[3] + matrix.a[4,3];
	pix[0] := Raster.Clamp[200H+округлиВниз(result[0]*255)];
	pix[1] := Raster.Clamp[200H+округлиВниз(result[1]*255)];
	pix[2] := Raster.Clamp[200H+округлиВниз(result[2]*255)];
	pix[3] := Raster.Clamp[200H+округлиВниз(result[3]*255)];
кон TransformByColorMatrix;

кон SVGFilters.
