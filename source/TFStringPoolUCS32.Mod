модуль TFStringPoolUCS32;

(* Модуль TFStringPool2 содержит нумерованные строки. Замена строки на число
  позволяет во многих случаях ускорить работу и сэкономить память. Тест см. в TestTFPET.Mod *)

использует
	UCS32;

конст
	BufSize = 1024;

тип
	Buffer = укль на массив BufSize из UCS32.CharJQ;
	BufferList = укль на массив из Buffer;

	StringPool* = окласс
	перем bufList : BufferList;
		end* : размерМЗ;
		nofBufs : размерМЗ;


		проц &Init*;
		нач
			end := 0;
			нов(bufList, 1);
			nofBufs := длинаМассива(bufList);
		кон Init;

		проц GrowBufList;
		перем i : размерМЗ;
			t : BufferList;
		нач
			нов(t, nofBufs * 2);
			нцДля i := 0 до nofBufs - 1 делай t[i] := bufList[i] кц;
			bufList := t;
			nofBufs := длинаМассива(bufList);
		кон GrowBufList;

		проц AddString*(конст str : UCS32.StringJQ) : размерМЗ;
		перем i, result, bufNr, bufPos : размерМЗ;
		нач {единолично}
			result := end;
			i := 0;
			нцПока str[i].UCS32CharCode # 0 делай
				bufNr := end DIV BufSize; bufPos := end остОтДеленияНа BufSize;
				если bufNr >= nofBufs то GrowBufList всё;
				если bufList[bufNr] = НУЛЬ то нов(bufList[bufNr]) всё;
				bufList[bufNr][bufPos] := str[i];
				увел(end); увел(i);
			кц;
			bufNr := end DIV BufSize; 	bufPos := end остОтДеленияНа BufSize;
			если bufNr >= nofBufs то GrowBufList всё;
			если bufList[bufNr] = НУЛЬ то нов(bufList[bufNr]) всё;
			bufList[bufNr][bufPos] := UCS32.IntToCharJQ(0);
			увел(end);
			возврат result
		кон AddString;

		проц GetString*(i : размерМЗ; перем str : UCS32.StringJQ);
		перем ch : UCS32.CharJQ; j : размерМЗ;
		нач {единолично}
			j := 0;
			нцДо
				ch := bufList[i DIV BufSize][i остОтДеленияНа BufSize];
				str[j] := ch;
				увел(i); увел(j)
			кцПри ch.UCS32CharCode = 0
		кон GetString;

		проц Equal*(a, b : размерМЗ) : булево;
		перем ca, cb : UCS32.CharJQ;
		нач {единолично}
			нцДо
				ca := bufList[a DIV BufSize][a остОтДеленияНа BufSize];
				cb := bufList[b DIV BufSize][b остОтДеленияНа BufSize];
				увел(a); увел(b)
			кцПри (UCS32.SravniRunyJQ(ca, cb) # UCS32.CmpEqual) или (ca.UCS32CharCode = 0);
			возврат UCS32.SravniRunyJQ(ca, cb) = UCS32.CmpEqual
		кон Equal;

	кон StringPool;

перем
	s : StringPool;


нач
	нов(s);
кон TFStringPoolUCS32.


