(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Example8;	(* pjm *)

(* Semaphores for Aos. *)

тип
	Semaphore* = окласс
		перем s: целМЗ;

		проц V*;
		нач {единолично} s := s+1 кон V;

		проц P*;
		нач {единолично} дождись(s > 0); s := s-1 кон P;

		проц &Init*(s: цел16);
		нач сам.s := s кон Init;

	кон Semaphore;

кон Example8.
