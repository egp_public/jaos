модуль Base64; (** AUTHOR "P.Hunziker - Ported from Oberon.Base64.Mod (JG 23.8.94) "; PURPOSE "Base64 encoding and decoding"; *)

использует Потоки, ЛогЯдра;

перем
		encTable: массив 64 из симв8;
		decTable: массив 128 из цел16;

проц Decode*(R: Потоки.Чтец; W:Потоки.Писарь);
		перем
			codes: массив 4 из цел16;
			i: цел16;
			ch: симв8;
			ok, end: булево;
	нач
		ok := истина; end := ложь;
		ch:=R.чИДайСимв8();
		нцДо
			i := 0;
			нцПока ok и (i < 4) делай
				нцПока ch<=" " делай ch:=R.чИДайСимв8(); кц;
				codes[i] := decTable[кодСимв8(ch)];
				ok := codes[i] >= 0; увел(i);
				если ok то ch:=R.чИДайСимв8() всё;
			кц;
			если i > 0 то
				если ok то
					W.пСимв8(симв8ИзКода(арифмСдвиг(codes[0], 2)+арифмСдвиг(codes[1], -4)));
					W.пСимв8(симв8ИзКода(арифмСдвиг(codes[1], 4)+арифмСдвиг(codes[2], -2)));
					W.пСимв8(симв8ИзКода(арифмСдвиг(codes[2], 6)+codes[3]))
				аесли ch = "=" то
					ok := истина; end := истина; умень(i);
					если i = 2 то W.пСимв8( симв8ИзКода(арифмСдвиг(codes[0], 2)+арифмСдвиг(codes[1], -4)))
					аесли i = 3 то
						W.пСимв8( симв8ИзКода(арифмСдвиг(codes[0], 2)+арифмСдвиг(codes[1], -4)));
						W.пСимв8( симв8ИзКода(арифмСдвиг(codes[1], 4)+арифмСдвиг(codes[2], -2)))
					аесли i # 0 то ok := ложь
					всё
				аесли i = 4 то
					ok := истина; end := истина;
					W.пСимв8( симв8ИзКода(арифмСдвиг(codes[0], 2)+арифмСдвиг(codes[1], -4)));
					W.пСимв8( симв8ИзКода(арифмСдвиг(codes[1], 4)+арифмСдвиг(codes[2], -2)));
					W.пСимв8( симв8ИзКода(арифмСдвиг(codes[2], 6)+codes[3]))
				аесли i = 1 то ok := истина; end := истина
				всё
			иначе
				end := истина
			всё;
		кцПри end;
		W.ПротолкниБуферВПоток;
	кон Decode;

	проц Encode*(R:Потоки.Чтец; W:Потоки.Писарь);
		перем
			i, j, c, c0, c1, c2, l: цел32;
			chars: массив 3 из симв8;

		проц OutCode(k:цел32);
		нач
			если l > 80 то	W.пВК_ПС; l := 0	всё;

			c0 :=кодСимв8(chars[0]);
			c := арифмСдвиг(c0, -2);
			W.пСимв8( encTable[c]);

			c0 := c0-арифмСдвиг(c, 2);
			c1 := кодСимв8(chars[1]);
			c := арифмСдвиг(c0, 4)+арифмСдвиг(c1, -4);
			если k>=1 то W.пСимв8(encTable[c]); всё;

			c1 := c1 остОтДеленияНа арифмСдвиг(1, 4);
			c2 := кодСимв8(chars[2]);
			c := арифмСдвиг(c1, 2)+арифмСдвиг(c2, -6);
			если k>=2 то W.пСимв8(encTable[c]); всё;

			c2 := c2 остОтДеленияНа арифмСдвиг(1, 6);
			если k>=3 то W.пСимв8(encTable[c2]); всё;
			увел(l, 4)
		кон OutCode;
	нач
		l := 0;
		R.чСимв8(chars[0]); i := 1;
		нцПока R.кодВозвратаПоследнейОперации=Потоки.Успех делай
			если i >= 3 то OutCode(i); i := 0 всё;
			R.чСимв8(chars[i]); увел(i)
		кц;
		умень(i);
		если i > 0 то
			j := i;
			нцПока i < 3 делай chars[i] := 0X; увел(i) кц;
			OutCode(j);
			нцПока j<3 делай W.пСимв8("="); увел(j) кц;
		всё;
		W.ПротолкниБуферВПоток;
	кон Encode;

	проц InitTables;
		перем i, max: цел16;
	нач
		max := кодСимв8("Z")-кодСимв8("A");
		нцДля i := 0 до max делай encTable[i] := симв8ИзКода(i+кодСимв8("A")) кц;
		увел(max);
		нцДля i := max до max+кодСимв8("z")-кодСимв8("a") делай encTable[i] := симв8ИзКода(i-max+кодСимв8("a")) кц;
		max := max+кодСимв8("z")-кодСимв8("a")+1;
		нцДля i := max до max+кодСимв8("9")-кодСимв8("0") делай encTable[i] := симв8ИзКода(i-max+кодСимв8("0")) кц;
		encTable[62] := "+";
		encTable[63] := "/";
		нцДля i := 0 до 127 делай decTable[i] := -1 кц;
		нцДля i := 0 до 63 делай decTable[кодСимв8(encTable[i])] := i кц
	кон InitTables;

	(*
	(* testing: expected behaviour:  "admin:1234" encode => "YWRtaW46MTIzNA==" decode => "admin:1234"*)
	PROCEDURE Test*;
	VAR W:Streams.StringWriter; R: Streams.StringReader; plain, base64: ARRAY 80 OF CHAR;
	BEGIN
		NEW(W,80); NEW(R,80);
		KernelLog.String('admin:1234 => '); KernelLog.Ln;
		R.Set('admin:1234');
		Encode(R,W);
		W.Get(base64);
		KernelLog.String(base64); KernelLog.String(" => ");

		NEW(W,80); NEW(R,80);
		R.Set(base64);
		Decode(R,W);
		W.Get(plain);
		KernelLog.String(plain); KernelLog.Ln;
	END Test;
	*)

нач
	InitTables;
кон Base64.

Base64.Test
System.Free Base64 ~
