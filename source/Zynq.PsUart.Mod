(**
	AUTHOR: Alexey Morozov, Timothee Martiel
	PURPOSE: driver implementation for Xilinx Zynq UART PS controller
*)

модуль PsUart;

использует НИЗКОУР, PsUartMin, PsUartInterrupts, Трассировка;

конст
	(** Receive errors - compatible with A2 Serials *)
	OverrunError* = 10;
	ParityError* = 11;
	FramingError* = 12;
	BreakInterrupt* = 13;

	DefaultRxBufSize* = 4096;
	DefaultTxBufSize* = 4096;

	ReceiveTimeoutInUs* = 500; (** Receive timeout in microseconds *)

	(* RX data interrupts *)
	RxDataInterrupts = {PsUartMin.XUARTPS_IXR_TOUT , PsUartMin.XUARTPS_IXR_RXFULL , PsUartMin.XUARTPS_IXR_RXOVR};

	(* RX error interrupts *)
	RxErrorInterrupts = {PsUartMin.XUARTPS_IXR_PARITY , PsUartMin.XUARTPS_IXR_FRAMING , PsUartMin.XUARTPS_IXR_OVER};

	RxInterrupts = RxDataInterrupts + RxErrorInterrupts;

тип

	UartId* = PsUartMin.UartId;
	ClockFrequency* = PsUartMin.ClockFrequency;

	UartController* = укль на запись
		id-: UartId; (** UART controller ID *)
		regs-: PsUartMin.UartRegisters; (** controller registers *)
		inputClock-: ClockFrequency; (** controller input clock in Hz *)
		bps-, data-, parity-, stop-: цел32; (** current parameter values *)

		open-: булево; (** TRUE if the controller is open *)

		rxBuf: укль на массив из симв8; (* receive (RX) circular buffer *)
		rxBufRdPos, rxBufWrPos: размерМЗ; (* RX buffer read and write positions *)

		errors: мнвоНаБитах32;
	кон;

перем
	uarts: массив 2 из UartController;

	(* Disable all UART interrupts *)
	проц DisableInterrupts(regs: PsUartMin.UartRegisters);
	нач
		regs.idr := PsUartMin.XUARTPS_IXR_MASK;
	кон DisableInterrupts;

	проц IntrHandler(param: динамическиТипизированныйУкль);
	перем
		uart: UartController;
		intrStatus: мнвоНаБитах32;
	нач
		uart := param(UartController);
		intrStatus := uart.regs.imr * uart.regs.isr;
		uart.regs.isr := intrStatus; (* clear the interrupt *)

		если intrStatus * RxInterrupts # {} то
			IntrHandlerRx(uart,intrStatus);
		всё;
	кон IntrHandler;

	проц IntrHandlerRx(uart: UartController; intrStatus: мнвоНаБитах32);
	перем
		bufWrPos: размерМЗ;
	нач

		если intrStatus * RxErrorInterrupts # {} то
			если PsUartMin.XUARTPS_IXR_OVER в intrStatus то
				включиВоМнвоНаБитах(uart.errors,OverrunError);
				Трассировка.пСтроку8("---rx overrun(1)---: intrStatus="); Трассировка.пМнвоНаБитахМЗКакЧисла(intrStatus); Трассировка.пВК_ПС;
				возврат;
			всё;
		всё;

		bufWrPos := uart.rxBufWrPos;

		нцПока ~(PsUartMin.XUARTPS_SR_RXEMPTY в uart.regs.sr) делай

			uart.rxBuf[bufWrPos] := симв8ИзКода(uart.regs.fifo);
			увел(bufWrPos);
			если bufWrPos = длинаМассива(uart.rxBuf) то
				bufWrPos := 0;
			всё;

			если bufWrPos = uart.rxBufRdPos то
				включиВоМнвоНаБитах(uart.errors,OverrunError);
				Трассировка.пСтроку8("---rx overrun(2)---: intrStatus="); Трассировка.пМнвоНаБитахМЗКакЧисла(intrStatus); Трассировка.пВК_ПС;
				возврат;
			всё;
		кц;

		uart.rxBufWrPos := bufWrPos;
	кон IntrHandlerRx;

	(*
		Returns TRUE if a cyclic buffer is full
	*)
	проц BufIsFull(bufWrPos, bufRdPos, bufSize: размерМЗ): булево;
	нач
		если bufWrPos # (bufSize-1) то
			возврат bufRdPos = (bufWrPos+1);
		иначе
			возврат bufRdPos = 0;
		всё;
	кон BufIsFull;

	(**
		Install a UART controller present in the system

		uart: ID (0-based index) of the UART controller to install
		base: controller base address
		inputClock: controller input clock in Hz
		res: returned error code, 0 in case of success
	*)
	проц Install* (uart: UartId; base: адресВПамяти; inputClock: ClockFrequency; перем res: целМЗ);
	перем ctl: UartController;
	нач
		PsUartMin.Install(uart, base, inputClock, res);
		если res # 0 то возврат; всё;

		нов(ctl);
		uarts[uart] := ctl;

		ctl.id := uart;
		ctl.regs := PsUartMin.GetUart(uart);
		ctl.inputClock := inputClock;
		ctl.open := ложь;
		ctl.bps := PsUartMin.DefaultBPS;
		ctl.data := PsUartMin.DefaultDataBits;
		ctl.parity := PsUartMin.DefaultParity;
		ctl.stop := PsUartMin.DefaultStop;

		нов(ctl.rxBuf,DefaultRxBufSize);

		утв(PsUartInterrupts.InstallInterruptHandler(uart,IntrHandler,ctl));
	кон Install;

	(**
		Get UART controller with a given ID

		uart: UART controller ID

		Returns NIL in case if no controller with given ID has been installed
	*)
	проц GetUart*(uart: UartId): UartController;
	нач
		если (uart >= 0) и (uart < длинаМассива(uarts)) то
			возврат uarts[uart];
		иначе возврат НУЛЬ;
		всё;
	кон GetUart;

	(**
		Open a UART controller

		uart: UART controller
		bps: baudrate
		data: number of data bits
		parity: parity control
		stop: number of stop bits
		res: returned error code, 0 in case of success
	*)
	проц Open*(uart: UartController; bps, data, parity, stop: цел32; перем res: целМЗ);
	перем n: размерМЗ;
	нач
		если uart.open то res := PsUartMin.PortInUse; возврат; всё;

		PsUartMin.Reset(uart.regs);

		если ~PsUartMin.SetBps(uart.regs, bps, res) или
			~PsUartMin.SetDataBits(uart.regs, data, res) или
			~PsUartMin.SetParity(uart.regs, parity, res) или
			~PsUartMin.SetStopBits(uart.regs, stop, res) то  возврат;
		всё;

		uart.bps := bps;
		uart.data := data;
		uart.parity := parity;
		uart.stop := stop;

		uart.rxBufWrPos := 0;
		uart.rxBufRdPos := 0;

		(* configure receive timeout to be as close as possible to ReceiveTimeoutInUs *)
		n := округлиВниз((ReceiveTimeoutInUs*вещ32(bps)+1000000) / 4000000 + 0.5);
		n := матМаксимум(1,матМинимум(255,n-1));
		uart.regs.rxtout := n;

		uart.regs.cr := uart.regs.cr + {PsUartMin.XUARTPS_CR_TORST}; (* restart receive timeout counter *)

		uart.regs.rxwm := 32; (* RX FIFO triggering threshold *)

		uart.regs.ier := RxInterrupts;

		PsUartMin.Enable(uart.regs,истина);

		res := 0;
		uart.open := истина;
	кон Open;

	(**
		Close a UART controller

		uart: UART controller
	*)
	проц Close*(uart: UartController);
	нач
		если uart.open то
			uart.open := ложь;
			DisableInterrupts(uart.regs);
			PsUartMin.Enable(uart.regs,ложь);
		всё;
	кон Close;

	проц OccupiedBufSpace(bufWrPos, bufRdPos, bufSize: размерМЗ): размерМЗ;
	перем n: размерМЗ;
	нач
		n := bufWrPos - bufRdPos;
		если n >= 0 то
			возврат n;
		иначе
			возврат n+bufSize;
		всё;
	кон OccupiedBufSpace;

	(* Returns the amount of available free space in a cyclic buffer *)
	проц AvailableBufSpace(bufWrPos, bufRdPos, bufSize: размерМЗ): размерМЗ;
	перем n: размерМЗ;
	нач
		n := bufWrPos - bufRdPos;
		если n >= 0 то
			возврат bufSize-1-n;
		иначе
			возврат -n-1;
		всё;
	кон AvailableBufSpace;

	(**
		Returns number of bytes available in the receive buffer of a UART controller

		uart: UART controller
		res: error code, 0 in case of success
	*)
	проц Available*(uart: UartController): размерМЗ;
	нач
		возврат OccupiedBufSpace(uart.rxBufWrPos,uart.rxBufRdPos,длинаМассива(uart.rxBuf));
	кон Available;

	(**
		Send a single character to the UART

		ch: character to send
		propagate: TRUE for flushing the TX FIFO buffer
		res: error code, 0 in case of success
	*)
	проц SendChar*(uart: UartController; ch: симв8; propagate: булево; onBusy: PsUartMin.BusyLoopCallback; перем res: целМЗ);
	нач
		PsUartMin.SendChar(uart.regs,ch,propagate,onBusy,res);
	кон SendChar;

	(**
		Send data to the UART
	*)
	проц Send*(uart: UartController; конст buf: массив из симв8; offs, len: размерМЗ; propagate: булево; onBusy: PsUartMin.BusyLoopCallback; перем res: целМЗ);
	нач
		нцПока uart.open и (len > 0) делай
			если ~(PsUartMin.XUARTPS_SR_TNFUL в uart.regs.sr) то
				uart.regs.fifo := кодСимв8(buf[offs]);
				увел(offs); умень(len);
			аесли (onBusy # НУЛЬ) и ~onBusy(res) то
				возврат;
			всё;
		кц;

		если propagate то (* flush the FIFO *)
			нцПока uart.open и ~(PsUartMin.XUARTPS_SR_TXEMPTY в uart.regs.sr) делай
				если (onBusy # НУЛЬ) и ~onBusy(res) то возврат; всё;
			кц;
		всё;

		если uart.open то
			res := PsUartMin.Ok;
		иначе
			res := PsUartMin.Closed;
		всё;
	кон Send;

	(**
		Receive a single character from UART

		res: error code, 0 in case of success

		Remarks: blocks until a character is available
	*)
	проц ReceiveChar*(uart: UartController; onBusy: PsUartMin.BusyLoopCallback; перем res: целМЗ): симв8;
	перем
		buf: массив 1 из симв8;
		len: размерМЗ;
	нач
		Receive(uart,buf,0,1,1,len,onBusy,res);
		возврат buf[0];
	кон ReceiveChar;

	(**
		Receive data from the UART
	*)
	проц Receive*(uart: UartController; перем buf: массив из симв8; offs, size, min: размерМЗ; перем len: размерМЗ; onBusy: PsUartMin.BusyLoopCallback; перем res: целМЗ);
	перем
		bufRdPos, bufWrPos, n: размерМЗ;
	нач
		если ~uart.open то res := PsUartMin.Closed; возврат; всё;

		res := 0;
		len := 0;

		если size = 0 то возврат; всё;

		min := матМинимум(size,min);

		нцПока uart.open и (size > 0) делай

			bufRdPos := uart.rxBufRdPos;
			bufWrPos := uart.rxBufWrPos;

			n := OccupiedBufSpace(bufWrPos,bufRdPos,длинаМассива(uart.rxBuf));

			если n # 0 то

				n := матМинимум(n,size);
				умень(size,n); увел(len,n);
				если min > 0 то умень(min,n); всё;

				(*!
					Make sure the receive buffer content
					corresponds to the state of uart.rxBufWrPos
				*)
				машКод
					DMB
				кон;

				нцПока n > 0 делай
					buf[offs] := uart.rxBuf[bufRdPos];
					увел(bufRdPos);
					если bufRdPos = длинаМассива(uart.rxBuf) то
						bufRdPos := 0;
					всё;
					увел(offs); умень(n);
				кц;

				uart.rxBufRdPos := bufRdPos;

			аесли min > 0 то
				если (onBusy # НУЛЬ) и ~onBusy(res) то возврат; всё;
			иначе
				возврат;
			всё;
		кц;
	кон Receive;

	проц PrintRegisters(regs: PsUartMin.UartRegisters);
	нач
		Трассировка.пСтроку8("cr("); Трассировка.п16ричное(адресОт(regs.cr),-8); Трассировка.пСтроку8("): "); Трассировка.пМнвоНаБитахМЗКакЧисла(regs.cr); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("mr("); Трассировка.п16ричное(адресОт(regs.mr),-8); Трассировка.пСтроку8("): "); Трассировка.пМнвоНаБитахМЗКакЧисла(regs.mr); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("ier(");  Трассировка.п16ричное(адресОт(regs.ier),-8); Трассировка.пСтроку8("): write only"); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("idr("); Трассировка.п16ричное(адресОт(regs.idr),-8); Трассировка.пСтроку8("): write only"); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("imr("); Трассировка.п16ричное(адресОт(regs.imr),-8); Трассировка.пСтроку8("): "); Трассировка.пМнвоНаБитахМЗКакЧисла(regs.imr); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("isr("); Трассировка.п16ричное(адресОт(regs.isr),-8); Трассировка.пСтроку8("): "); Трассировка.пМнвоНаБитахМЗКакЧисла(regs.isr); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("baudgen("); Трассировка.п16ричное(адресОт(regs.baudgen),-8); Трассировка.пСтроку8("): "); Трассировка.пЦел64(regs.baudgen,0); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("rxtout("); Трассировка.п16ричное(адресОт(regs.rxtout),-8); Трассировка.пСтроку8("): "); Трассировка.пЦел64(regs.rxtout,0); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("rxwm("); Трассировка.п16ричное(адресОт(regs.rxwm),-8); Трассировка.пСтроку8("): "); Трассировка.пЦел64(regs.rxwm,0); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("modemcr("); Трассировка.п16ричное(адресОт(regs.modemcr),-8); Трассировка.пСтроку8("): "); Трассировка.пМнвоНаБитахМЗКакЧисла(regs.modemcr); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("modemsr("); Трассировка.п16ричное(адресОт(regs.modemsr),-8); Трассировка.пСтроку8("): "); Трассировка.пМнвоНаБитахМЗКакЧисла(regs.modemsr); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("sr("); Трассировка.п16ричное(адресОт(regs.sr),-8); Трассировка.пСтроку8("): "); Трассировка.пМнвоНаБитахМЗКакЧисла(regs.sr); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("fifo(");  Трассировка.п16ричное(адресОт(regs.fifo),-8); Трассировка.пСтроку8("): --- "); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("bauddiv("); Трассировка.п16ричное(адресОт(regs.bauddiv),-8); Трассировка.пСтроку8("): "); Трассировка.пЦел64(regs.bauddiv,0); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("flowdel("); Трассировка.п16ричное(адресОт(regs.flowdel),-8); Трассировка.пСтроку8("): "); Трассировка.пЦел64(regs.flowdel,0); Трассировка.пВК_ПС;
		Трассировка.пСтроку8("txwm("); Трассировка.п16ричное(адресОт(regs.txwm),-8); Трассировка.пСтроку8("): "); Трассировка.пЦел64(regs.txwm,0); Трассировка.пВК_ПС;
	кон PrintRegisters;

	проц Show*;
	нач
		если uarts[0] # НУЛЬ то
			Трассировка.StringLn("PS UART0:");
			PrintRegisters(uarts[0].regs);
			Трассировка.пСтроку8("rxBufRdPos="); Трассировка.пЦел64(uarts[0].rxBufRdPos,0); Трассировка.пВК_ПС;
			Трассировка.пСтроку8("rxBufWrPos="); Трассировка.пЦел64(uarts[0].rxBufWrPos,0); Трассировка.пВК_ПС;
			Трассировка.пВК_ПС;
		всё;
		если uarts[1] # НУЛЬ то
			Трассировка.StringLn("PS UART1:");
			PrintRegisters(uarts[1].regs);
			Трассировка.пСтроку8("rxBufRdPos="); Трассировка.пЦел64(uarts[1].rxBufRdPos,0); Трассировка.пВК_ПС;
			Трассировка.пСтроку8("rxBufWrPos="); Трассировка.пЦел64(uarts[1].rxBufWrPos,0); Трассировка.пВК_ПС;
			Трассировка.пВК_ПС;
		всё;

	кон Show;

кон PsUart.
