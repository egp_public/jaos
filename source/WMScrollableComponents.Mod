модуль WMScrollableComponents; (** AUTHOR "Ingmar Nebel"; PURPOSE "Scrollable Container"; *)

использует
	Strings, XML, WMGraphics, WMRectangles, WMMessages, WMProperties, WMComponents, WMStandardComponents;

тип
	(* Local type-alias for convenience *)
	String = Strings.String;
	Panel = WMStandardComponents.Panel;

	(* special component to adapt to scroll target, in total conrol by ScrollablePanel, never use otherwise *)
	ScrollPanel*= окласс(WMComponents.VisualComponent)
	перем
		left, top, dx, dy: размерМЗ;
		CheckScrollbars: WMMessages.CompCommand;
		resizing: булево; (* distinguish whether AlignSubComponents is called from children or from Resized *)

		проц &New*(CheckScrollbars: WMMessages.CompCommand);
		нач
			Init;
			сам.CheckScrollbars := CheckScrollbars;
			left := 0; top := 0;
			SetNameAsString(StrScrollPanel);
		кон New;

		(* store total width and height of subcomponents, check it *)
		проц {перекрыта}AlignSubComponents*;
		перем c: XML.Content; vc : WMComponents.VisualComponent;
			r, rCopy, rEnclosing, vcBounds, b : WMRectangles.Rectangle;
		нач
			Acquire;
			если aligning то Release; возврат всё;
			aligning := истина;
			r := GetClientRect(); rCopy := r; rEnclosing := r;
			c := GetFirst();
			нцПока (c # НУЛЬ) делай
				если c суть WMComponents.VisualComponent то
					vc := c(WMComponents.VisualComponent);
					если vc.visible.Get() то
						b := vc.bearing.Get();
						просей vc.alignment.Get() из
						| WMComponents.AlignTop : vc.bounds.Set(WMRectangles.MakeRect(r.l + b.l , r.t + b.t, r.r - b.r, r.t + b.t + vc.bounds.GetHeight())); увел(r.t, vc.bounds.GetHeight() + b.t + b.b);
						| WMComponents.AlignLeft : vc.bounds.Set(WMRectangles.MakeRect(r.l + b.l, r.t + b.t, r.l + b.l + vc.bounds.GetWidth(), r.b - b.b)); увел(r.l, vc.bounds.GetWidth() + b.l + b.r)
						| WMComponents.AlignBottom : vc.bounds.Set(WMRectangles.MakeRect(r.l + b.l, r.b - vc.bounds.GetHeight() - b.b, r.r - b.r, r.b - b.b)); умень(r.b, vc.bounds.GetHeight() + b.t + b.b)
						| WMComponents.AlignRight : vc.bounds.Set(WMRectangles.MakeRect(r.r - vc.bounds.GetWidth() - b.r , r.t + b.t, r.r - b.r, r.b - b.b)); умень(r.r, vc.bounds.GetWidth() + b.l + b.r);
						| WMComponents.AlignClient : если ~WMRectangles.RectEmpty(r) то vc.bounds.Set(WMRectangles.MakeRect(r.l + b.l , r.t + b.t, r.r - b.r, r.b  - b.b)) всё
						иначе (* nothing *)
						всё;
						vcBounds := vc.bounds.Get();
						WMRectangles.ExtendRect(rEnclosing, vcBounds);
					всё
				всё;
				c := GetNext(c);
			кц;
			dx :=  матМаксимум(0, (rEnclosing.r-rEnclosing.l)-(rCopy.r-rCopy.l));
			dy := матМаксимум(0, (rEnclosing.b-rEnclosing.t)-(rCopy.b-rCopy.t));
			CheckLeftTop;
			aligning := ложь;
			Release;
			если ~resizing то CheckScrollbars(НУЛЬ, НУЛЬ) всё;
		кон AlignSubComponents;

		проц CheckLeftTop;
		нач
			left := матМинимум(left, dx);
			top := матМинимум(top, dy);
		кон CheckLeftTop;

		проц SetLeftTop(dxf, dyf: вещ32);
		нач
			сам.left := округлиВниз(dx * dxf); сам.top := округлиВниз(dy * dyf); CheckLeftTop;
		кон SetLeftTop;

		(** Special methods *)
		проц {перекрыта}Resized*;
		нач
			если sequencer # НУЛЬ то утв(sequencer.lock.HasWriteLock()) всё;
			resizing := истина;
			DisableUpdate;
			(* don't need to adjust parent, because bounds are always changed by parent, not third party
			p := SELF.GetParent();
			IF (p # NIL) & (p IS VisualComponent) THEN p(VisualComponent).AlignSubComponents END;
			*)
			AlignSubComponents;
			EnableUpdate;
			(*IF (p # NIL) & (p IS VisualComponent) THEN p(VisualComponent).Invalidate
			ELSE Invalidate()
			END*)
			resizing := ложь;
			Invalidate;
		кон Resized;

		(** declare a rectangle area as dirty *)
		проц {перекрыта}InvalidateRect*(r: WMRectangles.Rectangle);
		перем parent : XML.Element;
			m : WMMessages.Message; b, cr : WMRectangles.Rectangle;
		нач
			если ~initialized то возврат всё;
			если ~visible.Get() то возврат всё;
			если ~IsCallFromSequencer() то
				m.msgType := WMMessages.MsgExt;
				m.ext := WMComponents.invalidateRectMsg; m.x := r.l; m.y := r.t; m.dx := r.r; m.dy := r.b; m.sender := сам;
				если ~sequencer.Add(m) то всё;
			иначе
				parent := GetParent();
				если (parent # НУЛЬ) и (parent суть WMComponents.VisualComponent) то
					cr := GetClientRect();
					WMRectangles.MoveRel(r, -left, -top);
					WMRectangles.ClipRect(r, cr);
					если ~WMRectangles.RectEmpty(r) то
						b := bounds.Get();
						WMRectangles.MoveRel(r, b.l, b.t);
						parent(WMComponents.VisualComponent).InvalidateRect(r)
					всё
				всё
			всё
		кон InvalidateRect;

		проц {перекрыта}InvalidateCommand*(sender, par : динамическиТипизированныйУкль);
		перем cr: WMRectangles.Rectangle;
		нач
			если ~initialized то возврат всё;
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.InvalidateCommand, sender, par)
			аесли visible.Get() то
				cr := GetClientRect(); WMRectangles.MoveRel(cr, left, top);
				InvalidateRect(cr)
			всё
		кон InvalidateCommand;

		проц {перекрыта}HandleInternal*(перем msg : WMMessages.Message); (** PROTECTED *)
		нач
			утв(IsCallFromSequencer());
			если (msg.msgType = WMMessages.MsgPointer) или (msg.msgType = WMMessages.MsgDrag) то
				msg.x := msg.x + left; msg.y := msg.y + top;
			всё;
			HandleInternal^(msg);
		кон HandleInternal;

		проц {перекрыта}Draw*(canvas : WMGraphics.Canvas);
		перем canvasState: WMGraphics.CanvasState;
		нач
				canvas.SaveState(canvasState);
				canvas.SetDelta(canvas.dx - left, canvas.dy - top);
				DrawSubComponents(canvas);
				canvas.RestoreState(canvasState)
		кон Draw;

	кон ScrollPanel;

тип

	(** just shows an image, showing scrollbars if necessairy *)
	ScrollableContainer* = окласс(Panel)
	перем
		vScrollbar, hScrollbar : WMStandardComponents.Scrollbar;
		scrollPanel: ScrollPanel;
		dx, dy : размерМЗ;
		minNofLevels*, nofLevelsPerPage* : WMProperties.Int32Property;
		wheelScrolling- : WMProperties.BooleanProperty;

		проц & {перекрыта}Init*;
		нач
			Init^;
			SetGenerator("WMScrollableComponents.GenScrollableContainer");
			(* scrollbars *)
			нов(vScrollbar); vScrollbar.alignment.Set(WMComponents.AlignRight); AddInternalComponent^(vScrollbar);
			vScrollbar.onPositionChanged.Add(ScrollbarsChanged); vScrollbar.visible.Set(ложь);
			нов(hScrollbar); hScrollbar.alignment.Set(WMComponents.AlignBottom); AddInternalComponent^(hScrollbar);
			hScrollbar.vertical.Set(ложь); hScrollbar.onPositionChanged.Add(ScrollbarsChanged);
			hScrollbar.visible.Set(ложь);
			нов(scrollPanel, FitScrollTarget); scrollPanel.alignment.Set(WMComponents.AlignClient); AddInternalComponent^(scrollPanel);
			SetNameAsString(StrScrollableContainer);
			dx := 0;  dy := 0 ;
			нов(minNofLevels, PrototypeSCMinNofLevels, НУЛЬ, НУЛЬ); properties.Add(minNofLevels);
			нов(nofLevelsPerPage, PrototypeSCNofLevelsPerPage, НУЛЬ, НУЛЬ); properties.Add(nofLevelsPerPage);
			нов(wheelScrolling, PrototypeSCWheelScrolling, НУЛЬ, НУЛЬ); properties.Add(wheelScrolling);
		кон Init;

		проц {перекрыта}AlignSubComponents*;
		нач
			(* align scrollbars and scrollPanel first *)
			Acquire;
			если aligning то Release; возврат всё;
			AlignSubComponents^;
			(* the own bounds or client bounds may have changed *)
			aligning := истина;
			FitScrollTarget(НУЛЬ, НУЛЬ);
			aligning := ложь;
			Release;
		кон AlignSubComponents;

		проц {перекрыта}HandleInternal*(перем msg : WMMessages.Message);
		нач
			если wheelScrolling.Get() и (msg.msgType = WMMessages.MsgPointer) и (msg.msgSubType = WMMessages.MsgSubPointerMove) и (msg.dz # 0) то
				WheelMove(msg.dz);
				msg.dz := 0;
			всё;
			HandleInternal^(msg);
		кон HandleInternal;

		проц FitScrollTarget(sender, par: динамическиТипизированныйУкль);
		перем spw, sph, tw, th, sw, sh, w, h, rw, rh, nofLevels: размерМЗ;
		нач
			если (sequencer # НУЛЬ) и ~sequencer.IsCallFromSequencer() то
				sequencer.ScheduleEvent(FitScrollTarget, НУЛЬ, НУЛЬ)
			всё;
			если nofLevelsPerPage.Get() = 0 то возврат всё;
			утв(nofLevelsPerPage.Get() > 0);
			если (scrollPanel # НУЛЬ) то
				spw := scrollPanel.bounds.GetWidth(); sph := scrollPanel.bounds.GetHeight();
				tw := spw + scrollPanel.dx; th := sph + scrollPanel.dy;
				sw := vScrollbar.width.Get(); sh := hScrollbar.width.Get();
				w := bounds.GetWidth(); h := bounds.GetHeight();
				(* is hScrollbar visible ? *)
				если (tw > w) или ((th>h) и (tw>(w-sw))) то
					(* is vScrollbar visible ? *)
					если (th > (h-sh)) или (tw<=w) то rw := w - sw иначе rw := w всё;
					dx := tw- rw;
					hScrollbar.visible.Set(истина);
					если rw > 0 то
					nofLevels := матМаксимум(minNofLevels.Get(), nofLevelsPerPage.Get() * dx DIV rw);
					всё;
					hScrollbar.max.Set(nofLevels);
					(* hScrollbar.pageSize.Set(MAX(1, (rw * nofLevels) DIV dx)); *)
					hScrollbar.pageSize.Set(матМаксимум(1, (rw * nofLevels) DIV th) + 1);
					если (sequencer # НУЛЬ) и sequencer.IsCallFromSequencer() то
						hScrollbar.RecacheProperties; (* workaround because, InternalPropertyChanged is InUpdate *)
					всё;
				иначе
					dx := 0;
					hScrollbar.visible.Set(ложь);
				всё;
				(* is vScrollbar visible ? *)
				если (th > h) или ((tw>w) и (th>(h-sh))) то
					(* is hScrollbar visible ? *)
					если (tw > (w-sw)) или (th<=h)  то rh := h - sh иначе rh := h всё;
					dy := th - rh;
					vScrollbar.visible.Set(истина);
					если rh > 0 то
						nofLevels := матМаксимум(minNofLevels.Get(), nofLevelsPerPage.Get() * dy DIV rh)
					всё;
					vScrollbar.max.Set(nofLevels);
					(* vScrollbar.pageSize.Set(MAX(1, (rh * nofLevels) DIV dy)); *)
					vScrollbar.pageSize.Set(матМаксимум(1, (rh * nofLevels) DIV th) + 1);
					vScrollbar.RecacheProperties; (* workaround because, InternalPropertyChanged is InUpdate *)
				иначе
					dy := 0;
					vScrollbar.visible.Set(ложь);
				всё
			всё;
			если ~aligning то AlignSubComponents всё;
			Invalidate;
		кон FitScrollTarget;

		проц ScrollbarsChanged(sender, data : динамическиТипизированныйУкль);
		нач
			scrollPanel.SetLeftTop(hScrollbar.pos.Get() / (hScrollbar.max.Get() - hScrollbar.min.Get()),
				vScrollbar.pos.Get() / (vScrollbar.max.Get() - vScrollbar.min.Get()));
			Invalidate
		кон ScrollbarsChanged;

		проц {перекрыта}WheelMove*(dz : размерМЗ);
		конст Multiplier  = 3;
		перем pos : размерМЗ;
		нач
			WheelMove^(dz);
			если vScrollbar.visible.Get() то
				pos := vScrollbar.pos.Get() + Multiplier * dz;
				если pos < vScrollbar.min.Get() то pos := vScrollbar.min.Get(); всё;
				если pos > vScrollbar.max.Get() то pos := vScrollbar.max.Get(); всё;
				vScrollbar.pos.Set(pos);
				ScrollbarsChanged(НУЛЬ, НУЛЬ);
			всё;
		кон WheelMove;

		проц {перекрыта}AddInternalComponent*(component : WMComponents.Component);
		нач
			scrollPanel.AddInternalComponent(component);
		кон AddInternalComponent;

		(** Iff data IS WMGraphics.Image, it is set as background. Else the background is set to white *)
		(* Note: Only use for anonymous Images without a specific Name *)
		проц {перекрыта}AddContent*(content : XML.Content);
		нач
			если (content суть WMProperties.Properties) или (content = vScrollbar) или (content = hScrollbar) или (content = scrollPanel) то
				AddContent^(content);
			иначе
				scrollPanel.AddContent(content);
			всё;
		кон AddContent;

	кон ScrollableContainer;

перем

	Int32Prototype : WMProperties.Int32Property;

	(* Scrollable Container prototypes *)
	PrototypeSCMinNofLevels*, PrototypeSCNofLevelsPerPage*: WMProperties.Int32Property;
	PrototypeSCWheelScrolling : WMProperties.BooleanProperty;
	StrScrollPanel, StrScrollableContainer : String;

проц InitStrings;
нач
	StrScrollableContainer := Strings.NewString("ScrollableContainer");
	StrScrollPanel := Strings.NewString("ScrollPanel");
кон InitStrings;

проц InitPrototypes;
перем
	plScrollableContainer : WMProperties.PropertyList;
нач
	(* ScrollablePanel prototypes *)
	нов(plScrollableContainer); WMComponents.propertyListList.Add("Scrollable Container", plScrollableContainer);
	нов(Int32Prototype, НУЛЬ, NewString("MinNofLevels"), NewString("")); Int32Prototype.Set(8);
	нов(PrototypeSCMinNofLevels, Int32Prototype, НУЛЬ, НУЛЬ); plScrollableContainer.Add(PrototypeSCMinNofLevels);
	нов(Int32Prototype, НУЛЬ, NewString("NofLevelsPerPage"), NewString("")); Int32Prototype.Set(8);
	нов(PrototypeSCNofLevelsPerPage, Int32Prototype, НУЛЬ, НУЛЬ); plScrollableContainer.Add(PrototypeSCNofLevelsPerPage);
	нов(PrototypeSCWheelScrolling, НУЛЬ, NewString("WheelScrolling"), NewString("Mouse wheel scrolling?"));
	PrototypeSCWheelScrolling.Set(истина);
	WMComponents.propertyListList.UpdateStyle
кон InitPrototypes;

проц GenScrollableContainer*() : XML.Element;
перем scrollCont: ScrollableContainer;
нач нов(scrollCont); возврат scrollCont
кон GenScrollableContainer;

проц NewString(конст x : массив из симв8) : String;
перем t : String;
нач
	нов(t, длинаМассива(x)); копируйСтрокуДо0(x, t^); возврат t
кон NewString;

нач
	InitStrings;
	InitPrototypes;
кон WMScrollableComponents.

System.Free WMScrollableComponents~
