(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Network; (** AUTHOR "pjm, mvt"; PURPOSE "Abstract network device driver"; *)

использует НИЗКОУР, WSock32, ЭВМ, ЛогЯдра, Plugins, Kernel, Objects, Modules;
	перем

конст
	MaxLinkAdrSize* = 8; (** largest link address size in bytes *)
	MaxPacketSize* = 1600; (** maximum amount of data bytes in a link layer frame *)
	MaxNofBuffers = 10000; (* maximum number of buffers allowed within the whole net system *)

	(** Constants for LinkDevice.type *)
	TypePointToPoint* = 0;
	TypeEthernet* = 1;

	(** Constants for LinkDevice.Linked *)
	LinkNotLinked* = 0;
	LinkLinked* = 1;
	LinkUnknown* = 2;

	(** Constants for LinkDevice.calcChecksum and Buffer.calcChecksum *)
	ChecksumIP* = 0;
	ChecksumUDP* = 1;
	ChecksumTCP* = 2;

	(* Number of loopback packets that can be sent per 1-2 ms.
		This protects the upcall buffers from running out. *)
	MaxLoopbackPacketsPerMS = 500;

тип
	LinkAdr* = массив MaxLinkAdrSize из симв8; (** link layer address *)

	(** Buffer for passing network packets to upper layer protocols *)
	Buffer* = укль на запись
		data*: массив MaxPacketSize из симв8;
		ofs*: цел32; (** valid data starts at this offset *)
		len*: цел32; (** length of valid data *)
		l3ofs*: цел32; (** the layer 3 header starts at this offset *)
		l4ofs*: цел32; (** the layer 4 header starts at this offset *)
		src*: LinkAdr; (** link layer source address *)
		calcChecksum*: мнвоНаБитахМЗ; (** these checksums are already verified by the device *)
		int*: цел32; (** used in TCP, UDP and ICMP, but can be used by any upper layer protocol *)
		set*: мнвоНаБитахМЗ; (** used in TCP, but can be used by any upper layer protocol *)
		next*, prev*: Buffer; (** for queueing the buffer *)
	кон;

	ReceiverList = укль на запись
		next: ReceiverList;
		type: цел32;
		receiver: Receiver;
	кон;

	SendSnifferList = укль на запись
		next: SendSnifferList;
		sniffer: SendSniffer;
	кон;

	RecvSnifferList = укль на запись
		next: RecvSnifferList;
		sniffer: ReceiveSniffer;
	кон;

	(** Abstract implementation of a generic network driver object *)

	LinkDevice* = окласс (Plugins.Plugin)
		перем
			(** pubic device properties *)
			type-: цел32; (** LinkType: TypePointToPoint, TypeEthernet *)
			local-: LinkAdr; (** local link address *)
			broadcast-: LinkAdr; (** link address for sending a broadcast *)
			mtu-: цел32; (** largest packet size in bytes *)
			adrSize-: цел32; (** link address size in bytes *)
			sendCount-, recvCount-: цел64; (** number of bytes sent and received *)
			calcChecksum-: мнвоНаБитахМЗ; (** these checksums are calculated by the device hardware when sending. *)

			recList: ReceiverList; (* receiver list *)
			sendSnifferList: SendSnifferList; (* list for send sniffers *)
			recvSnifferList: RecvSnifferList; (* list for receive sniffers *)

			item: ReceiverList; (* temporary item in receiver list *)
			sniffer: RecvSnifferList; (* temporary item in receive sniffer list *)

			discard: булево; (* shall the current packet be discarded? (used in active body) *)
			finalized: булево; (* is object already finalized or currently finalizing? *)

			(* queue for buffers waiting for upcall *)
			upBufFirst, upBufLast: Buffer;
			buf: Buffer; (* temporary buffer for active body *)

			(* timer and packet count for loopback bandwidth control *)
			timer: Kernel.MilliTimer;
			packetCount: цел32;

		(** Constructor - Initialize the driver and the device.
			NOTE:
			Is normally overridden by device driver. If so, this constructor has to be called at the beginning
			of the overriding constructor!
		*)
		проц &Constr*(type, mtu, adrSize: цел32);
		перем res: целМЗ;
		нач
			утв((mtu >= 0) и (mtu <= MaxPacketSize));
			утв((adrSize >= 0) и (adrSize <= MaxLinkAdrSize));
			если type = TypeEthernet то
				утв(adrSize = 6);
			всё;
			сам.type := type;
			сам.mtu := mtu;
			сам.adrSize := adrSize;
			сам.sendCount := 0;
			сам.recvCount := 0;
			сам.calcChecksum := {};

			recList := НУЛЬ;
			upBufFirst := НУЛЬ;

			Kernel.SetTimer(timer, 2);
			packetCount := 0;

			finalized := ложь;

			sendSnifferList := НУЛЬ;
			recvSnifferList := НУЛЬ;
		кон Constr;

		(** Destructor - Finalize driver object. If connected = TRUE, device is still connected and has to be deinitialized.
			NOTE:
			Is normally overridden by device driver. If so, this method has to be called at the end
			of the overriding method!
		*)
		проц Finalize*(connected: булево);
		нач {единолично}
			утв(~finalized);
			finalized := истина;
		кон Finalize;

		(** Return the link status of the device.
			This function has to be overridden by the device driver in order to provide this information.
		*)
		проц Linked*(): цел32;
		нач
			возврат LinkUnknown;
		кон Linked;

		(** Send a packet. Called by its user. Can be called concurrently. *)

		проц Send*(dst: LinkAdr; type: цел32; перем l3hdr, l4hdr, data: массив из симв8; h3len, h4len, dofs, dlen: цел32; loopback: булево);
		перем
			sniffer: SendSnifferList;
			discard: булево; (* shall the packet be discarded? *)
		нач (* can run concurrently with InstallSendSniffer and RemoveSendSniffer *)
			утв(~finalized);
			discard := ложь;
			sniffer := sendSnifferList;
			нцПока sniffer # НУЛЬ делай
				(* call sniffer *)
				discard := discard или sniffer^.sniffer(сам, dst, type, l3hdr, l4hdr, data, h3len, h4len, dofs, dlen);
				sniffer := sniffer^.next;
			кц;
			если ~discard то
				(* send the packet *)
				если loopback то
					Loopback(dst, type, l3hdr, l4hdr, data, h3len, h4len, dofs, dlen);
				иначе
					DoSend(dst, type, l3hdr, l4hdr, data, h3len, h4len, dofs, dlen);
				всё;
				увел(sendCount, dlen + h3len + h4len);
			всё;
		кон Send;

		(** Do frame send operation. Must be overridden and implemented by device driver! *)
		(** Must be able to handle concurrent calls. e.g. by declaring itself as EXCLUSIVE! *)

		проц DoSend*(dst: LinkAdr; type: цел32; перем l3hdr, l4hdr, data: массив из симв8; h3len, h4len, dofs, dlen: цел32);
		нач
			СТОП(301); (* Abstract! *)
		кон DoSend;

		(* Do internal loopback. Send packet directly to the receive queue. *)

		проц Loopback(dst: LinkAdr; type: цел32; перем l3hdr, l4hdr, data: массив из симв8; h3len, h4len, dofs, dlen: цел32);
		перем buf: Buffer;
		нач
			если packetCount >= MaxLoopbackPacketsPerMS то
				нцПока ~Kernel.Expired(timer) делай
					(* no more packets can be sent until timer is expired *)
					Objects.Yield();
				кц;
				Kernel.SetTimer(timer, 2);
				packetCount := 0;
			всё;

			buf := GetNewBuffer();
			если buf # НУЛЬ то
				buf.l3ofs := 0;
				buf.l4ofs := 0;
				buf.ofs := 0;
				buf.len := 0;
				buf.src := dst;
				buf.calcChecksum := {ChecksumIP, ChecksumUDP, ChecksumTCP};

				(* Copy data to receive buffer *)
				Copy(l3hdr, buf.data, 0, buf.len, h3len);
				увел(buf.len, h3len);
				Copy(l4hdr, buf.data, 0, buf.len, h4len);
				увел(buf.len, h4len);
				Copy(data, buf.data, dofs, buf.len, dlen);
				увел(buf.len, dlen);

				(* Queue the receive buffer *)
				QueueBuffer(buf, type);
				ЭВМ.атомарноУвел(packetCount)
			иначе (* packet loss in loopback :o *)

			всё
		кон Loopback;

		(** Install a receiver for the given type. Only one receiver can be installed per type! *)
		проц InstallReceiver*(type: цел32; r: Receiver);
		перем item: ReceiverList;
		нач {единолично}
			(* can run concurrently with active body *)
			утв(~finalized);
			утв(r # НУЛЬ);

			(* test if there is already a receiver installed for this type *)
			item := recList;
			нцПока item # НУЛЬ делай
				утв(item^.type # type);
				item := item^.next;
			кц;

			(* create new entry *)
			нов(item);
			item^.type := type;
			item^.receiver := r;
			item^.next := recList;
			recList := item;
		кон InstallReceiver;

		(** Remove the currently installed receiver for the given type. *)
		проц RemoveReceiver*(type: цел32);
		перем item: ReceiverList;
		нач {единолично}
			(* can run concurrently with active body *)
			утв(~finalized);

			(* remove receiver *)
			если recList = НУЛЬ то
				(* empty list - nothing to remove *)
			аесли recList^.type = type то
				(* remove first item *)
				recList := recList^.next;
			иначе
				(* search list *)
				item := recList;
				нцПока (item^.next # НУЛЬ) и (item^.next^.type # type) делай
					item := item^.next;
				кц;
				если item^.next # НУЛЬ то
					item^.next := item^.next^.next;
				иначе
					(* no receiver found for this type *)
				всё;
			всё;
		кон RemoveReceiver;

		(** Install a sniffer for sent packets *)
		проц InstallSendSniffer*(s: SendSniffer);
		перем item: SendSnifferList;
		нач {единолично}
			утв(~finalized);
			item := sendSnifferList;
			нцПока (item # НУЛЬ) и (item^.sniffer # s) делай
				item := item^.next;
			кц;
			если item # НУЛЬ то
				(* sniffer already registered *)
			иначе
				нов(item);
				item^.sniffer := s;
				item^.next := sendSnifferList;
				sendSnifferList := item;
			всё;
		кон InstallSendSniffer;

		(** Remove a sniffer for sent packets *)
		проц RemoveSendSniffer*(s: SendSniffer);
		перем item: SendSnifferList;
		нач {единолично}
			утв(~finalized);
			если sendSnifferList = НУЛЬ то
				(* empty list *)
			аесли sendSnifferList^.sniffer = s то
				(* remove first item *)
				sendSnifferList := sendSnifferList^.next;
			иначе
				(* search list *)
				item := sendSnifferList;
				нцПока (item^.next # НУЛЬ) и (item^.next^.sniffer # s) делай
					item := item^.next;
				кц;
				если item^.next # НУЛЬ то
					item^.next := item^.next^.next;
				иначе
					(* sniffer not found *)
				всё;
			всё;
		кон RemoveSendSniffer;

		(** Install a sniffer for received packets *)
		проц InstallReceiveSniffer*(s: ReceiveSniffer);
		перем item: RecvSnifferList;
		нач {единолично}
			утв(~finalized);
			item := recvSnifferList;
			нцПока (item # НУЛЬ) и (item^.sniffer # s) делай
				item := item^.next;
			кц;
			если item # НУЛЬ то
				(* sniffer already registered *)
			иначе
				нов(item);
				item^.sniffer := s;
				item^.next := recvSnifferList;
				recvSnifferList := item;
			всё;
		кон InstallReceiveSniffer;

		(** Remove a sniffer for received packets *)
		проц RemoveReceiveSniffer*(s: ReceiveSniffer);
		перем item: RecvSnifferList;
		нач {единолично}
			утв(~finalized);
			если recvSnifferList = НУЛЬ то
				(* empty list *)
			аесли recvSnifferList^.sniffer = s то
				(* remove first item *)
				recvSnifferList := recvSnifferList^.next;
			иначе
				(* search list *)
				item := recvSnifferList;
				нцПока (item^.next # НУЛЬ) и (item^.next^.sniffer # s) делай
					item := item^.next;
				кц;
				если item^.next # НУЛЬ то
					item^.next := item^.next^.next;
				иначе
					(* sniffer not found *)
				всё;
			всё;
		кон RemoveReceiveSniffer;

		(** Queue buffer for upcall. Called from inside the LinkDevice object, normally from the interrupt handler. *)
		проц QueueBuffer*(buf: Buffer; type: цел32);
		нач {единолично}
			утв(buf # НУЛЬ);
			buf.int := type; (* use "int" field for type information *)
			buf.next := НУЛЬ;
			если upBufFirst = НУЛЬ то
				upBufFirst := buf;
			иначе
				upBufLast.next := buf;
			всё;
			upBufLast := buf;
		кон QueueBuffer;

	нач {активное, приоритет(Objects.High)}
		(* can run concurrently with SetReceiver, QueueBuffer, InstallReceiverSniffer and RemoveReceiverSniffer *)
		нц
			нач {единолично}
				дождись((upBufFirst # НУЛЬ) или finalized);
				если (upBufFirst = НУЛЬ) и finalized то
					(* terminate process after all buffer upcalls are done *)
					прервиЦикл;
				всё;
				buf := upBufFirst;
				upBufFirst := upBufFirst.next;
			кон;
			увел(recvCount, buf.len);
			discard := ложь;
			sniffer := recvSnifferList;
			нцПока sniffer # НУЛЬ делай
				(* call sniffer *)
				discard := discard или sniffer^.sniffer(сам, buf.int, buf);
				sniffer := sniffer^.next;
			кц;
			если ~discard то
				(* search for receiver *)
				item := recList;
				нцПока (item # НУЛЬ) и (item^.type # buf.int) делай
					item := item^.next;
				кц;
				discard := (item = НУЛЬ);
			всё;
			если discard то
				(* discard packet and return buffer *)
				ReturnBuffer(buf);
			иначе
				(* do upcall *)
				item^.receiver(сам, item^.type, buf);
			всё;
		кц;
	кон LinkDevice;

тип
	(** Upcall procedures *)

	(** Packet receiver upcall
		CAUTION:
		After the buffer has been used, it has to be returned by calling Network.ReturnBuffer(buffer)!
		The Receiver can do this by itself or delegate this job to other procedures or processes, wherever the
		buffer is passed to. It has not necessarily to be returned within the receiver upcall.
	*)
	Receiver* = проц {делегат} (dev: LinkDevice; type: цел32; buffer: Buffer);

	(* Sniffer for sent packets. May modify type, headers and data. Return TRUE if packet shall be discarded. *)
	(* Must be able to handle concurrent calls. e.g. by declaring itself as EXCLUSIVE. *)
	SendSniffer* = проц {делегат} (dev: LinkDevice; перем dst: LinkAdr; перем type: цел32; перем l3hdr, l4hdr, data: массив из симв8; перем h3len, h4len, dofs, dlen: цел32): булево;

	(* Sniffer for received packets. May modify type and buffer. Return TRUE if packet shall be discarded. *)
	(* Will never be called concurrenty from the same LinkDevice. *)
	ReceiveSniffer* = проц {делегат} (dev: LinkDevice; перем type: цел32; buffer: Buffer): булево;

(** Module variables *)

перем
	registry*: Plugins.Registry;

	nofBuf: цел32; (* number of buffers existing *)
	nofFreeBuf: цел32; (* number of free buffers *)
	freeBufList: Buffer; (* free buffer list *)

(** Get a new buffer - return NIL if MaxNofBuffers is exceeded *)

проц GetNewBuffer*(): Buffer;
перем item: Buffer;
нач {единолично}
	если freeBufList # НУЛЬ то
		(* free buffer is available *)
		item := freeBufList;
		freeBufList := freeBufList.next;
		ЭВМ.атомарноУвелНа(nofFreeBuf, -1);
	аесли nofBuf < MaxNofBuffers то
		(* no free buffer available - create new one *)
		нов(item);
		ЭВМ.атомарноУвел(nofBuf);
	иначе
		(* not allowed to create more buffers *)
		item := НУЛЬ;
	всё;
	возврат item;
кон GetNewBuffer;

(** Return a buffer to be reused *)

проц ReturnBuffer*(buf: Buffer);
нач {единолично}
	утв(buf # НУЛЬ);
	buf.next := freeBufList;
	freeBufList := buf;
	ЭВМ.атомарноУвел(nofFreeBuf);
кон ReturnBuffer;

(* Passed to registry.Enumerate() to Finalize each registered LinkDevice *)

проц Finalize(p: Plugins.Plugin);
нач
	p(LinkDevice).Finalize(истина);
кон Finalize;

(** Test whether the n bytes of buf1 and buf2 starting at ofs1 and ofs2 respectively are equal *)

проц Equal*(перем buf1, buf2: массив из симв8; ofs1, ofs2, n: цел32): булево;
нач
	нцПока (n > 0) и (buf1[ofs1] = buf2[ofs2]) делай увел(ofs1); увел(ofs2); умень(n) кц;
	возврат n <= 0
кон Equal;

(** Procedures to put and get data from and to arrays. No index checks are done due to performance! *)

(** Put a 32-bit host value into buf[ofs..ofs+3] *)

проц Put4*(перем buf: массив из симв8; ofs: размерМЗ; val: целМЗ);
машКод
#если I386 то
	MOV EAX, [EBP+val]
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	MOV [ECX+EBX], EAX
#аесли AMD64 то
	MOV EAX, [RBP+val]
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	MOV [RCX+RBX], EAX
#иначе
	unimplemented
#кон
кон Put4;

(** Put a 16-bit host value into buf[ofs..ofs+1] *)

проц Put2*(перем buf: массив из симв8; ofs: размерМЗ; val: целМЗ);
машКод
#если I386 то
	MOV EAX, [EBP+val]
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	MOV [ECX+EBX], AX
#аесли AMD64 то
	MOV EAX, [RBP+val]
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	MOV [RCX+RBX], AX
#иначе
	unimplemented
#кон
кон Put2;

(** Get a 32-bit host value from buf[ofs..ofs+3] *)

проц Get4*(конст buf: массив из симв8; ofs: размерМЗ): целМЗ;
машКод
#если I386 то
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	MOV EAX, [ECX+EBX]
#аесли AMD64 то
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	MOV EAX, [RCX+RBX]
#иначе
	unimplemented
#кон
кон Get4;

(** Get a 16-bit host value from buf[ofs..ofs+1] *)

проц Get2*(конст buf: массив из симв8; ofs: размерМЗ): целМЗ;
машКод
#если I386 то
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	XOR EAX, EAX
	MOV AX, [ECX+EBX]
#аесли AMD64 то
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	XOR EAX, EAX
	MOV AX, [RCX+RBX]
#иначе
	unimplemented
#кон
кон Get2;

(** Put a 32-bit host value into buf[ofs..ofs+3] in network byte order *)

проц PutNet4*(перем buf: массив из симв8; ofs: размерМЗ; val: целМЗ);
машКод
#если I386 то
	MOV EAX, [EBP+val]
	XCHG AL, AH
	ROL EAX, 16
	XCHG AL, AH
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	MOV [ECX+EBX], EAX
#аесли AMD64 то
	MOV EAX, [RBP+val]
	XCHG AL, AH
	ROL EAX, 16
	XCHG AL, AH
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	MOV [RCX+RBX], EAX
#иначе
	unimplemented
#кон
кон PutNet4;

(** Put a 16-bit host value into buf[ofs..ofs+1] in network byte order *)

проц PutNet2*(перем buf: массив из симв8; ofs: размерМЗ; val: целМЗ);
машКод
#если I386 то
	MOV EAX, [EBP+val]
	XCHG AL, AH
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	MOV [ECX+EBX], AX
#аесли AMD64 то
	MOV EAX, [RBP+val]
	XCHG AL, AH
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	MOV [RCX+RBX], AX
#иначе
	unimplemented
#кон
кон PutNet2;

(** Get a 32-bit network value from buf[ofs..ofs+3] in host byte order *)

проц GetNet4*(конст buf: массив из симв8; ofs: размерМЗ): целМЗ;
машКод
#если I386 то
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	MOV EAX, [ECX+EBX]
	XCHG AL, AH
	ROL EAX, 16
	XCHG AL, AH
#аесли AMD64 то
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	MOV EAX, [RCX+RBX]
	XCHG AL, AH
	ROL EAX, 16
	XCHG AL, AH
#иначе
	unimplemented
#кон
кон GetNet4;

(** Get a 16-bit network value from buf[ofs..ofs+1] in host byte order *)

проц GetNet2*(конст buf: массив из симв8; ofs: размерМЗ): целМЗ;
машКод
#если I386 то
	MOV EBX, [EBP+ofs]
	MOV ECX, [EBP+buf]
	XOR EAX, EAX
	MOV AX, [ECX+EBX]
	XCHG AL, AH
#аесли AMD64 то
	MOV RBX, [RBP+ofs]
	MOV RCX, [RBP+buf]
	XOR EAX, EAX
	MOV AX, [RCX+RBX]
	XCHG AL, AH
#иначе
	unimplemented
#кон
кон GetNet2;

(** Convert a LinkAdr to a printable string (up to size*3 characters) *)

проц LinkAdrToStr*(перем adr: LinkAdr; size: размерМЗ; перем s: массив из симв8);
перем
	i, j: размерМЗ;
	hex: массив 17 из симв8;
нач
	утв(длинаМассива(s) >= size*3); (* enough space for largest result *)
	hex := "0123456789ABCDEF";
	i := 0;
	нцДля j := 0 до size-1 делай
		s[i] := hex[кодСимв8(adr[j]) DIV 10H остОтДеленияНа 10H]; увел(i);
		s[i] := hex[кодСимв8(adr[j]) остОтДеленияНа 10H]; увел(i);
		если j = size-1 то s[i] := 0X иначе s[i] := ":" всё;
		увел(i);
	кц;
кон LinkAdrToStr;

(** Write a link address *)

проц OutLinkAdr*(перем adr: LinkAdr; size: размерМЗ);
перем s: массив MaxLinkAdrSize*3 из симв8;
нач
	LinkAdrToStr(adr, size, s);
	ЛогЯдра.пСтроку8(s);
кон OutLinkAdr;

(** Copy data from array to array *)
проц Copy*(конст from: массив из симв8; перем to: массив из симв8; fofs, tofs, len: размерМЗ);
нач
	если len > 0 то
		утв((fofs+len <= длинаМассива(from)) и (tofs+len <= длинаМассива(to)));
		НИЗКОУР.копируйПамять(адресОт(from[fofs]), адресОт(to[tofs]), len);
	всё;
кон Copy;

проц Cleanup;
нач
	registry.Enumerate(Finalize);
	Plugins.main.Remove(registry);
	WSock32.CleanUp;
кон Cleanup;

нач
	nofBuf := 0;
	nofFreeBuf := 0;

	нов(registry, "Network", "Network interface drivers");
	WSock32.Startup;

	Modules.InstallTermHandler(Cleanup);
кон Network.

(*
History:
10.10.2003	mvt	Complete redesign and additional implementation of buffer handling and upcall mechanism
17.10.2003	mvt	Changed the way of initialization and finalization (now only Constr/Finalize)
21.10.2003	mvt	Changed SetReceiver to InstallReceiver and RemoveReceiver
15.11.2003	mvt	Changed buffering to work with EXCLUSIVE sections instead of using locking and a semaphore.
16.11.2003	mvt	Added support for checksum calclulation by the device.
25.11.2003	mvt	Added l3ofs and l4ofs to Buffer type.
17.12.2003	mvt	Changed variable "linked" to method "Linked".
*)

(**
How to use the module:

The module is loaded as soon as it is used first. It needn't to be loaded explicitly at startup. It can also be unloaded an reloaded without reboot.

How to use a driver:

Network driver objects in Bluebottle are extensions of the Network.LinkDevice object. All loaded instances of network driver objects are registered in the registry Network.registry. To obtain access to a network driver, use the Get, Await or GetAll methods of this registry.

Example:
	VAR dev: Network.LinkDevice;
	dev := Network.registry.Get(""); (* if no name is specified, first device (or NIL) is returned *)

The Send method of LinkDevice is used to send a packet. The dst parameter specifies the link destination address (e.g., a 6-byte ethernet MAC address for an ethernet device). The type parameter specifies the link-layer protocol type (e.g. 800H when sending IP over ethernet). The source address of the packet is automatically generated by the device, if necessary.
For reasons of reducing buffer copying between network layers, the method allows 3 buffers to be passed:
The l3hdr, l4hdr, data, h3len, h4len, dofs and dlen fields specify 3 buffers:
One buffer for a layer 3 header, one for a layer 4 header and one for the payload of the packet. The buffers don't have to be filled like this. They are simply concatenated to one frame by the device driver. Therefore, each of them is allowed to be empty.

The layer 3 header is stored in: l3hdr[0..h3len-1]
The layer 4 header is stored in: l4hdr[0..h4len-1]
The payload is stored in data[dofs..dofs+dlen-1]

Example:
	CONST
		Type = 05555H;	(* link-layer protocol type *)
	VAR
		dlen: SIGNED32;
		l3hdr: ARRAY HdrLen OF CHAR;
		data: ARRAY MaxDataLen OF CHAR;

	(* - l3hdr[0..HdrLen-1] contains the layer 3 packet header.
		- data[0..dlen-1] contains the packet data.
		- there is no layer 4 header in this example, i.e. an empty buffer is passed (len=0)
	*)
	dev.Send(dev.broadcast, Type, l3hdr, l3hdr, data, HdrLen, 0, 0, dlen); (* send a broadcast packet *)

Packet receiving is driven by the driver object. A receiver interested in a specific type of packet registers itself using the InstallReceiver method. The type parameter specifies the link-layer protocol type, which must be unique. There can only be one receiver installed per type. When a packet arrives, the driver object looks at the protocol type and calls the specific receiver (if any is installed for this type).

Example:
	PROCEDURE Receiver(dev: Network.LinkDevice; type: SIGNED32; buffer: Network.Buffer);
	BEGIN
		ASSERT(type = Type);
		CheckAdr(buffer.src); (* some link layer source address checks *)
		IF ~(ChecksumForThisProtocol IN buffer.calcChecksum) THEN
			VerifyChecksum(buffer);
		END;
		ExamineLayer3Header(buffer.data, buffer.ofs, Layer3HeaderSize);
		ProcessPayload(buffer.data, buffer.ofs+Layer3HeaderSize, buffer.len-Header3LayerSize);

		(* MANDATORY!!
			Buffer must be returned here! - or at higher layers, if the buffer is passed there! *)
		Network.ReturnBuffer(buffer);
	END Receiver;

	dev.InstallReceiver(Type, Receiver); (* install the receiver *)

When passing the buffer to a higher layer (e.g. layer 4), the field "l3ofs" should be set in order to enable the higher layer protocol to access this layer's header (required by ICMP).
The same is valid for the field "l4ofs" when passing the buffer to a higher layer than 4.

The "type" field of a LinkDevice specifies what kind of device it is. Currently, two options (constants) are available:
- TypePointToPoint: dev is a point-to-point link (device), e.g. PPP
- TypeEthernet: dev is an ethernet device

For point-to-point links, the following rules are met:
In constructor, the "local" and "broadcast" parameters are ignored.
If it is not possible to transmit the layer 3 type of packet, the type is set to 0 for the received packet.
If the field "adrSize" is 0, the dst address parameter in Send() is ignored and the src address parameter in the Receiver is not defined.
If "adrSize" is > 0, the dst address passed in Send() is transmitted and presented as src address for the received packet.

The "local" and "broadcast" fields of the object specify the link-level address of the device, and the broadcast address, respectively. If needed, they have to be set during device initialization.
The mtu field specifies the largest allowed packet size in bytes, excluding layer 2 headers and trailers (e.g. 1500 for ethernet).

The "sendCount" and "recvCount" fields show the number of data bytes sent and received by the device since the driver object was loaded.

How to implement a driver:

IMPORTANT:
- Read first "How to use a driver"!
- Read comments of methods you override!

A network driver object is implemented by extending the LinkDevice object. At least the "DoSend" method has to be overridden with a concrete implementation. Normally, you will also override the constructor, the destructor (method "Finalize") and the method "Linked".

CAUTION:
If you override the constructor or destructor:
- At the beginning of the overriding constructor, the overridden constructor has to be called!
- At the end of the overriding destructor, the overridden destructor has to be called!

NOTE:
The device has to be registered and deregistered as a Plugin in Network.registry by the device driver as follows:
Add the device to the registry after it is ready to send/receive data!
Remove the device from the registry before you begin to deinitialize the device!

Normally, the device driver will have to install an interrupt handler for receiving packets. This handler must be installed in Objects. Interrupts registered in Machine are not allowed! (because they are not allowed to call QueueBuffer due to its EXCLUSIVE section)

If you use interrupts:
- do the minimum operations required for receiving and queueing a packet!
- return immediately after having done the operations!

Receiving and queueing packets is done like this:

When you are notified of an incomming network packet (normally by an interrupt), get a new buffer by calling:
buf := Network.GetNewBuffer();

If buf = NIL, the packet has to be discarded because all buffers are currently in use and the maximum amount of buffers (MaxNofBuffers) is exceeded.
If buf # NIL (the normal case), read the packet data into buf.data and set buf.ofs and buf.len accordingly.
The buffer is not initialized in any way.

If the device supports DMA, you could try to get the buffers earlier and pass their physical addesses to the device. With this you can save one packet copying operation!

In addition to the fields "data", "ofs", "len" of "buffer", it is mandatory to set the fields "src" and "calcChecksum" correctly. "src" is the link layer source address and "calcChecksum" is the set of checksums already verified by the device (normally {}).

As soon as the buffer contains the whole packet data, it is passed to the upper layers by calling:
QueueBuffer(buf, type);
Where "type" is the layer 3 type of the packet.

See TestNet.Mod (SendBroadcast, SendTest and Receiver) and IP.Mod (ARPRequest and ARPInput) for an example of packet sending and receiving. See Ethernet3Com90x.Mod for an example of an ethernet driver object and Loopback.Mod for a simple point-to-point link implementation.

*)
