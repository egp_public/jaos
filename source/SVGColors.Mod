модуль SVGColors;

использует Strings, SVGUtilities, Raster;

(* Constants defining colors in the format 0RRGGBBAAH *)
конст
	Aliceblue*				= цел32(0F0F8FFFFH);
	Antiquewhite*			= цел32(0FAEBD7FFH);
	Aqua*					= 000FFFFFFH;
	Aquamarine*			= 07FFFD4FFH;
	Azure*					= цел32(0F0FFFFFFH);
	Beige*					= цел32(0F5F5DCFFH);
	Bisque*					= цел32(0FFE4C4FFH);
	Black*					= 0000000FFH;
	Blanchedalmond*		= цел32(0FFEBCDFFH);
	Blue*					= 00000FFFFH;
	Blueviolet*				= цел32(08A2BE2FFH);
	Brown*					= цел32(0A52A2AFFH);
	Burlywood*				= цел32(0DEB887FFH);
	Cadetblue*				= 05F9EA0FFH;
	Chartreuse*				= 07FFF00FFH;
	Chocolate*				= цел32(0D2691EFFH);
	Coral*					= цел32(0FF7F50FFH);
	Cornflowerblue*		= 06495EDFFH;
	Cornsilk*				= цел32(0FFF8DCFFH);
	Crimson*				= цел32(0DC143CFFH);
	Cyan*					= 000FFFFFFH;
	Darkblue*				= 000008BFFH;
	Darkcyan*				= 0008B8BFFH;
	Darkgoldenrod*			= 0008B8BFFH;
	Darkgray*				= цел32(0A9A9A9FFH);
	Darkgreen*				= 0006400FFH;
	Darkgrey*				= цел32(0A9A9A9FFH);
	Darkkhaki*				= цел32(0BDB76BFFH);
	Darkmagenta*			= цел32(08B008BFFH);
	Darkolivegreen*		= 0556B2FFFH;
	Darkorange*			= цел32(0FF8C00FFH);
	Darkorchid*				= цел32(09932CCFFH);
	Darkred*				= цел32(08B0000FFH);
	Darksalmon*			= цел32(0E9967AFFH);
	Darkseagreen*			= цел32(08FBC8FFFH);
	Darkslateblue*			= 0483D8BFFH;
	Darkslategray*			= 02F4F4FFFH;
	Darkslategrey*			= 02F4F4FFFH;
	Darkturquoise*			= 000CED1FFH;
	Darkviolet*				= цел32(09400D3FFH);
	Deeppink*				= цел32(0FF1493FFH);
	Deepskyblue*			= 000BFFFFFH;
	Dimgray*				= 0696969FFH;
	Dimgrey*				= 0696969FFH;
	Dodgerblue*			= 01E90FFFFH;
	Firebrick*				= цел32(0B22222FFH);
	Floralwhite*			= цел32(0FFFAF0FFH);
	Forestgreen*			= 0228B22FFH;
	Fuchsia*				= цел32(0FF00FFFFH);
	Gainsboro*				= цел32(0DCDCDCFFH);
	Ghostwhite*			= цел32(0F8F8FFFFH);
	Gold*					= цел32(0FFD700FFH);
	Goldenrod*				= цел32(0DAA520FFH);
	Gray*					= цел32(0808080FFH);
	Grey*					= цел32(0808080FFH);
	Green*					= 0008000FFH;
	Greenyellow*			= цел32(0ADFF2FFFH);
	Honeydew*				= цел32(0F0FFF0FFH);
	Hotpink*				= цел32(0FF69B4FFH);
	Indianred*				= цел32(0CD5C5CFFH);
	Indigo*					= 04B0082FFH;
	Ivory*					= цел32(0FFFFF0FFH);
	Khaki*					= цел32(0F0E68CFFH);
	Lavender*				= цел32(0E6E6FAFFH);
	Lavenderblush*			= цел32(0FFF0F5FFH);
	Lawngreen*			= 07CFC00FFH;
	Lemonchiffon*			= цел32(0FFFACDFFH);
	Lightblue*				= цел32(0ADD8E6FFH);
	Lightcoral*				= цел32(0F08080FFH);
	Lightcyan*				= цел32(0E0FFFFFFH);
	Lightgoldenrodyellow*	= цел32(0FAFAD2FFH);
	Lightgray*				= цел32(0D3D3D3FFH);
	Lightgreen*				= цел32(090EE90FFH);
	Lightgrey*				= цел32(0D3D3D3FFH);
	Lightpink*				= цел32(0FFB6C1FFH);
	Lightsalmon*			= цел32(0FFA07AFFH);
	Lightseagreen*			= 020B2AAFFH;
	Lightskyblue*			= цел32(087CEFAFFH);
	Lightslategray*			= 0778899FFH;
	Lightslategrey*			= 0778899FFH;
	Lightsteelblue*			= цел32(0B0C4DEFFH);
	Lightyellow*			= цел32(0FFFFE0FFH);
	Lime*					= 000FF00FFH;
	Limegreen*				= 032CD32FFH;
	Linen*					= цел32(0FAF0E6FFH);
	Magenta*				= цел32(0FF00FFFFH);
	Maroon*				= цел32(0800000FFH);
	Mediumaquamarine*	= 066CDAAFFH;
	Mediumblue*			= 00000CDFFH;
	Mediumorchid*			= цел32(0BA55D3FFH);
	Mediumpurple*			= цел32(09370DBFFH);
	Mediumseagreen*		= 03CB371FFH;
	Mediumslateblue*		= 07B68EEFFH;
	Mediumspringgreen*	= 000FA9AFFH;
	Mediumturquoise*		= 048D1CCFFH;
	Mediumvioletred*		= цел32(0C71585FFH);
	Midnightblue*			= 0191970FFH;
	Mintcream*				= цел32(0F5FFFAFFH);
	Mistyrose*				= цел32(0FFE4E1FFH);
	Moccasin*				= цел32(0FFE4B5FFH);
	Navajowhite*			= цел32(0FFDEADFFH);
	Navy*					= 0000080FFH;
	Oldlace*				= цел32(0FDF5E6FFH);
	Olive*					= цел32(0808000FFH);
	Olivedrab*				= 06B8E23FFH;
	Orange*				= цел32(0FFA500FFH);
	Orangered*				= цел32(0FF4500FFH);
	Orchid*					= цел32(0DA70D6FFH);
	Palegoldenrod*			= цел32(0EEE8AAFFH);
	Palegreen*				= цел32(098FB98FFH);
	Paleturquoise*			= цел32(0AFEEEEFFH);
	Palevioletred*			= цел32(0DB7093FFH);
	Papayawhip*			= цел32(0FFEFD5FFH);
	Peachpuff*				= цел32(0FFDAB9FFH);
	Peru*					= цел32(0CD853FFFH);
	Pink*					= цел32(0FFC0CBFFH);
	Plum*					= цел32(0DDA0DDFFH);
	Powderblue*			= цел32(0B0E0E6FFH);
	Purple*					= цел32(0800080FFH);
	Red*					= цел32(0FF0000FFH);
	Rosybrown*				= цел32(0BC8F8FFFH);
	Royalblue*				= 04169E1FFH;
	Saddlebrown*			= цел32(08B4513FFH);
	Salmon*				= цел32(0FA8072FFH);
	Sandybrown*			= цел32(0F4A460FFH);
	Seagreen*				= 02E8B57FFH;
	Seashell*				= цел32(0FFF5EEFFH);
	Sienna*					= цел32(0A0522DFFH);
	Silver*					= цел32(0C0C0C0FFH);
	Skyblue*				= цел32(087CEEBFFH);
	Slateblue*				= 06A5ACDFFH;
	Slategray*				= 0708090FFH;
	Slategrey*				= 0708090FFH;
	Snow*					= цел32(0FFFAFAFFH);
	Springgreen*			= 000FF7FFFH;
	Steelblue*				= 04682B4FFH;
	Tan*					= цел32(0D2B48CFFH);
	Teal*					= 0008080FFH;
	Thistle*					= цел32(0D8BFD8FFH);
	Tomato*				= цел32(0FF6347FFH);
	Turquoise*				= 040E0D0FFH;
	Violet*					= цел32(0EE82EEFFH);
	Wheat*					= цел32(0F5DEB3FFH);
	White*					= цел32(0FFFFFFFFH);
	Whitesmoke*			= цел32(0F5F5F5FFH);
	Yellow*					= цел32(0FFFF00FFH);
	Yellowgreen*			= цел32(09ACD32FFH);

тип
	Color*=цел32;
	ColorSum*=массив 4 из вещ64;

(* Parse a color specified by name or value *)
проц Parse*(value: Strings.String; перем color: Color):булево;
перем res: целМЗ; i: размерМЗ; r, g, b: цел32;
	f: вещ64;
нач
	если value[0]='#' то
		value := Strings.Substring2(1,value^);
		Strings.HexStrToInt(value^, color, res);
		если res # Strings.Ok то возврат ложь всё;
		если Strings.Length(value^)=3 то
			(* Expand #rgb to #rrggbb *)
			r := (color DIV 0100H)  остОтДеленияНа 0100H;
			g := (color DIV 010H) остОтДеленияНа 010H;
			b := color остОтДеленияНа 010H;
			Unsplit(color, r+r*010H, g+g*010H, b+b*010H, 0FFH)
		иначе
			color := color * 0100H + 0FFH
		всё
	аесли Strings.StartsWith2("rgb(",value^) то
		i := 4;
		если Strings.IndexOfByte2('%', value^) # -1 то
			SVGUtilities.StrToFloatPos(value^, f, i);
			если value[i]#'%' то возврат ложь иначе увел(i) всё;
			SVGUtilities.SkipCommaWhiteSpace(i, value);
			r := округлиВниз((f*255)/100);

			SVGUtilities.StrToFloatPos(value^, f, i);
			если value[i]#'%' то возврат ложь иначе увел(i) всё;
			SVGUtilities.SkipCommaWhiteSpace(i, value);
			g := округлиВниз((f*255)/100);

			SVGUtilities.StrToFloatPos(value^, f, i);
			если value[i]#'%' то возврат ложь иначе увел(i) всё;
			SVGUtilities.SkipWhiteSpace(i, value);
			b := округлиВниз((f*255)/100);
		иначе
			Strings.StrToIntPos(value^, r, i);
			SVGUtilities.SkipCommaWhiteSpace(i, value);
			Strings.StrToIntPos(value^, g, i);
			SVGUtilities.SkipCommaWhiteSpace(i, value);
			Strings.StrToIntPos(value^, b, i);
			SVGUtilities.SkipWhiteSpace(i, value);
		всё;
		если value[i]#')' то
			SVGUtilities.Warning("color starting with rgb( omitted closing )");
			возврат ложь
		всё;
		Unsplit(color, r, g, b, 0FFH)
	аесли value^ = "aliceblue" то color := Aliceblue
	аесли value^ = "antiquewhite" то color := Antiquewhite
	аесли value^ = "aqua" то color := Aqua
	аесли value^ = "aquamarine" то color := Aquamarine
	аесли value^ = "azure" то color := Azure
	аесли value^ = "beige" то color := Beige
	аесли value^ = "bisque" то color := Bisque
	аесли value^ = "black" то color := Black
	аесли value^ = "blanchedalmond" то color := Blanchedalmond
	аесли value^ = "blue" то color := Blue
	аесли value^ = "blueviolet" то color := Blueviolet
	аесли value^ = "brown" то color := Brown
	аесли value^ = "burlywood" то color := Burlywood
	аесли value^ = "cadetblue" то color := Cadetblue
	аесли value^ = "chartreuse" то color := Chartreuse
	аесли value^ = "chocolate" то color := Chocolate
	аесли value^ = "coral" то color := Coral
	аесли value^ = "cornflowerblue" то color := Cornflowerblue
	аесли value^ = "cornsilk" то color := Cornsilk
	аесли value^ = "crimson" то color := Crimson
	аесли value^ = "cyan" то color := Cyan
	аесли value^ = "darkblue" то color := Darkblue
	аесли value^ = "darkcyan" то color := Darkcyan
	аесли value^ = "darkgoldenrod" то color := Darkgoldenrod
	аесли value^ = "darkgray" то color := Darkgray
	аесли value^ = "darkgreen" то color := Darkgreen
	аесли value^ = "darkgrey" то color := Darkgrey
	аесли value^ = "darkkhaki" то color := Darkkhaki
	аесли value^ = "darkmagenta" то color := Darkmagenta
	аесли value^ = "darkolivegreen" то color := Darkolivegreen
	аесли value^ = "darkorange" то color := Darkorange
	аесли value^ = "darkorchid" то color := Darkorchid
	аесли value^ = "darkred" то color := Darkred
	аесли value^ = "darksalmon" то color := Darksalmon
	аесли value^ = "darkseagreen" то color := Darkseagreen
	аесли value^ = "darkslateblue" то color := Darkslateblue
	аесли value^ = "darkslategray" то color := Darkslategray
	аесли value^ = "darkslategrey" то color := Darkslategrey
	аесли value^ = "darkturquoise" то color := Darkturquoise
	аесли value^ = "darkviolet" то color := Darkviolet
	аесли value^ = "deeppink" то color := Deeppink
	аесли value^ = "deepskyblue" то color := Deepskyblue
	аесли value^ = "dimgray" то color := Dimgray
	аесли value^ = "dimgrey" то color := Dimgrey
	аесли value^ = "dodgerblue" то color := Dodgerblue
	аесли value^ = "firebrick" то color := Firebrick
	аесли value^ = "floralwhite" то color := Floralwhite
	аесли value^ = "forestgreen" то color := Forestgreen
	аесли value^ = "fuchsia" то color := Fuchsia
	аесли value^ = "gainsboro" то color := Gainsboro
	аесли value^ = "ghostwhite" то color := Ghostwhite
	аесли value^ = "gold" то color := Gold
	аесли value^ = "goldenrod" то color := Goldenrod
	аесли value^ = "gray" то color := Gray
	аесли value^ = "grey" то color := Grey
	аесли value^ = "green" то color := Green
	аесли value^ = "greenyellow" то color := Greenyellow
	аесли value^ = "honeydew" то color := Honeydew
	аесли value^ = "hotpink" то color := Hotpink
	аесли value^ = "indianred" то color := Indianred
	аесли value^ = "indigo" то color := Indigo
	аесли value^ = "ivory" то color := Ivory
	аесли value^ = "khaki" то color := Khaki
	аесли value^ = "lavender" то color := Lavender
	аесли value^ = "lavenderblush" то color := Lavenderblush
	аесли value^ = "lawngreen" то color := Lawngreen
	аесли value^ = "lemonchiffon" то color := Lemonchiffon
	аесли value^ = "lightblue" то color := Lightblue
	аесли value^ = "lightcoral" то color := Lightcoral
	аесли value^ = "lightcyan" то color := Lightcyan
	аесли value^ = "lightgoldenrodyellow" то color := Lightgoldenrodyellow
	аесли value^ = "lightgray" то color := Lightgray
	аесли value^ = "lightgreen" то color := Lightgreen
	аесли value^ = "lightgrey" то color := Lightgrey
	аесли value^ = "lightpink" то color := Lightpink
	аесли value^ = "lightsalmon" то color := Lightsalmon
	аесли value^ = "lightseagreen" то color := Lightseagreen
	аесли value^ = "lightskyblue" то color := Lightskyblue
	аесли value^ = "lightslategray" то color := Lightslategray
	аесли value^ = "lightslategrey" то color := Lightslategrey
	аесли value^ = "lightsteelblue" то color := Lightsteelblue
	аесли value^ = "lightyellow" то color := Lightyellow
	аесли value^ = "lime" то color := Lime
	аесли value^ = "limegreen" то color := Limegreen
	аесли value^ = "linen" то color := Linen
	аесли value^ = "magenta" то color := Magenta
	аесли value^ = "maroon" то color := Maroon
	аесли value^ = "mediumaquamarine" то color := Mediumaquamarine
	аесли value^ = "mediumblue" то color := Mediumblue
	аесли value^ = "mediumorchid" то color := Mediumorchid
	аесли value^ = "mediumpurple" то color := Mediumpurple
	аесли value^ = "mediumseagreen" то color := Mediumseagreen
	аесли value^ = "mediumslateblue" то color := Mediumslateblue
	аесли value^ = "mediumspringgreen" то color := Mediumspringgreen
	аесли value^ = "mediumturquoise" то color := Mediumturquoise
	аесли value^ = "mediumvioletred" то color := Mediumvioletred
	аесли value^ = "midnightblue" то color := Midnightblue
	аесли value^ = "mintcream" то color := Mintcream
	аесли value^ = "mistyrose" то color := Mistyrose
	аесли value^ = "moccasin" то color := Moccasin
	аесли value^ = "navajowhite" то color := Navajowhite
	аесли value^ = "navy" то color := Navy
	аесли value^ = "oldlace" то color := Oldlace
	аесли value^ = "olive" то color := Olive
	аесли value^ = "olivedrab" то color := Olivedrab
	аесли value^ = "orange" то color := Orange
	аесли value^ = "orangered" то color := Orangered
	аесли value^ = "orchid" то color := Orchid
	аесли value^ = "palegoldenrod" то color := Palegoldenrod
	аесли value^ = "palegreen" то color := Palegreen
	аесли value^ = "paleturquoise" то color := Paleturquoise
	аесли value^ = "palevioletred" то color := Palevioletred
	аесли value^ = "papayawhip" то color := Papayawhip
	аесли value^ = "peachpuff" то color := Peachpuff
	аесли value^ = "peru" то color := Peru
	аесли value^ = "pink" то color := Pink
	аесли value^ = "plum" то color := Plum
	аесли value^ = "powderblue" то color := Powderblue
	аесли value^ = "purple" то color := Purple
	аесли value^ = "red" то color := Red
	аесли value^ = "rosybrown" то color := Rosybrown
	аесли value^ = "royalblue" то color := Royalblue
	аесли value^ = "saddlebrown" то color := Saddlebrown
	аесли value^ = "salmon" то color := Salmon
	аесли value^ = "sandybrown" то color := Sandybrown
	аесли value^ = "seagreen" то color := Seagreen
	аесли value^ = "seashell" то color := Seashell
	аесли value^ = "sienna" то color := Sienna
	аесли value^ = "silver" то color := Silver
	аесли value^ = "skyblue" то color := Skyblue
	аесли value^ = "slateblue" то color := Slateblue
	аесли value^ = "slategray" то color := Slategray
	аесли value^ = "slategrey" то color := Slategrey
	аесли value^ = "snow" то color := Snow
	аесли value^ = "springgreen" то color := Springgreen
	аесли value^ = "steelblue" то color := Steelblue
	аесли value^ = "tan" то color := Tan
	аесли value^ = "teal" то color := Teal
	аесли value^ = "thistle" то color := Thistle
	аесли value^ = "tomato" то color := Tomato
	аесли value^ = "turquoise" то color := Turquoise
	аесли value^ = "violet" то color := Violet
	аесли value^ = "wheat" то color := Wheat
	аесли value^ = "white" то color := White
	аесли value^ = "whitesmoke" то color := Whitesmoke
	аесли value^ = "yellow" то color := Yellow
	аесли value^ = "yellowgreen" то color := Yellowgreen
	иначе
		возврат ложь
	всё;
	возврат истина
кон Parse;

(* Split a color into r, g, b and a values *)
проц Split*(color: Color; перем r, g, b, a: цел16);
нач
	r := устарПреобразуйКБолееУзкомуЦел((color DIV 01000000H) остОтДеленияНа 0100H);
	g := устарПреобразуйКБолееУзкомуЦел((color DIV 010000H) остОтДеленияНа 0100H);
	b := устарПреобразуйКБолееУзкомуЦел((color DIV 0100H) остОтДеленияНа 0100H);
	a := устарПреобразуйКБолееУзкомуЦел(color остОтДеленияНа 0100H);
кон Split;

(* Create a color using r, g, b and a values *)
проц Unsplit*(перем color: Color; r, g, b, a: цел32);
нач
	color := ((r * 0100H + g) * 0100H + b) * 0100H + a
кон Unsplit;

(* Blend two pixels and premultiply their alpha values *)
проц BlendAndPremultiply*(перем a,b: Raster.Pixel; t: вещ64): Raster.Pixel;
перем p: Raster.Pixel;
	fa: вещ64;
нач
	fa := (кодСимв8(a[3])+(кодСимв8(b[3])-кодСимв8(a[3]))*t) / 255.0;
	p[0] := симв8ИзКода(округлиВниз( fa*(кодСимв8(a[0])+(кодСимв8(b[0])-кодСимв8(a[0]))*t) ));
	p[1] := симв8ИзКода(округлиВниз( fa*(кодСимв8(a[1])+(кодСимв8(b[1])-кодСимв8(a[1]))*t) ));
	p[2] := симв8ИзКода(округлиВниз( fa*(кодСимв8(a[2])+(кодСимв8(b[2])-кодСимв8(a[2]))*t) ));
	p[3] := симв8ИзКода(округлиВниз( fa*255 ));

	возврат p;
кон BlendAndPremultiply;

(* Convert a color to a pixel *)
проц ColorToPixel*(c: Color; перем p: Raster.Pixel);
перем r,g,b,a: цел16;
нач
	Split(c, r,g,b,a);
	p[0] := симв8ИзКода(b);
	p[1] := симв8ИзКода(g);
	p[2] := симв8ИзКода(r);
	p[3] := симв8ИзКода(a);
кон ColorToPixel;

(* Add the r, g, b and a values of a pixel to a running sum *)
проц WeightedAdd*(перем sum: ColorSum;  w: вещ64; pix: Raster.Pixel);
нач
	sum[0] := sum[0] + w*кодСимв8(pix[0]);
	sum[1] := sum[1] + w*кодСимв8(pix[1]);
	sum[2] := sum[2] + w*кодСимв8(pix[2]);
	sum[3] := sum[3] + w*кодСимв8(pix[3]);
кон WeightedAdd;

(* Convert a sum of r, g, b and a values to a pixel *)
проц ColorSumToPixel*(sum: ColorSum;  перем pix: Raster.Pixel);
нач
	pix[0] := симв8ИзКода(округлиВниз(sum[0]));
	pix[1] := симв8ИзКода(округлиВниз(sum[1]));
	pix[2] := симв8ИзКода(округлиВниз(sum[2]));
	pix[3] := симв8ИзКода(округлиВниз(sum[3]));
кон ColorSumToPixel;

кон SVGColors.
