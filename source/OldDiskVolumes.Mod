(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль OldDiskVolumes; (** AUTHOR "pjm"; PURPOSE "Generic disk-based volume"; *)

(* Files.Volume implementation based on Disks. *)

использует НИЗКОУР, ЭВМ, Plugins, Disks, Caches, Files;

конст
	BS = 512;	(* supported device block size *)
	CDBS = 2048;	(* cd device block size *)

	SystemReserved = 32;	(* Blocks reserved for system on Boot volumes *)

	CacheSS = 4096;
	CacheHash1 = 97;
	CacheHash2 = 997;
	CacheHash3 = 3331;
	CacheHash4 = 9973;
	CacheMin = CacheHash1;

	Header = "AosDiskVolumes: ";

перем
	cache: Caches.Cache;	(* shared cache for all volumes *)
	cacheSize, cacheHash: цел32;
	writeback: булево;
	cdid: массив 32 из симв8;

тип
	Volume* = окласс (Files.Volume)
		перем
			dev-: Disks.Device;
			cache: Caches.Cache;	(* cache associated with volume, if any *)
			blocks: цел32;	(* device blocks per volume block *)
			startfs-: цел32;	(* device block offset of file system start *)

		(** Get block from adr [1..size] of volume vol *)
		проц {перекрыта}GetBlock*(adr: цел32; перем blk: массив из симв8);
		перем res: целМЗ; block: цел32; buf: Caches.Buffer; valid: булево;
		нач {единолично}
			если (adr < 1) или (adr > size) то НИЗКОУР.СТОП(15) всё;
			утв(startfs > 0);	(* startfs initialized *)
			утв(длинаМассива(blk) >= blockSize);	(* index check *)
			block := startfs + (adr-1) * blocks;
			если cache # НУЛЬ то
				утв(cache.blockSize >= blockSize);
				cache.Acquire(dev, block, buf, valid);
				если ~valid то dev.Transfer(Disks.Read, block, blocks, buf.data^, 0, res)
				иначе res := Disks.Ok
				всё;
				НИЗКОУР.копируйПамять(адресОт(buf.data[0]), адресОт(blk[0]), blockSize);
				cache.Release(buf, ложь, ложь)
			иначе
				dev.Transfer(Disks.Read, block, blocks, blk, 0, res)
			всё;
			если res # Disks.Ok то НИЗКОУР.СТОП(17) всё
		кон GetBlock;

		(** Put block to adr [1..size] of volume vol *)
		проц {перекрыта}PutBlock*(adr: цел32; перем blk: массив из симв8);
		перем res: целМЗ; block: цел32; buf: Caches.Buffer; valid: булево;
		нач {единолично}
			если (adr < 1) или (adr > size) то НИЗКОУР.СТОП(15) всё;
			утв(startfs > 0);	(* startfs initialized *)
			утв(длинаМассива(blk) >= blockSize);	(* index check *)
			block := startfs + (adr-1) * blocks;
			если cache # НУЛЬ то
				утв(cache.blockSize >= blockSize);
				cache.Acquire(dev, block, buf, valid);
				утв(длинаМассива(buf.data) >= blockSize);	(* index check *)
				НИЗКОУР.копируйПамять(адресОт(blk[0]), адресОт(buf.data[0]), blockSize);
				если writeback то
					cache.Release(buf, истина, ложь)
				иначе
					dev.Transfer(Disks.Write, block, blocks, buf.data^, 0, res);
					cache.Release(buf, истина, истина)
				всё
			иначе
				dev.Transfer(Disks.Write, block, blocks, blk, 0, res)
			всё;
			если res # Disks.Ok то НИЗКОУР.СТОП(17) всё
		кон PutBlock;

		(** Finalize a volume and close its device. *)
		проц {перекрыта}Finalize*;
		перем res: целМЗ; i, j: цел32; ptable: Disks.PartitionTable;
		нач {единолично}
			если cache # НУЛЬ то cache.Synchronize; cache := НУЛЬ всё;
			i := 0; j := -1; ptable := dev.table;	(* todo: fix race! *)
			нцПока i # длинаМассива(ptable) делай
				если (startfs > ptable[i].start) и (startfs < ptable[i].start + ptable[i].size) то
					j := i
				всё;
				увел(i)
			кц;
			если j # -1 то
				утв(Disks.Mounted в ptable[j].flags);
				исключиИзМнваНаБитах(ptable[j].flags, Disks.Mounted)
			всё;
			dev.Close(res);	(* ignore res *)
			dev := НУЛЬ;
			Finalize^	(* see note in AosFS *)
		кон Finalize;

	кон Volume;

проц Get4(перем b: массив из симв8; i: цел32): цел32;
нач
	возврат кодСимв8(b[i]) + арифмСдвиг(кодСимв8(b[i+1]), 8) + арифмСдвиг(кодСимв8(b[i+2]), 16) + арифмСдвиг(кодСимв8(b[i+3]), 24)
кон Get4;

(* Get the file system parameters by reading the boot block. The pstart and psize parameters are the partition start and size in device blocks (size 512 or 2048).  The startfs parameter returns the offset of the file system from the start of the disk in device blocks.  The size parameter returns the size of the file system in volume blocks.  The vbs parameter returns the volume block size of the file system (4096 for AosFS, 2048 for NatFS). *)
проц GetOberonFS(dev: Disks.Device; pstart, psize: цел32; перем startfs, size, vbs: цел32; перем res: целМЗ);
конст FSID = 21534F41H; FSVer = 1; AosSS = 4096; NSS = 2048;
перем i, x, bc, fsofs: цел32; b: массив CDBS из симв8;
нач
	startfs := 0; size := 0; vbs := 0; fsofs := 0;	(* fsofs is the file system offset from the partition start in 512-byte blocks *)
	если (dev.blockSize = BS) и (psize > 0) то	(* "normal" device with 512-byte blocks *)
		dev.Transfer(Disks.Read, pstart, 1, b, 0, res)	(* read boot block of partition/disk *)
	аесли (dev.blockSize = CDBS) и (psize > 17) то
		(* typically pstart = 0 *)
		dev.Transfer(Disks.Read, pstart + 17, 1, b, 0, res);	(* read El Torito boot record *)
		если res = Disks.Ok то
			bc := Get4(b, 47H);	(* boot catalog location *)
			i := 0; нцПока (i < 20H) и (b[i] = cdid[i]) делай увел(i) кц;
			если (i = 20H) и (bc > 0) и (bc < psize) то
				dev.Transfer(Disks.Read, pstart + bc, 1, b, 0, res);	(* read boot catalog *)
				если (b[0] = 1X) и (b[1EH] = 55X) и (b[1FH] = 0AAX) то	(* validation entry ok (skip checksum) *)
					x := Get4(b, 20H+8);	(* start of virtual disk *)
					если (x > 0) и (x < psize) то
						dev.Transfer(Disks.Read, pstart + x, 1, b, 0, res);	(* read boot block of virtual disk *)
						fsofs := x * (CDBS DIV BS)	(* convert to 512-byte block address *)
					иначе
						res := 3	(* not bootable CD *)
					всё
				иначе
					res := 3	(* not bootable CD *)
				всё
			иначе
				res := 3	(* not bootable CD *)
			всё
		всё
	иначе
		res := 2	(* unsupported device block size *)
	всё;
	если res = Disks.Ok то	(* check boot sector *)
		b[0] := "x"; b[1] := "x"; b[2] := "x"; b[9] := 0X;
		если (b[510] = 55X) и (b[511] = 0AAX) то	(* boot sector id found *)
			утв(fsofs >= 0);
			если (Get4(b, 1F8H) = FSID) и (b[1FCH] = симв8ИзКода(FSVer)) и (арифмСдвиг(1, кодСимв8(b[1FDH])) = AosSS) то	(* Aos boot block id found *)
				vbs := AosSS;
				x := fsofs + Get4(b, 1F0H);	(* get offset in 512-byte blocks *)
				утв(x >= 0);
				size := Get4(b, 1F4H);	(* size in volume blocks *)
				утв(size >= 0);
				утв(AosSS остОтДеленияНа dev.blockSize = 0);
				утв(x + size * (AosSS DIV BS) <= psize * (dev.blockSize DIV BS));	(* range check *)
				утв(x остОтДеленияНа (dev.blockSize DIV BS) = 0);	(* correctly aligned *)
				startfs := pstart + x DIV (dev.blockSize DIV BS)	(* offset from start of device in device blocks *)
			аесли b = "xxxOBERON" то	(* Oberon boot block id found *)
				vbs := NSS;	(* NatFS *)
				x := кодСимв8(b[0EH]) + 256*устарПреобразуйКБолееШирокомуЦел(кодСимв8(b[0FH]));	(* reserved 512-byte blocks *)
				size := кодСимв8(b[13H]) + 256*устарПреобразуйКБолееШирокомуЦел(кодСимв8(b[14H]));	(* small size in 512-byte blocks *)
				если size = 0 то size := Get4(b, 20H) всё;	(* large size in 512-byte blocks *)
				если size > psize * (dev.blockSize DIV BS) то	(* limit to partition/disk size *)
					size := psize * (dev.blockSize DIV BS)
				всё;
				умень(size, x);	(* file system size in 512-byte blocks *)
				увел(x, fsofs);	(* file system offset in 512-byte blocks *)
				утв(x остОтДеленияНа (dev.blockSize DIV BS) = 0);	(* correctly aligned *)
				startfs := pstart + x DIV (dev.blockSize DIV BS);	(* offset from start of device in device blocks *)
				size := size DIV (NSS DIV BS)	(* convert 512-byte blocks to volume blocks *)
			иначе
				res := 1	(* unknown file system *)
			всё
		иначе
			res := 1	(* boot block id not found (unformatted?) *)
		всё
	всё;
	утв((startfs >= 0) и (size >= 0))
кон GetOberonFS;

проц InitCache;
перем i: размерМЗ; str: массив 16 из симв8;
нач
	если cache = НУЛЬ то
		ЭВМ.ДайЗначениеКлючаКонфигурацииЯОС("CacheSize", str);
		i := 0; cacheSize := ЭВМ.СтрВЦел32(i, str);
		если cacheSize # 0 то
			writeback := cacheSize < 0;
			cacheSize := матМодуль(cacheSize);
			если cacheSize < CacheMin то cacheSize := CacheMin всё;
			если cacheSize >= CacheHash4 то cacheHash := CacheHash4
			аесли cacheSize >= CacheHash3 то cacheHash := CacheHash3
			аесли cacheSize >= CacheHash2 то cacheHash := CacheHash2
			иначе cacheHash := CacheHash1
			всё;
			нов(cache, CacheSS, cacheHash, cacheSize)
		всё
	всё
кон InitCache;

(* Initialize a volume.  The startfs parameter is the start offset of the file system in device blocks.  The size parameter is the volume size in volume blocks.  The vbs parameter is the size of a volume block.  The part parameter is the partition index in the ptable.  The readonly parameter specifies if the volume should be mounted read only.  If the device is read only, the volume is always mounted read only. *)
проц InitVol(vol: Volume; startfs, size, vbs, part: цел32; ptable: Disks.PartitionTable; readonly: булево);
перем vflags: мнвоНаБитахМЗ;
нач
	vflags := {};
	если readonly или (Disks.ReadOnly в vol.dev.flags) то включиВоМнвоНаБитах(vflags, Files.ReadOnly) всё;
	если Disks.Removable в vol.dev.flags то включиВоМнвоНаБитах(vflags, Files.Removable) всё;
	утв(vbs остОтДеленияНа BS = 0);
	vol.blockSize := vbs;
	утв(vbs остОтДеленияНа vol.dev.blockSize = 0);	(* volume block size must be multiple of device block size *)
	vol.blocks := vbs DIV vol.dev.blockSize;	(* number of device blocks in a volume block *)
	vol.Init(vflags, size, SystemReserved);	(* initialize volume free block map *)
	копируйСтрокуДо0(vol.dev.name, vol.name); Files.AppendStr("#", vol.name); Files.AppendInt(part, vol.name);
	vol.startfs := startfs;
	включиВоМнвоНаБитах(ptable[part].flags, Disks.Mounted);
	если (cache = НУЛЬ) и (CacheSS >= vbs) то InitCache всё;	(* initialize cache the first time (fixme: race) *)
	если (cache # НУЛЬ) и (cache.blockSize >= vbs) то vol.cache := cache иначе vol.cache := НУЛЬ всё
кон InitVol;

(* Try to open the specified volume.  Sets p.vol # NIL on success. *)

проц TryOpen(context: Files.Parameters; dev: Disks.Device; part, dbs: цел32; readonly: булево);
перем vol: Volume; startfs, size, vbs: цел32; res: целМЗ; ptable: Disks.PartitionTable;
нач
	context.out.пСтроку8(Header); context.out.пСтроку8(dev.name);
	context.out.пСимв8("#"); context.out.пЦел64(part, 1); context.out.пСимв8(" ");
	dev.Open(res);
	если res = Disks.Ok то
		ptable := dev.table;
		если ((длинаМассива(ptable) = 1) и (part = 0)) или ((part > 0) и (part < длинаМассива(ptable))) то
			если (dbs = -1) или (dev.blockSize = dbs) то
				если ~(Disks.Mounted в ptable[part].flags) то
					GetOberonFS(dev, ptable[part].start, ptable[part].size, startfs, size, vbs, res);
					если (res = Disks.Ok) и (size > 0) и (vbs остОтДеленияНа dev.blockSize = 0) то
						нов(vol); vol.dev := dev;
						InitVol(vol, startfs, size, vbs, part, ptable, readonly);
						context.vol := vol
					иначе
						просей res из
							|1: context.error.пСтроку8(" partition not formatted")
							|2: context.error.пСтроку8(" bad block size")
							|3: context.error.пСтроку8(" not bootable CD")
						иначе
							context.error.пСтроку8(" boot block error "); context.error.пЦел64(res, 1);
							context.error.пСтроку8(" startfs="); context.error.пЦел64(startfs, 1);
							context.error.пСтроку8(" size="); context.error.пЦел64(size, 1);
							context.error.пСтроку8(" vbs="); context.error.пЦел64(vbs, 1);
							context.error.пСтроку8(" start="); context.error.пЦел64(ptable[part].start, 1);
						всё;
						context.error.пВК_ПС;
					всё
				иначе context.error.пСтроку8(" already mounted"); context.error.пВК_ПС;
				всё
			иначе context.error.пСтроку8(" wrong block size"); context.error.пВК_ПС;
			всё
		иначе context.error.пСтроку8(" invalid partition"); context.error.пВК_ПС;
		всё;
		если context.vol = НУЛЬ то
			dev.Close(res)	(* close again - ignore res *)
		всё
	иначе
		context.error.пСтроку8(" error "); context.error.пЦел64(res, 1); context.error.пВК_ПС;
	всё
кон TryOpen;

(** Generate a new disk volume object. Files.Par: [device] ["#" part] [",R"] *)
проц New*(context : Files.Parameters);
перем
	name: Plugins.Name; part, i: цел32;
	options : массив 8 из симв8; ch : симв8;
	table: Plugins.Table; readonly, retry: булево;
нач
	context.vol := НУЛЬ; retry := ложь;
	Files.GetDevPart(context.arg, name, part);

	(* read optional parameter *)
	context.arg.ПропустиБелоеПоле; ch := context.arg.ПодглядиСимв8();
	если (ch = ",") то context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(options); всё;
	readonly := options = ",R";

	Disks.registry.GetAll(table);
	если table # НУЛЬ то
		если name # "" то
			i := 0; нцПока (i # длинаМассива(table)) и (table[i].name # name) делай увел(i) кц;
			если i # длинаМассива(table) то
				TryOpen(context, table[i](Disks.Device), part, -1, readonly)
			иначе
				context.error.пСтроку8(Header); context.error.пСтроку8(name); context.error.пСтроку8(" not found"); context.error.пВК_ПС;
			всё
		иначе
			i := 0;
			нц
				TryOpen(context, table[i](Disks.Device), part, CDBS, readonly);
				увел(i);
				если (context.vol # НУЛЬ) или (i >= длинаМассива(table)) то прервиЦикл всё;
			кц
		всё
	иначе
		context.error.пСтроку8(Header); context.error.пСтроку8("no devices"); context.error.пВК_ПС;
	всё;
кон New;

нач
	cdid := "?CD001?EL TORITO SPECIFICATION?";
	cdid[0] := 0X; cdid[6] := 1X; cdid[30] := 0X; cdid[31] := 0X;
	cache := НУЛЬ; writeback := ложь
кон OldDiskVolumes.

(*
to do:
o fix races here, so that concurrent tools other than OFSTools and AosConsole can be used for mounting
o do not HALT blindly when drivers returns a bad res
*)
