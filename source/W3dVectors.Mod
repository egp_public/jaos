модуль W3dVectors; (** AUTHOR "TF"; PURPOSE "Vector operations for 3d partial port from RAILY 4.0"; *)
(* 28.06.1998 *)
(* 30.06.1998 *)
(* 30.08.1998 *)
(* 15.12.1998 *)
(* 18.01.2001 Bounding Sphere *)
(* 09.06.2001 CCW *)
(* Limitations in Oberon Port :
	No overloading(Added 2 for 2d and 3 for 3d versions),
	No default values,
	No default result variable --> less efficient
	No const parameters (can not use VAR(less "functional" --> less efficient) *)
использует Math := MathL;

тип
	 TVector2d* = запись x*, y*: вещ64 кон;
     TVector3d* = запись x*, y*, z*: вещ64 кон;

     TLineSegment2d* = запись A*, B*: TVector2d кон;
     TRectangle* = запись A*, B*: TVector2d кон;

     TBoundingSphere* = запись P*: TVector3d; r*:вещ64 кон;


конст TooSmall* = 0.00000000001;

проц Sqr(x: вещ64):вещ64;
нач
	возврат x * x
кон Sqr;

проц Vector2d*(x:вещ64; y:вещ64):TVector2d;
перем result : TVector2d;
нач result.x := x; result.y := y; возврат result кон Vector2d;

проц Vector3d*(x:вещ64; y:вещ64; z:вещ64):TVector3d;
перем result : TVector3d;
нач result.x := x; result.y := y; result.z := z; возврат result кон Vector3d;

проц VAdd2*(a, b: TVector2d):TVector2d;
перем result : TVector2d;
нач result.x:=a.x+b.x; result.y:=a.y+b.y; возврат result кон VAdd2;

проц VAdd3*(a, b: TVector3d):TVector3d;
перем result : TVector3d;
нач result.x:=a.x+b.x; result.y:=a.y+b.y; result.z:=a.z+b.z; возврат result кон VAdd3;

проц VSub2*(a, b: TVector2d):TVector2d;
перем result : TVector2d;
нач result.x:=a.x-b.x; result.y:=a.y-b.y; возврат result кон VSub2;

проц VSub3*(a, b: TVector3d):TVector3d;
перем result : TVector3d;
нач result.x:=a.x-b.x; result.y:=a.y-b.y; result.z:=a.z-b.z; возврат result кон VSub3;

проц VNeg2*(a: TVector2d):TVector2d;
перем result : TVector2d;
нач result.x:=-a.x; result.y:=-a.y; возврат result кон VNeg2;

проц VNeg3*(a: TVector3d):TVector3d;
перем result : TVector3d;
нач result.x:=-a.x; result.y:=-a.y; result.z:=-a.z; возврат result кон VNeg3;

проц VLength2*(a: TVector2d):вещ64;
перем t: вещ64;
нач
	a.x := матМодуль(a.x); a.y:= матМодуль(a.y);
	если a.x > a.y то t := a.x; a.x := a.y; a.y:=t всё;
	если a.x = 0 то возврат a.y
	иначе возврат a.y * Math.sqrt(1 + Sqr(a.x/a.y))
	всё
кон VLength2;

проц VLength2VV*(a, b: TVector2d):вещ64;
нач
	возврат VLength2(VSub2(a, b))
кон VLength2VV;

проц VLength3VV*(a, b: TVector3d):вещ64;
нач
	возврат VLength3(VSub3(a, b))
кон VLength3VV;

проц VLength3*(a: TVector3d):вещ64;
перем t: вещ64;
нач
	a.x := матМодуль(a.x); a.y:= матМодуль(a.y); a.z:=матМодуль(a.z);
	если a.x > a.y то t := a.x; a.x := a.y; a.y:=t всё;
	если a.y > a.z то t := a.y; a.y := a.z; a.z:=t всё;
	(* a.z >= a.y, a.z >= a.x *)
	если a.z = 0 то возврат 0
	иначе возврат a.z * Math.sqrt(1 + Sqr(a.x/a.z) + Sqr(a.y/a.z))
	всё
кон VLength3;

(* squared length *)
проц VLength2Sq*(a: TVector2d):вещ64;
нач
	возврат Sqr(a.x) + Sqr(a.y)
кон VLength2Sq;

проц VLength2VVSq*(a, b: TVector2d):вещ64;
нач
	возврат VLength2Sq(VSub2(a, b))
кон VLength2VVSq;

проц VLength3VVSq*(a, b: TVector3d):вещ64;
нач
	возврат VLength3Sq(VSub3(a, b))
кон VLength3VVSq;

проц VLength3Sq*(a: TVector3d):вещ64;
нач
	возврат Sqr(a.x) + Sqr(a.y) + Sqr(a.z)
кон VLength3Sq;

проц VScaled2*(a:TVector2d; factor:вещ64):TVector2d;
перем result : TVector2d;
нач result.x:=factor*a.x; result.y:=factor*a.y; возврат result кон VScaled2;

проц VScaled3*(a:TVector3d; factor:вещ64):TVector3d;
перем result : TVector3d;
нач result.x:=factor*a.x; result.y:=factor*a.y; result.z:=factor*a.z; возврат result  кон VScaled3;

проц VRot90*(a: TVector2d):TVector2d;
перем result : TVector2d;
нач result.x:=a.y; result.y:=-a.x; возврат result кон VRot90;

проц VNormed2*(a: TVector2d):TVector2d;
нач
	возврат VScaled2(a, 1/VLength2(a))
кон VNormed2;

проц VNormed3*(a: TVector3d):TVector3d;
нач возврат VScaled3(a, 1/VLength3(a)) кон VNormed3;

проц Scalar2*(a, b: TVector2d):вещ64;
нач возврат a.x*b.x+a.y*b.y кон Scalar2;

проц Scalar3*(a, b: TVector3d):вещ64;
нач возврат a.x*b.x+a.y*b.y +a.z*b.z кон Scalar3;

проц Cross*(a, b: TVector3d):TVector3d;
перем result : TVector3d;
нач result.x:=a.y*b.z-a.z*b.y; result.y:=a.z*b.x-a.x*b.z; result.z:=a.x*b.y-a.y*b.x; возврат result кон Cross;

проц CCW*(a, b, c: TVector2d):булево;
нач возврат (b.x-a.x) * (c.y-a.y) - (c.x-a.x) * (b.y-a.y) >= 0
кон CCW;

кон W3dVectors.

