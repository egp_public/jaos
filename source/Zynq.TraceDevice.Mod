(**
	AUTHOR: Timothee Martiel, Alexey Morozov
	PURPOSE: simple abtraction for installing kernel tracing over Zynq PS UART
*)
модуль TraceDevice;

использует НИЗКОУР, Platform, BootConfig, Трассировка, PsUartMin;

перем
	uart: PsUartMin.UartRegisters;

	проц TraceChar(ch: симв8);
	перем res: целМЗ;
	нач
		PsUartMin.SendChar(uart, ch, истина, НУЛЬ, res);
	кон TraceChar;

	проц Install *;
	перем
		uartId: PsUartMin.UartId;
		res: целМЗ;
	нач
		Трассировка.Настройся;
		uartId := BootConfig.GetIntValue("TracePort")-1;

		PsUartMin.Install(uartId, Platform.UartBase[uartId], BootConfig.GetIntValue("UartInputClockHz"), res);
		если res # 0 то возврат; всё;

		uart := PsUartMin.GetUart(uartId);

		если ~PsUartMin.SetBps(uart, BootConfig.GetIntValue("TraceBPS"), res) то возврат; всё;
		PsUartMin.Enable(uart,истина);

		Трассировка.пСимв8 := TraceChar;
	кон Install;

кон TraceDevice.
