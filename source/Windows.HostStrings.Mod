модуль HostStrings; 

использует UCS2, Kernel32, НИЗКОУР;
	
тип StrokaWindowsUTF16DoNolja* = укль { опасныйДоступКПамяти, неОтслСборщиком } на массив матМаксимум(размерМЗ) из бцел16;

проц ZagruziStrokuIzWindows*(vkh : StrokaWindowsUTF16DoNolja): UCS2.PString;
перем clipboardPos, clipboardLen : размерМЗ; res : UCS2.PString;

нач
	если vkh # Kernel32.NULL то
		(* obrezhet slishkom dlinnuju strochku molcha *)
		нцПока (vkh[ clipboardLen ] # 0) и (clipboardLen < матМаксимум(размерМЗ)) делай 
			увел( clipboardLen ) кц;
		нов( res, clipboardLen + 1 );
		нцПока clipboardPos < clipboardLen делай
			res[clipboardPos] := UCS2.IntToUCS2Char(vkh[clipboardPos]);
			увел( clipboardPos ); кц всё; 
	возврат res кон ZagruziStrokuIzWindows;


(* Stroka budet sozdana s flagami { Kernel32.GMemMoveable, Kernel32.GMemDDEShare } *)
проц OtpravqStrokuVWindowsDljaPutClipboard*(vkh : UCS2.PString; перем vYkh : StrokaWindowsUTF16DoNolja) : Kernel32.BOOL;
перем
	vYkhL: StrokaWindowsUTF16DoNolja;
	textLen : размерМЗ;
нач
	textLen := длинаМассива(vkh);
	vYkh := Kernel32.GlobalAlloc( { Kernel32.GMemMoveable, Kernel32.GMemDDEShare }, (textLen + 1 )*размер16_от(UCS2.Char) );
	если vYkh = Kernel32.NULL то
		возврат Kernel32.False всё;
	vYkhL := Kernel32.GlobalLock( vYkh );
	НИЗКОУР.копируйПамять( адресОт( vkh[ 0 ] ), vYkhL, ( textLen )*размер16_от(UCS2.Char) ); (* include NULL *)
	vYkhL[ textLen ] := 0; (* NULLNULL *)

	если Kernel32.GlobalUnlock( vYkh ) # Kernel32.True то
		возврат Kernel32.False всё;
	возврат Kernel32.True кон OtpravqStrokuVWindowsDljaPutClipboard;
	
кон HostStrings.

