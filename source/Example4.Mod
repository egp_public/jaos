(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Example4;	(* pjm *)

(*
Buffer allocator.
Ref: C.A.R. Hoare, "Monitors: An Operating System Structuring Concept", CACM 17(10), 1974
*)

тип
	Buffer* = окласс перем next: Buffer кон Buffer;	(** must be extended by client *)

	BufferPool* = окласс
		перем root: Buffer;

		проц Acquire*(перем buf: Buffer);	(** take a buffer from the pool *)
		нач {единолично}
			дождись(root # НУЛЬ);
			buf := root; root := root.next
		кон Acquire;

		проц Release*(buf: Buffer);	(** add a buffer to the pool *)
		нач {единолично}
			buf.next := root; root := buf
		кон Release;

		проц &Init*;
		нач
			root := НУЛЬ
		кон Init;

	кон BufferPool;

кон Example4.
