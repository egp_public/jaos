модуль SSHChannels;   (* GF  05.01.2021 *)

использует Kernel, Потоки, Log := ЛогЯдра, P := SSHPackets, T := SSHTransport;

конст
	WinSize = 8*1024;
	MaxPacketSize = 4*1024;   (* max packet size *)

	Opening* = 0;  Open* = 1;  Closed* = 2;  (* chanel states *)

	(*-----------------------------------------------------------------------*)

	
тип
	Packet = P.Packet;
	Connection = T.Connection;
	
	Channel* = окласс (Потоки.ФункциональныйИнтерфейсКПотоку)
	конст
		BufSize = 2*2048;
		перем
			state-: целМЗ;
			conn: Connection;
			rchan: цел32;
			clchan: цел32;
			rwsize: размерМЗ;
			clwsize: размерМЗ;
			rpmax: размерМЗ;   (* max packet size accepted by remote host *)
			interactive: булево;
						
			sendBuffer: массив BufSize из симв8; bufPos, sentBytes: размерМЗ;
			rpac: Packet;

			проц ЗапишиВПоток*( конст data: массив из симв8;  ofs, len: размерМЗ;  propagate: булево;  перем res: целМЗ );
			перем 
				i: размерМЗ;
			нач 
				нцДля i := 0 до len - 1 делай
					sendBuffer[bufPos] := data[ofs + i];  увел( bufPos );
					если bufPos = BufSize то  FlushBuffer  всё
				кц;
				если propagate то FlushBuffer  всё;
				res := Потоки.Успех
			кон ЗапишиВПоток;

			проц ПрочтиИзПотока*( перем data: массив из симв8;  ofs, size, min: размерМЗ;  перем len: размерМЗ; перем res: целМЗ );
			нач
				len := 0; 
				нцДо
					если rpac.pos < rpac.len то
						нцПока (len < size) и (rpac.pos < rpac.len) делай
							data[ofs] := rpac.GetChar( );
							увел( ofs ); увел( len ); 
						кц;
					иначе
						если state = Closed то
							res := Потоки.КонецФайла;  возврат
						всё;
						если conn.PacketAvailable() то
							rpac := GetPacket( );
							если rpac.type = P.ChannelEOF то
								res := Потоки.КонецФайла;  
								возврат
							всё;
							утв( rpac.type = P.Data );
						всё
					всё;
				кцПри len >= min;
				res := Потоки.Успех;
			кон ПрочтиИзПотока;


			проц FlushBuffer;
			перем 
				p: Packet;
			нач 
				если ~interactive то  GetWindowSpace  всё;
				если bufPos > 0 то
					нов( p, P.Data, bufPos + 9 );
						p.AppInteger( rchan );
						p.AppArray( sendBuffer, 0, bufPos );
					conn.SendPacket( p );  
					увел( sentBytes, bufPos );
					умень( rwsize, bufPos + 9 );
					bufPos := 0;
				всё;
			кон FlushBuffer;
			
			проц GetWindowSpace;
			перем p: Packet;
				space: размерМЗ;
			нач
				если sentBytes > 500 то
					если conn.PacketAvailable( ) то	
						p := conn.GetPacket( );
						если p.type = P.WindAdjust то
							p.SetPos( 5 );
							space :=  p.GetInteger( );
							увел( rwsize, space );
							timer.Sleep( 50 )
						всё
					всё
				всё
			кон GetWindowSpace; 


			проц GetPacket( ): Packet;
			перем newspace: цел32;
				rp, sp: Packet; 
				msg: массив 256 из симв8; repl: булево;
			нач 
				rp := conn.GetPacket( );
				просей rp.type из
				| P.WindAdjust:
						rp.SetPos( 5 );
						newspace := rp.GetInteger( );
						увел( rwsize, newspace );
						возврат GetPacket( )
				| P.ChannelEOF:
						возврат rp
				| P.ChannelClose:
						если state # Closed то
							Закрой
						всё;
						возврат P.emptyPacket
				| P.Data:
						rp.SetPos( 5 );
						умень( clwsize, rp.GetInteger( ) );	
						если clwsize < 5000 то	
							нов( sp, P.WindAdjust, 128 );
								sp.AppInteger( rchan );  
								sp.AppInteger( WinSize - clwsize );
							conn.SendPacket( sp );
							clwsize := WinSize
						всё;
						возврат rp
				| P.ExtData:  (* stderr *)
						rp.SetPos( 9 );
						rp.GetString( msg );
						Log.пСтроку8( msg ); Log.пВК_ПС;
						возврат GetPacket( )
				| P.ChannelRequest:
						(* ignore signal, exit-signal, exit-status *)
						rp.SetPos( 5 );  
						rp.GetString( msg );
						repl := rp.GetChar( ) # 0X;
						если repl то  rp.Show  всё;
						возврат P.emptyPacket
				иначе
					если state = Opening то
						возврат rp
					иначе
						ErrorResponse( "SSHChannels.GetPacket", rp ); 
						возврат P.emptyPacket
					всё
				всё;
			кон GetPacket;
				
				
			проц WindowChange*( width, height: размерМЗ );
			перем p: Packet;
			нач
				FlushBuffer;
				нов( p, P.ChannelRequest, 512 );
					p.AppInteger( rchan );
					p.AppString( "window-change" );
					p.AppChar( 0X );	(* false *)
					p.AppInteger( width );
					p.AppInteger( height );
					p.AppInteger( 0 );	(* width, pixel *)
					p.AppInteger( 0 );	(* height, pixel *)
				conn.SendPacket( p );
			кон WindowChange;

			
			проц Eof*;
			перем p: Packet;
			нач
				FlushBuffer;
				если state = Open то
					нов( p, P.ChannelEOF, 16 ); 
						p.AppInteger( rchan );  
					conn.SendPacket( p );
				всё;
			кон Eof;

			проц Закрой*;
			перем p: Packet;
			нач
				FlushBuffer;
				если state = Open то
					state := Closed;
					нов( p, P.ChannelClose, 64 );  
						p.AppInteger( rchan );  
					conn.SendPacket( p );
				всё;
				SkipInput( conn );	(* exit-signal, exit-status *)
				bufPos := 0;
			кон Закрой;
		

			проц & Init*( c: Connection );
			нач
				conn := c;  state := Opening;
				clchan := conn.GetChannelNo();
				clwsize := WinSize;
				bufPos := 0;  sentBytes := 0;
				нов( timer );
				rpac := P.emptyPacket	
			кон Init;

		кон Channel;

(*=================================================================*)
	
	проц OpenSession*( conn: Connection;  interactive: булево ): Channel;
	перем 
		sp, rp: Packet;  chan: Channel;
	нач
		нов( chan, conn );  

		нов( sp, P.ChannelOpen, 512 );
		sp.AppString( "session" );
		sp.AppInteger( chan.clchan );
		sp.AppInteger( WinSize );
		если interactive то  sp.AppInteger( 512 )  иначе  sp.AppInteger( MaxPacketSize )  всё;

		conn.SendPacket( sp );
		rp := conn.GetPacket( );
		если rp.type = P.GlobalRequest то  
			(* ignore server hostkey *)
			rp := conn.GetPacket( )
		всё;
		если rp.type # P.OpenConfirm то
			ErrorResponse( "SSHChannels.OpenSession", rp );  
			chan.Закрой;  возврат НУЛЬ
		всё;
		
		rp.SetPos( 5 );
		chan.rchan := rp.GetInteger( );
		chan.rwsize := rp.GetInteger( );
		chan.rpmax := rp.GetInteger( );
		
		нов( sp, P.ChannelRequest, 512 );
			sp.AppInteger( chan.rchan );
			sp.AppString( "pty-req" );
			sp.AppChar( 1X );   (* want reply *)
			sp.AppString( "vt100" );
			sp.AppInteger( 80 );  sp.AppInteger( 24 );   (* chars *)
			sp.AppInteger( 640 );  sp.AppInteger( 480 );   (* pixels *)
			если ~interactive то
				sp.AppInteger( 26 );
				sp.AppChar( симв8ИзКода( 50 ) );  sp.AppInteger( 0 );   (* -isig *)
				sp.AppChar( симв8ИзКода( 51 ) );  sp.AppInteger( 0 );   (* -icanon*)
				sp.AppChar( симв8ИзКода( 52 ) );  sp.AppInteger( 0 );   (* -xcase*)
				sp.AppChar( симв8ИзКода( 53 ) );  sp.AppInteger( 0 );   (* -echo *)
				sp.AppChar( симв8ИзКода( 91 ) );  sp.AppInteger( 1 );   (* 8 bit mode *)
			иначе
				sp.AppInteger( 1 )
			всё;
			sp.AppChar( 0X );   (* 0 = TTY OP END *)
		conn.SendPacket( sp );
		
		rp := conn.GetPacket( );
		если rp.type # P.ChannelSuccess то
			ErrorResponse( "SSHChannels.OpenSession: pty-request", rp );  
			chan.Закрой;  возврат НУЛЬ
		всё;
		
		нов( sp, P.ChannelRequest, 128 );
			sp.AppInteger( chan.rchan );
			sp.AppString( "shell" );
			sp.AppChar( 1X );   (* want reply *)
		conn.SendPacket( sp );

		rp := conn.GetPacket( );
		нцПока rp.type = P.WindAdjust делай
			rp.SetPos( 5 );
			увел( chan.rwsize, rp.GetInteger( ) );		
			rp := conn.GetPacket( )			
		кц;
		если (rp.type # P.ChannelSuccess) то
			ErrorResponse( "SSHChannels.OpenSession: shell request", rp );  
			chan.Закрой;  возврат НУЛЬ
		всё;
		если ~interactive то  SkipInput( conn ) (* shell promt *)  всё;
		chan.interactive := interactive;
		chan.state := Open;
		возврат chan
	кон OpenSession;
	
	
	проц OpenTransferChannel*( conn: Connection;  конст remCommand: массив из симв8 ): Channel;
	перем 
		sp, rp: Packet;  chan: Channel;
	нач
		нов( chan, conn );  

		нов( sp, P.ChannelOpen, 512 );
			sp.AppString( "session" );
			sp.AppInteger( chan.clchan );
			sp.AppInteger( WinSize );
			sp.AppInteger( MaxPacketSize );
		conn.SendPacket( sp );
		
		rp := conn.GetPacket( );
		если rp.type = P.GlobalRequest то  
			(* ignore server hostkey *)
			rp := conn.GetPacket( )
		всё;
		если rp.type = P.Data то
			rp := conn.GetPacket( )
		всё;
		нцПока rp.type = P.WindAdjust делай
			rp.SetPos( 5 );
			увел( chan.rwsize, rp.GetInteger( ) );		
			rp := conn.GetPacket( )			
		кц;
		
		если rp.type # P.OpenConfirm то
			ErrorResponse( "SSHChannels.OpenTransferChanel: session request", rp );  
			chan.Закрой;  возврат НУЛЬ
		всё;
		rp.SetPos( 5 );
		chan.rchan := rp.GetInteger( );
		chan.rwsize := rp.GetInteger( );
		chan.rpmax := rp.GetInteger( );

		нов( sp, P.ChannelRequest, 512 );
			sp.AppInteger( chan.rchan );
			sp.AppString( "exec" );
			sp.AppChar( 1X );   (* want reply *)
			sp.AppString( remCommand );
		conn.SendPacket( sp );
		
		rp := conn.GetPacket( );
		нцПока rp.type = P.WindAdjust делай
			rp.SetPos( 5 );
			увел( chan.rwsize, rp.GetInteger( ) );		
			rp := conn.GetPacket( )			
		кц;
		если (rp.type # P.ChannelSuccess) то
			ErrorResponse( "SSHChannels.OpenTransferChannel: exec request", rp );  
			chan.Закрой; возврат НУЛЬ
		всё;

		chan.interactive := ложь;
		возврат chan
	кон OpenTransferChannel;
	
	
(*=================================================================*)
	

	проц ErrorResponse( конст caller: массив из симв8; p: Packet );
	перем msg: массив 1024 из симв8;
	нач
		Log.пВК_ПС;  Log.пСтроку8( caller );
		если p.type = P.OpenFailure то
			p.SetPos( 5 );
			Log.пСтроку8( ": error code = " );  Log.пЦел64( p.GetInteger( ), 1 );
			p.GetString( msg );
			Log.пСимв8( ' ' );  Log.пСтроку8( msg );
		аесли p.type = P.RequestFailure то
			Log.пСтроку8( ": request failed " );
		иначе
			Log.пСтроку8( ": got unexpected packet: " );  
			p.Show
		всё;
		Log.пВК_ПС;
	кон ErrorResponse;
	
перем
	timer: Kernel.Timer;
	
	проц SkipInput( conn: Connection );  
	(* consume any incoming packets until connection is quiet *)
	перем 
		p: Packet;
	нач
		timer.Sleep( 200 );
		нцПока conn.PacketAvailable( )  делай  
			p := conn.GetPacket( );  
			timer.Sleep( 100 )  
		кц;
	кон SkipInput;


нач
	нов( timer )
кон SSHChannels.



