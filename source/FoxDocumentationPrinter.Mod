модуль FoxDocumentationPrinter; (** AUTHOR ""; PURPOSE ""; *)

использует Tree := FoxDocumentationTree, Scanner := FoxDocumentationScanner, Потоки;

конст debug=ложь;

тип
	ParagraphType=Tree.ParagraphType;
	ElementType= Tree.ElementType;
	Printer*= окласс
	перем writer: Потоки.Писарь;

		проц &InitPrinter*(l0writer: Потоки.Писарь);
		нач сам.writer := l0writer
		кон InitPrinter;

		проц Section*(section: Tree.Section);
		нач
			если debug то writer.пВК_ПС; writer.пСтроку8("//// Section with level "); writer.пЦел64(section.level,1); writer.пСтроку8("//// "); всё;
			writer.пВК_ПС;
			Chars("@", section.level);
			если section.label # НУЛЬ то writer.пСтроку8(section.label^) всё;
			Text(section.title); writer.пВК_ПС;
			ParagraphList(section.contents);
		кон Section;

		проц SectionList*(sections: Tree.Sections);
		перем i: размерМЗ;
		нач
			нцДля i := 0 до sections.Length()-1 делай
				Section(sections.GetSection(i));
			кц;
		кон SectionList;

		проц Document*(document: Tree.Document);
		нач
			ParagraphList(document.description);
			SectionList(document.sections);
		кон Document;

		проц Chars*(c: симв8; rep: цел32);
		нач
			нцПока rep>0 делай writer.пСимв8(c); умень(rep) кц;
		кон Chars;

		проц Paragraph*(paragraph: Tree.Paragraph);
		нач
			если debug то
				writer.пВК_ПС;
				writer.пСтроку8("//// paragraph of type ");
				просей paragraph.type из
				ParagraphType.Heading: writer.пСтроку8("Heading");
				|ParagraphType.TextBlock: writer.пСтроку8("TextBlock");
				|ParagraphType.Number: writer.пСтроку8("Number");
				|ParagraphType.Bullet: writer.пСтроку8("Bullet")
				|ParagraphType.Code: writer.пСтроку8("Code")
				|ParagraphType.Table: writer.пСтроку8("Table");
				|ParagraphType.Line: writer.пСтроку8("Line");
				|ParagraphType.Description: writer.пСтроку8("Description")
				всё;
				writer.пСтроку8("//// ");
			всё;
			writer.пВК_ПС;
			просей paragraph.type из
			ParagraphType.Heading: Chars('=', paragraph.level);
				если paragraph.label # НУЛЬ то writer.пСтроку8(paragraph.label^); всё;
				Text(paragraph.text);
			|ParagraphType.Description: writer.пСтроку8("#"); Text(paragraph.description); writer.пСтроку8("#"); Text(paragraph.text);
			|ParagraphType.TextBlock: Text(paragraph.text);
			|ParagraphType.Number: Chars('#', paragraph.level); Text(paragraph.text);
			|ParagraphType.Bullet: Chars('*', paragraph.level); Text(paragraph.text);
			|ParagraphType.Code: writer.пСтроку8("{{{"); writer.пВК_ПС; Text(paragraph.text); writer.пВК_ПС; writer.пСтроку8("}}}");
			|ParagraphType.Table: Text(paragraph.text);
			|ParagraphType.Line: writer.пСтроку8("----");
			иначе СТОП(200)
			всё;
			writer.пВК_ПС;
		кон Paragraph;

		проц ParagraphList*(paragraphs: Tree.Paragraphs);
		перем i: размерМЗ;
		нач
			нцДля i := 0 до paragraphs.Length()-1 делай
				Paragraph(paragraphs.GetParagraph(i))
			кц;
		кон ParagraphList;

		проц PrintText*(textElement: Tree.TextElement; separator: симв8);
		нач
			если textElement.text.Length() = 0 то
				если textElement.string # НУЛЬ то
					writer.пСтроку8(textElement.string^);
				всё;
			иначе
				Text(textElement.text);
			всё;
		кон PrintText;

		проц TextElement*(textElement: Tree.TextElement);
		нач
			если debug то
				writer.пВК_ПС; writer.пСтроку8("//// TextElement:");
				просей textElement.type из
				ElementType.Default : writer.пСтроку8("Default")
				|ElementType.Italic :  writer.пСтроку8("Italic")
				|ElementType.Bold :  writer.пСтроку8("Bold")
				|ElementType.HeaderCell :  writer.пСтроку8("HeaderCell")
				|ElementType.DataCell :  writer.пСтроку8("DataCell")
				|ElementType.Row :  writer.пСтроку8("Row")
				|ElementType.Link :  writer.пСтроку8("Link")
				|ElementType.WeakLink :  writer.пСтроку8("Link")
				|ElementType.Label :  writer.пСтроку8("Label")
				|ElementType.Code :  writer.пСтроку8("Code")
				|ElementType.LineBreak :  writer.пСтроку8("LineBreak")
				иначе всё;
				writer.пСтроку8("//// ");
			всё;
			(*
			IF separator # 0X THEN
				writer.Char(separator)
			END;
			*)
			просей textElement.type из
			ElementType.Default : PrintText(textElement,0X);
			|ElementType.Whitespace: writer.пСтроку8(" ");
			|ElementType.Italic : writer.пСтроку8("/"); PrintText(textElement,0X); writer.пСтроку8("/");
			|ElementType.Underline : writer.пСтроку8("_"); PrintText(textElement,0X); writer.пСтроку8("_");
			|ElementType.Bold : writer.пСтроку8("*"); PrintText(textElement,0X); writer.пСтроку8("*");
			|ElementType.HeaderCell : writer.пСтроку8("|="); PrintText(textElement,' ');
			|ElementType.DataCell : writer.пСтроку8("|"); PrintText(textElement,' ');
			|ElementType.Row : PrintText(textElement,0X); writer.пВК_ПС;
			|ElementType.Link, ElementType.WeakLink :
				writer.пСтроку8("[["); writer.пСтроку8(textElement.string^);
				если textElement.text.Length()#0 то
					writer.пСтроку8("|");  Text(textElement.text);
				всё;
				writer.пСтроку8("]]");
			|ElementType.Label : writer.пСтроку8("<<"); writer.пСтроку8(textElement.string^); writer.пСтроку8(">>");
			|ElementType.Code : writer.пСтроку8("{{{"); PrintText(textElement,0X); writer.пСтроку8("}}}");
			|ElementType.LineBreak : writer.пСтроку8("\\");
			всё;
		кон TextElement;

		проц Text*(text: Tree.Text);
		перем element: Tree.TextElement; i: размерМЗ;
		нач
			нцДля i := 0 до text.Length()-1 делай
				element := text.GetElement(i);
				TextElement(element);
			кц;
		кон Text;

	кон Printer;

кон FoxDocumentationPrinter.
