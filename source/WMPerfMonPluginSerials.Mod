модуль WMPerfMonPluginSerials; (** AUTHOR "staubesv"; PURPOSE "Performance Monitor Serial Port transfer rate plugin"; *)

использует
	WMPerfMonPlugins, Strings, Modules, Serials, Потоки;

конст
	PluginName = "Serials";
	PluginDescription = "Serial Port Transfer Rate";

тип

	SerialsParameter = укль на запись(WMPerfMonPlugins.Parameter)
		port : Serials.Port;
	кон;

	SerialPortTransferRate = окласс(WMPerfMonPlugins.Plugin)
	перем
		port : Serials.Port;

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		перем ds : WMPerfMonPlugins.DatasetDescriptor;
		нач
			p.name := PluginName; p.description := PluginDescription;
			сам.port := p(SerialsParameter).port;
			копируйСтрокуДо0(port.name, p.devicename);
			Strings.Append(p.devicename, " ("); Strings.Append(p.devicename, port.description); Strings.Append(p.devicename, ")");
			p.modulename := "WMPerfMonPluginSerials";
			p.autoMin := ложь; p.autoMax := истина; p.unit := "KB"; p.perSecond := истина; p.minDigits := 5; p.showSum := истина;

			нов(ds, 3);
			ds[0].name := "Total"; ds[0].unit := "KB"; включиВоМнвоНаБитах(ds[0].flags, WMPerfMonPlugins.Sum);
			ds[1].name := "Received"; ds[1].unit := "KB";
			ds[2].name := "Sent"; ds[2].unit := "KB";
			p.datasetDescriptor := ds;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		перем sent, received : Потоки.ТипМестоВПотоке;
		нач
			sent := port.charactersSent; received := port.charactersReceived;
			dataset[0] := sent + received / 1024;
			dataset[1] := received / 1024;
			dataset[2] := sent / 1024;
		кон UpdateDataset;

	кон SerialPortTransferRate;

перем
	plugins : массив Serials.MaxPorts из SerialPortTransferRate;

(** Install a serial port plugin for each serial port. Previously installed plugins will be removed *)
проц Install*;
перем
	plugin : SerialPortTransferRate; p : SerialsParameter;
	port : Serials.Port; i : цел32;
нач {единолично}
	RemovePlugins;
	нцДля i := 1 до Serials.MaxPorts делай
		port := Serials.GetPort(i);
		если port # НУЛЬ то
			нов(p); p.port := port;
			нов(plugin, p);
			plugins[i-1] := plugin;
		всё;
	кц;
кон Install;

проц RemovePlugins;
перем i : цел32;
нач
	(* Remove currently installed plugins *)
	нцДля i := 0 до длинаМассива(plugins)-1 делай
		если plugins[i] # НУЛЬ то
			plugins[i].Finalize;
			plugins[i] := НУЛЬ;
		всё;
	кц;
кон RemovePlugins;

проц Cleanup;
нач {единолично}
	RemovePlugins;
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
кон WMPerfMonPluginSerials.

WMPerfMonPluginSerials.Install ~	System.Free WMPerfMonPluginSerials ~
