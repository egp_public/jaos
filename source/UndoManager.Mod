модуль UndoManager; (** AUTHOR "???"; PURPOSE "Undo/Redo" *)

использует Texts;

конст
	None = 99;
	CStyle = 100;
	PStyle = 101;
	Attribute = 102;
	SpecialPiece = 103;

	AdvancedUndoStrategy = истина;

тип

	ListenerProc* = проц {делегат} (nrUndos, nrRedos : размерМЗ);

	UndoManager*= окласс(Texts.UndoManager)
		перем
			text: Texts.Text;
			undoQu, redoQu: Buffer;
			objUpdate: StyleUpdateInfo;
			nrUndoUpdates-, nrRedoUpdates-: размерМЗ;
			nrUpdatesListener* : ListenerProc;
			redo: булево;
			actualPos-: размерМЗ;

		проц & New*(memorySize: размерМЗ; redo: булево);
		нач
			если memorySize <= 0 то memorySize := 1001;всё;
			сам.redo := redo;
			text := НУЛЬ;
			нов(undoQu, memorySize);
			если redo то
				нов(redoQu, memorySize)
			иначе
				redoQu := НУЛЬ;
			всё;
			objUpdate := НУЛЬ;
			nrUndoUpdates := 0; nrRedoUpdates := 0;
			nrUpdatesListener := НУЛЬ;
			actualPos := 0;
		кон New;

		проц ResetRedo;
		нач
			nrRedoUpdates := 0;
			redoQu.Clear()
		кон ResetRedo;

		проц NextOperation(u: UpdateInfo);
		перем ui: UpdateInfo;
		нач
			если undoQu.IsFull() то
				ui := undoQu.RemoveOldest();
				умень(nrUndoUpdates);
			всё;
			undoQu.Push(u);
			увел(nrUndoUpdates);
		кон NextOperation;

		проц SaveOperation(u: UpdateInfo);
		нач
			если ~redo то возврат всё;
			redoQu.Push(u);
			увел(nrRedoUpdates);
		кон SaveOperation;

		проц {перекрыта}InsertText*(pos: размерМЗ; text: Texts.Text);
		перем i: InsertUpdateInfo; u:UpdateInfo; tr: Texts.TextReader; ucs32: симв32;
		нач
			ResetRedo;
			text.AcquireRead;
			если ~(undoQu.IsEmpty()) и (undoQu.Peek()  суть InsertUpdateInfo) и (text.GetLength() = 1) то
				u := undoQu.Peek();
				i := u(InsertUpdateInfo);
				если (~u.sealed) и (i.pos + i.len = pos) то
					i.t.AcquireWrite;
					i.t.CopyFromText(text, 0, text.GetLength(), i.len);
					i.len := i.len + text.GetLength();
					i.t.ReleaseWrite;

					нов(tr, text);
					tr.ReadCh(ucs32);
					если IsSeparator(ucs32) то i.sealed := истина всё;
					text.ReleaseRead;

					возврат
				всё
			всё;
			text.ReleaseRead;
			нов(i, pos, text);
			NextOperation(i);
		кон InsertText;

		проц {перекрыта}DeleteText*(pos: размерМЗ; text: Texts.Text);
		перем d: DeleteUpdateInfo; u: UpdateInfo;
		нач
			ResetRedo;
			text.AcquireRead;
			если (~ undoQu.IsEmpty()) и (undoQu.Peek() суть DeleteUpdateInfo) и (text.GetLength() = 1) то
				u := undoQu.Peek();
				d := u(DeleteUpdateInfo);
				если (d.pos = pos) то (* Delete key *)
					d.t.AcquireWrite;
					d.t.CopyFromText(text, 0, text.GetLength(),d.len);
					d.len := d.len + text.GetLength();
					d.t.ReleaseWrite;
					возврат
				аесли (d.pos - 1 = pos) то (* Backspace key *)
					d.t.AcquireWrite;
					d.t.CopyFromText(text, 0, text.GetLength(), 0);
					d.pos := pos;
					d.len := d.len + text.GetLength();
					d.t.ReleaseWrite;
					возврат
				всё;
			всё;
			text.ReleaseRead;

			нов(d, pos, text);
			NextOperation(d);
		кон DeleteText;

		проц {перекрыта}BeginObjectChange*(pos: размерМЗ);
		нач
			нов(objUpdate, pos);
		кон BeginObjectChange;

		проц {перекрыта}ObjectChanged*(pos, len: размерМЗ; type: целМЗ; obj: динамическиТипизированныйУкль);
		нач
			objUpdate.Append(pos, len, obj, type)
		кон ObjectChanged;

		проц {перекрыта}EndObjectChange*(len: размерМЗ; type: целМЗ; to: динамическиТипизированныйУкль);
		нач
			ResetRedo;
			objUpdate.len := len;
			objUpdate.type := type;
			objUpdate.new := to;
			NextOperation(objUpdate);
			objUpdate := НУЛЬ
		кон EndObjectChange;

		проц {перекрыта}SetText*(text: Texts.Text);
		нач
			сам.text := text;
		кон SetText;

		проц {перекрыта}Undo*;
		перем temp: Texts.Text; ui: UpdateInfo;
		нач
			temp := сам.text;
			если temp # НУЛЬ то
				temp.AcquireWrite;
				если ~ undoQu.IsEmpty() то
					ui := undoQu.Pop();
					temp.SetUndoManager(НУЛЬ); (* Disable recording *)
					ui.Undo(temp);
					temp.SetUndoManager(сам); (* Re-enable recording *)
					умень(nrUndoUpdates);
					SaveOperation(ui);
					actualPos := ui.pos;
				всё;
				temp.ReleaseWrite
			всё
		кон Undo;

		проц {перекрыта}Redo*;
		перем temp: Texts.Text; ui: UpdateInfo;
		нач
			если ~redo то возврат всё;
			temp := сам.text;
			если temp # НУЛЬ то
				temp.AcquireWrite;
				если ~ redoQu.IsEmpty() то
					ui := redoQu.Pop();
					temp.SetUndoManager(НУЛЬ); (* Disable recording *)
					ui.Redo(temp);
					temp.SetUndoManager(сам); (* Re-enable recording *)
					умень(nrRedoUpdates);
					NextOperation(ui);
					actualPos := ui.pos;
				всё;
				temp.ReleaseWrite
			всё
		кон Redo;

		проц {перекрыта}InformListeners*;
		перем l : ListenerProc;
		нач
			l := nrUpdatesListener;
			если (l # НУЛЬ) то l(nrUndoUpdates, nrRedoUpdates); всё;
		кон InformListeners;

	кон UndoManager;

	Buffer = окласс
		перем head, num: размерМЗ; buffer: укль на массив из UpdateInfo;

		проц Push*(x: UpdateInfo);
		нач
			утв(num <= длинаМассива(buffer));
			buffer[(head+num) остОтДеленияНа длинаМассива(buffer)] := x;
			увел(num)
		кон Push;

		проц RemoveOldest*(): UpdateInfo;
		перем x: UpdateInfo;
		нач
			x := buffer[head];
			head := (head+1) остОтДеленияНа длинаМассива(buffer);
			умень(num);
			возврат x
		кон RemoveOldest;

		проц Peek*(): UpdateInfo;
		нач
			возврат buffer[((head+num - 1) остОтДеленияНа длинаМассива(buffer))]
		кон Peek;

		проц Pop*(): UpdateInfo;
		перем x: UpdateInfo;
		нач
			x := buffer[((head+num - 1) остОтДеленияНа длинаМассива(buffer))];
			умень(num);
			возврат x
		кон Pop;

		проц IsFull*(): булево;
		нач
			возврат num = длинаМассива(buffer)
		кон IsFull;

		проц IsEmpty*(): булево;
		нач
			возврат num = 0
		кон IsEmpty;

		проц Clear*;
		нач
			head := 0; num := 0;
		кон Clear;

		проц &Init*(n: размерМЗ);
		нач
			head := 0; num := 0; нов(buffer, n)
		кон Init;

	кон Buffer;

	UpdateInfo= окласс
		перем
			pos : размерМЗ;
			sealed*: булево;

		проц Undo(text: Texts.Text); (* abstract *)
		кон Undo;

		проц Redo(text: Texts.Text); (* abstract *)
		кон Redo;

	кон UpdateInfo;

тип

	InsertUpdateInfo= окласс(UpdateInfo)
		перем
			len: размерМЗ;
			t: Texts.Text;

		проц &New*(pos: размерМЗ; text: Texts.Text);
		нач
			сам.pos := pos;
			сам.t := text;
			t.AcquireRead;
			сам.len := t.GetLength();
			t.ReleaseRead;
		кон New;

		проц {перекрыта}Undo(text: Texts.Text);
		нач
			t.AcquireRead;
			text.Delete(pos, t.GetLength());
			t.ReleaseRead;
		кон Undo;

		проц {перекрыта}Redo(text: Texts.Text);
		нач
			t.AcquireRead;
			text.CopyFromText(t, 0, t.GetLength(), pos);
			t.ReleaseRead;
		кон Redo;

	кон InsertUpdateInfo;

	DeleteUpdateInfo= окласс(UpdateInfo)
		перем
			len: размерМЗ;
			t: Texts.Text;

		проц &New*(pos: размерМЗ; text: Texts.Text);
		нач
			сам.pos := pos;
			сам.t := text;
			t.AcquireRead;
			сам.len := t.GetLength();
			t.ReleaseRead;
		кон New;

		проц {перекрыта}Undo(text: Texts.Text);
		нач
			t.AcquireRead;
			text.CopyFromText(t, 0, t.GetLength(), pos);
			t.ReleaseRead;
		кон Undo;

		проц {перекрыта}Redo(text: Texts.Text);
		нач
			t.AcquireRead;
			text.Delete(pos, t.GetLength());
			t.ReleaseRead;
		кон Redo;

	кон DeleteUpdateInfo;


	StyleInfo= укль на запись
		next: StyleInfo;
		pos, len: размерМЗ;
		style: динамическиТипизированныйУкль;
		type: целМЗ;
	кон;


	StyleUpdateInfo= окласс(UpdateInfo)
		перем
			len: размерМЗ;
			type: целМЗ;
			new: динамическиТипизированныйУкль;
			old: StyleInfo;

		проц &New*(pos: размерМЗ);
		нач
			сам.pos := pos;
		кон New;

		проц Append(pos, len: размерМЗ; style: динамическиТипизированныйУкль; type: целМЗ);
		перем ai: StyleInfo;
		нач
			нов(ai);
			ai.next := old;
			ai.pos := pos;
			ai.len := len;
			ai.style := style;
			ai.type := type;
			old := ai
		кон Append;

		проц SetObject(new: динамическиТипизированныйУкль);
		нач
			сам.new := new;
		кон SetObject;

		проц SetLen(len: размерМЗ);
		нач
			сам.len := len;
		кон SetLen;

		проц SetStyle*(textpos, len: размерМЗ; style: динамическиТипизированныйУкль);
		кон SetStyle;

		проц {перекрыта}Undo(text: Texts.Text);
		перем cur: StyleInfo;
		нач
			cur := old;
			нцПока cur # НУЛЬ делай
				просей cur.type из
					Attribute:
						если cur.style # НУЛЬ то
							text.SetAttributes(cur.pos, cur.len, cur.style(Texts.Attributes))
						иначе
							text.SetAttributes(cur.pos, cur.len, НУЛЬ)
						всё
					| CStyle:
						если cur.style # НУЛЬ то
							text.SetCharacterStyle(cur.pos, cur.len, cur.style(Texts.CharacterStyle))
						иначе
							text.SetCharacterStyle(cur.pos, cur.len, НУЛЬ)
						всё
					| PStyle:
						если cur.style # НУЛЬ то
							text.SetParagraphStyle(cur.pos, cur.len, cur.style(Texts.ParagraphStyle))
						иначе
							text.SetParagraphStyle(cur.pos, cur.len, НУЛЬ)
						всё
				всё;
				cur := cur.next;
			кц;
		кон Undo;

		проц {перекрыта}Redo(text: Texts.Text);
		нач
			просей type из
				Attribute:
					если new # НУЛЬ то
						text.SetAttributes(pos, len, new(Texts.Attributes))
					иначе
						text.SetAttributes(pos, len, НУЛЬ)
					всё
				| CStyle:
					если new # НУЛЬ то
						text.SetCharacterStyle(pos, len, new(Texts.CharacterStyle))
					иначе
						text.SetCharacterStyle(pos, len, НУЛЬ)
					всё
				| PStyle:
					если new # НУЛЬ то
						text.SetParagraphStyle(pos, len, new(Texts.ParagraphStyle))
					иначе
						text.SetParagraphStyle(pos, len, НУЛЬ)
					всё
			всё
		кон Redo;

	кон StyleUpdateInfo;


проц IsSeparator(uc: Texts.Char32): булево;
нач
	просей кодСимв32(uc) из
		 Texts.NewLineCharCode: возврат истина
		 | Texts.TabCharCode: если AdvancedUndoStrategy то возврат истина иначе возврат ложь всё
		 | Texts.SpaceCharCode: если AdvancedUndoStrategy то возврат истина иначе возврат ложь всё
	иначе
		возврат ложь;
	всё
кон IsSeparator;

кон UndoManager.
