модуль TFScopeToolsUCS32;

использует
	ЛогЯдра, TS := TFTypeSysUCS32, StringsUCS32, UCS32;

перем Depth : цел32;


проц FindType*(di : TS.Designator; scope : TS.Scope) : TS.Type;
перем
	d : TS.Expression;
	no : TS.NamedObject;
	s : массив 64 из симв32;
	m : TS.Module;
нач
	d := di;
	если (scope = НУЛЬ) или  (d = НУЛЬ) то возврат НУЛЬ всё;
	TS.s.GetString(d(TS.Ident).name,s);
	(* KernelLog.String("FindType: looking for ");
	KernelLog.String(s); KernelLog.Ln; *)

	no := scope.Find(s, истина);

	если no = НУЛЬ то возврат НУЛЬ всё;
	(* follow import *)
	если no суть TS.Import то m := TS.GetModule(no(TS.Import));

		если m = НУЛЬ то возврат НУЛЬ всё;
		scope := m.scope;
		если scope # НУЛЬ то
			d := d.next;
			нцПока (d # НУЛЬ) и (scope # НУЛЬ) делай
				TS.s.GetString(d(TS.Ident).name,s);
				no := scope.Find(s, ложь);
				если no # НУЛЬ то
					scope := no.scope;
				иначе scope := НУЛЬ
				всё
			кц
		всё
	всё;
	если no = НУЛЬ то возврат НУЛЬ всё;
	если no суть TS.TypeDecl то
		возврат no(TS.TypeDecl).type
	всё;
	возврат НУЛЬ
кон FindType;

проц DealiaseType*(t : TS.Type) : TS.Type;
перем pos : цел32; res : TS.Type; 
нач
	Depth := Depth + 1;
	если Depth >= 10 то 
		Depth := 0; 
		ЛогЯдра.пСтроку8("DealiaseType SO: basictype = "); 
		ЛогЯдра.пЦел64(t.basicType,0);
		если t.qualident # НУЛЬ то
			ЛогЯдра.пСтроку8(", have a qualident: ");
			ShowDesignator(t.qualident);
		всё;
		СТОП(3800) 
	всё;
	pos := -1;
	если t = НУЛЬ то Depth := Depth - 1; возврат НУЛЬ всё;
	если t.container = НУЛЬ то Depth := Depth - 1; возврат НУЛЬ всё;
	если (t.container # НУЛЬ)  и (t.container.owner # НУЛЬ) то
		pos := t.container.owner.pos.a
	всё;

	если t.kind = TS.TAlias то
		res := DealiaseType(FindType(t.qualident, t.container));
		Depth := Depth - 1;
		возврат res;
	иначе 
		Depth := Depth - 1;
		возврат t
	всё
кон DealiaseType;

(* For debugging *)
проц ShowDesignator*(d : TS.Designator);
перем s : массив 64 из симв32;
нач
	если d суть TS.Ident то TS.s.GetString(d(TS.Ident).name, s); ЛогЯдра.пСтроку32_вЮ8(s)
	аесли d суть TS.Index то
		ЛогЯдра.пСтроку8("[");
		ЛогЯдра.пСтроку8("]");
	аесли d суть TS.ActualParameters то
		ЛогЯдра.пСтроку8("(");
		ЛогЯдра.пСтроку8(")");
	всё;
	если (d.next # НУЛЬ) то
		если (d суть TS.Ident) то ЛогЯдра.пСтроку8(".") всё;
		ShowDesignator(d.next(TS.Ident))
	всё
кон ShowDesignator;

(* For debugging *)
проц ShowType*(t : TS.Type);
нач
	ЛогЯдра.пСтроку8("{");
	просей t.kind из
		|TS.TBasic : ЛогЯдра.пСтроку8("Basic type "); ЛогЯдра.пЦел64(t.basicType, 0);
		|TS.TAlias : ЛогЯдра.пСтроку8("Alias to "); ShowDesignator(t.qualident)
		|TS.TObject : ЛогЯдра.пСтроку8("OBJECT");
		|TS.TArray : ЛогЯдра.пСтроку8("[]");
		|TS.TPointer : ЛогЯдра.пСтроку8("POINTER TO ");
		|TS.TRecord :  ЛогЯдра.пСтроку8("RECORD");
		|TS.TEnum : ЛогЯдра.пСтроку8("ENUM");
		|TS.TProcedure : ЛогЯдра.пСтроку8("PROCEDURE");
	иначе ЛогЯдра.пСтроку8("UNKNOWN");
	всё;
	ЛогЯдра.пСтроку8("}");
кон ShowType;


(* GetSourceReference is for the "go to source". It shows the scope path in the form 
    Module.Procedure.SubProcedure and filename *)
проц GetSourceReference*(no : TS.NamedObject; перем filename, scopePath : массив из симв32 );
перем s : TS.Scope;
	stack : массив 32 из TS.NamedObject;
	tos : цел32;
нач
	UCS32.COPYJQvJQ(Лит32(""), scopePath);
	UCS32.COPYJQvJQ(Лит32(""), filename);
	если no = НУЛЬ то возврат всё;
	tos := 0;
	stack[tos] := no; увел(tos);
	s := no.container;
	нцПока s # НУЛЬ делай
		если s.owner # НУЛЬ то
			stack[tos] := s.owner; увел(tos);
		всё;
		s := s.parent
	кц;
	если (stack[tos - 1] # НУЛЬ) и (stack[tos - 1] суть TS.Module) то
		если stack[tos - 1](TS.Module).filename # НУЛЬ то
			UCS32.COPYJQvJQ(stack[tos - 1](TS.Module).filename^, filename)
		всё
	всё;

	нцДо
		умень(tos);
		StringsUCS32.Append(scopePath, stack[tos].name^);
		если tos # 0 то StringsUCS32.Append(scopePath, Лит32(".")) всё
	кцПри tos = 0
кон GetSourceReference;


(* GetSourceReference is for the "go to source". It shows the scope path in the form 
    Module.Procedure.SubProcedure and filename *)
проц GetFilenameByQualifiedName*(qualifiedName: StringsUCS32.String) : StringsUCS32.String; 
перем 
	moduleName : StringsUCS32.String;
	module : TS.Module;
	tos : размерМЗ;
нач
	tos := StringsUCS32.Pos(UCS32.U8vNovJQ('.')^, qualifiedName^);
	если tos = -1 то
		возврат НУЛЬ; всё;
	moduleName := StringsUCS32.Substring(0, tos, qualifiedName^);
	ЛогЯдра.пСтроку32_вЮ8(Лит32("GetFilenameByQualifiedName returned ")); 
	ЛогЯдра.пСтроку32_вЮ8(qualifiedName^); ЛогЯдра.пВК_ПС;
	module := TS.ReadSymbolFile(moduleName^); 
	если module = НУЛЬ то 
		возврат НУЛЬ
	иначе
		возврат module.filename всё кон GetFilenameByQualifiedName;

проц ID*(no : TS.NamedObject );
перем filename : массив 256 из симв32;
	scopePath : массив 256 из симв32;
нач
	GetSourceReference(no, filename, scopePath);
	если filename # Лит32("") то
		ЛогЯдра.пСтроку32_вЮ8(filename); ЛогЯдра.пВК_ПС;
	всё;
	ЛогЯдра.пСтроку32_вЮ8(scopePath); ЛогЯдра.пВК_ПС
кон ID;

(* IDScope prints reversed path to the scope s
    in the form "ownerName/ownersOwnerName/etc *)
проц IDScope*(s : TS.Scope);
нач
	нцПока s # НУЛЬ делай
		если s.owner # НУЛЬ то
			если s.owner.name = НУЛЬ то
				ЛогЯдра.пСтроку32_вЮ8(Лит32("{NIL}"))
			иначе
				 ЛогЯдра.пСтроку32_вЮ8(s.owner.name^); 
				 ЛогЯдра.пСтроку32_вЮ8(Лит32("/"));
			всё
		всё; s := s.parent
	кц;
кон IDScope;

проц QualidentToString*(scope : TS.Scope; q : TS.Designator) : StringsUCS32.String;
перем str : массив 128 из симв32; no : TS.NamedObject;
	qStr : массив 1024 из симв32;
нач
	UCS32.COPYJQvJQ(Лит32(""), qStr);
	если (q # НУЛЬ) и (q суть TS.Ident) то
		(* check if it is an alias import *)
		TS.s.GetString(q(TS.Ident).name, str); no := scope.Find(str, истина);
		если (no # НУЛЬ) и (no суть TS.Import) то
			StringsUCS32.Append(qStr, no(TS.Import).import^); StringsUCS32.Append(qStr, Лит32(".")); 
			если (q.next # НУЛЬ) и (q.next суть TS.Designator) то
				q := q.next(TS.Designator)
			иначе
				q := НУЛЬ
			всё
		всё;

		нцПока (q # НУЛЬ) и (q суть TS.Ident) делай
			TS.s.GetString(q(TS.Ident).name, str); StringsUCS32.Append(qStr, str);
			если q.next # НУЛЬ то StringsUCS32.Append(qStr, Лит32(".")) всё;
			если (q.next # НУЛЬ) и (q.next суть TS.Designator) то
				q := q.next(TS.Designator)
			иначе
				q := НУЛЬ
			всё
		кц
	всё;
	возврат StringsUCS32.NewString(qStr)
кон  QualidentToString;

нач 
Depth := 0; 
кон TFScopeToolsUCS32.
