модуль Archives; (** AUTHOR "FN"; PURPOSE "Abstract Archive Object"; *)

использует
	Configuration, Strings, Потоки;

конст
	ArchiveNameSize = 128;

тип
	StringObject*= окласс
	перем value -: массив 256 из симв8;

		проц & Init*(конст name : массив из симв8);
		перем i : размерМЗ;
		нач
			нцДля i := 0 до длинаМассива(name)-1 делай
				value[i] := name[i];
				если name[i] = 0X то возврат всё
			кц
		кон Init;

	кон StringObject;

	EntryInfo*= окласс

		проц GetName*() : Strings.String;
		нач СТОП(301)
		кон GetName;

		проц GetSize*() : цел32;
		нач СТОП(301)
		кон GetSize;

		проц GetInfoString*() : Strings.String;
		нач СТОП(301)
		кон GetInfoString;

	кон EntryInfo;

	Index* = укль на массив из EntryInfo;

	Archive* = окласс
	перем name *: массив ArchiveNameSize из симв8;

		проц Acquire*;
		нач СТОП(301)
		кон Acquire;

		проц Release*;
		нач СТОП(301)
		кон Release;

		проц GetIndex*() : Index;
		нач СТОП(301)
		кон GetIndex;

		проц GetEntryInfo*(конст name : массив из симв8) : EntryInfo;
		нач СТОП(301)
		кон GetEntryInfo;

		проц RemoveEntry*(конст name : массив из симв8);
		нач СТОП(301)
		кон RemoveEntry;

		проц RenameEntry*(конст from, to : массив из симв8) : EntryInfo;
		нач СТОП(301)
		кон RenameEntry;

		проц OpenSender*(конст name : массив из симв8) : Потоки.Делегат˛реализующийЗаписьВПоток;
		нач СТОП(301)
		кон OpenSender;

		проц OpenReceiver*(конст name : массив из симв8) : Потоки.Делегат˛реализующийЧтениеИзПотока;
		нач СТОП(301)
		кон OpenReceiver;

		проц Copy*(конст name : массив из симв8) : Archive;
		нач СТОП(301)
		кон Copy;

	кон Archive;

	FactoryProcedure = проц (name : StringObject) : Archive;

	(* ----- api ----------------------------------------------------- *)

	проц Old*(конст name, type : массив из симв8) : Archive;
	перем
		old : FactoryProcedure;
		a : StringObject;
		archive : Archive;
		config, factoryName : массив 128 из симв8;
		res : целМЗ;
	нач
		archive := НУЛЬ;
		config := "Archives."; Strings.Append(config, type);
		Configuration.Get(config, factoryName, res);
		если (res = Configuration.Ok) то
			дайПроцПоИмени(factoryName, "Old", old);
			если (old # НУЛЬ) то
				нов(a, name);
				archive := old(a);
			всё;
		всё;
		возврат archive;
	кон Old;

	проц New*(конст name, type : массив из симв8) : Archive;
	перем
		new : FactoryProcedure;
		a : StringObject;
		archive : Archive;
		config, factoryName : массив 128 из симв8;
		res : целМЗ;
	нач
		archive := НУЛЬ;
		config := "Archives."; Strings.Append(config, type);
		Configuration.Get(config, factoryName, res);
		если (res = Configuration.Ok) то
			дайПроцПоИмени(factoryName, "New", new);
			если (new # НУЛЬ) то
				нов(a, name);
				archive := new(a);
			всё;
		всё;
		возврат archive;
	кон New;

кон Archives.
