модуль MainMenu; (** AUTHOR "TF"; PURPOSE "Menu for Bluebottle"; *)

использует
	ЛогЯдра, XML, Modules, Files, Inputs,
	WMMessages, WMStandardComponents, WMComponents, WMTabComponents,
	Strings, WM := WMWindowManager;

конст
	MenuFilePrefix = "MenuPage";

тип
	Window* = окласс (WMComponents.FormWindow)
	перем
		tabs : WMTabComponents.Tabs;
		pages : массив 100 из WMComponents.VisualComponent;
		tabList : массив 100 из WMTabComponents.Tab;
		currentPage : WMComponents.VisualComponent;
		currentPageNr : размерМЗ;
		page : WMStandardComponents.Panel;
		moveToFront : булево;

		проц CreateForm(): WMComponents.VisualComponent;
		перем
			panel : WMStandardComponents.Panel;
			pagePanel : WMStandardComponents.Panel;
			tabs : WMTabComponents.Tabs;
		нач
			нов(panel); panel.bounds.SetExtents(1024, 60); panel.fillColor.Set(0); panel.takesFocus.Set(истина);

			нов(tabs); tabs.fillColor.Set(000600080H); tabs.bounds.SetHeight(20); tabs.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(tabs);
			сам.tabs := tabs;

			нов(pagePanel); pagePanel.fillColor.Set(0H); pagePanel.alignment.Set(WMComponents.AlignClient);
			panel.AddContent(pagePanel);
			page := pagePanel;

			возврат panel
		кон CreateForm;

		проц &New*;
		перем vc : WMComponents.VisualComponent;
			view : WM.ViewPort;
		нач
			vc := CreateForm();
			moveToFront := истина;
			currentPageNr := -1;
			tabs.onSelectTab.Add(TabSelected);

			Init(vc.bounds.GetWidth(), vc.bounds.GetHeight(), истина);
			SetContent(vc);
			SetTitle(Strings.NewString("MainMenu"));
			pointerThreshold := 10;
			manager := WM.GetDefaultManager();
			view := WM.GetDefaultView();
			manager.Add(0, view.height0 - GetHeight(), сам, {WM.FlagNavigation, WM.FlagHidden});
		кон New;

		проц {перекрыта}PointerMove*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
		нач
			PointerMove^(x, y, keys);
			moveToFront := ложь;
			manager.ToFront(сам);
		кон PointerMove;

		проц {перекрыта}PointerLeave*;
		нач
			PointerLeave^;
			moveToFront := истина;
		кон PointerLeave;

		проц SetOriginator*(extView : динамическиТипизированныйУкль);
		перем view : WM.ViewPort;
		нач
			если (extView # НУЛЬ) и (extView суть WM.ViewPort) то
				view := extView(WM.ViewPort)
			иначе view := WM.GetDefaultView()
			всё;
			Refresh(НУЛЬ, НУЛЬ);
			если ~(WM.FlagNavigation в сам.flags) то
				manager.SetWindowPos(сам, округлиВниз(view.range.l), округлиВниз(view.range.b) - GetHeight());
			всё;
			manager.ToFront(сам)
		кон SetOriginator;

		проц UpdatePages;
		перем i : размерМЗ;
			tab : WMTabComponents.Tab;
			s : Strings.String;
		нач
			DisableUpdate;
			tabs.RemoveAllTabs;
			если currentPage # НУЛЬ то page.RemoveContent(currentPage);
				currentPage := НУЛЬ
			всё;
			если currentPageNr >= 0 то currentPage := pages[currentPageNr] всё;
			нцДля i := 0 до 99 делай
				tabList[i] := НУЛЬ;
				если pages[i] # НУЛЬ то
					pages[i].alignment.Set(WMComponents.AlignClient);
					tab := tabs.NewTab();
					tabs.AddTab(tab);
					tabList[i] := tab;
					s := pages[i].GetAttributeValue("caption");
					если s = НУЛЬ то s := Strings.NewString("Untitled") всё;
					tabs.SetTabCaption(tab, s);
					tabs.SetTabData(tab, pages[i])
				всё
			кц;
			если currentPage = НУЛЬ то
				i := 0;
				нцПока (i < 100) и (currentPage = НУЛЬ) делай
					если pages[i] # НУЛЬ то currentPage := pages[i]; currentPageNr := i всё;
					увел(i);
				кц
			всё;
			EnableUpdate;
			если currentPage # НУЛЬ то
				page.AddContent(currentPage);
				currentPage.Reset(сам, НУЛЬ);
				page.AlignSubComponents;
				page.Invalidate;
				если tabList[currentPageNr] # НУЛЬ то tabs.Select(tabList[currentPageNr]) всё
			всё;
			tabs.Invalidate
		кон UpdatePages;

		проц TryLoad(конст name : массив из симв8; pos : цел32);
		перем x : XML.Content;
		нач
			если (pos >= 0) и (pos <= 99) то
				x := WMComponents.Load(name);
				если x # НУЛЬ то
					если x суть WMComponents.VisualComponent то
						pages[pos] := x(WMComponents.VisualComponent);
				 	всё
				 всё
			всё
		кон TryLoad;

		проц LoadPages*;
		перем mask : массив 64 из симв8;
			name : массив 256 из симв8; flags : мнвоНаБитахМЗ;
			time, date : цел32;
			size : Files.Size;
			i: размерМЗ;
			enumerator : Files.Enumerator;
			number: цел32;

			проц IsNum(ch : симв8) : булево;
			нач
				возврат (ch >= '0') и (ch <= '9')
			кон IsNum;

		нач
			DisableUpdate;
			нов(enumerator);
			нцДля i := 0 до 99 делай
				если pages[i] = currentPage то currentPageNr := i всё;
				pages[i] := НУЛЬ
			кц;
			mask := MenuFilePrefix;
			i := Strings.Length(mask);
			Strings.Append(mask, "*.XML");
			enumerator.Open(mask, {});
			нцПока enumerator.HasMoreEntries() делай
				если enumerator.GetEntry(name, flags, time, date, size) то
					i := Strings.Length(name);
					если IsNum(name[i - 6]) и IsNum(name[i - 5]) то
						number := (кодСимв8(name[i - 6]) - кодСимв8('0')) * 10 + (кодСимв8(name[i - 5]) - кодСимв8('0'));
						если pages[number] = НУЛЬ то (* use first incident for respecting the search path order *)
							TryLoad(name, number);
						всё;
					всё
				всё
			кц;
			enumerator.Close;
			EnableUpdate;
			UpdatePages
		кон LoadPages;

		проц Refresh(sender, data : динамическиТипизированныйУкль);
		нач
			LoadPages
		кон Refresh;

		проц TabSelected(sender, data : динамическиТипизированныйУкль);
		перем tab : WMTabComponents.Tab;
		нач
			если (data # НУЛЬ) и (data суть WMTabComponents.Tab) то
				DisableUpdate;
				page.RemoveContent(currentPage);
				tab := data(WMTabComponents.Tab);
				если (tab.data # НУЛЬ) и (tab.data суть WMComponents.VisualComponent) то
					currentPage := tab.data(WMComponents.VisualComponent);
					page.AddContent(currentPage);
					если ~currentPage.initialized то currentPage.Initialize всё;
					currentPage.Reset(сам, НУЛЬ);
					page.AlignSubComponents;
				всё;
				EnableUpdate;
				page.Invalidate
			всё
		кон TabSelected;

		проц {перекрыта}Close*;
		нач
			Close^;
			window := НУЛЬ
		кон Close;

	кон Window;

(* the starter decouples the sensitive callback from the WindowManager. *)
тип
	Starter = окласс
	перем
		originator : динамическиТипизированныйУкль;
		w : Window;

	проц &Init*(o : динамическиТипизированныйУкль);
	нач
		originator := o;
		w := НУЛЬ;
	кон Init;

	нач {активное}
		нач {единолично}
			если (window = НУЛЬ) то нов(window); w := window;
			иначе window.SetOriginator(originator);
			всё;
		кон;
		если (w # НУЛЬ) то w.LoadPages; всё;
	кон Starter;

перем
	window : Window;
	manager : WM.WindowManager;

(* This procedure is directly called by the window manager. It must be safe. *)
проц MessagePreview(перем m : WMMessages.Message; перем discard : булево);
перем starter : Starter;
нач
	если m.msgType = WMMessages.MsgKey то
		если (m.y = 0FF1BH) и ((m.flags * Inputs.Ctrl # {}) или (m.flags * Inputs.Meta # {})) то
			нов(starter, m.originator); discard := истина
		всё
	всё
кон MessagePreview;

проц Open*;
перем w : Window;
нач
	нач {единолично}
		если window = НУЛЬ то нов(window); w := window;
		иначе window.SetOriginator(НУЛЬ)
		всё;
	кон;
	если w # НУЛЬ то w.LoadPages всё;
кон Open;

проц Cleanup;
нач {единолично}
	ЛогЯдра.пСтроку8("Cleanup"); ЛогЯдра.пВК_ПС;
	(* removal must be done in all cases to avoid system freeze *)
	manager.RemoveMessagePreview(MessagePreview);
	если window # НУЛЬ то window.Close  всё;
кон Cleanup;

нач
	manager := WM.GetDefaultManager();
	Modules.InstallTermHandler(Cleanup)
кон MainMenu.

System.Free MainMenu WMTabComponents ~
MainMenu.Open ~
