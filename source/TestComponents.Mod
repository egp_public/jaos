модуль TestComponents; (** AUTHOR "staubesv"; PURPOSE "Simple test framework  for visual components"; *)

использует
	Modules, ЛогЯдра, Commands, Strings, XML, Repositories,
	WMWindowManager, WMMessages, WMComponents,
	WMStandardComponents, WMStringGrids;

тип

	KillerMsg = окласс
	кон KillerMsg;

тип

	TestBase = окласс (WMComponents.FormWindow)

		проц CreateForm() : WMComponents.VisualComponent; (* abstract *)
		кон CreateForm;

		проц &New*(конст windowTitle : массив из симв8);
		перем vc : WMComponents.VisualComponent;
		нач
			IncCount;
			vc := CreateForm();
			Init(vc.bounds.GetWidth(), vc.bounds.GetHeight(), ложь);
			SetContent(vc);
			SetTitle(Strings.NewString(windowTitle));
			WMWindowManager.DefaultAddWindow(сам);
		кон New;

		проц {перекрыта}Close*;
		нач
			Close^;
			DecCount
		кон Close;

		проц {перекрыта}Handle*(перем x : WMMessages.Message);
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) и (x.ext суть KillerMsg) то Close
			иначе Handle^(x)
			всё
		кон Handle;

	кон TestBase;

тип

	TestVisualComponent = окласс(TestBase)
	перем
		vc : WMComponents.VisualComponent;

		проц {перекрыта}CreateForm() : WMComponents.VisualComponent;
		нач
			возврат vc;
		кон CreateForm;

		проц &NewTest*(конст windowTitle : массив из симв8; vc : WMComponents.VisualComponent);
		нач
			утв(vc # НУЛЬ);
			сам.vc := vc;
			vc.bounds.SetExtents(640, 480);
			vc.alignment.Set(WMComponents.AlignClient);
			New(windowTitle);
		кон NewTest;

	кон TestVisualComponent;

тип

	StandardComponents = окласс(TestBase)
	перем
		label : WMStandardComponents.Label;

		проц SayHello(sender, data : динамическиТипизированныйУкль);
		нач
			ЛогЯдра.пСтроку8("SayHello"); ЛогЯдра.пВК_ПС
		кон SayHello;

		проц SayBye(sender, data : динамическиТипизированныйУкль);
		нач
			ЛогЯдра.пСтроку8("SayBye"); ЛогЯдра.пВК_ПС
		кон SayBye;

		проц {перекрыта}CreateForm() : WMComponents.VisualComponent;
		перем
			panel : WMStandardComponents.Panel;
			toolbar : WMStandardComponents.Panel;
			button : WMStandardComponents.Button;
		нач
			нов(panel);
			panel.bounds.SetExtents(640, 480);
			panel.fillColor.Set(цел32(0FFFFFFFFH));
			panel.takesFocus.Set(истина);

			нов(toolbar);
			toolbar.fillColor.Set(000FF00FFH);
			toolbar.bounds.SetHeight(20);
			toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);

			нов(button);
			button.alignment.Set(WMComponents.AlignLeft);
			button.caption.SetAOC("Hello");
			button.onClick.Add(SayHello);
			toolbar.AddContent(button);

			нов(button);
			button.alignment.Set(WMComponents.AlignLeft);
			button.caption.SetAOC("Bye");
			button.onClick.Add(SayBye);
			toolbar.AddContent(button);

			нов(label);
			label.bounds.SetHeight(20);
			label.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(label);

			возврат panel
		кон CreateForm;

	кон StandardComponents;

тип

	StringGrids = окласс(TestBase)
	перем
		label: WMStandardComponents.Label;
		tabs : WMStringGrids.StringGrid;

		проц Test(sender, data : динамическиТипизированныйУкль);
		перем i : цел32; str : массив 20 из симв8;
		нач
			tabs.model.Acquire;
			нцДля i := 0 до 4 * 5 - 1 делай
				Strings.IntToStr(i, str);
				tabs.model.SetCellText(i остОтДеленияНа 4, i DIV 4, Strings.NewString(str))
			кц;
			tabs.model.Release;
		кон Test;

		проц {перекрыта}CreateForm(): WMComponents.VisualComponent;
		перем
			panel : WMStandardComponents.Panel;
			toolbar: WMStandardComponents.Panel;
			button : WMStandardComponents.Button;
		нач
			нов(panel); panel.bounds.SetExtents(800, 700); panel.fillColor.Set(цел32(0FFFFFFFFH)); panel.takesFocus.Set(истина);

			нов(toolbar); toolbar.fillColor.Set(000FF00FFH); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);

			нов(button); button.alignment.Set(WMComponents.AlignLeft); button.caption.SetAOC("Test");
			button.onClick.Add(Test);
			toolbar.AddContent(button);

			нов(label); label.bounds.SetHeight(20);label.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(label);

			нов(tabs);
			tabs.alignment.Set(WMComponents.AlignClient);
			tabs.fillColor.Set(цел32(0FF0000FFH));
			panel.AddContent(tabs);
			tabs.model.Acquire;
			tabs.model.SetNofCols(4);
			tabs.model.SetNofRows(10);
			tabs.model.Release;

			возврат panel
		кон CreateForm;

	кон StringGrids;

перем
	nofWindows : цел32;

проц Test*(context : Commands.Context); (** name ~ *)
перем test : TestVisualComponent; name : массив 128 из симв8; element : XML.Element;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name);
	element := Repositories.registry.InstantiateElement(name);
	если (element # НУЛЬ) то
		если (element суть WMComponents.VisualComponent) то
			нов(test, name, element(WMComponents.VisualComponent));
		иначе
			context.out.пСтроку8("Component '"); context.out.пСтроку8(name); context.out.пСтроку8("' is not a VisualComponent.");
			context.out.пВК_ПС;
		всё;
	иначе
		context.out.пСтроку8("Component '"); context.out.пСтроку8(name); context.out.пСтроку8("' not found");
		context.out.пВК_ПС;
	всё;
кон Test;

проц TestComponent*(vc : WMComponents.VisualComponent);
перем test : TestVisualComponent;
нач
	утв(vc # НУЛЬ);
	нов(test, "Visual Component Test", vc);
кон TestComponent;

проц TestStandardComponents*;
перем test : StandardComponents;
нач
	нов(test, "StandardComponents Test");
кон TestStandardComponents;

проц TestStringGrids*;
перем test : StringGrids;
нач
	нов(test, "StringGrids Test");
кон TestStringGrids;

проц IncCount;
нач {единолично}
	увел(nofWindows)
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows)
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WMWindowManager.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WMWindowManager.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0)
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон TestComponents.

TestComponents.TestStandardComponents ~
TestComponents.TestStringGrids ~

TestComponents.Test ComponentList ~

System.Free TestComponents ~
