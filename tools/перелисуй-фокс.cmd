chcp 65001
@set JAOS_ROOT=%~dp0..
cd %JAOS_ROOT%\source
call winmerge LisScanner.перев.XML FoxScanner.перев.XML
call winmerge LisSyntaxTree.перев.XML FoxSyntaxTree.перев.XML
call winmerge LisSections.Mod FoxSections.Mod
call winmerge LisBinaryCode.Mod FoxBinaryCode.Mod
call winmerge LisFingerprinter.Mod FoxFingerprinter.Mod
call winmerge LisTextCompiler.Mod FoxTextCompiler.Mod
call winmerge LisInterfaceComparison.Mod FoxInterfaceComparison.Mod
call winmerge LisCodeGenerators.Mod FoxCodeGenerators.Mod
call winmerge LisAMD64Assembler.Mod FoxAMD64Assembler.Mod
call winmerge LisGenericObjectFile.Mod FoxGenericObjectFile.Mod
call winmerge LisAssembler.Mod FoxAssembler.Mod
call winmerge LisAMDBackend.Mod FoxAMDBackend.Mod
call winmerge LisARMInstructionSet.Mod FoxARMInstructionSet.Mod
call winmerge LisARMAssembler.Mod FoxARMAssembler.Mod
call winmerge LisARMBackend.Mod FoxARMBackend.Mod
call winmerge LisОтлПеч.Mod FoxОтлПеч.Mod
call winmerge LisГенЗагСлов.Mod FoxГенЗагСлов.Mod
call winmerge LisCompiler.Mod Compiler.Mod
call winmerge NoMacro.LisOberonFrontend.Mod NoMacro.FoxOberonFrontend.Mod
call winmerge Macro.LisOberonFrontend.Mod Macro.FoxOberonFrontend.Mod
rem таких файлов нет, а надо сделать!
call winmerge LisMacroИПП.Mod FoxMacroИПП.Mod
call winmerge LisIntermediateBackend.Mod FoxIntermediateBackend.Mod
call winmerge LisIntermediateCode.Mod FoxIntermediateCode.Mod
call winmerge LisSemanticChecker.Mod FoxSemanticChecker.Mod
call winmerge LisPrintout.Mod FoxPrintout.Mod
call winmerge LisGlobal.Mod FoxGlobal.Mod
call winmerge LisПереводчик.Mod FoxПереводчик.Mod
call winmerge LisУтилитыДляПереводаКода.Mod FoxУтилитыДляПереводаКода.Mod
call winmerge LisTextualSymbolFile.Mod FoxTextualSymbolFile.Mod
call winmerge LisSyntaxTree.Mod FoxSyntaxTree.Mod
call winmerge LisScanner.Mod FoxScanner.Mod
call winmerge LisParser.Mod FoxParser.Mod
call winmerge LisBackend.Mod FoxBackend.Mod
call winmerge LisFormats.Mod FoxFormats.Mod
call winmerge LisFrontend.Mod FoxFrontend.Mod
