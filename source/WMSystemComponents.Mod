модуль WMSystemComponents;	(** AUTHOR "TF/UG"; PURPOSE "Components for File-Listings etc"; *)

использует
	Files, Dates, Strings, XML, XMLObjects, WMProperties, WMEvents, WMComponents, WMTrees, WMGrids, WMStringGrids,
	WMRectangles, WMGraphics, Raster,
	ЛогЯдра, Configuration,
	WMDropTarget, Texts, TextUtilities, Потоки, WMPopups, WMDialogs, FileHandlers, Commands,
	Archives, UTF8Strings,
	Notepad,
	WM := WMWindowManager;

конст
	BufSize = 16*1024; (* internal buffer size, used for copy *)

	(*FileNameLength = 256;*)
	FileNameLength=Files.NameLength;

	TraceCopy = 0;
	TraceDragging = 1;

	Trace = {0};

	FilenamePlaceholder = "@filename";

тип
	 FilesDropInterface = окласс(WMDropTarget.DropFiles)
	 перем
	 	path : Files.FileName;
		f : Files.File;
		w : Files.Writer;
		refresh : WMEvents.EventSource;
		overwriteOnce, overwriteAll, overwriteNever, abort : булево;

		проц &New*(конст str : массив из симв8);
		нач
			копируйСтрокуДо0(str, path);
			нов(refresh, сам, НУЛЬ, НУЛЬ, НУЛЬ);
			overwriteAll := ложь; overwriteNever := ложь; abort := ложь;
		кон New;

		проц {перекрыта}OpenPut*(конст remoteName : массив из симв8; перем outw : Потоки.Писарь; перем res : целМЗ);
		перем oldFile : Files.File; name : массив 1024 из симв8;
		нач
			res := -1;
			если abort то возврат; всё;
			копируйСтрокуДо0(path, name); Strings.Append(name, remoteName);
			overwriteOnce := ложь;
			oldFile := Files.Old(name);
			если (oldFile # НУЛЬ) и ~overwriteAll и ~overwriteNever то
				res := WMDialogs.Message(WMDialogs.TConfirmation, "Confirm overwriting", remoteName, {WMDialogs.ResNo, WMDialogs.ResYes, WMDialogs.ResAll, WMDialogs.ResAbort, WMDialogs.ResNever});
				просей res из
					|WMDialogs.ResYes: overwriteOnce := истина;
					|WMDialogs.ResNo: overwriteOnce := ложь;
					|WMDialogs.ResAll: overwriteAll := истина;
					|WMDialogs.ResAbort: abort := истина;
					|WMDialogs.ResNever: overwriteNever := истина;
				иначе
					ЛогЯдра.пСтроку8("WMSystemComponents: Implementation error, unexpected WMDialog result type."); ЛогЯдра.пВК_ПС;
				всё;
			всё;

			если TraceCopy в Trace то ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(" ... "); всё;
			если (oldFile = НУЛЬ) или overwriteOnce или overwriteAll то
				f := Files.New(name);
				если f # НУЛЬ то
					Files.OpenWriter(w, f, 0);
					outw := w;
					res := Files.Ok;
					если TraceCopy в Trace то
						ЛогЯдра.пСтроку8(" done");
						если (oldFile # НУЛЬ) то ЛогЯдра.пСтроку8(" (overwritten)"); всё;
						ЛогЯдра.пСтроку8(".");
					всё;
				иначе
					ЛогЯдра.пСтроку8("Error: Could not create file "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пВК_ПС;
				всё;
			иначе
				если (TraceCopy в Trace) и (oldFile # НУЛЬ) то ЛогЯдра.пСтроку8("skipped."); ЛогЯдра.пВК_ПС; всё;
			всё;
			если TraceCopy в Trace то ЛогЯдра.пВК_ПС; всё;
		кон OpenPut;

		проц {перекрыта}ClosePut*(перем res : целМЗ);
		нач
			если (f # НУЛЬ) и (w # НУЛЬ)  то
				w.ПротолкниБуферВПоток;
				f.Update;
				Files.Register(f);
				refresh.Call(НУЛЬ)
			всё
		кон ClosePut;

	кон FilesDropInterface;

	FilesDropTarget = окласс(WMDropTarget.DropTarget)
	перем path : Files.FileName; eh : WMEvents.EventListener;

		проц &New*(str : Strings.String; e : WMEvents.EventListener);
		нач
			если str # НУЛЬ то копируйСтрокуДо0(str^, path) всё;
			MakePathString(path);
			eh := e
		кон New;

		проц {перекрыта}GetInterface*(type : цел32) : WMDropTarget.DropInterface;
		перем di : FilesDropInterface;
		нач
			если type = WMDropTarget.TypeFiles то
				нов(di, path);
				если eh # НУЛЬ то di.refresh.Add(eh) всё;
				возврат di
			иначе возврат НУЛЬ
			всё
		кон GetInterface;
	кон FilesDropTarget;

тип

	TreeData = окласс
	перем
		path, name : Strings.String;
	кон TreeData;

	DirectoryTree* = окласс(WMTrees.TreeView)
	перем
		enumerator : Files.Enumerator;
		tree : WMTrees.Tree;
		currentPath* : WMProperties.StringProperty;
		onPathChanged* : WMEvents.EventSource;
		tr : WMTrees.TreeNode;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(GSDirectoryTree);
			SetGenerator("WMSystemComponents.GenDirectoryTree");
			(* new properties *)
			нов(currentPath, DirTreePathProt, НУЛЬ, НУЛЬ); properties.Add(currentPath);
			(* new events *)
			нов(onPathChanged, сам, GSonPathChanged, GSonPathChangedInfo, сам.StringToCompCommand);
			events.Add(onPathChanged);

			tree := GetTree();
			нов(enumerator);
			onSelectNode.Add(NodeSelected);
			onExpandNode.Add(NodeExpanded);
			tree.Acquire;
			нов(tr);
			tree.SetRoot(tr);
			tree.SetNodeCaption(tr, WMComponents.NewString("FileSystems"));
			tree.InclNodeState(tr, WMTrees.NodeAlwaysExpanded);
			FillMountedFS(tree, tr);
			tree.Release;
		кон Init;

	
		(* kia: команда сбрасывает всё дерево и заново загружает только корневые узлы дисков. Думаю, вполне легально использовать эту же команду для открытия папки по указанному пути *)

		проц Refresh*;
		нач
			tree.Acquire;
			FillMountedFS(tree, tr);
			tree.Release;
		кон Refresh;

		проц NodeExpanded(sender, data : динамическиТипизированныйУкль);
		перем p : динамическиТипизированныйУкль;
		нач
			если (data = НУЛЬ) или ~(data суть WMTrees.TreeNode) то возврат всё;
			tree.Acquire;
			p := tree.GetNodeData(data(WMTrees.TreeNode));
			если (p # НУЛЬ) и (p суть TreeData) то
				если WMTrees.NodeSubnodesUnknown в tree.GetNodeState(data(WMTrees.TreeNode)) то
					EnumerateSubDirectories(tree, data(WMTrees.TreeNode), p(TreeData).path)
				всё
			всё;
			tree.Release
		кон NodeExpanded;

		проц NodeSelected(sender, data : динамическиТипизированныйУкль);
		перем p : динамическиТипизированныйУкль;
		нач
			если (data = НУЛЬ) или ~(data суть WMTrees.TreeNode) то возврат всё;
			tree.Acquire;
			p := tree.GetNodeData(data(WMTrees.TreeNode));
			если (p # НУЛЬ) и (p суть TreeData) то
				EnumerateSubDirectories(tree, data(WMTrees.TreeNode), p(TreeData).path);
				currentPath.Set(p(TreeData).path);
				onPathChanged.Call(p(TreeData).path)
			всё;
			tree.Release
		кон NodeSelected;

		проц {перекрыта}DragDropped*(x, y : размерМЗ; dragInfo : WM.DragInfo);
		перем node : WMTrees.TreeNode;
			dropTarget : FilesDropTarget;
			p : динамическиТипизированныйУкль;
		нач
			tree.Acquire;
			node := GetNodeAtPos(x, y);
			p := tree.GetNodeData(node);
			tree.Release;
			если (p # НУЛЬ) и (p суть TreeData) то
				нов(dropTarget, p(TreeData).path, НУЛЬ);
				dragInfo.data := dropTarget;
				ConfirmDrag(истина, dragInfo)
			всё
		кон DragDropped;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		нач
			если (property = currentPath) то (*SetPath(currentPath.Get()) *)
			иначе PropertyChanged^(sender, property)
			всё;
		кон PropertyChanged;

		(* binary insertion algorithm from "Algorithms and Data Structures" by N. Wirth *)
		проц SortDirs(перем dir: массив из TreeData);
		перем i, j, m, L, R : размерМЗ;
			x : TreeData;
			dirName, xName: Strings.String;
		нач
			нцДля i := 1 до длинаМассива(dir) - 1 делай
				x := dir[i]; L := 0; R := i; xName := Strings.NewString(x.name^); Strings.UpperCase(xName^);
				нцПока L < R делай
					m := (L + R) DIV 2;
					dirName := Strings.NewString(dir[m].name^); Strings.UpperCase(dirName^);
					если UTF8Strings.Compare(dirName^, xName^) = UTF8Strings.CmpGreater то
						R := m
					иначе
						L := m + 1
					всё
				кц;
				нцДля j := i до R + 1 шаг -1 делай dir[j] := dir[j - 1] кц;
				dir[R] := x
			кц
		кон SortDirs;

		проц EnumerateSubDirectories(tree : WMTrees.Tree; node : WMTrees.TreeNode; dir : Strings.String);
		перем
			name, path, filename, mask : Files.FileName; 
			flags : мнвоНаБитахМЗ; 
			time, date : цел32; 
			size : Files.Size;
			dirNode : WMTrees.TreeNode;
			td : TreeData; has : булево;
			dirArray: укль на массив из TreeData;
			i, sz: цел32;
		нач
			tree.Acquire;
			если tree.GetChildren(node) # НУЛЬ то tree.Release; возврат всё; (* assuming there will be no changes in the structures *)
			нцПока tree.GetChildren(node) # НУЛЬ делай tree.RemoveNode(tree.GetChildren(node)) кц;
			копируйСтрокуДо0(dir^, mask);
			если Strings.Length(mask) >= 1 то
				если mask[Strings.Length(mask) - 1] = ':' то Strings.Append(mask, '*')
				иначе Strings.Append(mask, '/*')
				всё
			иначе mask := '*'
			всё;
			enumerator.Open(mask, {});
			has := ложь;
			нцПока enumerator.HasMoreEntries() делай
				если enumerator.GetEntry(name, flags, time, date, size) то
					если Files.Directory в flags то
						has := истина; увел(sz)
					всё
				всё
			кц;
			enumerator.Reset();
			если has то
				нов(dirArray, sz);
				нцПока enumerator.HasMoreEntries() делай
					если enumerator.GetEntry(name, flags, time, date, size) то
						если Files.Directory в flags то
							has := истина;
							Files.SplitPath(name, path, filename);
							нов(td); td.path := WMComponents.NewString(name); td.name := WMComponents.NewString(filename);
							dirArray[i] := td; увел(i)
						всё
					всё
				кц;
				SortDirs(dirArray^);
				нцДля i := 0 до sz-1 делай
					нов(dirNode); td := dirArray[i];
					tree.SetNodeData(dirNode, td);
					tree.SetNodeCaption(dirNode, td.name);
					tree.InclNodeState(dirNode, WMTrees.NodeSubnodesUnknown);
					tree.AddChildNode(node, dirNode)
				кц
			всё;
			если has то tree.SetNodeState(node, {WMTrees.NodeExpanded})
			иначе tree.SetNodeState(node, {})
			всё;
			enumerator.Close;
			tree.Release
		кон EnumerateSubDirectories;

		проц FillMountedFS(tree : WMTrees.Tree; node : WMTrees.TreeNode);
		перем 
			list: Files.FileSystemTable;
			prefixNode : WMTrees.TreeNode;
			td : TreeData;
			i : размерМЗ;
			prefix : Files.Prefix;
			
			(* kia: переменные для мода *)
			rootPath, s: Strings.String;
			disk: Files.Prefix;
			folderPath, name: Files.FileName;
			root, folder: WMTrees.TreeNode;
			p: динамическиТипизированныйУкль;
			sa: Strings.StringArray;
			dirNode, found : WMTrees.TreeNode;
			
		нач
			Files.GetList(list);
			tree.Acquire;
			нцПока tree.GetChildren(node) # НУЛЬ делай tree.RemoveNode(tree.GetChildren(node)) кц;
			нцДля i := 0 до длинаМассива(list) - 1 делай
				нов(prefixNode);
				tree.SetNodeCaption(prefixNode, WMComponents.NewString(list[i].prefix));
				копируйСтрокуДо0(list[i].prefix, prefix); Strings.Append(prefix, ":");
				нов(td); td.path := WMComponents.NewString(prefix);
				tree.SetNodeData(prefixNode, td);
				tree.SetNodeState(prefixNode, {WMTrees.NodeSubnodesUnknown});
				tree.AddChildNode(node, prefixNode);
			кц;
			tree.Release;
			(* ####################################################################
			Мод сброса дерева в указанный каталог
			*)
			(* 03-01-2021 kia: штатный алгоритм выше не трогаю вообще, только 
			добавляю свой код после этого комментария. Смысл мода в том, что 
			если задано значение свойства currentPath, то логично выстроить 
			туда цепочку узлов при сбросе. Ведь дерево всё равно сбрасывается, 
			так пусть сбросится не безразлично, а определённо.
			Требование: пусть будет доступно, как если бы он сам открыл менеджер и туда дошёл.
			Иначе говоря, надо сгенерировать цепочку узлов, а не делать отдельный узел с папкой.
			*)
			tree.Acquire;
			folder := НУЛЬ;
			s := currentPath.Get();
			если s # НУЛЬ то
				Files.SplitName(s^, prefix, name);
				disk := prefix;
				Strings.AppendX(disk, ":");
				(* Новое дерево всегда имеет корневые узлы всех доступных файловых систем *)
				found := НУЛЬ;
				root := tree.GetChildren(tree.GetRoot());
				нцПока (root # НУЛЬ) и (found = НУЛЬ) делай
					p := tree.GetNodeData(root);
					если (p # НУЛЬ) и (p суть TreeData) то
						rootPath := p(TreeData).path;
						если ((rootPath # НУЛЬ) и (rootPath^ = disk)) то
							found := root;
						всё;
					всё;
					root := tree.GetNextSibling(root);
				кц;
				если (found # НУЛЬ) то
					folder := found;
					(* попали на нужный диск  в узле root, можно генерировать цепочку *)
					sa := Strings.Split(name, Files.PathDelimiter);
					(*  как одну строчку присвоить другой?*)
					folderPath := "";
					Strings.AppendX(folderPath, disk);
					(* теперь по каждой папке создаём узел *)
					нцДля i := 0 до длинаМассива(sa)-1 делай
						если (sa[i]^ # "") то
							(* путь наращивается очередной папкой *)
							Strings.AppendX(folderPath, '/');
							Strings.AppendX(folderPath, sa[i]^);
							(* данные узла *)
							нов(td);
							td.path := Strings.NewString(folderPath);
							td.name := Strings.NewString(sa[i]^);
							нов(dirNode);
							tree.SetNodeData(dirNode, td);
							tree.SetNodeCaption(dirNode, td.name);
							(* tree.InclNodeState(dirNode, WMTrees.NodeAlwaysExpanded); *)
							tree.AddChildNode(folder, dirNode);
							folder := dirNode;
						всё
					кц;
					(* раскрыть цепочку и установить выделение на целевой узел. *)
					tree.ExpandToRoot(folder);
					SelectNode(folder);
				всё;
			всё;		
			tree.Release;			
			(* #################################################################### *)
		кон FillMountedFS;

	кон DirectoryTree;

тип
	DirEntry* = окласс
	перем
		name*, path- : Strings.String;
		time, date: цел32;
		size*: Files.Size;
		flags : мнвоНаБитахМЗ;
		visible : булево;
		node* : WMTrees.TreeNode;

		проц &Init*(name, path : Strings.String; time, date : цел32; size: Files.Size; flags : мнвоНаБитахМЗ);
		нач
			сам.name := name;
			сам.path := path;
			сам.time := time;
			сам.date := date;
			сам.size := size;
			сам.flags := flags;
			visible := ложь;
			нов(node)
		кон Init;

	кон DirEntry;

	DirEntries* = укль на массив из DirEntry;

	SelectionWrapper* = укль на запись
		sel* : DirEntries;
		user* : динамическиТипизированныйУкль;
	кон;

	StringWrapper* = укль на запись
		string* : Strings.String;
	кон;

	FileList* = окласс(WMComponents.VisualComponent)
	перем
		grid : WMStringGrids.StringGrid;
		prefixSearch : WMProperties.BooleanProperty;
		path, filter : Strings.String;
		fullView, fromSearchReq : булево;
		popup: WMPopups.Popup;
		enumerator : Files.Enumerator;
		dir : DirEntries;
		selection : DirEntries;
		nfiles: размерМЗ; nofRows : цел32;
		px, py : размерМЗ;
		colWidths : WMGrids.Spacings;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(GSFileList);
			SetGenerator("WMSystemComponents.GenFileList");
			(* new properties *) (* it is not possible to change this property at any time but we leave it for the moment *)
			нов(prefixSearch, FileListPrefixSearchProt, НУЛЬ, НУЛЬ); properties.Add(prefixSearch);

			fullView := ложь;
			fromSearchReq := ложь;

			нов(grid);
			grid.alignment.Set(WMComponents.AlignClient);
			AddContent(grid);
			grid.SetExtDragDroppedHandler(MyDragDropped);
			grid.onClickSelected.Add(ClickSelected);
			grid.SetExtContextMenuHandler(ContextMenu);
			grid.onStartDrag.Add(MyStartDrag);
			grid.model.Acquire;
			grid.model.SetNofCols(1);
			grid.model.SetNofRows(1);
			grid.fixedRows.Set(1);
			нов(colWidths, 3);
			grid.model.SetCellText(0, 0, Strings.NewString("Filename"));
			grid.model.SetCellText(1, 0, Strings.NewString("Size"));
			grid.model.SetCellText(2, 0, Strings.NewString("Modified"));
			grid.SetSelectionMode(WMGrids.GridSelectRows);
			grid.model.Release;
			нов(enumerator);

			сам.path := Strings.NewString("");
			сам.filter := Strings.NewString("");
		кон Init;

		проц SetSearchReqFlag*;
		нач
			fromSearchReq := истина
		кон SetSearchReqFlag;

		проц GetSelection*() : DirEntries;
		перем selection : DirEntries;
			l, t, r, b, i, j : размерМЗ;
			p : динамическиТипизированныйУкль;
		нач
			grid.model.Acquire;
			grid.GetSelection(l, t, r, b);
			нов(selection, b- t + 1);
			j := 0;
			нцДля i := t до b делай
				p := grid.model.GetCellData(0, i);
				если (p # НУЛЬ) и (p суть DirEntry) то
					selection[j] := p(DirEntry);
					увел(j)
				всё
			кц;
			grid.model.Release;
			возврат selection
		кон GetSelection;

		проц ClickSelected(sender, data : динамическиТипизированныйУкль);
		перем curSel : DirEntries;
			w : SelectionWrapper;
			p : Files.FileName;
		нач
			если (data # НУЛЬ) и (data суть DirEntry) то
				нов(curSel, 1);
				curSel[0] := data(DirEntry);
				если Files.Directory в curSel[0].flags то
					копируйСтрокуДо0(curSel[0].path^, p); Strings.Append(p, curSel[0].name^);
					MakePathString(p);
					StartNewPath(Strings.NewString(p));
				иначе
					нов(w); w.sel := curSel; w.user := НУЛЬ;
					Open(sender, w)
				всё
			всё
		кон ClickSelected;

		проц HandleCommands(sender, data : динамическиТипизированныйУкль);
		перем
			w : SelectionWrapper;
			filename : Files.FileName;
			command : массив 1024 из симв8;
			position: размерМЗ; res: целМЗ;
			msg : массив 256 из симв8;
		нач
			если (data # НУЛЬ) и (data суть SelectionWrapper) то
				w := data (SelectionWrapper);
				если (w.user # НУЛЬ) и (w.user суть StringWrapper) и (w.user(StringWrapper).string # НУЛЬ) то
					если (w.sel[0].path # НУЛЬ) то
						копируйСтрокуДо0(w.sel[0].path^, filename);
						Strings.Append(filename, w.sel[0].name^);
					иначе
						копируйСтрокуДо0(w.sel[0].name^, filename);
					всё;

					копируйСтрокуДо0(w.user(StringWrapper).string^, command);
					position := Strings.Pos(FilenamePlaceholder, command);
					если (position # -1) то
						утв(w.sel[0].name^ # FilenamePlaceholder);
						нцДо
							Strings.Delete(command, position, Strings.Length(FilenamePlaceholder));
							Strings.Insert(filename, command, position);
							position := Strings.Pos(FilenamePlaceholder, command);
						кцПри (position = -1);
					иначе
						Strings.Append(command, " ");
						Strings.Append(command, w.sel[0].name^);
					всё;

					Commands.Call(command, {}, res, msg);

					если (res # Commands.Ok) то
						ЛогЯдра.пСтроку8("WMSystemComponents: Execution of command '");
						ЛогЯдра.пСтроку8(command); ЛогЯдра.пСтроку8("' failed, res: ");
						ЛогЯдра.пЦел64(res, 0);
						ЛогЯдра.пСтроку8(" ("); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пСтроку8(")");
						ЛогЯдра.пВК_ПС;
					всё;
				всё;
			всё;
		кон HandleCommands;

		проц ContextMenu(sender : динамическиТипизированныйУкль; x, y: размерМЗ);
		перем wmx, wmy : размерМЗ;
			curSel : DirEntries;
			w : SelectionWrapper;
			sw : StringWrapper;
			filename, extension : Files.FileName;
			config : массив 256 из симв8;
			ptr : динамическиТипизированныйУкль;
			element : XML.Element;
			enumerator : XMLObjects.Enumerator;
			name, value : XML.String;
		нач
			px := x; py := y;
			нов(popup);
			curSel := GetSelection();
			нов(w); w.sel := curSel; w.user := НУЛЬ;
			если ~fromSearchReq то
				(* allow operations on files such as renaming, duplicating, deleting, etc. only in not content-based search lists *)
				если длинаМассива(curSel) = 1 то
					popup.AddParButton("Open", Open, w);
					popup.AddParButton("Rename", Rename, w);
					popup.AddParButton("Duplicate", Duplicate, w);
					popup.AddParButton("EditText", EditText, w);
				всё;
				popup.AddParButton("Tar", Tar, w);
				popup.AddParButton("Delete", Delete, w);
				если (длинаМассива(curSel) = 1) и (curSel[0] # НУЛЬ) и (curSel[0].name # НУЛЬ)  то
					Files.SplitExtension(curSel[0].name^, filename, extension);
					Strings.LowerCase(extension);
					config := "Filehandlers.";
					Strings.Append(config, extension);
					element := Configuration.GetSection(config);
					если (element # НУЛЬ) то
						enumerator := element.GetContents();
						нцПока (enumerator.HasMoreElements()) делай
							ptr := enumerator.GetNext();
							если (ptr # НУЛЬ) и (ptr суть XML.Element) то
								element := ptr (XML.Element);
								name := element.GetAttributeValue("name");
								если (name # НУЛЬ) и (name^ # "Open") то
									value := element.GetAttributeValue("value");
									если (value # НУЛЬ) то
										нов(sw); sw.string := value;
										w.user := sw;
										popup.AddParButton(name^, HandleCommands, w);
									иначе
										ЛогЯдра.пСтроку8("WMSystemComponents: No value attribute in section ");
										ЛогЯдра.пСтроку8(config); ЛогЯдра.пВК_ПС;
									всё;
								всё;
							всё;
						кц;
					всё;
				всё;
			иначе
				если длинаМассива(curSel) = 1 то
					(* only allow opening of files in this case *)
					popup.AddParButton("Open", Open, w);
				всё
			всё;
			grid.Acquire; grid.ToWMCoordinates(x, y, wmx, wmy); grid.Release;
			popup.Popup(wmx, wmy)
		кон ContextMenu;

		проц Rename(sender, data : динамическиТипизированныйУкль);
		перем  d : DirEntry; rename : WMDialogs.MiniStringInput;
			wmx, wmy: размерМЗ; res: целМЗ;
			name, op, np : массив FileNameLength из симв8;
		нач
			если popup # НУЛЬ то popup.Close; popup := НУЛЬ всё;
			если (data # НУЛЬ) и (data суть SelectionWrapper) то
				d := data(SelectionWrapper).sel[0];
				если d # НУЛЬ то
					grid.Acquire; grid.ToWMCoordinates(px, py, wmx, wmy); grid.Release;
					нов(rename);
					копируйСтрокуДо0(d.name^, name);
					если rename.Show(wmx, wmy, name) = WMDialogs.ResOk то
						если name # d.name^ то
							копируйСтрокуДо0(d.path^, op); Strings.Append(op, d.name^);
							копируйСтрокуДо0(d.path^, np); Strings.Append(np, name);
							если ~FileExists(np) или
								(WMDialogs.Confirmation("Confirm overwriting existing file", np) = WMDialogs.ResYes) то
								Files.Rename(op, np, res);
								если res # 0 то
									ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
									WMDialogs.Error("Renaming failed", np);
								всё;
								Refresh(НУЛЬ, НУЛЬ)
							всё
						всё
					всё
				всё
			всё
		кон Rename;

		проц Delete(sender, data : динамическиТипизированныйУкль);
		перем  d : DirEntry; i: размерМЗ;
			dr  : цел32; res: целМЗ;
			dp : Files.FileName;
			delete, always, never : булево;
		нач
			если popup # НУЛЬ то popup.Close; popup := НУЛЬ всё;
			если (data # НУЛЬ) и (data суть SelectionWrapper) то
				always := ложь; never := ложь;
				нцДля i := 0 до длинаМассива(data(SelectionWrapper).sel) - 1 делай
					d := data(SelectionWrapper).sel[i];
					delete := ложь;
					если d # НУЛЬ то
						копируйСтрокуДо0(d.path^, dp); Strings.Append(dp, d.name^);
						если ~always и ~never то
							dr := WMDialogs.Message(WMDialogs.TConfirmation, "Confirm deleting file", dp,
								{WMDialogs.ResNo, WMDialogs.ResAbort, WMDialogs.ResYes, WMDialogs.ResAll});
							если dr в {WMDialogs.ResYes, WMDialogs.ResAll} то delete := истина всё;
							если dr = WMDialogs.ResAll то always := истина всё;
							если dr = WMDialogs.ResAbort то never := истина всё;
						всё;
						если ~never и (delete или always) то
							Files.Delete(dp, res);
							если res # 0 то
								WMDialogs.Error("Deleting failed", dp)
							всё;
							(* if the dialog was shown then visually update *)
							если delete то Refresh(НУЛЬ, НУЛЬ) всё
						всё
					всё
				кц;
				(* end of the operation refresh list *)
				Refresh(НУЛЬ, НУЛЬ)
			всё
		кон Delete;

		проц Duplicate(sender, data : динамическиТипизированныйУкль);
		перем  d : DirEntry;
			name : массив FileNameLength (* was 128*) из симв8;
			res : целМЗ;
		нач
			если popup # НУЛЬ то popup.Close; popup := НУЛЬ всё;
			если (data # НУЛЬ) и (data суть SelectionWrapper) то
				d := data(SelectionWrapper).sel[0];
				если d # НУЛЬ то
					копируйСтрокуДо0(d.path^, name);
					Strings.Append(name, d.name^);
					Files.Copy(name, res);
					если res = 0 то
						Strings.Append(name, ".COPY");
						Files.Paste(name, res);
						нцПока res # 0 делай
							если res = 2908 то
								если WMDialogs.QueryString("File already exists. Enter a new Name", name) = WMDialogs.ResOk то
									Files.Paste(name, res);
								иначе
									res := 0;
								всё;
							аесли res = 2909 то
								если WMDialogs.QueryString("FileName too long. Enter a new Name", name) = WMDialogs.ResOk то
									Files.Paste(name, res);
								иначе
									res := 0;
								всё;
							иначе
								WMDialogs.Error("Error", "Some Error occoured while duplicating");
							всё;
						кц;
					всё;
				всё;
				Refresh(НУЛЬ, НУЛЬ);
			всё;
		кон Duplicate;

		проц Tar(sender, data : динамическиТипизированныйУкль);
		перем
			d : DirEntry; i: размерМЗ; len : размерМЗ;
			filename, format, temp : Files.FileName;
			buf: массив BufSize из симв8;
			arc : Archives.Archive;
			file : Files.File; reader : Files.Reader;
			writer : Потоки.Писарь;

			проц GetFormatFromFilename(конст filename : массив из симв8; перем format : массив из симв8);
			перем file : массив FileNameLength (* was 128*) из симв8;
			нач
				если filename = "" то копируйСтрокуДо0("tar", format);
				иначе
					Strings.GetExtension(filename, file, format);
					Strings.LowerCase(format);
				 всё
			кон GetFormatFromFilename;

		нач
			если popup # НУЛЬ то popup.Close; popup := НУЛЬ всё;
			если (data # НУЛЬ) и (data суть SelectionWrapper) то
				если (WMDialogs.QueryString("Enter Name of Archive: ", filename) = WMDialogs.ResOk) то

					GetFormatFromFilename(filename, format);
					arc := Archives.Old(filename, format);
					если arc = НУЛЬ то
						arc := Archives.New(filename, format)
					всё;

					ЛогЯдра.пСтроку8("File Manager: building "); ЛогЯдра.пСтроку8(filename);
					нцДля i := 0 до длинаМассива(data(SelectionWrapper).sel) - 1 делай
						d := data(SelectionWrapper).sel[i];
						копируйСтрокуДо0(d.path^, temp); Strings.Append(temp, d.name^);
						file := Files.Old(temp);
						если file # НУЛЬ то
							Files.OpenReader(reader, file, 0);
							arc.Acquire;
							Потоки.НастройПисаря(writer, arc.OpenSender(d.name^));

							нцДо
								reader.чБайты(buf, 0, длинаМассива(buf), len); writer.пБайты(buf, 0, len);
							кцПри reader.кодВозвратаПоследнейОперации # 0;
							если writer # НУЛЬ то writer.ПротолкниБуферВПоток всё;
							arc.Release;
						всё;
					кц;
					ЛогЯдра.пСтроку8(" - done!"); ЛогЯдра.пВК_ПС;
					(* end of the operation refresh list *)
					Refresh(НУЛЬ, НУЛЬ)
				всё
			всё
		кон Tar;

		проц Open(sender, data : динамическиТипизированныйУкль);
		перем d : DirEntry; filename : Files.FileName;
		нач
			если popup # НУЛЬ то popup.Close; popup := НУЛЬ всё;
			если (data # НУЛЬ) и (data суть SelectionWrapper) то
				d := data(SelectionWrapper).sel[0];
				если d # НУЛЬ то
					копируйСтрокуДо0(d.path^, filename);
					Strings.Append(filename, d.name^);
					FileHandlers.OpenFile(filename, НУЛЬ, НУЛЬ)
				всё
			всё
		кон Open;

	проц EditText(sender, data : динамическиТипизированныйУкль);
		перем d : DirEntry; filename : Files.FileName; window : Notepad.Window; format : массив 32 из симв8;
		нач
			если popup # НУЛЬ то popup.Close; popup := НУЛЬ всё;
			если (data # НУЛЬ) и (data суть SelectionWrapper) то
				d := data(SelectionWrapper).sel[0];
				если d # НУЛЬ то
					копируйСтрокуДо0(d.path^, filename);
					Strings.Append(filename, d.name^);
					format := "АВТО";
					нов(window, НУЛЬ);
					window.editor.Load(filename, format);
				всё
			всё
		кон EditText;

		проц MyDragDropped(x, y : размерМЗ; dragInfo : WM.DragInfo; перем handled : булево);
		нач
			handled := истина;
			DragDropped(x, y, dragInfo)
		кон MyDragDropped;

		проц {перекрыта}DragDropped*(x, y : размерМЗ; dragInfo : WM.DragInfo);
		перем dropTarget : FilesDropTarget;
		нач
			нов(dropTarget, path, Refresh);
			dragInfo.data := dropTarget;
			ConfirmDrag(истина, dragInfo)
		кон DragDropped;

		проц MyStartDrag(sender, data : динамическиТипизированныйУкль);
		перем img : WMGraphics.Image;
			c : WMGraphics.BufferCanvas;
			top : цел32; i : размерМЗ;
		нач
			selection := GetSelection();
			(* render to bitmap *)
			нов(img);	Raster.Create(img, 100, 200, Raster.BGRA8888);
			нов(c, img);
			c.SetColor(цел32(0FFFF00FFH));
			top := 0;
			нцДля i := 0 до длинаМассива(selection) - 1 делай
				если selection[i] # НУЛЬ то
					c.Fill(WMRectangles.MakeRect(0, top, 100, top + 25), 0FF80H, WMGraphics.ModeCopy);
					c.DrawString(3, top + 20, selection[i].name^);
					увел(top, 25)
				всё
			кц;
			если grid.StartDrag(НУЛЬ, img, 0,0,DragArrived, НУЛЬ) то
				если TraceDragging в Trace то ЛогЯдра.пСтроку8("WMSystemComponents: DraggingStarted"); всё;
			иначе
				если TraceDragging в Trace то ЛогЯдра.пСтроку8("WMSystemComponents: Drag could not be started"); всё;
			всё;
		кон MyStartDrag;

		проц CopyFile(target : WMDropTarget.DropFiles; конст local, remote : массив из симв8; перем res : целМЗ);
		перем w : Потоки.Писарь;
			f : Files.File;
			r : Files.Reader;
			buf: массив BufSize из симв8; len: размерМЗ;
		нач
			res := -1;
			f := Files.Old(local);
			если f # НУЛЬ то
				Files.OpenReader(r, f, 0);
				target.OpenPut(remote, w, res);
				если res = 0 то
					нцДо
						r.чБайты(buf, 0, BufSize, len); w.пБайты(buf, 0, len);
					кцПри r.кодВозвратаПоследнейОперации # 0;
					target.ClosePut(res)
				всё;
			всё
		кон CopyFile;

		проц Refresh(sender, data : динамическиТипизированныйУкль);
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(сам.Refresh, sender, data)
			иначе
				ScanPath; PrepareList;
				grid.Acquire;
				grid.SetSelection(-1, -1, -1, -1);
				selection := НУЛЬ;
				grid.Release;
			всё
		кон Refresh;

		проц {перекрыта}Resized*;
		нач
			grid.model.Acquire;
			если fullView то
				colWidths[0] := (bounds.GetWidth() DIV 3)*2 - 20;
				colWidths[1] := bounds.GetWidth() DIV 6;
				colWidths[2] := bounds.GetWidth() DIV 6;
			иначе
				colWidths[0] := bounds.GetWidth();
			всё;
			grid.SetColSpacings(colWidths);
			grid.model.Release;
			Resized^;
		кон Resized;

		проц DragArrived(sender, data : динамическиТипизированныйУкль);
		перем di : WM.DragInfo;
			dt : WMDropTarget.DropTarget;
			itf : WMDropTarget.DropInterface;
			i : размерМЗ; res: целМЗ;
			sel : DirEntries;
			url : массив 1024 из симв8;
			text : Texts.Text;
			textPos : Texts.TextPosition;
			nl: массив 2 из симв8;
		нач
			sel := selection;
			если sel = НУЛЬ то возврат всё;

			если (data # НУЛЬ) и (data суть WM.DragInfo) то
				di := data(WM.DragInfo);
				если (di.data # НУЛЬ) и (di.data суть WMDropTarget.DropTarget) то
					dt := di.data(WMDropTarget.DropTarget)
				иначе возврат
				всё
			иначе возврат
			всё;

			itf := dt.GetInterface(WMDropTarget.TypeFiles);
			если itf # НУЛЬ то
				нцДля i := 0 до длинаМассива(selection) - 1 делай
					если selection[i] # НУЛЬ то
						копируйСтрокуДо0(selection[i].path^, url);
						Strings.Append(url, selection[i].name^);
						CopyFile(itf(WMDropTarget.DropFiles), url, selection[i].name^, res);
					всё
				кц;
				возврат
			всё;

			itf := dt.GetInterface(WMDropTarget.TypeURL);
			если itf # НУЛЬ то
				нцДля i := 0 до длинаМассива(selection) - 1 делай
					если selection[i] # НУЛЬ то
						копируйСтрокуДо0(selection[i].path^, url);
						Strings.Append(url, selection[i].name^);
						itf(WMDropTarget.DropURLs).URL(url, res)
					всё
				кц;
				возврат
			всё;

			itf := dt.GetInterface(WMDropTarget.TypeText);
			если itf # НУЛЬ то
				text := itf(WMDropTarget.DropText).text;
				textPos := itf(WMDropTarget.DropText).pos;
				если (text # НУЛЬ) и (textPos # НУЛЬ) то
					text.AcquireWrite;
					нцДля i := 0 до длинаМассива(selection) - 1 делай
						если selection[i] # НУЛЬ то
							копируйСтрокуДо0(selection[i].path^, url);
							Strings.Append(url, selection[i].name^);
							nl[0] := симв8ИзКода(кодСимв32(Texts.NewLineChar));
							nl[1] := 0X;
							Strings.Append(url, nl);
							TextUtilities.StrToText(text, textPos.GetPosition(), url)
						всё
					кц;
					text.ReleaseWrite
				всё;
				возврат
			всё;
		кон DragArrived;

		проц ResetGrid*;
		нач
			nofRows := 1;
			grid.model.Acquire;
			grid.model.SetNofRows(nofRows);
			grid.SetTopPosition(0, 0, истина);
			grid.SetSelection(0, 0, 0, 0);
			grid.model.Release
		кон ResetGrid;

		проц DisplayGrid*(конст data : массив из DirEntry; noEl : цел32);
		перем i, gridindex : цел32;
			d : DirEntry;
			t : массив FileNameLength (* was 128*) из симв8;
		нач
			grid.model.Acquire;
			grid.model.SetNofRows(nofRows + noEl);
			нцДля i := 0 до noEl -1 делай
				d := data[i];
				gridindex := nofRows + i;
				grid.model.SetCellText(0, gridindex , d.name);
				grid.model.SetCellData(0, gridindex, d);
				если fullView то
					Strings.IntToStr(d.size, t);
					grid.model.SetCellText(1, gridindex, Strings.NewString(t));
					Strings.FormatDateTime(" yyyy mmm dd hh:nn ", Dates.OberonToDateTime(d.date, d.time), t);
					grid.model.SetCellText(2, gridindex, Strings.NewString(t));
				всё;
				если Files.Directory в d.flags то
					grid.model.SetCellImage(0, gridindex, WMGraphics.LoadImage("icons.tar://Folder.png", истина))
				иначе
					grid.model.SetCellImage(0, gridindex, НУЛЬ)
				всё
			кц;
			grid.model.Release;
			nofRows := nofRows + noEl;
		кон DisplayGrid;

		проц ToggleProps*;
		нач
			grid.model.Acquire;
			если fullView то
				fullView := ложь;
				grid.model.SetNofCols(1);
				colWidths[0] := bounds.GetWidth();
			иначе
				fullView := истина;
				grid.model.SetNofCols(3);
				colWidths[0] := (bounds.GetWidth() DIV 3)*2 - 20;
				colWidths[1] := bounds.GetWidth() DIV 6;
				colWidths[2] := bounds.GetWidth() DIV 6;
				grid.model.SetCellText(1, 0, Strings.NewString("Size"));
				grid.model.SetCellText(2, 0, Strings.NewString("Modified"));
			всё;
			grid.SetColSpacings(colWidths);
			grid.model.Release;
			Refresh(НУЛЬ, НУЛЬ);
		кон ToggleProps;

		проц FillGridRow(rowNo : цел32; dir : DirEntry);
		перем t : массив FileNameLength (* was 128*) из симв8;
		нач
			grid.model.SetCellText(0, rowNo, dir.name);
			grid.model.SetCellData(0, rowNo, dir);
			если fullView то
				Strings.IntToStr(dir.size, t);
				grid.model.SetCellText(1, rowNo, Strings.NewString(t));
				Strings.FormatDateTime(" yyyy mmm dd hh:nn ", Dates.OberonToDateTime(dir.date, dir.time), t);
				grid.model.SetCellText(2, rowNo, Strings.NewString(t));
			всё;
			если Files.Directory в dir.flags то
				grid.model.SetCellImage(0, rowNo, WMGraphics.LoadImage("icons.tar://Folder.png", истина))
			иначе
				grid.model.SetCellImage(0, rowNo, НУЛЬ)
			всё;
		кон FillGridRow;

		проц PrepareList;
		перем i : размерМЗ; vis : цел32; mask : массив FileNameLength (* was 128*) из симв8; s : Strings.String;
		нач
			если dir = НУЛЬ то возврат всё;
			s := сам.filter;
			mask := "";
			если s # НУЛЬ то копируйСтрокуДо0(s^, mask) всё;
			если mask = "" то
				нцДля i := 0 до длинаМассива(dir) - 1 делай dir[i].visible := истина кц;
				vis := длинаМассива(dir)(цел32);
			иначе
				если prefixSearch.Get() и ( mask[Strings.Length(mask)] # "*") то Strings.Append(mask, "*") всё;
				vis := 0;
				нцДля i := 0 до длинаМассива(dir) - 1 делай
					если Strings.Match(mask, dir[i].name^) то
						dir[i].visible := истина;
						увел(vis)
					иначе dir[i].visible := ложь
					всё
				кц;
			всё;

			grid.model.Acquire;
			grid.model.SetNofRows(vis + 1);

			vis := 0;
			нцДля i := 0 до длинаМассива(dir) - 1 делай
				если dir[i].visible то
					FillGridRow(vis + 1, dir[i]);
					увел(vis)
				всё
			кц;
			grid.SetTopPosition(0, 0, истина);
			grid.model.Release;
		кон PrepareList;


		проц ScanPath;
		перем s, pathS : Strings.String;
			i, l : размерМЗ;
			name, path, filename, mask : Files.FileName; flags : мнвоНаБитахМЗ; time, date : цел32;
			size : Files.Size; sorted : булево;
		нач
			s := сам.path;
			если s = НУЛЬ то возврат всё;
			копируйСтрокуДо0(s^, mask);
			если Strings.Length(mask) > 1 то
				если mask[Strings.Length(mask) - 1] = ':' то Strings.Append(mask, '*')
				иначе Strings.Append(mask, '/*')
				всё
			иначе mask := '*'
			всё;

			если fullView то enumerator.Open(mask, {Files.EnumSize, Files.EnumTime}); иначе enumerator.Open(mask, {}); всё;
			nfiles := enumerator.size;
			i := 0;
			sorted := истина;
			нов(dir, enumerator.size);
			нцПока enumerator.HasMoreEntries() делай
				если enumerator.GetEntry(name, flags, time, date, size) то
					Files.SplitPath(name, path, filename);
					l := Strings.Length(path);
					path[l] := Files.PathDelimiter; path[l + 1] := 0X;
					если (pathS = НУЛЬ) или (pathS^ # path) то pathS := Strings.NewString(path) всё;
					нов(dir[i], Strings.NewString(filename), pathS, time, date, size, flags);
				всё;
				увел(i)
			кц;
			enumerator.Close;
			если fullView то SortDirDate иначе SortDir всё;
		кон ScanPath;

		проц StartNewPath*(path : Strings.String);
		нач
			сам.path := path;
			ScanPath;
			PrepareList
		кон StartNewPath;

		проц StartNewFilter*(filter : Strings.String);
		нач
			сам.filter := filter;
			PrepareList
		кон StartNewFilter;

		проц GetNofFiles*() : размерМЗ;
		нач
			возврат nfiles
		кон GetNofFiles;

		(* binary insertion algorithm from "Algorithms and Data Structures" by N. Wirth *)
		проц SortDir;
		перем
			i, j, m, L, R : размерМЗ;
			x : DirEntry;
			dirName, xName: Strings.String;
			dirFlag, xFlag: цел8;
		нач
			нцДля i := 1 до длинаМассива(dir) - 1 делай
				x := dir[i]; L := 0; R := i; xName := Strings.NewString(x.name^); Strings.UpperCase(xName^);
				если Files.Directory в x.flags то xFlag := 0 иначе xFlag := 1 всё;
				нцПока L < R делай
					m := (L + R) DIV 2; dirName := Strings.NewString(dir[m].name^); Strings.UpperCase(dirName^);
					если Files.Directory в dir[m].flags то dirFlag := 0 иначе dirFlag := 1 всё;
					если (dirFlag < xFlag) или ((dirFlag=xFlag) и (UTF8Strings.Compare(dirName^, xName^) = UTF8Strings.CmpGreater)) то
						R := m
					иначе
						L := m + 1
					всё
				кц;
				нцДля j := i до R + 1 шаг -1 делай dir[j] := dir[j - 1] кц;
				dir[R] := x
			кц
		кон SortDir;

		(* binary insertion algorithm from "Algorithms and Data Structures" by N. Wirth *)
		проц SortDirDate;
		перем
			i, j, m, L, R : размерМЗ;
			x : DirEntry;
			dirTime,dirDate:цел32;
			dirFlag, xFlag: цел8;
		нач
			нцДля i := 1 до длинаМассива(dir) - 1 делай
				x := dir[i]; L := 0; R := i;
				если Files.Directory в x.flags то xFlag := 0 иначе xFlag := 1 всё;
				нцПока L < R делай
					m := (L + R) DIV 2;
					dirTime := dir[m].time; dirDate:= dir[m].date;
					если Files.Directory в dir[m].flags то dirFlag := 0 иначе dirFlag := 1 всё;
					если (dirFlag < xFlag) или ((dirFlag=xFlag) и ((dirDate< x.date) или ((dirDate=x.date) и  (dirTime<x.time)))) то
						R := m
					иначе
						L := m + 1
					всё
				кц;
				нцДля j := i до R + 1 шаг -1 делай dir[j] := dir[j - 1] кц;
				dir[R] := x
			кц
		кон SortDirDate;

	кон FileList;

перем
	DirTreePathProt : WMProperties.StringProperty;
	FileListPrefixSearchProt : WMProperties.BooleanProperty;

	GSonPathChanged, GSonPathChangedInfo : Strings.String;
	GSDirectoryTree, GSFileList : Strings.String;

проц GenFileList*() : XML.Element;
перем f : FileList;
нач
	нов(f); возврат f;
кон GenFileList;

проц GenDirectoryTree*() : XML.Element;
перем t : DirectoryTree;
нач
	нов(t); возврат t;
кон GenDirectoryTree;

проц InitStrings;
нач
	GSonPathChanged := Strings.NewString("onPathChanged");
	GSonPathChangedInfo := Strings.NewString("called when the path is changed");
	GSDirectoryTree := Strings.NewString("DirectoryTree");
	GSFileList := Strings.NewString("FileList");
кон InitStrings;

проц InitPrototypes;
нач
	нов(DirTreePathProt, НУЛЬ, Strings.NewString("CurrentPath"), Strings.NewString("contains the selected path"));
	нов(FileListPrefixSearchProt, НУЛЬ, Strings.NewString("PrefixSearch"), Strings.NewString("match prefix only"));
	FileListPrefixSearchProt.Set(истина);
кон InitPrototypes;

проц FileExists*(конст name : массив из симв8) : булево;
нач
	возврат Files.Old(name) # НУЛЬ
кон FileExists;

проц MakePathString*(перем s : массив из симв8);
перем l : размерМЗ;
нач
	l := Strings.Length(s);
	если (l > 1) и (s[l - 1] # ":") и (s[l - 1] # "/") то Strings.Append(s, "/") всё;
кон MakePathString;

нач
	InitStrings;
	InitPrototypes;
кон WMSystemComponents.

System.Free WMSystemComponents ~

