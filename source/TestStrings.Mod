модуль TestStrings; (** AUTHOR "staubesv"; PURPOSE "Testbed for Strings.Mod"; *)

использует
	НИЗКОУР,
	Commands, Strings, ЛогЯдра;

конст
	Step = матМаксимум(цел32) DIV 5;

проц TestIntegerConversion*(context : Commands.Context);
перем i, temp : цел32; string : массив 16 из симв8;
нач
	context.out.пСтроку8("Test integer <-> string conversion... "); context.out.ПротолкниБуферВПоток;
	i := матМинимум(цел32);
	нцПока (i < матМаксимум(цел32)) делай
		Strings.IntToStr(i, string);
		Strings.StrToInt(string, temp);
		утв(i = temp);
		увел(i);
		если (i остОтДеленияНа Step = 0) то
			если (i < 0) то
				context.out.пЦел64(округлиВниз(100* ((матМаксимум(цел32) + i)  / матМаксимум(цел32) / 2)), 0);
			иначе
				context.out.пЦел64(округлиВниз(100* (i / матМаксимум(цел32) / 2)) + 50, 0);
			всё;
			context.out.пСтроку8("% "); context.out.ПротолкниБуферВПоток;
		всё;
	кц;
	Strings.IntToStr(матМаксимум(цел32), string);
	Strings.StrToInt(string, temp);
	утв(temp = матМаксимум(цел32));
	context.out.пСтроку8("100% done."); context.out.пВК_ПС;
кон TestIntegerConversion;

проц TestHexConversion*(context : Commands.Context);
перем i, val : цел32; res: целМЗ; string : массив 16 из симв8;
нач
	context.out.пСтроку8("Test hex <-> string conversion... "); context.out.ПротолкниБуферВПоток;
	i := матМинимум(цел32);
	нцПока (i < матМаксимум(цел32)) делай
		Strings.IntToHexStr(i, 8, string);
		Strings.HexStrToInt(string, val, res);
		если (res # Strings.Ok) или (i # val) то
			context.out.пСтроку8("Error for string "); context.out.пСтроку8(string); context.out.пВК_ПС;
		всё;
		утв((res = Strings.Ok) и (i = val));
		увел(i);
		если (i остОтДеленияНа Step = 0) то
			если (i < 0) то
				context.out.пЦел64(округлиВниз(100* ((матМаксимум(цел32) + i)  / матМаксимум(цел32) / 2)), 0);
			иначе
				context.out.пЦел64(округлиВниз(100* (i / матМаксимум(цел32) / 2)) + 50, 0);
			всё;
			context.out.пСтроку8("% "); context.out.ПротолкниБуферВПоток;
		всё;
	кц;
	Strings.IntToHexStr(i, 8, string);
	Strings.HexStrToInt(string, val, res);
	утв((res = Strings.Ok) и (i = val));
	context.out.пСтроку8("100% done."); context.out.пВК_ПС;
кон TestHexConversion;

проц TestNegativeHexConversion*(context : Commands.Context);
перем i : цел32;

	проц Test(number : цел64);
	перем string, signedString : массив 16 из симв8; val, idx: цел32; res: целМЗ;
	нач
		Strings.IntToHexStr(number, 8, string);
		signedString[0] := "-";
		idx := 0; нцПока (string[idx] # 0X) делай signedString[idx + 1] := string[idx]; увел(idx); кц;
		signedString[idx + 1] := 0X;
		Strings.HexStrToInt(signedString, val, res);
		если (res # Strings.Ok) или (-i # val) то
			context.out.пСтроку8("Error for string "); context.out.пСтроку8(string); context.out.пВК_ПС;
		всё;
		утв((res = Strings.Ok) и (-i = val));
	кон Test;

	проц TestMaxLongintPlus1;
	перем string : массив 16 из симв8; val : цел32; res: целМЗ;
	нач
		string := "-80000000";
		Strings.HexStrToInt(string, val, res);
		утв((res = Strings.Ok) и (val = матМинимум(цел32)));
	кон TestMaxLongintPlus1;

нач
	context.out.пСтроку8("Test negative hex <-> string conversion... "); context.out.ПротолкниБуферВПоток;
	i := 0;
	нцПока (i < матМаксимум(цел32)) делай
		Test(i);
		увел(i);
		если (i остОтДеленияНа Step = 0) то
			если (i < 0) то
				context.out.пЦел64(округлиВниз(100* ((матМаксимум(цел32) + i)  / матМаксимум(цел32) / 2)), 0);
			иначе
				context.out.пЦел64(округлиВниз(100* (i / матМаксимум(цел32) / 2)) + 50, 0);
			всё;
			context.out.пСтроку8("% "); context.out.ПротолкниБуферВПоток;
		всё;
	кц;
	Test(матМаксимум(цел32));
	TestMaxLongintPlus1;
	context.out.пСтроку8("100% done."); context.out.пВК_ПС;
кон TestNegativeHexConversion;

проц TestSetConversion*(context : Commands.Context);
перем i : цел32; temp : мнвоНаБитахМЗ; string : массив 64 из симв8;
нач
	context.out.пСтроку8("Test set <-> string conversion...  "); context.out.ПротолкниБуферВПоток;
	i := матМинимум(цел32);
	нцПока (i < матМаксимум(цел32)) делай
		Strings.SetToStr(мнвоНаБитахМЗ(i), string);
		Strings.StrToSet(string, temp);
		если (мнвоНаБитахМЗ(i) # temp) то
			ЛогЯдра.пМнвоНаБитахМЗКакБиты(мнвоНаБитахМЗ(i), 0, 32); ЛогЯдра.пСтроку8(" # "); ЛогЯдра.пМнвоНаБитахМЗКакБиты(temp, 0, 32); ЛогЯдра.пВК_ПС;
		всё;
		утв(мнвоНаБитахМЗ( i) = temp);
		увел(i);
		если (i остОтДеленияНа Step = 0) то
			если (i < 0) то
				context.out.пЦел64(округлиВниз(100* ((матМаксимум(цел32) + i)  / матМаксимум(цел32) / 2)), 0);
			иначе
				context.out.пЦел64(округлиВниз(100* (i / матМаксимум(цел32) / 2)) + 50, 0);
			всё;
			context.out.пСтроку8("% "); context.out.ПротолкниБуферВПоток;
		всё;
	кц;
	Strings.SetToStr(мнвоНаБитахМЗ(матМаксимум(цел32)), string);
	Strings.StrToSet(string, temp);
	утв(мнвоНаБитахМЗ(матМаксимум(цел32)) = temp);
	context.out.пСтроку8("100% done."); context.out.пВК_ПС;
кон TestSetConversion;

проц TestSplitJoin*(context : Commands.Context);
перем string : массив 1024 из симв8; separator : симв8; sa : Strings.StringArray; s : Strings.String; i : размерМЗ;
нач
	separator := 0X;
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string);
	context.arg.ПропустиБелоеПоле; context.arg.чСимв8(separator);
	context.out.пСтроку8("String: '"); context.out.пСтроку8(string); context.out.пСтроку8("', separator: ");
	если (separator # 0X) то context.out.пСимв8(separator); иначе context.out.пСтроку8("none"); всё;
	context.out.пВК_ПС;
	sa := Strings.Split(string, separator);
	нцДля i := 0 до длинаМассива(sa)-1 делай
		context.out.пЦел64(i, 2); context.out.пСтроку8(": ");
		context.out.пСтроку8(sa[i]^);
		context.out.пВК_ПС;
	кц;
	s := Strings.Join(sa, 0, длинаМассива(sa)-1, separator);
	context.out.пСтроку8("Joined string: '"); context.out.пСтроку8(s^); context.out.пСтроку8("'"); context.out.пВК_ПС;
	context.out.пСтроку8("Success: ");
	если (s^ = string) то context.out.пСтроку8("Yes"); иначе context.out.пСтроку8("No"); всё;
	context.out.пВК_ПС;
кон TestSplitJoin;

проц PerformTests*(context : Commands.Context);
нач
	TestIntegerConversion(context);
	TestHexConversion(context);
	TestNegativeHexConversion(context);
	TestSetConversion(context);
кон PerformTests;

кон TestStrings.

TestStrings.TestIntegerConversion ~
TestStrings.TestHexConversion ~
TestStrings.TestNegativeHexConversion ~
TestStrings.TestSetConversion ~

TestStrings.TestSplitJoin A:/Test/HelloWord/Test.Mod / ~

TestStrings.PerformTests ~

System.Free TestStrings ~
