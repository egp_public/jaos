модуль TCPPortLog; (** AUTHOR "TF"; PURPOSE "TCP port logger"; *)

использует
	Clock, ЛогЯдра, Modules, Network, IP, TCP;

тип
	TimeDate = запись h, m, s, day,month,year: цел32 кон;

проц GetTime(перем dt: TimeDate);
нач
	Clock.Get(dt.h, dt.year);
	dt.s := dt.h остОтДеленияНа 64; dt.h := dt.h DIV 64;
	dt.m := dt.h остОтДеленияНа 64; dt.h := dt.h DIV 64;
	dt.h := dt.h остОтДеленияНа 24;
	dt.day := dt.year остОтДеленияНа 32; dt.year := dt.year DIV 32;
	dt.month := dt.year остОтДеленияНа 16; dt.year := dt.year DIV 16;
	увел(dt.year, 1900)
кон GetTime;

проц IntToStr(v, len: цел32; перем s: массив из симв8; перем pos: цел32);
перем i: цел32;
нач
	нцДля i := 1 до len делай s[pos+len-i] := симв8ИзКода(кодСимв8("0")+v остОтДеленияНа 10); v := v DIV 10 кц;
	увел(pos, len)
кон IntToStr;

проц TimeDateToStr(dt: TimeDate; перем s: массив из симв8);
перем p: цел32;
нач
	IntToStr(dt.day, 2, s, p); s[p] := "."; увел(p);
	IntToStr(dt.month, 2, s, p); s[p] := "."; увел(p);
	IntToStr(dt.year, 2, s, p); s[p] := " "; увел(p);
	IntToStr(dt.h, 2, s, p); s[p] := ":"; увел(p);
	IntToStr(dt.m, 2, s, p); s[p] := ":"; увел(p);
	IntToStr(dt.s, 2, s, p); s[p] := 0X
кон TimeDateToStr;

проц DumpListener(fip: IP.Adr; buffer: Network.Buffer);
перем fport, lport, flags: цел32; time: TimeDate; adrStr, timeStr: массив 32 из симв8;
нач
	GetTime(time); TimeDateToStr(time, timeStr);
	fport := Network.GetNet2(buffer.data, buffer.ofs);
	lport := Network.GetNet2(buffer.data, buffer.ofs+2);
	flags := кодСимв8(buffer.data[buffer.ofs+13]);
	IP.AdrToStr(fip, adrStr);
	ЛогЯдра.ЗахватВЕдиноличноеПользование;  ЛогЯдра.пСтроку8(timeStr);
	ЛогЯдра.пСтроку8(" Rejected TCP segment to port "); ЛогЯдра.пЦел64(lport, 1); ЛогЯдра.пСтроку8(" from "); ЛогЯдра.пСтроку8(adrStr);
	ЛогЯдра.пСтроку8(":"); ЛогЯдра.пЦел64(fport, 1); ЛогЯдра.пСтроку8(" {");
	если нечётноеЛи¿(арифмСдвиг(flags, -7)) то ЛогЯдра.пСтроку8(" res7") всё;
	если нечётноеЛи¿(арифмСдвиг(flags, -6)) то ЛогЯдра.пСтроку8(" res6") всё;
	если нечётноеЛи¿(арифмСдвиг(flags, -5)) то ЛогЯдра.пСтроку8(" URG") всё;
	если нечётноеЛи¿(арифмСдвиг(flags, -4)) то ЛогЯдра.пСтроку8(" ACK") всё;
	если нечётноеЛи¿(арифмСдвиг(flags, -3)) то ЛогЯдра.пСтроку8(" PSH") всё;
	если нечётноеЛи¿(арифмСдвиг(flags, -2)) то ЛогЯдра.пСтроку8(" RST") всё;
	если нечётноеЛи¿(арифмСдвиг(flags, -1)) то ЛогЯдра.пСтроку8(" SYN") всё;
	если нечётноеЛи¿(flags) то ЛогЯдра.пСтроку8(" FIN") всё;
	ЛогЯдра.пСтроку8(" } ");
	ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
кон DumpListener;

проц Install*;
нач
	TCP.SetDefaultListener(DumpListener);
	ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Default TCP port listener installed"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
кон Install;

проц Remove*;
нач
	TCP.SetDefaultListener(НУЛЬ);
	ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Default TCP port listener removed"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
кон Remove;

нач
	Modules.InstallTermHandler(Remove)
кон TCPPortLog.

TCPPortLog.Install

