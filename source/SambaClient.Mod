модуль SambaClient; (** AUTHOR "mancos"; PURPOSE "SMB Client"; *)

использует
	НИЗКОУР, Потоки, ЛогЯдра, Dates, Strings, Locks, Files, DNS, IP, TCP;

конст
	PID = 9876;
	NativeOS = "A2";
	NativeLanMan = "STECIFS";
	PrimaryDomain = "WORKGROUP";
	Trace = ложь;
	SendBufferSize = 32000;
	RWLimit = 2048;
	SMBPort* = 445;

тип
	Connection = укль на запись
		out: Потоки.Писарь;
		in: Потоки.Чтец;
		tid, uid, sid: цел16;
		ipaddr: массив 16 из симв8;
		user, pw: массив 64 из симв8;
		path, mask, fnLast: массив 256 из симв8;
	кон;

	FileSystem* = окласс(Files.FileSystem)	(** shareable *)
		перем
			c: Connection;
			connection: TCP.Connection;
			lock: Locks.RecursiveLock;

		(** Create a new file with the specified name.  End users use Files.New instead. *)

		проц {перекрыта}New0*(конст name: массив из симв8): Files.File;
		перем
			key: цел32;
			f: File;
		нач
			если Trace то ЛогЯдра.пСтроку8("FileSystem.New - Name: "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пВК_ПС(); всё;
			lock.Acquire();
			key := OpenAndX(c, name, 41H, истина);
			lock.Release();

			если key # 0 то
				нов(f);
				f.fs := сам;
				f.c := c;
				f.key := key;
				f.openRead := ложь;
				копируйСтрокуДо0(name, f.filename);
				возврат f;
			иначе
				возврат НУЛЬ;
			всё;
		кон New0;

		(** Open an existing file. The same file descriptor is returned if a file is opened multiple times.  End users use Files.Old instead. *)

		проц {перекрыта}Old0*(конст name: массив из симв8): Files.File;
		перем
			f: File;
			key: цел32;
		нач
			если Trace то ЛогЯдра.пСтроку8("FileSystem.Old - Name: "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пВК_ПС(); всё;

			key := FileKey(name);

			если key # 0 то
				нов(f);
				f.c := c;
				f.fs := сам;
				f.key := key;
				f.openRead := истина;
				копируйСтрокуДо0(name, f.filename);
				возврат f;
			иначе
				возврат НУЛЬ;
			всё;
		кон Old0;

		(** Delete a file. res = 0 indicates success.  End users use Files.Delete instead. *)

		проц {перекрыта}Delete0*(конст name: массив из симв8; перем key: цел32; перем res: целМЗ);
		перем
			check : булево;
			closekey : цел32;
			nameTrimmed: Files.FileName;
		нач
			если Trace то
				ЛогЯдра.пСтроку8("FileSystem.Delete"); ЛогЯдра.пВК_ПС();
				ЛогЯдра.пСтроку8(" -- Name: "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пВК_ПС();
			всё;

			lock.Acquire();
			closekey := OpenAndX(c, name, 40H, ложь);
			CloseFile(c, closekey);
			lock.Release();
			утв(длинаМассива(nameTrimmed) > Strings.Length(name));
			копируйСтрокуДо0(name,nameTrimmed);
			Strings.TrimLeft(nameTrimmed, симв8ИзКода(2FH));

			lock.Acquire();
			(* SEND *)
			SendSMBHeader(цел16(39 + Strings.Length(nameTrimmed)), 06X, c);
			c.out.пМладшийБайт_сп(1);								(* Word count *)
			c.out.пЦел16_мз(22);							(* search attributes *)
			c.out.пЦел16_мз(цел16(2 + Strings.Length(nameTrimmed)));
			c.out.пМладшийБайт_сп(4);								(* ascii *)
			c.out.пСтроку8˛включаяСимв0(nameTrimmed);						(* name *)
			c.out.ПротолкниБуферВПоток();

			(* RECEIVE *)
		 	check := RecieveResponse(06X, c);
		 	lock.Release();

			если ~check то
				если Trace то ЛогЯдра.пСтроку8(" -- ERROR on Delete"); ЛогЯдра.пВК_ПС(); всё;
				res := -1;
			иначе
				res := 0;
			всё;
		кон Delete0;

		(** Rename a file. res = 0 indicates success.  End users use Files.Rename instead. *)

		проц {перекрыта}Rename0*(конст old, new: массив из симв8; f: Files.File; перем res: целМЗ);
		перем
			check : булево;
			closekey : цел32;
			old1, new1: Files.FileName;
		нач
			если Trace то
				ЛогЯдра.пСтроку8("FileSystem.Rename"); ЛогЯдра.пВК_ПС();
				ЛогЯдра.пСтроку8(" -- Old: "); ЛогЯдра.пСтроку8(old); ЛогЯдра.пВК_ПС();
				ЛогЯдра.пСтроку8(" -- New: "); ЛогЯдра.пСтроку8(new); ЛогЯдра.пВК_ПС();
			всё;

			lock.Acquire();
			closekey := OpenAndX(c, old, 40H, ложь);
			CloseFile(c, closekey);
			lock.Release();
			копируйСтрокуДо0(old,old1);
			копируйСтрокуДо0(new,new1);
			ReplaceSlash(old1);
			ReplaceSlash(new1);

			lock.Acquire();
			(* SEND *)
			SendSMBHeader(цел16(41 + Strings.Length(new1) + Strings.Length(old1)), 07X, c);
			c.out.пМладшийБайт_сп(1);			(* Word count *)
			c.out.пЦел16_мз(22);		(* search attributes *)
			c.out.пЦел16_мз(цел16(4 + Strings.Length(new1) + Strings.Length(old1)));
			c.out.пМладшийБайт_сп(4);			(* ascii *)
			c.out.пСтроку8˛включаяСимв0(old1);	(* old name *)
			c.out.пМладшийБайт_сп(4);			(* ascii *)
			c.out.пСтроку8˛включаяСимв0(new1);	(* new name *)
			c.out.ПротолкниБуферВПоток();

			(* RECEIVE *)
		 	check := RecieveResponse(07X, c);
		 	lock.Release();

			если ~check то
				если Trace то ЛогЯдра.пСтроку8(" -- ERROR on Rename"); ЛогЯдра.пВК_ПС(); всё;
				res := -1;
			иначе
				res := 0;
			всё;
		кон Rename0;

		(** Enumerate canonical file names. mask may contain * wildcards.  For internal use only.  End users use Enumerator instead. *)

		проц {перекрыта}Enumerate0*(конст mask: массив из симв8; flags: мнвоНаБитахМЗ; enum: Files.Enumerator);
		перем
			check, endOfSearch, findFirst: булево;
			byteCount, dataOff, paraOff, eos: цел16;
			fileFlags : мнвоНаБитахМЗ;
			curPos: Files.Position;
			attr, nextEntryOff, progress: цел32; maskLen: размерМЗ;
			t: массив 2 из цел32;
			ch: массив 2 из симв8;
			dt: Dates.DateTime;
			date, time, size: цел32;
			maskModified, dirMask, filename: Files.FileName;
		нач
			endOfSearch := ложь;
			findFirst := истина;
			maskLen := Strings.Length(mask);
			копируйСтрокуДо0(mask, dirMask);
			копируйСтрокуДо0(mask, maskModified);
			ReplaceSlash(maskModified);

			если Trace то
				ЛогЯдра.пСтроку8("FileSystem.Enumerate"); ЛогЯдра.пВК_ПС();
				ЛогЯдра.пСтроку8(" -- Mask: "); ЛогЯдра.пСтроку8(maskModified); ЛогЯдра.пВК_ПС();
			всё;

			если maskModified = "*" то
				ch[0] := симв8ИзКода(92); ch[1] := 0X;
				Strings.Concat(ch, "*", c.mask);
				dirMask := "/";
			аесли Strings.EndsWith("\\*", maskModified) то
				Strings.Truncate(maskModified, maskLen - 2);
				Strings.Truncate(dirMask, maskLen - 2);
				Strings.Concat(maskModified, "*", c.mask);
			аесли Strings.EndsWith("\*", maskModified) то
				Strings.Truncate(maskModified, maskLen - 1);
				Strings.Truncate(dirMask, maskLen - 1);
				Strings.Concat(maskModified, "*", c.mask);
			иначе
				возврат;
			всё;

			нцПока ~endOfSearch делай

				lock.Acquire();

				если findFirst то
					check := Trans2Find(c, 1);
				иначе
					check := Trans2Find(c, 2);
				всё;

				если ~check то
					если Trace то ЛогЯдра.пСтроку8(" -- ERROR on Enumerate"); ЛогЯдра.пВК_ПС(); всё;
	 				lock.Release();
					возврат;
				всё;

				(* RECEIVE *)
				c.in.ПропустиБайты(1);		(* wct *)
				c.in.ПропустиБайты(2);		(* parameter count *)
				c.in.ПропустиБайты(2);		(* data count *)
				c.in.ПропустиБайты(2);		(* reserved *)
				c.in.ПропустиБайты(2);		(* parameter count *)
				c.in.чЦел16_мз(paraOff);	(* parameter offset *)
				c.in.ПропустиБайты(2);		(* parameter displacement *)
				c.in.чЦел16_мз(byteCount);	(* data count *)
				c.in.чЦел16_мз(dataOff);	(* data offset *)
				c.in.ПропустиБайты(2);		(* data displacement *)
				c.in.ПропустиБайты(1);		(* setup count *)
				c.in.ПропустиБайты(1);		(* reserved *)
				c.in.ПропустиБайты(2);		(* byte count *)
				c.in.ПропустиБайты(paraOff - 55);

				если findFirst то
					c.in.чЦел16_мз(c.sid);	(* search id *)
				всё;

				c.in.ПропустиБайты(2);		(* search count *)
				c.in.чЦел16_мз(eos);		(* end of search *)

				если eos = 1 то
					endOfSearch := истина;
				всё;

				c.in.ПропустиБайты(2);		(* EA error *)
				c.in.ПропустиБайты(2);		(* last name offset *)

				если findFirst то
					c.in.ПропустиБайты(dataOff - paraOff - 10);
				иначе
					c.in.ПропустиБайты(dataOff - paraOff - 8);
				всё;

				progress := 0;
				check := истина;

				нцПока progress < byteCount делай
					curPos := c.in.МестоВПотоке();
					c.in.чЦел32_мз(nextEntryOff);
					если nextEntryOff = 0 то
						check := ложь;
						progress := byteCount;
					иначе
						progress := progress + nextEntryOff;	(* next entry offset *)
						curPos := curPos + nextEntryOff;
					всё;
					c.in.ПропустиБайты(4);		(* file index *)
					c.in.ПропустиБайты(8);		(* creation time *)
					c.in.ПропустиБайты(8);		(* last access *)
					c.in.чЦел32_мз(t[0]);
					c.in.чЦел32_мз(t[1]);		(* last write *)
					GetDateTime(t, dt);
					если ~Dates.ValidDateTime(dt) то
						dt := Dates.Now();
						ЛогЯдра.пСтроку8("SambaClient: Replaced invalid date & time by current time"); ЛогЯдра.пВК_ПС;
					всё;
					Dates.DateTimeToOberon(dt, date, time);
					c.in.ПропустиБайты(8);		(* change *)
					c.in.чЦел32_мз(size);
					c.in.ПропустиБайты(4);		(* end of file *)
					c.in.ПропустиБайты(8);		(* allocation *)
					c.in.чЦел32_мз(attr);		(* attributes *)
					fileFlags := {};
					если (4 в мнвоНаБитахМЗ(attr)) то включиВоМнвоНаБитах(fileFlags, Files.Directory); всё;
					если (0 в мнвоНаБитахМЗ(attr)) то включиВоМнвоНаБитах(fileFlags, Files.ReadOnly); всё;
					c.in.ПропустиБайты(4);		(* name len *)
					c.in.ПропустиБайты(4);		(* ea len *)
					c.in.ПропустиБайты(1);		(* short file len *)
					c.in.ПропустиБайты(1);		(* reserved *)
					c.in.ПропустиБайты(24);		(* short file name *)
					c.in.чСтроку8˛включаяСимв0(filename);
					копируйСтрокуДо0(filename, c.fnLast);
					Strings.Concat(dirMask, filename, filename);
					Files.JoinName(prefix, filename, filename);

					enum.PutEntry(filename, fileFlags, time, date, size);

					нцПока (c.in.МестоВПотоке() # curPos) и check делай
						c.in.ПропустиБайты(1);
					кц;
				кц;
				findFirst := ложь;
				lock.Release();
			кц;
		кон Enumerate0;

		(** Return the unique non-zero key of the named file, if it exists. *)

		проц {перекрыта}FileKey*(конст name: массив из симв8): цел32;
		перем key: цел32;
		нач
			lock.Acquire();
			key := OpenAndX(c, name, 40H, ложь);
			lock.Release();
			возврат key;
		кон FileKey;

		(** Create a new directory structure. May not be supported by the actual implementation.
			End users use Files.CreateDirectory instead.*)

		проц {перекрыта}CreateDirectory0*(конст name: массив из симв8; перем res: целМЗ);
		перем check: булево;
		нач
			если Trace то
				ЛогЯдра.пСтроку8("FileSystem.CreateDirectory"); ЛогЯдра.пВК_ПС();
				ЛогЯдра.пСтроку8(" -- Name: "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пВК_ПС();
			всё;

			lock.Acquire();
			(* SEND *)
			SendSMBHeader(цел16(37+ Strings.Length(name)), 00X, c);
			c.out.пМладшийБайт_сп(0);											(* Word count *)
			c.out.пЦел16_мз(цел16(2 + Strings.Length(name)));			(* byte count *)
			c.out.пМладшийБайт_сп(4);											(* buffer format *)
			c.out.пСтроку8˛включаяСимв0(name);
			c.out.ПротолкниБуферВПоток();

			(* RECEIVE *)
		 	check := RecieveResponse(00X, c);
		 	lock.Release();

			если ~check то
				если Trace то ЛогЯдра.пСтроку8(" -- ERROR on CreateDirectory"); ЛогЯдра.пВК_ПС(); всё;
				res := -1;
			иначе
				res := 0;
			всё;
		кон CreateDirectory0;

		(** Remove a directory. If force=TRUE, any subdirectories and files should be automatically deleted.
			End users use Files.RemoveDirectory instead. *)

		проц {перекрыта}RemoveDirectory0*(конст name: массив из симв8; force: булево; перем key: цел32; перем res: целМЗ);
		перем check: булево;
		нач
			если Trace то
				ЛогЯдра.пСтроку8("FileSystem.DeleteDirectory"); ЛогЯдра.пВК_ПС();
				ЛогЯдра.пСтроку8(" -- Name: "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пВК_ПС();
			всё;

			lock.Acquire();
			(* SEND *)
			SendSMBHeader(цел16(37+ Strings.Length(name)), 01X, c);
			c.out.пМладшийБайт_сп(0);											(* Word count *)
			c.out.пЦел16_мз(цел16(2 + Strings.Length(name)));			(* byte count *)
			c.out.пМладшийБайт_сп(4);											(* buffer format *)
			c.out.пСтроку8˛включаяСимв0(name);
			c.out.ПротолкниБуферВПоток();

			(* RECEIVE *)
		 	check := RecieveResponse(01X, c);
		 	lock.Release();

			если ~check то
				если Trace то ЛогЯдра.пСтроку8(" -- ERROR on DeleteDirectory"); ЛогЯдра.пВК_ПС(); всё;
				res := -1;
			иначе
				res := 0;
			всё;
		кон RemoveDirectory0;

		(** Finalize the file system. *)

		проц {перекрыта}Finalize*;
		нач
			Finalize^;
			connection.Закрой();
		кон Finalize;

	кон FileSystem;

тип

	File* = окласс(Files.File)	(** sharable *)
		перем
			c: Connection;
			filename: массив 256 из симв8;
			openRead: булево;

		(** Position a Rider at a certain position in a file. Multiple Riders can be positioned at different locations in a file. A Rider cannot be positioned beyond the end of a file. *)

		проц {перекрыта}Set*(перем r: Files.Rider; pos: Files.Position);
		нач
			r.apos := pos;
			r.file := сам;
		кон Set;

		(** Return the offset of a Rider positioned on a file. *)

		проц {перекрыта}Pos*(перем r: Files.Rider): Files.Position;
		нач
			возврат r.apos;
		кон Pos;

		(** Read a byte from a file, advancing the Rider one byte further.  R.eof indicates if the end of the file has been passed. *)

		проц {перекрыта}Read*(перем r: Files.Rider; перем x: симв8);
		перем a : массив 1 из симв8;
		нач
			a[0] := x;
			ReadBytes(r,a,0,1);
		кон Read;

		(** Read a sequence of len bytes into the buffer x at offset ofs, advancing the Rider. Less bytes will be read when reading over the end of the file. r.res indicates the number of unread bytes. x must be big enough to hold all the bytes. *)

		проц {перекрыта}ReadBytes*(перем r: Files.Rider; перем x: массив из симв8; ofs, len: размерМЗ);
		перем
			check: булево;
			dataOff, byteCount, padding: цел16;
			i : размерМЗ;
			adjLen, adjOff: цел16;
			localKey: цел16;
		нач
			если Trace то ЛогЯдра.пСтроку8("File.ReadBytes - Read AndX"); ЛогЯдра.пВК_ПС(); всё;

			fs(FileSystem).lock.Acquire();

			если ~(openRead и (key # 0)) то
				localKey := OpenAndX(c, filename, 40H, ложь);

				если localKey # 0 то
					key := localKey;
					openRead := истина;
				иначе
					r.res := len;
					fs(FileSystem).lock.Release();
					возврат;
				всё;
			всё;

		 	adjOff := 0;
			r.res := 0;
		 	нцПока len > 0 делай
	 			если len > RWLimit то
	 				adjLen := RWLimit;
	 			иначе
	 				adjLen := цел16(len);
	 			всё;

			 	(* SEND *)
			 	SendSMBHeader(55, 2EX, c);
			 	c.out.пМладшийБайт_сп(10);				(* word count *)
			 	c.out.пМладшийБайт_сп(255);			(* andx*)
			 	c.out.пМладшийБайт_сп(0);				(* reserved *)
			 	c.out.пЦел16_мз(0);				(* andx offset *)
			 	c.out.пЦел16_мз(устарПреобразуйКБолееУзкомуЦел(key));	(* fid *)
			 	c.out.пЦел32_мз(r.apos(цел32));	(* offset *)
			 	c.out.пЦел16_мз(adjLen);		(* max count low *)
			 	c.out.пЦел16_мз(adjLen);		(* min count *)
			 	c.out.пЦел32_сп(0);				(* max count 64k *)
			 	c.out.пЦел16_мз(0);				(* remaining *)
			 	c.out.пЦел16_мз(0);				(* byte count *)
			 	c.out.ПротолкниБуферВПоток();

			 	(* RECEIVE *)
			 	check := RecieveResponse(2EX, c);

				если ~check то
					если Trace то ЛогЯдра.пСтроку8(" -- ERROR on Read AndX"); ЛогЯдра.пВК_ПС(); всё;
					r.res := r.res + len;
					fs(FileSystem).lock.Release();
					возврат;
				всё;

				c.in.ПропустиБайты(13);
				c.in.чЦел16_мз(dataOff);
				c.in.ПропустиБайты(10);
				c.in.чЦел16_мз(byteCount);

				если (dataOff = 0) то
					r.res := r.res + len;
					fs(FileSystem).lock.Release();
					возврат;
				всё;

				padding := dataOff - 59;
				c.in.ПропустиБайты(padding);
				byteCount := byteCount - padding;

				если Trace то ЛогЯдра.пСтроку8(" -- ByteCount: "); ЛогЯдра.пЦел64(byteCount, 0); ЛогЯдра.пВК_ПС(); всё;

				i := adjOff;

				c.in.чБайты(x, ofs, byteCount, i);
				ofs := ofs + i;
				r.apos := r.apos + i;

				r.res := r.res + adjLen - i;
				len := len - adjLen;
				adjOff := adjOff + adjLen;
			кц;
			fs(FileSystem).lock.Release();
		кон ReadBytes;

		(** Write a byte into the file at the Rider position, advancing the Rider by one. *)
		проц {перекрыта}Write*(перем r: Files.Rider; x: симв8);
		перем
			a: массив 1 из симв8;
		нач
			a[0] := x;
			WriteBytes(r,a,0,1);
		кон Write;

		(** Write the buffer x containing len bytes (starting at offset ofs) into a file at the Rider position. *)

		проц {перекрыта}WriteBytes*(перем r: Files.Rider; конст x: массив из симв8; ofs, len: размерМЗ);
		перем
			check: булево;
			bytesWritten: цел16;
			adjLen, adjOff: цел16;
			localKey: цел16;
		нач
			fs(FileSystem).lock.Acquire();

			если ~(~openRead и (key # 0)) то
				localKey := OpenAndX(c, filename, 41H, ложь);

				если localKey # 0 то
					key := localKey;
					openRead := ложь;
				иначе
					r.res := len;
					fs(FileSystem).lock.Release();
					возврат;
				всё;
			всё;

			если Trace то
				ЛогЯдра.пСтроку8("FileSystem.WriteBytes - Write AndX");
				ЛогЯдра.пСтроку8(" ("); ЛогЯдра.пЦел64(len, 0); ЛогЯдра.пСтроку8(" bytes from offset ");
				ЛогЯдра.пЦел64(ofs, 0); ЛогЯдра.пСтроку8(")");
				ЛогЯдра.пВК_ПС();
			всё;

	 		fs(FileSystem).lock.Acquire();
	 		adjOff := 0;

	 		нцПока len > 0 делай
	 			если len > RWLimit то
	 				adjLen := RWLimit;
	 			иначе
	 				adjLen := len(цел16);
	 			всё;

			 	(* Send *)
			 	SendSMBHeader(59 + adjLen, 2FX, c);
			 	c.out.пМладшийБайт_сп(12);				(* word count *)
			 	c.out.пМладшийБайт_сп(255);			(* andx*)
			 	c.out.пМладшийБайт_сп(0);				(* reserved *)
			 	c.out.пЦел16_мз(0);				(* andx offset *)
			 	c.out.пЦел16_мз(устарПреобразуйКБолееУзкомуЦел(key));	(* fid *)
			 	c.out.пЦел32_мз(r.apos(цел32));	(* offset *)
			 	c.out.пЦел32_мз(0);			(* reserved *)
			 	c.out.пЦел16_мз(0);				(* write mode *)
			 	c.out.пЦел16_мз(0);				(* remaining *)
			 	c.out.пЦел16_мз(0);				(* max count 64k *)
			 	c.out.пЦел16_мз(adjLen);		(* max count low *)
			 	c.out.пЦел16_мз(59);			(* data offset*)
			 	c.out.пЦел16_мз(adjLen);		(* byte count *)

			 	если adjLen # 0 то
			 		если Trace то ЛогЯдра.пСтроку8(" -- Write bytes: "); ЛогЯдра.пЦел64(adjLen, 0); ЛогЯдра.пВК_ПС(); всё;
			 		c.out.пБайты(x, adjOff, adjLen);
			 	иначе
			 		если Trace то ЛогЯдра.пСтроку8(" -- No bytes written!"); ЛогЯдра.пВК_ПС(); всё;
			 	всё;
			 	c.out.ПротолкниБуферВПоток();

				(* RECEIVE *)
				check := RecieveResponse(2FX, c);

				если ~check то
	 				если Trace то ЛогЯдра.пСтроку8(" -- ERROR on Write AndX"); ЛогЯдра.пВК_ПС(); всё;
					fs(FileSystem).lock.Release();
					возврат;
				всё;

				c.in.ПропустиБайты(5);
				c.in.чЦел16_мз(bytesWritten);

				если Trace то
					ЛогЯдра.пСтроку8(" -- Bytes written: "); ЛогЯдра.пЦел64(bytesWritten, 0); ЛогЯдра.пВК_ПС();
				всё;

				r.apos := r.apos + bytesWritten;
				len := len - adjLen;
				adjOff := adjOff + adjLen;
			кц;
			fs(FileSystem).lock.Release();
		кон WriteBytes;

		(** Return the current length of a file. *)

		проц {перекрыта}Length*(): Files.Size;
		перем
			filesize: цел32;
			check: булево;
		нач
			fs(FileSystem).lock.Acquire();

			если Trace то
				ЛогЯдра.пСтроку8("File.Length"); ЛогЯдра.пВК_ПС();
				ЛогЯдра.пСтроку8(" -- Name: "); ЛогЯдра.пСтроку8(filename); ЛогЯдра.пВК_ПС();
			всё;

		 	(* SEND *)
		 	SendSMBHeader(37 + цел16(Strings.Length(filename)), 08X, c);
			c.out.пМладшийБайт_сп(0);											(* word count *)
			c.out.пЦел16_мз(цел16(Strings.Length(filename)) + 1);		(* byte count *)
			c.out.пМладшийБайт_сп(4);											(* buffer format ascii *)
			c.out.пСтроку8˛включаяСимв0(filename);								(* filename *)
			c.out.ПротолкниБуферВПоток();

			если Trace то ЛогЯдра.пСтроку8(" -- Query Information Request sent"); ЛогЯдра.пВК_ПС(); всё;

			(* RECEIVE *)
		 	check := RecieveResponse(08X, c);

			если ~check то
				если Trace то
	 				ЛогЯдра.пСтроку8(" -- ERROR on Query Information Request"); ЛогЯдра.пВК_ПС();
	 			всё;
				fs(FileSystem).lock.Release();
				возврат 0;
			всё;

			c.in.ПропустиБайты(7);
			c.in.чЦел32_мз(filesize);

			fs(FileSystem).lock.Release();

			если Trace то ЛогЯдра.пСтроку8(" -- File size: "); ЛогЯдра.пЦел64(filesize, 0); ЛогЯдра.пВК_ПС(); всё;
			возврат filesize;
		кон Length;

		(** Return the time (t) and date (d) when a file was last modified. *)

		проц {перекрыта}GetDate*(перем t, d: цел32);
		нач СТОП(301) кон GetDate;	(* abstract *)

		(** Set the modification time (t) and date (d) of a file. *)

		проц {перекрыта}SetDate*(t, d: цел32);
		нач СТОП(301) кон SetDate;	(* abstract *)

		(** Return the canonical name of a file. *)

		проц {перекрыта}GetName*(перем name: массив из симв8);
		нач
			Files.JoinName(fs.prefix, filename, name);
			если Trace то
				ЛогЯдра.пСтроку8("File.GetName"); ЛогЯдра.пВК_ПС();
				ЛогЯдра.пСтроку8(" -- Name: "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пВК_ПС();
			всё;
		кон GetName;

		(** Register a file created with New in the directory, replacing the previous file in the directory with the same name. The file is automatically updated.  End users use Files.Register instead. *)

		проц {перекрыта}Register0*(перем res: целМЗ);
		нач кон Register0;

		(** Flush the changes made to a file from its buffers. Register0 will automatically update a file. *)

		проц {перекрыта}Update*;
		нач кон Update;

	кон File;

тип

	TCPSender = окласс
	перем
		connection: TCP.Connection;

		проц Connect(конст host: массив из симв8; port: цел32; перем c: Connection);
		перем
			fadr: IP.Adr;
			res: целМЗ;
		нач {единолично}
			res := 0;
			DNS.HostByName(host, fadr, res);
			если res = DNS.Ok то
				нов(connection);
				connection.Open(TCP.NilPort, fadr, port, res);
				если res = TCP.Ok то
					если Trace то ЛогЯдра.пСтроку8("Connection open!"); ЛогЯдра.пВК_ПС(); всё;
					нов(c.out, connection.ЗапишиВПоток, SendBufferSize);
					нов(c.in, connection.ПрочтиИзПотока, SendBufferSize);
				всё;
			всё;
		кон Connect;

	кон TCPSender;

проц SendSMBHeader(ntb: цел16; cmd: симв8; c: Connection);
нач
	(* NETBIOS *)
	c.out.п2МладшихБайта_сп(0);
	c.out.п2МладшихБайта_сп(ntb);

	(* SMB *)
	c.out.пСимв8(симв8ИзКода(255));
	c.out.пСтроку8("SMB");
	c.out.пСимв8(cmd);		(* Command *)
	c.out.пЦел32_сп(0);			(* status code *)
	c.out.пМладшийБайт_сп(24);			(* FLAGS *)
	c.out.пЦел16_мз(1);			(* FLAGS 2 *)
	c.out.пЦел32_сп(0);
	c.out.пЦел32_сп(0);			(* EXTRA *)
	c.out.пЦел32_сп(0);
	c.out.пЦел16_мз(c.tid);		(* TID *)
	c.out.пЦел16_мз(PID);		(* PID *)
	c.out.пЦел16_мз(c.uid);		(* UID *)
	c.out.пЦел16_мз(0);			(* MID *)
кон SendSMBHeader;

проц RecieveResponse(cmd: симв8; c: Connection): булево;
перем
	check: булево;
	variable: цел16;
нач
	check := ложь;
	c.in.ОчистьСостояниеБуфера();

	(* NETBIOS *)
	c.in.ПропустиБайты(4);

	(* SMB *)
	check := CheckFFSMB(c);

	если ~check то
		если Trace то ЛогЯдра.пСтроку8("SMB Header does not start with 0xFF SMB");	 ЛогЯдра.пВК_ПС(); всё;
		c.in.ОчистьСостояниеБуфера();
		возврат ложь;
	всё;

	variable := устарПреобразуйКБолееУзкомуЦел(c.in.чЦел8сп());

	если симв8ИзКода(variable) # cmd то
		если Trace то ЛогЯдра.пСтроку8("SMB Command is NOT "); ЛогЯдра.пСимв8(cmd); ЛогЯдра.пВК_ПС(); всё;
		c.in.ОчистьСостояниеБуфера();
		возврат ложь;
	всё;

	variable := устарПреобразуйКБолееУзкомуЦел(c.in.чЦел32_сп());

	если variable # 0 то
		если Trace то ЛогЯдра.пСтроку8("There has been a DOS error"); ЛогЯдра.пВК_ПС(); всё;
		c.in.ОчистьСостояниеБуфера();
		возврат ложь;
	всё;

	c.in.ПропустиБайты(15);
	c.in.чЦел16_мз(variable);

	если (c.tid = 0) и (variable > 0) то
		c.tid := variable;
	аесли (c.tid = variable) то
		(* OK *)
	аесли (c.tid # variable) то
		если Trace то ЛогЯдра.пСтроку8(" -- TID does not match"); ЛогЯдра.пВК_ПС(); всё;
		возврат ложь;
	иначе
		если Trace то ЛогЯдра.пСтроку8(" -- TID Error "); ЛогЯдра.пЦел64(variable, 0); ЛогЯдра.пВК_ПС(); всё;
		возврат ложь;
	всё;

	c.in.чЦел16_мз(variable);

	если variable # PID то
		если Trace то ЛогЯдра.пСтроку8(" -- PID does not match"); ЛогЯдра.пВК_ПС(); всё;
		возврат ложь;
	всё;

	c.in.чЦел16_мз(variable);

	если (c.uid = 0) и (variable > 0) то
		c.uid := variable;
	аесли (c.uid = variable) то
		(* OK *)
	аесли (c.uid # variable) то
		если Trace то ЛогЯдра.пСтроку8(" -- UID does not match"); ЛогЯдра.пВК_ПС(); всё;
		возврат ложь;
	иначе
		если Trace то ЛогЯдра.пСтроку8(" -- UID Error "); ЛогЯдра.пЦел64(variable, 0); ЛогЯдра.пВК_ПС(); всё;
		возврат ложь;
	всё;

	c.in.ПропустиБайты(2);
	возврат истина;
кон RecieveResponse;

проц CheckFFSMB(c: Connection): булево;
перем
	variable: цел8;
нач
	c.in.чЦел8_мз(variable);
	если variable = -1 то
		c.in.чЦел8_мз(variable);
		если variable = 83 то
			c.in.чЦел8_мз(variable);
			если variable = 77 то
				c.in.чЦел8_мз(variable);
				если variable = 66 то
					возврат истина;
				всё;
			всё;
		всё;
	всё;
	возврат ложь;
кон CheckFFSMB;

проц ReplaceSlash(перем name: массив из симв8);
перем
	i: цел32;
нач
	i := 0;
	нцПока (i < Strings.Length(name)) делай
		если name[i] = симв8ИзКода(2FH) то
			name[i] := симв8ИзКода(5CH);
		всё;
		увел(i)
	кц;
кон ReplaceSlash;

проц NegotiateProtocol(c: Connection): булево;
перем
	check : булево;
	variable: цел32;
нач
	если Trace то ЛогЯдра.пСтроку8("Negotiate Protocol"); ЛогЯдра.пВК_ПС(); всё;

	(* SEND *)
	SendSMBHeader(47, 72X, c);
	c.out.пМладшийБайт_сп(0);			(* Word count *)
	c.out.пМладшийБайт_сп(12);			(* Byte count *)
	c.out.пМладшийБайт_сп(0);
	c.out.пСимв8(2X);
	c.out.пСтроку8˛включаяСимв0("NT LM 0.12");
	c.out.ПротолкниБуферВПоток();

	(* RECEIVE *)
	check := RecieveResponse(72X, c);

	если ~check то
		возврат ложь;
	всё;

	variable := c.in.чЦел8сп();

	если variable # 17 то
		если Trace то ЛогЯдра.пСтроку8(" -- Message Size is not 17: "); ЛогЯдра.пЦел64(variable, 2); ЛогЯдра.пВК_ПС(); всё;
		возврат ложь;
	иначе
		если Trace то ЛогЯдра.пСтроку8(" -- Message Size is 17: NT LM 0.12"); ЛогЯдра.пВК_ПС(); всё;
		возврат истина;
	всё;
кон NegotiateProtocol;

проц SessionSetup(c: Connection): булево;
перем
	byteCount: цел16;
	check : булево;
нач
	если Trace то ЛогЯдра.пСтроку8("Session Setup"); ЛогЯдра.пВК_ПС(); всё;

	(* SEND *)
	byteCount := цел16(
		Strings.Length(c.user)
		+ Strings.Length(c.pw)
		+ Strings.Length(PrimaryDomain)
		+ Strings.Length(NativeOS)
		+ Strings.Length(NativeLanMan));

	SendSMBHeader(66 + byteCount, 73X, c);
	c.out.пМладшийБайт_сп(13);			(* Word count *)
	c.out.пМладшийБайт_сп(255);		(* no andx *)
	c.out.пМладшийБайт_сп(0);			(* reserved *)
	c.out.пЦел16_мз(0);			(* andx offset *)
	c.out.пЦел16_мз(32767);	(* buffersize *)
	c.out.пЦел16_мз(2);			(* mpx *)
	c.out.пЦел16_мз(0);			(* Vc *)
	c.out.пЦел32_сп(0);			(* session key *)
	c.out.пЦел16_мз(цел16(Strings.Length(c.pw)+1));	(* ANSI len *)
	c.out.пЦел16_мз(0);	(* UNICODE len *)
	c.out.пЦел32_сп(0);			(* reserved *)
	c.out.пЦел32_сп(268435456);	(* capabilities *)
	c.out.пМладшийБайт_сп(byteCount + 5);
	c.out.пМладшийБайт_сп(0);
	c.out.пСтроку8˛включаяСимв0(c.pw);
	c.out.пСтроку8˛включаяСимв0(c.user);
	c.out.пСтроку8˛включаяСимв0(PrimaryDomain);
	c.out.пСтроку8˛включаяСимв0(NativeOS);
	c.out.пСтроку8˛включаяСимв0(NativeLanMan);
	c.out.ПротолкниБуферВПоток();

	(* RECEIVE *)
	check := RecieveResponse(73X, c);

	если ~check то
		возврат ложь;
	всё;

	если Trace то ЛогЯдра.пСтроку8(" -- UID: "); ЛогЯдра.пЦел64(c.uid, 0); ЛогЯдра.пВК_ПС(); всё;
	возврат истина;
кон SessionSetup;

проц TreeConnect(c: Connection): булево;
перем
	check : булево;
нач
	если Trace то ЛогЯдра.пСтроку8("Tree Connect"); ЛогЯдра.пВК_ПС(); всё;

	(* SEND *)
	SendSMBHeader(54 + цел16(Strings.Length(c.ipaddr) + Strings.Length(c.path) + Strings.Length(c.pw)), 75X, c);
	c.out.пМладшийБайт_сп(4);			(* Word count *)
	c.out.пМладшийБайт_сп(255);		(* no andx *)
	c.out.пМладшийБайт_сп(0);			(* reserved *)
	c.out.пЦел16_мз(0);			(* andx offset *)
	c.out.пЦел16_мз(0);			(* disconnected tid *)
	c.out.пЦел16_мз(цел16(Strings.Length(c.pw)+1));	(* pw length *)
	c.out.пЦел16_мз(цел16(11 + Strings.Length(c.ipaddr) + Strings.Length(c.path) + Strings.Length(c.pw)));		(* bcc *)
	c.out.пСтроку8˛включаяСимв0(c.pw);
	c.out.пСтроку8("\\");
	c.out.пСтроку8(c.ipaddr);
	c.out.пСтроку8("\");
	c.out.пСтроку8˛включаяСимв0(c.path);
	c.out.пСтроку8˛включаяСимв0("?????");
	c.out.ПротолкниБуферВПоток();

	(* RECEIVE *)
	check := RecieveResponse(75X, c);

	если ~check то
		возврат ложь;
	всё;

	если Trace то ЛогЯдра.пСтроку8(" -- TID : "); ЛогЯдра.пЦел64(c.tid, 0); ЛогЯдра.пВК_ПС(); всё;

	возврат истина;
кон TreeConnect;

проц Trans2Find(c: Connection; cmd: цел16): булево;
перем
	check : булево;
	len: цел16;
нач
	если Trace то ЛогЯдра.пСтроку8("TRANS 2 - "); всё;

	если cmd = 1 то			(* FIND FIRST *)
		len := цел16(Strings.Length(c.mask));
		если Trace то ЛогЯдра.пСтроку8("FIND FIRST"); ЛогЯдра.пВК_ПС; всё;
	аесли cmd = 2 то		(* FIND NEXT *)
		len := цел16(Strings.Length(c.fnLast));
		если Trace то ЛогЯдра.пСтроку8("FIND NEXT"); ЛогЯдра.пВК_ПС; всё;
	иначе
		возврат ложь;
	всё;

	(* SEND *)
	SendSMBHeader(81 + len, 32X, c);
	c.out.пМладшийБайт_сп(15);			(* Word count *)
	c.out.пЦел16_мз(13+len);	(* parameter count 18 *)
	c.out.пЦел16_мз(0);			(* data count *)
	c.out.пЦел16_мз(10);		(* max par count *)
	c.out.пЦел16_мз(-1);		(* max data count *)
	c.out.пМладшийБайт_сп(0);			(* max setup *)
	c.out.пМладшийБайт_сп(0);			(* reserved *)
	c.out.пЦел16_мз(0);			(* flags *)
	c.out.пЦел32_сп(0);			(* timeout *)
	c.out.пЦел16_мз(0);			(* reserved *)
	c.out.пЦел16_мз(13+len);	(* parameter count 18 *)
	c.out.пЦел16_мз(68);		(* par offset *)
	c.out.пЦел16_мз(0);			(* data count *)
	c.out.пЦел16_мз(0);			(* data offset 86 *)
	c.out.пМладшийБайт_сп(1);			(* setup count *)
	c.out.пМладшийБайт_сп(0);			(* reserved *)
	c.out.пЦел16_мз(cmd);		(* subcommand *)
	c.out.пЦел16_мз(16+len);	(* byte cnt 21 *)
	c.out.пМладшийБайт_сп(0);			(* padding *)
	c.out.пМладшийБайт_сп(0);			(* padding *)
	c.out.пМладшийБайт_сп(0);			(* padding *)

	если cmd = 1 то
		c.out.пЦел16_мз(22);		(* search attri *)
		c.out.пЦел16_мз(25);		(* search count 10 *)
		c.out.пЦел16_мз(6);			(* flags *)
		c.out.пЦел16_мз(260);		(* loi *)
		c.out.пЦел32_сп(0);			(* storage *)
		c.out.пСтроку8˛включаяСимв0(c.mask);
	аесли cmd = 2 то
		c.out.пЦел16_мз(c.sid);		(* sid *)
		c.out.пЦел16_мз(25);		(* search count 10 *)
		c.out.пЦел16_мз(260);		(* loi *)
		c.out.пЦел32_сп(0);			(* resume *)
		c.out.пЦел16_мз(6);			(* flags *)
		c.out.пСтроку8˛включаяСимв0(c.fnLast);
	иначе
		возврат ложь;
	всё;
	c.out.ПротолкниБуферВПоток();

	(* RECEIVE *)
	check := RecieveResponse(32X, c);

	если ~check то
		возврат ложь;
	всё;

	возврат истина;
кон Trans2Find;

проц OpenAndX(c: Connection; name: массив из симв8; access: цел16; create: булево): цел16;
перем
	check: булево;
	fid: цел16;
нач
	если Trace то
		ЛогЯдра.пСтроку8("Open AndX"); ЛогЯдра.пВК_ПС();
		ЛогЯдра.пСтроку8(" -- Name: "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пВК_ПС();
		ЛогЯдра.пСтроку8(" -- Access: "); ЛогЯдра.пЦел64(access, 0); ЛогЯдра.пВК_ПС();
		ЛогЯдра.пСтроку8(" -- Create: "); ЛогЯдра.пБулево(create); ЛогЯдра.пВК_ПС();
	всё;

	ReplaceSlash(name);

	(* SEND *)
	SendSMBHeader(66 + цел16(Strings.Length(name)), 2DX, c);
	c.out.пМладшийБайт_сп(15);			(* word count *)
	c.out.пМладшийБайт_сп(255);		(* andx*)
	c.out.пМладшийБайт_сп(0); 			(* reserved *)
	c.out.пЦел16_мз(0);			(* andx offset *)
	c.out.пЦел16_мз(0);			(* flags *)
	c.out.пЦел16_мз(access);	(* desired access *)
	c.out.пЦел16_мз(6);			(* search attributes *)
	c.out.пЦел16_мз(0);			(* file attributes *)
	c.out.пЦел32_сп(0); 			(* create.time *)

	если create то
		c.out.пЦел16_мз(17);
	иначе
		c.out.пЦел16_мз(1);		(* open function *)
	всё;

	c.out.пЦел32_сп(0);			(* allocation size *)
	c.out.пЦел32_сп(0);			(* timeout *)
	c.out.пЦел32_сп(0);			(* reserved *)
	c.out.пЦел16_мз(1 + цел16(Strings.Length(name)));	(* byte count *)
	c.out.пСтроку8˛включаяСимв0(name);
	c.out.ПротолкниБуферВПоток();

	(* RECEIVE *)
	check := RecieveResponse(2DX, c);

	если ~check то
		если Trace то ЛогЯдра.пСтроку8(" -- ERROR on Open AndX - FID: 0"); ЛогЯдра.пВК_ПС(); всё;
		возврат 0;
	всё;

	c.in.ПропустиБайты(5);
	c.in.чЦел16_мз(fid);

	если Trace то ЛогЯдра.пСтроку8(" -- FID: "); ЛогЯдра.пЦел64(fid, 0); ЛогЯдра.пВК_ПС(); всё;

	возврат fid;
кон OpenAndX;

проц CloseFile(c: Connection; key: цел32);
перем
	check: булево;
нач
	если Trace то ЛогЯдра.пСтроку8("Close File"); ЛогЯдра.пВК_ПС(); всё;

	(* SEND *)
	SendSMBHeader(41, 04X, c);
	c.out.пМладшийБайт_сп(3);					(* Word count *)
	c.out.пЦел16_мз(устарПреобразуйКБолееУзкомуЦел(key));		(* fid *)
	c.out.пЦел32_сп(0);					(* last write *)
	c.out.пЦел16_мз(0);					(* byte count *)
	c.out.ПротолкниБуферВПоток();

	(* RECEIVE *)
	check := RecieveResponse(04X, c);
кон CloseFile;

проц NewFS*(context: Files.Parameters);
перем
	fs: FileSystem;
	connection: TCP.Connection;
	c: Connection;
	check: булево;
нач
	если Files.This(context.prefix) = НУЛЬ то
		нов(c);
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(c.ipaddr);
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(c.path);
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(c.user);
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(c.pw);

		если Trace то
			ЛогЯдра.пСтроку8("Connecting to "); ЛогЯдра.пСтроку8(c.ipaddr); ЛогЯдра.пВК_ПС();
			ЛогЯдра.пСтроку8("Path: "); ЛогЯдра.пСтроку8(c.path); ЛогЯдра.пВК_ПС();
		всё;

		check := StartClient(c, connection);

		если (~check) или (connection = НУЛЬ) то
			context.error.пСтроку8("CONNECTION ERROR!"); context.error.пВК_ПС;
			возврат;
		всё;

		нов(fs);
		fs.desc := "SmbFS";
		fs.c := c;
		нов(fs.lock);
		fs.connection := connection;
		Files.Add(fs, context.prefix);
	иначе
		context.error.пСтроку8("DiskFS: "); context.error.пСтроку8(context.prefix); context.error.пСтроку8(" already in use");
		context.error.пВК_ПС;
	всё;
кон NewFS;

проц StartClient(перем c: Connection; перем connection: TCP.Connection): булево;
перем
	tcpsender: TCPSender;
	check: булево;
нач
	c.tid := 0;
	c.uid := 0;
	нов(tcpsender);
	tcpsender.Connect(c.ipaddr, SMBPort, c);

	если (c.in # НУЛЬ) и (c.out # НУЛЬ) то
		connection := tcpsender.connection;
		check := NegotiateProtocol(c);
		check := check и SessionSetup(c);
		check := check и TreeConnect(c);
		возврат check;
	иначе
		возврат ложь;
	всё;
кон StartClient;

проц GetDateTime(t: массив из цел32; перем datetime: Dates.DateTime);
перем
	second, minute, hour, day, month, year, totalDays, NofDaysMnth: цел32;
	tsh: цел64;
	ts: цел32;
	continue: булево;
нач
	tsh := t[1] * 100000000H + t[0];
	tsh := tsh DIV 10000000;
	tsh := tsh - 11644473600;
	ts := устарПреобразуйКБолееУзкомуЦел(tsh);
	second := ts остОтДеленияНа 60;
	minute := (ts остОтДеленияНа 3600) DIV 60;
	hour := (ts остОтДеленияНа 86400) DIV 3600;
	ts := ts - (hour * 3600) - (minute * 60) - second;
	totalDays := ts DIV 86400;
	year := 1970;
	continue := истина;

	нцПока (totalDays > 365) и continue делай
		если Dates.LeapYear(year) то
			если totalDays > 366 то
				totalDays := totalDays - 366;
			иначе
				умень(year);
				continue := ложь;
			всё;
		иначе
			totalDays := totalDays - 365;
		всё;
		увел(year);
	кц;

	month := 1;
	continue := истина;

	нцПока (totalDays > 28)  и continue делай
		NofDaysMnth := Dates.NofDays(year, month);
		если totalDays >= NofDaysMnth то
			увел(month);
			totalDays := totalDays - NofDaysMnth;
		иначе
			continue := ложь;
		всё;
	кц;

	day := totalDays + 1;
	datetime.year := year;
	datetime.month := month;
	datetime.day := day;
	datetime.hour := hour;
	datetime.minute := minute;
	datetime.second := second;
кон GetDateTime;

кон SambaClient.

(* 
System.Free SambaClient ~

FSTools.Mount SMB SmbFS 192.168.1.1 sharename userid password~

FSTools.Mount SMB2 SmbFS 127.0.0.1 SBMShared id pwd~

FSTools.Mount SMB SmbFS 192.168.1.1  ~

FSTools.Mount SMB SmbFS 192.168.1.99 test ~

FSTools.Mount SMB SmbFS 192.168.1.99 d ~


FSTools.Mount SMB SmbFS 129.132.50.25 test guest guest ~

FSTools.Mount SMB SmbFS 129.132.50.7 test ~

FSTools.Mount SMB SmbFS 192.168.1.102 test ~

FSTools.Mount SMB SmbFS 127.0.0.1 ~

FSTools.Mount SMB SmbFS 127.0.0.1 ~

FSTools.Unmount SMB ~
*)
