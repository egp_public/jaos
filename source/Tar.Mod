модуль Tar; (** AUTHOR "ejz/FN"; PURPOSE "Aos tar program"; *)

использует
	Commands, Потоки, Files, ЛогЯдра, Strings, Archives, Locks;

конст
	RecordSize = 512;
	NamSiz = 100; TuNmLen = 32; TgNmLen = 32;
	EntryNameSize = 128;

	SegmentSize = 1024*8;

	StreamClosed* = 10; (** error *)

тип
	Header = укль на запись
		name: массив NamSiz из симв8;
		mode: массив 8 из симв8;
		uid: массив 8 из симв8;
		gid: массив 8 из симв8;
		size: массив 12 из симв8;
		mtime: массив 12 из симв8;
		chksum: массив 8 из симв8;
		linkflag: массив 1 из симв8;
		linkname: массив NamSiz из симв8;
		magic: массив 8 из симв8;
		uname: массив TuNmLen из симв8;
		gname: массив TgNmLen из симв8;
		devmajor: массив 8 из симв8;
		devminor: массив 8 из симв8;
	кон;

	(** contains info about an archive entry *)
	EntryInfo*= окласс(Archives.EntryInfo)
	перем
		name : массив EntryNameSize из симв8;
		size : цел32;

		проц & Init*(конст name : массив из симв8; size : цел32);
		нач
			копируйСтрокуДо0(name, сам.name); сам.size := size
		кон Init;

		проц {перекрыта}GetName*() : Strings.String;
		перем n : Strings.String;
		нач
			нов(n, EntryNameSize); копируйСтрокуДо0(name, n^);
			возврат n
		кон GetName;

		проц {перекрыта}GetSize*() : цел32;
		нач
			возврат size
		кон GetSize;

		проц {перекрыта}GetInfoString*() : Strings.String;
		перем s : Strings.String;
			temp : массив 10 из симв8;
		нач
			нов(s, 128);
			Strings.Append(s^, "Name : ");
			Strings.Append(s^, name);
			Strings.Append(s^, "; Size : ");
			Strings.IntToStr(size, temp);
			Strings.Append(s^, temp);
			Strings.Append(s^, ";");
			возврат s
		кон GetInfoString;

	кон EntryInfo;

	(* for internal use only. represent an archive entry *)
	Entry = окласс
	перем
		next : Entry;
		pos : Files.Position; (* pointer to beginning of entry header in tar file *)
		header : Header;

		проц & Init*;
		нач
			нов(header)
		кон Init;

		проц SetName(конст name : массив из симв8);
		перем i : размерМЗ;
		нач
			утв(длинаМассива(name) <= NamSiz);
			нцДля i := 0 до длинаМассива(name)-1 делай header.name[i] := name[i] кц
		кон SetName;

		проц SetSize(size : цел32);
		нач
			IntToOctStr(size, сам.header.size)
		кон SetSize;

		проц GetSize() : цел32;
		перем i : цел32;
		нач
			OctStrToInt(header.size, i); возврат i
		кон GetSize;

		проц CalculateCheckSum;
		нач
			CalcCheckSum(header)
		кон CalculateCheckSum;

	кон Entry;

	(* for internal use only. lets read a specified amount of data *)
	SizeReader = окласс
	перем input : Потоки.Чтец;
		max : размерМЗ;
		archive : Archive;

		проц &Init*(input: Потоки.Чтец; size: цел32; archive : Archive);
		нач
			сам.input := input; сам.max := size; сам.archive := archive
		кон Init;

		проц Receive(перем buf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);
		нач
			если max = 0 то
				res := -1;
				возврат
			всё;
			если size < min то size := min всё;
			если size > max то size := max всё;

			input.чБайты(buf, ofs, size, len);
			умень(max, len);
			res := input.кодВозвратаПоследнейОперации
		кон Receive;

	кон SizeReader;

	(* for internal use only. abstract buffer class *)
	Buffer = окласс

		проц Send(конст data : массив из симв8; ofs, len : размерМЗ; propagate : булево; перем res : целМЗ);
		нач СТОП(301)
		кон Send;

	кон Buffer;

	(* used by MemoryBuffer *)
	BufferSegment = окласс
	перем buf : массив SegmentSize из симв8;
		next : BufferSegment;
	кон BufferSegment;

	(* infinite memory-data-buffer. Buffers any data sent to 'Send' until propagate is TRUE, then all data is written to 'archive' *)
	MemoryBuffer = окласс(Buffer)
	перем
		first, current : BufferSegment;
		segmentCount, currentIndex : цел32;
		archive : Archive;
		name : массив NamSiz из симв8;
		closed : булево;

		(* parameters :  a: Archive; name: archive entry that will be written to *)
		проц & Init*(a : Archive; конст name : массив из симв8);
		нач
			archive := a;
			CopyArchiveName(name, сам.name);
			нов(first);
			current := first;
			segmentCount := 1;
			currentIndex := 0;
			closed := ложь
		кон Init;

		(* buffer any data until propagate is TRUE *)
		проц {перекрыта}Send(конст data : массив из симв8; ofs, len : размерМЗ; propagate : булево; перем res : целМЗ);
		перем i : размерМЗ;
		нач
			если closed то res := StreamClosed; возврат всё;
			res := Потоки.Успех;
			нцДля i := 0 до len-1 делай
				если currentIndex = SegmentSize то NewSegment() всё;
				current.buf[currentIndex] := data[ofs+i];
				увел(currentIndex)
			кц;
			если propagate то WriteBuffer(); closed := истина всё
		кон Send;

		(* extend buffer *)
		проц NewSegment;
		перем b : BufferSegment;
		нач
			нов(b);
			current.next := b;
			current := b;
			увел(segmentCount);
			currentIndex := 0
		кон NewSegment;

		(* lock archive for exclusive access and append header::buffer at the end *)
		проц WriteBuffer;
		перем w : Files.Writer;
			size : цел32;
			e : Entry;
			c : BufferSegment;
		нач
			archive.Acquire;
			size := (segmentCount-1)*SegmentSize + currentIndex;
			archive.RemoveEntry(name);
			нов(e);
			e.SetName(name);
			e.SetSize(size);
			e.pos := archive.file.Length();
			e.CalculateCheckSum();
			archive.AddEntryNode(e);
			Files.OpenWriter(w, archive.file, e.pos);
			(* write header *)
			WriteHeader(w, e.header);
			(* write data *)
			c := first;
			нцПока c # current делай
				w.пБайты(c.buf, 0, SegmentSize);
				c := c.next
			кц;
			w.пБайты(c.buf, 0, currentIndex);
			(* padding *)
			size := (-size) остОтДеленияНа RecordSize;
			нцПока size > 0 делай w.пСимв8(0X); умень(size) кц;
			w.ПротолкниБуферВПоток;
			archive.Release
		кон WriteBuffer;

	кон MemoryBuffer;

	(** tar archive; store a number of files in one archive *)
	Archive* = окласс(Archives.Archive)
	перем index : Entry;
		file : Files.File;
		lock : Locks.RecursiveLock;

		проц & Init*(f : Files.File);
		нач
			f.GetName(name);
			file := f;
			BuildIndex();
			нов(lock)
		кон Init;

		проц {перекрыта}Acquire*;
		нач
			lock.Acquire
		кон Acquire;

		проц {перекрыта}Release*;
		нач
			lock.Release
		кон Release;

		(** return list of archive entries *)
		проц {перекрыта}GetIndex*() : Archives.Index;
		перем i : цел32;
			e : Entry;
			result : Archives.Index;
			ei : EntryInfo;
		нач
			утв(lock.HasLock());
			i := 0;
			e := index;
			нцПока e # НУЛЬ делай увел(i); e := e.next кц;
			нов(result, i);
			i := 0;
			e := index;
			нцПока e # НУЛЬ делай
				нов(ei, e.header.name, e.GetSize());
				result[i] := ei;
				e := e.next;
				увел(i)
			кц;
			возврат result
		кон GetIndex;

		(** get info for a specific entry. return NIL if no such entry exists *)
		проц {перекрыта}GetEntryInfo*(конст name : массив из симв8) : Archives.EntryInfo;
		перем e : Entry;
			ei : EntryInfo;
		нач
			e := FindEntry(name);
			если e = НУЛЬ то возврат НУЛЬ всё;
			нов(ei, e.header.name, e.GetSize());
			возврат ei
		кон GetEntryInfo;

		(** remove named entry *)
		проц {перекрыта}RemoveEntry*(конст name : массив из симв8);
		перем newFile : Files.File;
			in : Files.Reader;
			out : Files.Writer;
			hdr : Header;
			pos, size: цел32;
		нач
			утв(lock.HasLock());
			newFile := Files.New(сам.name);
			Files.Register(newFile);
			Files.OpenWriter(out, newFile, 0);
			нов(hdr);
			pos := 0; Files.OpenReader(in, file, 0);
			нцПока (in.кодВозвратаПоследнейОперации = Потоки.Успех) и ReadHeader(in, hdr) делай
				OctStrToInt(hdr.size, size);
				size := size + ((-size) остОтДеленияНа RecordSize); (* entry + padding *)
				если hdr.name # name то
					WriteHeader(out, hdr);
					Files.OpenReader(in, file, pos + RecordSize);
					TransferBytes(in, out, size)
				всё;
				pos := pos + RecordSize + size;
				Files.OpenReader(in, file, pos);
				нов(hdr)
			кц;
			out.ПротолкниБуферВПоток;
			file := newFile;
			BuildIndex()
		кон RemoveEntry;

		(** rename an archive entry. return new EntryInfo or NIL if failed. *)
		проц {перекрыта}RenameEntry*(конст from, to : массив из симв8) : Archives.EntryInfo;
		перем e : Entry;
			w : Files.Writer;
			ei : EntryInfo;
		нач
			утв(lock.HasLock());
			e := FindEntry(from);
			если e = НУЛЬ то возврат НУЛЬ всё;
			копируйСтрокуДо0(to, e.header.name);
			CalcCheckSum(e.header);
			Files.OpenWriter(w, file, e.pos);
			WriteHeader(w, e.header);
			w.ПротолкниБуферВПоток();
			нов(ei, to, e.GetSize());
			возврат ei
		кон RenameEntry;

		(** open a sender to write an entry with name to archive. the data will be written when Update is called *)
		проц {перекрыта}OpenSender*(конст name : массив из симв8) : Потоки.Делегат˛реализующийЗаписьВПоток;
		перем buffer : MemoryBuffer;
		нач
			утв(lock.HasLock());
			утв(name  # "");
			нов(buffer, сам, name);
			возврат buffer.Send
		кон OpenSender;

		(** read entry from archive *)
		проц {перекрыта}OpenReceiver*(конст name : массив из симв8) : Потоки.Делегат˛реализующийЧтениеИзПотока;
		перем r : Files.Reader;
			s : SizeReader;
			size : цел32;
			entry : Entry;
		нач
			утв(lock.HasLock());
			entry := FindEntry(name);
			если entry = НУЛЬ то возврат НУЛЬ всё;
			Files.OpenReader(r, file, entry.pos+RecordSize);
			OctStrToInt(entry.header.size, size);
			нов(s, r, size, сам);
			возврат s.Receive
		кон OpenReceiver;

		(** save a clone of the archive under a different name *)
		проц {перекрыта}Copy*(конст name : массив из симв8) : Archives.Archive;
		перем copy : Archive;
			new : Files.File;
		нач
			утв(lock.HasLock());
			new := Files.New(name);
			CopyFiles(file, new);
			Files.Register(new);
			нов(copy, new);
			возврат copy
		кон Copy;

		(* ----- internal functions ------------------------------------------------*)

		(* build internal index structure  *)
		проц BuildIndex;
		перем in : Files.Reader;
			hdr : Header;
			pos, size : цел32;
			e : Entry;
		нач
			index := НУЛЬ;
			нов(hdr);
			pos := 0; Files.OpenReader(in, file, 0);
			нцПока (in.кодВозвратаПоследнейОперации = Потоки.Успех) и ReadHeader(in, hdr) делай
				нов(e); e.header := hdr;
				AddEntryNode(e);
				OctStrToInt(hdr.size, size);
				e.pos := pos;
				pos := pos + RecordSize + size + ((-size) остОтДеленияНа RecordSize);
				если in.можноЛиПерейтиКМестуВПотоке¿() то
					in.ПерейдиКМестуВПотоке(pos)
				иначе
					Files.OpenReader(in, file, pos);
				всё;
				нов(hdr)
			кц;
			если (in.кодВозвратаПоследнейОперации = Потоки.Успех) и (hdr.chksum # "") то
				ЛогЯдра.пСтроку8(hdr.name); ЛогЯдра.пСтроку8("  checksum error"); ЛогЯдра.пВК_ПС
			всё
		кон BuildIndex;

		(* return Entry with name, return NIL if not found *)
		проц FindEntry(конст name : массив из симв8) : Entry;
		перем e : Entry;
		нач
			e := index;
			нцПока e # НУЛЬ делай
				если e.header.name = name то возврат e всё;
				e := e.next
			кц;
			возврат НУЛЬ
		кон FindEntry;

		(* for internal use only. add an entry to the archive *)
		проц AddEntryNode(e : Entry);
		нач
			e.next := index; index := e
		кон AddEntryNode;

	кон Archive;

	(* ----- helpers ---------------------------------------------------------------------- *)

	проц ReadHeaderBytes(R: Потоки.Чтец; перем buf: массив из симв8; len: цел32; перем chksum: цел32);
	перем i: цел32; ch: симв8;
	нач
		i := 0;
		нцПока i < len делай
			R.чСимв8(ch); buf[i] := ch;
			увел(chksum, кодСимв8(ch)); увел(i)
		кц
	кон ReadHeaderBytes;

	проц ReadHeader(R: Потоки.Чтец; перем hdr: Header): булево;
	перем chksum, chksum2: цел32; len: размерМЗ;
	нач
		утв(hdr # НУЛЬ);
		chksum := 0;
		ReadHeaderBytes(R, hdr.name, NamSiz, chksum);
		ReadHeaderBytes(R, hdr.mode, 8, chksum);
		ReadHeaderBytes(R, hdr.uid, 8, chksum);
		ReadHeaderBytes(R, hdr.gid, 8, chksum);
		ReadHeaderBytes(R, hdr.size, 12, chksum);
		ReadHeaderBytes(R, hdr.mtime, 12, chksum);
		R.чБайты(hdr.chksum, 0, 8, len);
		ReadHeaderBytes(R, hdr.linkflag, 1, chksum);
		ReadHeaderBytes(R, hdr.linkname, NamSiz, chksum);
		ReadHeaderBytes(R, hdr.magic, 8, chksum);
		ReadHeaderBytes(R, hdr.uname, TuNmLen, chksum);
		ReadHeaderBytes(R, hdr.gname, TgNmLen, chksum);
		ReadHeaderBytes(R, hdr.devmajor, 8, chksum);
		ReadHeaderBytes(R, hdr.devminor, 8, chksum);
		увел(chksum, 8*32); OctStrToInt(hdr.chksum, chksum2);
		возврат chksum = chksum2
	кон ReadHeader;

	проц Empty(перем buf: массив из симв8; len: цел32);
	перем i: цел32;
	нач
		i := 0; нцПока i < len делай buf[i] := 0X; увел(i) кц
	кон Empty;

	проц EmptyHeader(перем hdr: Header);
	нач
		утв(hdr # НУЛЬ);
		Empty(hdr.name, NamSiz);
		Empty(hdr.mode, 8);
		Empty(hdr.uid, 8);
		Empty(hdr.gid, 8);
		Empty(hdr.size, 12);
		Empty(hdr.mtime, 12);
		Empty(hdr.chksum, 8);
		Empty(hdr.linkflag, 1);
		Empty(hdr.linkname, NamSiz);
		Empty(hdr.magic, 8);
		Empty(hdr.uname, TuNmLen);
		Empty(hdr.gname, TgNmLen);
		Empty(hdr.devmajor, 8);
		Empty(hdr.devminor, 8)
	кон EmptyHeader;

	проц CheckHeaderBytes(конст buf: массив из симв8; len: цел32; перем chksum: цел32);
	перем i: цел32;
	нач
		i := 0; нцПока i < len делай увел(chksum, кодСимв8(buf[i])); увел(i) кц
	кон CheckHeaderBytes;

	проц CalcCheckSum(перем hdr: Header);
	перем chksum: цел32;
	нач
		утв(hdr # НУЛЬ);
		CheckHeaderBytes(hdr.name, NamSiz, chksum);
		CheckHeaderBytes(hdr.mode, 8, chksum);
		CheckHeaderBytes(hdr.uid, 8, chksum);
		CheckHeaderBytes(hdr.gid, 8, chksum);
		CheckHeaderBytes(hdr.size, 12, chksum);
		CheckHeaderBytes(hdr.mtime, 12, chksum);
		CheckHeaderBytes(hdr.linkflag, 1, chksum);
		CheckHeaderBytes(hdr.linkname, NamSiz, chksum);
		CheckHeaderBytes(hdr.magic, 8, chksum);
		CheckHeaderBytes(hdr.uname, TuNmLen, chksum);
		CheckHeaderBytes(hdr.gname, TgNmLen, chksum);
		CheckHeaderBytes(hdr.devmajor, 8, chksum);
		CheckHeaderBytes(hdr.devminor, 8, chksum);
		увел(chksum, 8*32);
		IntToOctStr(chksum, hdr.chksum)
	кон CalcCheckSum;

	проц WriteHeader(W: Потоки.Писарь; перем hdr: Header);
	перем i: цел32;
	нач
		утв(hdr # НУЛЬ);
		W.пБайты(hdr.name, 0, NamSiz);
		W.пБайты(hdr.mode, 0, 8);
		W.пБайты(hdr.uid, 0, 8);
		W.пБайты(hdr.gid, 0, 8);
		W.пБайты(hdr.size, 0, 12);
		W.пБайты(hdr.mtime, 0, 12);
		W.пБайты(hdr.chksum, 0, 8);
		W.пБайты(hdr.linkflag, 0, 1);
		W.пБайты(hdr.linkname, 0, NamSiz);
		W.пБайты(hdr.magic, 0, 8);
		W.пБайты(hdr.uname, 0, TuNmLen);
		W.пБайты(hdr.gname, 0, TgNmLen);
		W.пБайты(hdr.devmajor, 0, 8);
		W.пБайты(hdr.devminor, 0, 8);
		i := 345;
		нцПока i < 512 делай
			W.пСимв8(0X); увел(i)
		кц
	кон WriteHeader;

	проц OctStrToInt(конст  str: массив из симв8; перем val: цел32);
		перем i, d: цел32; ch: симв8;
	нач
		i := 0; ch := str[0]; val := 0;
		нцПока (ch = " ") делай
			увел(i); ch := str[i];
		кц;
		нцПока (ch >= "0") и (ch <= "7")  делай
			d := кодСимв8(ch) - кодСимв8("0");
			увел(i); ch := str[i];
			если val <= ((матМаксимум(цел32)-d) DIV 8) то
				val := 8*val+d
			иначе
				СТОП(99)
			всё
		кц
	кон OctStrToInt;

	проц IntToOctStr(val: цел64; перем str: массив из симв8);
		перем i: размерМЗ;
	нач
		i := длинаМассива(str)-1; str[i] := 0X;
		нцПока i > 0 делай
			умень(i);
			str[i] := симв8ИзКода((val остОтДеленияНа 8) + кодСимв8("0"));
			val := val DIV 8
		кц
	кон IntToOctStr;

	проц CopyArchiveName(конст from : массив из симв8; перем to : массив из симв8);
	перем i : размерМЗ;
	нач
		если длинаМассива(from) < NamSiz то i := длинаМассива(from)-1 иначе i := NamSiz-1 всё;
		нцПока i > -1 делай to[i] := from[i]; умень(i) кц
	кон CopyArchiveName;

	проц Backup(f: Files.File);
	перем old, new: Files.FileName; res: целМЗ;
	нач
		f.GetName(old); копируйСтрокуДо0(old, new);
		Strings.Append(new, ".Bak");
		ЛогЯдра.пСтроку8("  "); ЛогЯдра.пСтроку8(new); ЛогЯдра.пВК_ПС();
		Files.Rename(old, new, res);
		утв(res = 0)
	кон Backup;

	проц CopyFiles(перем from, to : Files.File);
	перем in : Files.Reader;
		out : Files.Writer;
	нач
		Files.OpenReader(in, from, 0);
		Files.OpenWriter(out, to, 0);
		TransferBytes(in, out, from.Length());
		out.ПротолкниБуферВПоток
	кон CopyFiles;

	проц TransferBytes(from : Files.Reader; to : Files.Writer; n : Files.Size);
	перем buf : массив 1024 из симв8;
		len : размерМЗ;
	нач
		нцПока n > 1024 делай
			from.чБайты(buf, 0, 1024, len);
			to.пБайты(buf, 0, 1024);
			умень(n, 1024)
		кц;
		from.чБайты(buf, 0, размерМЗ(n), len);
		to.пБайты(buf, 0, размерМЗ(n));
		to.ПротолкниБуферВПоток()
	кон TransferBytes;

	(* ----- api --------------------------------------------------------------------------- *)

	(** open an existing archive. applications should use the method Old in the superclass *)
	проц Old*(name : Archives.StringObject) : Archives.Archive;
	перем archive : Archive; file : Files.File;
	нач
		file := Files.Old(name.value);
		если file = НУЛЬ то
			возврат НУЛЬ
		иначе
			нов(archive, file);
			возврат archive
		всё
	кон Old;

	(** create a new archive, overwrite existing. applications should use the method New in the superclass *)
	проц New*(name : Archives.StringObject) :Archives.Archive;
	перем archive : Archive; file : Files.File;
	нач
		file := Files.New(name.value);
		Files.Register(file);
		нов(archive, file);
		возврат archive
	кон New;

	(* ----- command line tools --------------------------------------------------------------- *)

	проц List*(context : Commands.Context);
	перем
		fn: Files.FileName; F: Files.File; R: Files.Reader;
		hdr: Header; pos, size: цел32;
	нач
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fn);
		F := Files.Old(fn);
		если F = НУЛЬ то context.out.пСтроку8(fn); context.out.пСтроку8(" : no such file found."); context.out.пВК_ПС; возврат всё;
		нов(hdr);
		pos := 0; Files.OpenReader(R, F, 0);
		нцПока (R.кодВозвратаПоследнейОперации = Потоки.Успех) и ReadHeader(R, hdr) делай
			context.out.пСтроку8(hdr.name); context.out.пСтроку8("  ");
			OctStrToInt(hdr.size, size);
			context.out.пЦел64(size, 0); context.out.пВК_ПС;
			pos := pos + RecordSize + size + ((-size) остОтДеленияНа RecordSize);
			Files.OpenReader(R, F, pos)
		кц;
		если (R.кодВозвратаПоследнейОперации = Потоки.Успех) и (hdr.chksum # "") то
			context.out.пСтроку8(hdr.name); context.out.пСтроку8("  checksum error"); context.out.пВК_ПС;
		всё;
	кон List;

	проц Extract*(context : Commands.Context);
	перем
		fn: Files.FileName; F, f: Files.File; R: Files.Reader; w: Files.Writer;
		hdr: Header; pos, size, i: цел32; ch: симв8;
	нач
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fn);
		F := Files.Old(fn);
		если F = НУЛЬ то context.out.пСтроку8(fn); context.out.пСтроку8(" : no such file found."); context.out.пВК_ПС; возврат всё;
		нов(hdr);
		pos := 0; Files.OpenReader(R, F, 0);
		нцПока (R.кодВозвратаПоследнейОперации = Потоки.Успех) и ReadHeader(R, hdr) делай
			context.out.пСтроку8(hdr.name); context.out.пСтроку8("  ");
			OctStrToInt(hdr.size, size);
			context.out.пЦел64(size, 0); context.out.пВК_ПС;
			f := Files.Old(hdr.name);
			если f # НУЛЬ то Backup(f) всё;
			f := Files.New(hdr.name); Files.OpenWriter(w, f, 0);
			Files.OpenReader(R, F, pos + RecordSize);
			i := 0;
			нцПока i < size делай
				R.чСимв8(ch); w.пСимв8(ch); увел(i)
			кц;
			w.ПротолкниБуферВПоток(); Files.Register(f);
			pos := pos + RecordSize + size + ((-size) остОтДеленияНа RecordSize);
			Files.OpenReader(R, F, pos)
		кц;
		если (R.кодВозвратаПоследнейОперации = Потоки.Успех) и (hdr.chksum # "") то
			context.out.пСтроку8(hdr.name); context.out.пСтроку8("  checksum error"); context.out.пВК_ПС()
		всё;
	кон Extract;

	проц Create*(context : Commands.Context);
	перем
		fn, archivename: Files.FileName; F, f: Files.File; W: Files.Writer; r: Files.Reader;
		hdr: Header; size, i: Files.Position; ch: симв8;
		nofAdded, nofErrors : цел32;
	нач
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(archivename);
		context.out.пСтроку8("Creating "); context.out.пСтроку8(archivename); context.out.пВК_ПС;
		F := Files.New(archivename); Files.OpenWriter(W, F, 0);
		nofAdded := 0; nofErrors := 0;
		нцПока context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fn) делай
			f := Files.Old(fn);
			если f # НУЛЬ то
				Files.OpenReader(r, f, 0); size := f.Length();
				нов(hdr); копируйСтрокуДо0(fn, hdr.name);
				IntToOctStr(size, hdr.size);
				CalcCheckSum(hdr);
				WriteHeader(W, hdr);
				i := 0;
				нцПока i < size делай
					r.чСимв8(ch); W.пСимв8(ch); увел(i)
				кц;
				size := (-size) остОтДеленияНа RecordSize;
				нцПока size > 0 делай
					W.пСимв8(0X); умень(size)
				кц;
				увел(nofAdded);
				context.out.пСтроку8(fn); context.out.пСтроку8(" added"); context.out.пВК_ПС;
			иначе
				увел(nofErrors);
				context.out.пСтроку8(fn); context.out.пСтроку8(" not found"); context.out.пВК_ПС;
			всё;
		кц;
		EmptyHeader(hdr); WriteHeader(W, hdr);
		W.ПротолкниБуферВПоток(); Files.Register(F);
		context.out.пСтроку8("Added "); context.out.пЦел64(nofAdded, 0); context.out.пСтроку8(" files to archive ");
		context.out.пСтроку8(archivename);
		если nofErrors > 0 то
			context.out.пСтроку8(" ("); context.out.пЦел64(nofErrors, 0); context.out.пСтроку8(" errors)");
		всё;
		context.out.пВК_ПС;
	кон Create;

кон Tar.

System.Free Tar ~
