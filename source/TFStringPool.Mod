модуль TFStringPool;

использует
	Трассировка, Strings;

конст
	BufSize = 1024;

тип
	Buffer = укль на массив BufSize из симв8;
	BufferList = укль на массив из Buffer;

	StringPool* = окласс
	перем bufList : BufferList;
		end* : размерМЗ;
		nofBufs : размерМЗ;


		проц &Init*;
		нач
			end := 0;
			нов(bufList, 1);
			nofBufs := длинаМассива(bufList);
		кон Init;

		проц GrowBufList;
		перем i : размерМЗ;
			t : BufferList;
		нач
			нов(t, nofBufs * 2);
			нцДля i := 0 до nofBufs - 1 делай t[i] := bufList[i] кц;
			bufList := t;
			nofBufs := длинаМассива(bufList);
		кон GrowBufList;

		проц AddString*(конст str : массив из симв8) : размерМЗ;
		перем i, result, bufNr, bufPos : размерМЗ;
		нач {единолично}
			result := end;
			i := 0;
			нцПока str[i] # 0X делай
				bufNr := end DIV BufSize; bufPos := end остОтДеленияНа BufSize;
				если bufNr >= nofBufs то GrowBufList всё;
				если bufList[bufNr] = НУЛЬ то нов(bufList[bufNr]) всё;
				bufList[bufNr][bufPos] := str[i];
				увел(end); увел(i);
			кц;
			bufNr := end DIV BufSize; 	bufPos := end остОтДеленияНа BufSize;
			если bufNr >= nofBufs то GrowBufList всё;
			если bufList[bufNr] = НУЛЬ то нов(bufList[bufNr]) всё;
			bufList[bufNr][bufPos] := 0X;
			увел(end);
			возврат result
		кон AddString;

		проц GetString*(i : размерМЗ; перем str : массив из симв8);
		перем ch : симв8; j : размерМЗ;
		нач {единолично}
			j := 0;
			нцДо
				ch := bufList[i DIV BufSize][i остОтДеленияНа BufSize];
				str[j] := ch;
				увел(i); увел(j)
			кцПри ch = 0X
		кон GetString;

		проц Equal*(a, b : размерМЗ) : булево;
		перем ca, cb : симв8;
		нач {единолично}
			нцДо
				ca := bufList[a DIV BufSize][a остОтДеленияНа BufSize];
				cb := bufList[b DIV BufSize][b остОтДеленияНа BufSize];
				увел(a); увел(b)
			кцПри (ca # cb) или (ca = 0X);
			возврат ca = cb
		кон Equal;

	кон StringPool;

перем
	s : StringPool;

проц Test*(par : динамическиТипизированныйУкль) : динамическиТипизированныйУкль;
перем x, y : массив 128 из симв8;
	i : размерМЗ;
	buf : укль на массив из размерМЗ;
нач
	нов(buf, 1000000);
	нцДля i := 0 до 1000000 - 1  делай
		Strings.IntToStr(i, x);
(*		Strings.Append(x, " - Huga"); *)
		buf[i] := s.AddString(x);
	кц;

	нцДля i := 0 до 1000000 - 1  делай
		Strings.IntToStr(i, x);
(*		Strings.Append(x, " - Huga"); *)
		s.GetString(buf[i], y);
		если x # y то Трассировка.пСтроку8("Failed"); Трассировка.пВК_ПС;  всё;
	кц;
	Трассировка.пСтроку8("done"); Трассировка.пВК_ПС;

	возврат НУЛЬ
кон Test;


нач
	нов(s);

кон TFStringPool.


TFStringPool.Test ~
System.Free TFStringPool ~
