модуль StreamUtilities; (** AUTHOR "Patrick Hunziker"; PURPOSE "stream utilities"; *)
(* daisychaining of readers or writers with 'logging side-stream', or with size limitation*)

использует Потоки, НИЗКОУР (*, KernelLog, Commands*);

конст
	ReaderBufSize = Потоки.РазмерПоУмолчаниюБуфераЧтеца;
	WriterBufSize = Потоки.РазмерПоУмолчаниюБуфераПисаря;

(* writer that can daisychained with another writer that extracts a copy of the data flow to a monitor stream*)
тип	WriterMonitor* = окласс (Потоки.Писарь);
		перем out, monitor : Потоки.Писарь;

		проц &Init*(out:Потоки.Писарь; monitor: Потоки.Писарь);
		нач
			новПисарь(Sender, WriterBufSize);
			сам.out := out;
			сам.monitor:=monitor;
			Сбрось;
		кон Init;

		проц Sender(конст outBuf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
		нач
			out.пБайты(outBuf, ofs, len);
			monitor.пБайты(outBuf, ofs, len);
			увел(отправленоБайт,len);
			если propagate то out.ПротолкниБуферВПоток; monitor.ПротолкниБуферВПоток всё;
			res:=out.кодВозвратаПоследнейОперации;
		кон Sender;

		проц {перекрыта}можноЛиПерейтиКМестуВПотоке¿*(): булево;
		нач возврат out.можноЛиПерейтиКМестуВПотоке¿()
		кон можноЛиПерейтиКМестуВПотоке¿;

		проц {перекрыта}ПерейдиКМестуВПотоке*(pos: Потоки.ТипМестоВПотоке);
		нач Сбрось; out.ПерейдиКМестуВПотоке(pos);
		кон ПерейдиКМестуВПотоке;

		проц {перекрыта}МестоВПотоке*(): Потоки.ТипМестоВПотоке;
		нач возврат out.МестоВПотоке()
		кон МестоВПотоке;

	кон WriterMonitor;

тип  Encryptor*=проц{делегат}( перем buf: массив из симв8;  pos, len: размерМЗ );

(* encrypting writer that can daisychained with another writer *)
тип	EncryptingWriter* = окласс (Потоки.Писарь);
		перем out: Потоки.Писарь; encrypt: Encryptor; buf: укль на массив из симв8;

		проц &Init*(out:Потоки.Писарь; encrypt:Encryptor);
		нач
			новПисарь(Sender, WriterBufSize);
			нов(buf, WriterBufSize);
			сам.out := out;
			сам.encrypt:=encrypt;
			Сбрось;
		кон Init;

		проц Sender(конст outBuf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
		перем i: размерМЗ;
		нач
			нцДля i:=0 до len-1 делай buf[i]:=outBuf[ofs+i] кц;
			если encrypt#НУЛЬ то encrypt(buf^,0,len); всё;
			out.пБайты(buf^, 0, len);
			увел(отправленоБайт,len);
			если propagate то out.ПротолкниБуферВПоток всё;
			res:=out.кодВозвратаПоследнейОперации;
		кон Sender;

		проц {перекрыта}МестоВПотоке*(): Потоки.ТипМестоВПотоке;
		нач возврат out.МестоВПотоке()
		кон МестоВПотоке;

	кон EncryptingWriter;

	тип  Decryptor*=проц{делегат}( перем buf: массив из симв8;  pos, len: размерМЗ );

	(* reader that can daisychained with another reader that reads and decrypts a stream*)
	DecryptingReader* = окласс(Потоки.Чтец)
		перем in: Потоки.Чтец;
			decrypt:Decryptor;

		проц &Init*(in: Потоки.Чтец; decrypt: Decryptor);
		нач
			новЧтец(Receiver, ReaderBufSize);
			сам.in := in;
			сам.decrypt:=decrypt;
		кон Init;

		проц Receiver(перем inBuf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);
		нач
			утв((size > 0) и (min <= size) и (min >= 0));
			in.чБайты(inBuf, ofs, size, len);
			если decrypt#НУЛЬ то decrypt(inBuf, ofs, len); всё;
			увел(прочитаноБайт,len);
			res:=in.кодВозвратаПоследнейОперации;
		кон Receiver;

		проц {перекрыта}МестоВПотоке*(): Потоки.ТипМестоВПотоке;
		нач возврат in.МестоВПотоке()
		кон МестоВПотоке;

	кон DecryptingReader;


тип

	WriteEntry = укль на запись buf: укль на массив из симв8; len: размерМЗ; propagate: булево; next: WriteEntry кон;

(* writer that writes asynchronously - updates are delayed until the thread is ready for it
	useful to avoid file writing delay problems.
	Caution: Pos() and SetPos() enforce synchronisation.
	*)

	AsynchronousWriter= окласс(Потоки.Писарь)
	перем
		first, last: WriteEntry;
		free: WriteEntry;
		size: размерМЗ;
		sender: Потоки.Делегат˛реализующийЗаписьВПоток;

		проц & {перекрыта}новПисарь(sender: Потоки.Делегат˛реализующийЗаписьВПоток; size: размерМЗ);
		нач
			first := НУЛЬ; last := НУЛЬ; free := НУЛЬ;
			сам.size := size;
			сам.sender := sender;
			новПисарь^(Add, size);
			Сбрось;
		кон новПисарь;

		проц Add(конст outBuf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
		перем entry: WriteEntry;
		нач
			если ~ToLastEntry(outBuf, ofs, len, propagate, res) то
				entry := GetFreeEntry(матМаксимум(сам.size, len));
				НИЗКОУР.копируйПамять(операцияАдресОт outBuf[ofs], операцияАдресОт entry.buf[0], len);
				entry.len := len;
				entry.propagate := propagate;
				PutEntry(entry)
			всё;
		кон Add;

		(* check last entry for enough space to host data. If available, remove from list and return *)
		проц ToLastEntry(конст outBuf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ): булево;
		нач{единолично}
			если last = НУЛЬ то возврат ложь
			аесли last.propagate # propagate то возврат ложь
			аесли (last.len + len > длинаМассива(last.buf^)) то возврат ложь
			иначе
				НИЗКОУР.копируйПамять(операцияАдресОт outBuf[ofs], операцияАдресОт last.buf[last.len], len);
				увел(last.len, len);
				res := 0;
				возврат истина
			всё;
		кон ToLastEntry;

		проц GetFreeEntry(len: размерМЗ): WriteEntry;
		перем entry: WriteEntry;
		нач{единолично}
			если free = НУЛЬ то нов(entry) иначе entry := free; free := free.next всё;
			если (entry.buf = НУЛЬ) или (длинаМассива(entry.buf)< len) то нов(entry.buf, len) всё;
			entry.len := 0; entry.propagate := ложь;
			возврат entry
		кон GetFreeEntry;

		проц ReturnEntry(entry: WriteEntry);
		нач{единолично}
			entry.next := free;
			free := entry
		кон ReturnEntry;

		проц PutEntry(entry: WriteEntry);
		нач{единолично}
			если last = НУЛЬ то first := entry; last := entry
			иначе last.next := entry; last := entry всё;
			entry.next := НУЛЬ;
		кон PutEntry;

		проц GetEntry(): WriteEntry;
		перем entry: WriteEntry;
		нач{единолично}
			дождись(first # НУЛЬ);
			entry := first;
			first := first.next;
			если first = НУЛЬ то last := НУЛЬ всё;
			возврат entry
		кон GetEntry;

		проц ProcessWrites;
		перем entry: WriteEntry;
		нач
			нц
				entry := GetEntry();

				sender(entry.buf^, 0, entry.len, entry.propagate, кодВозвратаПоследнейОперации);

				ReturnEntry(entry);
			кц;
		кон ProcessWrites;

	нач{активное}
		ProcessWrites;
	кон AsynchronousWriter;


	AsynchronousForwarder* = окласс (AsynchronousWriter);
		перем out: Потоки.Писарь;

		проц &Init*(out:Потоки.Писарь);
		нач
			сам.out := out;
			новПисарь(Sender, WriterBufSize);
		кон Init;

		проц {перекрыта}можноЛиПерейтиКМестуВПотоке¿*(): булево;
		нач возврат out.можноЛиПерейтиКМестуВПотоке¿()
		кон можноЛиПерейтиКМестуВПотоке¿;

		проц {перекрыта}ПерейдиКМестуВПотоке*(pos: Потоки.ТипМестоВПотоке);
		нач{единолично}
			дождись(first = НУЛЬ);
			Сбрось; out.ПерейдиКМестуВПотоке(pos);
		кон ПерейдиКМестуВПотоке;

		проц {перекрыта}МестоВПотоке*(): Потоки.ТипМестоВПотоке;
		нач{единолично}
			дождись(first = НУЛЬ);
			возврат out.МестоВПотоке()
		кон МестоВПотоке;

		проц Sender(конст outBuf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
		нач
			out.пБайты(outBuf, ofs, len);
			если propagate то out.ПротолкниБуферВПоток всё;
			увел(отправленоБайт,len);
			res:=out.кодВозвратаПоследнейОперации;
		кон Sender;

	кон AsynchronousForwarder;

	(* reader that can daisychained with another reader that extracts a copy of the data flow to a monitor stream*)
	ReaderMonitor* = окласс(Потоки.Чтец)
		перем in: Потоки.Чтец;
			monitor: Потоки.Писарь;

		проц &Init*(in: Потоки.Чтец; monitor: Потоки.Писарь);
		нач
			новЧтец(Receiver, ReaderBufSize);
			сам.in := in;
			сам.monitor:=monitor;
		кон Init;

		проц Receiver(перем buf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);
		нач
			утв((size > 0) и (min <= size) и (min >= 0));
			size := матМаксимум(min,матМинимум(in.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество(),size));
			in.чБайты(buf, ofs, size, len);
			увел(прочитаноБайт,len);
			res:=in.кодВозвратаПоследнейОперации;
			monitor.пБайты(buf, ofs, len);
			monitor.ПротолкниБуферВПоток;
		кон Receiver;

		проц {перекрыта}можноЛиПерейтиКМестуВПотоке¿*(): булево;
		нач возврат in.можноЛиПерейтиКМестуВПотоке¿()
		кон можноЛиПерейтиКМестуВПотоке¿;

		проц {перекрыта}ПерейдиКМестуВПотоке*(pos: Потоки.ТипМестоВПотоке);
		нач ОчистьСостояниеБуфера; in.ПерейдиКМестуВПотоке(pos)
		кон ПерейдиКМестуВПотоке;

		проц {перекрыта}МестоВПотоке*(): Потоки.ТипМестоВПотоке;
		нач возврат in.МестоВПотоке()
		кон МестоВПотоке;

	кон ReaderMonitor;

	LimitedWriter* = окласс (Потоки.Писарь);
		перем out : Потоки.Писарь;
			size, remain-: размерМЗ;

		проц &Init*(out:Потоки.Писарь; size: размерМЗ);
		нач
			новПисарь(Sender, матМинимум(size, WriterBufSize));
			сам.out := out;
			сам.size:=size; remain:=size;
		кон Init;

		проц Sender(конст outBuf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
		перем num:размерМЗ;
		нач
			num:=матМинимум(remain,len);
			out.пБайты(outBuf, ofs, num);
			умень(remain, num);
			если propagate то out.ПротолкниБуферВПоток всё;
			если num<len то res:=Потоки.КонецФайла иначе res:=out.кодВозвратаПоследнейОперации всё;
		кон Sender;

		проц {перекрыта}Сбрось*;
		нач
			remain:=size;
		кон Сбрось;

	кон LimitedWriter;

	LimitedReader* = окласс (Потоки.Чтец);
		перем in : Потоки.Чтец;
			total, remain-: размерМЗ;

		проц &Init*(in:Потоки.Чтец; size: размерМЗ);
		нач
			новЧтец(Receiver, матМинимум(size, ReaderBufSize));
			сам.in := in;
			total:=size; remain:=size;
		кон Init;

		проц Receiver(перем buf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);
		перем num, length: размерМЗ;
		нач
			утв(size >= 0);
			если (remain=0) то len:=0; res:=Потоки.КонецФайла; возврат всё;
			in.чБайты(buf, ofs, матМинимум(remain,size), length);
			len := length;
			умень(remain,len); увел(прочитаноБайт,len);
			res:=in.кодВозвратаПоследнейОперации;
		кон Receiver;

		проц {перекрыта}ОчистьСостояниеБуфера*;
		нач
			remain:=total;
		кон ОчистьСостояниеБуфера;
	кон LimitedReader;

	(* convert stream to Base64 encoding on-the-fly *)
тип	Base64Writer* = окласс (Потоки.Писарь);
		перем out : Потоки.Писарь;  buf: укль на массив из симв8;
			group, i, ll: размерМЗ; done:булево;

		проц &Init*(out:Потоки.Писарь);
		нач
			если ~tablesReady то InitTables всё;
			новПисарь(Sender, WriterBufSize);
			нов(buf, WriterBufSize);
			сам.out := out;
			Сбрось;
		кон Init;

		проц {перекрыта}Сбрось*;
		нач
			Сбрось^;
			group := 0;  i := 0;  ll := 0; done:=ложь;
		кон Сбрось;

		проц Sender(конст outBuf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
		перем ix: размерМЗ;
		нач
			если done то res:=Потоки.КонецФайла; возврат всё;
			ix := ofs; (* encoding snipped from CryptoBase64 *)
			нцПока ix < ofs+len делай
				group := group*100H + кодСимв8( outBuf[ix] );  увел( ix );  увел( i );
				если i = 3 то
					out.пСимв8( etab[group DIV 40000H остОтДеленияНа 64] );
					out.пСимв8( etab[group DIV 1000H остОтДеленияНа 64] );
					out.пСимв8( etab[group DIV 40H остОтДеленияНа 64] );
					out.пСимв8( etab[group остОтДеленияНа 64] );
					увел(отправленоБайт,4);
					увел( ll, 4 );
					если ll >= 72 то  out.пВК_ПС;  ll := 0  всё;
					group := 0;
					i := 0
				всё;
			кц;

			если propagate то out.ПротолкниБуферВПоток всё;
			res:=out.кодВозвратаПоследнейОперации;
		кон Sender;

		проц Close*; (*required termination of a Base64 sequence*)
		нач
			ПротолкниБуферВПоток;
			если i > 0 то   (* encode rest *)
				если i = 1 то  group := group*100H  всё;
				out.пСимв8( etab[group DIV 400H остОтДеленияНа 64] );
				out.пСимв8( etab[group DIV 10H остОтДеленияНа 64] );
				если i = 1 то  out.пСимв8( '=' )  иначе  out.пСимв8( etab[group*4 остОтДеленияНа 64] )  всё;
				out.пСимв8( '=' );
			всё;
			out.ПротолкниБуферВПоток;
			кодВозвратаПоследнейОперации:=out.кодВозвратаПоследнейОперации;
		кон Close;

		проц {перекрыта}МестоВПотоке*(): Потоки.ТипМестоВПотоке;
		нач возврат out.МестоВПотоке()
		кон МестоВПотоке;

	кон Base64Writer;

		(* decode Base64 stream to cleartext on-the-fly *)
	Base64Reader* = окласс(Потоки.Чтец)
		перем in: Потоки.Чтец;
			i, rest, group: размерМЗ;
			code: цел16;
			done:булево;

		проц &Init*(in: Потоки.Чтец);
		нач
			новЧтец(Receiver, ReaderBufSize);
			сам.in := in;
			in.ПропустиПробелыИТабуляции;
			если ~tablesReady то InitTables всё;
			ОчистьСостояниеБуфера;
		кон Init;

		проц {перекрыта}ОчистьСостояниеБуфера*;
		нач
			ОчистьСостояниеБуфера^;
			group := 0;  i := 0; code:=0; done:=ложь;
		кон ОчистьСостояниеБуфера;

		проц Receiver(перем inBuf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);
		перем c:симв8; size4: размерМЗ;
		нач
			size4:=size DIV 4 * 4; (* a multiple of 4*)
			утв((size > 0) и (min <= size4) и (min >= 0));
			len:=0;
			если done то res:=Потоки.КонецФайла; возврат
			иначе res:=Потоки.Успех
			всё;
			(* decoding snipped from CryptoBase64 *)
			нцДо  in.чСимв8( c )  кцПри (c > ' ') или (c = 0X) или (in.кодВозвратаПоследнейОперации#Потоки.Успех);
			если (in.кодВозвратаПоследнейОперации=Потоки.Успех) то
				code := dtab[кодСимв8( c )];
				нцПока (code >= 0) и (in.кодВозвратаПоследнейОперации=Потоки.Успех) и (len< size4) делай
					group := group*64 + code;  увел( i );
					если i = 4 то
						inBuf[ofs+len] := симв8ИзКода( group DIV 10000H остОтДеленияНа 100H );  увел( len );
						inBuf[ofs+len] := симв8ИзКода( group DIV 100H остОтДеленияНа 100H );  увел( len );
						inBuf[ofs+len] := симв8ИзКода( group остОтДеленияНа 100H );  увел( len );
						group := 0;  i := 0
					всё;
					нцДо  in.чСимв8( c )  кцПри (c > ' ') или (c = 0X) или (in.кодВозвратаПоследнейОперации#Потоки.Успех);
					code := dtab[кодСимв8( c )];
				кц;

				если c = '=' то   (* decode rest *)
					если i < 2 то  res:=Потоки.НеверныйФормат;
					иначе
						group := group*64;  rest := 2;  in.чСимв8( c );
						если in.кодВозвратаПоследнейОперации=Потоки.Успех то
						если c = '=' то  group := group*64;  rest := 1  всё;
							inBuf[ofs+len] := симв8ИзКода( group DIV 10000H );  увел( len );
							если rest = 2 то  inBuf[ofs+len] := симв8ИзКода( group DIV 100H остОтДеленияНа 100H );  увел( len )  всё;
							done:=истина;
						иначе res:=in.кодВозвратаПоследнейОперации
						всё;
					всё;
				всё;
			иначе
				res:=in.кодВозвратаПоследнейОперации;
			всё;
			увел(прочитаноБайт,len);
		кон Receiver;

		проц {перекрыта}МестоВПотоке*(): Потоки.ТипМестоВПотоке;
		нач возврат in.МестоВПотоке()
		кон МестоВПотоке;

	кон Base64Reader;

тип DumpWriter*= окласс(Потоки.Писарь);
		проц Send* ( конст buf: массив из симв8;  ofs, len: размерМЗ;  propagate: булево;  перем res: целМЗ );
		нач
			res:=Потоки.Успех;
		кон Send;
	кон DumpWriter;



	перем (*tables for Base64*)
		etab: массив 64 из симв8;
		dtab: массив 128 из цел16;
		tablesReady:булево;

	проц InitTables;
		перем i, max: цел16;
		нач
			max := кодСимв8("Z") - кодСимв8("A");
			нцДля i := 0 до max делай
				etab[i] := симв8ИзКода( i + кодСимв8("A") )
			кц;
			увел(max);
			нцДля i := max до max + кодСимв8("z") - кодСимв8("a") делай
				etab[i] := симв8ИзКода( i - max + кодСимв8("a") )
			кц;
			max := max + кодСимв8("z") - кодСимв8("a") + 1;
			нцДля i := max до max + кодСимв8("9") - кодСимв8("0") делай
				etab[i] := симв8ИзКода( i - max + кодСимв8("0") )
			кц;
			etab[62] := "+";
			etab[63] := "/";
			нцДля i := 0 до 127 делай  dtab[i] := -1  кц;
			нцДля i := 0 до 63 делай  dtab[кодСимв8( etab[i] )] := i  кц;
			tablesReady:=истина;
		кон InitTables;


	(*open a monitoring writer on the out stream*)
	проц OpenWriterMonitor*(перем w: Потоки.Писарь; out:Потоки.Писарь; monitor: Потоки.Писарь);
	перем wm: WriterMonitor;
	нач
		нов(wm, out, monitor); w:=wm;
	кон OpenWriterMonitor;


	проц OpenAsynchronousForwarder*(out: Потоки.Писарь): Потоки.Писарь;
	перем a: AsynchronousForwarder;
	нач
		нов(a, out); возврат a
	кон OpenAsynchronousForwarder;


	(*open a monitoring reader on the in stream*)
	проц OpenReaderMonitor*(перем r: Потоки.Чтец; in:Потоки.Чтец; monitor: Потоки.Писарь);
	перем rm: ReaderMonitor;
		нач
			нов(rm, in, monitor); r:=rm;
		кон OpenReaderMonitor;

	(*open a size limited writer r on the out stream*)
	проц OpenLimitedWriter*(перем w: Потоки.Писарь; out: Потоки.Писарь; size:размерМЗ);
	перем lw: LimitedWriter;
		нач
			нов(lw, out, size); w:=lw;
		кон OpenLimitedWriter;

	(*open a size limited reader r on the in stream*)
	проц OpenLimitedReader*(перем r: Потоки.Чтец; in: Потоки.Чтец; size:размерМЗ);
	перем lr: LimitedReader;
		нач
			нов(lr, in, size); r:=lr;
		кон OpenLimitedReader;
(*
(* application example: reader/writer monitors *)
PROCEDURE Test*(context:Commands.Context);
VAR w, log: Streams.Writer;
	r:Streams.Reader;
	s: ARRAY 64 OF CHAR;
	res:BOOLEAN;
BEGIN
	NEW(log, KernelLog.Send, WriterBufSize);

	OpenReaderMonitor(r, context.arg, log); (*monitor the context.arg reader and send monitored input to log *)
	res:=r.GetString(s);

	OpenWriterMonitor(w, context.out, log);(* monitor the context.out writer and send monitored data to log*)
	w.String("holla"); w.Ln;
	w.Update;
END Test;

(* application example: size limited streams *)
PROCEDURE Test2*(context:Commands.Context);
VAR w, log: Streams.Writer;
	r:Streams.Reader;
	s: ARRAY 64 OF CHAR;
	res:BOOLEAN;
BEGIN
	NEW(log, KernelLog.Send, WriterBufSize);

	OpenLimitedReader(r, context.arg, 7); (*monitor the context.arg reader and send monitored input to log *)
	res:=r.GetString(s);
	log.String(s); log.Ln;
	res:=r.GetString(s);
	log.String(s); log.Ln;
	log.Update;

	OpenLimitedWriter(w, log, 6);(* monitor the context.out writer and send monitored data to log*)
	w.String("123456789"); w.Ln; w.Update;
END Test2;
*)
(*
PROCEDURE TestAsync*;
VAR log: Streams.Writer; i: SIZE;
BEGIN
	NEW(log, KernelLog.Send,128);
	log := OpenAsynchronousForwarder(log);
	FOR i := 0 TO 200 DO
		log.String(" Hallo from asynch "); log.Ln; log.Update;
	END;
	KernelLog.String(" D O N E "); KernelLog.Ln;
END TestAsync;


PROCEDURE TestAsync2*;
VAR log: AsynchronousWriter; i: SIZE;
BEGIN
	NEW(log, KernelLog.Send,128);
	FOR i := 0 TO 200 DO
		log.String(" Hallo from asynch2 "); log.Ln; log.Update;
	END;
	KernelLog.String(" D O N E "); KernelLog.Ln;
END TestAsync2;

PROCEDURE TestEncode(VAR buf: ARRAY OF CHAR; pos,len:SIZE);
VAR i:SIZE;
BEGIN
	FOR i:=pos TO pos+len-1 DO
		IF (buf[i]>="!") & (buf[i]<"~") THEN buf[i]:=CHR(ORD(buf[i])+1) END;
	END;
END TestEncode;

PROCEDURE TestDecode(VAR buf: ARRAY OF CHAR; pos,len:SIZE);
VAR i:SIZE;
BEGIN
	FOR i:=pos TO pos+len-1 DO
		IF (buf[i]>"!") & (buf[i]<="~") THEN buf[i]:=CHR(ORD(buf[i])-1) END;
	END;
END TestDecode;

PROCEDURE TestCrypt*(context:Commands.Context);
VAR log: Streams.Writer; secretwriter: EncryptingWriter; secretreader:DecryptingReader;
	string:ARRAY 64 OF CHAR; res:BOOLEAN;
BEGIN
	NEW(log, KernelLog.Send,128);

	NEW(secretreader, context.arg, TestDecode);
	res:=secretreader.GetString(string);
	log.String("decoded secret: "); log.String(string); log.Ln; log.String("encoded secret: ");  log.Update;

	NEW(secretwriter, log, TestEncode);
	secretwriter.String(string); secretwriter.Update;
END TestCrypt;
*)
(*
PROCEDURE TestBase64*(context:Commands.Context);
VAR secretwriter: Base64Writer;
	secretreader:Base64Reader;
	string:ARRAY 64 OF CHAR;
BEGIN
	NEW(secretwriter, context.out);
	secretwriter.String("admin:1234"); (* expect "YWRtaW46MTIzNA==" *)
	secretwriter.Close;
	context.out.Ln; context.out.Update;

	NEW(secretreader, context.arg);
	secretreader.String(string);
	context.out.String(string); (* given the argument "YWRtaW46MTIzNA==", expect "admin:1234" *)
	context.out.Ln; context.out.Update;
END TestBase64;
*)
кон StreamUtilities.

System.FreeDownTo StreamUtilities ~

StreamUtilities.Test hello ~
StreamUtilities.Test2 abcd efghijk ~
StreamUtilities.TestAsync abcd efghijk ~
StreamUtilities.TestCrypt IfmmpXpsme ~
StreamUtilities.TestBase64 YWRtaW46MTIzNA== ~

StreamUtilities.TestAsync

StreamUtilities.TestAsync2

