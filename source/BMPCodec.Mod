модуль BMPCodec; (** AUTHOR "eos,afi,tf"; PURPOSE "BMP Codec"; *)

(* some of the load and store routines look suspicious but i dont have the spec right now *)
(* 2008-02-15 : pboenhof : fixing SkipBytes values in BMPDecoder.Open (in.received had been used instead of in.Pos()) *)
использует
	Codecs, ЛогЯдра, Потоки, WMGraphics, Raster, Strings;


конст
	FileHeaderSize = 14;
	RGB = 0; RLE8 = 1; RLE4 = 2; BITFIELDS = 3;	(* compression *)


тип
	BMPDecoder = окласс(Codecs.ImageDecoder)
	перем errors : булево;
		in : Потоки.Чтец;
		size, reserved, offset, width, height, compression, colors, importantColors, col, redMask, greenMask, blueMask: цел32;
		planes, bpp: цел16; pal: Raster.Palette;
		img : Raster.Image;
		decoded : булево;

		проц Error(x : массив из симв8);
		нач
			ЛогЯдра.пСтроку8("BMP Decoder Error: ");
			ЛогЯдра.пСтроку8(x); ЛогЯдра.пВК_ПС;
			errors := истина
		кон Error;

		проц Log(x : массив из симв8);
		нач
			ЛогЯдра.пСтроку8("BMP Decoder Info: ");
			ЛогЯдра.пСтроку8(x); ЛогЯдра.пВК_ПС;
		кон Log;

		(* open the decoder on a file *)
		проц {перекрыта}Open*(in : Потоки.Чтец; перем res : целМЗ);
		перем
			pix: Raster.Pixel; ch: симв8;
		нач
			errors := ложь;
			decoded := ложь;
			res := -1;
			если in = НУЛЬ то Error("Input Stream is NIL"); возврат всё;
			сам.in := in;

			(* bitmap file header *)
			если (in.чИДайСимв8() # "B") или (in.чИДайСимв8() # "M") то Error("Not a BMP stream"); возврат всё;

			in.чЦел32_мз(size); in.чЦел32_мз(reserved); in.чЦел32_мз(offset);

			(* bitmap info header *)
			in.чЦел32_мз(size);	(* size of bitmap info header *)
			если size > 40 то Log("ignoring extra header fields") всё;
			in.чЦел32_мз(width); in.чЦел32_мз(height);
			in.чЦел16_мз(planes);
			если planes # 1 то Error("Can not handle multi-plane files"); возврат  всё;
			in.чЦел16_мз(bpp);
			если ~((bpp = 1) или (bpp = 4) или (bpp = 8) или (bpp = 16) или (bpp = 24) или (bpp = 32)) то
				 Log("Can not handle this bpp."); ЛогЯдра.пСтроку8("bpp = "); ЛогЯдра.пЦел64(bpp, 0); ЛогЯдра.пВК_ПС; возврат
			всё;
			in.чЦел32_мз(compression);
			если ~(compression в {RGB, RLE8, RLE4, BITFIELDS}) то
				Log("can't deal with compression type "); ЛогЯдра.пСтроку8("compression = "); ЛогЯдра.пЦел64(compression, 0); возврат
			всё;
			in.ПропустиБайты(12); in.чЦел32_мз(colors);
			если (colors = 0) и (bpp < 16) то colors := арифмСдвиг(1, bpp) всё;
			in.чЦел32_мз(importantColors);

			(* 54 bytes consumed by "in" up to here *)

			(* color table *)
			если bpp < 16 то
				in.ПропустиБайты(FileHeaderSize + size - in.МестоВПотоке());
				нов(pal); col := 0; pix[Raster.a] := 0FFX;
				нцПока col < colors делай
					in.чСимв8(pix[Raster.b]); in.чСимв8(pix[Raster.g]); in.чСимв8(pix[Raster.r]); in.чСимв8(ch);
					pal.col[col] := pix;
					увел(col)
				кц;
				Raster.InitPalette(pal, устарПреобразуйКБолееУзкомуЦел(colors), 2 + bpp DIV 4)
			аесли ((bpp = 16) или (bpp = 32)) и (compression = BITFIELDS) то
				in.чЦел32_мз(redMask); in.чЦел32_мз(greenMask); in.чЦел32_мз(blueMask)
			всё;
			нов(img);
			(* bitmap data *)
			in.ПропустиБайты(offset - in.МестоВПотоке());
			res := 0;
		кон Open;

		проц {перекрыта}GetImageInfo*(перем width, height : размерМЗ; перем format, maxProgressionLevel : цел32);
		нач
			width := сам.width;
			height := сам.height;
		кон GetImageInfo;

		(** Render will read and decode the image data up to progrssionLevel.
			If the progressionLevel is lower than a previously rendered progressionLevel,
			the new level can be ignored by the decoder. If no progressionLevel is set with
			SetProgressionLevel, the level is assumed to be maxProgressionLevel of the image,
			which corresponds to best image quality.
		 *)
		проц {перекрыта}SetProgressionLevel*(progressionLevel: цел32);
		кон SetProgressionLevel;

		(* return the image in Raster format that best matches the format *)
		проц {перекрыта}GetNativeImage*(перем img : Raster.Image);
		нач
			если ~decoded и ~errors то
				img := сам.img;
				просей bpp из
				| 1: Load1(img, width, height, pal, in)
				| 4: Load4(img, width, height, compression, pal, in)
				| 8: Load8(img, width, height, compression, pal, in)
				| 16: Load16(img, width, height, compression, redMask, greenMask, blueMask, in)
				| 24: Load24(img, width, height, in)
				| 32: Load32(img, width, height, compression, redMask, greenMask, blueMask, in)
				всё;
				decoded := истина
			всё
		кон GetNativeImage;

		(* renders the image into the given Raster.Image at the given progressionLevel *)
		проц {перекрыта}Render*(img : Raster.Image);
		перем canvas : WMGraphics.BufferCanvas;
		нач
			GetNativeImage(сам.img);
			если ~errors то
				нов(canvas, img);
				canvas.DrawImage(0, 0, сам.img, WMGraphics.ModeCopy);
			всё
		кон Render;
	кон BMPDecoder;

	BMPEncoder* = окласс(Codecs.ImageEncoder)
	перем out : Потоки.Писарь;

		(* open the encoder on a Streams writer *)
		проц {перекрыта}Open*(out : Потоки.Писарь);
		нач
			сам.out := out
		кон Open;

		проц {перекрыта}SetQuality*(quality : цел32);
		кон SetQuality;

		проц {перекрыта}WriteImage*(img : Raster.Image; перем res : целМЗ);
		перем
			col, redMask, greenMask, blueMask: цел32;
			palentries, dataSize: цел32; bpp: цел16;
			sm: Strings.Buffer;
			buf : Потоки.Писарь;
			data : Strings.String;
		нач
			если img.fmt.pal # НУЛЬ то palentries := img.fmt.pal.used иначе palentries := 0 всё;

			нов(sm, img.width * img.height * 4); (* conservative *)
			buf := sm.GetWriter();
			(* Bitmap data *)
			если img.fmt.code = Raster.bgra8888 то Store32(img, img.width, -img.height, redMask, greenMask, blueMask, buf)
			аесли img.fmt.code = Raster.bgr888 то Store24(img, img.width, -img.height, buf)
			аесли img.fmt.code = Raster.bgr466 то Store16(img, img.width, -img.height, redMask, greenMask, blueMask, buf)
			аесли img.fmt.code = Raster.bgr555 то Store16(img, img.width, -img.height, redMask, greenMask, blueMask, buf)
			аесли img.fmt.code = Raster.bgr565 то Store16(img, img.width, -img.height, redMask, greenMask, blueMask, buf)
			аесли img.fmt.pal.used = 256 то Store8(img, img.width, -img.height, buf)
			аесли img.fmt.pal.used = 16 то Store4(img, img.width, -img.height, buf)
			аесли img.fmt.pal.used = 2 то Store1(img, img.width, -img.height, buf)
			всё;
			dataSize := sm.GetLength()(цел32);

			(* Bitmap file header *)
			out.пСтроку8("BM");
			если (img.fmt.code = Raster.bgra8888) или (img.fmt.code = Raster.bgr888) или (img.fmt.code = Raster.bgr466) или
				(img.fmt.code = Raster.bgr555) или (img.fmt.code = Raster.bgr565)
			то
				(* additional size of color masks *)
				out.пЦел32_мз(12 + 14 + 40 + palentries * 4 + dataSize); out.пЦел32_мз(0); out.пЦел32_мз(12 + 14 + 40 + palentries * 4);
			иначе
				out.пЦел32_мз(14 + 40 + palentries * 4 + dataSize); out.пЦел32_мз(0); out.пЦел32_мз(14 + 40 + palentries * 4);
			всё;

			(* Bitmap info header *)
			out.пЦел32_мз(40); (* header size *)
			out.пЦел32_мз(img.width(цел32));
			out.пЦел32_мз(img.height(цел32));
			out.пЦел16_мз(1);	(* biPlanes *)
			если img.fmt.pal # НУЛЬ то
				просей img.fmt.pal.used из
					  2: bpp := 1
					| 16: bpp := 4
					| 256: bpp := 8
				иначе
				всё
			аесли (img.fmt.code = Raster.bgr565) или (img.fmt.code = Raster.bgr555) или (img.fmt.code = Raster.bgr466) то
				bpp := 16
			аесли img.fmt.code = Raster.bgr888 то bpp := 24
			аесли img.fmt.code = Raster.bgra8888 то bpp := 32
			всё;
			out.пЦел16_мз(bpp);	(* biBitCount *)
			(* biCompression *)
			просей img.fmt.code из
				  Raster.bgr565: out.пЦел32_мз(BITFIELDS)	(* BITFIELDS compression *)
				| Raster.bgr555: out.пЦел32_мз(BITFIELDS)	(* BITFIELDS compression *)
				| Raster.bgr466: out.пЦел32_мз(BITFIELDS)	(* BITFIELDS compression *)
				| Raster.bgra8888: out.пЦел32_мз(BITFIELDS)	(* BITFIELDS compression *)
			иначе
				out.пЦел32_мз(0)
			всё;
			out.пЦел32_мз(dataSize);(* biSizeImage *)
			out.пЦел32_мз(0);(* biXPelsPerMeter *)
			out.пЦел32_мз(0);(* biYPelsPerMeter *)
			если img.fmt.pal # НУЛЬ то
				out.пЦел32_мз(img.fmt.pal.used);	(* biClrUsed *)
				out.пЦел32_мз(img.fmt.pal.used);	(* biClrImportant *)
				(* RGBQUAD color table *)
				col := 0;
				нцПока col < img.fmt.pal.used делай
					out.пСимв8(img.fmt.pal.col[col, Raster.b]);
					out.пСимв8(img.fmt.pal.col[col, Raster.g]);
					out.пСимв8(img.fmt.pal.col[col, Raster.r]);
					out.пСимв8(0X);
					увел(col)
				кц
			иначе
				out.пЦел32_мз(0);
				out.пЦел32_мз(0)
			всё;

			просей img.fmt.code из
				  Raster.bgr565: out.пЦел32_мз(0F800H); out.пЦел32_мз(07E0H); out.пЦел32_мз(01FH)
				| Raster.bgr555: out.пЦел32_мз(07C00H); out.пЦел32_мз(03E0H); out.пЦел32_мз(01FH)
				| Raster.bgr466: out.пЦел32_мз(0FC00H); out.пЦел32_мз(03F0H); out.пЦел32_мз(00FH)
				| Raster.bgr888: out.пЦел32_мз(0FF00H); out.пЦел32_мз(0FF0H); out.пЦел32_мз(0FFH)
				| Raster.bgra8888: out.пЦел32_мз(0FF0000H); out.пЦел32_мз(0FF00H); out.пЦел32_мз(0FFH)
			иначе
			всё;

			data := sm.GetString();
			out.пБайты(data^, 0, sm.GetLength());

			res := 0;
			out.ПротолкниБуферВПоток
		кон WriteImage;

	кон BMPEncoder;

проц Unmask (val, mask, nbits: цел32): цел32;
	перем res, m, bits: цел32;
нач
	res := val; m := mask; bits := 0;
	нцПока (m # 0) и ~нечётноеЛи¿(m) делай	(* shift down to first valid bit *)
		m := m DIV 2; res := res DIV 2
	кц;
	нцПока нечётноеЛи¿(m) делай	(* count valid bits *)
		m := m DIV 2; увел(bits)
	кц;
	res := res остОтДеленияНа арифмСдвиг(1, bits);	(* mask out everything else *)
	нцПока bits > nbits делай	(* reduce to requested number of bits *)
		res := res DIV 2; умень(bits)
	кц;
	нцПока bits < nbits делай	(* blow up to requested number of bits *)
		res := 2*res+1; увел(bits)
	кц;
	возврат res
кон Unmask;

проц Load1 (img: Raster.Image; w, h: цел32; перем pal: Raster.Palette; r : Потоки.Чтец);
	перем y, dy, x, b, i: цел32; p: размерМЗ; fmt: Raster.Format;
нач
	если h > 0 то y := 0; dy := 1
	иначе h := -h; y := h-1; dy := -1
	всё;
	Raster.InitPaletteFormat(fmt, pal);
	Raster.Create(img, w, h, fmt);
	нцПока h > 0 делай
		x := 0; p := (img.height - y - 1) * img.bpr;
		нцПока x < w делай
			b := кодСимв8(r.чИДайСимв8());
			нцДля i := -7 до 0 делай
				если x < w то
					img.mem[p] := симв8ИзКода(арифмСдвиг(b, i) остОтДеленияНа 2); увел(p)
				всё;
				увел(x)
			кц
		кц;
		нцПока x остОтДеленияНа 32 # 0 делай r.ПропустиБайты(1); увел(x, 8) кц;	(* align to double word boundary *)
		умень(h); увел(y, dy)
	кц
кон Load1;

проц Load4 (img: Raster.Image; w, h, compression: цел32; pal: Raster.Palette; r: Потоки.Чтец);
	перем y, dy, x, b, i: цел32; fmt: Raster.Format; ch: симв8; col: массив 2 из симв8; p: размерМЗ;
нач
	если h > 0 то y := 0; dy := 1
	иначе h := -h; y := h-1; dy := -1
	всё;
	Raster.InitPaletteFormat(fmt, pal);
	Raster.Create(img, w, h, fmt);
	нцПока h > 0 делай
		x := 0; p := (img.height - y - 1) * img.bpr;
		нцПока x < w делай
			b := кодСимв8(r.чИДайСимв8());
			если compression = RLE4 то
				r.чСимв8(ch);
				если b # 0 то	(* encoded mode *)
					i := 0; col[0] := симв8ИзКода(кодСимв8(ch) DIV 10H); col[1] := симв8ИзКода(кодСимв8(ch) остОтДеленияНа 10H);
					нцПока i < b делай
						img.mem[p] := col[i остОтДеленияНа 2]; увел(i); увел(p)
					кц;
					увел(x, b)
				аесли ch = 0X то	(* end of line *)
					умень(h); увел(y, dy); x := 0; p := y * img.bpr	(* align to double word boundary? *)
				аесли ch = 1X то	(* end of bitmap data *)
					возврат
				аесли ch = 2X то	(* delta record *)
					r.чСимв8(ch); увел(x, устарПреобразуйКБолееШирокомуЦел(кодСимв8(ch)));
					r.чСимв8(ch); увел(y, устарПреобразуйКБолееШирокомуЦел(кодСимв8(ch))); p := y * img.bpr + x
				иначе	(* absolute mode *)
					b := кодСимв8(ch);
					нцДля i := 1 до b делай
						если нечётноеЛи¿(i) то r.чСимв8(ch); img.mem[p] := симв8ИзКода(кодСимв8(ch) DIV 10H); увел(p)
						иначе img.mem[p] := симв8ИзКода(кодСимв8(ch) остОтДеленияНа 10H); увел(p)
						всё
					кц;
					увел(x, b);
					если нечётноеЛи¿((b+1) DIV 2) то r.чСимв8(ch) всё;	(* align run length to even number of bytes *)
				всё
			иначе	(* uncompressed *)
				img.mem[p] := симв8ИзКода(b DIV 10H); увел(p);
				если x+1 < w то
					img.mem[p] := симв8ИзКода(b остОтДеленияНа 10H); увел(p)
				всё;
				увел(x, 2)
			всё
		кц;
		если compression = RGB то	(* is this also needed for RLE4 compression? *)
			нцПока x остОтДеленияНа 8 # 0 делай r.ПропустиБайты(1);  увел(x, 2) кц	(* align to double word boundary *)
		всё;
		умень(h); увел(y, dy)
	кц
кон Load4;

проц Load8 (img: Raster.Image; w, h, compression: цел32; pal: Raster.Palette; r: Потоки.Чтец);
	перем y, dy, x, b, i: цел32; fmt: Raster.Format; ch: симв8; p: размерМЗ;
нач
	если h > 0 то y := 0; dy := 1
	иначе h := -h; y := h-1; dy := -1
	всё;
	Raster.InitPaletteFormat(fmt, pal);
	Raster.Create(img, устарПреобразуйКБолееУзкомуЦел(w), устарПреобразуйКБолееУзкомуЦел(h), fmt);
	нцПока h > 0 делай
		x := 0; p := (img.height - y - 1) * img.bpr;
		нцПока x < w делай
			r.чСимв8(ch);
			если compression = RLE8 то
				b := кодСимв8(ch); r.чСимв8(ch);
				если b # 0 то	(* encoded mode *)
					нцДля i := 1 до b делай
						img.mem[p] := ch; увел(p)
					кц;
					увел(x, b)
				аесли ch = 0X то	(* end of line *)
					умень(h); увел(y, dy); x := 0; p := y * img.bpr	(* align to double word boundary? *)
				аесли ch = 1X то	(* end of bitmap data *)
					возврат
				аесли ch = 2X то	(* delta record *)
					r.чСимв8(ch); увел(x, устарПреобразуйКБолееШирокомуЦел(кодСимв8(ch)));
					r.чСимв8(ch); увел(y, устарПреобразуйКБолееШирокомуЦел(кодСимв8(ch))); p := y * img.bpr + x
				иначе	(* absolute mode *)
					b := кодСимв8(ch);
					нцДля i := 1 до b делай
						r.чСимв8(img.mem[p]); увел(p)
					кц;
					увел(x, b);
					если нечётноеЛи¿(b) то r.чСимв8(ch) всё;	(* align run length to even number of bytes *)
				всё
			иначе	(* uncompressed *)
				img.mem[p] := ch; увел(p); увел(x)
			всё
		кц;
		если compression = RGB то	(* is this also needed for RLE8 compression? *)
			нцПока x остОтДеленияНа 4 # 0 делай r.ПропустиБайты(1); увел(x) кц	(* align to double word boundary *)
		всё;
		умень(h); увел(y, dy)
	кц
кон Load8;

проц Load16 (img: Raster.Image; w, h, compression, rMask, gMask, bMask: цел32; r: Потоки.Чтец);
	перем y, dy, x, val, red, green, blue: цел32; convert: булево; p: размерМЗ;
нач
	если h > 0 то y := 0; dy := 1
	иначе h := -h; y := h-1; dy := -1
	всё;
	convert := ложь;
	если (compression = RGB) или (compression = BITFIELDS) и (rMask = 7C00H) и (gMask = 3E0H) и (bMask = 1FH) то
		Raster.Create(img, устарПреобразуйКБолееУзкомуЦел(w), устарПреобразуйКБолееУзкомуЦел(h), Raster.BGR555)
	аесли (compression = BITFIELDS) и (rMask = 0F800H) и (gMask = 7E0H) и (bMask = 1FH) то
		Raster.Create(img, устарПреобразуйКБолееУзкомуЦел(w), устарПреобразуйКБолееУзкомуЦел(h), Raster.BGR565)
	аесли (compression = BITFIELDS) и (rMask = 0FC00H) и (gMask = 3F0H) и (bMask = 0FH) то
		Raster.Create(img, устарПреобразуйКБолееУзкомуЦел(w), устарПреобразуйКБолееУзкомуЦел(h), Raster.BGR466)
	иначе
		Raster.Create(img, устарПреобразуйКБолееУзкомуЦел(w), устарПреобразуйКБолееУзкомуЦел(h), Raster.BGR565);
		convert := истина
	всё;
	нцПока h > 0 делай
		x := 0; p := (img.height - y - 1) * img.bpr;
		нцПока x < w делай
			r.чСимв8(img.mem[p]); r.чСимв8(img.mem[p+1]);
			если convert то
				val := кодСимв8(img.mem[p]) + арифмСдвиг(кодСимв8(img.mem[p+1]), 8);
				red := Unmask(val, rMask, 5); green := Unmask(val, gMask, 6); blue := Unmask(val, bMask, 5);
				val := blue + арифмСдвиг(green, 5) + арифмСдвиг(red, 11);
				img.mem[p] := симв8ИзКода(val); img.mem[p+1] := симв8ИзКода(val DIV 100H);
			всё;
			увел(x); увел(p, 2)
		кц;
		если нечётноеЛи¿(w) то r.ПропустиБайты(2) всё;
		умень(h); увел(y, dy)
	кц
кон Load16;

проц Load24 (img: Raster.Image; w, h: цел32; r: Потоки.Чтец);
	перем y, dy, x: цел32; p: размерМЗ;
нач
	если h > 0 то y := 0; dy := 1
	иначе h := -h; y := h-1; dy := -1
	всё;
	Raster.Create(img, устарПреобразуйКБолееУзкомуЦел(w), устарПреобразуйКБолееУзкомуЦел(h), Raster.BGR888);
	нцПока h > 0 делай
		x := 0; p := (img.height - y - 1) * img.bpr;
		нцПока x < w делай
			r.чСимв8(img.mem[p]); r.чСимв8(img.mem[p+1]); r.чСимв8(img.mem[p+2]);
			увел(x); увел(p, 3)
		кц;
		r.ПропустиБайты(w остОтДеленияНа 4);
		умень(h); увел(y, dy)
	кц
кон Load24;

проц Load32 (img: Raster.Image; w, h, compression, rMask, gMask, bMask: цел32; r: Потоки.Чтец);
	перем y, dy, x, val, red, green, blue: цел32; convert: булево; ch: симв8; p: размерМЗ;
нач
	если h > 0 то y := 0; dy := 1
	иначе h := -h; y := h-1; dy := -1
	всё;
	Raster.Create(img, устарПреобразуйКБолееУзкомуЦел(w), устарПреобразуйКБолееУзкомуЦел(h), Raster.BGR888);
	convert := (compression = BITFIELDS) и ((rMask # 0FF0000H) или (gMask # 0FF00H) или (bMask # 0FFH));
	нцПока h > 0 делай
		x := 0; p := (img.height - y - 1)  * img.bpr;
		нцПока x < w делай
			r.чСимв8(img.mem[p]); r.чСимв8(img.mem[p+1]); r.чСимв8(img.mem[p+2]); r.чСимв8(ch);
			если convert то
				val := кодСимв8(img.mem[p]) + арифмСдвиг(кодСимв8(img.mem[p+1]), 8) + арифмСдвиг(кодСимв8(img.mem[p+2]), 16) + арифмСдвиг(кодСимв8(ch), 24);
				red := Unmask(val, rMask, 8); green := Unmask(val, gMask, 8); blue := Unmask(val, bMask, 8);
				img.mem[p] := симв8ИзКода(blue); img.mem[p+1] := симв8ИзКода(green); img.mem[p+2] := симв8ИзКода(red)
			всё;
			увел(x); увел(p, 3)
		кц;
		умень(h); увел(y, dy)
	кц
кон Load32;

проц Store1 (img: Raster.Image; w, h: размерМЗ; out : Потоки.Писарь);
	перем y, dy, x, b, i: размерМЗ; p: размерМЗ;
нач
	если h > 0 то y := 0; dy := 1
	иначе h := -h; y := h-1; dy := -1
	всё;
	нцПока h > 0 делай
		x := 0; p := y * img.bpr;
		нцПока x < w делай
			b := 0;
			нцДля i := -7 до 0 делай
				если x < w то b := арифмСдвиг(b, 1) + кодСимв8(img.mem[p]); увел(p) всё;
				увел(x)
			кц;
			out.пСимв8(симв8ИзКода(b))
		кц;
		нцПока x остОтДеленияНа 32 # 0 делай out.пСимв8(0X); увел(x, 8) кц;	(* Align to double word boundary *)
		умень(h); увел(y, dy)
	кц
кон Store1;

проц Store4 (img: Raster.Image; w, h: размерМЗ; out : Потоки.Писарь);
	перем y, dy, x, b: размерМЗ; p: размерМЗ;
нач
	если h > 0 то y := 0; dy := 1
	иначе h := -h; y := h-1; dy := -1
	всё;
	нцПока h > 0 делай
		x := 0; p := y * img.bpr;
		нцПока x < w делай
			b := 0;
			b := кодСимв8(img.mem[p]) остОтДеленияНа 10H; увел(p);
			если x+1 < w то
				b := арифмСдвиг(b, 4) + кодСимв8(img.mem[p]) остОтДеленияНа 10H; увел(p)
			всё;
			out.пСимв8(симв8ИзКода(b));
			увел(x, 2)
		кц;
		умень(h); увел(y, dy)
	кц
кон Store4;

проц Store8 (img: Raster.Image; w, h: размерМЗ; out : Потоки.Писарь);
	перем y, dy, x: размерМЗ; p: размерМЗ;
нач
	если h > 0 то y := 0; dy := 1
	иначе h := -h; y := h-1; dy := -1
	всё;
	нцПока h > 0 делай
		x := 0; p := y * img.bpr;
		нцПока x < w делай
			out.пСимв8(img.mem[p]);
			увел(p); увел(x)
		кц;
		умень(h); увел(y, dy)
	кц
кон Store8;

проц Store16 (img: Raster.Image; w, h: размерМЗ; rMask, gMask, bMask: цел32; out : Потоки.Писарь);
	перем y, dy, x: размерМЗ; p: размерМЗ;
нач
	если h > 0 то y := 0; dy := 1
	иначе h := -h; y := h-1; dy := -1
	всё;
	нцПока h > 0 делай
		x := 0; p := y * img.bpr;
		out.пБайты(img.mem^, p, w * 2);
		если нечётноеЛи¿(w) то out.пСимв8(0X); out.пСимв8(0X) всё;
		умень(h); увел(y, dy)
	кц
кон Store16;

проц Store24 (img: Raster.Image; w, h: размерМЗ; out : Потоки.Писарь);
	перем y, dy, x: размерМЗ; align: массив 3 из симв8; p: размерМЗ;
нач
	если h > 0 то y := 0; dy := 1
	иначе h := -h; y := h-1; dy := -1
	всё;
	нцПока h > 0 делай
		x := 0; p := y * img.bpr;
		нцПока x < w делай
			out.пСимв8(img.mem[p]); out.пСимв8(img.mem[p+1]); out.пСимв8(img.mem[p+2]);
			увел(x); увел(p, 3)
		кц;
		out.пБайты(align, 0, w остОтДеленияНа 4);
		умень(h); увел(y, dy)
	кц
кон Store24;

проц Store32 (img: Raster.Image; w, h: размерМЗ; rMask, gMask, bMask: цел32; out : Потоки.Писарь);
	перем y, dy, x: размерМЗ; p: размерМЗ;
нач
	если h > 0 то y := 0; dy := 1
	иначе h := -h; y := h-1; dy := -1
	всё;
	нцПока h > 0 делай
		x := 0; p := y * img.bpr;
		нцПока x < w делай
			out.пСимв8(img.mem[p]); out.пСимв8(img.mem[p+1]); out.пСимв8(img.mem[p+2]); out.пСимв8(img.mem[p+3]);
			увел(x); увел(p, 4)
		кц;
		умень(h); увел(y, dy)
	кц
кон Store32;

проц DecoderFactory*() : Codecs.ImageDecoder;
перем p : BMPDecoder;
нач
	нов(p);
	возврат p
кон DecoderFactory;

проц EncoderFactory*() : Codecs.ImageEncoder;
перем p : BMPEncoder;
нач
	нов(p);
	возврат p
кон EncoderFactory;

кон BMPCodec.
