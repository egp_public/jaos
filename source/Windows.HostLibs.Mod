(**
	AUTHOR: Alexey Morozov
	PURPOSE: Dynamic library tools for Windows platforms
*)
модуль HostLibs;

использует
	НИЗКОУР, Kernel32;

тип
	LibHandle* = Kernel32.HANDLE; (** dynamic library handle type *)

конст
	NilLibHandle* = Kernel32.NULL; (** invalid library handle *)

	(**
		Load a dynamic library

		fileName: library file name
		lib: returned loaded library handle; NilLibHandle in case of an error

		Return: TRUE in case of success
	*)
	проц LoadLibrary*(конст fileName: массив из симв8; перем lib: LibHandle): булево;
	перем err: цел32;
	нач
		lib := Kernel32.LoadLibrary(fileName);
		если lib # NilLibHandle то
			возврат истина;
		всё;
		err := Kernel32.GetLastError();
		трассируй(fileName, err);
	выходя
		возврат ложь;
	кон LoadLibrary;

	(**
		Free a previously loaded dynamic library

		lib: library handle

		Return: TRUE in case of success
	*)
	проц FreeLibrary*(конст lib: LibHandle): булево;
	нач
		возврат (Kernel32.FreeLibrary(lib) # 0);
	выходя
		возврат ложь;
	кон FreeLibrary;

	(**
		Get a procedure from a loaded dynamic library

		lib: library handle
		name: name of the procedure
		procAddr: address of the destination procedure pointer (e.g. ADDRESSOF(procedureVariable))

		Return: TRUE in case of success
	*)
	проц GetProcedure*(конст lib: LibHandle; конст name: массив из симв8; конст procAddr: адресВПамяти): булево;
	перем addr: адресВПамяти;
	нач
		утв(procAddr # НУЛЬ);
		Kernel32.GetProcAddress(lib,name,addr);
		если addr # НУЛЬ то
			НИЗКОУР.запишиОбъектПоАдресу(procAddr,addr);
			возврат истина;
		всё;
	выходя
		возврат ложь;
	кон GetProcedure;

кон HostLibs.
