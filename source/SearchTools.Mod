модуль SearchTools; (** AUTHOR "staubesv"; PURPOSE "Some simple search tools"; *)

использует
	Потоки, Commands, Options, Files, Strings, UTF8Strings, Texts, TextUtilities;

тип

	SearchString = массив 256 из симв8;
	SearchStringUCS = массив 256 из Texts.Char32;

	Parameters = укль на запись
		repeat: булево;
		subdirectory: булево;
	кон;

	PatternParameters = укль на запись (Parameters)
		searchString : SearchString;
		replaceString : SearchString;
	кон;

	Statistics = окласс
	перем
		nofFiles : размерМЗ;
		nofMatches, nofConflicts, nofErrors : размерМЗ;

		verbose : булево;
		abort : булево;

		проц &Reset*;
		нач
			nofFiles := 0;
			nofMatches := 0; nofErrors := 0; nofConflicts := 0;
			verbose := ложь; abort := ложь;
		кон Reset;

		проц Show(w : Потоки.Писарь);
		нач
			w.пЦел64(nofMatches, 0); w.пСтроку8(" matches of "); w.пЦел64(nofFiles, 0); w.пСтроку8(" files");
			если (nofConflicts > 0) то w.пСтроку8(", "); w.пЦел64(nofConflicts, 0); w.пСтроку8(" conflict(s)"); всё;
			если (nofErrors > 0) то w.пСтроку8(", "); w.пЦел64(nofErrors, 0); w.пСтроку8(" error(s)"); всё;
			w.пСтроку8("."); w.пВК_ПС;
		кон Show;

	кон Statistics;

	EnumProc = проц (конст filename : Files.FileName; param : Parameters; stats : Statistics; context : Commands.Context);


проц FindString(конст filename : Files.FileName; param : Parameters; stats : Statistics; context : Commands.Context);
перем text : Texts.Text; pos: размерМЗ; format, res : целМЗ; searchString : SearchStringUCS; idx, nbrOfHits : размерМЗ;
нач
	утв(param суть PatternParameters);
	nbrOfHits := 0;
	idx := 0;
	просейТип param:PatternParameters делай
		UTF8Strings.UTF8toUnicode(param.searchString, searchString, idx);
	всё;
	нов(text);
	TextUtilities.LoadAuto(text, filename, format, res);
	если (res = 0) то
		text.AcquireRead;
		pos := TextUtilities.Pos(searchString, 0, text);
		нцПока (pos > 0) делай
			увел(nbrOfHits);
			pos := TextUtilities.Pos(searchString, pos + 1, text);
		кц;
		text.ReleaseRead;
		если (nbrOfHits > 0) то
			увел(stats.nofMatches);
			context.out.пСтроку8(filename);
			если stats.verbose то
				context.out.пСтроку8(" ("); context.out.пЦел64(nbrOfHits, 0); context.out.пСтроку8(" hits"); context.out.пСтроку8(")");
			всё;
			context.out.пВК_ПС;
		всё;
	иначе
		увел(stats.nofErrors);
		context.error.пСтроку8("Coult not load text: "); context.error.пСтроку8(filename); context.error.пВК_ПС;
	всё;
кон FindString;

проц ReplaceString(конст filename : Files.FileName; param : Parameters; stats : Statistics; context : Commands.Context);
перем
	text : Texts.Text; pos: размерМЗ; format, res: целМЗ;
	searchString, replaceString : SearchStringUCS; idx : размерМЗ;
	searchStringLen, replaceStringLen : размерМЗ;
	replaceCount : размерМЗ;
	conflict : булево;

	(* Replace string at position <pos> of length <len> with <replString> *)
	проц Replace(pos, len : размерМЗ; конст replString : SearchStringUCS);
	нач
		text.Delete(pos, len);
		text.InsertUCS32(pos, replString);
		len := TextUtilities.UCS32StrLength(replString);
	кон Replace;

нач
	утв(param суть PatternParameters);
	replaceCount := 0; conflict := ложь;
	просейТип param:PatternParameters делай
		idx := 0; UTF8Strings.UTF8toUnicode(param.searchString, searchString, idx);
		idx := 0; UTF8Strings.UTF8toUnicode(param.replaceString, replaceString, idx);
	всё;
	searchStringLen := TextUtilities.UCS32StrLength(searchString);
	replaceStringLen := TextUtilities.UCS32StrLength(replaceString);
	нов(text);
	TextUtilities.LoadAuto(text, filename, format, res);
	если (res = 0) то
		text.AcquireWrite;
		pos := TextUtilities.Pos(replaceString, 0, text);
		если (pos > 0) то увел(stats.nofConflicts); conflict := истина; всё;

		pos := TextUtilities.Pos(searchString, 0, text);
		нцПока (pos > 0) делай
			увел(replaceCount);
			Replace(pos, searchStringLen, replaceString);
			pos := TextUtilities.Pos(searchString, pos + replaceStringLen, text);
		кц;
		text.ReleaseWrite;
		если (replaceCount > 0) то
			увел(stats.nofMatches);
			context.out.пСтроку8(filename);
			если stats.verbose то
				context.out.пСтроку8(" ("); context.out.пЦел64(replaceCount, 0); context.out.пСтроку8(" replacements)");
				если conflict то context.out.пСтроку8(" CONFLICT"); всё;
			всё;
			context.out.пВК_ПС;
			res := -1;
			если (format = 0) то TextUtilities.StoreOberonText(text, filename, res);
			аесли (format = 1) то TextUtilities.StoreText(text, filename, res);
			аесли (format = 2) то TextUtilities.ExportUTF8(text, filename, res);
			иначе
				увел(stats.nofErrors);
				context.error.пСтроку8("Could not store text: "); context.error.пСтроку8(filename);
				context.error.пСтроку8(" (Format unknown)"); context.error.пВК_ПС;
			всё;
			если (res # 0) то
				увел(stats.nofErrors);
				context.error.пСтроку8("Could not store text: "); context.error.пСтроку8(filename); context.error.пВК_ПС;
			всё;
		всё;
	иначе
		увел(stats.nofErrors);
		context.error.пСтроку8("Could not load text: "); context.error.пСтроку8(filename); context.error.пВК_ПС;
	всё;
кон ReplaceString;

(* 	Boyer-Moore match for streams. This procedure opens a file as character stream and does not take special care of formatting information.
	It also doesn't do statistics but just outputs the filename if a match occurs and returns after the first match *)
проц FindStringRaw(конст filename : Files.FileName; param : Parameters; stats : Statistics; context : Commands.Context);
перем
	r : Files.Reader;
	f : Files.File;
	m: размерМЗ;
	p : PatternParameters;
нач
	утв(param суть PatternParameters);
	p := param (PatternParameters);
	m := Strings.Length(p.searchString);
	f := Files.Old(filename);
	если f # НУЛЬ то
		Files.OpenReader(r, f, 0);
		SearchPatternRaw(r,НУЛЬ, p.searchString);
		если r.кодВозвратаПоследнейОперации=0 то
			context.out.пСтроку8(filename); context.out.пВК_ПС; context.out.ПротолкниБуферВПоток;
			возврат;
		всё;
	иначе
		context.error.пСтроку8("Could not open file "); context.error.пСтроку8(filename); context.error.пВК_ПС;
		context.error.ПротолкниБуферВПоток;
	всё
кон FindStringRaw;


(* 	Boyer-Moore match for streams. This procedure opens a file as character stream and does not take special care of formatting information.
	It also doesn't do statistics but just outputs the filename if a match occurs and returns after the first match.
	If a Streams.Writer is provided, it is fed with the data in the interval before each <pattern> location.
	Postcondition:
	- If pattern is found, r.res=0 and Reader is positioned after <pattern> ; otherwise r.res#0  *)

проц SearchPatternRaw*(r : Потоки.Чтец; w: Потоки.Писарь; конст pattern: массив из симв8);
перем
	d : массив 256 из размерМЗ;
	cb : Strings.String;
	pos, cpos, i, j, k, m, shift : размерМЗ;
нач
	m := Strings.Length(pattern);
	нов(cb, m);
	нцПока (r.кодВозвратаПоследнейОперации = 0 ) и (cpos < m) делай
		cb[cpos] := r.чИДайСимв8();
		увел(cpos);
	кц;
	если r.кодВозвратаПоследнейОперации = 0 то
		нцДля i := 0 до 255 делай d[i] := m кц;
		нцДля i := 0 до m-2 делай d[кодСимв8(pattern[i])] := m - i - 1 кц;
		i := m;
		нц
			j := m; k := i;
			нцДо умень(k); умень(j);
			кцПри (j < 0) или (pattern[j] # cb[k остОтДеленияНа m]);
			если j<0 то прервиЦикл всё;
			shift := d[кодСимв8(cb[(i-1) остОтДеленияНа m])];
			i := i + shift;
			нцПока (cpos < i) и (r.кодВозвратаПоследнейОперации = 0) делай
				pos:=cpos остОтДеленияНа m;
				если w#НУЛЬ то w.пСимв8(cb[pos]);всё;
				cb[pos] := r.чИДайСимв8();
				увел(cpos);
			кц;
			если r.кодВозвратаПоследнейОперации#0 то прервиЦикл всё;
		кц;
		если w#НУЛЬ то w.ПротолкниБуферВПоток всё;
	всё;
кон SearchPatternRaw;

проц Enumerate(конст path, pattern : массив из симв8; param : Parameters; proc : EnumProc; stats : Statistics; context : Commands.Context);
перем
	enum : Files.Enumerator;
	filename : Files.FileName;
	fileflags : мнвоНаБитахМЗ;
	time, date : цел32; nofMatches : размерМЗ;
	size : Files.Size;
нач
	утв(proc # НУЛЬ);
	если path # "" то Files.JoinPath(path, pattern, filename) всё;
	нов(enum); enum.Open(filename, {});
	нцПока enum.GetEntry(filename, fileflags, time, date, size) и ~stats.abort делай
		если ~(Files.Directory в fileflags) то
			нцДо
				nofMatches := stats.nofMatches;
				proc(filename, param, stats, context);
			кцПри ~param.repeat или (stats.nofMatches = nofMatches);
			context.out.ПротолкниБуферВПоток; context.error.ПротолкниБуферВПоток;
			увел(stats.nofFiles);
		всё;
	кц;
	если param.subdirectory то
	Files.JoinPath(path, "*", filename);
	enum.Open(filename, {});
	нцПока enum.GetEntry(filename, fileflags, time, date, size) и ~stats.abort делай
		если (Files.Directory в fileflags) то
			Enumerate(filename, pattern, param, proc, stats, context);
		всё;
	кц;
	всё;
	enum.Close;
кон Enumerate;

проц Unescape (конст source: массив из симв8; перем dest: массив из симв8);
перем si, di: размерМЗ;
нач
	si := 0; di := 0;
	нцПока source[si] # 0X делай
		если (source[si] = '\') и (source[si + 1] # 0X) то
			увел (si);
			просей source[si] из
			| 't': dest[di] := 09X;
			| 'n': dest[di] := 0AX;
			| 'r': dest[di] := 0DX;
			| 'w': dest[di] := 20X;
			иначе dest[di] := source[si];
			всё;
		иначе
			dest[di] := source[si];
		всё;
		увел (si); увел (di);
	кц;
	dest[di] := 0X;
кон Unescape;

(** List all files that match <filePattern> and contain the specified <searchString> *)
проц Find*(context : Commands.Context); (** [Optinos] filePattern searchString *)
перем
	options : Options.Options;
	filePattern, path, pattern : Files.FileName; searchString : SearchString;
	param : PatternParameters;
	stats : Statistics;
нач
	нов(options);
	options.Add("v", "verbose", Options.Flag);
	options.Add("f", "formatted", Options.Flag);
	options.Add("r", "repeat", Options.Flag);
	options.Add("s", "subdirectory", Options.Flag);

	если options.Parse(context.arg, context.error) то
		filePattern := ""; searchString := "";
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filePattern);
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(searchString);
		если (searchString # "") то
			нов(stats);
			stats.verbose := options.GetFlag("verbose");
			нов(param);
			param.repeat := options.GetFlag("repeat");
			param.subdirectory := options.GetFlag("subdirectory");
			Unescape(searchString, param.searchString);
			если stats.verbose то
				context.out.пСтроку8("Searching '"); context.out.пСтроку8(searchString); context.out.пСтроку8("' in ");
				context.out.пСтроку8(filePattern); context.out.пСтроку8("..."); context.out.пВК_ПС; context.out.ПротолкниБуферВПоток;
			всё;
			Files.SplitPath(filePattern, path, pattern);
			если options.GetFlag("formatted") то
				Enumerate(path, pattern, param, FindString, stats, context);
			иначе
				Enumerate(path, pattern, param, FindStringRaw, stats, context);
			всё;
			если stats.verbose то
				stats.Show(context.out);
			всё;
		иначе
			context.error.пСтроку8("No valid search string parameter"); context.error.пВК_ПС;
		всё;
	всё;
кон Find;

(** Replace all occurences of <searchString> by <replaceString> in files matching to <filePattern> *)
проц Replace*(context : Commands.Context); (** filePattern searchString replaceString *)
перем
	options : Options.Options;
	filePattern, path, pattern : Files.FileName; searchString, replaceString : SearchString;
	param : PatternParameters;
	stats : Statistics;
нач
	нов(options);
	options.Add("v", "verbose", Options.Flag);
	options.Add("r", "repeat", Options.Flag);
	options.Add("s", "subdirectory", Options.Flag);
	если options.Parse(context.arg, context.error) то
		filePattern := ""; searchString := ""; replaceString := "";
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filePattern);
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(searchString);
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(replaceString);
		если (searchString # "") то
			нцПока (searchString # "") делай
				context.out.пСтроку8("Replacing '"); context.out.пСтроку8(searchString); context.out.пСтроку8("' by '"); context.out.пСтроку8(replaceString); context.out.пВК_ПС;
				нов(stats);
				stats.verbose := options.GetFlag("verbose");
				нов(param);
				param.repeat := options.GetFlag("repeat");
				param.subdirectory := options.GetFlag("subdirectory");
				Unescape(searchString, param.searchString);
				Unescape(replaceString, param.replaceString);
				если stats.verbose то
					context.out.пСтроку8("Replacing '"); context.out.пСтроку8(searchString); context.out.пСтроку8("' by '"); context.out.пСтроку8(replaceString);
					context.out.пСтроку8("' in "); context.out.пСтроку8(filePattern); context.out.пСтроку8("... "); context.out.пВК_ПС;
				всё;
				Files.SplitPath(filePattern, path, pattern);
				Enumerate(path, pattern, param, ReplaceString, stats, context);
				если stats.verbose то
					stats.Show(context.out);
				всё;
				context.out.пСтроку8("done."); context.out.пВК_ПС;
				context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(searchString);
				context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(replaceString);
			кц;
		иначе
			context.error.пСтроку8("No valid search string parameter"); context.error.пВК_ПС;
		всё;
	всё;
кон Replace;


кон SearchTools.

System.Free SearchTools ~

SearchTools.Find -s -r E:/Highdim/Hardware/*.xml "?{" ~

SearchTools.Find E:/WinaosNewCommands/source/*.Mod Objects  ~
SearchTools.Find E:/WinaosNewCommands/winaos/src/*.Mod Commands  ~

SearchTools.Replace E:/WinaosNewCommands/source/*.Mod AosCommands Commands ~
SearchTools.Replace E:/WinaosNewCommands/winaos/src/*.Mod AosCommands Commands ~

System.FreeDownTo SearchTools ~
