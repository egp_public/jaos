(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль LinEqLU;   (** AUTHOR "adf"; PURPOSE "LU matrix decomposition with total pivoting"; *)

использует Int := NbrInt, Nbr := NbrRe, Vec := VecRe, Mtx := MtxRe, Errors := DataErrors, LinEq := LinEqRe;

тип
	(** For solving moderate sized linear systems of equations, even if the rank is less than the dimension. *)
	Solver* = окласс (LinEq.Solver)
	перем rank, dim: цел32;
		mtxMag: Nbr.Real;
		colPivot, rowPivot: укль на массив из цел32;
		lu: Mtx.Matrix;

		проц Decompose;
		перем i, j, k: цел32;  abs, adjustment, factor, maxCell, ratio: Nbr.Real;
		нач
			dim := lu.rows;  нов( colPivot, dim - 1 );  нов( rowPivot, dim - 1 );
			(* Perform total pivoting. *)
			нцДля k := 0 до dim - 2 делай
				maxCell := 0;
				нцДля j := k до dim - 1 делай
					нцДля i := k до dim - 1 делай
						abs := матМодуль( lu.Get( i, j ) );
						если abs > maxCell то maxCell := abs;  rowPivot[k] := i;  colPivot[k] := j всё
					кц
				кц;
				если rowPivot[k] # k то lu.SwapRows( rowPivot[k], k ) всё;
				если colPivot[k] # k то lu.SwapColumns( colPivot[k], k ) всё
			кц;
			(* LU decomposition of the pivoted matrix. *)
			нцДля k := 0 до dim - 2 делай
				если lu.Get( k, k ) # 0 то
					rank := k + 1;
					нцДля i := rank до dim - 1 делай
						ratio := lu.Get( i, k ) / lu.Get( k, k );
						нцДля j := rank до dim - 1 делай
							adjustment := ratio * lu.Get( k, j );  factor := lu.Get( i, j ) - adjustment;  lu.Set( i, j, factor )
						кц;
						lu.Set( i, k, ratio )
					кц
				иначе
					(* rank # dim; therefore, only factored dominant submatrix of dimension rank. *)
					возврат
				всё
			кц;
			если lu.Get( dim - 1, dim - 1 ) # 0 то rank := dim всё
		кон Decompose;

	(** Requires NEW to pass matrix A as a parameter when creating a solver object. *)
		проц & {перекрыта}Initialize*( перем A: Mtx.Matrix );
		нач
			если A # НУЛЬ то lu := A.Copy();  LinEq.NormalizeMatrix( lu, mtxMag );  Decompose
			иначе Errors.Error( "A NIL matrix was supplied." )
			всё
		кон Initialize;

	(** Solves  Ax = b  for  x  given  b. *)
		проц {перекрыта}Solve*( перем b: Vec.Vector ): Vec.Vector;
		перем i, k: цел32;  adjustment, factor, mag, ratio, zero: Nbr.Real;  x: Vec.Vector;
		нач
			если b # НУЛЬ то
				если dim = b.lenx то
					x := b.Copy();  LinEq.NormalizeVector( x, mag );
					(* Exchange rows in the working vector. *)
					нцДля i := 0 до dim - 2 делай
						если rowPivot[i] # i то x.Swap( rowPivot[i], i ) всё
					кц;
					(* Forward substitution. *)
					нцДля k := 0 до rank - 2 делай
						нцДля i := k + 1 до rank - 1 делай
							adjustment := lu.Get( i, k ) * x.Get( k );  factor := x.Get( i ) - adjustment;  x.Set( i, factor )
						кц
					кц;
					(* Backward substitution. *)
					нцДля k := rank - 1 до 1 шаг -1 делай
						ratio := x.Get( k ) / lu.Get( k, k );  x.Set( k, ratio );
						нцДля i := 0 до k - 1 делай
							adjustment := lu.Get( i, k ) * x.Get( k );  factor := x.Get( i ) - adjustment;  x.Set( i, factor )
						кц
					кц;
					ratio := x.Get( 0 ) / lu.Get( 0, 0 );  x.Set( 0, ratio );
					(* Place zeros in the solution vector at positions rank to dim-1. *)
					zero := 0;
					нцДля i := rank до dim - 1 делай x.Set( i, zero ) кц;
					(* Remove the pivoting. *)
					нцДля i := dim - 2 до 0 шаг -1 делай
						если colPivot[i] # i то x.Swap( colPivot[i], i ) всё
					кц;
					(* Renormalize the solution. *)
					x.Multiply( mag / mtxMag )
				иначе x := НУЛЬ;  Errors.Error( "Incompatible dimension for vector b." )
				всё
			иначе x := НУЛЬ;  Errors.Error( "A NIL right-hand-side vector was supplied." )
			всё;
			возврат x
		кон Solve;

	(** Returns the rank of matrix  A, which can be less than its size. *)
		проц Rank*( ): Int.Integer;
		перем r: Int.Integer;
		нач
			r := rank;  возврат r
		кон Rank;

	кон Solver;

	(** Computes the inverse of a square matrix A and returns A-1 if it exists; otherwise, it returns NIL. *)
	проц Invert*( перем A: Mtx.Matrix ): Mtx.Matrix;
	перем i, k: цел32;  zero, one: Nbr.Real;  unit, soln: Vec.Vector;  inverse: Mtx.Matrix;  vecArray: Vec.Array;
		mtxArray: Mtx.Array;  lu: Solver;
	нач
		inverse := НУЛЬ;
		если A # НУЛЬ то
			если A.rows = A.cols то
				нов( lu, A );
				если lu.rank = lu.dim то
					нов( vecArray, lu.dim );  zero := 0;  one := 1;
					нцДля i := 0 до lu.dim - 1 делай vecArray[i] := 0 кц;
					unit := vecArray^;  нов( mtxArray, lu.dim, lu.dim );
					нцДля k := 0 до lu.dim - 1 делай
						unit.Set( k, one );  soln := lu.Solve( unit );  unit.Set( k, zero );
						нцДля i := 0 до lu.dim - 1 делай mtxArray[i, k] := soln.Get( i ) кц
					кц;
					inverse := mtxArray^;
				иначе Errors.Error( "The matrix is singular." )
				всё
			иначе Errors.Error( "The matrix is not square; it's inverse can't be found." )
			всё
		иначе Errors.Error( "A NIL matrix was supplied." )
		всё;
		возврат inverse
	кон Invert;

кон LinEqLU.
