модуль WMTetris;	(** AUTHOR "TF"; PURPOSE "Tetris with semitransparent blocks"; *)

использует
	Modules, Kernel, Random, Strings,
	Raster, WMRasterScale, WMRectangles, WMGraphics, WMGraphicUtilities,
	WMMessages, WM := WMWindowManager, WMDialogs, Inputs;

конст

	Border = 10; (* window border in number of pixels *)

	BoxSize = 16;

	(* Width and height of game field in number of BoxSize's *)
	Width = 10;	Height = 30;

	(* Position of game field *)
	FieldOffsetX = 120;
	FieldOffsetY = Border;

	InfoOffsetX = Border;
	InfoOffsetY = 100;
	InfoWidth = FieldOffsetX - 2*Border;
	InfoHeight = 110 + 2 * Border;
	InfoLineHeight  = 20;

	WindowWidth = 1*Border + FieldOffsetX + Width*BoxSize;
	WindowHeight = 2*Border + Height*BoxSize;

	BevelBorder = 3;
	BlockSize = 5;

	NofBlocks = 7;
	RandomDrop = ложь;

	LinesToLevelRatio = 10; (* level = lines DIV LinesToLevelRatio *)

	(* Additions bonus points when removing more than one line at once (1 line = 1 point) *)
	TwoLinesBonus = 6; (* 2 lines -> 8 points *)
	ThreeLinesBonus = 13; (* 3 lines -> 16 points *)
	FourLinesBonus = 46; (* 4 lines -> 50 points *)

	SameColorBonus = 50; (* Bonus when removing a line where all boxes have the same color *)
	LevelUpBonus = 20;

	Initialized = 0;
	Running = 5;
	Paused = 6;
	Restarting = 7;
	Finished = 8;
	Terminating = 9;
	Terminated = 10;

перем
	colors : массив NofBlocks + 1 из Raster.Pixel;

тип
	KillerMsg = окласс
	кон KillerMsg;

	Block = массив BlockSize, BlockSize из симв8;

	Window = окласс (WM.BufferWindow)
	перем
		dropped : булево;
		field : массив Width из массив Height из симв8;
		rotBlock, block, nextBlock : Block;
		posX, posY : цел32;
		mode : Raster.Mode;
		random : Random.Generator;
		lines, blocks, delay, delayDec, level, points : цел32;

		generateNewBlock : булево;

		timer : Kernel.Timer;
		state : цел32;

		backgroundImage : WMGraphics.Image;

		проц &New*(alpha : булево);
		перем pixel : Raster.Pixel;
		нач
			IncCount;
			Init(WindowWidth, WindowHeight, alpha);
			Raster.InitMode(mode, Raster.srcCopy); нов(timer); нов(random); random.InitSeed(Kernel.GetTicks());
			Raster.SetRGBA(pixel, 0C0H, 0C0H, 0CCH, 0CCH);
			Raster.Fill(img, 0, 0, WindowWidth, WindowHeight, pixel, mode);
			backgroundImage := WMGraphics.LoadImage("SaasFee.jpg", истина);
			если (backgroundImage # НУЛЬ) то
				WMRasterScale.Scale(
					backgroundImage, WMRectangles.MakeRect(0, 0, backgroundImage.width, backgroundImage.height),
					img, WMRectangles.MakeRect(0, 0, img.width, img.height),
					WMRectangles.MakeRect(0, 0, img.width, img.height),
					WMRasterScale.ModeCopy, WMRasterScale.ScaleBilinear);
			всё;
			(* Game field *)
			Raster.Fill(img, FieldOffsetX, FieldOffsetY, FieldOffsetX + Width*BoxSize, FieldOffsetY + Height*BoxSize, colors[0], mode);
			WMGraphicUtilities.DrawBevel(canvas, WMRectangles.MakeRect(
				FieldOffsetX - BevelBorder, FieldOffsetY - BevelBorder, FieldOffsetX + Width*BoxSize + BevelBorder, FieldOffsetY + Height*BoxSize + BevelBorder),
				2, истина, цел32(0FFFFFFFFH), WMGraphics.ModeCopy);

			(* Preview panel *)
			Raster.Fill(img, Border, Border, FieldOffsetX - Border, Border + BlockSize*BoxSize, colors[0], mode);
			WMGraphicUtilities.DrawBevel(canvas, WMRectangles.MakeRect(
				Border - BevelBorder, Border - BevelBorder, FieldOffsetX - Border + BevelBorder, Border + BlockSize*BoxSize + BevelBorder),
				2, истина, цел32(0FFFFFFFFH), WMGraphics.ModeCopy);
			Reset;
			pointerThreshold := 10;
			WM.DefaultAddWindow(сам);
			SetTitle(Strings.NewString("WM Transparent Tetris"));
			SetIcon(WMGraphics.LoadImage("WMIcons.tar://WMTetris.png", истина));
			state := Initialized;
		кон New;

		проц SetState(l0state : цел32);
		нач {единолично}
			если (сам.state < Terminating) или (l0state = Terminated) то
				сам.state := l0state;
			всё;
		кон SetState;

		проц AwaitState(l1state : цел32);
		нач {единолично}
			дождись(сам.state = l1state);
		кон AwaitState;

		проц DrawInfo;
		перем string : массив 128 из симв8; nbr : массив 16 из симв8;

			проц DrawLine(line : цел32; конст l2string : массив из симв8);
			нач
				утв(line >= 1);
				WMGraphics.DrawStringInRect(canvas,
					WMRectangles.MakeRect(
						InfoOffsetX + Border, InfoOffsetY + Border + (line-1) * InfoLineHeight,
						InfoOffsetX + InfoWidth - Border, InfoOffsetY + Border + line * InfoLineHeight),
					ложь, WMGraphics.AlignCenter, WMGraphics.AlignTop, l2string);
			кон DrawLine;

		нач
			canvas.Fill(WMRectangles.MakeRect(InfoOffsetX, InfoOffsetY, FieldOffsetX - Border, InfoOffsetY + InfoHeight), цел32(0FFFFFFA0H), WMGraphics.ModeCopy);
			WMGraphicUtilities.DrawBevel(canvas, WMRectangles.MakeRect(
				InfoOffsetX - BevelBorder, InfoOffsetY - BevelBorder, FieldOffsetX - Border + BevelBorder, InfoOffsetY + InfoHeight + BevelBorder),
				2, истина, цел32(0FFFFFFFFH), WMGraphics.ModeCopy);
			canvas.SetColor(WMGraphics.Black);
			если (state = Running) или (state = Finished) то
				если (state = Finished) то
					DrawLine(1, "Press 'Space'");
					DrawLine(2, "to restart!");
				всё;
				(* Number of lines completed *)
				string := "Lines: "; Strings.IntToStr(lines, nbr); Strings.Append(string, nbr);
				DrawLine(3, string);
				(* Number of blocks *)
				string := "Blocks: "; Strings.IntToStr(blocks-1, nbr); Strings.Append(string, nbr);
				DrawLine(4, string);
				(* Level *)
	 			string := "Level: "; Strings.IntToStr(level, nbr); Strings.Append(string, nbr);
				DrawLine(5, string);
				(* Points *)
				string := "Points: "; Strings.IntToStr(points, nbr); Strings.Append(string, nbr);
				DrawLine(6, string);
			аесли (state = Initialized) то
				DrawLine(1, "Press 'Space'");
				DrawLine(2, "to start!");
			аесли (state = Paused) то
				DrawLine(1, "Press 'Space'");
				DrawLine(2, "to continue!");
			всё;
			Invalidate(WMRectangles.MakeRect(
				InfoOffsetX - BevelBorder, InfoOffsetY - BevelBorder, FieldOffsetX - Border + BevelBorder, InfoOffsetY + InfoHeight + BevelBorder));
		кон DrawInfo;

		проц {перекрыта}StyleChanged*;
		нач
			DrawInfo
		кон StyleChanged;

		проц RotateBlock(конст l3block : Block) : Block;
		перем i, j : цел16; temp : Block;
		нач
			нцДля i := 0 до BlockSize - 1 делай нцДля j := 0 до BlockSize - 1 делай temp[j, i] := l3block[(BlockSize - 1) - i, j] кц кц;
			возврат temp
		кон RotateBlock;

		проц DrawBox(x, y : цел32; color : симв8);
		перем pix : Raster.Pixel;
		нач
			pix := colors [кодСимв8(color)];
			если (x >= 0) и (x < Width) и (y >= 0) и (y < Height) то
				Raster.Fill(img, FieldOffsetX + x * BoxSize, FieldOffsetY + y * BoxSize,
					FieldOffsetX + x * BoxSize+ BoxSize, FieldOffsetY + y * BoxSize + BoxSize, pix, mode);
				если (color # 0X) то
					WMGraphicUtilities.RectGlassShade(canvas, WMRectangles.MakeRect(
						FieldOffsetX + x * BoxSize, FieldOffsetY + y * BoxSize,
						FieldOffsetX + x * BoxSize+ BoxSize, FieldOffsetY + y * BoxSize + BoxSize), 2, истина);
				всё;
			всё;
		кон DrawBox;

		проц DrawPreview(конст l4block : Block);
		перем
			i, j : цел32;

			проц l5DrawBox(x, y : цел32; color : симв8);
			перем pix : Raster.Pixel;
			нач
				pix := colors [кодСимв8(color)];
				Raster.Fill(img, Border + x * BoxSize, Border + y * BoxSize,
					Border + x * BoxSize+ BoxSize, Border + y * BoxSize + BoxSize, pix, mode);
				если (color # 0X) то
					WMGraphicUtilities.RectGlassShade(canvas, WMRectangles.MakeRect(
						Border + x * BoxSize, Border + y * BoxSize,
						Border + x * BoxSize+ BoxSize, Border + y * BoxSize + BoxSize), 2, истина);
				всё;
			кон l5DrawBox;

		нач
			нцДля i := 0 до BlockSize - 1 делай
				нцДля j := 0 до BlockSize - 1 делай
					l5DrawBox(i, j, l4block[i, j]);
				кц;
			кц;
			Invalidate(WMRectangles.MakeRect(Border, Border, Border + BlockSize*BoxSize, Border + BlockSize*BoxSize));
		кон DrawPreview;

		проц SetBlock(x, y : цел32; clear : булево);
		перем i, j : цел32;
		нач
			нцДля i := 0 до BlockSize - 1 делай нцДля j := 0 до BlockSize - 1 делай
				если block[i, j] # 0X то
					если (i + x < Width) и (j + y >= 0) и (j + y < Height) то
						если clear то
							field[i + x, j + y] := 0X;
							DrawBox(i + x, j + y, 0X)
						иначе field[i + x, j + y] := block[i, j];
							DrawBox(i + x, j + y, block[i, j])
						всё
					всё
				всё
			кц кц
		кон SetBlock;

		проц HasDownCollision(x, y : цел32) : булево;
		перем i, j : цел32;
		нач
			нцДля i := 0 до BlockSize - 1 делай нцДля j := 0 до BlockSize - 1 делай
				если block[i, j] # 0X то
					если (i + x < Width) и (j + y >= 0) то
						если (j + y < Height) то
							если (block[i, j] # 0X) и (field[i + x, j + y] # 0X) то возврат истина всё
						аесли block[i, j] # 0X то возврат истина
						всё
					иначе возврат истина
					всё
				всё
			кц кц;
			возврат ложь
		кон HasDownCollision;

		проц HasCollision(конст bl : Block; x, y : цел32) : булево;
		перем i, j : цел32;
		нач
			нцДля i := 0 до BlockSize - 1 делай нцДля j := 0 до BlockSize - 1 делай
				если bl[i, j] # 0X то
					если (i + x >= Width) или (i + x < 0) или (j + y >= Height) или (field[i + x, j + y] # 0X) то возврат истина всё
				всё
			кц кц;
			возврат ложь
		кон HasCollision;

		проц Move(dir : цел32) : булево;
		перем newX, newY : цел32; result : булево;
		нач
			newX := posX; newY := posY;
			если dir = 0 то увел(newX)
			аесли dir = 1 то умень(newX)
			аесли dir = 2 то увел(newY)
			всё;

			SetBlock(posX, posY, истина);
			если ~HasCollision(block, newX, newY) то posX := newX; posY := newY; result := истина
			иначе result := ложь
			всё;
			SetBlock(posX, posY, ложь);
			Invalidate(WMRectangles.MakeRect(FieldOffsetX + posX * BoxSize - BoxSize, FieldOffsetY + posY * BoxSize - BoxSize,
				FieldOffsetX + posX * BoxSize + BlockSize * BoxSize + BoxSize,  FieldOffsetY + posY * BoxSize + BlockSize*BoxSize +BoxSize));
			возврат result
		кон Move;

		проц {перекрыта}KeyEvent*(ucs : размерМЗ; l6flags: мнвоНаБитахМЗ; keysym : размерМЗ);
		перем ignore : булево;
			l7rotBlock : Block;
		нач {единолично}
			если Inputs.Release в l6flags то
				возврат;
			аесли (state >= Terminating) то
				возврат;
			аесли (state = Initialized) то
				если (keysym = 020H) то state := Running; всё;
			аесли (state = Running) то
				если (keysym = 0FF50H) или (keysym = 0FF51H) то (* Move left *)
					ignore := Move(1);
				аесли (keysym = 0FF55H)или (keysym = 0FF53H) то (* Move right *)
					ignore := Move(0)
				аесли (keysym = 0FF52H) то (* Rotate block *)
					SetBlock(posX, posY, истина);
					l7rotBlock := RotateBlock(block);
					если ~HasCollision(l7rotBlock, posX, posY) то block := l7rotBlock всё;
					SetBlock(posX, posY, ложь);
					Invalidate(WMRectangles.MakeRect(
						FieldOffsetX + posX * BoxSize - BoxSize, FieldOffsetY + posY * BoxSize - BoxSize,
						FieldOffsetX + posX * BoxSize + BlockSize * BoxSize, FieldOffsetY + posY * BoxSize + BlockSize * BoxSize));
				аесли (keysym = 0FF54H) или (keysym = 0FF0DH) или (keysym = 20H) то (* Drop block *)
					dropped := истина;
				аесли (keysym = 070H) то (* p key *)
					state := Paused;
				всё;
			аесли (state = Finished) то
				если (keysym = 020H) то state := Restarting; всё;
			аесли (state = Paused) то
				если (keysym = 020H) или (keysym = 070H)  то state := Running; всё;
			всё;
		кон KeyEvent;

		проц NewBlock() : Block;
		перем
			newBlock : Block;
			i, j : цел32; kind : цел32;
			color : симв8;

			проц Set(x, y : цел32);
			нач
				newBlock[x, y] := color
			кон Set;

		нач
			dropped := ложь;
			posX := Width DIV 2 - 1; posY := 0;

			нцДля i := 0 до BlockSize - 1 делай нцДля j := 0 до BlockSize - 1 делай newBlock [i, j] := 0X кц кц;
			kind := random.Integer() остОтДеленияНа NofBlocks;

			color := симв8ИзКода(1 + kind);

			просей kind из
				| 0 : Set(0, 2); Set(1, 2); Set(2, 2); Set(3, 2)
				| 1 : Set(1, 3); Set(2, 3); Set(3, 3); Set(2, 2)
				| 2 : Set(1, 1); Set(1, 2); Set(2, 2); Set(2, 3)
				| 3 : Set(2, 1); Set(1, 2); Set(2, 2); Set(1, 3)
				| 4 : Set(2, 1); Set(2, 2); Set(2, 3); Set(3, 3)
				| 5 : Set(2, 1); Set(2, 2); Set(2, 3); Set(1, 3)
				| 6 : Set(1, 1); Set(1, 2); Set(2, 1); Set(2, 2)
			всё;
			увел(blocks);
			DrawPreview(newBlock);
			возврат newBlock;
		кон NewBlock;

		проц RemoveLine(y : цел32);
		перем i, j : цел32; oldLevel : цел32;
		нач
			нцДля i := 0 до Width - 1 делай
				нцДля j := y до 1 шаг - 1 делай
					field[i, j] := field[i, j - 1];
					DrawBox(i, j, field[i, j])
				кц;
				field[i, 0] := 0X;
				DrawBox(i, 0, 0X)
			кц;
			Invalidate(WMRectangles.MakeRect(FieldOffsetX, FieldOffsetY, FieldOffsetX + Width * BoxSize, FieldOffsetY + y * BoxSize + BoxSize));
			увел(lines);
			timer.Sleep(200);
			oldLevel := level;
			level := lines DIV LinesToLevelRatio;
			если (oldLevel < level) и (delay > 10) то
				points := points + LevelUpBonus;
				умень(delay, delayDec);
				если delayDec >= 10 то delayDec := delayDec DIV 2 всё
			всё;
		кон RemoveLine;

		проц ClearLines;
		перем y, x, c : цел32; linesRemoved : цел32; color : симв8; sameColor : булево;
		нач
			linesRemoved := 0;
			y := Height - 1;
			нцПока y > 0 делай
				sameColor := истина; color := field[0, y];
				c := 0;
				нцДля x := 0 до Width - 1 делай
					если field[x, y] # 0X то
						если (field[x, y] # color) то
							sameColor := ложь;
						всё;
						увел(c);
					всё;
				кц;
				если c = Width то
					RemoveLine(y);
					увел(linesRemoved);
					если sameColor то points := points + SameColorBonus; всё;
				иначе
					умень(y);
				всё;
			кц;
			если (linesRemoved > 0) то
				points := points + linesRemoved;
				если (linesRemoved = 2) то
					points := points + TwoLinesBonus;
				аесли (linesRemoved = 3) то
					points := points + ThreeLinesBonus;
				аесли (linesRemoved = 4) то
					points := points + FourLinesBonus;
				всё;
			всё;
		кон ClearLines;

		проц DropStep;
		перем needNew : булево;
		нач {единолично}
			SetBlock(posX, posY, истина);
			если ~HasDownCollision(posX, posY +1) то увел(posY); needNew := ложь иначе needNew := истина всё;
			SetBlock(posX, posY, ложь);
			Invalidate(WMRectangles.MakeRect(
				FieldOffsetX + posX * BoxSize - BoxSize, FieldOffsetY + posY * BoxSize - BoxSize,
				FieldOffsetX + posX * BoxSize + BlockSize * BoxSize, FieldOffsetY + posY * BoxSize + BlockSize*BoxSize));
			если needNew то
				ClearLines;
				block := nextBlock;
				nextBlock := NewBlock();
				если HasCollision(block, posX, posY) то
					state := Finished;
					WMDialogs.Information("Game Over", "You have lost the game");
				всё;
			всё;
		кон DropStep;

		проц Reset;
		перем x,y : цел32;
		нач
			нцДля x := 0 до Width-1 делай
				нцДля y := 0 до Height-1 делай
					field[x,y] := 0X
				кц
			кц;
			blocks := 0; lines := 0; points := 0; level := 0;
			delay :=150; delayDec := 30;
			Raster.Fill(img, FieldOffsetX, FieldOffsetY, FieldOffsetX + Width*BoxSize, FieldOffsetY + Height*BoxSize, colors[0], mode);
			Invalidate(WMRectangles.MakeRect(FieldOffsetX, FieldOffsetY, FieldOffsetX + Width*BoxSize, FieldOffsetY + Height*BoxSize));
		кон Reset;

		проц {перекрыта}Close*;
		нач
			SetState(Terminating);
			timer.Wakeup;
			AwaitState(Terminated);
			Close^;
			DecCount;
		кон Close;

		проц {перекрыта}Handle*(перем x : WMMessages.Message);
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) и (x.ext суть KillerMsg) то
				Close;
			иначе Handle^(x)
			всё
		кон Handle;

	нач {активное}
		generateNewBlock := истина;
		block := NewBlock();
		nextBlock := NewBlock();
		нц
			DrawInfo;
			нач {единолично} дождись((state = Running) или (state = Restarting) или (state = Terminating)); кон;
			если (state = Terminating) то
				прервиЦикл;
			аесли (state = Restarting) то
				SetState(Running);
				Reset;
				block := NewBlock();
				nextBlock := NewBlock();
			иначе
				если ~dropped то timer.Sleep(delay) всё;
				если RandomDrop то
					просей random.Dice(3) из
						| 0 : если Move(0) то всё;
						| 1 : если Move(1) то всё;
						| 2 : SetBlock(posX, posY, истина);
							 rotBlock := RotateBlock(block);
							 если ~HasCollision(rotBlock, posX, posY) то block := rotBlock всё;
							 SetBlock(posX, posY, ложь);
							 Invalidate(WMRectangles.MakeRect(
							 	FieldOffsetX + posX * BoxSize - BoxSize, FieldOffsetY + posY * BoxSize - BoxSize,
							 	FieldOffsetX + posX * BoxSize + BlockSize * BoxSize, FieldOffsetY + posY * BoxSize + BlockSize * BoxSize));
					всё;
				всё;
				DropStep;
			всё;
		кц;
		SetState(Terminated);
	кон Window;

перем
	nofWindows : цел32;

проц Open*;
перем winstance : Window;
нач
	нов(winstance, истина);
кон Open;

проц IncCount;
нач {единолично}
	увел(nofWindows)
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows)
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WM.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WM.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0);
кон Cleanup;

нач
	Raster.SetRGBA(colors[0], 0, 0, 0, 0);
	Raster.SetRGBA(colors[1], 255, 0, 0, 128);
	Raster.SetRGBA(colors[2], 0, 255, 0, 128);
	Raster.SetRGBA(colors[3], 0, 0, 255, 128);
	Raster.SetRGBA(colors[4], 200, 200, 0, 200);
	Raster.SetRGBA(colors[5], 255, 0, 255, 128);
	Raster.SetRGBA(colors[6], 0, 255, 255, 200);
	Raster.SetRGBA(colors[7], 256, 128, 100, 200);
	Modules.InstallTermHandler(Cleanup)
кон WMTetris.

System.Free WMTetris ~
WMTetris.Open ~
