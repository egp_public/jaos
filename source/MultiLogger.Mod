модуль MultiLogger; (** AUTHOR "TF"; PURPOSE "Log window with a rich-text enabled TextWriter."; *)

использует
	Потоки, TextUtilities,
	WMComponents, WMEditors, WMGraphics, WMStandardComponents,
	WM := WMWindowManager;

тип
	LogWindow* = окласс (WMComponents.FormWindow)
	перем
		panel : WMStandardComponents.Panel;
		out- : WMEditors.Editor;
		tw- : TextUtilities.TextWriter;
		open : булево;

		проц &New*(конст title : массив из симв8; перем stream : Потоки.Писарь);
		нач
			нов(panel); panel.bounds.SetExtents(640, 420); panel.fillColor.Set(WMGraphics.RGBAToColor(255, 255, 255, 255));

			нов(out); out.alignment.Set(WMComponents.AlignClient);
			out.tv.showBorder.Set(истина);
			panel.AddContent(out);

			Init(panel.bounds.GetWidth(), panel.bounds.GetHeight(), ложь);
			SetContent(panel);
			manager := WM.GetDefaultManager();
			SetTitle(WMComponents.NewString(title));
			WM.DefaultAddWindow(сам);
			нов(tw, out.text);
			stream := tw;
			open := истина
		кон New;

		проц {перекрыта}Close*;
		нач
			open := ложь;
			Close^
		кон Close;

	кон LogWindow;

кон MultiLogger.
