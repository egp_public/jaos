модуль FoxIntermediateObjectFile; (** AUTHOR ""; PURPOSE "Intermediate Object File Writer"; *)

использует
	Formats := FoxFormats, Sections := FoxSections, IntermediateCode := FoxIntermediateCode, ObjectFile,
	Files, Strings, Options, Diagnostics, SymbolFileFormat := FoxTextualSymbolFile, Потоки, Basic := FoxBasic,
	SyntaxTree := FoxSyntaxTree,  D := Debugging, Global := FoxGlobal, Parser := FoxIntermediateParser, Commands,  ЛогЯдра, Backend := FoxBackend;

конст
	Trace = ложь;
	DeveloperVersion=истина;
	Version=2;

тип ObjectFileFormat* = окласс (Formats.ObjectFileFormat)
	перем prefix, extension: Files.FileName; textual: булево;

		проц & InitObjectFileFormat*;
		нач
			Init;
			prefix := ""; extension := ".Fil";
		кон InitObjectFileFormat;

		проц ExportModuleTextual(module: Sections.Module; writer: Потоки.Писарь);
		перем
			section: Sections.Section;
			intermediateCodeSection: IntermediateCode.Section;
			i: размерМЗ;
		нач
			(* prepare sections for output *)
			нцДля i := 0 до module.allSections.Length() - 1 делай
				section := module.allSections.GetSection(i);
				утв(section суть IntermediateCode.Section);
				intermediateCodeSection := section(IntermediateCode.Section);
				intermediateCodeSection.SetResolved(НУЛЬ); (* remove generated binary code *)
				intermediateCodeSection.DeleteComments (* remove comments *)
			кц;
			module.Dump(writer)
		кон ExportModuleTextual;

		проц ExportModuleBinary(module: Sections.Module; w: Потоки.Писарь; poolMap: ObjectFile.PoolMap);
		перем
			section: Sections.Section;
			intermediateCodeSection: IntermediateCode.Section;

			проц SectionName(sectionName: ObjectFile.SegmentedName);
			перем name: ObjectFile.SectionName; i,num: размерМЗ;
			нач
				i := 0;
				нцДо
					num := poolMap.Get(sectionName[i]);
					w.пЦел64_7б(num);
					увел(i);
				кцПри (i = длинаМассива(sectionName)) или (num < 0);
			кон SectionName;

			проц WriteOperand(конст operand: IntermediateCode.Operand);

				проц Type(t: IntermediateCode.Type);
				нач
					w.пЦел8_мз(t.form);
					w.пЦел16_мз(t.sizeInBits);
				кон Type;

				проц RegisterClass(c: IntermediateCode.RegisterClass);
				нач
					w.пЦел8_мз(c.class);
					w.пЦел16_мз(c.number);
				кон RegisterClass;

			нач
				Type(operand.type);
				w.пЦел64_7б(operand.mode);
				просей operand.mode из
				IntermediateCode.Undefined:
				|IntermediateCode.ModeMemory:
						если operand.register # IntermediateCode.None то
							w.пЦел64_7б(0);
							w.пЦел64_7б(operand.register);
							w.пЦел64_7б(operand.offset);
						аесли operand.symbol.name # "" то
							w.пЦел64_7б(1);
							SectionName(operand.symbol.name);
							w.пЦел64_7б(operand.symbolOffset);
							w.пЦел64_7б(operand.offset);
						иначе
							w.пЦел64_7б(2);
							w.пЦел64_мз(operand.intValue)
						всё;
				|IntermediateCode.ModeRegister:
					w.пЦел64_7б(operand.register);
					RegisterClass(operand.registerClass);
					w.пЦел64_7б(operand.offset);
				|IntermediateCode.ModeImmediate:
					если operand.symbol.name # "" то
						w.пЦел64_7б(0);
						SectionName(operand.symbol.name);
						w.пЦел64_7б(operand.symbolOffset);
						w.пЦел64_7б(operand.offset);
					иначе
						w.пЦел64_7б(1);
						если operand.type.form в IntermediateCode.Integer то
							w.пЦел64_мз(operand.intValue);
						иначе
							w.пВещ64_мз(operand.floatValue);
						всё;
					всё;
				|IntermediateCode.ModeString:
					w.пЦел64_7б(Strings.Length(operand.string^));
					w.пСтроку8˛включаяСимв0(operand.string^);
				|IntermediateCode.ModeNumber:
					w.пЦел64_мз(operand.intValue);
				всё;

			кон WriteOperand;

			проц WriteInstruction(конст instr: IntermediateCode.Instruction);
			нач
				w.пЦел64_7б(instr.opcode);
				если instr.opcode = IntermediateCode.special то w.пЦел64_7б(instr.subtype) всё;
				WriteOperand(instr.op1);
				WriteOperand(instr.op2);
				WriteOperand(instr.op3);
			кон WriteInstruction;

			проц WriteSection(l0section: IntermediateCode.Section);
			перем i: размерМЗ;
			нач
				w.пЦел32_мз(l0section.type);
				SectionName(l0section.name);
				w.пБулево_мз(l0section.fixed);
				w.пЦел64_7б(l0section.positionOrAlignment);
				w.пЦел64_7б(l0section.fingerprint);
				w.пЦел64_7б(l0section.bitsPerUnit);

				w.пЦел64_7б(l0section.pc);
				нцДля i := 0 до l0section.pc-1 делай
					WriteInstruction(l0section.instructions[i]);
				кц;
			кон WriteSection;

			проц SectionList(list: Sections.SectionList);
			перем l1section: Sections.Section; i: размерМЗ;
			нач
				w.пЦел64_7б(list.Length());
				нцДля i := 0 до list.Length() - 1 делай
					l1section := list.GetSection(i);
					WriteSection(l1section(IntermediateCode.Section));
				кц;
			кон SectionList;

			проц Imports(imports: Sections.NameList);
			перем name: SyntaxTree.СтрокаИдентификатора; i: размерМЗ;
			нач
				w.пЦел64_7б(imports.Length());
				нцДля i := 0 до imports.Length()-1 делай
					name := imports.GetName(i);
					w.пСтроку8˛включаяСимв0(name);
				кц;
			кон Imports;

		нач
			w.пСтроку8˛включаяСимв0(module.moduleName);
			w.пСтроку8˛включаяСимв0(module.platformName);

			Imports(module.imports);

			SectionList(module.allSections);
		кон ExportModuleBinary;

		проц {перекрыта}Export*(module: Formats.GeneratedModule; symbolFileFormat: Formats.SymbolFileFormat): булево;
		перем
			filename: Files.FileName;
			file: Files.File;
			writer: Files.Writer;
			poolMap: ObjectFile.PoolMap;
		нач
			если Trace то D.String(">>> export intermediate object file"); D.Ln всё;

			если ~(module суть Sections.Module) то
				Basic.Error(diagnostics, module.moduleName, Basic.invalidPosition, "generated module format does not match object file format");
				возврат ложь;
			всё;

			если prefix # "" то Files.JoinPath(prefix, module.moduleName, filename); иначе копируйСтрокуДо0 (module.moduleName, filename); всё;
			Files.JoinExtension(filename, extension, filename);

			если Trace то D.String(">>> filename: "); D.String(filename); D.Ln всё;

			file := Files.New(filename);
			если file = НУЛЬ то
				Basic.Error(diagnostics, module.moduleName, Basic.invalidPosition, "failed to open object file for writting");
				возврат ложь
			всё;

			Files.OpenWriter(writer, file, 0);
			если textual то
				WriteHeader(writer, ложь, module(Sections.Module).allSections, poolMap);
				ExportModuleTextual(module(Sections.Module),writer);
			иначе
				WriteHeader(writer, истина, module(Sections.Module).allSections, poolMap);
				ExportModuleBinary(module(Sections.Module),writer, poolMap);
			всё;
			writer.ПротолкниБуферВПоток;
			file.Update;
			Files.Register(file);

			возврат истина
		кон Export;

		проц ImportModuleBinary(r: Потоки.Чтец; module: Sections.Module; system: Global.System; poolMap: ObjectFile.PoolMap): булево;
		перем
			section: Sections.Section;
			name: ObjectFile.SectionName;
			addressType: IntermediateCode.Type;

			проц SectionName(перем sectionName: ObjectFile.SegmentedName);
			перем name: ObjectFile.SectionName; i, num: цел32;
			нач
				i := 0;
				нцДо
					r.чЦел32_7б(num);
					sectionName[i] := poolMap.Get(num);
					увел(i);
				кцПри (i = длинаМассива(sectionName)) или (num < 0);
				нцПока i < длинаМассива(sectionName) делай
					sectionName[i] := -1; увел(i);
				кц;
			кон SectionName;

			проц ReadOperand(перем operand: IntermediateCode.Operand);
			перем type: IntermediateCode.Type; mode, subMode: цел32; register: цел32; registerClass: IntermediateCode.RegisterClass;
				offset: цел32; int: цел64; real: вещ64; l2name: ObjectFile.SegmentedName; symbolOffset: цел32;
				string: Strings.String; len: цел32;
				symbolSection: Sections.Section;

				проц Type(перем t: IntermediateCode.Type);
				перем form: цел8; sizeInBits: цел16;
				нач
					r.чЦел8_мз(form);
					r.чЦел16_мз(sizeInBits);
					IntermediateCode.InitType(t, form, sizeInBits)
				кон Type;

				проц RegisterClass(перем c: IntermediateCode.RegisterClass);
				перем class: цел8; number: цел16;
				нач
					r.чЦел8_мз(class);
					r.чЦел16_мз(number);
					IntermediateCode.InitRegisterClass(c, class, number)
				кон RegisterClass;

			нач
				Type(type);
				IntermediateCode.SetType(operand, type);
				r.чЦел32_7б(mode);
				просей mode из
				IntermediateCode.Undefined:
					IntermediateCode.InitOperand(operand); (* no operand *)
				|IntermediateCode.ModeMemory:
						r.чЦел32_7б(subMode);
						если subMode = 0 то
							r.чЦел32_7б(register);
							r.чЦел32_7б(offset);
							IntermediateCode.InitRegister(operand, addressType, IntermediateCode.GeneralPurposeRegister, register);
						аесли subMode = 1 то
							SectionName(l2name);
							r.чЦел32_7б(symbolOffset);
							r.чЦел32_7б(offset);
							IntermediateCode.InitAddress(operand, addressType, l2name, 0, symbolOffset);
						иначе
							offset := 0;
							утв(subMode = 2);
							r.чЦел64_мз(int);
							IntermediateCode.InitImmediate(operand, addressType, int);
						всё;
						IntermediateCode.InitMemory(operand, type, operand, offset);
				|IntermediateCode.ModeRegister:
					r.чЦел32_7б(register); RegisterClass(registerClass); r.чЦел32_7б(offset);
					IntermediateCode.InitRegister(operand, type, registerClass, register);
					IntermediateCode.AddOffset(operand, offset);
				|IntermediateCode.ModeImmediate:
					r.чЦел32_7б(subMode);
					если subMode = 0 то (* ?? *)
						SectionName(l2name);
						r.чЦел32_7б(symbolOffset);
						r.чЦел32_7б(offset);
						IntermediateCode.InitAddress(operand, type, l2name, 0, symbolOffset);
						IntermediateCode.AddOffset(operand, offset);
					иначе
						утв(subMode = 1);
						если operand.type.form в IntermediateCode.Integer то
							r.чЦел64_мз(int);
							IntermediateCode.InitImmediate(operand, type, int);
						иначе
							r.чВещ64_мз(real);
							IntermediateCode.InitFloatImmediate(operand, type, real);
						всё;
					всё;
				|IntermediateCode.ModeString:
					r.чЦел32_7б(len);
					нов(string, len);
					r.чСтроку8˛включаяСимв0(string^);
					IntermediateCode.InitString(operand, string);
				|IntermediateCode.ModeNumber:
					r.чЦел64_мз(int);
					IntermediateCode.InitNumber(operand, int)
				всё;
			кон ReadOperand;

			проц ReadInstruction(l3section: IntermediateCode.Section);
			перем opcode, subtype: цел32; instruction: IntermediateCode.Instruction; op1, op2, op3: IntermediateCode.Operand;
			нач
				r.чЦел32_7б(opcode);
				если opcode = IntermediateCode.special то r.чЦел32_7б(subtype) всё;

				ReadOperand(op1);
				ReadOperand(op2);
				ReadOperand(op3);
				IntermediateCode.InitInstruction(instruction, Basic.invalidPosition, цел8(opcode), op1, op2, op3);
				IntermediateCode.SetSubType(instruction, цел8(subtype));
				l3section.Emit(instruction);
			кон ReadInstruction;

			проц ReadSection(sectionList: Sections.SectionList);
			перем l4section: IntermediateCode.Section;
				isDefinition: булево;
				l5name: Basic.SegmentedName;
				symbol: SyntaxTree.ОбъявлениеИменованнойСущности;
				comment: булево;
				type: цел32;
				fixed: булево;
				positionOrAlignment, fingerprint, bitsPerUnit: цел32;
				pc,i: цел32;
			нач
				r.чЦел32_мз(type);
				SectionName(l5name);
				r.чБулево_мз(fixed);
				r.чЦел32_7б(positionOrAlignment);
				r.чЦел32_7б(fingerprint);
				r.чЦел32_7б(bitsPerUnit);

				l4section := IntermediateCode.NewSection(sectionList, цел8(type), l5name, НУЛЬ, ложь); (* keeps section if already present *)
				если bitsPerUnit < 0 то (* unknown *)
					если (type = Sections.VarSection) или (type = Sections.ConstSection) то
						bitsPerUnit := system.dataUnit
					иначе
						(*bitsPerUnit := system.codeUnit*)
						(*Unit is already set.*)
					всё;
				всё;
				l4section.SetBitsPerUnit(bitsPerUnit);
				l4section.SetFingerprint(fingerprint);
				l4section.SetPositionOrAlignment(fixed, positionOrAlignment);

				r.чЦел32_7б(pc);
				нцДля i := 0 до pc-1 делай
					ReadInstruction(l4section);
				кц;
			кон ReadSection;

			проц SectionList(list: Sections.SectionList);
			перем section: Sections.Section; length,i: цел32;
			нач
				r.чЦел32_7б(length);
				нцДля i := 0 до length - 1 делай
					ReadSection(list);
				кц;
			кон SectionList;

			проц Imports(imports: Sections.NameList);
			перем l6name: SyntaxTree.СтрокаИдентификатора; length,i: цел32;
			нач
				r.чЦел32_7б(length);
				нцДля i := 0 до length-1 делай
					r.чСтроку8˛включаяСимв0(l6name);
					imports.AddName(l6name);
				кц;
			кон Imports;

		нач
			addressType := IntermediateCode.UnsignedIntegerType(system.addressSize);
			r.чСтроку8˛включаяСимв0(name); module.SetModuleName(name);
			r.чСтроку8˛включаяСимв0(name); module.SetPlatformName(name);
			Imports(module.imports);
			SectionList(module.allSections);
			возврат истина
		кон ImportModuleBinary;

		проц ImportModuleTextual(r: Потоки.Чтец; module: Sections.Module; system: Global.System): булево;
		нач
			возврат Parser.ParseReader(r, diagnostics, module)
		кон ImportModuleTextual;

		проц Import*(конст moduleName: массив из симв8; system: Global.System): Sections.Module;
		перем module: Sections.Module; file: Files.File; reader: Files.Reader; binary: булево; filename: Files.FileName; poolMap: ObjectFile.PoolMap;
		нач
			если prefix # "" то Files.JoinPath(prefix, moduleName, filename); иначе копируйСтрокуДо0 (moduleName, filename); всё;
			Files.JoinExtension(filename, extension, filename);

			file := Files.Old(filename);
			если file = НУЛЬ то возврат НУЛЬ всё;
			нов(reader, file, 0);
			ReadHeader(reader, binary, poolMap);
			нов(module, НУЛЬ, system);
			если binary и ImportModuleBinary(reader, module, system, poolMap) или  ~binary и ImportModuleTextual(reader, module, system) то
				возврат module
			иначе
				возврат НУЛЬ
			всё;
		кон Import;

		проц {перекрыта}DefineOptions* (options: Options.Options);
		нач
			options.Add(0X,"objectFileExtension",Options.String);
			options.Add(0X,"objectFilePrefix",Options.String);
			options.Add(0X,"textualObjectFile",Options.Flag);
		кон DefineOptions;

		проц {перекрыта}GetOptions* (options: Options.Options);
		нач
			если ~options.GetString("objectFileExtension",extension) то extension := "Fil"; всё;
			если ~options.GetString("objectFilePrefix",prefix) то prefix := ""; всё;
			textual := options.GetFlag("textualObjectFile");
		кон GetOptions;

		проц {перекрыта}DefaultSymbolFileFormat*(): Formats.SymbolFileFormat;
		нач возврат SymbolFileFormat.Get();
		кон DefaultSymbolFileFormat;

		проц {перекрыта}GetExtension*(перем ext: массив из симв8);
		нач копируйСтрокуДо0(extension, ext)
		кон GetExtension;

		проц {перекрыта}SetExtension*(конст ext: массив из симв8);
		нач копируйСтрокуДо0(ext, extension)
		кон SetExtension;

	кон ObjectFileFormat;

	проц Get*(): Formats.ObjectFileFormat;
	перем intermediateObjectFileFormat: ObjectFileFormat;
	нач нов(intermediateObjectFileFormat); возврат intermediateObjectFileFormat
	кон Get;

	проц ReadHeader(reader: Потоки.Чтец; перем binary: булево; перем poolMap: ObjectFile.PoolMap);
	перем ch: симв8; version: цел32; string: массив 32 из симв8; i,j,pos,size: цел32; name: ObjectFile.SectionName;
	нач
		reader.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string);
		binary := string="FoxILB";
		если ~binary то утв(string="FoxILT") всё;
		reader.ПропустиБелоеПоле;
		reader.чСимв8(ch); утв(ch='v');
		reader.чЦел32(version,ложь);
		если version < Version то ЛогЯдра.пСтроку8("warning: old object file encountered"); ЛогЯдра.пВК_ПС всё;
		reader.чСимв8(ch); утв(ch='.');
		если ~binary то reader.ПропустиБелоеПоле
		иначе
			нов(poolMap, 64);
			poolMap.Read(reader);
		всё;
	кон ReadHeader;

	проц WriteHeader(writer: Потоки.Писарь; binary: булево; sections: Sections.SectionList; перем poolMap: ObjectFile.PoolMap);
	перем i: размерМЗ; section: Sections.Section; fixups: цел32; fixupList: ObjectFile.Fixups;

		проц ProcessOperand(конст operand: IntermediateCode.Operand);
		нач
			если operand.symbol.name # "" то
				poolMap.PutSegmentedName(operand.symbol.name)
			всё;
		кон ProcessOperand;

		проц ProcessInstruction(конст instruction: IntermediateCode.Instruction);
		нач
			ProcessOperand(instruction.op1);
			ProcessOperand(instruction.op2);
			ProcessOperand(instruction.op3);
		кон ProcessInstruction;

		проц ProcessSection(l7section: IntermediateCode.Section);
		перем l8i: размерМЗ;
		нач
			если l7section.resolved # НУЛЬ то
				poolMap.PutSegmentedName(l7section.name);
				нцДля l8i := 0 до l7section.pc-1 делай
					ProcessInstruction(l7section.instructions[l8i]);
				кц;
			всё;
		кон ProcessSection;

	нач
		если binary то writer.пСтроку8("FoxILB");
		иначе writer.пСтроку8("FoxILT");
		всё;
		writer.пСимв8(' ');
		writer.пСимв8('v'); writer.пЦел64(Version,0); writer.пСимв8(".");
		если ~binary то writer.пВК_ПС
		иначе
			нов(poolMap,512);
			poolMap.BeginWriting(writer);
			нцДля i := 0 до sections.Length()-1 делай
				section := sections.GetSection(i);
				ProcessSection(section(IntermediateCode.Section));
			кц;
			poolMap.EndWriting;
		всё;
	кон WriteHeader;

	(* test code to display --not public *)
	проц Show*(context: Commands.Context);
	перем
		fileName: Files.FileName; file: Files.File; reader: Files.Reader; writer: Потоки.Писарь;
		section: ObjectFile.Section; binary: булево; poolMap, poolMapDummy: ObjectFile.PoolMap;
		objectFile: ObjectFileFormat; module: Sections.Module; backend: Backend.Backend;
		extension: Files.FileName;
	нач
		если DeveloperVersion то
			если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fileName) то
				backend := Backend.GetBackendByName("TRM");
				Files.SplitExtension(fileName, fileName, extension);
				нов(objectFile);
				если extension # "" то objectFile.SetExtension(extension) всё;
				module := objectFile.Import(fileName, backend.GetSystem());
				writer := Basic.GetWriter(Basic.GetDebugWriter(fileName));
				objectFile.ExportModuleTextual(module, writer);
				writer.ПротолкниБуферВПоток;
			иначе
				context.error.пСтроку8("no file specificed"); context.error.пВК_ПС
			всё;
		иначе СТОП(200)
		всё;
	кон Show;

кон FoxIntermediateObjectFile.

System.FreeDownTo FoxIntermediateObjectFile ~
FoxIntermediateObjectFile.Show Builtins  ~

			(* test code to compare ..
			backend: Backend.Backend;
			IF prefix # "" THEN Files.JoinPath(prefix, module.moduleName, filename); ELSE COPY (module.moduleName, filename); END;
			Files.JoinExtension(filename, "fil2", filename);
			file := Files.New(filename);
			backend := Backend.GetBackendByName("TRM");
			Files.OpenWriter(writer, file, 0);
			module := Import(module.moduleName, backend.GetSystem());
			ExportModuleTextual(module(Sections.Module), writer);
			writer.Update;
			Files.Register(file);
			*)
