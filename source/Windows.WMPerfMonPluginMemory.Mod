модуль WMPerfMonPluginMemory; (** AUTHOR "staubesv"; PURPOSE "Performance Monitor memory utilization plugin"; *)

использует
	Kernel32, Modules, WMPerfMonPlugins, Heaps;

конст
	ModuleName = "WMPerfMonPluginMemory";

тип

	(* Heaps.GetHeapInfo is a slow operation. HeapHelper provides its results to multiple plugins *)
	HeapHelper = окласс(WMPerfMonPlugins.Helper)
	перем
		free, total, largest : размерМЗ;

		проц {перекрыта}Update*;
		нач
			Heaps.GetHeapInfo(total, free, largest);
		кон Update;

	кон HeapHelper;

тип

	MemoryLoad* = окласс(WMPerfMonPlugins.Plugin)
	перем
		h : HeapHelper;

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		перем ds : WMPerfMonPlugins.DatasetDescriptor;
		нач
			p.name := "Heap"; p.description := "Heap statistics"; p.modulename := ModuleName;
			p.autoMax := истина; p.unit := "KB"; p.minDigits := 7;
			p.noSuperSampling := истина;
			p.helper := heapHelper; h := heapHelper;
			нов(ds, 3);
			ds[0].name := "Size"; включиВоМнвоНаБитах(ds[0].flags, WMPerfMonPlugins.Maximum);
			ds[1].name := "Free";
			ds[2].name := "LargestBlock"; включиВоМнвоНаБитах(ds[2].flags, WMPerfMonPlugins.Standalone);
			p.datasetDescriptor := ds;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		нач
			dataset[0] := h.total DIV 1024;
			dataset[1] := h.free DIV 1024;
			dataset[2] := h.largest DIV 1024;
		кон UpdateDataset;

	кон MemoryLoad;

тип

	WindowsMemoryLoad = окласс(WMPerfMonPlugins.Plugin)
	перем
		status : Kernel32.MemoryStatusEx;

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		перем ds : WMPerfMonPlugins.DatasetDescriptor;
		нач
			p.name := "Memory"; p.description := "Windows Memory Statistics"; p.modulename := ModuleName;
			p.noSuperSampling := истина;
			p.autoMax := истина; p.unit := "KB"; p.minDigits := 7;
			нов(ds, 7);
			ds[0].name := "Memory Load [%]";
			ds[1].name := "Total (Physical)";
			ds[2].name := "Free (Physical)";
			ds[3].name := "Total (Page file)";
			ds[4].name := "Free (Page file)";
			ds[5].name := "Total (Virtual)";
			ds[6].name := "Free (Virtual)";
			p.datasetDescriptor := ds;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		нач
			status.dwLength := 64;
			если (Kernel32.GlobalMemoryStatusEx(status) = Kernel32.True) то
				dataset[0] := status.dwMemoryLoad;
				dataset[1] := status.ullTotalPhys DIV 1024;
				dataset[2] := status.ullAvailPhys DIV 1024;
				dataset[3] := status.ullTotalPageFile DIV 1024;
				dataset[4] := status.ullAvailPageFile DIV 1024;
				dataset[5] := status.ullTotalVirtual DIV 1024;
				dataset[6] := status.ullAvailVirtual DIV 1024;
			всё;
		кон UpdateDataset;

	кон WindowsMemoryLoad;

перем
	heapHelper : HeapHelper;

проц InitPlugins;
перем
	par : WMPerfMonPlugins.Parameter;
	ml : MemoryLoad;
	wml : WindowsMemoryLoad;
нач
	нов(par); нов(ml, par);
	нов(par); нов(wml, par);
кон InitPlugins;

проц Install*;
кон Install;

проц Cleanup;
нач
	WMPerfMonPlugins.updater.RemoveByModuleName(ModuleName);
кон Cleanup;

нач
	нов(heapHelper);
	InitPlugins;
	Modules.InstallTermHandler(Cleanup);
кон WMPerfMonPluginMemory.

WMPerfMonPluginMemory.Install ~	System.Free WMPerfMonPluginMemory ~
