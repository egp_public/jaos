(* Aos, Copyright 2001, Pieter Muller, ETH Zurich; AUTHOR "pjm"; (С) Денис Будяк 2021*)

модуль Pipes; (* Трубы (тж известны как пайпы (pipe) и каналы. Что запишешь в трубу, то из неё можно и прочитать *)

конст
	Ok = 0;
	Closed = 4201;

тип
(**  Труба - это двусторонний канал ввода-вывода с буфером внутри. Что 
  в него записали, то же из него можно и прочитать *)
	Pipe* = окласс
		перем
			head, колво: размерМЗ;
			buffer: укль на массив из симв8;
			closed: булево;
			трассировать*: булево; (* трассировать весь обмен. 
				Может быть, нужен отдельный тип? Плюс к тому, открытость этого поля - УЯЗВИМОСТЬ *)

		(** возвращает к-во байт, к-рое может быть прочитано без блокировки *)
		проц Available*() : размерМЗ;
		нач
			если трассировать то
				трассируй(колво) всё;
			возврат колво кон Available;

		проц Send*(конст buf: массив из симв8; ofs, len: размерМЗ; propagate : булево; перем res: целМЗ);
		нач
			утв(len >= 0);
			если трассировать и (len # 0) то
				трассируй(len) всё;
			нач {единолично}
				нц
					если len = 0 то res := Ok; прервиЦикл всё;
					дождись((колво < длинаМассива(buffer)) или closed);
					если closed то
						res := Closed; прервиЦикл
					аесли колво < длинаМассива(buffer) то
						buffer[(head+колво) остОтДеленияНа длинаМассива(buffer)] := buf[ofs];
						если трассировать то
							трассируй(buf[ofs]) всё;
						увел(ofs); увел(колво); умень(len)
					всё
				кц;
			если трассировать и (len # 0) то
				трассируй(res) всё;
			кон
		кон Send;

		проц Receive*(перем buf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);
		нач
			утв((size > 0) и (min <= size) и (min >= 0));
			len := 0;
			нач {единолично}
				нц
					если min > 0 то дождись((колво > 0) или closed) всё;	(* wait until some data available *)
					если колво > 0 то
						buf[ofs] := buffer[head];
						если трассировать то
							трассируй(buf[ofs]) всё;
						head := (head + 1) остОтДеленияНа длинаМассива(buffer);
						умень(колво); увел(ofs); увел(len); умень(min); умень(size)
					аесли closed то
						res := Closed; прервиЦикл
					всё;
					если (колво = 0) и (min <= 0) или (size = 0) то res := Ok; прервиЦикл всё
				кц;
				если трассировать и (len # 0) то
					трассируй(len) всё;
			кон
		кон Receive;

		проц &Init*(size: размерМЗ);
		нач
			head := 0; колво := 0;
			если (buffer = НУЛЬ) или (длинаМассива(buffer) # size) то
				нов( buffer, size );
			всё;
			closed := ложь
		кон Init;

		проц Close*;
		нач {единолично}
			closed := истина
		кон Close;

	кон Pipe;

(** Any stream input procedure or method. Identical to the definition in Streams.Mod, but I don't want to import that here*)
тип	Receiver* = проц {делегат} ( перем buf: массив из симв8;  ofs, size, min: размерМЗ;  перем len: размерМЗ; перем res: целМЗ );
(*makes a decoupled copy of a receiver*)
(*internally it's a pipe that automatically reads the input in a continuous loop*)
тип ReadDecoupler*= окласс
	перем
		pipe: Pipe;
		inputReceiver: Receiver;
		smallbuf: массив[128] из симв8;
		len: размерМЗ;
		res: целМЗ;

	проц &InitReadDecoupler*(inputReceiver: Receiver);
	нач
		нов(pipe,128);
		сам.inputReceiver:=inputReceiver;
	кон InitReadDecoupler;

	проц Receive*(перем buf: массив из симв8; ofs, size, min: размерМЗ; перем len: размерМЗ; перем res: целМЗ);
	нач
		pipe.Receive(buf,ofs,size,min,len,res);
	кон Receive;

нач{активное}

		нц
			(*todo: check res, if inputReceiver throws anything but 'OK' exit the loop*)
			inputReceiver(smallbuf, 0, 128, 1, len, res); (*read at least 1 byte into the small buffer*)
			pipe.Send(smallbuf, 0, len, ложь, res);   (*feed the amount read (usually 1 entire line) into the pipe to cross threads*)
		кц;
кон ReadDecoupler;

кон Pipes.
