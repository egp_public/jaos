модуль Gpio; (** AUTHOR "Timothée Martiel"; PURPOSE "GPIO driver"; *)

(*! TODO:
	- Interrupt configuration
*)

использует Platform;

конст
	(** Direction: Input *)
	Input *		= ложь;
	(** Direction: Output *)
	Output *	= истина;

тип
	DataReg = укль {опасныйДоступКПамяти,неОтслСборщиком} на запись
		DATA: массив 4 из мнвоНаБитахМЗ;
		DATA_RO: массив 4 из мнвоНаБитахМЗ;
	кон;

	BankCtrlReg = укль {опасныйДоступКПамяти,неОтслСборщиком} на запись
		DIRM, OEN, INT_MASK, INT_EN, INT_DIS, INT_STAT, INT_TYPE, INT_POLARITY, INT_ANY: мнвоНаБитахМЗ;
	кон;

перем
	bankCtrlRegs: массив 4 из BankCtrlReg;
	dataRegs: DataReg;
	i: цел32;

	(** Set the direction of 'gpio' to output if 'out' is TRUE and to input otherwise. *)
	проц SetDirection * (gpio: цел32; out: булево);
	перем
		bank, ofs: цел32;
	нач
		GetBankOfs(gpio, bank, ofs);
		если (bank < 0) и (ofs < 0) то возврат всё;

		если out то
			включиВоМнвоНаБитах(bankCtrlRegs[bank].DIRM, ofs)
		иначе
			исключиИзМнваНаБитах(bankCtrlRegs[bank].DIRM, ofs)
		всё
	кон SetDirection;

	(** If 'on' enable (if 'off' disable) output for 'gpio'. 'gpio' direction must be set to output prior to this call. *)
	проц EnableOutput * (gpio: цел32; on: булево);
	перем
		bank, ofs: цел32;
	нач
		GetBankOfs(gpio, bank, ofs);
		если (bank < 0) и (ofs < 0) то возврат всё;

		если on то
			включиВоМнвоНаБитах(bankCtrlRegs[bank].OEN, ofs)
		иначе
			исключиИзМнваНаБитах(bankCtrlRegs[bank].OEN, ofs)
		всё
	кон EnableOutput;

	(** Set the data of output GPIO 'gpio' to 'data' (TRUE for high, FALSE for low). *)
	проц SetData * (gpio: цел32; data: булево);
	перем
		bank, ofs: цел32;
	нач
		GetBankOfs(gpio, bank, ofs);
		если (bank < 0) и (ofs < 0) то возврат всё;

		если data то
			включиВоМнвоНаБитах(dataRegs.DATA[bank], ofs)
		иначе
			исключиИзМнваНаБитах(dataRegs.DATA[bank], ofs)
		всё
	кон SetData;

	(** Get data of input GPIO 'gpio': TRUE for high, FALSE for low. *)
	проц GetData * (gpio: цел32): булево;
	перем
		bank, ofs: цел32;
	нач
		GetBankOfs(gpio, bank, ofs);
		если (bank < 0) и (ofs < 0) то СТОП(100) всё;

		возврат ofs в dataRegs.DATA_RO[bank]
	кон GetData;

	проц GetBankOfs (gpio: цел32; перем bank, ofs: цел32);
	нач
		если gpio < 54 то
			bank := gpio DIV 32;
			ofs := gpio остОтДеленияНа 32
		иначе
			(*! TODO: implement *)
		всё;
		bank := -1;
		ofs := -1
	кон GetBankOfs;

нач
	dataRegs := Platform.GpioData;
	нцДля i := 0 до Platform.GpioBankNb - 1 делай
		bankCtrlRegs[i] := Platform.GpioBank[i]
	кц
кон Gpio.
