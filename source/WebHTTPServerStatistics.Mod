модуль WebHTTPServerStatistics; (** AUTHOR "staubesv"; PURPOSE "Simple WebHTTP server link hit counter"; *)
(* No tricks variant. Much room for optimizations. *)

использует
	ЭВМ, Modules, Kernel, Потоки, Commands, WebHTTP, WebHTTPServer;

тип

	Link = окласс
	перем
		host : массив 256 из симв8;
		uri : массив 1024 из симв8;
		hits : цел32;
		next : Link;

		проц Hit;
		нач
			ЭВМ.атомарноУвел(hits);
		кон Hit;

		проц ToStream(out : Потоки.Писарь);
		нач {единолично}
			утв(out # НУЛЬ);
			out.пСтроку8(host); out.пСтроку8(uri); out.пСтроку8(": "); out.пЦел64(hits, 0); out.пВК_ПС;
		кон ToStream;

		проц &Init*(конст host, uri : массив из симв8);
		нач
			копируйСтрокуДо0(host, сам.host);
			копируйСтрокуДо0(uri, сам.uri);
			hits := 0;
			next := НУЛЬ;
		кон Init;

	кон Link;

перем
	links : Link;

(** Note: Don't take any locks here!! Unoptimized!! *)
проц Listener(request : WebHTTP.RequestHeader; response : WebHTTP.ResponseHeader);
перем link : Link;
нач
	link := links.next;
	нцПока (link # НУЛЬ) делай
		если (request.host = link.host) и (request.uri = link.uri) то link.Hit; всё;
		link := link.next;
	кц;
кон Listener;

проц AddURI*(context : Commands.Context); (* host uri ~ *)
перем
	host : массив 256 из симв8; uri : массив 1024 из симв8;
	link : Link;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(host) и context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(uri) то
		нов(link, host, uri);
		нач {единолично}
			link.next := links.next;
			links.next := link;
		кон;
		context.out.пСтроку8("Added "); context.out.пСтроку8(host); context.out.пСтроку8(uri); context.out.пВК_ПС;
	иначе
		context.error.пСтроку8("Expected host and uri parameters"); context.error.пВК_ПС;
	всё;
кон AddURI;

(** Show hit statistics *)
проц Show*(context : Commands.Context);
перем link : Link;
нач
	context.out.пСтроку8("Hit statistics: "); context.out.пВК_ПС;
	если (links.next # НУЛЬ) то
		link := links.next;
		нцПока (link # НУЛЬ) делай
			link.ToStream(context.out); context.out.ПротолкниБуферВПоток;
			link := link.next;
		кц;
	иначе
		context.out.пСтроку8("No URIs registered"); context.out.пВК_ПС;
	всё;
кон Show;

проц Cleanup;
перем timer : Kernel.Timer;
нач
	WebHTTPServer.listener := НУЛЬ;
	нов(timer); timer.Sleep(500);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	нов(links, "Head of List", "");
	WebHTTPServer.listener := Listener;
кон WebHTTPServerStatistics.

WebHTTPServerStatistics.AddURI powerbottle.inf.ethz.ch /index.html~

WebHTTPServerStatistics.Show ~

System.Free WebHTTPServerStatistics ~
