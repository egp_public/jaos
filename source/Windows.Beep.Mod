(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Beep;   (** AUTHOR "pjm"; PURPOSE "PC speaker driver"; *)

использует Kernel32, Kernel(* , KernelLog*) ;

конст

	(** Sound the PC speaker continuously at the specified frequency.  Use 0 to switch off the sound.  Not sharable. *)
тип
	Beeper = окласс
	перем hz: целМЗ;
		beep: булево;

		проц & Init*;
		нач
			(*KernelLog.String("Beep:Beeper.Init"); KernelLog.Ln;*)
			beep := ложь;
		кон Init;

		проц On( hz: целМЗ );
		нач {единолично}
			сам.hz := hz;  beep := истина;
		кон On;

		проц Off;
		нач {единолично}
			beep := ложь;
		кон Off;

		проц Beep;
		перем retBOOL: Kernel32.BOOL;								(* Dan 09.11.05 *)
		нач
			(*KernelLog.String("Beeper.Beep"); KernelLog.Ln;  *)
			нц
				нцПока (beep) делай  retBOOL :=Kernel32.Beep( hz, 50 );  кц;
				нач {единолично}
					дождись( beep );
				кон;
			кц;
		кон Beep;

	нач {активное}
					(*KernelLog.String("Beep:Beeper.Active"); KernelLog.Ln; *)  Beep();
	кон Beeper;

перем
	beep: Beeper;

	проц Beep*( hz: целМЗ );
	перем s: мнвоНаБитахМЗ;
	нач
		если (hz # 0) то
			если beep = НУЛЬ то нов( beep ) всё;
			beep.On( hz )
		иначе
			если beep # НУЛЬ то beep.Off();  всё;
		всё;
	кон Beep;

	проц Test*;
	перем timer: Kernel.Timer;
	нач
		нов(timer);
		Beep( 2000 ); timer.Sleep(1000); Beep(0);
	кон Test;

	проц TestOn*;
	нач
		Beep( 2000 );
	кон TestOn;

	проц TestOff*;
	нач
		Beep( 0 );
	кон TestOff;

кон Beep.

System.Free Beep ~

Beep.TimerTest
Beep.Test
Beep.TestOn
Beep.TestOff
~


