(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Objects;   (** AUTHOR "pjm, G.F."; PURPOSE "Active object runtime support"; *)


использует НИЗКОУР, Трассировка, Glue, Unix, ЭВМ, Heaps, Modules;

конст
	ConcurrentGC = истина; (* if TRUE then release needs to be compiled with --writeBarriers *)
	LinuxHost = Unix.Version = "Linux";

	(*! Process flags, meaningless in Unix ports !!! *)
	PleaseHalt* = 10;		(* Process requested to Halt itself soon *)
	Unbreakable*= 11;		(* FINALLY shall not catch HALT exception (PleaseHalt is also set) *)
	SelfTermination*=12;	(* Indicates the process has requested to terminate ifself (PleaseHalt is also set) *)
	Preempted* = 27;		(* Has been preempted. *)
	Resistant* = 28;		(* Can only be destroyed by itself *)


	MinPriority*	= Unix.ThreadLow;
	Low*			= Unix.ThreadLow + 1;
	Normal*		= Unix.ThreadNormal;
	High*			= Unix.ThreadHigh - 2;
	GCPriority*	= Unix.ThreadHigh - 1;
	Realtime*	= Unix.ThreadHigh;

	(* Process flag defined by compiler in OPC.CallRecBody *)
	Restart* = 0;	(* Restart/Destroy process on exception *)

	(* Process modes (in UnixAos Running means Running or Ready!) *)
	Unknown* = 0;  Ready* = 1;  Running* = 2;  AwaitingLock* = 3;
	AwaitingCond* = 4;  AwaitingEvent* = 5;  Terminated* = 6;

	Second* = 1000;	(* frequency of ticks increments in Hz *)

	DefaultStacksize = 128*1024;


перем
	(* timer *)
	timerActivity		: TimerActivity;
	clock				: Clock;
	timers				: Timer;
	timerListMutex	: Unix.Mutex_t;

	timerStopped: булево;

	(* processes *)
	root-	: Process;	(*!  Anchor of all instantiated threads in system *)
	stacksize: размерМЗ;		(* stack size of active objects, adjustable via boot parameter *)

	processList		: Unix.Mutex_t;
	createProcess	: Unix.Mutex_t;
	startProcess		: Unix.Mutex_t;
	startEventLock	: Unix.Mutex_t;
	lockMutex		: Unix.Mutex_t;
	childrunning		: Unix.Condition_t;

	processPointer	: Unix.Key_t;

	newProcess: Process;
	nextPID: целМЗ;

	finalizerCaller	: FinalizerCaller;
	mainthread: Unix.Thread_t;

	startedMainProcess := ложь: булево;

тип

	LockT= укль на запись
		mtx, enter: адресВПамяти;
	кон;

	CpuCyclesArray* = массив ЭВМ.МаксКвоПроцессоров из цел64;

	ProtectedObject = укль на запись кон;

	ObjectHeader = Heaps.ProtRecBlock;

	ProcessQueue = Heaps.ProcessQueue;

	EventHandler* = проц  {делегат};



	Timer* =  окласс
	перем
		next: Timer;
		trigger: цел32;
		handler: EventHandler
	кон Timer;

	TimerActivity = окласс
	перем
		t, r: Timer;  h: EventHandler;  restart: булево;

		проц UpdateTicks;
		нач {единолично}
			ЭВМ.UpdateTicks
		кон UpdateTicks;

		проц Restart;
		нач {единолично}
			restart := истина
		кон Restart;

	нач {активное, SAFE, приоритет(High)}
		restart := ложь;
		нц
			t := timers;
			если t # НУЛЬ то
				h := НУЛЬ;  r := НУЛЬ;
				нач {единолично}
					дождись( (ЭВМ.ticks >= t.trigger) или restart );  restart := ложь;
					если ЭВМ.ticks >= t.trigger то
						h := t.handler;  r := t
					всё
				кон;
				если r # НУЛЬ то  Remove( r )  всё;
				если h # НУЛЬ то  (* not canceled *) h всё
			иначе
				нач{единолично}
					дождись( restart );  restart := ложь;
				кон
			всё
		кц
	кон TimerActivity;

	Clock* = окласс
	нач {активное}
		нц
			Unix.ThrSleep( 10 );
			если ~timerStopped то  timerActivity.UpdateTicks  всё
		кц;
	кон Clock;

	FinalizerCaller = окласс	(* separate active object that calls finalizers *)
	перем
		n: Heaps.FinalizerNode;  start: булево;

		проц Activate;
		нач {единолично}
			start := истина
		кон Activate;

	нач {активное, SAFE, приоритет(High)}
		start := ложь;
		нц
			нач {единолично} дождись( start ) кон;
			start := ложь;
			нц
				n := Heaps.GetFinalizer();
				если n = НУЛЬ то прервиЦикл всё;
				если n.collection # НУЛЬ то
					n.collection.RemoveAll(n.objStrong)	(* remove it if it is not removed yet *)
				всё;
				если n.finalizer # НУЛЬ то
					n.finalizer(n.objStrong)	(* may acquire locks *)
				всё;
			кц;
		кц
	кон FinalizerCaller;



	Body = проц ( self: ProtectedObject );
	Condition = проц ( slink: адресВПамяти ): булево;

	GCContext = запись
		nextPos: размерМЗ; (* 0 to start with *)
		last: массив 32 из запись bp, pc, sp: адресВПамяти кон;

		проц AddContext(bp, pc, sp: адресВПамяти);
		нач
			last[nextPos].bp := bp;
			last[nextPos].pc := pc;
			last[nextPos].sp := sp;
			увел(nextPos);
		кон AddContext;

		проц GetNextContext(перем pos: размерМЗ; перем bp, pc, sp: адресВПамяти): булево;
		нач
			если pos < 1 то возврат ложь всё;
			умень(pos);
			bp := last[pos].bp;
			pc := last[pos].pc;
			sp := last[pos].sp;
			возврат истина;
		кон GetNextContext;

		проц RemoveContext();
		нач
			если nextPos > 0 то
				умень(nextPos);
				last[nextPos].bp := НУЛЬ;
				last[nextPos].pc := НУЛЬ;
				last[nextPos].sp := НУЛЬ;
			всё;
		кон RemoveContext;
	кон;

	Process* = окласс (Heaps.ProcessLink)
	перем
		threadId-			: Unix.Thread_t;
		nextProcess-	: Process;	(* next in list of all processes *)
		stackBottom	-	: адресВПамяти;
		id-				: целМЗ;
		body			: Body;
		mode-		: целМЗ;
		flags-			: мнвоНаБитахМЗ;
		priority-		: целМЗ;	(* only effective if Aos is running SUID root *)
		succ			: Process;   		  	(* in ProcessQueue *)
		obj-			: ProtectedObject;	(* associated active object *)
		condition-	: Condition;   			(* awaited process' condition *)
		condFP-		: адресВПамяти;			(* awaited process' condition's context *)
		continue		: Unix.Condition_t;	(* gets signaled when condition yields true *)
		waitingOn-	: ProtectedObject;
		procID-		: цел32;				(*! processor ID where running, not used in UnixAos *)
		state-			: ЭВМ.СостояниеПроцессора;
		state0	: массив 2048 из симв8;		(* thread state at body start, used for restart after trap *)
		context: динамическиТипизированныйУкль; (* commands context *)
		lastThreadTimes: цел64;
		gcContext: GCContext;

		проц SetMode (mode: целМЗ);
		нач
				ЭВМ.ЗапросиБлокировку(ЭВМ.ЯдернаяБлокировкаДляРаботыСКучей);(* process does not silently stop or run when GC is active *)
				сам.mode := mode;
				ЭВМ.ОтпустиБлокировку(ЭВМ.ЯдернаяБлокировкаДляРаботыСКучей);
		кон SetMode;

		проц {перекрыта}FindRoots;
		перем sp, bp, sb, n, a0, a1, adr, pc: адресВПамяти; desc {неОтслСборщиком}: Modules.ProcedureDescPointer; i: размерМЗ; p {неОтслСборщиком}: динамическиТипизированныйУкль;
			contextPos: размерМЗ; suspended: булево;
		нач {UNTRACKED}
			если (mode < Running) или (mode = Terminated) то  возврат  всё;
			suspended := ложь;
			если сам = CurrentProcess() то
				state.SP := НИЗКОУР.GetStackPointer( );
				state.BP := НИЗКОУР.GetFramePointer( );
				state.PC := ЭВМ.ТекСчётчикКоманд();
			аесли ConcurrentGC то
				suspended := истина;
				Suspend(сам);
			всё;
			sb := stackBottom;
			contextPos := сравнениеСОбменом(gcContext.nextPos,0,0);
			sp := state.SP; bp := state.BP; pc := state.PC;  (* <---- might all be invalid, when A2 was left ! *)

			если TraceProcessHook # НУЛЬ то (* stack trace *)
				TraceProcessHook(сам, pc, bp, sp, sb);
			всё;

			если contextPos >= 0 то (* this thread is currently active in A2 *)
				если Heaps.GCType = Heaps.HeuristicStackInspectionGC то
					(* debugging info*)
					если (stackBottom # 0) и (bp <= stackBottom) и (bp >= sp - размерОт адресВПамяти) то
						НИЗКОУР.прочтиОбъектПоАдресу( bp, n );
						если нечётноеЛи¿( n ) то (* procedure descriptor at bp *)
							если n > 1024 то
							desc := НИЗКОУР.подмениТипЗначения( Modules.ProcedureDescPointer, n-1 ); (* tracing !*)
							всё;
						всё;
					всё;
					если (stackBottom # 0) и (sp # 0) и (sp <= stackBottom) то
						Heaps.RegisterCandidates( sp, stackBottom - sp );
					всё;
				аесли Heaps.GCType = Heaps.MetaDataForStackGC то
					нц
						если (bp <= stackBottom) и (bp >= sp) то
						аесли ЭВМ.АдресНаКучеЛи¿(pc) и (bp # НУЛЬ) то (* bp = NIL can happen when code has no procedure activation frame *)
							(* ok, valid stack frame from A2, we can trace this *)
						аесли gcContext.GetNextContext(contextPos, bp, pc, sp) то
							если TraceProcessHook # НУЛЬ то (* stack trace *)
								TraceProcessHook(сам, pc, bp, sp-128, sb);
							всё;
						иначе
							прервиЦикл
						всё;
						если ЭВМ.АдресНаКучеЛи¿(pc) и (bp > sp) то (* in A2 *)
							НИЗКОУР.прочтиОбъектПоАдресу( bp-размер16_от(адресВПамяти), desc );
							если desc # НУЛЬ то
								a0 := адресОт( desc.offsets );
								a1 :=  НИЗКОУР.подмениТипЗначения( адресВПамяти, desc.offsets );
								утв( a0+размер16_от( адресВПамяти ) = a1, 54321 );
								нцДля i := 0 до длинаМассива( desc.offsets ) - 1 делай
									adr := bp + desc.offsets[i]; (* pointer at offset *)
									НИЗКОУР.прочтиОбъектПоАдресу( adr, p ); (* load pointer *)
									если p # НУЛЬ то
										Heaps.Mark( p );
									всё
								кц
							всё;
						всё;
						НИЗКОУР.прочтиОбъектПоАдресу(bp + размер16_от(адресВПамяти), pc);
						НИЗКОУР.прочтиОбъектПоАдресу(bp, bp);
					кц; (* LOOP *)
				всё; (* gctype *)
			всё; (* contextpos *)
			если ConcurrentGC то
				если suspended то
					Resume(сам);
				всё;
			всё;
		кон FindRoots;

		проц Cancel;
		перем pt, t: Process;  kt: Unix.Thread_t;
		нач
			если сам = CurrentProcess() то  Exit
			иначе
				ЭВМ.ЗапросиБлокировку( ЭВМ.X11 );  (* let the thread to be killed first finish its last I/O, if any *)
				Unix.MtxLock( processList );
					pt := НУЛЬ; t := root;  kt := 0;
					нцПока (t # НУЛЬ ) и (t # сам) делай  pt := t;  t := t.nextProcess  кц;
					если t = сам то
						kt := threadId;
						если pt = НУЛЬ то  root := t.nextProcess  иначе  pt.nextProcess := t.nextProcess  всё;
					всё;
				Unix.MtxUnlock( processList );
				если kt # 0 то  Unix.ThrKill( kt )  всё;
				ЭВМ.ОтпустиБлокировку( ЭВМ.X11 );
			всё
		кон Cancel;

		проц GetPriority( ): целМЗ;
		нач
			возврат Unix.ThrGetPriority( threadId )
		кон GetPriority;

		проц SetPriority( prio: целМЗ );
		перем pr: целМЗ;
		нач
		(*
			Linux/Solaris11: works only with SUID root and FIFO threads
			Solaris10: woks only with SUID root
			Darwin: works allways
		*)
			pr := матМаксимум( ЭВМ.prioLow, матМинимум( prio, ЭВМ.prioHigh ) );
			Unix.ThrSetPriority( threadId, pr );
			priority := GetPriority( )
		кон SetPriority;


		проц & Initialize( obj: ProtectedObject;  bodyProc: Body;  prio: целМЗ; fl: мнвоНаБитахМЗ; stacksize: размерМЗ);
		нач
			сам.obj := obj;  condition := НУЛЬ;  continue := Unix.NewCond( );
			flags := fl;
			priority := prio;
			nextProcess := НУЛЬ;
			context := CurrentContext();
			(* must happen here before body is created! *)
			gcContext.nextPos := 0;
			если сравнениеСОбменом(root,НУЛЬ,сам) = НУЛЬ то
				(* first process *)
				stackBottom := Glue.stackBottom;
				threadId := Unix.ThrThis( );
				id := 0;  nextPID := 1;
				mode := Running;
				Unix.WriteKey( processPointer, сам );
				утв(~сравнениеСОбменом(startedMainProcess, ложь, истина));
			иначе
				newProcess := сам;
				утв( bodyProc # НУЛЬ );
				body := bodyProc;
				Unix.MtxLock( startProcess );
				Unix.MtxLock( startEventLock ); (* the cond wait below opens this lock again! *)
				threadId := Unix.ThrStart( BodyStarter, stacksize );
				Unix.CondWait( childrunning, startEventLock );
				Unix.MtxUnlock( startEventLock );
				Unix.MtxUnlock( startProcess );
				RegisterFinalizer( сам, FinalizeProcess );
			всё;
		кон Initialize;

	кон Process;

тип	GCStatusExt = окласс (Heaps.GCStatus)

		(*	called from Heaps.InvokeGC, i.e. this is a hidden upcall. However, it is necessary to take the Machine.Objects
			lock here since writing the set of variables here must not be interrupted, i.e. atomic writing of the set of variables
			is absolutely necessary.  They system may hang if the lock is not taken. *)
		проц {перекрыта}SetgcOngoing( value: булево );
		нач
			если value то
				ЭВМ.ЗапросиБлокировку( ЭВМ.ЯдернаяБлокировкаДляРаботыСКучей );
				если ConcurrentGC то
					Heaps.CollectGarbage( Modules.root );
				иначе
					SuspendActivities;
					Heaps.CollectGarbage( Modules.root );
					ResumeActivities;
				всё;
				ЭВМ.ОтпустиБлокировку( ЭВМ.ЯдернаяБлокировкаДляРаботыСКучей );
				finalizerCaller.Activate;
			всё;
		кон SetgcOngoing;

	кон GCStatusExt;



	проц BodyStarter;
	перем p: Process;  res: целМЗ; sp: адресВПамяти;
	нач
		(* make sure the process cannot be GCed in an intermediate unsafe state *)
		ЭВМ.ЗапросиБлокировку(ЭВМ.ЯдернаяБлокировкаДляРаботыСКучей);
		Unix.MtxLock( startEventLock );
		p := newProcess;  newProcess := НУЛЬ;
		Unix.WriteKey( processPointer, p );
		p.id := nextPID;  увел( nextPID );
		p.stackBottom := НИЗКОУР.GetFramePointer( );
		ЭВМ.ОтпустиБлокировку(ЭВМ.ЯдернаяБлокировкаДляРаботыСКучей);

		Unix.MtxLock( processList );
		p.nextProcess := root;  root := p;
		Unix.MtxUnlock( processList );
		Unix.CondSignal( childrunning );
		Unix.MtxUnlock( startEventLock );

		p.SetPriority( p.priority );

		если Restart в p.flags то
			(* store and restore stack pointer because it is saved on the stack during SYSVABI calls to ensure stack alignment *)
			sp := НИЗКОУР.GetStackPointer ();
			res := Unix.sigsetjmp( адресОт( p.state0[0] ), 1 );
			НИЗКОУР.SetStackPointer (sp);
		всё;
		p.SetMode(Running);
		p.body( p.obj );
		Exit
	кон BodyStarter;




	(*---------------------   create,  lock,  await,  unlock   -------------------------*)


	(* initialize the ObjectHeader, requires lockMutex temporarily *)
	проц InitProtHeader( hdr {неОтслСборщиком}: ObjectHeader);
	перем lock: LockT;
	нач
		(* we cannot hold the lockMute here because allocation can trigger the GC that requires the lock when activating the finalizers *)
		нов(lock);
		Unix.MtxLock(lockMutex);
		если hdr.lock = НУЛЬ то
			lock.mtx := Unix.NewMtx( );  lock.enter := Unix.NewCond( );  hdr.lockedBy := НУЛЬ;
			hdr.lock := lock;
		всё;
		Unix.MtxUnlock(lockMutex);
	кон InitProtHeader;


	проц CreateProcess*( body: Body;  priority: целМЗ;  flags: мнвоНаБитахМЗ;  obj: ProtectedObject );
	перем p: Process;  hdr{неОтслСборщиком}: ObjectHeader;
	нач
		Unix.MtxLock( createProcess );
		НИЗКОУР.прочтиОбъектПоАдресу( НИЗКОУР.подмениТипЗначения( адресВПамяти, obj ) + Heaps.HeapBlockOffset, hdr );
		InitProtHeader( hdr );
		если priority = 0 то  priority := Normal  всё;
		нов( p, obj, body, priority, flags, stacksize ) ;	(* execute BodyStarter as new (posix or solaris) thread *)
		Unix.MtxUnlock( createProcess );
		RegisterFinalizer( obj, FinalizeActiveObj )
	кон CreateProcess;

	проц Lock*( obj: ProtectedObject;  exclusive: булево );
	перем hdr {неОтслСборщиком}: ObjectHeader;  p : Process; lock: LockT;
	нач
		утв( exclusive );   (* shared not implemented yet *)
		НИЗКОУР.прочтиОбъектПоАдресу( НИЗКОУР.подмениТипЗначения( адресВПамяти, obj ) + Heaps.HeapBlockOffset, hdr );
		p := CurrentProcess();

		(*! we might want to replace the lock mutex by a lock free construct *)
		если hdr.lock = НУЛЬ то  InitProtHeader( hdr )  всё;
		lock := НИЗКОУР.подмениТипЗначения(LockT, hdr.lock);

		p.mode := AwaitingLock;
		Unix.MtxLock( lock.mtx );
		нцПока hdr.lockedBy # НУЛЬ делай
			(* wait until threads with complied AWAIT conditions have left the monitor *)
			Unix.CondWait( lock.enter, lock.mtx );
		кц;
		p.mode := Running;  hdr.lockedBy := p;  p.waitingOn := НУЛЬ
	кон Lock;

	проц Await*( cond: Condition;  slink: адресВПамяти;  obj: ProtectedObject;  flags: мнвоНаБитахМЗ );
	перем hdr {неОтслСборщиком}: ObjectHeader;  p, c: Process; lock: LockT;
	нач
		если 1 в flags то  (* compiler did not generate IF *)
			если cond( slink ) то  (* condition already true *)  возврат  всё
		всё;
		НИЗКОУР.прочтиОбъектПоАдресу( НИЗКОУР.подмениТипЗначения( адресВПамяти, obj ) + Heaps.HeapBlockOffset, hdr );  c := НУЛЬ;
		lock := НИЗКОУР.подмениТипЗначения(LockT, hdr.lock);
		если hdr.awaitingCond.head # НУЛЬ то  c := FindCondition( hdr.awaitingCond )  всё;

		p := CurrentProcess();
		утв( hdr.lockedBy = p, 2204 );
		p.succ := НУЛЬ;  p.condition := cond;  p.condFP := slink;  p.waitingOn := obj;

		Put( hdr.awaitingCond, p );
		hdr.lockedBy := c;
		если c # НУЛЬ то  Unix.CondSignal( c.continue )  иначе  Unix.CondSignal( lock.enter )  всё;

		p.mode := AwaitingCond;
		Unix.CondWait( p.continue, lock.mtx );
		p.mode := Running;  hdr.lockedBy := p;  p.waitingOn := НУЛЬ
	кон Await;

	проц Unlock*( obj: ProtectedObject;  dummy: булево );
	перем hdr{неОтслСборщиком}: ObjectHeader;  c: Process; lock: LockT;
	нач
		НИЗКОУР.прочтиОбъектПоАдресу( НИЗКОУР.подмениТипЗначения( адресВПамяти, obj ) + Heaps.HeapBlockOffset, hdr );  c := НУЛЬ;
		lock := НИЗКОУР.подмениТипЗначения(LockT,hdr.lock);
		если hdr.awaitingCond.head # НУЛЬ то  c := FindCondition( hdr.awaitingCond )  всё;

		hdr.lockedBy := c;
		если c # НУЛЬ то  Unix.CondSignal( c.continue )  иначе  Unix.CondSignal( lock.enter )  всё;
		Unix.MtxUnlock( lock.mtx );
	кон Unlock;

	проц FindCondition( перем q: ProcessQueue ): Process;
	перем first, cand: Process;
	нач
		Get( q, first );
		если first.condition( first.condFP ) то  возврат first  иначе  Put( q, first )  всё;
		нцПока q.head # first делай
			Get( q, cand );
			если cand.condition( cand.condFP ) то  возврат cand  иначе  Put( q, cand )  всё;
		кц;
		возврат НУЛЬ
	кон FindCondition;

	проц Get( перем queue: ProcessQueue;  перем new: Process );
	перем t: Process;
	нач
		t := queue.head(Process);
		если t # НУЛЬ то
			если t = queue.tail то  queue.head := НУЛЬ;  queue.tail := НУЛЬ
			иначе  queue.head := t.succ;  t.succ := НУЛЬ
			всё
		всё;
		new := t
	кон Get;

	проц Put( перем queue: ProcessQueue;  t: Process );
	нач
		если queue.head = НУЛЬ то  queue.head := t  иначе  queue.tail(Process).succ := t  всё;
		queue.tail := t
	кон Put;

	(*-------------------------------------------------------------------------*)

	проц Terminate-;
	нач
		Exit
	кон Terminate;

	проц TerminateThis*( p: Process; unbreakable: булево );
	нач
		p.SetMode(Terminated);
		p.Cancel
	кон TerminateThis;

	проц SetPriority*( pri: целМЗ );		(* Set the current process' priority. *)
	перем me: Process;
	нач
		me := CurrentProcess();
		me.SetPriority( pri )
	кон SetPriority;

	проц Sleep*( ms: цел32 );
	нач
		Unix.ThrSleep( ms );
	кон Sleep;

	проц Yield-;	(* Relinquish control. *)
	нач
		Unix.ThrYield( );
	кон Yield;

	(* Return current process. (DEPRECATED, use ActiveObject) *)
	проц CurrentProcess*( ):  {неОтслСборщиком}Process;
	нач
		возврат НИЗКОУР.подмениТипЗначения( Process , Unix.ReadKey( processPointer ) )
	кон CurrentProcess;

	проц CurrentContext*(): динамическиТипизированныйУкль;
	перем p : Process;
	нач
		p := CurrentProcess();
		если p # НУЛЬ то возврат p.context
		иначе возврат НУЛЬ
		всё;
	кон CurrentContext;

	проц SetContext*(context: динамическиТипизированныйУкль);
	перем p: Process;
	нач
		p := CurrentProcess();
		если p # НУЛЬ то p.context := context всё;
	кон SetContext;


	(* Return the active object currently executing. *)
	проц ActiveObject*( ): динамическиТипизированныйУкль;
	перем p: Process;
	нач
		p := CurrentProcess();
		возврат p.obj
	кон ActiveObject;


	(* Return stack bottom of process. For compatibility WinAos/UnixAos/NativeAos  *)
	проц GetStackBottom*(p: Process): адресВПамяти;
	нач
		возврат p.stackBottom
	кон GetStackBottom;

	проц GetStack*(p: Process; перем pc, bp, sp, sb: адресВПамяти; перем contextPos: размерМЗ);
	нач
		UpdateProcessState(p);
		bp := p.state.BP;
		pc := p.state.PC;
		sp := p.state.SP;
		sb := p.stackBottom;
		contextPos := p.gcContext.nextPos;

		если CurrentProcess() = p то
			sp := НИЗКОУР.GetStackPointer();
			bp := НИЗКОУР.GetFramePointer();
			НИЗКОУР.прочтиОбъектПоАдресу(bp, bp);
			НИЗКОУР.прочтиОбъектПоАдресу(bp+размер16_от(адресВПамяти), pc);
		иначе
			если ЭВМ.АдресНаКучеЛи¿(pc) то
			иначе
				неважно NextValidStackSegment(p, bp, pc, sp, contextPos);
			всё;
		всё;
	кон GetStack;

	проц NextValidStackSegment*(p: Process; перем bp, pc, sp: адресВПамяти; перем contextPos: размерМЗ): булево;
	нач
		bp := НУЛЬ;
		нцПока p.gcContext.GetNextContext(contextPos, bp, pc, sp) и (bp = НУЛЬ) делай
		кц;
		возврат bp  # НУЛЬ;
	кон NextValidStackSegment;

	проц GetProcessID*( ): целМЗ;
	перем p: Process;
	нач
		p := CurrentProcess();
		возврат p.id;
	кон GetProcessID;

	проц NumReady*( ): цел32;
	перем n: цел32; p: Process;
	нач
		n := 0;
		Unix.MtxLock( processList );
		p := root;
		нцПока p # НУЛЬ делай
			если p.mode = Running то увел( n ) всё;  
			p := p.nextProcess
		кц;
		Unix.MtxUnlock( processList );
		возврат n
	кон NumReady;


	проц GetCpuCycles*( process : Process; перем cpuCycles: CpuCyclesArray; all: булево );
	перем temp : цел64;
	нач
		утв(process # НУЛЬ);
		cpuCycles[0] := ЭВМ.ДайКвоТактовПроцессораСМоментаПерезапуска();

		если ~all то
			temp := process.lastThreadTimes;
			process.lastThreadTimes := cpuCycles[0];
			cpuCycles[0] := cpuCycles[0] - temp;
		всё;
	кон GetCpuCycles;



	(*-----------------------------------------------------------------------*)

	проц RegisterFinalizer( obj: динамическиТипизированныйУкль;  fin: Heaps.Finalizer );
	перем n: Heaps.FinalizerNode;
	нач
		нов( n ); n.finalizer := fin;  Heaps.AddFinalizer( obj, n );
	кон RegisterFinalizer;

	проц FinalizeActiveObj( obj: динамическиТипизированныйУкль );
	перем p: Process;
	нач
		Unix.MtxLock( processList );
			p := root;
			нцПока (p # НУЛЬ) и (p.obj # obj) делай p := p.nextProcess  кц;
		Unix.MtxUnlock( processList );
		если (p # НУЛЬ) и (p.obj = obj) то
			p.SetMode(Terminated);
			Unix.CondDestroy( p.continue );  p.continue := 0;
			FinalizeProtObject( obj );
			p.Cancel
		всё;
	кон FinalizeActiveObj;

	проц FinalizeProtObject( obj: динамическиТипизированныйУкль );
	перем hdr{неОтслСборщиком}: ObjectHeader; lock: LockT;
	нач
		НИЗКОУР.прочтиОбъектПоАдресу( НИЗКОУР.подмениТипЗначения( адресВПамяти, obj ) + Heaps.HeapBlockOffset, hdr );
		если hdr.lock # НУЛЬ то
			lock := НИЗКОУР.подмениТипЗначения(LockT, hdr.lock);
			Unix.MtxDestroy( lock.mtx );  lock.mtx := 0
		всё
	кон FinalizeProtObject;


	проц FinalizeProcess( obj: динамическиТипизированныйУкль );
	перем p: Process;
	нач
		p := obj(Process);
		если p.continue # 0 то
			Unix.CondDestroy( p.continue );  p.continue := 0
		всё
	кон FinalizeProcess;

	(* Terminate calling thread. *)
	проц Exit;
	перем prev, p, me: Process;
	нач
		me := CurrentProcess();
		me.SetMode(Terminated);
		Unix.MtxLock( processList );
		prev := НУЛЬ;  p := root;
		нцПока (p # НУЛЬ ) и (p # me) делай  prev := p;  p := p.nextProcess  кц;
		если p = me то
			если prev = НУЛЬ то  root := p.nextProcess  иначе  prev.nextProcess := p.nextProcess  всё;
		всё;
		Unix.MtxUnlock( processList );
		Unix.ThrExit( );
	кон Exit;

	проц ExitTrap-;
	перем p: Process;
	нач
		p := CurrentProcess();
		если p.id = 0 то
			(* main thread *)
			Unix.exit( 1 )
		всё;
		(* restart the object body if it was given the SAFE flag *)
		если Restart в p.flags то
			Unix.siglongjmp( адресОт( p.state0[0] ), 1 )
		всё;
		Exit
	кон ExitTrap;




	(*---------------------------- Timer --------------------------------*)


	проц Remove( t: Timer );  (* remove timer from list of active timers *)
	перем p, x: Timer;
	нач
		Unix.MtxLock( timerListMutex );
		t.trigger := 0;  t.handler := НУЛЬ;
		если timers # НУЛЬ то
			если t = timers то
				timers := t.next
			иначе
				p := timers;  x := p.next;
				нцПока (x # НУЛЬ) и (x # t)  делай  p := x;  x := p.next  кц;
				если x = t то  p.next := t.next  всё
			всё;
			t.next := НУЛЬ
		всё;
		Unix.MtxUnlock( timerListMutex )
	кон Remove;

	проц Insert( t: Timer );
	перем  p, x: Timer;
	нач
		Unix.MtxLock( timerListMutex );
		p := НУЛЬ;  x := timers;
		нцПока (x # НУЛЬ) и (x.trigger < t.trigger)  делай  p := x;  x := p.next  кц;
		t.next := x;
		если p = НУЛЬ то  timers := t  иначе   p.next := t  всё;
		Unix.MtxUnlock( timerListMutex )
	кон Insert;

	проц SetTimeout*( t: Timer;  h: EventHandler;  ms: цел32 );
	нач
		утв( ( t # НУЛЬ) и ( h # НУЛЬ) );
		Remove( t );
		если ms < 1 то ms := 1 всё;
		t.trigger := ЭВМ.ticks + ms;  t.handler := h;
		Insert( t );
		timerActivity.Restart
	кон SetTimeout;

	проц SetTimeoutAt*( t: Timer;  h: EventHandler;  ms: цел32 );
	нач
		утв( (t # НУЛЬ) и (h # НУЛЬ) );
		Remove( t );
		t.trigger := ms;  t.handler := h;
		Insert( t );
		timerActivity.Restart
	кон SetTimeoutAt;

	проц CancelTimeout*( t: Timer );
	нач
		Remove( t )
	кон CancelTimeout;

	(*--------------------  Garbage Collection  ------------------------------------*)


	проц GetContext( ctxt: Unix.Ucontext );
	перем p: Process; context: Unix.McontextDesc;
	нач
		p := CurrentProcess( );
		если p.gcContext.nextPos = 0 то  (* in A2 *)
		Unix.CopyContext( ctxt.mc, context );
			p.state.PC := context.r_pc;
			p.state.BP := context.r_bp;
			p.state.SP := context.r_sp
		иначе
			перем contextPos := p.gcContext.nextPos;
			если p.gcContext.GetNextContext(contextPos, p.state.BP, p.state.PC, p.state.SP) то всё;
		всё
	кон GetContext;

	(* called by WMProcessInfo to obtain the current state of a process *)
	проц UpdateProcessState*( p: Process );
	нач
		ЭВМ.ЗапросиБлокировку(ЭВМ.ЯдернаяБлокировкаДляРаботыСКучей);  (* must not run concurrently with GC *)
		если p = CurrentProcess() то
			p.state.PC := ЭВМ.ТекСчётчикКоманд( );
			p.state.BP := НИЗКОУР.GetFramePointer( );
			p.state.SP := НИЗКОУР.GetStackPointer( )
		иначе
			если p.mode = Running то
				timerStopped := истина;
					Suspend(p);
					Resume(p);
				timerStopped := ложь
			всё
		всё;
		ЭВМ.ОтпустиБлокировку(ЭВМ.ЯдернаяБлокировкаДляРаботыСКучей);
	кон UpdateProcessState;

	(* suspend and store state *)
	проц Suspend(p: Process);
	нач
		ЭВМ.ЗапросиБлокировку(ЭВМ.X11);
		Unix.ThrSuspend( p.threadId, p.mode = Running ); (*	the second parameter is ignored in the Linux port
																		but important for the other Unix ports. cf. Solaris.Unix.ThrSuspend *)
		ЭВМ.ОтпустиБлокировку(ЭВМ.X11);
	кон Suspend;

	проц Resume(p: Process);
	нач
		Unix.ThrResume( p.threadId );
	кон Resume;

	проц SuspendActivities;
	перем t, me: Process;
	нач
		Unix.MtxLock( processList ); (* no process should silently quit during global thread suspension *)
		me := CurrentProcess();
		t := root;
		нцПока t # НУЛЬ делай
			если t # me то
				Suspend(t)
			всё;
			t := t.nextProcess
		кц;
		Unix.MtxUnlock( processList );
		ЭВМ.ОтпустиБлокировку(ЭВМ.X11);
	кон SuspendActivities;

	проц ResumeActivities;
	перем t, me: Process;
	нач
		Unix.MtxLock( processList ); (* no process should silently quit during global thread suspension *)
		me := CurrentProcess();
		t := root;
		нцПока t # НУЛЬ делай
			если (t # me) то
				Resume(t);
			всё;
			t := t.nextProcess
		кц;
		Unix.MtxUnlock( processList );
	кон ResumeActivities;


	проц CurrentProcessTime*(): цел64;
	нач
		возврат  ЭВМ.ДайКвоТактовПроцессораСМоментаПерезапуска()
	кон CurrentProcessTime;

	проц TimerFrequency*(): цел64;
	нач
		возврат ЭВМ.timerFrequency;
	кон TimerFrequency;

	(* Leave A2 is called when a process leaves A2 by a call to the underlying OS API *)
	проц LeaveA2*;
	перем cur {неОтслСборщиком}: Process; ebp, pc, sp: адресВПамяти;
	нач{UNTRACKED}
		если сравнениеСОбменом(startedMainProcess,ложь,ложь) то
			cur := CurrentProcess();
			если cur # НУЛЬ то
				ebp := НИЗКОУР.GetFramePointer();
				sp := НИЗКОУР.GetStackPointer();
				НИЗКОУР.прочтиОбъектПоАдресу(ebp+размер16_от(адресВПамяти), pc);
				НИЗКОУР.прочтиОбъектПоАдресу(ebp, ebp);
				cur.gcContext.AddContext(ebp, pc, sp);				
			всё;
		всё;
	кон LeaveA2;

	(* reenter is called when a process returns from a call to the underlying OS API *)
	проц ReenterA2*;
	перем cur{неОтслСборщиком}: Process;
	нач{UNTRACKED}
		если сравнениеСОбменом(startedMainProcess,ложь,ложь) то
			cur := CurrentProcess();
			если cur # НУЛЬ то
				cur.gcContext.RemoveContext();
			всё;
		всё;
	кон ReenterA2;

	(*----------------------------- initialization ----------------------------------*)

	проц StartTimerActivity;
	нач
		timerListMutex := Unix.NewMtx( );  timers := НУЛЬ;
		нов( timerActivity );
	кон StartTimerActivity;


	проц GetStacksize;
	перем str: массив  32 из  симв8;  i: размерМЗ;
	нач
		ЭВМ.ДайЗначениеКлючаКонфигурацииЯОС( "StackSize", str );
		если str = "" то  stacksize := DefaultStacksize
		иначе
			i := 0;  stacksize := ЭВМ.СтрВЦел32( i, str );
			stacksize := stacksize * 1024;
		всё;
		если Glue.debug # {} то
			Трассировка.пСтроку8( "Stacksize of active objects = " );
			Трассировка.пЦел64( stacksize DIV 1024, 0 );  Трассировка.StringLn( "K"  )
		всё;
	кон GetStacksize;


	проц Convert;
	перем p: Process; o: ProtectedObject;
	нач
		(* make current thread the first active object  *)
		mainthread := Unix.ThrThis();
		нов(o); (* alibi object, e.g. for Locks *)
		нов( p, o, НУЛЬ, 0, {}, 0 );
	кон Convert;

	проц Init;
	нач
		Unix.suspendHandler := GetContext;

		createProcess := Unix.NewMtx( );  processList := Unix.NewMtx( );
		startEventLock := Unix.NewMtx( );  childrunning := Unix.NewCond( );
		lockMutex := Unix.NewMtx( );
		startProcess := Unix.NewMtx( );

		processPointer := Unix.NewKey( );

		GetStacksize;
		Convert;
		StartTimerActivity;  timerStopped := ложь;
		нов( clock );
		нов( finalizerCaller );

		Heaps.gcStatus := GCStatusFactory()
	кон Init;

тип
	MainThread = окласс
	перем exit: булево;

		проц & Init;
		нач
			exit := ложь;
		кон Init;

		проц Await();
		нач {единолично}
			дождись( exit );
		кон Await;

	кон MainThread;

перем main: MainThread;

	проц MainThreadSleep;
	нач
		нов( main );
		main.Await( );
		Unix.exit( 0 );
	кон MainThreadSleep;

	проц {неРасширяемая} Final;
	нач
		MainThreadSleep;
	кон Final;

	проц GCStatusFactory(): Heaps.GCStatus;
	перем gcStatusExt : GCStatusExt;
	нач
		утв( Heaps.gcStatus = НУЛЬ );
		нов( gcStatusExt );
		возврат gcStatusExt
	кон GCStatusFactory;

перем
	(* for compatibility and later extension *)
	TraceProcessHook*: проц (prcoess: Process; pc, bp: адресВПамяти; stacklow, stackhigh: адресВПамяти);

нач
	TraceProcessHook := НУЛЬ;
	Init;
кон Objects.

