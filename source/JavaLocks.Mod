(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль JavaLocks;	(* pjm *)

(*
Java-like locks for Aos.
The timeout case is very ugly, because handling external events is currently difficult.
Ref: The Java Language Specification, section 17.13-17.14
*)

использует Objects;

тип
	JavaLock* = окласс
		перем
			depth, in, out: цел32;
			locker: динамическиТипизированныйУкль;

		проц Lock*;
		перем me: динамическиТипизированныйУкль;
		нач {единолично}
			me := Objects.ActiveObject();
			дождись((locker = НУЛЬ) или (locker = me));
			увел(depth);
			locker := me
		кон Lock;

		проц Unlock*;
		нач {единолично}
			утв(locker = Objects.ActiveObject());
			умень(depth);
			если depth = 0 то locker := НУЛЬ всё
		кон Unlock;

		проц Wait*;
		перем ticket, mydepth: цел32; me: динамическиТипизированныйУкль;
		нач {единолично}
			me := Objects.ActiveObject();
			утв(locker = me);
			mydepth := depth; depth := 0; locker := НУЛЬ;
			ticket := in; увел(in);
			дождись((ticket - out < 0) и (locker = НУЛЬ));
			depth := mydepth; locker := me
		кон Wait;

		проц WaitTime*(ms: цел32);
		перем ticket, mydepth: цел32; me: динамическиТипизированныйУкль; sleeper: Sleeper;
		нач {единолично}
			me := Objects.ActiveObject();
			утв(locker = me);
			mydepth := depth; depth := 0; locker := НУЛЬ;
			ticket := in; увел(in);
			нов(sleeper, сам, ms);	(* allocate a sleeper for the current thread *)
			дождись((sleeper.done или (ticket - out < 0)) и (locker = НУЛЬ));
			sleeper.Stop;
			depth := mydepth; locker := me
		кон WaitTime;

		проц Notify*;
		нач {единолично}
			утв(locker = Objects.ActiveObject());
			если out # in то увел(out) всё
		кон Notify;

		проц NotifyAll*;
		нач {единолично}
			утв(locker = Objects.ActiveObject());
			out := in
		кон NotifyAll;

		проц Wakeup;
		нач {единолично}
		кон Wakeup;

		проц &Init*;
		нач
			depth := 0; locker := НУЛЬ; in := 0; out := 0
		кон Init;

	кон JavaLock;

тип
	Sleeper = окласс 	(* to do: simplify this *)
		перем lock: JavaLock; done: булево; timer: Objects.Timer;

		проц HandleTimeout;
		нач {единолично}
			если lock # НУЛЬ то done := истина; lock.Wakeup всё
		кон HandleTimeout;

		проц Stop;
		нач {единолично}
			lock := НУЛЬ; Objects.CancelTimeout(timer)
		кон Stop;

		проц &Start*(lock: JavaLock; ms: цел32);
		нач
			нов(timer);
			сам.lock := lock; done := ложь;
			Objects.SetTimeout(timer, сам.HandleTimeout, ms)
		кон Start;

	кон Sleeper;

кон JavaLocks.
