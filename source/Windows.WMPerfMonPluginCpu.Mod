модуль WMPerfMonPluginCpu; (** AUTHOR "staubesv"; PURPOSE "Performance Monitor CPU load plugin"; *)

использует
	WMPerfMonPlugins, Objects, Modules;

конст

	ModuleName = "WMPerfMonPluginCpu";

тип

	(* Dummy parameter for compatiblity only *)
	CpuParameter* = укль на запись (WMPerfMonPlugins.Parameter)
		processorID* : размерМЗ;
	кон;

	CpuLoad* = окласс(WMPerfMonPlugins.Plugin);

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		нач
			p.name := "NumReady"; p.description := "Number of processes in ready queue";
			p.modulename := ModuleName;
			p.autoMin := истина; p.autoMax := истина; p.minDigits := 2;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		нач
			dataset[0] := Objects.NumReady();
		кон UpdateDataset;

	кон CpuLoad;

перем
	nbrOfCpus- : цел32;

проц InitPlugins;
перем cpuLoad : CpuLoad; par : WMPerfMonPlugins.Parameter;
нач
	нов(par); нов(cpuLoad, par);
кон InitPlugins;

проц Install*;
кон Install;

проц Cleanup;
нач
	WMPerfMonPlugins.updater.RemoveByModuleName(ModuleName);
кон Cleanup;

нач
	nbrOfCpus := 1;
	InitPlugins;
	Modules.InstallTermHandler(Cleanup);
кон WMPerfMonPluginCpu.

WMPerfMonPluginCpu.Install ~ 	System.Free WMPerfMonPluginCpu ~
