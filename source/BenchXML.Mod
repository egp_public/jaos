модуль BenchXML; (** AUTHOR "staubesv"; PURPOSE "Simple XML parser benchmark"; *)

использует
	ЛогЯдра, Потоки, Modules, Commands, Options, Dates, Strings, Files, XML, XMLScanner, XMLParser;

конст
	DefaultNofTimes = 1000;
	DefaultNofWorkers = 1;

	Waiting = 0;
	Working = 1;
	Terminating = 2;
	Terminated = 3;

тип

	Worker = окласс
	перем
		file : Files.File;

		pooling : мнвоНаБитахМЗ;
		nofTimes : цел32;
		state : цел32;

		проц &Init*(file : Files.File; nofTimes : цел32; pooling : мнвоНаБитахМЗ);
		нач
			утв((file # НУЛЬ) и (nofTimes > 0));
			сам.file := file;
			сам.nofTimes := nofTimes;
			сам.pooling := pooling;
			state := Waiting;
		кон Init;

		проц Start;
		нач {единолично}
			если (state < Terminating) то
				state := Working;
			всё;
		кон Start;

		проц Terminate;
		нач {единолично}
			если (state # Terminated) то state := Terminating; всё;
			дождись(state = Terminated);
		кон Terminate;

		проц Parse;
		перем document : XML.Document; i : цел32;
		нач
			i := 0;
			нцПока ~error и (i < nofTimes) и (state = Working) делай
				document := ParseFile(file, pooling);
				увел(i);
			кц;
		кон Parse;

	нач {активное}
		нач {единолично} дождись((state = Working) или (state = Terminating)); кон;
		если (state = Working) то
			Parse;
		всё;
		DecrementNofActiveWorkers;
		нач {единолично} state := Terminated; кон;
	кон Worker;

перем
	error : булево;
	workers : укль на массив из Worker;
	nofActiveWorkers : цел32;

проц DecrementNofActiveWorkers;
нач {единолично}
	умень(nofActiveWorkers);
кон DecrementNofActiveWorkers;

проц ParseFile(file : Files.File; pooling : мнвоНаБитахМЗ) : XML.Document;
перем
	reader : Files.Reader;
	scanner : XMLScanner.Scanner;
	parser : XMLParser.Parser;
нач
	утв(file # НУЛЬ);
	нов(reader, file, 0);
	нов(scanner, reader);
	scanner.SetStringPooling(pooling);
	нов(parser, scanner);
	parser.reportError := DefaultReportError;
	возврат parser.Parse();
кон ParseFile;

проц BenchParser*(context : Commands.Context); (** [Options] filename ~ *)
перем
	filename : Files.FileName; options : Options.Options;
	file : Files.File;
	i, nofTimes, nofWorkers : цел32;
	pooling : мнвоНаБитахМЗ;
	start, end : Dates.DateTime;
	nofDays, nofHours, nofMinutes, nofSeconds : цел32;
нач {единолично} (* protects global variable error *)
	нов(options);
	options.Add("n", "nofTimes", Options.Integer);
	options.Add("p", "pooling", Options.Flag);
	options.Add("w", "workers", Options.Integer);
	если options.Parse(context.arg, context.error) то
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename) то
			если ~options.GetInteger("nofTimes", nofTimes) или (nofTimes <= 0) то nofTimes := DefaultNofTimes; всё;
			если ~options.GetInteger("workers", nofWorkers) или (nofWorkers <= 0) то nofWorkers := DefaultNofWorkers; всё;
			если ~options.GetFlag("pooling") то pooling := {}; иначе pooling := {0..31}; всё;
			если (nofTimes остОтДеленияНа nofWorkers = 0) то
				утв(nofTimes # 0);
				утв(nofWorkers > 0);
				file := Files.Old(filename);
				если (file # НУЛЬ) то
					context.out.пСтроку8("Parsing file "); context.out.пСтроку8(filename); context.out.пСтроку8(" "); context.out.пЦел64(nofTimes, 0); context.out.пСтроку8(" times ");
					context.out.пСтроку8(" using "); context.out.пЦел64(nofWorkers, 0); context.out.пСтроку8(" worker threads ...");
					context.out.ПротолкниБуферВПоток;
					нов(workers, nofWorkers);
					nofTimes := nofTimes DIV nofWorkers;
					nofActiveWorkers := nofWorkers;
					нцДля i := 0 до nofWorkers - 1 делай нов(workers[i], file, nofTimes, pooling); кц;
					start := Dates.Now();
					нцДля i := 0 до nofWorkers - 1 делай workers[i].Start; кц;
					error := ложь;
					дождись(nofActiveWorkers = 0);
					workers := НУЛЬ;
					end := Dates.Now();
					Dates.TimeDifference(start, end, nofDays, nofHours, nofMinutes, nofSeconds);
					nofSeconds := ToSeconds(nofDays, nofHours, nofMinutes, nofSeconds);
					context.out.пСтроку8("done in "); Strings.ShowTimeDifference(start, end, context.out);
					context.out.пСтроку8(" (");
					context.out.пВещ64_ФФТ(nofSeconds / (nofTimes * nofWorkers), 8, 3, 0);
					context.out.пСтроку8(")"); context.out.пВК_ПС;
				иначе
					context.error.пСтроку8("File "); context.error.пСтроку8(filename); context.error.пСтроку8(" not found.");
					context.error.пВК_ПС;
				всё;
			иначе
				context.error.пСтроку8("Parameter error: nofTimes MOD nofWorkers # 0"); context.error.пВК_ПС;
			всё;
		иначе
			ShowUsage(context.error);
		всё;
	всё;
кон BenchParser;

проц ToSeconds(nofDays, nofHours, nofMinutes, nofSeconds : цел32) : цел32;
нач
	возврат (86400 * nofDays + 3600 * nofHours + 60 * nofMinutes + nofSeconds);
кон ToSeconds;

проц DefaultReportError(pos, line, col: Потоки.ТипМестоВПотоке; конст msg: массив из симв8);
нач
	error := истина;
	ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСимв8(симв8ИзКода(9H)); ЛогЯдра.пСимв8(симв8ИзКода(9H)); ЛогЯдра.пСтроку8("pos "); ЛогЯдра.пЦел64(pos, 6);
	ЛогЯдра.пСтроку8(", line "); ЛогЯдра.пЦел64(line, 0); ЛогЯдра.пСтроку8(", col "); ЛогЯдра.пЦел64(col, 0);
	ЛогЯдра.пСтроку8("    "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
кон DefaultReportError;

проц ShowUsage(out : Потоки.Писарь);
нач
	утв(out # НУЛЬ);
	out.пСтроку8("Usage: BenchXML.Bench [Options] filename ~"); out.пВК_ПС;
кон ShowUsage;

проц Cleanup;
перем i : размерМЗ;
нач
	если (workers # НУЛЬ) то
		нцДля i := 0 до длинаМассива(workers)-1 делай
			workers[i].Terminate;
		кц;
		workers := НУЛЬ;
	всё;
кон Cleanup;

нач
	workers := НУЛЬ;
	Modules.InstallTermHandler(Cleanup);
кон BenchXML.

System.DoCommands
	BenchXML.BenchParser --nofTimes=16 Test.XML ~
	BenchXML.BenchParser --nofTimes=16 --workers=2 Test.XML ~
	BenchXML.BenchParser --nofTimes=16 --workers=4 Test.XML ~
	BenchXML.BenchParser --nofTimes=16 --workers=8 Test.XML ~
~

System.DoCommands
	BenchXML.BenchParser --nofTimes=16 --pooling Test.XML ~
	BenchXML.BenchParser --nofTimes=16 --workers=2 --pooling Test.XML ~
	BenchXML.BenchParser --nofTimes=16 --workers=4 --pooling Test.XML ~
	BenchXML.BenchParser --nofTimes=16 --workers=8 --pooling Test.XML ~
~

System.Free BenchXML ~
