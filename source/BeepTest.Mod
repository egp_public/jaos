модуль BeepTest; (** AUTHOR "afi"; PURPOSE "Hearing test using PC speaker"; *)

использует Beep, Kernel, ЛогЯдра;

перем busy : булево; timer : Kernel.Timer;

проц Go*;
перем i : цел32;
нач
	если ~busy то
		busy := истина;
		нцДля i := 1 до 20 делай	(* in 250Hz steps of 1 second *)
			ЛогЯдра.пЦел64(i * 250, 5);
			Beep.Beep(i * 250);
			timer.Sleep(1000);
			Beep.Beep(0);
		кц;
		ЛогЯдра.пВК_ПС;
		ЛогЯдра.пСтроку8("Done");
		ЛогЯдра.пВК_ПС;
		busy := ложь;
	всё;
кон Go;

нач
	нов(timer);
кон BeepTest.

BeepTest.Go ~
System.Free BeepTest ~
