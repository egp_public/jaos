модуль srRender;
использует  Modules, Commands, WMWindowManager, Raster, Objects, Random,
Rectangles := WMRectangles,  Out  := ЛогЯдра, Math, srBase,  srMath,  srRastermovie, srvoxels,
srRayEngine;

тип SREAL=srBase.SREAL;
тип Aperture = srBase.Aperture;
тип Ray = srBase.Ray;
тип Voxel = srBase.Voxel;
тип PT = srBase.PT;

тип SNAP = запись
	lookphi, looktheta : SREAL;
	aperture: Aperture;
	x,y,z,cdroll: SREAL;
	detail: цел16;
кон;

тип VoxWindow = окласс(WMWindowManager.DoubleBufferWindow)
перем
	alive, alpha: булево;
	i: цел32;
	random: Random.Generator;
	camera: Camera;
	speed: SREAL;
	pointerlastx, pointerlasty: размерМЗ;
	pi: WMWindowManager.PointerInfo;
	px, pdx, py, pdy: размерМЗ;
	pkeys: мнвоНаБитахМЗ;
	t1, t2, dt: цел32;
	big, focus, voxconnect: булево;
	connectvox: Voxel;
	Key: симв8;
	movemode: симв8;
	raysperframe: цел32;

проц & New*(W,H, i,j: цел16; large: булево);
нач
	Init(W, H, истина);
	raysperframe:=W*H;
	manager := WMWindowManager.GetDefaultManager();
	manager.Add(i,j, сам, { WMWindowManager.FlagFrame, WMWindowManager.FlagClose, WMWindowManager.FlagStayOnTop});
    WMWindowManager.LoadCursor("recticle.png", 3,3, pi);
	SetPointerInfo(pi);
	нов(camera, сам, W, H,large);
	speed := 0.0001;
	movemode := 'f';
кон New;

(*PROCEDURE Draw*(canvas : Graphics.Canvas; w, h, q : SIGNED32);
BEGIN
	IF filter THEN Draw^(canvas, w, h, q) ELSE Draw^(canvas, w, h, 0) END
END Draw; *)

проц {перекрыта}FocusGot*;
нач
	focus := истина;
кон FocusGot;

проц {перекрыта}FocusLost*;
нач
	focus := ложь;
кон FocusLost;

проц {перекрыта}Close*;
нач
	srBase.worldalive := ложь;
	Close^;
кон Close;

проц speedup*;
нач
	speed := speed * 3 / 2;
кон speedup;

проц slowdown*;
нач
	speed := speed * 2 / 3;
кон slowdown;

проц {перекрыта}KeyEvent*(ucs : размерМЗ; flags : мнвоНаБитахМЗ; keysym : размерМЗ);
нач
	Key := симв8ИзКода(ucs);
	просей симв8ИзКода(ucs) из
		'+': speed := speed * 3 / 2;
		|'-': speed := speed * 2 / 3;
		|'f': foveate := ~foveate;
		|'1': camera.xjet(1);
		|'4': camera.xjet(-1);
		|'2': camera.yjet(1);
		|'5': camera.yjet(-1);
		|'3': camera.zjet(1);
		|'6': camera.zjet(-1);
		| ']': aperture.width := aperture.width * 101/99;
			aperture.height := aperture.height *101/99;
			camera.rayschanged  := истина;
		| '[': aperture.width := aperture.width * 99/101;
			aperture.height := aperture.height *99/101;
			camera.rayschanged  := истина;
		| 'q': Close;
		| 'i': увел(srBase.iterlimit);
		| 'o': умень(srBase.iterlimit);
		| 'g': srBase.gravity := ~srBase.gravity; если srBase.gravity то Out.пСтроку8("G.g.g.raibbity!") всё;
		| 'c': увел(srBase.rlimit);
		| 'C':  camera.connectray(px,py,voxconnect, connectvox);
		| 'v': умень(srBase.rlimit);
		| '=': srBase.worldalive:=~srBase.worldalive;
		| 'r': camera.startrecording;
		| 'R': camera.stoprecording;
		| 'p': camera.startplaying;
		| 'P': camera.stopplaying;
		| 'd': DEATH := истина;
		| 'l': LOOK := истина;
		| 'k' : camera.filter := ~camera.filter;
		| 'z' :  cvx := cvx - 0.01
		| 'x' :  cvx := cvx + 0.01
		| 's': srRastermovie.snapshot(backImg)
		| 'm': srvoxels.trailswitch
		| 'h': hopcamera
		| ' ': camera.stop
		| 'Z': cdroll:= cdroll+1;
		| '*': srBase.STOPGO;
		| 't': TRAILS:=~TRAILS;
		иначе
	всё;
кон KeyEvent;

проц {перекрыта}PointerDown*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
нач
	pointerlastx := x ; pointerlasty := y;
	pkeys := keys;
	srRayEngine.fast:=истина;
кон PointerDown;

проц deathray;
нач
	camera.deathray(pointerlastx,pointerlasty)
кон deathray;

проц {перекрыта}PointerUp*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
нач
	pkeys := keys;
	DEATH:=ложь;
	srRayEngine.fast:=ложь;
кон PointerUp;

проц {перекрыта}PointerMove*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
нач
	pdx := px - x; pdy := py - y;
	px := матМодуль(x); py := матМодуль(y); pkeys := keys;
кон PointerMove;

проц {перекрыта}PointerLeave*;
нач
	focus := ложь;
кон PointerLeave;

проц {перекрыта}WheelMove*(dz : размерМЗ);
нач
    	camera.up(dz/10);
кон WheelMove;

проц move;
нач
	если movieplaying то camera.movietick;
	иначе
		если movemode = "f" то fly иначе pan всё;
		camera.tick;
	всё
кон move;

проц fly;
нач
	если 0 в pkeys то camera.forward(px,py) всё ;
(*	IF 1 IN pkeys THEN cvl := cvl+pdx/44; cvu := cvu+pdy/44 END;*)
	если 2 в pkeys то camera.backward(px,py) всё;
кон fly;

проц pan;
(*
BEGIN
	IF 0 IN pkeys THEN camera.forward(px,py);
	ELSIF 1 IN pkeys THEN cvl := cvl+pdx/4; camera.up(pdy)
	ELSIF 2 IN pkeys THEN camera.backward(px,py);
	END *)
кон pan;

кон VoxWindow;

тип Camera = окласс
перем
	window: VoxWindow;
	random: Random.Generator;
	filter,rayschanged, ang1: булево;
	fovealeft, fovearight, foveabottom, foveatop: цел32;
	fovea: булево;
	cam: srBase.PT;
	mode : Raster.Mode;
	pixel: Raster.Pixel;
	W,H: цел16;
	XLOOK,YLOOK: цел32;
	large: булево;

проц & init *(w: VoxWindow; width, height: цел16; l: булево);
нач
	window := w;
	large:=l;
	W := width; H := height;
	Raster.InitMode(mode, Raster.srcCopy);
(*	lookray.theta := 3.14;
	lookray.phi := 3.14;
	aperture.width := 4;
	aperture.height := 3;
	angletoray(lookray);
	framecount := 0;
	fovealeft :=  - 30;
	fovearight := fovealeft + 60;
	foveabottom := (H DIV 2) - 30;
	foveatop := foveabottom + 60;
	rayschanged := TRUE;
*)
кон init;

проц move;
перем
	x,y,z,d: SREAL;
	v: Voxel;
	proberay: Ray;

проц normalize(перем x,y,z,d: SREAL);
нач
	d := Math.sqrt(x*x + y*y+z*z);
	x := x/d; y := y/d; z:=z/d
кон normalize;

проц denormalize(перем x,y,z,d: SREAL);
нач
	x := x*d; y := y*d; z:=z*d
кон denormalize;

нач
	x := cx + cvx; y := cy + cvy; z := cz + cvz;
	v := block.probe(x,y,z);
	если  (v=НУЛЬ) или v.passable то
		cx := x; cy := y; cz := z;
		srBase.clamp3(cx,cy,cz);
	аесли v#НУЛЬ то
		proberay := srBase.blankray;
		normalize(cvx,cvy,cvz,d);
		proberay.dxyz.x := cvx;
		proberay.dxyz.y := cvy;
		proberay.dxyz.z := cvz;
		denormalize(cvx,cvy,cvz,d);
		proberay.xyz.x := cx;
		proberay.xyz.y := cy;
		proberay.xyz.z := cz;
		block.probeShade(proberay,cvx,cvy,cvz);
	всё;
кон move;

проц snap;
нач
	frames[frame].x := cx;
	frames[frame].y := cy;
	frames[frame].z := cz;
	frames[frame].looktheta := lookray.theta;
	frames[frame].lookphi := lookray.phi;
	frames[frame].aperture := aperture;
	frames[frame].cdroll := cdroll;
	frames[frame].detail := srBase.rlimit;
	увел(frame);
	если frame остОтДеленияНа 50 = 0 то Out.пЦел64(frame, 10); Out.пСтроку8(" frames"); Out.пВК_ПС; всё;
	если frame > 9999 то movierecording := ложь;  Out.пСтроку8(" out of film") всё
кон snap;

проц stop;
нач
	cvx := 0;
	cvy := 0;
	cvz := 0;
кон stop;

проц movietick;
перем
	s: SNAP;
нач
	s := frames[tickframe];
	cam.x := s.x;
	cam.y := s.y;
	cam.z := s.z;
	lookray.theta := s.looktheta;
	lookray.phi := s.lookphi;
	aperture := s.aperture;
	cdroll:=s.cdroll;
	srBase.rlimit := s.detail;
	увел(tickframe);
	если tickframe >= frame то stopplaying всё;
кон movietick;

проц startrecording;
нач
	movierecording := истина;
	Out.пСтроку8("movie recording on");
	srBase.RESET;
кон startrecording;

проц stoprecording;
нач
	movierecording := ложь;
	Out.пСтроку8("movie recording off")
кон stoprecording;

проц startplaying;
нач
	tickframe := 0;
	srBase.RESET;
	movieplaying := истина;
	Out.пСтроку8("movie playing on");
кон startplaying;

проц stopplaying;
нач
	movieplaying := ложь;
	srBase.worldalive :=ложь;
	Out.пСтроку8("movie playing off")
кон stopplaying;

проц deathray (x, y: размерМЗ);
нач
	если large то
		block.deathray(srBase.lrays[x,y]);
	иначе
		block.deathray(srBase.rays[x,y]);
	всё
кон deathray;

проц mutateray (x, y: цел32);
нач
	если (x < W) и (y < H) то
		block.mutateray(srBase.rays[x ,y]);
	всё
кон mutateray;

проц connectray(x, y: размерМЗ; перем connection: булево; перем vox: Voxel);
нач
	если  (x < W) и (y < H) то
		block.connectray(srBase.rays[x ,y], connection, vox);
	всё
кон connectray;

проц forward (x, y: размерМЗ);
перем
	speed: SREAL;
нач
	x:=x остОтДеленияНа W;
	y:=y остОтДеленияНа H;
	если large то
		left((x - (W/2)) / 150);
		up((y - (H/2)) / 150);
		speed := window.speed;
		cvx := cvx + srBase.lrays[x,y].dxyz.x * speed;
		cvy := cvy + srBase.lrays[x,y].dxyz.y * speed;
		cvz := cvz + srBase.lrays[x,y].dxyz.z * speed;
		clampspeed(cvx,cvy,cvz, 1);
	иначе
		left((x - (W/2)) / 150);
		up((y - (H/2)) / 150);
		speed := window.speed;
		cvx := cvx + srBase.rays[x,y].dxyz.x * speed;
		cvy := cvy + srBase.rays[x,y].dxyz.y * speed;
		cvz := cvz + srBase.rays[x,y].dxyz.z * speed;
		clampspeed(cvx,cvy,cvz, 1);
	всё
кон forward;

проц backward (x, y: размерМЗ);
перем
	speed: SREAL;
нач
	x:=x остОтДеленияНа W;
	y:=y остОтДеленияНа H;
		если large то
		left((x - (W/2)) / 50);
		up((y - (H/2)) / 50);
		speed := window.speed;
		cvx := cvx - srBase.lrays[x,y].dxyz.x * speed;
		cvy := cvy - srBase.lrays[x,y].dxyz.y * speed;
		cvz := cvz - srBase.lrays[x,y].dxyz.z * speed;
		clampspeed(cvx,cvy,cvz, 1);
	иначе
		left((x - (W/2)) / 50);
		up((y - (H/2)) / 50);
		speed := window.speed;
		cvx := cvx - srBase.rays[x,y].dxyz.x * speed;
		cvy := cvy - srBase.rays[x,y].dxyz.y * speed;
		cvz := cvz - srBase.rays[x,y].dxyz.z * speed;
		clampspeed(cvx,cvy,cvz, 1);
	всё
кон backward;

проц xjet(jet: SREAL);
нач
	cvx:=cvx+(jet*window.speed);
кон xjet;

проц yjet(jet: SREAL);
нач
	cvy:=cvy+(jet*window.speed);
кон yjet;

проц zjet(jet: SREAL);
нач
	cvz:=cvz+(jet*window.speed);
кон zjet;

проц  jitter;
кон jitter;

(* PROCEDURE initrays;
VAR
	i, j: SIGNED32;
	theta, phi, dtheta, dphi: SREAL;
BEGIN
	dtheta := aperture.width / W;
	dphi := aperture.height / H;
	theta := lookray.theta - aperture.width / 2; 	(*left*)
	FOR i := 0 TO W - 1 DO
		theta := theta + dtheta;
		phi := lookray.phi - aperture.height / 2; 	(*bottom*)
		FOR j := 0 TO H - 1 DO
			phi := phi + dphi;
			srBase.rays[i, j] := srBase.blankray;
			srBase.rays[i, j].theta := theta;
			srBase.rays[i, j].phi := phi;
			angletoray(srBase.rays[i, j]);
			srBase.rays[i, j].xyz := cam;
			srBase.rays[i, j].lxyz := cam;
		END
	END;
END initrays; *)

проц initrays;
перем
	i, j: цел32;
	theta, phi, dtheta, dphi: SREAL;
	down: srBase.PT;
	look: srBase.Ray;
нач
	dtheta := aperture.width / W;
	dphi := aperture.height / H;
	theta := lookray.theta-aperture.width / 2;
	srBase.setPT(down,0,1,0);
	если large то
		look:=srBase.LLOOK;
		нцДля i := 0 до W - 1 делай
			theta := theta + dtheta;
			phi :=  -aperture.height / 2; 	(*bottom*)
			нцДля j := 0 до H - 1 делай
				phi := phi + dphi;
				srBase.lrays[i, j] := srBase.blankray;
				srBase.lrays[i, j].theta := theta;
				srBase.lrays[i, j].phi := phi;
				angletoray(srBase.lrays[i, j]);
		  	  	srMath.orrot(srBase.lrays[i, j].dxyz, down, theta);
		  	  	srMath.orrot(srBase.lrays[i, j].dxyz, look.dxyz, cdroll);
				ddray(srBase.lrays[i, j]);
				srBase.lrays[i, j].xyz := cam;
				srBase.lrays[i, j].lxyz := cam;
			кц
		кц
	иначе
		look:=srBase.LOOK;
		нцДля i := 0 до W - 1 делай
			theta := theta + dtheta;
			phi :=  -aperture.height / 2; 	(*bottom*)
			нцДля j := 0 до H - 1 делай
				phi := phi + dphi;
				srBase.rays[i, j] := srBase.blankray;
				srBase.rays[i, j].theta := theta;
				srBase.rays[i, j].phi := phi;
				angletoray(srBase.rays[i, j]);
		  	  	srMath.orrot(srBase.rays[i, j].dxyz, down, theta);
		  	  	srMath.orrot(srBase.rays[i, j].dxyz, look.dxyz, cdroll);
				ddray(srBase.rays[i, j]);
				srBase.rays[i, j].xyz := cam;
				srBase.rays[i, j].lxyz := cam;
			кц
		кц
	всё
кон initrays;

проц foveate(x,y: цел32);
нач
	fovealeft := (x-foveasize) остОтДеленияНа W;
	fovearight := (x+foveasize) остОтДеленияНа W;
	foveadown := (y-foveasize) остОтДеленияНа H;
	foveaup := (y+foveasize) остОтДеленияНа H;
кон foveate;

проц tracetiled;
перем
	i, j: цел32;
	pixel : Raster.Pixel;
нач
	block.tick;
	если large то
		srRayEngine.lgo;
		если srRayEngine.fast то
			нцДля i := 0 до W-1 шаг 2 делай
				нцДля j := 0 до H-1 шаг 2 делай
					Raster.SetRGB(pixel, округлиВниз(srBase.limage[i,j].red * 255), округлиВниз(srBase.limage[i,j].green * 255),
					округлиВниз(srBase.limage[i,j].blue * 255));
					Raster.Fill(window.backImg,i,j,i+2,j+2,pixel, mode);
				кц
			кц
		иначе
			нцДля i := 0 до W-1 делай
				нцДля j := 0 до H-1 делай
					Raster.SetRGB(pixel, округлиВниз(srBase.limage[i,j].red * 255), округлиВниз(srBase.limage[i,j].green * 255),
					округлиВниз(srBase.limage[i,j].blue * 255));
					Raster.Put(window.backImg,i,j,pixel, mode);
				кц
			кц
		всё
	иначе
		srRayEngine.go;
		нцДля i := 0 до W-1 делай
			нцДля j := 0 до H-1 делай
				Raster.SetRGB(pixel, округлиВниз(srBase.image[i,j].red * 255), округлиВниз(srBase.image[i,j].green * 255),
				округлиВниз(srBase.image[i,j].blue * 255));
				Raster.Put(window.backImg,i,j,pixel, mode);
			кц
		кц
	всё;
	window.Swap;
	window.Invalidate(Rectangles.MakeRect(0, 0, window.GetWidth(), window.GetHeight()))
кон tracetiled;

(*PROCEDURE trace(VAR ray: srBase.Ray);
BEGIN
	block.Shade(ray);
	srBase.clamp3(ray.r, ray.g, ray.b);
END trace;*)

проц left (th: SREAL);
нач
	lookray.theta := lookray.theta + th/10;
	если lookray.theta > 6.28 то lookray.theta := 0 всё
кон left;

проц up (ph: SREAL);
нач
	cdroll:=cdroll+ph/10;
	если cdroll>6.28 то cdroll:=0 всё;
кон up;

проц trail(a,b:PT);
перем
	v: Voxel;
нач
	v:=block.probe(b.x,b.y,b.z);
	srvoxels.cameratrail(block,a,b);
кон trail;

проц tick;
перем
	oldcam:PT;
нач
	oldcam:=cam;
	cam.x := cx; cam.y := cy; cam.z := cz;
	если TRAILS то trail(oldcam,cam) всё;
	если srBase.gravity то cvz := cvz+1/10000 всё;
	cvx := cvx*38/39; cvy := cvy*38/39; cvz := cvz* 38/39; cvl := cvl*18/19; cvu := cvu*18/19;
(*	croll := croll + cdroll/150;
	cdroll:=cdroll*16/19; *)
	move;
	left(cvl);
	up(cvu);
кон tick;

кон Camera;

тип MainLoop=окласс
перем
	t1, t2, dt,f: цел32;
	fr: SREAL;
	framerate, lastframerate: цел32;

нач {активное, приоритет(Objects.Normal)}
	win:=window;
	нцДо
		srBase.tick;
		block:= srBase.world;
		если lwindow.focus то win := lwindow всё;
		если window.focus то win := window всё;
		win.camera.initrays;
		если DEATH то
			srBase.deathflag:=ложь;
			win.deathray;
			DEATH:=ложь
		всё;
		win.move;
		win.camera.tracetiled;
		если movierecording то win.camera.snap всё;
		если movieplaying то
			srRastermovie.snap(win.img);
		всё
	кцПри ~srBase.worldalive;
	Close;
кон MainLoop;

перем
	main: MainLoop;
	win, window, lwindow: VoxWindow;
	cx, cy, cz, cvx, cvy, cvz, cvl, cvu: SREAL;
	cdroll: SREAL;
	lookray: Ray;
	rand: Random.Generator;
	wcount: цел16;
	frame, tickframe: цел32;
	frames: массив 10000 из SNAP;
	movierecording, movieplaying: булево;
	foveate: булево;
	foveasize, foveadown, foveaup: цел32;
	block: Voxel;
	DEATH, LOOK, TRAILS: булево;
	tracetiled: булево;
	aperture: Aperture;
	framecount: цел32;

проц angletoray(перем ray: srBase.Ray);
(* VAR d: SREAL; *)
нач
	ray.dxyz.x := srMath.cos(ray.theta) * srMath.cos(ray.phi);
	ray.dxyz.y := srMath.sin(ray.theta) * srMath.cos(ray.phi);
	ray.dxyz.z := srMath.sin(ray.phi);
(*	d := Math.sqrt(ray.dxyz.x*ray.dxyz.x + ray.dxyz.y* ray.dxyz.y+ray.dxyz.z*ray.dxyz.z);  (* Norma! Liza! Ray! Front and center, oh dark thirty!*)
	ray.dxyz.x := ray.dxyz.x/d;
	ray.dxyz.y := ray.dxyz.y/d;
	ray.dxyz.z := ray.dxyz.z/d; *)
	ray.ddxyz.x := ray.dxyz.x/1000000;
	ray.ddxyz.y := ray.dxyz.y/1000000;
	ray.ddxyz.z := ray.dxyz.z/1000000;
кон angletoray;
(*
PROCEDURE raytangle(VAR ray: srBase.Ray);
VAR x,y, z: SREAL;
BEGIN
	x := ray.xyz.x; y := ray.xyz.y; z := 0;
	srBase.normalize(x,y,z);
	ray.theta := srMath.arccos(x);
	ray.phi := srMath.arccos(1-ray.dxyz.z);
END raytangle;

PROCEDURE carttosph(VAR p: PT; theta, phi: SREAL);
BEGIN
	p.x := srMath.cos(theta) * srMath.cos(phi);
	p.y := srMath.sin(theta) * srMath.cos(phi);
	p.z := srMath.sin(phi);
END carttosph;

PROCEDURE sphtocart( p: PT; VAR theta, phi: SREAL);
VAR
	x,y, z: SREAL;
BEGIN
	x := p.x; y := p.y; z := 0;
	srBase.normalize(x,y,z);
	theta := srMath.arccos(x);
	phi := srMath.arccos(1-p.z);
END sphtocart;

PROCEDURE gray(VAR ray: Ray);
VAR
	gray: SREAL;
BEGIN
	gray := (ray.r + ray.g + ray.b)/3;
	ray.r := gray; ray.g := gray; ray.b := gray;
END gray;
*)

проц ddray(перем ray: srBase.Ray);
нач
	ray.ddxyz.x := ray.dxyz.x/10000;
	ray.ddxyz.y := ray.dxyz.y/10000;
	ray.ddxyz.z := ray.dxyz.z/10000;
кон ddray;

проц clampspeed(перем r,g,b: SREAL; speed: SREAL);
нач
	если r < -speed то r := -speed аесли r>speed то r := speed всё;
	если g < -speed то g := -speed аесли g>speed то g := speed всё;
	если b < -100 то b := -speed аесли b>speed то b := speed всё;
кон clampspeed;

проц Demo*;
перем res : целМЗ; msg : массив 128 из симв8;
нач
	Commands.Call("Notepad.Open srReadMe.Text", {Commands.Wait}, res, msg); (* ignore res *)
	Open;
кон Demo;

проц Open*;
нач
	srBase.worldalive := истина;
	нов(window, srBase.W, srBase.H, 100, 100, ложь);
	нов(lwindow, srBase.LW, srBase.LH, 150, 200, истина);
	нов(main);
кон Open;

проц Close*;
нач
	 если window# НУЛЬ то window.Close; window := НУЛЬ всё;
	 если lwindow#НУЛЬ то lwindow.Close; lwindow:=НУЛЬ; всё;
кон Close;

проц hopcamera;
нач
	cx :=1/2+1/117; cy := 1/2+1/117; cz := 1/2-1/117;
кон hopcamera;

нач
	lookray.theta := 3.14;
	lookray.phi := 3.14;
	aperture.width := 3/2;
	aperture.height := 2;
	angletoray(lookray);
	framecount := 0;
(****************************************)
	wcount := 0;
	нов(rand);
	block:= srBase.world;
	утв(block# НУЛЬ);
	srRayEngine.setBlock(block);
	Modules.InstallTermHandler(Close);
	foveasize := 30;
	hopcamera;  (* PLACE CAMERA *)
	tracetiled:=истина;
кон srRender.
(* 
srRender.Open ~

System.Free srRender srvoxels ~

System.Free
srBase
srRayEngine
srMath
srE
srGL
srHex
srImage
srVoxel
srVoxel2
srVoxel3
srVolShader
srVoxel4
srVoxel5
srM2Space
srM3Space
srM5Space
srM6Space
srRastermovie
srTexVox
srThermoCell
srTree
sr3DTexture
srLifeVox
srvoxels
srRender
TuringCoatWnd~

PC.Compile \s
srBase.Mod
srRayEngine.Mod
srMath.Mod
srE.Mod
srGL.Mod
srHex.Mod
srImage.Mod
srVoxel.Mod
srVoxel2.Mod
srVoxel3.Mod
srVolShader.Mod
srVoxel4.Mod
srVoxel5.Mod
srM2Space.Mod
srM3Space.Mod
srM5Space.Mod
srM6Space.Mod
srRastermovie.Mod
srTexVox.Mod
srThermoCell.Mod
srTree.Mod
sr3DTexture.Mod
srLifeVox.Mod
srvoxels.Mod
srRender.Mod
TuringCoatWnd.Mod~

#############################################

Tar.Create tracer.Tar
srBase.Mod
srRayEngine.Mod
srMath.Mod
srE.Mod
srGL.Mod
srHex.Mod
srImage.Mod
srVoxel.Mod
srVoxel2.Mod
srVoxel3.Mod
srVolShader.Mod
srVoxel4.Mod
srVoxel5.Mod
srM2Space.Mod
srM3Space.Mod
srM5Space.Mod
srM6Space.Mod
srRastermovie.Mod
srTexVox.Mod
srThermoCell.Mod
srTree.Mod
sr3DTexture.Mod
srLifeVox.Mod
srvoxels.Mod
srRender.Mod
TuringCoatWnd.Mod
srskin.skin
ateney.jpg
BathingInterrupted.jpg
DESTROYUSA.jpg
~
*)
