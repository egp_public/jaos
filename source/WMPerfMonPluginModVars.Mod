модуль WMPerfMonPluginModVars; (** AUTHOR "staubesv"; PURPOSE "Performance Monitor module variables  plugin"; *)
(**
 * History:
 *
 *	16.02.2006	First Release (staubesv)
 *)

использует
	НИЗКОУР,
	WMPerfMonPlugins,
	Modules, Reflection, Commands, Потоки, Strings;

конст

	ModuleName = "WMPerfMonPluginModVars";

	(* Supported module variable types. MUST NOT BE CHANGED *)
	Byte= 1;
	Boolean = 2;
	Char= 3;
	Shortint = 4;
	Integer = 5;
	Longint = 6;
	Set = 9;
	Real = 7;
	Longreal = 13; (* not yet implemented *)
	Ptr = 29;

тип

	VariableDescriptor* = запись
		moduleName* : Modules.Name;
		variableName* : массив 64 из симв8;
	кон;

	VariableDescriptors* = укль на массив из VariableDescriptor;

	ModVarParameter = укль на запись (WMPerfMonPlugins.Parameter)
		vd : VariableDescriptors;
	кон;

тип

	ModVar = окласс(WMPerfMonPlugins.Plugin)
	перем
		vd : VariableDescriptors;

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		нач
			утв((p # НУЛЬ) и (p суть ModVarParameter) и (p(ModVarParameter).vd # НУЛЬ));
			vd := p(ModVarParameter).vd;
			p.description := "Module variables access adapter";
			p.modulename := ModuleName;
			p.autoMax := истина; p.minDigits := 5;
			p.datasetDescriptor := GetDatasetDescriptor(vd);
		кон Init;

		проц {перекрыта}UpdateDataset*;
		перем module : Modules.Module; variable : Reflection.Variable; i : размерМЗ; value : вещ64;
		нач
			нцДля i := 0 до длинаМассива(vd)-1 делай
				module := Modules.ModuleByName(vd[i].moduleName);
				если module # НУЛЬ то
					если Reflection.FindVar(module, vd[i].variableName, variable) то
						если GetValueOf(variable, value) то
							dataset[i] := устарПреобразуйКБолееУзкомуЦел(value);
						иначе
						всё;
					иначе
					всё;
				иначе
				всё;
			кц;
		кон UpdateDataset;

	кон ModVar;

проц GetValueOf(v: Reflection.Variable; перем value : вещ64) : булево;
перем short : цел8; int : цел16; long : цел32; ch : симв8; real : вещ32;
нач
	если (v.n = 1) то (* arrays not supported *)
		просей v.type из
			|Byte: НИЗКОУР.прочтиОбъектПоАдресу(v.adr, ch); value := кодСимв8(ch);
			|Boolean: НИЗКОУР.прочтиОбъектПоАдресу(v.adr, ch); если ch = 0X то value := 0; иначе value := 1; всё;
			|Char: НИЗКОУР.прочтиОбъектПоАдресу(v.adr, ch); value := кодСимв8(ch);
			|Shortint: short := НИЗКОУР.прочти8битПоАдресу(v.adr); value := short;
			|Integer: int := НИЗКОУР.прочти16битПоАдресу(v.adr); value := int;
			|Longint: long := НИЗКОУР.прочти32битаПоАдресу(v.adr); value := long;
			|Set: long := НИЗКОУР.прочти32битаПоАдресу(v.adr); value := long;
			|Real: real := НИЗКОУР.прочти32битаПоАдресу(v.adr); value := real;
			|Longreal:
			|Ptr: long := НИЗКОУР.прочти32битаПоАдресу(v.adr); value := long;
		иначе
			возврат ложь;
		всё;
	иначе
		возврат ложь;
	всё;
	возврат истина;
кон GetValueOf;

проц GetDatasetDescriptor(vd : VariableDescriptors) : WMPerfMonPlugins.DatasetDescriptor;
перем ds : WMPerfMonPlugins.DatasetDescriptor; i : размерМЗ;
нач
	утв(vd # НУЛЬ);
	нов(ds, длинаМассива(vd));
	нцДля i := 0 до длинаМассива(vd)-1 делай
		копируйСтрокуДо0(vd[i].variableName, ds[i].name);
	кц;
	возврат ds;
кон GetDatasetDescriptor;

проц InstallPlugin*(конст name : WMPerfMonPlugins.Name; vd : VariableDescriptors);
перем par : ModVarParameter; plugin : ModVar;
нач
	утв(vd # НУЛЬ);
	нов(par); par.vd := vd; par.name := name;
	нов(plugin, par);
кон InstallPlugin;

проц Add(перем vd : VariableDescriptors; конст moduleName, variableName : массив из симв8);
перем new : VariableDescriptors; i : размерМЗ;
нач
	если vd = НУЛЬ то
		нов(vd, 1);
		копируйСтрокуДо0(moduleName, vd[0].moduleName);
		копируйСтрокуДо0(variableName, vd[0].variableName);
	иначе
		нов(new, длинаМассива(vd)+1);
		i := 0; нцПока (i < длинаМассива(vd)) делай new[i] := vd[i]; увел(i); кц;
		копируйСтрокуДо0(moduleName, new[i].moduleName);
		копируйСтрокуДо0(variableName, new[i].variableName);
		vd := new;
	всё;
кон Add;

проц Install*(context : Commands.Context); (** pluginname modulename.variablename  {" " modulename.variablename} ~ *)
перем
	string : массив 128 из симв8; pluginname : WMPerfMonPlugins.Name;
	vd : VariableDescriptors; split : Strings.StringArray;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(pluginname);
	нцПока context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string) делай
		split := Strings.Split(string, ".");
		если длинаМассива(split) = 2 то
			Add(vd, split[0]^, split[1]^);
		иначе
			ShowUsage(context.error); возврат;
		всё;
	кц;
	если vd # НУЛЬ то
		InstallPlugin(pluginname, vd);
		context.out.пСтроку8("WMPerfMonPluginModVars: Installed plugin "); context.out.пСтроку8(pluginname);
		context.out.пВК_ПС;
	иначе
		ShowUsage(context.error);
	всё;
кон Install;

проц ShowUsage(w : Потоки.Писарь);
нач
	w.пСтроку8(ModuleName); w.пСтроку8(": Expected parameters: modulename.variablename"); w.пВК_ПС;
кон ShowUsage;

проц Cleanup;
нач
	WMPerfMonPlugins.updater.RemoveByModuleName(ModuleName);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
кон WMPerfMonPluginModVars.

System.Free WMPerfMonPluginModVars ~

WMPerfMonPluginModVars.Install ~

WMPerfMonPluginModVars.Install Network Network.nofBuf ~

WMPerfMonPluginModVars.Install Test WMMessages.messagesAdded WMMessages.messagesDiscarded ~
