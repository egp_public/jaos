<?xml version="1.0" encoding="UTF-8"?>
<?bluebottle format version="0.1" ?>
<?xml-stylesheet type="text/xsl" href="http://bluebottle.ethz.ch/bluebottle.xsl" ?>
<Text>
<Span style="Assertion"><![CDATA[Введение в ЯОС
]]></Span><Span style="Normal"><![CDATA[
]]></Span><Span style="Bold"><![CDATA[Что такое ЯОС?

]]></Span><Span style="Normal"><![CDATA[ЯОС - это операционная система, способная работать на x86, AMD64, ARM, хотя в настоящий момент поддерживаются только сборки под x86 и AMD64. Помимо работы на железе, ЯОС может собираться как приложение для Windows и Linux. Наиболее часто пересобирается
и может быть запущена в новейшем виде сборка под x86, вызываемая из-под Windows. Сборку под железо можно запустить только после самостоятельной сборки. 

]]></Span><Span style="Bold"><![CDATA[Исходные тексты и внешняя документация
]]></Span><Span style="Normal"><![CDATA[
Во-первых, все исходные тексты доступны прямо из среды. Например, если встать на надпись «Kernel.Mod» и нажать Alt-F7, то откроется исходный текст одного из модулей ядра системы. Файлы документации находятся в поддиректории док корневого каталога ЯОС.

Официальный репозиторий исходных текстов находится по адресу https://gitlab.com/budden/ja-o-s (в данной ОС веб-браузер не сможет открыть этот сложный сайт), 

]]></Span><Span style="Bold"><![CDATA[История и особенности

]]></Span><Span style="Normal"><![CDATA[ЯОС является проектом русификации операционной системы A2 (также известной как Bluebottle) из семейства Оберон-систем. Основная цель ЯОС - это создать учебно-экспериментальную операционную систему полностью на русском языке. Отличия от родительской системы можете посмотреть на сайте. ]]></Span><Span style="Bold"><![CDATA[ 

Графический интерфейс

]]></Span><Span style="Normal"><![CDATA[Графический интефрейс пользователя (ГПИ) в ЯОС в целом похож на любой настольный ГПИ, а также в нём есть некоторые особенности из Оберонов (например, запуск команд из любого текста). 

]]></Span><Span style="Bold"><![CDATA[	Управление рабочим столом с помощью клавиатуры и мыши]]></Span><Span style="Normal"><![CDATA[

		В WinAos, Linux32 и под QEMU, кнопка "Win" на современных клавиатурах выполняет роль кнопки Meta.
		Meta-Mouse-Wheel : zoom in/out (WinAos: Use Shift-Alt-Wheel)
		Meta-Mouse-Move : pan the viewport if the mouse hits the border of the screen (WinAos: not supported)
		Meta-PgUp : zoom in 2x
		Meta-PgDn : zoom out 2x
		Meta-Home : All desktop elements visible
		Meta-End : zoom to 1:1
		Meta-Left : Move viewport left by one screen in the current zoom factor
		Meta-Right : Move viewport right by one screen in the current zoom factor
		Meta-Up : Move viewport up by one screen in the current zoom factor
		Meta-Down : Move viewport down by one screen in the current zoom factor
		
		Alt+Return : Сделать окно на весь экран или обратно
		Ctrl-F4 : Закрыть окно
		Ctrl-W : закрыть вкладку в редакторе
		Alt+F7 : Открыть файл, место в файле или место ошибки компилятора
		F2/F3 : Переключиться к следующему или предыдущему приложению.
		Alt-F2 или Ctrl-F1: Показать/скрыть панель задач и главное меню.
		CTRL+SHIFT+1 или F4 : Переключиться в окно KernelLog
		F5 : - переключение рус/англ раскладки
		F12 (в среде разработки) - перейти к определению. 
		CTRL+SHIFT+O : Очистить KernelLog
		Ctrl-C, Ctrl-X, Ctrl-V - скопировать, вырезать, вставить
		Ctrl-Shift-V - вставить из буфера обмена хозяйской ОС (в режиме приложения)
		
		Alt-Shift is a sort of a substitute for the Meta key which works sometimes. ]]></Span><Span style="Comment"><![CDATA[(The meta key is often marked with symbol that is associated with big software vendor)]]></Span><Span style="Normal"><![CDATA[
		
	]]></Span><Span style="Bold"><![CDATA[   W]]></Span><Span style="Normal"><![CDATA[indows.Display.Mod.HandleKey содержит какие-то странные и глобальные для Windows действия:
		Кнопки F1, F9 - каким-то образом служат для имитации левой и правой    Meta. Левый Alt и Shift, нажатые одновременно, тоже вроде имитируют Meta,но в моей Windows это сочетание перехватывается ОС, так что не могу понять, как оно работает. 
		В Linux не работают сочетания Ctrl-0, Ctrl-1, ..., Ctrl-9.
		
	]]></Span><Span style="Bold"><![CDATA[The Main Menu

]]></Span><Span style="Normal"><![CDATA[		Ctrl-Esc : Move the main menu to the current screen (WinAos: Ctrl-Alt-Esc OR META+ESC; Linux - Ctrl-Alt-ESC)
		The menu is created using a number of MenuPage??.XML files, where ?? is a two digit decimal number corresponding to the page position in the menu.
		 00, 10, 20, ... 90 are reserved for the release
		Use any other number for a custom design page. 
		It is easiest to open an existing menu page and to edit it according to one's need.
		Example: ]]></Span><Span style="Debug"><![CDATA[Notepad.Open ]]></Span><Span style="Normal"><![CDATA[MenuPage10.XML ~    ]]></Span><Span style="Comment"><![CDATA[<-- the blue marked text is a command. You can click it with the middle mouse button to execute !]]></Span><Span style="Normal"><![CDATA[
		
]]></Span><Span style="Bold"><![CDATA[	Text Editors

]]></Span><Span style="Normal"><![CDATA[		]]></Span><Span style="Bold"><![CDATA[Mouse Commands:]]></Span><Span style="Normal"><![CDATA[

]]></Span><Span style="Bold"><![CDATA[			Navigation]]></Span><Span style="Normal"><![CDATA[
				Positioning the text cursor : Place the mouse cursor in the text and click the left mouse button
				Mouse-Wheel : Scroll up/down
]]></Span><Span style="Comment"><![CDATA[				]]></Span><Span style="Normal"><![CDATA[Alternatively, the scrollbar can be used for scrolling.
			]]></Span><Span style="Bold"><![CDATA[Selection]]></Span><Span style="Normal"><![CDATA[
				Place the mouse cursor at the beginning of the intended selection, press (and keep pressed) the left mouse button, move the mouse cursor 
				to the end of the intended selection, release the left mouse button
]]></Span><Span style="Bold"><![CDATA[			Drag and Drop
]]></Span><Span style="Normal"><![CDATA[				Select some text (see above), klick into the selected text with the left mouse button. Keep the button pressed and move the mouse to
				the position where to drag the text. If you want a copy to be dragged, keep the keyboard CTRL key pressed.
]]></Span><Span style="Bold"><![CDATA[			Starting Commands
]]></Span><Span style="Normal"><![CDATA[				Position the mouse cursor to a command, then click with the middle mouse button. Make sure command's parameters are delimited with the ~ character.
				OK : ]]></Span><Span style="Debug"><![CDATA[WMTetris.]]></Span><Span style="Normal"><![CDATA[Open ~ ]]></Span><Span style="Stupid"><![CDATA[ WMTetris.Open]]></Span><Span style="Normal"><![CDATA[ alone is not encouraged.
]]></Span><Span style="Comment"><![CDATA[				Since many commands can run in parallel, the parameters need to be copied before execution. 
				Although a command  works even without delimiter, it is a waste of memory.]]></Span><Span style="Normal"><![CDATA[
]]></Span><Span style="Bold"><![CDATA[			Opening Documents
]]></Span><Span style="Normal"><![CDATA[				Position the mouse cursor to a document name, then click with the middle mouse button, keep the button pressed, additionally press the right button, release both. (Middle-Right-Interclick)
]]></Span><Span style="Comment"><![CDATA[			]]></Span><Span style="Bold"><![CDATA[Copying a text style]]></Span><Span style="Normal"><![CDATA[
]]></Span><Span style="Comment"><![CDATA[			]]></Span><Span style="Normal"><![CDATA[	Select the text you want to apply the new style to. Hold down the Alt-key and click into a piece of text that contains the desired style.
]]></Span><Span style="Comment"><![CDATA[				The text where the format is copied from needs to have the keyboard focus, so that it can detect the Alt-key being pressed.]]></Span><Span style="Normal"><![CDATA[
			
]]></Span><Span style="Bold"><![CDATA[		Key Commands ]]></Span><Span style="Normal"><![CDATA[:

			]]></Span><Span style="Bold"><![CDATA[Navigation]]></Span><Span style="Normal"><![CDATA[
				Left, Right : move the text cursor left and right by one char. 
							Press the CTRL modifier key to move word by word
				Up, Down : Move the cursor one line up or down 
				PgUp, PgDn : Move cursor one visible page up/down
				Home, End : Begin or end of line.
							Press the CTRL-key to jump to the begin or end of the text
				SHIFT : The SHIFT modifier key can be combined with all the navigation keys above to navigate and select at the same time 
				CTRL-C	(or CTRL-INS) : Copy selection to clipboard	
				CTRL-V	(or SHIFT-INS) : Paste clipboard
				CTRL-W	Paste clipboard of host operating system
				CTRL-X	(or SHIFT-DELETE) : Cut (= copy to clipboard and delete text)
			
]]></Span><Span style="Bold"><![CDATA[			Starting Commands
]]></Span><Span style="Normal"><![CDATA[				Position the text cursor on a command. Press CTRL-Enter. ]]></Span><Span style="Comment"><![CDATA[See Starting Commands in the mouse commands for details.]]></Span><Span style="Normal"><![CDATA[  
			
			]]></Span><Span style="Bold"><![CDATA[Opening Document]]></Span><Span style="Normal"><![CDATA[s
				Position the text cursor on a document name. Press CTRL-Shift-Enter. ]]></Span><Span style="Comment"><![CDATA[See Opening Documents in the mouse commands for details.]]></Span><Span style="Normal"><![CDATA[  

]]></Span><Span style="Bold"><![CDATA[			Indentation 
]]></Span><Span style="Normal"><![CDATA[				Select the text to indent and then press TAB or SHIFT-TAB to indent the selection
				
]]></Span><Span style="Bold"><![CDATA[			Macros
]]></Span><Span style="Normal"><![CDATA[				Macros are invoked by pressing the Ins key while the cursor is placed behind a macro name.
				Macro parameters are separated by ":"
]]></Span><Span style="Debug"><![CDATA[				]]></Span><Span style="AdHoc Oberon 11 2 0 FFFFFFF 0000000"><![CDATA[Some helpful macros :]]></Span><Span style="Debug"><![CDATA[
					]]></Span><Span style="Normal"><![CDATA[name:M			]]></Span><Span style="Comment"><![CDATA[			MODULE ...]]></Span><Span style="Normal"><![CDATA[
					name:P			]]></Span><Span style="Comment"><![CDATA[			PROCEDURE ...]]></Span><Span style="Normal"><![CDATA[
					classname:O 	]]></Span><Span style="Comment"><![CDATA[			TYPE classname = OBJECT ...]]></Span><Span style="Normal"><![CDATA[
					classname:superclassname:o ]]></Span><Span style="Comment"><![CDATA[TYPE classname = OBJECT(superclassname) ...]]></Span><Span style="Normal"><![CDATA[
			]]></Span><Span style="AdHoc Oberon 11 2 0 FFFFFFF 0000000"><![CDATA[	output macros:]]></Span><Span style="Normal"><![CDATA[ oi, os, oc take one identifier parameter
					e.g. identifier:oi	]]></Span><Span style="Comment"><![CDATA[KernelLog.Int(identifier);]]></Span><Span style="Normal"><![CDATA[
					ol	output line takes no parameters
	]]></Span><Span style="Debug"><![CDATA[			]]></Span><Span style="AdHoc Oberon 11 2 0 FFFFFFF 0000000"><![CDATA[debug macros: ]]></Span><Span style="Normal"><![CDATA[di, ds, dc, dr take one identifier parameter
					e.g. identifier:ds	KernelLog.String("identifier = "); KernelLog.String(identifier);
]]></Span><Span style="Debug"><![CDATA[				]]></Span><Span style="AdHoc Oberon 11 2 0 FFFFFFF 0000000"><![CDATA[XML macros:]]></Span><Span style="Debug"><![CDATA[
]]></Span><Span style="Normal"><![CDATA[					name:t	<name></name>
					similar:T]]></Span><Span style="Bold"><![CDATA[
]]></Span><Span style="Normal"><![CDATA[
]]></Span><Span style="AdHoc Oberon 11 0 0 A0000FF 0000000"><![CDATA[				]]></Span><Span style="Normal"><![CDATA[The macros are defined in]]></Span><Span style="AdHoc Oberon 11 0 0 A0000FF 0000000"><![CDATA[:
]]></Span><Span style="Debug"><![CDATA[					Notepad.Open ]]></Span><Span style="Normal"><![CDATA[Macros.XML ~]]></Span><Span style="Bold"><![CDATA[
]]></Span><Span style="Debug"><![CDATA[					]]></Span><Span style="Bold"><![CDATA[
]]></Span><Span style="Normal"><![CDATA[		]]></Span><Span style="Bold"><![CDATA[Input Method Editors]]></Span><Span style="Normal"><![CDATA[

			Currently there are thre input mode editors, a rudimentary Pinyin Input Method Editor a Cyrillic input mode editor and a UnicodeIME.
			They can be installed with ]]></Span><Span style="Debug"><![CDATA[WMPinyinIME.Install]]></Span><Span style="Normal"><![CDATA[~ ]]></Span><Span style="Debug"><![CDATA[, WMCyrillicIME.Install ]]></Span><Span style="Normal"><![CDATA[~ or ]]></Span><Span style="Debug"><![CDATA[WMUnicodeIME.Install ]]></Span><Span style="Normal"><![CDATA[~
			After an input mode editor is installed, press CTRL-Space in any editor to enable/disable it (.      F5  ).
			To automatically load an IME, edit the Configuration.XML file. (]]></Span><Span style="Debug"><![CDATA[ Notepad.Open ]]></Span><Span style="Normal"><![CDATA[Configuration.XML ~ )
	
]]></Span><Span style="Bold"><![CDATA[	Other Functions

		Storing the desktop
]]></Span><Span style="Normal"><![CDATA[			In the ]]></Span><Span style="Highlight"><![CDATA[System ]]></Span><Span style="Normal"><![CDATA[menu, press the]]></Span><Span style="Highlight"><![CDATA[ SaveDesktop ]]></Span><Span style="Comment"><![CDATA[(mark a waypoint) ]]></Span><Span style="Normal"><![CDATA[button to store the state of the programs on the desktop ]]></Span><Span style="Comment"><![CDATA[(not all programs support saving their state)]]></Span><Span style="Normal"><![CDATA[
			
		]]></Span><Span style="Bold"><![CDATA[Reboot the system ]]></Span><Span style="Stupid"><![CDATA[
]]></Span><Span style="Normal"><![CDATA[			]]></Span><Span style="Stupid"><![CDATA[Ctrl-Alt-Del (terminates the system and reboots without warning !!!)]]></Span><Span style="Normal"><![CDATA[
			
		]]></Span><Span style="Bold"><![CDATA[Backdrop images]]></Span><Span style="Normal"><![CDATA[
			Backdrop images can be added at any place and in any size on the virtual desktop. Clicking on a backdrop will zoom the backdrop to fullscreen.
			Backdrops can be removed by clicking them with the right mouse button and selecting ]]></Span><Span style="Highlight"><![CDATA[Remove]]></Span><Span style="Normal"><![CDATA[.
]]></Span><Span style="Comment"><![CDATA[			Hint: Try not to put too many backdrops at the same position on the virtual desktop
]]></Span><Span style="Normal"><![CDATA[			To make the backdrop permanent, you can just save store the desktop by pressing the ]]></Span><Span style="Highlight"><![CDATA[SaveDesktop ]]></Span><Span style="Normal"><![CDATA[button in the ]]></Span><Span style="Highlight"><![CDATA[System ]]></Span><Span style="Normal"><![CDATA[menu
		
		]]></Span><Span style="Bold"><![CDATA[Window Styles]]></Span><Span style="Normal"><![CDATA[
			The window style can be changed in the ]]></Span><Span style="Highlight"><![CDATA[Looks ]]></Span><Span style="Normal"><![CDATA[menu. To make a style permanent, enter the corresponding command in the Configuration.XML ]]></Span><Span style="Highlight"><![CDATA[Autostart ]]></Span><Span style="Normal"><![CDATA[section or
			use ]]></Span><Span style="Highlight"><![CDATA[SaveDesktop ]]></Span><Span style="Normal"><![CDATA[in the ]]></Span><Span style="Highlight"><![CDATA[System ]]></Span><Span style="Normal"><![CDATA[menu


]]></Span><Span style="Bold"><![CDATA[	Fonts
]]></Span><Span style="Normal"><![CDATA[		Unicode TTF fonts are supported. You can install ttf files by simply copying the file. The file name must be the same as the
		font name. You can download a free font by Cyberbit using this tool: ]]></Span><Span style="Debug"><![CDATA[CyberbitNetInstall.Start]]></Span><Span style="Normal"><![CDATA[ ~ (6MB download. Requires 13MB on the disk)]]></Span><Span style="Bold"><![CDATA[


Mounting Additional Drives and Partitions

]]></Span><Span style="Normal"><![CDATA[Mounting of new filesystems is done with the FSTools module. The following commands are important:

	FSTools.Mount prefix filesystem partition ~ 
	FSTools.SetDefault prefix ~
	FSTools.Unmount prefix ~
	]]></Span><Span style="Debug"><![CDATA[FSTools.Watch ]]></Span><Span style="Normal"><![CDATA[~

Examples:
]]></Span><Span style="Comment"><![CDATA[	]]></Span><Span style="Normal"><![CDATA[To mount a FAT formatted USB-Stick ]]></Span><Span style="Comment"><![CDATA[(assuming a single standard partitioned and configured USB stick)
]]></Span><Span style="Normal"><![CDATA[		]]></Span><Span style="Debug"><![CDATA[FSTools.Mount ]]></Span><Span style="Normal"><![CDATA[USB FatFS USB0#1~
	To unmount the USB stick
		]]></Span><Span style="Debug"><![CDATA[FSTools.Unmount]]></Span><Span style="Normal"><![CDATA[ USB~]]></Span><Span style="Bold"><![CDATA[
]]></Span><Span style="Normal"><![CDATA[
Программирование

		Редактор для программиста называется ИСР. 
		]]></Span><Span style="Debug"><![CDATA[ИСР.]]></Span><Span style="Normal"><![CDATA[Открой WMShell.Mod ~
		Также он доступен из системного меню (Корень/Разработка/среда разработки)
		
		В редакторе нажмите F1 для получения справки. 
		
		
		Errors appear at the bottom of the window. Click on an error to position the cursor at the corresponding position in the source text..
		
		To find a position of a Trap, select the the number ???? behind PC=???? in the kernel log. Then press the find PC.
		If no PC position is selected, PET will ask
		
		The left part of the window gives a structural overview over your program. 
		Click into the tree to position the cursor at the position of the clicked structure in the program text.
		
		To free a module, call SystemTools.Free HelloWorld ~
		
]]></Span><Span style="Bold"><![CDATA[Miscellaneous]]></Span><Span style="Normal"><![CDATA[

		Bluebottle contains lots of tools, some did not find into the system menu, e.g:

			TV:			A TV Application for BT848 compatible TV cards. (]]></Span><Span style="Debug"><![CDATA[ Notepad.Open ]]></Span><Span style="Normal"><![CDATA[TV.Tool ~ )

			Sound:			Currently supported sound cards: 
								]]></Span><Span style="Debug"><![CDATA[i810Sound.Install]]></Span><Span style="Normal"><![CDATA[ ~  (Intel/Realtek compatibles)
]]></Span><Span style="Debug"><![CDATA[								EnsoniqSound.Install ]]></Span><Span style="Normal"><![CDATA[~ 
]]></Span><Span style="Debug"><![CDATA[								YMF754.Install ~ ]]></Span><Span style="Normal"><![CDATA[(Yamaha)]]></Span><Span style="Debug"><![CDATA[
]]></Span><Span style="Normal"><![CDATA[
			MP3Player : 	See ]]></Span><Span style="Debug"><![CDATA[PET.Open ]]></Span><Span style="Normal"><![CDATA[MP3Player.Mod ~
	
			OGGPlayer :	]]></Span><Span style="Debug"><![CDATA[OGGVorbisPlayer.Play]]></Span><Span style="Normal"><![CDATA[ somfile.ogg ~
			
			Network : 		Currently supported network cards:
]]></Span><Span style="Debug"><![CDATA[								RTL8139.Install ~ ]]></Span><Span style="Normal"><![CDATA[(Realtek 100MB/s card)]]></Span><Span style="Debug"><![CDATA[
								RTL8169.Install ~ ]]></Span><Span style="Normal"><![CDATA[(Realtek 1GB/s card)]]></Span><Span style="Debug"><![CDATA[
								Ethernet3Com90x.Install ~
								Intel8255x.Install ~
]]></Span><Span style="Normal"><![CDATA[							Check: Network.Mod, IP.Mod, UDP.Mod, DNS.Mod, TCP.Mod
								
			Webserver: 	HTTP/1.1 plugin enabled multi host webserver.  See ]]></Span><Span style="Debug"><![CDATA[PET.Open ]]></Span><Span style="Normal"><![CDATA[WebHTTPServer.Mod ~

			FTP-Server:	See ]]></Span><Span style="Debug"><![CDATA[PET.Open ]]></Span><Span style="Normal"><![CDATA[WebFTPServer.Mod ~ 
			
			USB	: 		Support for Mouse and many storage devices. 
							The system is bootable from some usb sticks.
							See ]]></Span><Span style="Debug"><![CDATA[PET.Open]]></Span><Span style="Normal"><![CDATA[ Usb.Text ~
			
			ARM/XSCALE : If you are interested in the ARM/XSCALE ports write an email to ask@bluebottle.ethz.ch (check the website before ! ;-) )
			
]]></Span><Span style="Assertion"><![CDATA[		and many more... you have the source... have fun!]]></Span>

</Text>
