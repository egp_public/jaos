модуль SortDemo;	(** AUTHOR "GF"; PURPOSE "animated sort demo" *)

(* this is an extended A2-port of the SortDemo done by W.Weck in 1993 for Oberon V4 *)

использует
	Raster, Random, WMRectangles, Strings, WMMessages, WMDialogs,
	WM := WMWindowManager, WMComponents, WMStandardComponents,
	Log := ЛогЯдра, Clock, Kernel, ЭВМ;

конст
	N = 120;  ElemSize = 5;
	MaxConcurrentSorters = 5;	(* assuming we have 6 processor cores *)
	WindowSize = N*ElemSize;

тип
	ElementType = цел32;
	Index = цел16;
	SortValues = массив N из ElementType;

	SortArray =  окласс (WM.BufferWindow)
	перем
		cw: ControlWindow;
		white, grey, col1, yellow: Raster.Pixel;

		mode: Raster.Mode;

		sortname: массив 32 из симв8;
		delay: цел32;
		ha, hb: Index;  (* highlighted elements *)
		concurrent: булево;  concSorters: цел32;
		nofcomps, nofswaps, nofmoves: цел32;
		compareWeight, swapWeight: вещ64; (* in relation to element move *)
		timer: Kernel.Timer;

		data, backup: SortValues;
		highlighted: массив N из булево;


		проц &New( win: ControlWindow );
		нач
			cw := win;
			Init( WindowSize, WindowSize, ложь );
			manager := WM.GetDefaultManager();
			manager.Add( 40, 250, сам, {WM.FlagFrame, WM.FlagClose, WM.FlagNoResizing} );
			SetTitle( Strings.NewString( "SortArray" ) );
			Raster.SetRGB( white, 255, 255, 255 );
			Raster.SetRGB( yellow, 255, 255, 0 );
			Raster.SetRGB( grey, 110, 110, 110 );
			Raster.SetRGB( col1, 210, 140, 75 );

			Raster.InitMode( mode, Raster.srcCopy );

			concSorters :=  0;  concurrent := ложь;
			delay := 16;  нов( timer );
			compareWeight := 3.0;  swapWeight := 2.4;
			ShowWeightings;

			OrderData;  backup :=data;
		кон New;


		проц {перекрыта}Handle*( перем x: WMMessages.Message );
		нач
			если x.msgType = WMMessages.MsgClose то  cw.Close  иначе  Handle^( x )  всё
		кон Handle;

		проц Pause;
		перем t: Kernel.Timer;
		нач
			если delay > 0 то
				если ~concurrent то  timer.Sleep( delay )
				иначе
					нов( t );  t.Sleep( delay )
				всё
			всё
		кон Pause;

		проц ShowWeightings;
		перем digits: массив 8 из симв8;
		нач
			Log.пВК_ПС;
			Log.пСтроку8( "SortDemo weightings: element move = 1, element swap =");
			Strings.FloatToStr( swapWeight, 3, 1, 0, digits );
			Log.пСтроку8( digits );
			Log.пСтроку8( ", compare =" );
			Strings.FloatToStr( compareWeight, 3, 1, 0, digits );
			Log.пСтроку8( digits );  Log.пВК_ПС;
		кон ShowWeightings;

		проц AdjWeightings;
		перем digits: массив 8 из симв8;
		нач
			Strings.FloatToStr( compareWeight, 3, 1, 0, digits );
			если WMDialogs.QueryString( "Input compare weight", digits ) = 0 то
				Strings.StrToFloat( digits, compareWeight )
			всё;
			Strings.FloatToStr( swapWeight, 3, 1, 0, digits );
			если WMDialogs.QueryString( "Input swap weight", digits ) = 0 то
				Strings.StrToFloat( digits, swapWeight )
			всё;
			ShowWeightings
		кон AdjWeightings;


		проц InitSort;
		нач
			nofcomps := 0;  nofswaps := 0;  nofmoves := 0;
			backup := data
		кон InitSort;

		проц FinishSort;
		нач
			UnHighlight( ha ); UnHighlight( hb );
			Log.пСтроку8( sortname );  Log.пСтроку8( ":  " );
			Log.пЦел64( nofcomps, 1 );  Log.пСтроку8( " compares,  " );
			Log.пЦел64( nofswaps, 1 );  Log.пСтроку8( " swaps,  " );
			Log.пЦел64( nofmoves, 1 );  Log.пСтроку8( " moves,  total effort: " );
			Log.пЦел64( округлиВниз( nofcomps*compareWeight) + округлиВниз(nofswaps*swapWeight) + nofmoves, 0 );  Log.пВК_ПС;
		кон FinishSort;


		проц DrawElement( n: Index );
		перем
			x, y, len: цел32;
		нач
			len := data[n];
			x := ElemSize*n;  y := WindowSize - 1 - ElemSize*len;
			если len < N то  Raster.Fill( img, x, 0, x+ElemSize, y, white, mode )  всё;
			Raster.Fill( img, x, y+1, x+ElemSize, y+ElemSize+1, col1, mode );
			если len > 1 то
				Raster.Fill( img, x, y+ElemSize+1, x+1, WindowSize, white, mode );
				Raster.Fill( img, x+1, y+ElemSize+1, x+ElemSize-1, WindowSize, grey, mode );
				Raster.Fill( img, x+ElemSize-1, y+ElemSize+1, x+ElemSize, WindowSize, white, mode );
			всё;
			Invalidate( WMRectangles.MakeRect( x, 0, x+ElemSize, WindowSize ) );
			highlighted[n] := ложь;
		кон DrawElement;


		проц UnHighlight( n: Index );
		перем
			x, y, len: цел32;
		нач
			если highlighted[n] то
				len := data[n];
				x := ElemSize*n;  y := WindowSize - 1 - ElemSize*len;
				если len > 1 то
					Raster.Fill( img, x+1, y+ElemSize+1, x+ElemSize-1, WindowSize, grey, mode );
				всё;
				Invalidate( WMRectangles.MakeRect( x, 0, x+ElemSize, WindowSize ) );
				highlighted[n] := ложь;
			всё
		кон UnHighlight;

		проц Highlight( n: Index );
		перем
			x, y, len: цел32;
		нач
			если ~highlighted[n] то
				len := data[n];
				x := ElemSize*n;  y := WindowSize - 1 - ElemSize*len;
				если len > 1 то
					Raster.Fill( img, x+1, y+ElemSize+1, x+ElemSize-1, WindowSize, yellow, mode )
				всё;
				Invalidate( WMRectangles.MakeRect( x, 0, x+ElemSize, WindowSize ) );
				highlighted[n] := истина;
			всё
		кон Highlight;

		проц Highlight2( a, b: Index );
		нач
			если ~concurrent то
				если (ha # a) и (ha # b) то  UnHighlight( ha )  всё;
				если (hb # a) и (hb # b) то  UnHighlight( hb )  всё;
			всё;
			Highlight( a );  Highlight( b );
			ha := a;  hb := b;
			Pause
		кон Highlight2;


		проц Randomize( n: цел16 );
		перем i, j, k: Index;
			random: Random.Generator;
			t, d: цел32;
		нач
			нов( random );  Clock.Get( t, d );  random.InitSeed( t );
			нцДля i := 1 до n делай
				j := устарПреобразуйКБолееУзкомуЦел( random.Dice( N ) );
				k := устарПреобразуйКБолееУзкомуЦел( random.Dice( N ) );
				Swap( j, k );
			кц
		кон Randomize;


		проц OrderData;
		перем i: Index;
		нач
			нцДля i := 0 до N-1 делай  data[i] := i + 1;  DrawElement( i )  кц;
		кон OrderData;

		проц RevOrderData;
		перем i: Index;
		нач
			нцДля i := 0 до N-1 делай  data[i] := N - i;  DrawElement( i )  кц;
		кон RevOrderData;

		проц BadOrder;	(* worst case for quicksort *)
		перем i, m: Index;
		нач
			m := (N - 1) DIV 2;
			нцДля i := 0 до m-1 делай  data[i] := i + 1  кц;
			data[m] := N;
			нцДля i := m+1 до N-1 делай  data[i] := i   кц;
			нцДля i := 0 до N-1 делай  DrawElement( i )  кц;
		кон BadOrder;

		проц PrevOrder;
		перем i: Index;
		нач
			data := backup;
			нцДля i := 0 до N-1 делай  DrawElement( i )  кц;
		кон PrevOrder;


		проц DecSpeed;
		нач
			если delay # 0 то  delay := 2*delay  иначе  delay := 4 всё;
		кон DecSpeed;

		проц IncSpeed;
		нач
			если delay > 4 то  delay := delay DIV 2   иначе  delay := 0 всё;
		кон IncSpeed;



		проц Swap( i, j: Index );
		перем tmp: цел32;
		нач
			если i # j то
				tmp := data[i];  data[i] := data[j];  data[j] := tmp;
				DrawElement( i );  DrawElement( j );
				ЭВМ.атомарноУвел( nofswaps )
			всё
		кон Swap;


		проц Less( i, j: Index ): булево;
		нач
			если delay > 0 то
				Highlight2( i, j );
			всё;
			ЭВМ.атомарноУвел( nofcomps );
			возврат data[i] < data[j];
		кон Less;



		проц BubbleSort( lo, hi: Index );
		перем i: Index;  swaps: цел32;
		нач
			нцДо
				swaps := 0;
				нцДля i := lo до hi - 1 делай
					если Less( i + 1, i )  то  Swap( i, i + 1 );  увел( swaps )  всё
				кц;
			кцПри swaps = 0
		кон BubbleSort;


		проц BubbleSortOptim( lo, hi: Index );
		перем i, first, last: Index;  swaps: цел32;
		нач
			first := lo;  last := hi - 1;
			нц
				swaps := 0;
				нцДля i := first до last делай
					если Less( i + 1, i )  то  Swap( i, i + 1 );  увел( swaps );  last := i  всё
				кц;
				если swaps = 0 то  прервиЦикл  всё;
				swaps := 0;
				нцДля i := last до first + 1 шаг -1 делай
					если Less( i, i - 1 )  то  Swap( i, i - 1 );  увел( swaps );  first := i  всё
				кц;
				если swaps = 0 то  прервиЦикл  всё;
			кц
		кон BubbleSortOptim;


		проц SelectSort( lo, hi: Index );
		перем i, j, min: Index;
		нач
			нцДля i := lo до hi делай
				min := i;
				нцДля j := i + 1 до hi делай
					если Less( j, min ) то  min := j  всё
				кц;
				если i # min то  Swap( i, min )  всё
			кц
		кон SelectSort;


		проц ShellSort( lo, hi: Index );
		перем i, j, h: Index;
		нач
			i := 4;  h := 1;
			нцПока (lo+i) <= hi делай  i := i*2;  h := h*2 + 1  кц;
			нцПока h # 0 делай
				i := lo + h;
				нцПока i <= hi делай
					j := i - h;
					нцПока (j >= lo) и Less( j + h, j ) делай  Swap( j, j + h );  j := j - h  кц;
					увел( i )
				кц;
				h := (h - 1) DIV 2
			кц;
		кон ShellSort;


		проц InsertSort( lo, hi: Index );
		перем
			x, l, m, ip, i: Index;
			tmp: ElementType;
		нач
			x := lo + 1;
			нцПока x <= hi делай
				если Less( x, x - 1 )то
					(* find insert position ip, binary search *)
					ip := x - 1;  l := lo;
					нцПока l < ip делай
						m := (l + ip) DIV 2;
						если Less( x, m ) то  ip := m  иначе  l := m + 1  всё
					кц;
					(* insert data[x] at position ip *)
					tmp := data[x];  i := x;
					нцДо
						data[i] := data[i - 1];  DrawElement( i );  увел( nofmoves );  умень( i )
					кцПри i = ip;
					data[ip] := tmp;  DrawElement( ip );  Pause;
					умень( nofmoves );  увел( nofswaps )
				всё;
				увел( x )
			кц
		кон InsertSort;


		проц QuickSort( lo, hi: Index );
		перем
			i, j, m: Index;
		нач
			если lo < hi то
				i := lo;  j := hi;  m := (lo + hi) DIV 2;
				нцДо
					нцПока Less( i, m ) делай  увел( i )  кц;
					нцПока Less( m, j ) делай  умень( j )  кц;
					если i <= j то
						если m = i то  m := j  аесли m = j то  m := i  всё;
						Swap( i, j );  увел( i );  умень( j )
					всё
				кцПри i > j;
				QuickSort( lo, j );  QuickSort( i, hi )
			всё;
		кон QuickSort;



		проц QuickSortOptim( lo, hi: Index );
		перем i, j, m: Index;  n: цел32;
		нач
			если lo < hi то
				n := hi - lo + 1;
				если n = 2 то
					если Less( hi, lo ) то  Swap( lo, hi )  всё
				аесли (n < 16) и (compareWeight > 1.7) то
					InsertSort( lo, hi )  (* less expensive compares! *)
				иначе
					(* QuickSort *)
					i := lo;  j := hi;  m := (lo + hi) DIV 2;
					нцДо
						нцПока Less( i, m ) делай  увел( i )  кц;
						нцПока Less( m, j ) делай  умень( j )  кц;
						если i <= j то
							если m = i то  m := j  аесли m = j то  m := i  всё;
							Swap( i, j );  увел( i );  умень( j )
						всё
					кцПри i > j;
					QuickSortOptim( lo, j );  QuickSortOptim( i, hi )
				всё
			всё;
		кон QuickSortOptim;


		проц QuickSortConc( lo, hi: Index );
		перем
			i, j, m: Index;
			csorter: ConcurrentSorter;
		нач
			если lo < hi то
				i := lo;  j := hi;  m := (lo + hi) DIV 2;
				нцДо
					нцПока Less( i, m ) делай  UnHighlight( i );  увел( i )  кц;  UnHighlight( i );
					нцПока Less( m, j ) делай  UnHighlight( j );  умень( j )  кц;  UnHighlight( j );
					UnHighlight( m );
					если i <= j то
						если m = i то  m := j  аесли m = j то  m := i  всё;
						Swap( i, j );  увел( i );  умень( j )
					всё
				кцПри i > j;
				если concSorters < MaxConcurrentSorters то
					нов( csorter, сам, lo, j );  QuickSortConc( i, hi )
				иначе
					QuickSortConc( lo, j );  QuickSortConc( i, hi )
				всё
			всё;
		кон QuickSortConc;


		проц DoBubbleSort;
		нач
			sortname := "BubbleSort";
			BubbleSort( 0, N-1 )
		кон DoBubbleSort;

		проц DoBubbleSortOptim;
		нач
			sortname := "BubbleSort optim.";
			BubbleSortOptim( 0, N-1 )
		кон DoBubbleSortOptim;

		проц DoSelectSort;
		нач
			sortname := "SelectSort";
			SelectSort( 0, N-1 )
		кон DoSelectSort;


		проц DoShellSort;
		нач
			sortname := "ShellSort";
			ShellSort( 0, N-1 )
		кон DoShellSort;


		проц DoInsertSort;
		нач
			sortname := "InsertSort";
			InsertSort( 0, N-1 )
		кон DoInsertSort;


		проц DoQuickSort;
		нач
			sortname := "QuickSort";
			QuickSort( 0, N-1 );
		кон DoQuickSort;

		проц DoQuickSortOptim;
		нач
			sortname := "QuickSort optim.";
			QuickSortOptim( 0, N - 1 );
		кон DoQuickSortOptim;

		проц DoQuickSortConc;
		перем t: Kernel.Timer;
		нач
			sortname := "QuickSort conc.";
			concurrent := истина;  concSorters := 0;
			QuickSortConc( 0, N-1 );
			(* now wait until all concurrent activities have finished *)
			нов( t );  нцПока concSorters > 0 делай  t.Sleep( 50 )  кц;
			concurrent := ложь;
		кон DoQuickSortConc;



		проц HeapSort;
		перем l, r: Index;

			проц Sift( l, r: Index );
			перем i, j: Index;
			нач
				i := l;  j := 2*l + 1;
				если (j + 1 < r) и Less( j, j + 1 ) то  увел( j )  всё;
				нцПока (j < r) и ~Less( j, i ) делай
					Swap( i, j );
					i := j;  j := 2*j + 1;
					если (j + 1 < r) и Less( j, j + 1 ) то  увел( j )  всё
				кц
			кон Sift;

		нач
			sortname := "HeapSort";
			r := N;  l := N DIV 2;
			нцПока l > 0 делай  умень( l );  Sift( l, r )  кц;
			нцПока r > 0 делай  умень( r );  Swap( 0, r );  Sift( 0, r )  кц;
		кон HeapSort;



		проц SmoothSort;  	(* W.Weck 21 Jan 93, SmoothSort due to E.W.Dijkstra, J.Gutknecht *)
		перем q, r, p, b, c: Index;

			проц up( перем b, c: Index );
			перем b1: Index;
			нач  b1 := b;  b := b + c + 1;  c := b1
			кон up;

			проц down( перем b, c: Index );
			перем c1: Index;
			нач  c1 := c;  c := b - c - 1;  b := c1
			кон down;

			проц sift( r, b, c: Index );
			перем r1: Index;
			нач
				нцПока b >= 3 делай  r1 := r - b + c;
					если Less( r1, r - 1 ) то  r1 := r - 1;  down( b, c )  всё;
					если Less( r, r1 ) то  Swap( r, r1 );  r := r1;  down( b, c )  иначе  b := 1  всё
				кц
			кон sift;

			проц trinkle( r, p, b, c: Index );
			перем r1, r2: Index;
			нач
				нцПока p > 0 делай
					нцПока ~нечётноеЛи¿( p ) делай  p := p DIV 2;  up( b, c )  кц;
					r2 := r - b;
					если (p = 1) или ~Less( r, r2 ) то  p := 0
					иначе  p := p - 1;
						если b = 1 то  Swap( r, r2 );  r := r2
						иначе  r1 := r - b + c;
							если Less( r1, r - 1 ) то  r1 := r - 1;  down( b, c );  p := p*2  всё;
							если ~Less( r2, r1 ) то  Swap( r, r2 );  r := r2  иначе  Swap( r, r1 );  r := r1;  down( b, c );  p := 0  всё
						всё
					всё
				кц;
				sift( r, b, c )
			кон trinkle;

			проц semiTrinkle( r, p, b, c: Index );
			перем r1: Index;
			нач  r1 := r - c;
				если Less( r, r1 ) то  Swap( r, r1 );  trinkle( r1, p, b, c )  всё
			кон semiTrinkle;

		нач
			sortname := "SmoothSort";
			q := 1;  r := 0;  p := 1;  b := 1;  c := 1;
			нцПока q # N делай
				если p остОтДеленияНа 8 = 3 (* p = ... 011 *) то
					sift( r, b, c );  p := (p + 1) DIV 4;  up( b, c );  up( b, c ) (* b >= 3 *)
				иначе  (* p = ... 01 *)
					если (q + c) < N то  sift( r, b, c )  иначе  trinkle( r, p, b, c )  всё;
					down( b, c );  p := p*2;
					нцПока b # 1 делай  down( b, c );  p := p*2  кц;
					p := p + 1
				всё;
				q := q + 1;  r := r + 1
			кц;
			trinkle( r, p, b, c );
			нцПока q # 1 делай  q := q - 1;  p := p - 1;
				если b = 1 то  r := r - 1;
					нцПока ~нечётноеЛи¿( p ) делай  p := p DIV 2;  up( b, c )  кц
				иначе  (* b >= 3 *)  r := r - b + c;
					если p > 0 то  semiTrinkle( r, p, b, c )  всё;
					down( b, c );  p := p*2 + 1;  r := r + c;  semiTrinkle( r, p, b, c );  down( b, c );  p := p*2 + 1
				всё
			кц;
		кон SmoothSort;


	кон SortArray;


тип
	ConcurrentSorter = окласс
	перем
		lo, hi: Index;
		av: SortArray;

		проц &Init ( a: SortArray;  low, high: Index );
		нач
			av := a;
			lo := low; hi := high;
		кон Init;

	нач {активное}
		ЭВМ.атомарноУвел( av.concSorters );
		av.QuickSortConc( lo, hi );
		ЭВМ.атомарноУмень( av.concSorters )
	кон ConcurrentSorter;


тип
	SortStarter = окласс
	тип
		SortProcedure = проц {делегат};
	перем
		sort: SortProcedure;
		av: SortArray;
		running, terminated: булево;

		проц &Init( a: SortArray );
		нач
			sort := НУЛЬ;  av := a;
			running := ложь;  terminated := ложь
		кон Init;

		проц Start( proc: SortProcedure );
		нач
			если ~running то
				нач {единолично}  sort := proc  кон
			всё
		кон Start;

		проц Terminate;
		нач {единолично}
			terminated := истина
		кон Terminate;

	нач {активное}
		нач {единолично}
			нцДо
				дождись( (sort # НУЛЬ) или terminated );
				если ~terminated то
					running := истина;
					av.InitSort;  sort;  av.FinishSort;
					running := ложь;
				всё;
				sort := НУЛЬ
			кцПри terminated
		кон
	кон SortStarter;

тип
	ControlWindow = окласс( WMComponents.FormWindow )
		перем
			toolbar: WMStandardComponents.Panel;
			button : WMStandardComponents.Button;

			av: SortArray;
			sorter: SortStarter;


			проц &New;
			перем vc: WMComponents.VisualComponent;
			нач
				vc := CreateForm();
				Init( vc.bounds.GetWidth(), vc.bounds.GetHeight(), ложь );
				SetContent( vc );
				SetTitle( WM.NewString( "Sort Demo" ) );
				WM.DefaultAddWindow( сам );
				нов( av, сам );
				нов( sorter, av )
			кон New;

			проц {перекрыта}Handle*( перем x: WMMessages.Message );
			нач
				если x.msgType = WMMessages.MsgClose то  Close  иначе  Handle^( x )  всё
			кон Handle;

			проц {перекрыта}Close*;
			нач
				sorter.Terminate;  av.Close;  Close^
			кон Close;


			проц CreateForm( ): WMComponents.VisualComponent;
			перем
				panel: WMStandardComponents.Panel;
				label : WMStandardComponents.Label;
			нач
				нов( panel );
					panel.bounds.SetWidth( 560 );
					panel.bounds.SetHeight( 80 );
					panel.fillColor.Set( цел32( 0FFFFFFFFH ) );


				нов( toolbar );
					toolbar.bounds.SetHeight( 20 );
					toolbar.alignment.Set( WMComponents.AlignTop );
					toolbar.fillColor.Set( цел32( 0CCCCCCFFH ) );

				нов( label );
					label.bounds.SetWidth( 70 );
					label.alignment.Set( WMComponents.AlignLeft );
					label.caption.SetAOC( " Array init: " );
					label.textColor.Set( 0000000FFH );
				toolbar.AddContent(label);


				нов( button );
					button.bounds.SetWidth( 70 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( " order " );
					button.onClick.Add( Order );
				toolbar.AddContent( button );

				нов( button );
					button.bounds.SetWidth( 70 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( " rev. order " );
					button.onClick.Add( RevOrder );
				toolbar.AddContent( button );

				нов( button );
					button.bounds.SetWidth( 70 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( "bad order" );
					button.onClick.Add( BadOrder );
				toolbar.AddContent( button );

				нов( button );
					button.bounds.SetWidth( 70 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( "prev. order" );
					button.onClick.Add( PrevOrder );
				toolbar.AddContent( button );

				нов( button );
					button.bounds.SetWidth( 70 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( " rand 10 " );
					button.onClick.Add( Rand10 );
				toolbar.AddContent( button );

				нов( button );
					button.bounds.SetWidth( 70 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( " rand 100 " );
					button.onClick.Add( Rand100 );
				toolbar.AddContent( button );

				нов( button );
					button.bounds.SetWidth( 70 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( " rand 200 " );
					button.onClick.Add( Rand200 );
				toolbar.AddContent( button );

				panel.AddContent( toolbar );

				нов( toolbar );
					toolbar.bounds.SetHeight( 20 );
					toolbar.alignment.Set( WMComponents.AlignTop );
					toolbar.fillColor.Set( цел32( 0CCCCCCFFH ) );

				нов( label );
					label.bounds.SetWidth( 70 );
					label.alignment.Set( WMComponents.AlignLeft );
					label.caption.SetAOC( " Sorter: " );
					label.textColor.Set( 0000000FFH );
				toolbar.AddContent(label);


				нов( button );
					button.bounds.SetWidth( 80 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( " Bubble " );
					button.onClick.Add( StartBubbleSort );
				toolbar.AddContent( button );

				нов( button );
					button.bounds.SetWidth( 80 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( " Select " );
					button.onClick.Add( StartSelectSort );
				toolbar.AddContent( button );

				нов( button );
					button.bounds.SetWidth( 80 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( " Shell " );
					button.onClick.Add( StartShellSort );
				toolbar.AddContent( button );

				нов( button );
					button.bounds.SetWidth( 90 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( " Quick " );
					button.onClick.Add( StartQuickSort );
				toolbar.AddContent( button );

				нов( button );
					button.bounds.SetWidth( 80 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( " Heap " );
					button.onClick.Add( StartHeapSort );
				toolbar.AddContent( button );

				нов( button );
					button.bounds.SetWidth( 80 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( " Smooth " );
					button.onClick.Add( StartSmoothSort );
				toolbar.AddContent( button );

				panel.AddContent( toolbar );


				нов( toolbar );
					toolbar.bounds.SetHeight( 20 );
					toolbar.alignment.Set( WMComponents.AlignTop );
					toolbar.fillColor.Set( цел32( 0CCCCCCFFH ) );

				нов( label );
					label.bounds.SetWidth( 70 );
					label.alignment.Set( WMComponents.AlignLeft );
					label.caption.SetAOC( " Sorter: " );
					label.textColor.Set( 0000000FFH );
				toolbar.AddContent(label);


				нов( button );
					button.bounds.SetWidth( 80 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( "opt Bubble" );
					button.onClick.Add( StartBubbleSortOptim );
				toolbar.AddContent( button );

				нов( button );
					button.bounds.SetWidth( 80 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( "Insert" );
					button.onClick.Add( StartInsertSort );
				toolbar.AddContent( button );

				нов( label );
					label.bounds.SetWidth( 80 );
					label.alignment.Set( WMComponents.AlignLeft );
					label.textColor.Set( 0000000FFH );
				toolbar.AddContent(label);

				нов( button );
					button.bounds.SetWidth( 125 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( " optim. Quick " );
					button.onClick.Add( StartQuickSortOptim );
				toolbar.AddContent( button );

				нов( button );
					button.bounds.SetWidth( 125 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( " concurrent Quick " );
					button.onClick.Add( StartQuickSortConc );
				toolbar.AddContent( button );

				panel.AddContent( toolbar );



				нов( toolbar );
					toolbar.bounds.SetHeight( 20 );
					toolbar.alignment.Set( WMComponents.AlignTop );
					toolbar.fillColor.Set( цел32( 0CCCCCCFFH ) );

				нов( label );
					label.bounds.SetWidth( 70 );
					label.alignment.Set( WMComponents.AlignLeft );
					label.caption.SetAOC( " Speed: " );
					label.textColor.Set( 0000000FFH );
				toolbar.AddContent(label);


				нов( button );
					button.bounds.SetWidth( 40 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( " - " );
					button.onClick.Add( DecSpeed );
				toolbar.AddContent( button );


				нов( button );
					button.bounds.SetWidth( 40 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( " + " );
					button.onClick.Add( IncSpeed );
				toolbar.AddContent( button );


				нов( label );
					label.bounds.SetWidth( 80 );
					label.alignment.Set( WMComponents.AlignLeft );
					label.textColor.Set( 0000000FFH );
				toolbar.AddContent(label);

				нов( button );
					button.bounds.SetWidth( 160 );
					button.alignment.Set( WMComponents.AlignLeft );
					button.caption.SetAOC( "adjust weightings" );
					button.onClick.Add( AdjWeightings );
				toolbar.AddContent( button );


				panel.AddContent( toolbar );

				возврат panel
			кон CreateForm;


			проц Order( sender, data: динамическиТипизированныйУкль );
			нач
				если ~ sorter.running то  av.OrderData  всё
			кон Order;

			проц RevOrder( sender, data: динамическиТипизированныйУкль );
			нач
				если ~ sorter.running то  av.RevOrderData  всё
			кон RevOrder;

			проц BadOrder( sender, data: динамическиТипизированныйУкль );
			нач
				если ~ sorter.running то  av.BadOrder  всё
			кон BadOrder;

			проц PrevOrder( sender, data: динамическиТипизированныйУкль );
			нач
				если ~ sorter.running то  av.PrevOrder  всё
			кон PrevOrder;


			проц Rand10( sender, data: динамическиТипизированныйУкль );
			нач
				если ~ sorter.running то  av.Randomize( 10 )  всё
			кон Rand10;

			проц Rand100( sender, data: динамическиТипизированныйУкль );
			нач
				если ~ sorter.running то  av.Randomize( 100 )  всё
			кон Rand100;

			проц Rand200( sender, data: динамическиТипизированныйУкль );
			нач
				если ~ sorter.running то  av.Randomize( 200 )  всё
			кон Rand200;


			проц IncSpeed( sender, data: динамическиТипизированныйУкль );
			нач
				av.IncSpeed
			кон IncSpeed;

			проц DecSpeed( sender, data: динамическиТипизированныйУкль );
			нач
				av.DecSpeed
			кон DecSpeed;

			проц AdjWeightings( sender, data: динамическиТипизированныйУкль );
			нач
				av.AdjWeightings
			кон AdjWeightings;

			проц StartBubbleSort( sender, data: динамическиТипизированныйУкль );
			нач
				sorter.Start( av.DoBubbleSort )
			кон StartBubbleSort;

			проц StartBubbleSortOptim( sender, data: динамическиТипизированныйУкль );
			нач
				sorter.Start( av.DoBubbleSortOptim )
			кон StartBubbleSortOptim;

			проц StartInsertSort( sender, data: динамическиТипизированныйУкль );
			нач
				sorter.Start( av.DoInsertSort )
			кон StartInsertSort;

			проц StartSelectSort( sender, data: динамическиТипизированныйУкль );
			нач
				sorter.Start( av.DoSelectSort )
			кон StartSelectSort;


			проц StartQuickSortOptim( sender, data: динамическиТипизированныйУкль );
			нач
				sorter.Start( av.DoQuickSortOptim )
			кон StartQuickSortOptim;

			проц StartShellSort( sender, data: динамическиТипизированныйУкль );
			нач
				sorter.Start( av.DoShellSort )
			кон StartShellSort;


			проц StartQuickSort( sender, data: динамическиТипизированныйУкль );
			нач
				sorter.Start( av.DoQuickSort )
			кон StartQuickSort;


			проц StartQuickSortConc( sender, data: динамическиТипизированныйУкль );
			нач
				sorter.Start( av.DoQuickSortConc )
			кон StartQuickSortConc;


			проц StartHeapSort( sender, data: динамическиТипизированныйУкль );
			нач
				sorter.Start( av.HeapSort )
			кон StartHeapSort;


			проц StartSmoothSort( sender, data: динамическиТипизированныйУкль );
			нач
				sorter.Start( av.SmoothSort )
			кон StartSmoothSort;

	кон ControlWindow;



перем
	w: ControlWindow;

	проц Open*;
	нач
		если w # НУЛЬ то  w.Close  всё;
		нов( w )
	кон Open;


кон SortDemo.

----------------------------------------------------

		SortDemo.Open

		System.Free SortDemo ~

