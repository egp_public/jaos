модуль Debugging;   (**  AUTHOR "fof"; PURPOSE "Debugging facilities";  **)

использует НИЗКОУР, Потоки, ЛогЯдра, Files, StringPool,  Modules, Objects, Reflection, ЭВМ, Commands, Locks, Dates, Heaps;

перем
	DefaultLog, Log-: Потоки.Писарь;  f: Files.File;  lock: Locks.RWLock;

	проц Memory*( from, tov: адресВПамяти );
	перем i: адресВПамяти; val: размерМЗ;
	нач
		Log.пСтроку8( ">>>>>" );  Log.пВК_ПС;
		если from = 0 то Log.пСтроку8( "NIL Region" );  Log.пВК_ПС;  иначе
			нцДля i := from до tov шаг размер16_от(адресВПамяти) делай
				Log.пАдресВПамяти( i );  Log.пСтроку8( "H (" );  Log.пЦел64( i, 0 );
				Log.пСтроку8( "," );  Log.пЦел64( i - from, 4 );  Log.пСтроку8( ") " );
				НИЗКОУР.прочтиОбъектПоАдресу( i, val );  Log.пАдресВПамяти( val );  Log.пСтроку8( "H = " );
				Log.пЦел64( val, 10 );  Log.пВК_ПС;
			кц;
		всё;
		Log.пСтроку8( "<<<<<" );  Log.пВК_ПС;
		Log.ПротолкниБуферВПоток;
	кон Memory;


	проц CallerBP(bp: адресВПамяти): адресВПамяти;
	перем n: адресВПамяти;
	нач
		если bp # НУЛЬ то
			НИЗКОУР.прочтиОбъектПоАдресу(bp, n);
			если нечётноеЛи¿(n) то увел(bp, размер16_от(адресВПамяти)) всё;
			НИЗКОУР.прочтиОбъектПоАдресу(bp, bp);
		всё;
		возврат bp;
	кон CallerBP;

	проц ViewStack( ebp, esp: адресВПамяти; конст s: массив из симв8);
	перем i: адресВПамяти; val: размерМЗ; prevBP:адресВПамяти;
	конст adrSize= размер16_от(адресВПамяти);
	нач
		Log.пСтроку8( ">>>>> " );  Log.пСтроку8(s); Log.пСтроку8 (" >>>>>> "); Log.пВК_ПС;
		prevBP := CallerBP(ebp);
		если prevBP-ebp > 1024 то prevBP := ebp всё;
		нцДля i := prevBP до esp шаг -adrSize делай
			Log.пАдресВПамяти( i );  Log.пСтроку8( "H (" );  Log.пЦел64( i, 0 );  Log.пСтроку8( "," );
			Log.пЦел64( i - ebp, 4 );  Log.пСтроку8( ") " );  НИЗКОУР.прочтиОбъектПоАдресу( i, val );
			Log.пАдресВПамяти( val );  Log.пСтроку8( "H = " );  Log.пЦел64( val, 10 );
			если i = prevBP то Log.пСтроку8("  <-----  caller EBP"); всё;
			если i = ebp то Log.пСтроку8("  <----- EBP"); всё;
			если i = esp то Log.пСтроку8("  <----- ESP"); всё;
			Log.пВК_ПС;
		кц;
		Log.пСтроку8( "<<<<<" );  Log.пВК_ПС;
		Log.ПротолкниБуферВПоток;

	кон ViewStack;

	проц Stack*(конст s: массив из симв8);
	перем bp,oldbp: адресВПамяти;
	нач
		bp := НИЗКОУР.GetFramePointer();
		oldbp := CallerBP(bp);
		ViewStack(oldbp,bp+4*размер16_от(адресВПамяти),s);
	кон Stack;

	проц TraceBackThis( eip, ebp: адресВПамяти; stacklow, stackhigh:адресВПамяти );   (* do a stack trace back w.r.t. given instruction and frame pointers *)
	нач
		Log.пВК_ПС;  Log.пСтроку8( "#######################" );
		Log.пВК_ПС;  Log.пСтроку8( "# Debugging.TraceBack #" );
		Log.пВК_ПС;  Log.пСтроку8( "#######################" );
		Log.пВК_ПС;  Reflection.StackTraceBack( Log, eip, ebp, stacklow, stackhigh, истина , ложь );
		Log.ПротолкниБуферВПоток;
	кон TraceBackThis;

	проц TraceBack*;   (* do a stack trace back starting at the calling instruction position *)
	нач
		Enter;
		TraceBackThis( ЭВМ.ТекСчётчикКоманд(), НИЗКОУР.GetFramePointer(), НИЗКОУР.GetStackPointer(), Objects.GetStackBottom(Objects.CurrentProcess()) );
		Exit;
	кон TraceBack;

	(* TraceBackAll implemented in System.ShowStacks *)

	проц FileStart*(context: Commands.Context);   (* start writing to a the file Debugging.Text *)
	перем w: Files.Writer;  filename: массив 256 из симв8;
	нач
		если context # НУЛЬ то
			context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename);
		иначе
			filename := ""
		всё;
		если (filename = "")  то filename := "Debugging.Text" всё;
		ЛогЯдра.пСтроку8("filename = "); ЛогЯдра.пСтроку8(filename); ЛогЯдра.пСтроку8("<"); ЛогЯдра.пВК_ПС;
		f := Files.New( filename );
		Files.OpenWriter( w, f, 0 );
		Log := w
	кон FileStart;

	проц FileEnd*;   (* stop writing to Debugging.Text *)
	нач
		Log.ПротолкниБуферВПоток;  Files.Register( f );  f.Update;  f := НУЛЬ;  Log := DefaultLog;
	кон FileEnd;

	(* shortcut for String, usage deprecated *)
	проц Str*( конст name: массив из симв8 );
	нач
		Log.пСтроку8( name );
	кон Str;

	проц String*(конст name: массив из симв8);
	нач
		Log.пСтроку8(name);
	кон String;

	проц Address*(i: адресВПамяти);
	нач
		Log.пАдресВПамяти(i);
	кон Address;

	проц Int*( i: цел64; j: целМЗ );
	нач
		Log.пЦел64( i, j );
	кон Int;

	проц Set*(set: мнвоНаБитахМЗ);
	перем i: целМЗ; first: булево;
	нач
		Log.пСтроку8("{"); first := истина;
		нцДля i := матМинимум(мнвоНаБитахМЗ) до матМаксимум(мнвоНаБитахМЗ) делай
			если i в set то
				если first то first := ложь иначе Log.пСтроку8(",") всё;
				Log.пЦел64(i,1)
			всё;
		кц;
		Log.пСтроку8("}");
	кон Set;


	проц Float*( r: вещ64; len: целМЗ );
	нач
		Log.пВещ64( r, len );
	кон Float;

	проц Hex*( i: цел64; j: целМЗ );
	нач
		Log.п16ричное( i, j );
	кон Hex;

	проц HIntHex*( x: цел64 );
	нач
		Hex( устарПреобразуйКБолееУзкомуЦел( арифмСдвиг( x, -32 ) ),1 );  Hex( устарПреобразуйКБолееУзкомуЦел( x ),1 )
	кон HIntHex;

	проц Char*( c: симв8 );
	нач
		Log.пСимв8( c );
	кон Char;

	проц Update*;
	нач
		Log.ПротолкниБуферВПоток;
	кон Update;

	проц Ln*;
	нач
		Log.пВК_ПС;  Update;
	кон Ln;

	проц Type*( p: динамическиТипизированныйУкль );   (* output the type name of object pointed to by p *)
	перем t: Modules.TypeDesc;
	нач
		если p = НУЛЬ то Str( "NIL (no type)" )
		иначе
			t := Modules.TypeOf( p );
			если t = НУЛЬ то Str( "unknown" ) иначе Str( t.mod.name );  Str( "." );  Str( t.name );  всё;
		всё;
	кон Type;

	проц Str0*( idx: StringPool.Index );   (* output string index as string *)
	перем name: массив 256 из симв8;
	нач
		StringPool.GetString( idx, name );  Log.пСтроку8( name );
	кон Str0;

	проц Enter*;   (* start exclusive writing *)
	перем a: динамическиТипизированныйУкль;  p: Objects.Process;	dt: Dates.DateTime;
	нач
		lock.AcquireWrite;  Ln;
		Str( "{ [P " );  p := Objects.CurrentProcess();  Int( p.id,1 );  Str( " " );  a := Objects.ActiveObject();  Type( a );  Str( "] " );
		dt := Dates.Now(); Int(dt.hour,0); Char(':'); Int(dt.minute,0); Char(':'); Int(dt.second,0);

	кон Enter;

	проц Exit*;   (* end exclusive writing *)
	нач
		Str( "}" );  Log.ПротолкниБуферВПоток;  lock.ReleaseWrite;
	кон Exit;

	проц Nothing;
	нач
	кон Nothing;

	проц Halt*;
	нач СТОП (1234);
	кон Halt;

	проц DisableGC*;
	нач
		Heaps.GC := Nothing;
		трассируй(Heaps.GC, "disabled");
	кон DisableGC;

	проц EnableGC*;
	нач
		Heaps.GC := Heaps.InvokeGC;
		трассируй(Heaps.GC, "enabled");
	кон EnableGC;

	(* useful for debugging the GC / metadata *)
	проц ReportProcedureDescriptors*;
	перем m: Modules.Module; i,j: размерМЗ;
	нач
		m := Modules.root;
		нцПока (m # НУЛЬ) делай
			трассируй(m.name);
			если m.procTable # НУЛЬ то
				нцДля i := 0 до длинаМассива(m.procTable)-1 делай
					(*TRACE(m.procTable[i]);*)
					Log.пАдресВПамяти(m.procTable[i]);
					Log.пСтроку8(":");
					Reflection.WriteProc(Log, m.procTable[i].pcFrom);
					Log.пСтроку8(" ptrs @ ");
					нцДля j := 0 до длинаМассива(m.procTable[i].offsets)-1 делай
						Log.пЦел64(m.procTable[i].offsets[j],1);
						Log.пСтроку8(" ");
					кц;
					Log.пВК_ПС;
				кц;
			всё;
			m := m.next;
		кц;
	кон ReportProcedureDescriptors;

	проц ReportModule* (context: Commands.Context);
	перем m: Modules.Module; name: Modules.Name;
	нач
		если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках (name) то
			context.result := Commands.CommandParseError;
			возврат;
		всё;
		m := Modules.root;
		нцПока (m # НУЛЬ) делай
			если m.name = name то
				Reflection.Report (context.out, m.refs, 0);
				context.result := Commands.Ok;
				возврат;
			всё;
			m := m.next;
		кц;
		context.result := Commands.CommandError;
	кон ReportModule;

	проц Test*;
	нач
		Stack("Stack");
	кон Test;


нач
	Потоки.НастройПисаря( DefaultLog, ЛогЯдра.ЗапишиВПоток );  Log := DefaultLog; нов( lock );
кон Debugging.

System.FreeDownTo Debugging ~
Debugging.ReportProcedureDescriptors
