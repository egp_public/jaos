модуль WMUnicodeMarkerTool;	(** AUTHOR "gubsermi"; PURPOSE "Unicode Marker Tool"; *)


использует
	Strings, WMComponents, WMRestorable, WMCharMap, WMMessages,
	WMStandardComponents, WMTextView, Modules, WM := WMWindowManager,
	XML, UnicodeBidirectionality;

конст
	AlignLeft = 0; AlignCenter = 1; AlignRight = 2; AlignJustified = 3;
	StyleRegular = 0; StyleBold = 1; StyleItalic = 2; StyleBoldItalic = 3;

тип
	KillerMsg = окласс
	кон KillerMsg;

	ContextMenuData = окласс
	перем val: цел32;
		проц &New*(val: цел32);
		нач
			сам.val := val;
		кон New;
	кон ContextMenuData;

	Window* = окласс (WMComponents.FormWindow)
	перем
		leftRightMarkerButton, rightLeftMarkerButton, zeroWidthJoinerButton, zeroWidthNonJoinerButton,
		leftRightEmbeddingButton, rightLeftEmbeddingButton, leftRightOverrideButton, rightLeftOverrideButton,
		popDirectionalFormatButton, otherMarkerButton, displayMarkersButton, focusButton: WMStandardComponents.Button;
		winpanel : WMStandardComponents.Panel;

		проц CreateForm(): WMComponents.VisualComponent;
		перем
			panel : WMStandardComponents.Panel;
			toolbar: WMStandardComponents.Panel;

			проц AB(panel : WMStandardComponents.Panel; btn: WMStandardComponents.Button);
			нач
				btn.alignment.Set(WMComponents.AlignLeft); btn.bounds.SetWidth(60); panel.AddContent(btn)
			кон AB;

		нач
			нов(panel); panel.bounds.SetExtents(120, 140); panel.takesFocus.Set(истина);


			(* styles *)
			(* *)
			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);
			нов(leftRightMarkerButton); leftRightMarkerButton.caption.SetAOC("LRM"); AB(toolbar, leftRightMarkerButton);
			нов(rightLeftMarkerButton); rightLeftMarkerButton.caption.SetAOC("RLM"); AB(toolbar, rightLeftMarkerButton);

			(* *)
			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);
			нов(zeroWidthJoinerButton); zeroWidthJoinerButton.caption.SetAOC("ZWJ"); AB(toolbar, zeroWidthJoinerButton);
			нов(zeroWidthNonJoinerButton); zeroWidthNonJoinerButton.caption.SetAOC("ZWNJ"); AB(toolbar, zeroWidthNonJoinerButton);

			(* *)
			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);
			нов(leftRightEmbeddingButton); leftRightEmbeddingButton.caption.SetAOC("LRE"); AB(toolbar, leftRightEmbeddingButton);
			нов(rightLeftEmbeddingButton); rightLeftEmbeddingButton.caption.SetAOC("RLE"); AB(toolbar, rightLeftEmbeddingButton);

			(* *)
			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);
			нов(leftRightOverrideButton); leftRightOverrideButton.caption.SetAOC("LRO"); AB(toolbar, leftRightOverrideButton);
			нов(rightLeftOverrideButton); rightLeftOverrideButton.caption.SetAOC("RLO"); AB(toolbar, rightLeftOverrideButton);

			(* *)
			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);
			нов(popDirectionalFormatButton); popDirectionalFormatButton.caption.SetAOC("PDF"); AB(toolbar, popDirectionalFormatButton);
			нов(otherMarkerButton); otherMarkerButton.caption.SetAOC("other"); AB(toolbar, otherMarkerButton);

			(* *)
			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);
			нов(displayMarkersButton);
			если UnicodeBidirectionality.showUnicodeControlCharacters то
				displayMarkersButton.caption.SetAOC("hide markers");
			иначе
				displayMarkersButton.caption.SetAOC("show markers");
			всё;
			displayMarkersButton.alignment.Set(WMComponents.AlignLeft);
			displayMarkersButton.bounds.SetWidth(120);
			toolbar.AddContent(displayMarkersButton);

			(* *)
			нов(toolbar); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);
			нов(focusButton); focusButton.caption.SetAOC("focus");
			focusButton.alignment.Set(WMComponents.AlignLeft);
			focusButton.bounds.SetWidth(120);
			toolbar.AddContent(focusButton);

			winpanel := panel;
			возврат panel
		кон CreateForm;

		проц &New*(c : WMRestorable.Context);
		перем
			vc : WMComponents.VisualComponent;
		нач
			IncCount;
			vc := CreateForm();

			leftRightMarkerButton.onClick.Add(InsertMarker);
			rightLeftMarkerButton.onClick.Add(InsertMarker);
			zeroWidthJoinerButton.onClick.Add(InsertMarker);
			zeroWidthNonJoinerButton.onClick.Add(InsertMarker);
			leftRightEmbeddingButton.onClick.Add(InsertMarker);
			rightLeftEmbeddingButton.onClick.Add(InsertMarker);
			leftRightOverrideButton.onClick.Add(InsertMarker);
			rightLeftOverrideButton.onClick.Add(InsertMarker);
			popDirectionalFormatButton.onClick.Add(InsertMarker);
			displayMarkersButton.onClick.Add(ChangeVisibility);
			otherMarkerButton.onClick.Add(OpenUnicodeMap);

			Init(vc.bounds.GetWidth(), vc.bounds.GetHeight(), истина);
			SetContent(vc);

			если c # НУЛЬ то
				WMRestorable.AddByContext(сам, c);
				Resized(GetWidth(), GetHeight());
			иначе
				WM.ExtAddWindow(сам, 50, 50, {WM.FlagStayOnTop, WM.FlagFrame, WM.FlagClose, WM.FlagMinimize});
			всё;
			SetTitle(Strings.NewString("Unicode Markers"));
		кон New;

		(* -- Handlers -- *)

		(* Opens a map with all Unicode characters *)
		проц OpenUnicodeMap(sender, data : динамическиТипизированныйУкль);
		нач
			WMCharMap.Open;
		кон OpenUnicodeMap;

		(* Switches the markers' visibility on and off *)
		проц ChangeVisibility(sender, data : динамическиТипизированныйУкль);
		нач
			если UnicodeBidirectionality.showUnicodeControlCharacters то
				UnicodeBidirectionality.showUnicodeControlCharacters := ложь;
				displayMarkersButton.caption.SetAOC("show markers");
			иначе
				UnicodeBidirectionality.showUnicodeControlCharacters := истина;
				displayMarkersButton.caption.SetAOC("hide markers");
			всё;
			WMTextView.Refresh;
		кон ChangeVisibility;

		(* Insert a certain marker into the currently active text *)
		проц InsertMarker(sender, data : динамическиТипизированныйУкль);
		перем
			res : цел16;
		нач
			если sender = leftRightMarkerButton то
				res := WMTextView.InsertChar(симв32ИзКода(200EH));
			аесли sender = rightLeftMarkerButton то
				res := WMTextView.InsertChar(симв32ИзКода(200FH));
			аесли sender = zeroWidthJoinerButton то
				res := WMTextView.InsertChar(симв32ИзКода(200DH));
			аесли sender = zeroWidthNonJoinerButton то
				res := WMTextView.InsertChar(симв32ИзКода(200CH));
			аесли sender = leftRightEmbeddingButton то
				res := WMTextView.InsertChar(симв32ИзКода(202AH));
			аесли sender = rightLeftEmbeddingButton то
				res := WMTextView.InsertChar(симв32ИзКода(202BH));
			аесли sender = leftRightOverrideButton то
				res := WMTextView.InsertChar(симв32ИзКода(202DH));
			аесли sender = rightLeftOverrideButton то
				res := WMTextView.InsertChar(симв32ИзКода(202EH));
			аесли sender = popDirectionalFormatButton то
				res := WMTextView.InsertChar(симв32ИзКода(202CH));
			всё;

			если res = 0 то
				focusButton.clDefault.Set(0CC0080H);
				focusButton.caption.SetAOC("");
			иначе
				focusButton.clDefault.Set(0CC000080H);
				если res = -1 то
					focusButton.caption.SetAOC("bad format");
				аесли res = -2 то
					focusButton.caption.SetAOC("no text");
				иначе
					focusButton.caption.SetAOC("no textview");
				всё;
			всё;
		кон InsertMarker;

		(* Closes the current instance of the Unicode Marker Tool *)
		проц {перекрыта}Close*;
		нач
			Close^;
			DecCount
		кон Close;

		(* Handles messages/events *)
		проц {перекрыта}Handle*(перем x: WMMessages.Message);
		перем
			data : XML.Element;
			name : массив 32 из симв8;
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) то
				если (x.ext суть KillerMsg) то
					Close
				аесли (x.ext суть WMRestorable.Storage) то
					нов(data);  name := "UnicodeMarkerToolData"; data.SetName(name);
					x.ext(WMRestorable.Storage).Add("WMUnicodeMarkerTool", "WMUnicodeMarkerTool.Restore", сам, data);
				иначе
					Handle^(x);
				всё
			иначе
				Handle^(x)
			всё
		кон Handle;

	кон Window;


перем
	nofWindows : цел32;
	ctxAlignLeft, ctxAlignCenter, ctxAlignRight, ctxAlignJustified : ContextMenuData;
	ctxRegular, ctxBold, ctxItalic, ctxBoldItalic : ContextMenuData;


(* Opens a new instance of the Unicode Marker Tool *)
проц Open*;
перем winstance : Window;
нач
	нов(winstance, НУЛЬ);
кон Open;

(* Restores an instance of the Unicode Marker Tool *)
проц Restore*(context : WMRestorable.Context);
перем w : Window;
нач
	нов(w, context);
кон Restore;

проц IncCount;
нач {единолично}
	увел(nofWindows)
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows)
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WM.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WM.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0)
кон Cleanup;

нач
	нов(ctxAlignLeft, AlignLeft);
	нов(ctxAlignCenter, AlignCenter);
	нов(ctxAlignRight, AlignRight);
	нов(ctxAlignJustified, AlignJustified);
	нов(ctxRegular, StyleRegular);
	нов(ctxBold, StyleBold);
	нов(ctxItalic, StyleItalic);
	нов(ctxBoldItalic, StyleBoldItalic);

	Modules.InstallTermHandler(Cleanup)
кон WMUnicodeMarkerTool.

System.Free WMUnicodeMarkerTool~
WMUnicodeMarkerTool.Open  ~




