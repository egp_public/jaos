модуль srRotaVox;
использует srBase, srMath, Math;

тип Voxel=srBase.Voxel;
тип PT=srBase.PT;
тип Ray=srBase.Ray;
тип SREAL=srBase.SREAL;

тип RVox*=окласс(Voxel);
перем
	child: Voxel;
	phi: вещ32; (* rotation angle about Z axis in radians *)
	dtick: вещ32;
	center:PT;

проц&init*(c:Voxel; dt: вещ32);
нач
	child:=c;
	dtick:=dt;
	register;
	center.x:=1/2; center.y:=1/2; center.z:=1/2;
кон init;

проц {перекрыта}tick*;
нач
	phi:=phi+dtick;
	если phi > 6.2832 то phi:=0 всё
кон tick;

проц d2(a,b:PT):SREAL;
нач
	 возврат((a.x-b.x)*(a.x-b.x)+ (a.y-b.y)*(a.y-b.y) + (a.z-b.z)*(a.z-b.z));
кон d2;

проц dia(a,b:PT):SREAL;
нач
	 возврат Math.sqrt((a.x-b.x)*(a.x-b.x)+ (a.y-b.y)*(a.y-b.y) + (a.z-b.z)*(a.z-b.z));
кон dia;

проц {перекрыта}Shade*(перем ray: Ray);
перем
	a,b,c,dxyz:PT;
	dc,x,y,z: вещ32;
	i: цел16;
нач
	(* advance ray to its intersection with the sphere of radius 1/2 centered in the voxel*)
	a:=ray.lxyz;
	b:= srBase.Exit(ray);
	c.x := (a.x+b.x)/2; c.y := (a.y+b.y)/2; c.z := (a.z + b.z)/2;
	нцДля i := 0 до 12 делай
		dc := d2(center,c);
		если dc < 1/4 то
			b:=c;
		иначе
			a:=c;
		всё;
		c.x := (a.x+b.x)/2; c.y := (a.y+b.y)/2; c.z := (a.z + b.z)/2;
	кц;
	dc := d2(center,c);
	если матМодуль(dc-1/4)<0.001 то
		a.x:=(x-1/2)*2; a.y:=(y-1/2)*2; a.z:=(z-1/2)*2;      (* we know that the vector from (1/2,1/2,1/2) to (x,y,z) has length 1/2 so this translates and normalizeds it *)
		srMath.orrot(a,Zaxis,phi);
		a.x:=a.x/2+1/2; a.y:=a.y/2+1/2; a.z:=a.z/2+1/2;	(* denormalize and translate bacck*)
		ray.lxyz:=a;
		dxyz:=ray.dxyz;
		srMath.orrot(ray.dxyz,Zaxis,-phi);
		ray.normal:=ray.dxyz;
		child.Shade(ray);
		если ~ray.changed то
			ray.dxyz:=dxyz

		иначе		(* reflection or refraction. Must rotate back to enclosing coordinate system *)
					(* 2 B implemented *)
			ray.changed := ложь;
			ray.dxyz:=dxyz
		всё
	всё;
кон Shade;

кон RVox;

перем
	Zaxis: PT;
нач
	Zaxis.z:=1;

кон srRotaVox.
