модуль ЭВМ;
(** AUTHOR "pjm,fof"; PURPOSE "Bootstrapping, configuration and machine interface, adaption to windows fof"; *)
(* red marked parts are WinAos specific *)

использует НИЗКОУР, Трассировка, Kernel32;

конст
#если AMD64 то
	Version = "ЯОС под Win64 ";
#аесли I386 то
	Version = "ЯОС под Win32 ";
#иначе
	-- UNDEFINED
#кон

	DefaultConfigFile = "aos.ini";
	DefaultGenericConfigFile = "aosg.ini";
	UserConfigFile = "myaos.ini";

	МаксКвоПроцессоров* = 8;	(* dummy definition to make GC for both Win32 and I386 work *)

#если AMD64 то
	РасширениеОбъектногоФайлаПоУмолч* = ".GofWw";
#аесли I386 то
	РасширениеОбъектногоФайлаПоУмолч* = ".Obw";
#иначе
	-- UNDEFINED
#кон

	(** bits in features variable *)
	MTTR* = 12;  MMX* = 23;

	ВклПечатьПриЗагрузкеЛи¿* = ложь;   (** display more debug output during booting *)

конст
	StaticBlockSize = 8*размер16_от(адресВПамяти);		(* static heap block size *)

(** standard lock levels (in order) *)  (* also refer to Traps.Show *)
	ЯдернаяБлокировкаДляВыводаТрассировки* = 0;   (* Trace output *)
	ЯдернаяБлокировкаДляРаботыСПамятью* = 1;   (* Virtual memory management, stack and page allocation *)
	ЯдернаяБлокировкаДляРаботыСКучей* = 2;   (* Storage allocation and Garbage collection *)
	ЯдернаяБлокировкаДляОбработкиПрерываний* = 3;   (* Interrupt handling. *)
	ЯдернаяБлокировкаСпискаМодулей* = 4;   (* Module list *)
	ЯдернаяБлокировкаОчередиГотовности* = 5;   (* Ready queue *)
	ЯдернаяБлокировкаДляМежпроцессорногоПрерывания* = 6;   (* Interprocessor interrupts *)
	ЯдернаяБлокировкаДляЛогаЯдра* = 7;   (* Atomic output *)
	ЯдернаяБлокировкаДляСборщикаМусора* = 8;
	квоЯдерныхБлокировок = 9;   (* { <= 32 } *)

	StrongChecks = истина;


	MemBlockSize = 32*1024*1024; (* must be multiple of StaticBlockSize *)

	NilVal = 0;

	ЧастотаКвантовВремени˛Гц* = 1000; (* frequency of ticks increments in Hz *)


конст
		(* error codes *)
		Успех* = 0;
		НулевойАдрес* = -1;	(* nil value for addresses (not same as pointer NIL value) *)

тип
	Поставщик* = массив 13 из симв8;
	IDMap* = массив 16 из цел8;

	ДиапазонПамяти* = запись
		адр*, разм*: цел32
	кон;

	УкльНаЗаголовокБлокаПамяти* = укль {опасныйДоступКПамяти, неОтслСборщиком} на ЗаголовокБлокаПамяти;
	ЗаголовокБлокаПамяти* = запись
		следщ- : УкльНаЗаголовокБлокаПамяти;
		адресЭтогоБлока-: адресВПамяти; 		(* sort key in linked list of memory blocks *)
		разм-: размерМЗ;
		адресНачалаДанных-, адресЗаКонцомДанных-: адресВПамяти
	кон;

	(* dummy definition to make GC work for both I386 and Win32 - copied from BIOS.I386.Machine.Mod, but not really used *)
	Стек* = запись	(** values are read-only *)
		нижнийПределВключительно-: адресВПамяти;		(* lowest virtual address that may be allocated for stack *)
		тек*: адресВПамяти;		(* lowest address on allocated stack *)	(* exported for Objects only *)
		верхнийПределНевключительно*: адресВПамяти;	(* next virtual address after stack *)	(* exported for Objects only *)
	кон;

перем

	LastAddress: запись кон;
	дноСтекаПервогоПроцесса-: адресВПамяти;

	MMX_поддерживаетсяЛи¿*: булево;
	SSE_поддерживаетсяЛи¿*: булево;
	SSE2_поддерживаетсяЛи¿*: булево;
	SSE3_поддерживаетсяЛи¿-: булево; (* PH 04/11*)
	SSSE3_поддерживаетсяЛи¿-: булево;
	SSE41_поддерживаетсяЛи¿-: булево;
	SSE42_поддерживаетсяЛи¿-: булево;
	SSE5_поддерживаетсяЛи¿-: булево;
	AVX_поддерживаетсяЛи¿-: булево;

	версияЯОС*: массив 64 из симв8;   (** Aos version *)
	свойстваПроцессора_1*,свойстваПроцессора_2*: мнвоНаБитахМЗ;   (** processor features *)
	значПоУмолчРегистраУправленияОперациямиСПлавающейТочкой*: мнвоНаБитахМЗ;   (** default floating-point control register value (default rounding mode is towards -infinity, for ENTIER) *)
	частотаПоСчётчикуМеткиВремени˛МГц*: цел64;   (** clock rate of GetTimer() in MHz, or 0 if not known *)
	времяНачалаЗагрузки-: цел64; (** in timer units *)

	команднаяСтрокаЗапускаЯОС_изХозяйскойОС-: массив 256 из бцел16;
	hin, hout: Kernel32.HANDLE;

перем
	ядерныеБлокировки-: массив квоЯдерныхБлокировок из симв8;  (* not implemented as SET because of shared access *)
	cs: массив квоЯдерныхБлокировок из Kernel32.CriticalSection;
	taken: массив квоЯдерныхБлокировок из цел32;
	defaultConfigFile, userConfigFile, traceName: массив Kernel32.MaxPath из симв8;

	gcThreshold: размерМЗ; (* не используется *)
	первыйБлокПамятиДляКучи-{неОтслСборщиком}, последнийБлокПамятиДляКучи-{неОтслСборщиком}: УкльНаЗаголовокБлокаПамяти; (* head and tail of sorted list of memory blocks *)


	(** Convert a string to an integer.  Parameter i specifies where in the string scanning should begin (usually 0 in the first call).  Scanning stops at the first non-valid character, and i returns the updated position.  Parameter s is the string to be scanned.  The value is returned as result, or 0 if not valid.  Syntax: number = ["-"] digit {digit} ["H" | "h"] .  digit = "0" | ... "9" | "A" .. "F" | "a" .. "f" .  If the number contains any hexdecimal letter, or if it ends in "H" or "h", it is interpreted as hexadecimal. *)

	проц СтрВЦел32*( перем НачинаяСПозиции: размерМЗ;  конст Строка: массив из симв8 ): цел32;
	перем vd, vh, sgn, d: цел32;  hex: булево;
	нач
		vd := 0;  vh := 0;  hex := ложь;
		если Строка[НачинаяСПозиции] = "-" то sgn := -1;  увел( НачинаяСПозиции ) иначе sgn := 1 всё;
		нц
			если (Строка[НачинаяСПозиции] >= "0") и (Строка[НачинаяСПозиции] <= "9") то d := кодСимв8( Строка[НачинаяСПозиции] ) - кодСимв8( "0" )
			аесли (ASCII_вЗаглавную( Строка[НачинаяСПозиции] ) >= "A") и (ASCII_вЗаглавную( Строка[НачинаяСПозиции] ) <= "F") то d := кодСимв8( ASCII_вЗаглавную( Строка[НачинаяСПозиции] ) ) - кодСимв8( "A" ) + 10;  hex := истина
			иначе прервиЦикл
			всё;
			vd := 10 * vd + d;  vh := 16 * vh + d;  увел( НачинаяСПозиции )
		кц;
		если ASCII_вЗаглавную( Строка[НачинаяСПозиции] ) = "H" то hex := истина;  увел( НачинаяСПозиции ) всё;   (* optional H *)
		если hex то vd := vh всё;
		возврат sgn * vd
	кон СтрВЦел32;

	(** -- Atomic operations -- *)

	(** Atomic INC(x). *)
	проц -атомарноУвел*(перем x: цел32);
	машКод
#если AMD64 то
		POP RAX
		LOCK
		INC DWORD [RAX]
#аесли I386 то
		POP	EAX
		LOCK
		INC	DWORD[EAX]
#иначе
	UNIMPLEMENTED
#кон
	кон атомарноУвел;

	(** Atomic DEC(x). *)
	проц -атомарноУмень*(перем x: цел32);
	машКод
#если AMD64 то
		POP RAX
		LOCK
		DEC DWORD [RAX]
#аесли I386 то
		POP	EAX
		LOCK
		DEC	DWORD[EAX]
#иначе
	UNIMPLEMENTED
#кон
	кон атомарноУмень;

	(** Atomic EXCL. *)
	проц атомарноИсключиИзМнваНаБитах* (перем s: мнвоНаБитахМЗ; bit: цел32);
	машКод
#если AMD64 то
		MOV EAX, [RBP + bit]
		MOV RBX, [RBP + s]
		LOCK
		BTR [RBX], EAX
#аесли I386 то
		MOV EAX, [EBP + bit]
		MOV EBX, [EBP + s]
		LOCK
		BTR [EBX], EAX
#иначе
	UNIMPLEMENTED
#кон
	кон атомарноИсключиИзМнваНаБитах;

	(** Atomic INC(x, y). *)
	проц -атомарноУвелНа*(перем x: цел32; y: цел32);
	машКод
#если AMD64 то
		POP RBX
		POP RAX
		LOCK
		ADD DWORD [RAX], EBX
#аесли I386 то
		POP	EBX
		POP	EAX
		LOCK
		ADD	DWORD[EAX], EBX
#иначе
	UNIMPLEMENTED
#кон
	кон атомарноУвелНа;

	(** Atomic test-and-set. Set x = TRUE and return old value of x. *)
	проц -атомарноПрочитайИЗапиши*(перем x: булево): булево;
	машКод
#если AMD64 то
		POP RBX
		MOV AL, 1
		XCHG [RBX], AL
#аесли I386 то
		POP	EBX
		MOV	AL, 1
		XCHG	[EBX], AL
#иначе
	UNIMPLEMENTED
#кон
	кон атомарноПрочитайИЗапиши;

	(* Atomic compare-and-swap. Set x = new if x = old and return old value of x *)
	проц -атомарноСравниИЗамени* (перем x: цел32; old, new: цел32): цел32;
	машКод
#если AMD64 то
		POP RBX		; new
		POP RAX		; old
		POP RCX		; address of x
		LOCK CMPXCHG [RCX], EBX	; atomicly compare x with old and set it to new if equal
#аесли I386 то
		POP EBX		; new
		POP EAX		; old
		POP ECX		; address of x
		DB 0F0X, 00FX, 0B1X, 019X	; LOCK CMPXCHG [ECX], EBX; atomicly compare x with old and set it to new if equal

#иначе
	UNIMPLEMENTED
#кон
	кон атомарноСравниИЗамени;


	(** This procedure should be called in all spin loops as a hint to the processor (e.g. Pentium 4). *)
	проц -ПодсказкаОСпинКонструкциях*;
	машКод
		PAUSE
	кон ПодсказкаОСпинКонструкциях;

	(** -- Miscellaneous -- *)

	(* Return current instruction pointer *)
	проц ТекСчётчикКоманд* (): адресВПамяти;
	машКод
#если AMD64 то
		MOV RAX, [RBP + 8]
#аесли I386 то
		MOV EAX, [EBP+4]
#иначе
	UNIMPLEMENTED
#кон
	кон ТекСчётчикКоманд;

	(** Fill "size" bytes at "destAdr" with "filler". "size" must be multiple of 4. *)
	проц ЗаполниДиапазонБайтовПредставлениемЦел32* (destAdr: адресВПамяти; size: размерМЗ; filler: цел32);
	машКод
#если AMD64 то
		MOV RDI, [RBP + destAdr]
		MOV RCX, [RBP + size]
		MOV EAX, [RBP + filler]
		TEST RCX, 3
		JZ ok
		PUSH 8	; ASSERT failure
		INT 3
	ok:
		SHR RCX, 2
		CLD
		REP STOSD
#аесли I386 то
		MOV	EDI, [EBP+destAdr]
		MOV	ECX, [EBP+size]
		MOV	EAX, [EBP+filler]
		TEST	ECX, 3
		JZ	ok
		PUSH	8	;  ASSERT failure
		INT	3
	ok:
		SHR	ECX, 2
		CLD
		REP	STOSD
#иначе
	UNIMPLEMENTED
#кон
	кон ЗаполниДиапазонБайтовПредставлениемЦел32;
	(** -- Processor initialization -- *)

	проц -SetFCR( s: мнвоНаБитахМЗ );
	машКод
#если AMD64 то
		FLDCW	[RSP]	;  parameter s
		POP	RAX
#аесли I386 то
		FLDCW	[ESP]	;  parameter s
		POP	EAX
#иначе
	UNIMPLEMENTED
#кон
	кон SetFCR;

	проц -FCR( ): мнвоНаБитахМЗ;
	машКод
#если AMD64 то
		PUSH	0
		FNSTCW	[RSP]
		FWAIT
		POP	RAX
#аесли I386 то
		PUSH	0
		FNSTCW	[ESP]
		FWAIT
		POP	EAX
#иначе
	UNIMPLEMENTED
#кон
	кон FCR;

	проц -InitFPU;
	машКод
		FNINIT
	кон InitFPU;

(** Setup FPU control word of current processor. *)

	проц УстРегистрУправленияСопроцессором*;
	нач
		InitFPU;  SetFCR( значПоУмолчРегистраУправленияОперациямиСПлавающейТочкой )
	кон УстРегистрУправленияСопроцессором;

(** CPU identification. *)

	проц ДайИдентификаторПроцессора*( перем vendor: Поставщик;  перем version: цел32;  перем features1,features2: мнвоНаБитахМЗ );
	машКод
#если AMD64 то
		MOV	EAX, 0
		CPUID
		CMP	EAX, 0
		JNE	ok
		MOV	RSI, [RBP+vendor]
		MOV	[RSI], AL	;  AL = 0
		MOV	RSI, [RBP+version]
		MOV	[RSI], EAX	;  EAX = 0
		MOV	RSI, [RBP+features1]
		MOV	[RSI], EAX
		MOV	RSI, [RBP+features2]
		MOV	[RSI], EAX
		JMP	end
		ok:
		MOV	RSI, [RBP+vendor]
		MOV	[RSI], EBX
		MOV	[RSI+4], EDX
		MOV	[RSI+8], ECX
		MOV	BYTE [RSI+12], 0
		MOV	EAX, 1
		CPUID
		MOV	RSI, [RBP+version]
		MOV	[RSI], EAX
		MOV	RSI, [RBP+features1]
		MOV	[RSI], EDX
		MOV	RSI, [RBP+features2]
		MOV	[RSI], ECX
		end:
#аесли I386 то
		MOV	EAX, 0
		CPUID
		CMP	EAX, 0
		JNE	ok
		MOV	ESI, [EBP+vendor]
		MOV	[ESI], AL	;  AL = 0
		MOV	ESI, [EBP+version]
		MOV	[ESI], EAX	;  EAX = 0
		MOV	ESI, [EBP+features1]
		MOV	[ESI], EAX
		MOV	ESI, [EBP+features2]
		MOV	[ESI], EAX
		JMP	end
		ok:
		MOV	ESI, [EBP+vendor]
		MOV	[ESI], EBX
		MOV	[ESI+4], EDX
		MOV	[ESI+8], ECX
		MOV	BYTE [ESI+12], 0
		MOV	EAX, 1
		CPUID
		MOV	ESI, [EBP+version]
		MOV	[ESI], EAX
		MOV	ESI, [EBP+features1]
		MOV	[ESI], EDX
		MOV	ESI, [EBP+features2]
		MOV	[ESI], ECX
		end:
#иначе
	UNIMPLEMENTED
#кон
	кон ДайИдентификаторПроцессора;

	проц ДайЗначениеКлючаКонфигурацииЯОС*( конст Ключ: массив из симв8;  перем Знач: массив из симв8 );
	конст ConfigKey = "Configuration";
	нач
		копируйСтрокуДо0 ("", Знач);

		если Kernel32.GetPrivateProfileString (ConfigKey, Ключ, "", Знач, длинаМассива (Знач)(Kernel32.DWORD), userConfigFile) # 0 то
		аесли Kernel32.GetPrivateProfileString (ConfigKey, Ключ, "", Знач, длинаМассива (Знач)(Kernel32.DWORD), defaultConfigFile) # 0 то
		всё;

		если (Ключ = "ObjectFileExtension") и (Знач = "") то
#если AMD64 то
			если Kernel32.Generic то
				Знач := ".GofWw";
			иначе
				Знач := ".Obww"
			всё;
#аесли I386 то
			если Kernel32.Generic то
				Знач := ".GofW";
			иначе
				Знач := ".Obw"
			всё;
#иначе
	UNIMPLEMENTED
#кон
		всё;
	кон ДайЗначениеКлючаКонфигурацииЯОС;

	проц ОстановиЯОС*( сПерезапускомЛи¿: булево );
	нач
		RemoveTraceFile;
		Kernel32.Shutdown( 0 );   (* calls the finalizer of Heaps *)
	кон ОстановиЯОС;

(* Dan: from new Machine *)
проц -ДайКвоТактовПроцессораСМоментаПерезапуска*(): цел64;
машКод {SYSTEM.Pentium}
	RDTSC	; set EDX:EAX
кон ДайКвоТактовПроцессораСМоментаПерезапуска;

(* Dan:  mono CPU PCs *)
проц НомерТекущегоПроцессора*(): цел32;
нач
	возврат 0
кон НомерТекущегоПроцессора;


(**
 * Flush Data Cache for the specified virtual address range. If len is negative, flushes the whole cache.
 * This is used on some architecture to interact with DMA hardware (e.g. Ethernet and USB. It can be
 * left empty on Intel architecture.
 *)
проц СбросьКешДанныхДляДиапазона * (adr: адресВПамяти; len: размерМЗ);
кон СбросьКешДанныхДляДиапазона;

(**
 * Invalidate Data Cache for the specified virtual address range. If len is negative, flushes the whole cache.
 * This is used on some architecture to interact with DMA hardware (e.g. Ethernet and USB. It can be
 * left empty on Intel architecture.
 *)
проц ИнвалидируйКешДанныхДляДиапазона * (adr: адресВПамяти; len: размерМЗ);
кон ИнвалидируйКешДанныхДляДиапазона;

(**
 * Invalidate Instruction Cache for the specified virtual address range. If len is negative, flushes the whole cache.
 * This is used on some architecture to interact with DMA hardware (e.g. Ethernet and USB. It can be
 * left empty on Intel architecture.
 *)
проц ИнвалидируйКешИнструкцийДляДиапазона * (adr: адресВПамяти; len: размерМЗ);
кон ИнвалидируйКешИнструкцийДляДиапазона;


(* setup MMX, SSE and SSE2..SSE5 and AVX extension *)

проц SetupSSE2Ext;
конст
	MMXFlag=23;(*IN features from EBX*)
	FXSRFlag = 24;
	SSEFlag = 25;
	SSE2Flag = 26;
	SSE3Flag = 0; (*IN features2 from ECX*) (*PH 04/11*)
	SSSE3Flag =9;
	SSE41Flag =19;
	SSE42Flag =20;
	SSE5Flag = 11;
	AVXFlag = 28;
нач
	MMX_поддерживаетсяЛи¿ := MMXFlag в свойстваПроцессора_1;
	SSE_поддерживаетсяЛи¿ := SSEFlag в свойстваПроцессора_1;
	SSE2_поддерживаетсяЛи¿ := SSE_поддерживаетсяЛи¿ и (SSE2Flag в свойстваПроцессора_1);
	SSE3_поддерживаетсяЛи¿ := SSE2_поддерживаетсяЛи¿ и (SSE3Flag в свойстваПроцессора_2);
	SSSE3_поддерживаетсяЛи¿ := SSE3_поддерживаетсяЛи¿ и (SSSE3Flag в свойстваПроцессора_2); (* PH 04/11*)
	SSE41_поддерживаетсяЛи¿ := SSE3_поддерживаетсяЛи¿ и (SSE41Flag в свойстваПроцессора_2);
	SSE42_поддерживаетсяЛи¿ := SSE3_поддерживаетсяЛи¿ и (SSE42Flag в свойстваПроцессора_2);
	SSE5_поддерживаетсяЛи¿ := SSE3_поддерживаетсяЛи¿ и (SSE5Flag в свойстваПроцессора_2);
	AVX_поддерживаетсяЛи¿ := SSE3_поддерживаетсяЛи¿ и (AVXFlag в свойстваПроцессора_2);

	если SSE_поддерживаетсяЛи¿ и (FXSRFlag в свойстваПроцессора_1) то
		(* InitSSE(); *) (*! not privileged mode in Windows not allowed *)
	всё;
кон SetupSSE2Ext;

(* см. также StdIO.ПолучиКоманднуюСтрокуИзОС *)
проц ПолучиКоманднуюСтрокуИзОС*(перем рез: массив из бцел16);	
перем adr: адресВПамяти; i: цел32; лUCS2: бцел16;
нач
	adr := Kernel32.GetCommandLineW();
	НИЗКОУР.прочтиОбъектПоАдресу(adr,лUCS2);
	i := 0;
	нцПока (i<длинаМассива(рез)-1) и (лUCS2 # 0) делай
		команднаяСтрокаЗапускаЯОС_изХозяйскойОС[i] := лUCS2;
		увел(adr,размер16_от(бцел16)); увел(i);
		НИЗКОУР.прочтиОбъектПоАдресу(adr,лUCS2);
	кц;кон ПолучиКоманднуюСтрокуИзОС;


проц TraceChar(c: симв8);
перем len: цел32; b: Kernel32.BOOL;
нач
	len := 1;
	b := Kernel32.WriteFile(hout,c,len,len,НУЛЬ);
кон TraceChar;

проц ВпредьВыводиТрассировкуВФайл*;
нач
	traceName := "SystemTrace.txt";
	SetupTraceName(traceName);
	Трассировка.пСтроку8("trace -> file "); Трассировка.пСтроку8(traceName); Трассировка.пВК_ПС;
	hout := Kernel32.CreateFile(traceName, {Kernel32.GenericWrite}, {Kernel32.FileShareRead}, НУЛЬ, Kernel32.CreateAlways, {Kernel32.FileAttributeNormal}, Kernel32.NULL);
	неважно Kernel32.GetFullPathName(traceName, длинаМассива(traceName),traceName, НУЛЬ);
	Трассировка.пСимв8 := TraceChar;
	Трассировка.пСтроку8(версияЯОС); Трассировка.пВК_ПС;
кон ВпредьВыводиТрассировкуВФайл;

проц ВпредьВыводиТрассировкуНаКонсоль*;
перем res: целМЗ;
нач
	Трассировка.пСтроку8("trace -> console"); Трассировка.пВК_ПС;
	res := Kernel32.AllocConsole ();
	hin := Kernel32.GetStdHandle (Kernel32.STDInput);
	hout := Kernel32.GetStdHandle (Kernel32.STDOutput);
	Трассировка.пСимв8 := TraceChar;
	Трассировка.пСтроку8(версияЯОС); Трассировка.пВК_ПС;
кон ВпредьВыводиТрассировкуНаКонсоль;

проц SetupTraceName(перем имяТрассы: массив из симв8);
перем
	ext: массив 256 из симв8;
	extPos,i,j: цел32;
	systemTime: Kernel32.SystemTime;
	ch: симв8;

	проц AppendDecimals(int: цел32; from, to: цел32);
	нач
		нцПока to >= from делай
			имяТрассы[i] := симв8ИзКода(кодСимв8("0")+ int DIV to остОтДеленияНа 10); увел(i);
			to := to DIV 10;
		кц;
	кон AppendDecimals;

нач
	Kernel32.GetLocalTime(systemTime);

	extPos := 0;
	нцДо
		ch := traceName[i];

		если ch = "." то j := 0; extPos := i всё;

		ext[j] := ch;
		увел(j); увел(i);
	кцПри ch = 0X;

	если extPos > 0 то i := extPos всё;
	ext[j] := 0X;

	AppendDecimals(systemTime.wYear,1,1000);
	AppendDecimals(systemTime.wMonth,1,10);
	AppendDecimals(systemTime.wDay,1,10);
	traceName[i] := "_"; увел(i);
	AppendDecimals(systemTime.wHour,1,10);
	AppendDecimals(systemTime.wMinute,1,10);
	AppendDecimals(systemTime.wSecond,1,10);
	traceName[i] := "_"; увел(i);
	AppendDecimals(systemTime.wMilliseconds,10,100);
	j := 0;
	нцДо
		ch := ext[j];
		traceName[i] := ch;
		увел(i); увел(j);
	кцПри ch = 0X;

кон SetupTraceName;

проц RemoveTraceFile;
перем res: целМЗ;
нач
	если (traceName # "") и (traceName # "Console") то
		Трассировка.пСтроку8("removing "); Трассировка.пСтроку8(traceName); Трассировка.пВК_ПС;
		(*Trace.Char := LogChar;*)
		res := Kernel32.CloseHandle(hout);
		если res = 0 то
			res := Kernel32.GetLastError();
			Трассировка.пСтроку8("could not close "); Трассировка.пСтроку8("; res = "); Трассировка.пЦел64(res,1); Трассировка.пВК_ПС;
		всё;
		res := Kernel32.DeleteFile(traceName);
		если res = 0 то
			res := Kernel32.GetLastError();
			Трассировка.пСтроку8("could not delete "); Трассировка.пСтроку8(traceName); Трассировка.пСтроку8("; res = "); Трассировка.пЦел64(res,1); Трассировка.пВК_ПС;
		всё;
	всё;
кон RemoveTraceFile;

проц ToExecutablePath(конст name: массив из симв8; перем fullName: массив из симв8);
перем i,j: цел32;
нач
	неважно Kernel32.GetModuleFileName(Kernel32.hInstance, fullName, длинаМассива( fullName )(Kernel32.DWORD) );
	j := -1; i := 0;
	нцПока fullName[i] # 0X делай
		если fullName[i] = '\' то j := i всё;
		увел( i )
	кц;
	i := 0; увел(j);
	нцПока name[i] # 0X делай
		fullName[j] := name[i]; увел(i); увел(j);
	кц;
	fullName[j] := 0X;
кон ToExecutablePath;

проц Append(перем s: массив из симв8; конст t: массив из симв8);
перем i,j: цел32;
нач
	i := 0;
	нцПока(s[i] # 0X) делай увел(i) кц;
	j := 0;
	нцПока (t[j] # 0X) делай
		s[i] := t[j];
		увел(i); увел(j);
	кц;
	s[i] := 0X;
кон Append;

проц ИнициализируйЯОС*;
перем vendor: Поставщик; ver: цел32; hfile: Kernel32.HANDLE;
нач
	Kernel32.Init;
	(*	trace[1] := 0X; Trace.Char := LogChar; Trace.Color := TraceColor; *)
	InitLocks();
	времяНачалаЗагрузки:=ДайКвоТактовПроцессораСМоментаПерезапуска();

	копируйСтрокуДо0( Version, версияЯОС );
	Append(версияЯОС, НИЗКОУР.Date);
	ДайИдентификаторПроцессора(vendor, ver, свойстваПроцессора_1,свойстваПроцессора_2);	 SetupSSE2Ext;
	значПоУмолчРегистраУправленияОперациямиСПлавающейТочкой := (FCR() - {0,2,3,10,11}) + {0..5,8,9};	(* default FCR RC=00B *)

	ПолучиКоманднуюСтрокуИзОС(команднаяСтрокаЗапускаЯОС_изХозяйскойОС);

	если Kernel32.Generic то
		ToExecutablePath(DefaultGenericConfigFile, defaultConfigFile);
	иначе
		ToExecutablePath(DefaultConfigFile, defaultConfigFile);
	всё;
	копируйСтрокуДо0(UserConfigFile, userConfigFile);
	hfile := Kernel32.CreateFile( userConfigFile, {Kernel32.GenericRead}, {Kernel32.FileShareRead}, НУЛЬ , Kernel32.OpenExisting, {Kernel32.FileAttributeNormal}, 0 );
	если hfile = Kernel32.InvalidHandleValue то
		ToExecutablePath(UserConfigFile, userConfigFile);
	иначе
		неважно Kernel32.CloseHandle(hfile)
	всё;

	(*
	(* ever used ? *)
	ParseLine(commandLine, userConfigFile);
	userConfigFile[Kernel32.GetFullPathName (userConfigFile, Kernel32.MaxPath, userConfigFile, 0)] := 0X;

	traceName[0] := 0X;
	GetConfig("Trace",traceName);
	Trace.String("traceName "); Trace.String(traceName); Trace.Ln;
	IF traceName = "File" THEN SetTraceFile;
	ELSIF traceName = "Console" THEN SetTraceConsole
	(* else trace is on kernel log *)
	END;
	Trace.String("Machine init done"); Trace.Ln;
	*)
кон ИнициализируйЯОС;

	проц {INITIAL, NOPAF} ЗапустиЯОС*;
	нач
		дноСтекаПервогоПроцесса := НИЗКОУР.GetStackPointer();
		ИнициализируйЯОС; (* cannot allocate variables in here *)
	кон ЗапустиЯОС;

	(* Initialize locks. *)
	проц InitLocks;
	перем i: цел32;
	нач
		i := 0;
		нцПока i < квоЯдерныхБлокировок делай Kernel32.InitializeCriticalSection( cs[i] ); ядерныеБлокировки[i] := "N"; taken[i] := 0; увел( i ) кц;
	кон InitLocks;

	проц СбросьВсеЯдерныеБлокировки*;
	перем i: цел32;
	нач
		i := 0;
		нцПока i < квоЯдерныхБлокировок делай Kernel32.DeleteCriticalSection( cs[i] ); taken[i] := 0; ядерныеБлокировки[i] := "N"; увел( i ) кц;
	кон СбросьВсеЯдерныеБлокировки;

(** Acquire a spin-lock. *)
	проц ЗапросиБлокировку*( Уровень: цел32 );   (* non reentrant lock  (non reentrance "ensured" by ASSERT statement ), CriticalSections are reentrant *)
	перем i: целМЗ;
	нач
		Kernel32.EnterCriticalSection( cs[Уровень] );
		taken[Уровень] := Kernel32.GetCurrentThreadId();
		если StrongChecks то
			утв ( ядерныеБлокировки[Уровень] = "N", 1001 );
			нцДля i := 0 до Уровень-1 делай
				утв(taken[i] # taken[Уровень]);
			кц
		аесли ядерныеБлокировки[Уровень] # "N" то
			Трассировка.пСтроку8("warning: reentered non-reentrant lock"); Трассировка.пВК_ПС;
		всё;
		ядерныеБлокировки[Уровень] := "Y";
	кон ЗапросиБлокировку;

(** Release a spin-lock. *)
	проц ОтпустиБлокировку*( Уровень: цел32 );   (* release lock *)
	нач
		если StrongChecks то
			утв ( ядерныеБлокировки[Уровень] ="Y", 1002 );
			утв( taken[Уровень] = Kernel32.GetCurrentThreadId());
		аесли ядерныеБлокировки[Уровень] # "Y" то
			Трассировка.пСтроку8("warning: reentered non-reentrant lock"); Трассировка.пВК_ПС;
		всё;
		ядерныеБлокировки[Уровень] := "N";
		taken[Уровень] := 0;
		Kernel32.LeaveCriticalSection( cs[Уровень] )
	кон ОтпустиБлокировку;


	(* added by Alexey *)
	проц GetMemStatus(перем stat: Kernel32.MemoryStatusEx): булево;
	нач
		stat.dwLength := 64;
		если Kernel32.GlobalMemoryStatusEx(stat) = 1 то
			возврат истина;
		иначе
			возврат ложь;
		всё;
	кон GetMemStatus;

(** dummy procedure to make GC work for both I386 and Win32 *)
проц ДайТекущиеСтекиНаКаждомПроцессоре*(перем п: массив из Стек);
перем i: цел32;
нач
	нцДля i := 0 до МаксКвоПроцессоров-1 делай
		п[i].тек := NilVal;
		п[i].верхнийПределНевключительно := NilVal
	кц
кон ДайТекущиеСтекиНаКаждомПроцессоре;

(* Set machine-dependent parameter gcThreshold *)
проц УстановиПорогСборкиМусора*;
нач
	gcThreshold := 10*1024*1024; (* 10 MB *)
кон УстановиПорогСборкиМусора;

(** Get first memory block and first free address, the first free address is identical to memBlockHead.endBlockAdr *)
проц ДайИзначальныйДиапазонПамятиДляКучи*(перем выхАдресНачалаДанных, выхАдресЗаКонцомДанных, выхАдресСвободногоБлока: адресВПамяти);
нач
	выхАдресНачалаДанных := НУЛЬ; выхАдресЗаКонцомДанных := НУЛЬ; выхАдресСвободногоБлока := НУЛЬ;
кон ДайИзначальныйДиапазонПамятиДляКучи;

(* returns if an address is a currently allocated heap address *)
проц АдресНаКучеЛи¿*(Адр: адресВПамяти): булево;
перем mb: УкльНаЗаголовокБлокаПамяти;
нач
	если (Адр>=операцияАдресОт Kernel32.EntryPoint) и (Адр<=адресОт(LastAddress)) то возврат истина всё;
	если (Адр < первыйБлокПамятиДляКучи.адресНачалаДанных) или (Адр>последнийБлокПамятиДляКучи.адресЗаКонцомДанных) то возврат ложь всё;
	mb := первыйБлокПамятиДляКучи;
	нцПока mb # НУЛЬ делай
		если (Адр >= mb.адресНачалаДанных) и (Адр <= mb.адресЗаКонцомДанных) то  возврат истина  всё;
		mb := mb.следщ;
	кц;
	возврат ложь;
кон АдресНаКучеЛи¿;

проц ДайСведенияОКуче* (перем выхВсего˛Кб, выхСвободноМин˛Кб, выхСвободноМак˛Кб: размерМЗ);
перем
	stat: Kernel32.MemoryStatusEx;
нач
	выхВсего˛Кб := матМаксимум(цел32); выхСвободноМин˛Кб := 0; выхСвободноМак˛Кб := выхВсего˛Кб;
	(*<< added by Alexey *)
	если GetMemStatus(stat) то
		выхВсего˛Кб := устарПреобразуйКБолееУзкомуЦел(stat.ullTotalVirtual DIV 1024);
		выхСвободноМин˛Кб := 0;
		выхСвободноМак˛Кб := устарПреобразуйКБолееУзкомуЦел(stat.ullAvailVirtual DIV 1024);
	всё;
	(* added by Alexey >>*)
кон ДайСведенияОКуче;

(* ug *)
проц ВыведиЗаголовкиБлоковПамятиВТрассу*;
перем memBlock {неОтслСборщиком}: УкльНаЗаголовокБлокаПамяти; i : цел32;
нач
	memBlock := первыйБлокПамятиДляКучи;
	i := 0;
	нцПока memBlock # НУЛЬ делай
		Трассировка.пСтроку8("block "); Трассировка.пЦел64(i, 0); Трассировка.пСтроку8(": startAdr = "); Трассировка.п16ричное(memBlock.адресЭтогоБлока, 0);
		Трассировка.пСтроку8(" size = "); Трассировка.п16ричное(memBlock.разм, 0);
		Трассировка.пСтроку8(" beginBlockAdr = "); Трассировка.п16ричное(memBlock.адресНачалаДанных, 0);
		Трассировка.пСтроку8(" endBlockAdr = "); Трассировка.п16ричное(memBlock.адресЗаКонцомДанных, 0); Трассировка.пВК_ПС;
		memBlock := memBlock.следщ;
		увел(i)
	кц
кон ВыведиЗаголовкиБлоковПамятиВТрассу;

(* insert given memory block in sorted list of memory blocks, sort key is startAdr field - called during GC *)
проц InsertMemoryBlock(memBlock: УкльНаЗаголовокБлокаПамяти);
перем cur {неОтслСборщиком}, prev {неОтслСборщиком}: УкльНаЗаголовокБлокаПамяти;
нач
	cur := первыйБлокПамятиДляКучи;
	prev := НУЛЬ;
	нцПока (cur # НУЛЬ) и (cur.адресЭтогоБлока < memBlock.адресЭтогоБлока) делай
		prev := cur;
		cur := cur.следщ
	кц;
	если prev = НУЛЬ то (* insert at head of list *)
		memBlock.следщ := первыйБлокПамятиДляКучи;
		первыйБлокПамятиДляКучи := memBlock
	иначе (* insert in middle or at end of list *)
		memBlock.следщ := cur;
		prev.следщ := memBlock;
	всё;
		если cur = НУЛЬ то
			последнийБлокПамятиДляКучи := memBlock
		всё
кон InsertMemoryBlock;

	проц ВыделиЕщёПамятьДляКучи*( Мусор: цел32; Размер˛НеМенее˛байт: размерМЗ; перем выхБлок: УкльНаЗаголовокБлокаПамяти; перем выхАдресНачалаДанных, выхАдресЗаКонцомДанных: адресВПамяти );
	перем mBlock: УкльНаЗаголовокБлокаПамяти;  alloc: размерМЗ;  adr,initVal: адресВПамяти; continue: булево;
	нач
		утв(размер16_от(ЗаголовокБлокаПамяти) <= StaticBlockSize); (* make sure MemoryBlock contents fits into one StaticBlock *)
		alloc := Размер˛НеМенее˛байт + StaticBlockSize;
		если alloc < MemBlockSize то alloc := MemBlockSize всё;
		увел( alloc, (-alloc) остОтДеленияНа StaticBlockSize );

		если последнийБлокПамятиДляКучи # НУЛЬ то
			initVal := последнийБлокПамятиДляКучи.адресЭтогоБлока + последнийБлокПамятиДляКучи.разм;
		иначе
			initVal := НУЛЬ
		всё;
		adr := Kernel32.VirtualAlloc(initVal, alloc, {Kernel32.MEMCommit, Kernel32.MEMReserve}, {Kernel32.PageExecuteReadWrite});
		если adr = NilVal то (* allocation failed *)
			adr := Kernel32.VirtualAlloc(NilVal, alloc, {Kernel32.MEMCommit}, {Kernel32.PageExecuteReadWrite});
		всё;
		continue := adr = initVal;
		утв(adr остОтДеленияНа StaticBlockSize = 0); (* is fulfilled because VirtualAlloc is on page granularity *)

	если adr # 0 то

		если continue то
			выхБлок := последнийБлокПамятиДляКучи;
			выхБлок.разм := выхБлок.разм + alloc;

			выхАдресНачалаДанных := последнийБлокПамятиДляКучи.адресЗаКонцомДанных;
			выхАдресЗаКонцомДанных := выхАдресНачалаДанных;
			увел(выхАдресЗаКонцомДанных, alloc);
		иначе
			mBlock := adr;
			mBlock.следщ := НУЛЬ;
			mBlock.адресЭтогоБлока := adr;
			mBlock.разм := alloc;

			выхАдресНачалаДанных := adr + StaticBlockSize;
			выхАдресЗаКонцомДанных := выхАдресНачалаДанных + alloc - StaticBlockSize;

			mBlock.адресНачалаДанных := выхАдресНачалаДанных;
			mBlock.адресЗаКонцомДанных := выхАдресНачалаДанных; (* block is still empty -- Heaps module will set the upper bound *)

			InsertMemoryBlock( mBlock );

			выхБлок := mBlock;
		всё;
	иначе
		выхАдресНачалаДанных := 0; выхАдресЗаКонцомДанных := 0; выхБлок := НУЛЬ;
	всё;
кон ВыделиЕщёПамятьДляКучи;


(* Set memory block end address *)
проц УстАдресЗаКонцомДанныхВЗаголовкеБлокаПамяти*(Заголовок: УкльНаЗаголовокБлокаПамяти; вхАдресЗаКонцомДанных: адресВПамяти);
нач
	утв(вхАдресЗаКонцомДанных >= Заголовок.адресНачалаДанных);
	Заголовок.адресЗаКонцомДанных := вхАдресЗаКонцомДанных
кон УстАдресЗаКонцомДанныхВЗаголовкеБлокаПамяти;

(* Free unused memory block - called during GC *)
проц ОчистьБлокПамяти*(вхБлок: УкльНаЗаголовокБлокаПамяти);
перем cur {неОтслСборщиком}, prev {неОтслСборщиком}: УкльНаЗаголовокБлокаПамяти;
	startAdr: адресВПамяти;
нач
	cur := первыйБлокПамятиДляКучи;
	prev := НУЛЬ;
	нцПока (cur # НУЛЬ) и (cur # вхБлок) делай
		prev := cur;
		cur := cur.следщ
	кц;
	если cur = вхБлок то
		если prev = НУЛЬ то
			первыйБлокПамятиДляКучи := cur.следщ;
		иначе
			prev.следщ := cur.следщ;
			если prev.следщ = НУЛЬ то
				последнийБлокПамятиДляКучи := prev
			всё
		всё;
		вхБлок.следщ := НУЛЬ;
		startAdr := вхБлок.адресЭтогоБлока; (* this value must be cached for the second call of Kernel32.VirtualFree *)
		неважно Kernel32.VirtualFree(вхБлок.адресЭтогоБлока, вхБлок.разм, {Kernel32.MEMDecommit});
		неважно Kernel32.VirtualFree(startAdr , 0, {Kernel32.MEMRelease});
	иначе
		СТОП(535)	(* error in memory block management *)
	всё;
кон ОчистьБлокПамяти;

проц ДайФизическийАдресДиапазонаПамяти*(Адр: адресВПамяти; Разм: размерМЗ): адресВПамяти;
кон ДайФизическийАдресДиапазонаПамяти;

(* function returning the number of processors that are available to Aos *)
проц ДайКвоПроцессоров*( ): цел32;
перем info: Kernel32.SystemInfo;
нач
	Kernel32.GetSystemInfo( info );
	возврат info.dwNumberOfProcessors
кон ДайКвоПроцессоров;

(* function for changing byte order *)
проц ПоменяйПорядокБайтЦел32* (n: цел32): цел32;
машКод
#если AMD64 то
	MOV EAX, [RBP+n]				; load n in eax
	BSWAP EAX						; swap byte order
#аесли I386 то
	MOV EAX, [EBP+n]				; load n in eax
	BSWAP EAX
#иначе
	UNIMPLEMENTED
#кон
кон ПоменяйПорядокБайтЦел32;

	проц  чПорт8*(port: цел32; перем val: симв8);
	кон чПорт8;

	проц  чПорт16*(port: цел32; перем val: цел16);
	кон чПорт16;

	проц  чПорт32*(port: цел32; перем val: цел32);
	кон чПорт32;

	проц  пПорт8*(port: цел32; val: симв8);
	кон пПорт8;

	проц  пПорт16*(port: цел32; val: цел16);
	кон пПорт16;

	проц  пПорт32*(port: цел32; val: цел32);
	кон пПорт32;

нач
	если ~Kernel32.Generic то
		ИнициализируйЯОС
	всё;
	Трассировка.пСтроку8(версияЯОС); Трассировка.пВК_ПС;
кон ЭВМ.
