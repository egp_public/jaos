(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Disks; (** AUTHOR "pjm"; PURPOSE "Abstract disk driver"; *)

использует НИЗКОУР, ЛогЯдра, Modules, Plugins;

конст
	Read* = 0; Write* = 1;	(** Device.Transfer.op *)

		(** res parameter *)
	Ok* = 0;	(** no error *)

		(** common errors - a device may also return its own error codes > 0 *)
	MediaChanged* = 2501;	(** media in removable device was changed unexpectedly *)
	WriteProtected* = 2502;	(** write failed because media is write-protected *)
	Unsupported* = 2503;	(** operation is currently not supported on this device *)
	DeviceInUse* = 2504;	(** the device is open (UpdatePartitionTable) *)
	MediaMissing* = 2505;	(** the device media is not present *)

		(** Device.flags *)
	ReadOnly* = 0;	(** the current media can not be written to (flags only valid after Open) *)
	Removable* = 1;	(** the device has removable media *)

		(** Partition.flags *)
	Mounted* = 0;	(** a file system is currently mounted on the partition (set by file system implementations) *)
	Primary* = 1;	(** a primary partition *)
	Boot* = 2;	(** a bootable partition *)
	Valid* = 3;	(** media contains a valid partition table. *)

	BS = 512;	(* default block size *)

	Trace = ложь;
	TraceBoot = ложь;

	Stats* = истина;

тип
	Message* = запись кон;

	Partition* = запись
		type*: цел32;	(** partition type *)
		start*, size*: цел32;	(** start block and size of partition in blocks *)
		flags*: мнвоНаБитахМЗ;	(** Mounted, Primary, Boot, Valid *)
		ptblock*: цел32;	(** block containing partition table entry *)
		ptoffset*: цел32;	(** offset in partition table of entry, 0 if unknown *)
	кон;
	PartitionTable* = укль на массив из Partition;

(** The base for block devices. It provides operations on an abstract array of disk blocks of blockSize bytes, numbered from 0 to size-1. If applicable, a PC-format partition table starts at block 0, and can be read into the table field with Open or UpdatePartitionTable. *)

	Device* = окласс (Plugins.Plugin)	(** fields read-only, initialized by extender *)
		перем
			blockSize*: цел32;	(** in bytes - unit of block, num & size parameters *)
			flags*: мнвоНаБитахМЗ;	(** ReadOnly, Removable *)
			table*: PartitionTable;	(** cache for partition table *)
			openCount*: цел32;	(** number of times device has been opened *)

			(** statistics *)
			NbytesRead*, NbytesWritten*, (** successfully tranfered bytes *)
			NnofReads*, NnofWrites*, NnofOthers*, (** operation count *)
			NnofErrors* : цел64; (** read/write errors *)

		проц Transfer*(op, block, num: цел32; перем data: массив из симв8; ofs: размерМЗ; перем res: целМЗ);
		нач
			res := Unsupported
		кон Transfer;

		проц GetSize*(перем size: цел32; перем res: целМЗ);
		нач
			res := Unsupported
		кон GetSize;

		проц Handle*(перем msg: Message;  перем res: целМЗ);
		нач
			res := Unsupported
		кон Handle;

		(** Open the device and increment its open count if successful. If the device is opened for the first time, lock it and update its partition table. *)

		проц Open*(перем res: целМЗ);
		перем lockMsg: LockMsg; unlockMsg: UnlockMsg; ignore: целМЗ;
		нач
			res := Ok;
			если openCount = 0 то
				Handle(lockMsg, res);
				если TraceBoot то
					ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("LockMsg = "); ЛогЯдра.пЦел64(res, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
				всё;
				если (res = Ok) или (res = Unsupported) то
					UpdatePartitionTable(сам, res);
					если res # Ok то Handle(unlockMsg, ignore) всё
				всё
			всё;
			если res = Ok то увел(openCount) всё
		кон Open;

		(** Close the device and decrement its open count. Unlock it if the open count has reached 0. *)

		проц Close*(перем res: целМЗ);
		перем unlockMsg: UnlockMsg;
		нач
			res := Ok; утв(openCount > 0);
			умень(openCount);
			если openCount = 0 то
				Handle(unlockMsg, res);
				если TraceBoot то
					ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("UnlockMsg = "); ЛогЯдра.пЦел64(res, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
				всё;
				если res = Unsupported то res := Ok всё;
				table := НУЛЬ
			всё
		кон Close;

	кон Device;

	EjectMsg* = запись (Message) кон;	(** eject the media *)
	SyncMsg* = запись (Message) кон;	(** sync driver caches *)
	LockMsg* = запись (Message) кон;	(** disallow manual ejection *)
	UnlockMsg* = запись (Message) кон;	(** allow manual ejection *)
	SavePowerMsg* = запись (Message) кон;	(** spin down the device *)
	GetGeometryMsg* = запись (Message)	(** return physical geometry *)
		cyls*, hds*, spt*: цел32
	кон;
	ShutdownMsg* = запись (Message) кон;	(** shut down the device (driver) *)

	DiskBlock = массив BS из симв8;

перем
	registry*: Plugins.Registry;

(** Initialize a device. Defaults: blockSize = 512, flags = {}, all methods return res = Unsupported. *)

проц InitDevice*(d: Device);
нач
	d.desc := "";
	d.blockSize := BS; d.flags := {}; d.table := НУЛЬ; d.openCount := 0
кон InitDevice;

(*
Partition table starts at 01BEH in partition table sector and consists of 4 records of the type:

	00    bootind: Types.Byte;
	01    head: Types.Byte;
	02    sectorcyl: Types.Byte;
	03    cyl: Types.Byte;
	04    type: Types.Byte;
	05    head2: Types.Byte;
	06    sector2cyl: Types.Byte;
	07    cyl2: Types.Byte;
	08    start: Types.DWord;
	12    num: Types.DWord

References:
	MSKB Q69912 MS-DOS Partitioning Summary
	MSKB Q51978 Order in Which MS-DOS and Windows 95 Assigns Drive Letters
	MSKB Q151414 Windows 95 Partition Types Not Recognized by Windows NT
	MSKB Q93373 Default Drive Letters and Partitions in Windows NT
*)

проц Resize(перем p: PartitionTable;  n: размерМЗ);
перем old: PartitionTable;  i, len: размерМЗ;
нач
	len := длинаМассива(p);  нцПока len < n делай len := 2*len кц;
	old := p;  нов(p, len);
	нцДля i := 0 до длинаМассива(old)-1 делай p[i] := old[i] кц
кон Resize;

проц Get4(перем b: массив из симв8;  i: цел32): цел32;
нач
	возврат кодСимв8(b[i]) + арифмСдвиг(кодСимв8(b[i+1]), 8) + арифмСдвиг(кодСимв8(b[i+2]), 16) + арифмСдвиг(кодСимв8(b[i+3]), 24)
кон Get4;

проц Extended(type: цел32): булево;
нач
	возврат (type = 5) или (type = 15)
кон Extended;

проц ValidFlag(f: симв8): булево;
нач
	возврат (f = 0X) или (f = 80X) или (f = 81X)
кон ValidFlag;

(* Read primary partition table entries into p *)

проц ReadPrimary(перем b: DiskBlock; dev: Device;  перем p: PartitionTable;  перем n: цел32; перем res: целМЗ; перем valid: булево);
перем e, size, i: цел32;
нач
	n := 0;
	dev.Transfer(Read, 0, 1, b, 0, res);
	если (res = Ok) и (b[510] = 055X) и (b[511] = 0AAX) то	(* signature ok *)
		valid := ValidFlag(b[01BEH]) и ValidFlag(b[01BEH+16]) и ValidFlag(b[01BEH+32]) и ValidFlag(b[01BEH+48]);
		если valid то
			нцДля i := 0 до 3 делай
				e := 01BEH + 16*i;  size := Get4(b, e+12);
				если (b[e+4] # 0X) и (size # 0) то	(* non-empty partition *)
					Resize(p, n+1);  p[n].type := кодСимв8(b[e+4]);
					p[n].start := Get4(b, e+8);  p[n].size := size; p[n].flags := {Valid, Primary};
					если b[e] # 0X то включиВоМнвоНаБитах(p[n].flags, Boot) всё;
					p[n].ptblock := 0; p[n].ptoffset := e;
					увел(n)
				всё
			кц
		всё
	иначе
		если Trace то
			ЛогЯдра.пСтроку8("Disks: ReadPrimary = "); ЛогЯдра.пЦел64(res, 1);
			ЛогЯдра.пСтроку8(" on "); ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пВК_ПС;
			если res = 0 то ЛогЯдра.пБлокПамяти16рично(адресОт(b[0]), BS) всё
		всё
	всё
кон ReadPrimary;

(* Read "logical drive" partitions into p *)

проц ReadLogical(перем b: DiskBlock; dev: Device;  first: цел32;  перем p: PartitionTable;  перем n: цел32; перем res: целМЗ);
перем e, sec, size, i: цел32; found: булево;
нач
	sec := first;
	нцДо
		found := ложь;
		dev.Transfer(Read, sec, 1, b, 0, res);
		если (res = Ok) и (b[510] = 055X) и (b[511] = 0AAX) то
			нцДля i := 0 до 3 делай	(* look for partition entry (max one expected) *)
				e := 01BEH + 16*i;  size := Get4(b, e+12);
				если (b[e+4] # 0X) и ~Extended(кодСимв8(b[e+4])) и (size # 0) то
					Resize(p, n+1);  p[n].type := кодСимв8(b[e+4]);
					p[n].start := sec + Get4(b, e+8);  p[n].size := size; p[n].flags := {Valid};
					если b[e] # 0X то включиВоМнвоНаБитах(p[n].flags, Boot) всё;
					p[n].ptblock := sec; p[n].ptoffset := e;
					увел(n)
				всё
			кц;
			i := 0;
			нцПока (i # 4) и ~found делай	(* look for nested extended entry (max one expected) *)
				e := 01BEH + 16*i;  size := Get4(b, e+12);
				если Extended(кодСимв8(b[e+4])) и (size # 0) то	(* found *)
					sec := first + Get4(b, e+8);
					i := 4;  found := истина
				иначе
					увел(i)
				всё
			кц
		иначе
			если Trace то
				ЛогЯдра.пСтроку8("Disks: ReadLogical = ");  ЛогЯдра.пЦел64(res, 1);
				ЛогЯдра.пСтроку8(" on ");  ЛогЯдра.пСтроку8(dev.name);
				ЛогЯдра.пСтроку8(" sector ");  ЛогЯдра.пЦел64(sec, 1);  ЛогЯдра.пВК_ПС
			всё
		всё
	кцПри ~found
кон ReadLogical;

(** Read a PC-format partition table starting at block 0 and initialize dev.table. dev.table[0] is a virtual
partition spanning the entire device, with type = 256. If the device has been opened before, do nothing and
return DeviceInUse, otherwise return Ok. On any other error dev.table is set NIL. *)

проц UpdatePartitionTable*(dev: Device; перем res: целМЗ);
перем p, t: PartitionTable; i, pn, tn, size: цел32; buf: DiskBlock; valid: булево;
нач
	если dev.openCount = 0 то
		tn := 0; res := Ok;
		dev.table := НУЛЬ;
		dev.GetSize(size, res);
		если (res = Ok) и (size = 0) то res := MediaMissing всё;	(* workaround for broken drivers *)
		если (res = Ok) и (dev.blockSize = BS) то
			нов(p, 4); нов(t, 8);
			ReadPrimary(buf, dev, p, pn, res, valid);
			i := 0;
			нцПока valid и (i # pn) и (res = Ok) делай
				Resize(t, tn+1);  t[tn] := p[i];  увел(tn);
				если Extended(p[i].type) то
					ReadLogical(buf, dev, p[i].start, t, tn, res)
				всё;
				увел(i)
			кц
		всё;
		если res = Ok то
			нов(dev.table, tn+1);
			dev.table[0].type := 256;
			если valid то dev.table[0].flags := {Valid} иначе dev.table[0].flags := {} всё;
			dev.table[0].start := 0; dev.table[0].size := size;
			нцДля i := 1 до tn делай dev.table[i] := t[i-1] кц
		всё
	иначе
		res := DeviceInUse	(* could not update partition table *)
	всё;
	если TraceBoot то
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("UpdatePartitionTable = "); ЛогЯдра.пЦел64(res, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё
кон UpdatePartitionTable;

(* Clean up the installed devices. *)
проц Cleanup;
перем dev: Plugins.Table; i: размерМЗ; res: целМЗ; msg: ShutdownMsg;
нач
	registry.GetAll(dev);
	если dev # НУЛЬ то
		нцДля i := 0 до длинаМассива(dev^)-1 делай
			dev[i](Device).Handle(msg, res)	(* ignore res *)
		кц
	всё
кон Cleanup;

нач
	нов(registry, "Disks", "Disk drivers");
	Modules.InstallTermHandler(Cleanup)
кон Disks.

(*
29.10.1999	pjm	Started
19.01.1999	pjm	32-bit block numbers again
22.01.1999	pjm	Hash call error in Acquire fixed
16.02.1999	pjm	LRU design error fixed
04.05.1999	pjm	Ported over new things from Native version, added partitioning
20.10.2000	pjm	Added Cleanup and Valid from Native
26.03.2007	staubesv Added Device.NnofReads, Device.NnofWrites, Device.NnofOthers and Device.NnofErrors

Block size 512 and SIGNED32 block number limit us to 1TB = 1024GB.
*)
