# Сборка и запуск на Zybo-z7-10

## Обозначения

В ЯОС можно создавать именованные файловые системы. Файловая система ЯОС: указывает на корень репозитория (если всё правильно настроено
в скрипте запуска). Поэтому, если написано "Удалите директорию ЯОС:/что-то-там", то не надо удалять директорию ЯОС. 

## Выберите подходящую версию ЯОС

Например, ветку git "Zybo-Z710-ЯОС-2". Может повести и с веткой main (подходит только англоязычная ветка).
На ветку нужно переключиться с помощью git. Также собирается A2 ревизии 9953 (2020-03-03, сообщение "added support of building A2 for Zybo Z7-10 board from Digilent Inc.")

## Раздобудьте оборудование

Вам понадобится: 
- Zybo-z7-10
- Блок питания 5в 2.4а бочко-джек 21мм плюс внутри, например, GS15E-1P1J 
- Кабель USB - Micro USB
- HDMI-провод и монитор, который его понимает
- карта Micro SD, не все карты подходят, но карта с надписями
"Micro SD HC C 4 8 Gb C08G TAIWAN Kingston SDC4/8GB 14 F(C)" подошла (должна быть не ниже 4 класса скорости)
- устройство, с помощью которого вы запишете образ ОС на SD карту (в моём случае это SD ридер в компьютере под управлением Windows и переходник SD-MicroSD)

## Скачайте программы

- Зайдите в Windows (по Linux см. отдельный раздел в конце)
- Установите Win32 Disk Imager
- Установите Putty

## Запустите ЯОС

- создайте директорию ЯОС:/Win64/Work
- выполните ЯОС:/Win64/a2.bat

## Соберите ЯОС 

- Если вы собираете ЯОС для Zybo повторно, то перезапустите ЯОС и удалите директорию ЯОС:/Win64/Work/build
- Откройте лог ядра (F4)
- откройте файл с командами сборки ИСР.Открой ЯОС:ARM/ZyboZ710.Tool ~
- найдите команду для порождения файла замен для режима 1024x768 и выполните её
- найдите команду System.DoCommands, после которой есть комментарий "(* обеспечим находимость диркектории ARM *)" и выполните её. 
- выполните следующую команду System.DoCommands, после неё идёт комментарий "Cleanup bootloader-specific files"
- через минуту или около того должно показаться сообщение "ZyboZ710 Zynq A2 image has been built!"
- должен возникнуть файл ЯОС:/Win64/Work/build/ZyboZ710.ZynqA2.img

## Запишите образ на SD карту

- с помощью программы Win32 Disk Imager запишите образ на SD карту (старые данные на карте пропадут)

## Подготовьте Zybo

В этом пункте, если в чём-то не уверены - читайте руководство по Zybo.

- Поставьте джампер JP6 в положение "WALL" (питание от адаптера)
- поставьте джампер JP5 в положение "SD"
- замкните контакты джампера JP2 (хотя это для данного опыта неважно)
- поставьте SD карту с записанным образом в разъём платы
- убедитесь, что выключатель питания находится в положении "Off"
- включите адаптер питания в сеть
- подключите адаптер питания
- подключите HDMI и монитор к левому разъёму (HDMI TX)
- включите монитор
- подключите шнур USB-MicroUSB к компьютеру и разъёму J12

## Запуск

- включите Zybo выключателем. Сразу должен загореться диод LD13 PGood, а через 5-10 секунд - 
диоды LD12 Done и LD6, и в это же время должна показаться картинка рабочего стола на экране.
- в "диспетчере устройств" ПК найдите Порты(COM и LPT) и внутри должно быть что-то типа USB-Serial. Оттуда берёте номер COM-порта. Он появляется только при включённой Zybo, например, COM5
- запустите (на компьютере) putty и задайте параметры:
    - тип соединения (connection type) - Serial
    - порт (serial line) - COM5
    - скорость (speed) -  115200
- далее в putty раскройте дерево свойств Connection/Serial и установите:
    - Data bits 8
    - Stop bits 1
    - Parity: none
    - Flow control: none (DTR/RTS enable)
- подключитесь (в дереве выберите session и нажмите кнопку Open)
- появится чёрный экран, нажмите Enter, должна появиться подсказка "Zynq A2 >"
- переключитесь в английскую раскладку и подайте команды, например, `PET.Open Configuration.XML` и нажмите Enter (не `~`). На мониторе, подключённом к Zybo, должно появиться окно редактора. Или `справка`, а затем - Enter. 
- мышь и клавиатура по идее должны подключаться через USB разветвитель, но они не работают
- сеть не работает. По идее, она должна бы заработать после команды XEmac.Install, которую нужно подавать при подключенном проводе Ethernet, но не заработала.
- всё (выключите питание Zybo и отключите все проводочки)

...здесь раньше был ещё текст, неоформленные записки. Он касался FPGA. Если нужен - ищите в истории файла...