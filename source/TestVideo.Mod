модуль TestVideo; (** AUTHOR "thomas.frey@alumni.ethz.ch"; PURPOSE "Computer Vision Experiments"; *)

использует
	Kernel, Modules, Raster, VideoExample, Commands, Options, ЛогЯдра, Random, WMGraphics, WMRectangles, Kernel32, НИЗКОУР, Vectors := W3dVectors, Math := MathL;

конст
	Ok* = 0;
	TooManyLabels* = 1;
	PathTooLong* = 2;
	DirN = 0; DirNE = 1; DirE = 2; DirSE = 3; DirS = 4; DirSW = 5; DirW = 6; DirNW = 7;
	DebugLabeler = ложь;
	DebugTracer = ложь;
	DebugLiner = ложь;

тип
	LabelInfo* = запись
		firstPos : цел32;
		nofPixels : цел32;
		label : цел16;
	кон;

	Point = запись
		x, y : размерМЗ;
	кон;

перем
	threshold, pixThreshold : цел32;
	labelBuffer : укль на массив из цел16;
	equivalence : массив 32*1024 из цел16;
	labelInfo : массив 32*1024 из LabelInfo;
	labelColor : массив 32*1024 из цел32;
	g : WMGraphics.BufferCanvas;
	dirX, dirY : массив 8 из цел32;
	rectified : VideoExample.PreviewWindow;

	intensityBuffer, thresholdBuffer : укль на массив из симв8;

	проц RGBToYUVReal(r, g, b : цел32; перем y, u, v : цел32);
	нач
		y := округлиВниз(0.299 * r + 0.587 * g + 0.114 * b);
		u := округлиВниз(128  - 0.16874 * r - 0.33126 * g + 0.5 * b);
		v := округлиВниз(128 + 0.5 * r - 0.41869 * g - 0.08131 * b);
	кон RGBToYUVReal;

	(** Analytical solution for homography for the case of 4 points mapping to the unit rectangle.
		According to "ProjectiveMappings for Image Warping" by Paul Heckbert, 	15-869, Image-Based Modeling and Rendering *)
	проц CalculateUnitSquareHomography(конст p : массив из Point; перем H, inverse : массив  из вещ64);
	перем sx, sy, dx1, dy1, dx2, dy2, a, b, c, d, e, f, g, h, z : вещ64;
	нач
		sx := (p[0].x - p[1].x) + (p[2].x - p[3].x);
		sy := (p[0].y - p[1].y) + (p[2].y - p[3].y);
		dx1 := p[1].x - p[2].x;
		dx2 := p[3].x - p[2].x;
		dy1 := p[1].y - p[2].y;
		dy2 := p[3].y - p[2].y;

		z := dx1 * dy2 - dy1 * dx2;
		g := (sx * dy2 - sy * dx2) / z;
		h := (sy * dx1 - sx * dy1) / z;

		a := p[1].x - p[0].x + g * p[1].x;
		b := p[3].x - p[0].x + h * p[3].x;
		c := p[0].x;
		d := p[1].y - p[0].y + g * p[1].y;
		e := p[3].y - p[0].y + h * p[3].y;
		f := p[0].y;
		H[0] := a; H[1] := b; H[2] := c;
		H[3] := d; H[4] := e; H[5] := f;
		H[6] := g; H[7] := h; H[8] := 1;

		(* inverse transformation *)
		inverse[0] := e - f * h; inverse[1] := c * h - b; inverse[2] := b * f - c * e;
		inverse[3] := f * g - d; inverse[4] := a - c * g; inverse[5] := c * d - a * f;
		inverse[6] := d * h - e * g; inverse[7] := b * g - a * h; inverse[8] := a * e - b * d
	кон CalculateUnitSquareHomography;

	проц MapProjective(конст H : массив из вещ64; u, v : вещ64; перем x, y : вещ64);
	нач
		x := (H[0] * u + H[1] * v + H[2]) / (H[6] * u + H[7] * v + 1);
		y := (H[3] * u + H[4] * v + H[5]) / (H[6] * u + H[7] * v + 1)
	кон MapProjective;

	проц MapInverseProjective(конст H : массив из вещ64; u, v : вещ64; перем x, y : вещ64);
	перем z : вещ64;
	нач
		x := (H[0] * u + H[1] * v + H[2]) / (H[6] *u + H[7] * v + 1);
		y := (H[3] * u + H[4] * v + H[5]) / (H[6] *u + H[7] * v + 1);
		z := (H[6] * u + H[7] * v + H[8]) / (H[6] *u + H[7] * v + 1);
		x := x / z;
		y := y / z;
	кон MapInverseProjective;

проц Transform(src, dst : Raster.Image; конст points : массив из Point);
перем h, hinv : массив 9 из вещ64;
	x,  y, six, siy : размерМЗ;
	u, v, sx, sy : вещ64;
	mode : Raster.Mode;
	pix : Raster.Pixel;
нач
	Raster.InitMode(mode, Raster.srcCopy);
	CalculateUnitSquareHomography(points, h, hinv);
	нцДля y := 0 до dst.height - 1 делай
		v := y / dst.height;
		нцДля x := 0 до dst.width - 1 делай
			u := x / dst.width;
			MapProjective(h, u, v, sx, sy);
			six := округлиВниз(sx + 0.5); siy := округлиВниз(sy + 0.5);
			если (six > 0) и (siy > 0) и (six < src.width) и (siy < src.height) то
				Raster.Get(src, six, siy, pix, mode);
			иначе Raster.SetRGBA(pix, 0, 0, 0, 255)
			всё;
			Raster.Put(dst, x, y, pix, mode)
		кц
	кц
кон Transform;

проц SearchHVLines(buffer : Raster.Image);
перем x, y : размерМЗ; tr, tg, tb, ta : цел32;
	sum : цел32;
	hArray, vArray : массив 2048 из цел32;
	mode : Raster.Mode;
	pix : Raster.Pixel;
нач
	Raster.InitMode(mode, Raster.srcCopy);
	нцДля y := 0 до buffer.height - 1 делай
		нцДля x := 0 до buffer.width - 1 делай
			Raster.Get(buffer, x, y, pix, mode); Raster.GetRGBA(pix, tr, tg, tb, ta);
			sum := (tr + tg + tb);
			увел(hArray[x], sum);
			увел(vArray[y], sum);
		кц
	кц;
	нцДля y := 0 до buffer.height - 1 делай
		если vArray[y] < threshold * buffer.width то
			rectified.canvas.Line(0, y, buffer.width, y, 0FF00FFH, WMGraphics.ModeCopy);
		всё
	кц;
	нцДля x := 0 до buffer.width - 1 делай
		если hArray[x] < threshold * buffer.height то
			rectified.canvas.Line(x, 0, x, buffer.height, 0FF00FFH, WMGraphics.ModeCopy);
		всё
	кц
кон SearchHVLines;

проц IsEmptyField(buffer : Raster.Image; x, y , w, h : размерМЗ) : булево;
перем i, j : размерМЗ; tr, tg, tb, ta : цел32;
	mode : Raster.Mode;
	pix : Raster.Pixel;
	nonEmpty : цел32;
нач
	Raster.InitMode(mode, Raster.srcCopy);
	nonEmpty := 0;
	нцДля j := y до y + h - 1 делай
		нцДля i := x до x + w - 1 делай
			Raster.Get(buffer,i,j, pix, mode); Raster.GetRGBA(pix, tr, tg, tb, ta);
			если (tr + tg + tb) < threshold  то
				Raster.SetRGBA(pix, 255, 0, 0, 255); Raster.Put(buffer, i, j, pix, mode);
				увел(nonEmpty)
			всё
		кц
	кц;
	возврат nonEmpty < 8*w*h DIV 100;
кон IsEmptyField;

проц Dist(buffer : Raster.Image; x0, y0, x1, y1, w, h : размерМЗ) : цел32;
перем i, j : размерМЗ; tr, tg, tb, ta, s0, s1 : цел32;
	mode : Raster.Mode;
	pix : Raster.Pixel;
	sum : цел32;
нач
	Raster.InitMode(mode, Raster.srcCopy);
	sum := 0;
	нцДля j := 0 до h - 1 делай
		нцДля i := 0 до w - 1 делай
			Raster.Get(buffer, x0 + i, y0 + j, pix, mode); Raster.GetRGBA(pix, tr, tg, tb, ta); s0 := (tr + tg + tb);
			Raster.Get(buffer, x1 + i, y1 + j, pix, mode); Raster.GetRGBA(pix, tr, tg, tb, ta); s1 := (tr + tg + tb);
			sum := sum + (threshold - s0) * (threshold - s1);
		кц
	кц;
	возврат sum;
кон Dist;

проц CheckFields(buffer : Raster.Image);
перем i, j, x, y, w, h : размерМЗ;
	empty : массив 9, 9 из булево;
	nofNumbers : размерМЗ;
	numbers : массив 81 из размерМЗ;

	cluster: массив 81 из запись
		nofFields : цел8;
		fields : массив 81 из цел8;
	кон;
	distance, dist : массив 81, 81 из цел32;

	проц SetDist(a, b : размерМЗ; d : цел32);
	нач
		если a < b то dist[a, b] := d
		иначе dist[b, a] := d
		всё
	кон SetDist;

	проц GetDist(a, b : размерМЗ) : цел32;
	нач
		если a < b то возврат dist[a, b]
		иначе возврат dist[b, a]
		всё
	кон GetDist;

	проц GetSmallest(перем maxi, maxj : размерМЗ);
	перем max : цел32;
		first : булево;
	нач
		first := истина;
		нцДля j := 0 до nofNumbers - 1 делай
			нцДля i := 0 до j - 1 делай
				если first то
					max := GetDist(i, j);
					maxi := i; maxj := j;
					first := ложь
				иначе
					если GetDist(i, j) > max то
						max := GetDist(i, j);
						maxi := i; maxj := j;
					всё
				всё
			кц;
		кц
	кон GetSmallest;

	проц Cluster;
	перем i : размерМЗ;
	нач
		нцДля i := 0 до 81 - 1 делай cluster[i].nofFields := 0; cluster[i].fields[0] := цел8(i) кц;
		нцДля i := 0 до nofNumbers - 1 делай cluster[i].nofFields := 1 кц;
		нцДля i := 0 до nofNumbers - 1 делай

		кц;
	кон Cluster;

нач
	w := buffer.width DIV 9 - 5;
	h := buffer.height DIV 9 - 5;
	nofNumbers := 0;
	нцДля j := 0 до 9 - 1 делай
		нцДля i := 0 до 9 - 1 делай
			x := (i * buffer.width DIV 9) + 5;
			y := (j * buffer.height DIV 9) + 5;
			если IsEmptyField(buffer, x, y, w, h) то
				empty[j, i] := истина;
				rectified.canvas.Fill(WMRectangles.MakeRect(x, y, x + w, y + h), 00FF80H, WMGraphics.ModeSrcOverDst);
			иначе empty[j, i] := ложь;
				numbers[nofNumbers] := 9 * j + i;
				увел(nofNumbers)
			всё
		кц
	кц;
	нцДля j := 0 до nofNumbers - 1 делай
		нцДля i := 0 до j - 1 делай
			distance[j, i] := Dist(buffer,
				(numbers[j] DIV 9) * buffer.width DIV 9 + 4, (numbers[j] остОтДеленияНа 9) * buffer.height DIV 9 + 4,
				(numbers[i] DIV 9) * buffer.width DIV 9 + 4, (numbers[i] остОтДеленияНа 9) * buffer.height DIV 9 + 4,
				w, h);
			SetDist(j, i, distance[j, i]);
(*			KernelLog.Int(distance[j, i], 0); KernelLog.String(" "); *)
		кц;
		ЛогЯдра.пВК_ПС;
	кц;
кон CheckFields;

(**
	Labels 8-way connected components in the image. Max components that can be found 32768.
	buffer : the image that should be labled
	labelBuffer : buffer with at least w * h integers for labels
	equivalenceBuffer : storage space for maxLabels label;
	colorThreshold : ...
	*)
проц BinaryLabler*(buffer : Raster.Image; перем labelBuffer, equivalence : массив из цел16; colorThreshold, pixelThreshold, maxLabels : цел32;
	перем labelInfo : массив из LabelInfo;
	unifyLabels : булево; перем nofFLabels : цел32; перем res : целМЗ);
перем i, x, y, w, h: размерМЗ; color: цел32;
	tr, tg, tb, ta  : цел32;
	mode : Raster.Mode;
	pix : Raster.Pixel;
	nofLabels : цел16;
	lbufpos, lastLineLbufPos, minClass : цел32;
	lastsum, sum, cl, ctl, ct, ctr, tlabel : цел16;
	adr : адресВПамяти;
	ch : симв8;

	проц Equivalence(x, y : цел32);
	нач
		если x > y то equivalence[x] := устарПреобразуйКБолееУзкомуЦел(y) иначе equivalence[y] := устарПреобразуйКБолееУзкомуЦел(x) всё
	кон Equivalence;

	проц NewLabel(lbufPos : цел32);
	нач
		если nofLabels < maxLabels то
			увел(nofLabels);
			labelBuffer[lbufpos] := nofLabels;
			labelInfo[nofLabels].firstPos := lbufPos;
			labelInfo[nofLabels].nofPixels := 1
		иначе
			res := TooManyLabels;
		всё
	кон NewLabel;

нач
	утв(maxLabels <= матМаксимум(цел16));
	утв(длинаМассива(equivalence) >= maxLabels);
	утв(длинаМассива(labelBuffer) >= w*h);
	res := Ok;
	w := buffer.width; h := buffer.height;
	(* initialize equivalences *)
	нцДля i := 0 до длинаМассива( equivalence) - 1 делай equivalence[i] := цел16(i) кц;

	Raster.InitMode(mode, Raster.srcCopy);

	nofLabels := 0;
	(* first line *)
	lbufpos := 0;
	нцДля x := 0 до w - 1 делай
		Raster.Get(buffer, x, 0, pix, mode); Raster.GetRGBA(pix, tr, tg, tb, ta);
		если (tr + tg + tb < threshold) то
			если (x > 0) и (labelBuffer[lbufpos - 1] > 0) то
				labelBuffer[lbufpos] := labelBuffer[lbufpos - 1]
			иначе NewLabel(lbufpos)
			всё;
		иначе labelBuffer[lbufpos] := 0
		всё;
		увел(lbufpos)
	кц;

	lastLineLbufPos := 0;
	нцДля y := 1 до h - 1 делай
		adr := buffer.adr + y *  buffer.bpr;
		НИЗКОУР.прочтиОбъектПоАдресу(adr, ch); lastsum := кодСимв8(ch); увел(adr);
		НИЗКОУР.прочтиОбъектПоАдресу(adr, ch); lastsum := lastsum + кодСимв8(ch); увел(adr);
		НИЗКОУР.прочтиОбъектПоАдресу(adr, ch); lastsum := lastsum + кодСимв8(ch); увел(adr);
		увел(lbufpos); увел(lastLineLbufPos);
		нцДля x := 1 до w - 1 делай
			(*Raster.Get(buffer, x, y, pix, mode); Raster.GetRGBA(pix, tr, tg, tb, ta);*)
			НИЗКОУР.прочтиОбъектПоАдресу(adr, ch); sum := кодСимв8(ch); увел(adr);
			НИЗКОУР.прочтиОбъектПоАдресу(adr, ch); sum := sum + кодСимв8(ch); увел(adr);
			НИЗКОУР.прочтиОбъектПоАдресу(adr, ch); sum := sum + кодСимв8(ch); увел(adr);

			lastsum := sum;
			если ( sum (* tr + tg + tb*) < threshold) то
				если (x > 0)  то
					cl := labelBuffer[lbufpos - 1];
					ctl := labelBuffer[lastLineLbufPos - 1];
				иначе cl := 0; ctl := 0
				всё;
				ct := labelBuffer[lastLineLbufPos];
				если x < w - 1 то ctr := labelBuffer[lastLineLbufPos + 1] иначе ctr := 0 всё;
				если (cl + ctl + ct + ctr = 0)(*(cl = 0) & (ctl = 0) & (ct = 0) & (ctr = 0)*) то NewLabel(lbufpos)
				иначе
					minClass := 0FFFFH;
				 	если (cl # 0) и (cl < minClass) то minClass := cl всё;
				 	если (ctl # 0) и (ctl < minClass) то minClass := ctl всё;
				 	если (ct # 0) и (ct < minClass) то minClass := ct всё;
				 	если (ctr # 0) и (ctr < minClass) то minClass := ctr всё;
				 	если equivalence[minClass] < minClass то minClass := equivalence[minClass] всё;
				 	labelBuffer[lbufpos] := устарПреобразуйКБолееУзкомуЦел(minClass);
				 	увел(labelInfo[minClass].nofPixels);
				 	если (cl # 0) и (cl # minClass) то Equivalence(minClass, cl) всё;
				 	если (ctl # 0) и (ctl # minClass) то Equivalence(minClass, ctl) всё;
				 	если (ct # 0) и (ct # minClass) то Equivalence(minClass, ct) всё;
				 	если (ctr # 0) и (ctr # minClass) то Equivalence(minClass, ctr) всё;
				всё;
			иначе labelBuffer[lbufpos] := 0
			всё;
			увел(lbufpos);
			увел(lastLineLbufPos)
		кц
	кц;

	(* ensure all equivalences are pointing to the lowest numbered label id *)
	нцДля i := 1 до nofLabels - 1 делай
		если equivalence[i] < i то нцПока equivalence[equivalence[i]] < equivalence[i] делай equivalence[i] := equivalence[equivalence[i]] кц всё;
	кц;

	если unifyLabels то
		нцДля i := 0 до w * h - 1 делай labelBuffer[i] := equivalence[labelBuffer[i]] кц
	всё;

	(* sum up the pixel sizes and adjust the first position of the region *)
	нцДля i := 1 до nofLabels - 1 делай
		если equivalence[i] # i то
			labelInfo[equivalence[i]].firstPos := матМинимум(labelInfo[equivalence[i]].firstPos, labelInfo[i].firstPos);
			увел(labelInfo[equivalence[i]].nofPixels, labelInfo[i].nofPixels);
			labelInfo[i].nofPixels := 0;
			labelInfo[i].label := equivalence[i]
		всё;
	кц;

	если DebugLabeler то
		lbufpos := 0;
		нцДля y := 0 до h - 1 делай
			нцДля x := 0 до w - 1 делай
				tlabel := equivalence[labelBuffer[lbufpos]];
				если (tlabel>0) и  (labelInfo[tlabel].nofPixels >= pixelThreshold) то color := labelColor[tlabel]
				иначе color := цел32(0FFFFFFFFH);
					Raster.SetRGBA(pix, ((color DIV 65536) DIV 256) остОтДеленияНа 256, (color DIV 65536) остОтДеленияНа 256,
						(color DIV 256) остОтДеленияНа 256, 255);
					Raster.Put(buffer, x, y, pix, mode);
				всё;
				увел(lbufpos);
			кц
		кц
	всё;
	(* count and compress the labels *)
	nofFLabels := 0;
	нцДля i := 1 до nofLabels - 1 делай
		если (equivalence[i] = i) и (labelInfo[i].nofPixels >= pixelThreshold) то
			labelInfo[nofFLabels] := labelInfo[i];
			увел(nofFLabels)
		всё;
	кц;

кон BinaryLabler;

(* trace a region in the label buffer. The image buffer is used for the width and height and debug output.*)
проц Trace(buffer : Raster.Image; конст labelBuffer : массив из цел16; перем labelInfo :  LabelInfo;
			перем length : цел32; перем path : массив из Point;
			перем res : целМЗ);
перем x, y, tx, ty : размерМЗ;
	w, h, i, j: размерМЗ;
	dir, p, p2 : размерМЗ;
	mode : Raster.Mode;
	pix : Raster.Pixel;
	deltaX, deltaY : массив 8 из размерМЗ;
нач
	res := Ok;
	w := buffer.width; h := buffer.height;
	x := labelInfo.firstPos остОтДеленияНа w; y := labelInfo.firstPos DIV w;
	Raster.SetRGBA(pix, 255, 255, 0, 255);
	Raster.Put(buffer, x, y, pix, mode);

	deltaX[DirN] := 0; deltaY[DirN] := -w; (* N *)
	deltaX[DirNE] := 1; deltaY[DirNE] := -w; (* NE *)
	deltaX[DirE] := 1; deltaY[DirE] := 0; (* E *)
	deltaX[DirSE] := 1; deltaY[DirSE] := w; (* SE *)
	deltaX[DirS] := 0; deltaY[DirS] := w; (* S *)
	deltaX[DirSW] := -1; deltaY[DirSW] := w; (* SW *)
	deltaX[DirW] := -1; deltaY[DirW] := 0; (* W *)
	deltaX[DirNW] := -1; deltaY[DirNW] := -w; (* NW *)

	length := 0;
	p := labelInfo.firstPos;
	x := p остОтДеленияНа w; y := p DIV w;
	dir := 5;
	j := 0;
	нц
		если length >= длинаМассива(path) то res := PathTooLong; прервиЦикл всё;
		dir := (dir + 5) остОтДеленияНа 8;
		i := 0;
		нц
			увел(i);
			если i >  8 то возврат всё;
			p2 := p + deltaX[dir] + deltaY[dir];
			tx := x + dirX[dir];
			ty := y + dirY[dir];
			если (tx >= 0) и (tx < w) и (ty >=  0) и (ty < h) и (labelBuffer[p2] # 0) то прервиЦикл всё;
			dir := (dir + 1) остОтДеленияНа 8;
		кц;
		p := p2;
		x := tx; y := ty;

		если DebugTracer то
			если g = НУЛЬ то нов(g, buffer) всё;
			g.Fill(WMRectangles.MakeRect(x-1, y-1, x+1, y+1), 0FFFFH, WMGraphics.ModeCopy);
		всё;
		(* SLOW *)
		path[length].x := p остОтДеленияНа w;
		path[length].y := p DIV w;
		увел(length);
		если p = labelInfo.firstPos то прервиЦикл всё;
	кц;

кон Trace;

проц SimplifyPoly(перем path : массив из Point; nofPoints, tolerance: цел32; перем resultPoint: цел32);
перем i, j : цел32;
	dir0, dir1 : Vectors.TVector2d;
нач
	если nofPoints > 2 то
		i := 2; j := 1;
		нцПока i < nofPoints делай
			dir0 := Vectors.VNormed2(Vectors.Vector2d(path[j].x - path[j - 1].x, path[j].y - path[j - 1].y));
			dir1 := Vectors.VNormed2(Vectors.Vector2d(path[i].x - path[i - 1].x, path[i].y - path[i - 1].y));
			если Vectors.Scalar2(dir0, dir1) < 0.8 то увел(j) всё;
			path[j] := path[i];
			увел(i);
		кц
	всё;
	resultPoint := j+ 1;
кон SimplifyPoly;

проц ExtractLines(buffer : Raster.Image; конст path : массив из Point; pathLength : цел32; перем poly : массив из Point; перем nofPoints : цел32 );
перем i, p, nofLines, straight, nonStraight : цел32;
	l: цел32;

	проц IsLine(from, to, l : цел32) : булево;
	перем i, d : размерМЗ;
		x0, x1, y0, y1, px, py : размерМЗ;
	нач
		i := from;
		x0 := path[from].x; y0 := path[from].y;
		x1 := path[to].x; y1 := path[to].y;

		увел(i);
		нцПока i < to делай
			px := path[i].x; py := path[i].y;
			d := матМодуль((x1 - x0) * (y0 - py) - (x0 - px) * (y1 - y0));
			 (* / SQRT(SQR(x1-x0) + SQR(y1-y0)) *)
			если d > l то возврат ложь всё;
			увел(i);
		кц;
		возврат истина
	кон IsLine;

нач
	утв(длинаМассива(poly) >= 3);
	nofLines := 0; nonStraight := 0; straight := 0;
	p := 3; i := 0;
	нцПока p < pathLength делай
		если IsLine(i, p, 2) то
			l := 6;
			нцПока ((i + l) < pathLength) и IsLine(i, i + l, l) делай увел(l, 2) кц;
			если (i + l) >= pathLength то l := pathLength - i - 1 всё;
			нцПока ~IsLine(i, i + l, l) делай умень(l) кц;
			p := i + l;
			если DebugLiner то
				если g = НУЛЬ то нов(g, buffer) всё;
				g.Fill(WMRectangles.MakeRect(path[p].x-2, path[p].y-2, path[p].x+2, path[p].y+2), 00FFH, WMGraphics.ModeCopy);
				g.Fill(WMRectangles.MakeRect(path[p].x-1, path[p].y-1, path[p].x+1, path[p].y+1), цел32(0FFFF00FFH), WMGraphics.ModeCopy);
			всё;
			если nofLines >= длинаМассива(poly) то возврат всё;
			если nofLines = 0 то	poly[0] := path[i]; увел(nofLines) всё;
			poly[nofLines] := path[p]; увел(nofLines);
			i := p;
			увел(straight);
		иначе увел(i);
			увел(nonStraight)
		всё;
		p := i + 3;
	кц;
	(* IF nonStraight - straight > 30 THEN RETURN END; *)
	(* not general : assumes closed polygon *)
	если nofLines > 0 то
		poly[nofLines-1] := poly[0];
	всё;
	SimplifyPoly(poly, nofLines, 0, nofLines);
	если DebugLiner то
		нцДля i := 0 до nofLines - 1 делай
			g.Fill(WMRectangles.MakeRect(poly[i].x-2, poly[i].y-2, poly[i].x+2, poly[i].y+2), 00FFH, WMGraphics.ModeCopy);
			g.Fill(WMRectangles.MakeRect(poly[i].x-1, poly[i].y-1, poly[i].x+1, poly[i].y+1), цел32(0FF0000FFH), WMGraphics.ModeCopy);
		кц
	 всё;
	 nofPoints := nofLines;
кон ExtractLines;

проц GetTimer():цел64;
перем t : цел64;
	res : Kernel32.BOOL;
нач
	res := Kernel32.QueryPerformanceCounter(НИЗКОУР.подмениТипЗначения(Kernel32.LargeInteger, t));
	возврат t;
кон GetTimer;

проц GetFreq():цел64;
перем t : цел64;
	res : Kernel32.BOOL;
нач
	res := Kernel32.QueryPerformanceFrequency(НИЗКОУР.подмениТипЗначения(Kernel32.LargeInteger, t));
	возврат t;
кон GetFreq;

проц Label2(buffer : Raster.Image);
перем nof, length, i, j : цел32; w, h: размерМЗ; res: целМЗ;
	path : массив 1024*4 из Point;
	poly : массив 40 из Point;
	nofPoints : цел32;
	t0, t1, labeltime,  tracetime, linetime : цел64;
	f  : вещ64;
	gp : массив 50 из WMGraphics.Point2d;
	проц Sqr(x: вещ64):вещ64;
	нач
		возврат x * x
	кон Sqr;
нач
	w := buffer.width; h := buffer.height;
	если (labelBuffer = НУЛЬ) или (длинаМассива(labelBuffer^) < w*h) то нов(labelBuffer, w*h) всё;
	t0 := GetTimer();
	BinaryLabler(buffer, labelBuffer^, equivalence, threshold, pixThreshold, 32767, labelInfo, истина, nof, res);
	t1 := GetTimer();
	labeltime := t1 - t0;
	tracetime := 0; linetime := 0;
	если g = НУЛЬ то нов(g, buffer) всё;
	если res = 0 то
		нцДля i := 0 до nof - 1 делай
			t0 := GetTimer();
			Trace(buffer, labelBuffer^, labelInfo[i], length, path, res);
			t1 := GetTimer(); tracetime := tracetime + (t1 - t0);
			если res = 0 то
				t0 := GetTimer();
				ExtractLines(buffer, path, length, poly, nofPoints);
				если (nofPoints = 5) то
					нцДля j := 0 до nofPoints - 1 делай gp[j].x := poly[j].x; gp[j].y := poly[j].y кц;
					если g = НУЛЬ то нов(g, buffer) всё;
					если (Math.sqrt(Sqr(poly[1].x - poly[0].x) + Sqr(poly[1].y - poly[0].y)) > 20) и
						(Math.sqrt(Sqr(poly[2].x - poly[1].x) + Sqr(poly[2].y - poly[1].y)) > 20) и
						(Math.sqrt(Sqr(poly[3].x - poly[2].x) + Sqr(poly[3].y - poly[2].y)) > 20) и
						(Math.sqrt(Sqr(poly[4].x - poly[3].x) + Sqr(poly[4].y - poly[3].y)) > 20) и
						(Math.sqrt(Sqr(poly[2].x - poly[0].x) + Sqr(poly[2].y - poly[0].y)) > 40) и
						(Math.sqrt(Sqr(poly[1].x - poly[3].x) + Sqr(poly[1].y - poly[3].y)) > 40) и
						(Math.sqrt(
							Sqr((poly[0].x + poly[1].x) / 2 - (poly[2].x + poly[3].x) / 2) +
							Sqr((poly[0].y + poly[1].y) / 2 - (poly[2].y + poly[3].y) / 2)) > 40) и
						(Math.sqrt(
							Sqr((poly[1].x + poly[2].x) / 2 - (poly[3].x + poly[4].x) / 2) +
							Sqr((poly[1].y + poly[2].y) / 2 - (poly[3].y + poly[4].y) / 2)) > 40) то
						Transform(buffer, rectified.img, poly);
						CheckFields(rectified.img);
						rectified.Invalidate(WMRectangles.MakeRect(0, 0, rectified.GetWidth(), rectified.GetHeight()));
						g.Line((poly[0].x + poly[1].x) DIV 2, (poly[0].y + poly[1].y) DIV 2 , (poly[2].x + poly[3].x) DIV 2, (poly[2].y + poly[3].y) DIV 2, цел32(0FF0000FFH), WMGraphics.ModeSrcOverDst);

						g.FillPolygonFlat(gp, nofPoints, 000FF0080H, WMGraphics.ModeSrcOverDst);
						g.Line(poly[0].x, poly[0].y, poly[2].x, poly[2].y, 000FFFFFFH, WMGraphics.ModeSrcOverDst);
						g.Line(poly[1].x, poly[1].y, poly[3].x, poly[3].y, 000FFFFFFH, WMGraphics.ModeSrcOverDst);
					всё;

(*					g.FillPolygonFlat(gp, nofPoints, SIGNED32(0FF00FF80H), WMGraphics.ModeSrcOverDst)	*)
				аесли nofPoints = 6 то
					g.FillPolygonFlat(gp, nofPoints, цел32(0FF000020H), WMGraphics.ModeSrcOverDst);
				всё;
				t1 := GetTimer(); linetime := linetime + (t1 - t0);
			всё
		кц
	всё;
	f := GetFreq();
	f := f / 1000;
(*	KernelLog.String("nof= "); KernelLog.Int(nof, 0); KernelLog.Ln;
	KernelLog.String("labeltime = "); KernelLog.Int(ENTIER(labeltime / f), 0); KernelLog.Ln;
	KernelLog.String("tracetime = "); KernelLog.Int(ENTIER(tracetime / f), 0); KernelLog.Ln;
	KernelLog.String("linetime = "); KernelLog.Int(ENTIER(linetime / f), 0); KernelLog.Ln;
*)
кон Label2;

проц YUVFilter(buffer : Raster.Image);
перем x, y, w , h : размерМЗ;
	tr, tg, tb, ta, cy, cu, cv : цел32;
	mode : Raster.Mode;
	pix : Raster.Pixel;
нач
	Raster.InitMode(mode, Raster.srcCopy);
	w := buffer.width; h := buffer.height;
	(* rgb to yuv *)
	нцДля y := 0 до h - 1 делай
		нцДля x := 0 до w - 1 делай
			Raster.Get(buffer, x, y, pix, mode); Raster.GetRGBA(pix, tr, tg, tb, ta);
			RGBToYUVReal(tr, tg, tb, cy, cu, cv);
			Raster.SetRGBA(pix, cy, cu, cv, 255);
			Raster.Put(buffer, x, y, pix, mode);

		кц
	кц;
кон YUVFilter;

проц SetYUVFilter*;
нач
	VideoExample.InstallFrameHandler(YUVFilter)
кон SetYUVFilter;

проц BWFilter(buffer : Raster.Image);
перем x, y, w , h : размерМЗ;
	tr, tg, tb, ta, cy, cu, cv : цел32;
	mode : Raster.Mode;
	pix : Raster.Pixel;
нач
	Raster.InitMode(mode, Raster.srcCopy);
	w := buffer.width; h := buffer.height;
	нцДля y := 0 до h - 1 делай
		нцДля x := 0 до w - 1 делай
			Raster.Get(buffer, x, y, pix, mode); Raster.GetRGBA(pix, tr, tg, tb, ta);
			RGBToYUVReal(tr, tg, tb, cy, cu, cv);
			Raster.SetRGBA(pix, cy, cy, cy, 255);
			Raster.Put(buffer, x, y, pix, mode);

		кц
	кц;
кон BWFilter;

проц SetBWFilter*;
нач
	VideoExample.InstallFrameHandler(BWFilter)
кон SetBWFilter;

проц RedDotFilter(buffer : Raster.Image);
перем x, y, w , h : размерМЗ;
	tr, tg, tb, ta : цел32;
	mode : Raster.Mode;
	pix : Raster.Pixel;
нач
	Raster.InitMode(mode, Raster.srcCopy);
	w := buffer.width; h := buffer.height;
	нцДля y := 0 до h - 1 делай
		нцДля x := 0 до w - 1 делай
			Raster.Get(buffer, x, y, pix, mode); Raster.GetRGBA(pix, tr, tg, tb, ta);
			если (tr > 50) и (tg < 20) и (tb < 20) то
				Raster.SetRGBA(pix, 255, 255, 0, 255);
				Raster.Put(buffer, x, y, pix, mode);
			всё
		кц
	кц;
кон RedDotFilter;

проц SetRedDotFilter*;
нач
	VideoExample.InstallFrameHandler(RedDotFilter)
кон SetRedDotFilter;

проц ThresholdFilter(buffer : Raster.Image);
перем x, y, w , h : размерМЗ;
	sum, lastsum, tr, tg, tb, ta : цел32;
	mode : Raster.Mode;
	pix : Raster.Pixel;
	tresh : цел32;
	darkMode : булево;
нач
	Raster.InitMode(mode, Raster.srcCopy);
	w := buffer.width; h := buffer.height;
	нцДля y := 0 до h - 1 делай
		Raster.Get(buffer, 0, y, pix, mode); Raster.GetRGBA(pix, tr, tg, tb, ta); sum := tr + tg + tb;
		lastsum := sum;
		darkMode := sum < threshold;
		нцДля x := 1 до w - 1 делай
			Raster.Get(buffer, x, y, pix, mode); Raster.GetRGBA(pix, tr, tg, tb, ta); sum := tr + tg + tb;
			если darkMode то
				если (sum < tresh) то
					Raster.SetRGBA(pix, 255, 0, 0, 255);
					Raster.Put(buffer, x, y, pix, mode);
				иначе darkMode := ложь; tresh := threshold
				всё;
			иначе
				если (sum < 3*lastsum DIV 4) или (sum < tresh) то (*(sum > threshold) *)
					если sum > tresh то tresh := 2*lastsum DIV 4 всё;
					Raster.SetRGBA(pix, 255, 0, 0, 255);
					Raster.Put(buffer, x, y, pix, mode);
					darkMode := истина
				всё
			всё;
			lastsum := sum
		кц
	кц;
кон ThresholdFilter;

проц SetThresholdFilter*(context : Commands.Context);
перем
	options: Options.Options;
нач
	нов(options);
	options.Add("t","threshold",Options.Integer);
	threshold := 50;
	если options.Parse(context.arg, context.error) то
		если options.GetInteger("threshold", threshold) то всё;
	всё;
	VideoExample.InstallFrameHandler(ThresholdFilter)
кон SetThresholdFilter;

проц AdaptiveThresholdFilter(buffer : Raster.Image);
перем x, y, w, h, p, t : размерМЗ;
	sum : цел32;
	mode : Raster.Mode;
	pix : Raster.Pixel;
	ch : симв8;
	adr : адресВПамяти;
	total : цел32;
нач
	Raster.InitMode(mode, Raster.srcCopy);
	w := buffer.width; h := buffer.height;
	если (intensityBuffer = НУЛЬ) или (длинаМассива(intensityBuffer^) < w*h) то нов(intensityBuffer, w*h) всё;

	p := 0; total := 0;
	нцДля y := 0 до h - 1 делай
		adr := buffer.adr + y *  buffer.bpr;
		нцДля x := 0 до w - 1 делай
			увел(adr);
			НИЗКОУР.прочтиОбъектПоАдресу(adr, ch); sum :=  кодСимв8(ch); увел(adr);
			НИЗКОУР.прочтиОбъектПоАдресу(adr, ch); sum := sum + кодСимв8(ch); увел(adr);
			intensityBuffer[p] := симв8ИзКода(sum DIV 2);
			total := total + (sum DIV 2);
			увел(p)
		кц;
	кц;
	t := 5* (total DIV (w * h)) DIV 8;

	p := 0;
	нцДля y := 0 до h - 1 делай
		нцДля x := 0 до w - 1 делай
			если  кодСимв8(intensityBuffer[p]) < t то
				Raster.SetRGBA(pix, 255, 0, 0, 255)
			иначе
				Raster.SetRGBA(pix, 0, 0, 0, 255)
			всё;
			Raster.Put(buffer, x, y, pix, mode);
			увел(p);
		кц;
	кц;
кон AdaptiveThresholdFilter;

проц SetAdaptiveThresholdFilter*(context : Commands.Context);
нач
	VideoExample.InstallFrameHandler(AdaptiveThresholdFilter)
кон SetAdaptiveThresholdFilter;

проц FineAdaptiveThresholdFilter(buffer : Raster.Image);
конст WindowX = 32;
перем x, y, w, h, p, t : размерМЗ;
	sum : цел32;
	mode : Raster.Mode;
	pix : Raster.Pixel;
	ch : симв8;
	adr : адресВПамяти;
	total : цел32;

нач
	Raster.InitMode(mode, Raster.srcCopy);
	w := buffer.width; h := buffer.height;
	если (intensityBuffer = НУЛЬ) или (длинаМассива(intensityBuffer^) < w*h) то нов(intensityBuffer, w*h) всё;
	если (thresholdBuffer = НУЛЬ) или (длинаМассива(thresholdBuffer^) < w*h) то нов(thresholdBuffer, w*h) всё;

	(* create intensity array *)
	p := 0; total := 0;
	нцДля y := 0 до h - 1 делай
		adr := buffer.adr + y *  buffer.bpr;
		нцДля x := 0 до w - 1 делай
			увел(adr);
			НИЗКОУР.прочтиОбъектПоАдресу(adr, ch); sum :=  кодСимв8(ch); увел(adr);
			НИЗКОУР.прочтиОбъектПоАдресу(adr, ch); sum := sum + кодСимв8(ch); увел(adr);
			intensityBuffer[p] := симв8ИзКода(sum DIV 2);
			total := total + (sum DIV 2);
			увел(p)
		кц;
	кц;

	p := 0;
	нцДля y := 0 до h - 1 делай
		total := 0;
		нцДля x := 0 до WindowX - 1 делай увел(total, кодСимв8(intensityBuffer[p])); увел(p) кц;
		t := y * w;
		нцДля x := 0 до WindowX DIV 2 - 1 делай thresholdBuffer[t] := симв8ИзКода(total DIV WindowX); увел(t) кц;
		нцДля x := WindowX DIV 2 до w - WindowX DIV 2 - 1 делай
			total := total - кодСимв8(intensityBuffer[p- WindowX]) + кодСимв8(intensityBuffer[p]);
			thresholdBuffer[t] := симв8ИзКода(total DIV WindowX ); увел(t);
			увел(p)
		кц;
		нцДля x := w - WindowX DIV 2 до w - 1 делай thresholdBuffer[t] := симв8ИзКода(total DIV WindowX); увел(t) кц;
	кц;

	p := 0;
	нцДля y := 0 до (h-1) DIV 4 - 1 делай
		нцДля x := 0 до w - 1 делай
	(*		total := 	ORD(thresholdBuffer[p]);
			total := 	total + ORD(thresholdBuffer[p + w]);
			total := 	total + ORD(thresholdBuffer[p + 2 * w]);
			total := 	total + ORD(thresholdBuffer[p + 3 * w]);
			total := 7*total DIV (4*8); *)
			total := 	кодСимв8(thresholdBuffer[p]);
			total := 	total + кодСимв8(thresholdBuffer[p + 1* w]);
			total := 	total + кодСимв8(thresholdBuffer[p + 2 * w]);
			total := 	total + кодСимв8(thresholdBuffer[p + 3 * w]);
			total := 	total + кодСимв8(thresholdBuffer[p + 4 * w]);
			total := 	total + кодСимв8(thresholdBuffer[p + 5 * w]);
			total := 	total + кодСимв8(thresholdBuffer[p + 6 * w]);
			total := 	total + кодСимв8(thresholdBuffer[p + 7 * w]);
			total := 14*total DIV (8*16);
			thresholdBuffer[p] := симв8ИзКода(total); thresholdBuffer[p + w] := симв8ИзКода(total); thresholdBuffer[p + 2 * w] := симв8ИзКода(total); thresholdBuffer[p + 3 * w] := симв8ИзКода(total);
			увел(p)
		кц;
		увел(p, 3 * w);
	кц;

	p := 0;
	нцДля y := 0 до h - 1 делай
		нцДля x := 0 до w - 1 делай
			если  кодСимв8(intensityBuffer[p]) < кодСимв8(thresholdBuffer[p]) то
				Raster.SetRGBA(pix, 255, 0, 0, 255)
			иначе
				Raster.SetRGBA(pix, 0, 0, 0, 255)
			всё;
			Raster.Put(buffer, x, y, pix, mode);
			увел(p);
		кц;
	кц;
кон FineAdaptiveThresholdFilter;

проц SetFineAdaptiveThresholdFilter*(context : Commands.Context);
нач
	VideoExample.InstallFrameHandler(FineAdaptiveThresholdFilter)
кон SetFineAdaptiveThresholdFilter;

проц SetLabelFilter*(context : Commands.Context);
перем
	options: Options.Options;
нач
	нов(options);
	options.Add("t","threshold", Options.Integer);
	options.Add("p","pixelThreshold", Options.Integer);
	threshold := 50;
	pixThreshold := 50;
	если options.Parse(context.arg, context.error) то
		если options.GetInteger("threshold", threshold) то всё;
		если options.GetInteger("pixelThreshold", pixThreshold) то всё;
	всё;
	VideoExample.InstallFrameHandler(Label2)
кон SetLabelFilter;

проц Uninstall*;
нач
	VideoExample.InstallFrameHandler(НУЛЬ)
кон Uninstall;

проц Init;
перем i : цел32;
	gen : Random.Generator;
нач
	нов(gen);
	нцДля i := 1 до длинаМассива(labelColor) - 1 делай labelColor[i] := gen.Integer();  кц;
	labelColor[0] := цел32(0FFFFFFFFH);

	dirX[DirN] := 0; dirY[DirN] := -1; (* N *)
	dirX[DirNE] := 1; dirY[DirNE] := -1; (* NE *)
	dirX[DirE] := 1; dirY[DirE] := 0; (* E *)
	dirX[DirSE] := 1; dirY[DirSE] := 1; (* SE *)
	dirX[DirS] := 0; dirY[DirS] := 1; (* S *)
	dirX[DirSW] := -1; dirY[DirSW] := 1; (* SW *)
	dirX[DirW] := -1; dirY[DirW] := 0; (* W *)
	dirX[DirNW] := -1; dirY[DirNW] := -1; (* NW *)
кон Init;


проц Cleanup;
перем timer : Kernel.Timer;
нач
	VideoExample.InstallFrameHandler(НУЛЬ);
	(* hack to not remove the module while a frame is still being filtered *)
	нов(timer);
	timer.Sleep(1000);
кон Cleanup;

нач
	нов(rectified, 256, 256);
	Init;
	SetYUVFilter();
	Modules.InstallTermHandler(Cleanup)
кон TestVideo.

System.Free TestVideo ~
TestVideo.SetLabelFilter -t=250 ~
TestVideo.SetThresholdFilter -t=300 ~

TestVideo.SetRedDotFilter ~
TestVideo.SetYUVFilter ~
TestVideo.SetBWFilter ~
TestVideo.Uninstall ~


VideoExample.Start ~
VideoExample.Stop ~

System.Free TestVideo ~
TestVideo.SetLabelFilter -t=360 p=20 ~
VideoExample.SimulateImage "sample0.jpg" ~
