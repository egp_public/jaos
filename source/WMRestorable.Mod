модуль WMRestorable; (** AUTHOR "tf"; PURPOSE "Save and restore the desktop"; *)

использует
	Commands, Files, Kernel, Modules, ЛогЯдра, Strings, WMMessages,
	XML, XMLObjects, XMLScanner, XMLParser, WM := WMWindowManager, Потоки;

тип

	String* = Strings.String;
	XmlElement* = XML.Element;

	Context* = окласс
	перем
		l*, t*, r*, b* : размерМЗ;
		flags* : мнвоНаБитахМЗ;
		appData* : XML.Element;
	кон Context;

	RestoreContextProc = проц(context : Context);

тип

	Storage* = окласс
	перем data : XML.Element;

		проц &New*;
		нач
			нов(data); data.SetName("Desktop");
		кон New;

		проц Add*(конст name, loader : массив из симв8; w : WM.Window; appData : XML.Element);
		перем window: XML.Element;
		нач {единолично}
			нов(window); window.SetName("Window");

			StoreString(window, "name", name);
			StoreString(window, "loader", loader);

			StoreSize(window, "l", w.bounds.l);
			StoreSize(window, "t", w.bounds.t);
			StoreSize(window, "r", w.bounds.r);
			StoreSize(window, "b", w.bounds.b);

			StoreSet(window, "flags", w.flags);

			если appData # НУЛЬ то window.AddContent(appData) всё;

			data.AddContent(window)
		кон Add;

		проц Write*(конст name : массив из симв8);
		перем f : Files.File; w : Files.Writer;
		нач {единолично}
			f := Files.New(name);
			Files.OpenWriter(w, f, 0);
			data.Write(w, НУЛЬ, 0);
			w.ПротолкниБуферВПоток;
			Files.Register(f)
		кон Write;

	кон Storage;

тип

	Loader = окласс
	перем
		restoreContextProc : RestoreContextProc;
		par : Context;

		проц &New*(c : RestoreContextProc; par : Context);
		нач
			сам.restoreContextProc := c; сам.par := par
		кон New;

	нач {активное}
		restoreContextProc(par);
	кон Loader;

перем hasErrors : булево;

проц Store*(c : Commands.Context); (** filename ~ *)
перем
	context : Storage;
	m : WM.WindowManager;
	msg : WMMessages.Message;
	t : Kernel.Timer;
	filename : массив 256 из симв8;
нач
	c.arg.ПропустиБелоеПоле; c.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename);
	нов(context);
	msg.ext := context; msg.msgType := WMMessages.MsgExt;
	m := WM.GetDefaultManager();
	m.Broadcast(msg);
	c.out.пСтроку8("WMRestorable: Saving desktop to "); c.out.пСтроку8(filename); c.out.пСтроку8("..."); c.out.пВК_ПС;
	нов(t); t.Sleep(100);
	context.Write(filename);
	нов(t); t.Sleep(500);
	context.Write(filename);
	нов(t); t.Sleep(1500);
	context.Write(filename);
	c.out.пСтроку8("WMRestorable: Desktop saved."); c.out.пВК_ПС;
кон Store;

проц AddByContext*(w : WM.Window; c : Context);
перем manager : WM.WindowManager;
нач
	manager := WM.GetDefaultManager();
	w.bounds.l := c.l;
	w.bounds.t := c.t;
	w.bounds.r := c.r;
	w.bounds.b := c.b;
	manager.Add(c.l, c.t, w, c.flags);
кон AddByContext;

(* Report errors while parsing *)
проц Error(pos, line, row : Потоки.ТипМестоВПотоке; конст msg : массив из симв8);
нач
	ЛогЯдра.пСтроку8("Parse error at pos "); ЛогЯдра.пЦел64(pos, 5); ЛогЯдра.пСтроку8(" in line "); ЛогЯдра.пЦел64(line,5);
	ЛогЯдра.пСтроку8(" row "); ЛогЯдра.пЦел64(row, 5); ЛогЯдра.пСтроку8(" - "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС;
	hasErrors := истина
кон Error;

проц RestoreWindow(w : XML.Element);
перем
	l : Strings.String;
	proc : RestoreContextProc;
	moduleName, procedureName : Modules.Name;
	msg : массив 128 из симв8;
	res : целМЗ;
	loader : Loader;
	context : Context;
	c : XML.Content;
нач
	l := w.GetAttributeValue("loader");
	если l # НУЛЬ то
		нов(context);
		LoadSize(w, "l", context.l);
		LoadSize(w, "t", context.t);
		LoadSize(w, "r", context.r);
		LoadSize(w, "b", context.b);
		LoadSet(w, "flags", context.flags);
		c := w.GetFirst();
		если (c # НУЛЬ) и (c суть XML.Element) то
			context.appData := c(XML.Element);
		всё;
		Commands.Split(l^, moduleName, procedureName, res, msg);
		если (res = Commands.Ok) то
			дайПроцПоИмени(moduleName, procedureName, proc);
			если (proc # НУЛЬ) то
				нов(loader, proc, context);
			всё;
		всё;
	всё
кон RestoreWindow;

проц StoreWindow*(window: WM.Window; конст fileName: массив из симв8);
перем context: Storage; msg: WMMessages.Message;
нач
	нов(context);
	msg.ext := context; msg.msgType := WMMessages.MsgExt;
	window.Handle(msg);
	context.Write(fileName);
кон StoreWindow;

проц Load*(context : Commands.Context); (** filename ~ *)
перем f : Files.File;
	scanner : XMLScanner.Scanner;
	parser : XMLParser.Parser;
	reader : Files.Reader;
	doc : XML.Document;
	root : XML.Element;
	s : Strings.String;
	p : XML.Content;
	fn : массив 256 из симв8;
нач {единолично}
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fn);
	context.out.пСтроку8("WMRestorable: Loading desktop from "); context.out.пСтроку8(fn); context.out.пСтроку8("..."); context.out.пВК_ПС;
	hasErrors := ложь;
	f := Files.Old(fn);
	если f # НУЛЬ то
		нов(reader, f, 0);
		нов(scanner, reader); scanner.reportError := Error;
		нов(parser, scanner); parser.reportError := Error;
		doc := parser.Parse();
		если hasErrors то возврат всё;
		root := doc.GetRoot();
		p := root.GetFirst();
		нцПока (p # НУЛЬ) делай
			если p суть XML.Element то
				s := p(XML.Element).GetName();
				если (s # НУЛЬ) и (s^ = "Window") то
					RestoreWindow(p(XML.Element))
				всё
			всё;
			p := root.GetNext(p);
		кц
	иначе
		context.error.пСтроку8("WMRestorable: File "); context.error.пСтроку8(fn); context.error.пСтроку8(" not found."); context.error.пВК_ПС;
	всё;
кон Load;

(** Returns a XML element contained in Context.appData by its path
	e.g. GetElement(c, "Configuration\DisplaySettings") *)
проц GetElement*(c : Context; конст path : массив из симв8) : XmlElement;
конст
	PathDelimiter = "\";
перем
	strings : Strings.StringArray;
	string : String;
	elem : XML.Element;
	idx : цел32;
нач
	если (c # НУЛЬ) и (c.appData # НУЛЬ) то
		elem := c.appData (XML.Element);
		strings := Strings.Split(path, PathDelimiter);
		string := elem.GetName();
		если (string # НУЛЬ) или (string^ = strings[0]^) то
			idx := 1;
			нц
				если (idx >= длинаМассива(strings)) или (elem = НУЛЬ) то прервиЦикл; всё;
				elem := GetElementByName(elem, strings[idx]^);
				увел(idx);
			кц;
		всё;
	всё;
	если elem = НУЛЬ то
		ЛогЯдра.пСтроку8("WMRestorable: Element "); ЛогЯдра.пСтроку8(path); ЛогЯдра.пСтроку8(" not found."); ЛогЯдра.пВК_ПС;
	всё;
	возврат elem;
кон GetElement;

проц LoadBoolean*(elem : XML.Element; конст name : массив из симв8; перем value : булево);
перем string : String;
нач
	string := elem.GetAttributeValue(name);
	если (string # НУЛЬ) то
		если (string^ = "true") то value := истина; иначе value := ложь; всё;
	иначе
		ShowLoadError("LoadBoolean", elem, name);
	всё;
кон LoadBoolean;

проц StoreBoolean*(elem : XML.Element; конст name : массив из симв8; value : булево);
перем a : XML.Attribute; string : массив 8 из симв8;
нач
	нов(a); a.SetName(name);
	если value то string := "true"; иначе string := "false"; всё; a.SetValue(string);
	elem.AddAttribute(a);
кон StoreBoolean;

проц LoadLongint*(elem : XML.Element; конст name : массив из симв8; перем value : цел32);
перем string : String;
нач
	string := elem.GetAttributeValue(name);
	если string # НУЛЬ то
		Strings.StrToInt(string^, value);
	иначе
		ShowLoadError("LoadLongint", elem, name);
	всё;
кон LoadLongint;

проц LoadSize*(elem : XML.Element; конст name : массив из симв8; перем value : размерМЗ);
перем string : String;
нач
	string := elem.GetAttributeValue(name);
	если string # НУЛЬ то
		Strings.StrToSize(string^, value);
	иначе
		ShowLoadError("LoadLongint", elem, name);
	всё;
кон LoadSize;

проц StoreLongint*(elem : XML.Element; конст name : массив из симв8; value : цел32);
перем a : XML.Attribute; string : массив 32 из симв8;
нач
	нов(a); a.SetName(name); Strings.IntToStr(value, string); a.SetValue(string);
	elem.AddAttribute(a);
кон StoreLongint;

проц StoreSize*(elem : XML.Element; конст name : массив из симв8; value : размерМЗ);
перем a : XML.Attribute; string : массив 32 из симв8;
нач
	нов(a); a.SetName(name); Strings.IntToStr(value, string); a.SetValue(string);
	elem.AddAttribute(a);
кон StoreSize;

проц LoadString*(elem : XML.Element; конст name : массив из симв8; перем value : массив из симв8);
перем string : String;
нач
	string := elem.GetAttributeValue(name);
	если string # НУЛЬ то
		копируйСтрокуДо0(string^, value);
	иначе
		value[0] := 0X; ShowLoadError("LoadString", elem, name);
	всё;
кон LoadString;

проц StoreString*(elem : XML.Element; конст name, value : массив из симв8);
перем a : XML.Attribute;
нач
	нов(a); a.SetName(name); a.SetValue(value); elem.AddAttribute(a);
кон StoreString;

проц LoadStringPtr*(elem : XML.Element; конст name : массив из симв8; перем value : String);
нач
	value := elem.GetAttributeValue(name);
	если value = НУЛЬ то
		ShowLoadError("LoadStringPtr", elem, name);
	всё;
кон LoadStringPtr;

проц StoreStringPtr*(elem : XML.Element; конст name : массив из симв8;  value : String);
перем a : XML.Attribute;
нач
	если (value # НУЛЬ) то
		нов(a); a.SetName(name); a.SetValue(value^); elem.AddAttribute(a);
	всё;
кон StoreStringPtr;

проц LoadSet*(elem : XML.Element; конст name : массив из симв8; перем value : мнвоНаБитахМЗ);
перем string : String;
нач
	value := {};
	string := elem.GetAttributeValue(name);
	если (string # НУЛЬ) то
		Strings.StrToSet(string^, value);
	всё;
кон LoadSet;

проц StoreSet*(elem : XML.Element; конст name : массив из симв8; value : мнвоНаБитахМЗ);
перем a : XML.Attribute; string : массив 128 из симв8;
нач
	нов(a); a.SetName(name); Strings.SetToStr(value, string); a.SetValue(string);
	elem.AddAttribute(a);
кон StoreSet;

проц GetElementByName(parent : XML.Element; конст name : массив из симв8) : XML.Element;
перем elem : XML.Element; enum : XMLObjects.Enumerator; ptr : динамическиТипизированныйУкль; string : String;
нач
	если parent # НУЛЬ то
		enum := parent.GetContents(); enum.Reset();
		нцПока enum.HasMoreElements() делай
			ptr := enum.GetNext();
			если ptr суть XML.Element то
				elem := ptr (XML.Element);
				string := elem.GetName();
				если (string # НУЛЬ) и (string^ = name) то
					возврат elem;
				всё;
			всё;
		кц;
	всё;
	возврат НУЛЬ;
кон GetElementByName;

проц ShowLoadError(конст procedureName : массив из симв8; elem : XML.Element; конст name : массив из симв8);
перем string : String;
нач
	ЛогЯдра.пСтроку8("WMRestorable: "); ЛогЯдра.пСтроку8(procedureName);
	ЛогЯдра.пСтроку8(": Attribute '"); ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8("' of element ");
	string := elem.GetName(); если string # НУЛЬ то ЛогЯдра.пСтроку8(string^); иначе ЛогЯдра.пСтроку8("<no name>"); всё;
	ЛогЯдра.пСтроку8(" not found.");
	ЛогЯдра.пВК_ПС;
кон ShowLoadError;

кон WMRestorable.

System.Free WMRestorable ~
WMRestorable.Store ~
WMRestorable.Load ~

PET.Open Auto.dsk ~	(* used to verify the behaviour of WMRestorable *)
