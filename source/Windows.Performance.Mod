модуль Performance;

	перем
		load*: массив 3 из вещ32;	(* load estimates *)
		idle*: массив 1 из цел32;	(* idle percentage estimates *)
нач
	idle[0] := -1;
кон Performance.

(*
Notes:
o "load" is a Unix-like estimate of the average number of ready and running processes over the past 1, 5 and 15 minutes.
o "idle" is an estimate of the percentage of idle time per processor over the last 10 seconds.
o When a processor is not available, its idle estimate is -1.
*)
