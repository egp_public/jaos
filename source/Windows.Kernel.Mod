
(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)
(* red marked parts are specific for WinAos, fof *)

модуль Kernel;   (** AUTHOR "pjm, ejz, fof, ug"; PURPOSE "Implementation-independent kernel interface"; *)

использует НИЗКОУР, Kernel32, ЭВМ, Heaps, (* Modules, *) Objects;

конст

	TimerFree = 0;  TimerSleeping = 1;  TimerWoken = 2;  TimerExpired = 3;   (* Timer state *)

	Second* = ЭВМ.ЧастотаКвантовВремени˛Гц;

тип
	(** Finalizer for FinalizedCollection.Add. *)
	Finalizer* = Heaps.Finalizer;   (** PROCEDURE (obj: ANY) *)

	(** Enumerator for FinalizedCollection.Enumerate. *)
	Enumerator* = проц {делегат} ( obj: динамическиТипизированныйУкль;  перем cont: булево );

	FinalizerNode = укль на запись (Heaps.FinalizerNode)
		nextObj {неОтслСборщиком}: FinalizerNode;	(* in collection *)
	кон;

	(** Polling timer. *)
	MilliTimer* = запись
		start, target: цел32
	кон;

тип
	(** Delay timer *)
	Timer* = окласс
	перем
		timer: Objects.Timer;
		state-: цел8;
		nofHandleTimeout-, nofHandleTimeout2- : цел32;
		nofSleeps-, nofSleepsLeft- : цел32;
		nofAwaits-, nofAwaitsLeft- : цел32;

		проц HandleTimeout;
		нач {единолично}
			увел(nofHandleTimeout);
			если state # TimerFree то увел(nofHandleTimeout2); state := TimerExpired всё
		кон HandleTimeout;

		(** Delay the calling process the specified number of milliseconds or until Wakeup is called. Only one process may sleep on a specific timer at a time. *)
		проц Sleep*(ms: цел32);
		нач {единолично}
			увел(nofSleeps);
			утв(state = TimerFree);	(* only one process may sleep on a timer *)
			state := TimerSleeping;
			Objects.SetTimeout(timer, HandleTimeout, ms);
			увел(nofAwaits);
			дождись(state # TimerSleeping);
			увел(nofAwaitsLeft);
			если state # TimerExpired то Objects.CancelTimeout(timer) всё;
			state := TimerFree;
			увел(nofSleepsLeft);
		кон Sleep;

		(** Wake up the process sleeping on the timer, if any. *)
		проц Wakeup*;
		нач {единолично}
			если state = TimerSleeping то state := TimerWoken всё
		кон Wakeup;

		(** Initializer. *)
		проц &Init*;
		нач
			state := TimerFree; нов(timer);
			nofHandleTimeout := 0; nofHandleTimeout2 := 0;
			nofSleeps := 0; nofSleepsLeft := 0;
			nofAwaits := 0; nofAwaitsLeft := 0;
		кон Init;

	кон Timer;

тип
	(** A collection of objects that are finalized automatically by the garbage collector. *)
	FinalizedCollection* = окласс (Heaps.FinalizedCollection)
		перем root: FinalizerNode;	(* weak list of contents linked by nextObj *)

		(** Add obj to collection. Parameter fin specifies finalizer, or NIL if not required. *)	(* may be called multiple times *)
		проц Add*(obj: динамическиТипизированныйУкль; fin: Finalizer);
		перем n: FinalizerNode;
		нач
			нов(n); n.collection := сам; n.finalizer := fin;
			Heaps.AddFinalizer(obj, n);
			нач {единолично}
				n.nextObj := root.nextObj; root.nextObj := n	(* add to collection *)
			кон
		кон Add;

		(** Remove one occurrence of obj from collection. *)
		проц Remove*(obj: динамическиТипизированныйУкль);
		перем p, n: FinalizerNode;
		нач {единолично}
			p := root; n := p.nextObj;
			нцПока (n # НУЛЬ) и (n.objWeak # obj) делай
				p := n; n := n.nextObj
			кц;
			если n # НУЛЬ то p.nextObj := n.nextObj всё;
			(* leave in global finalizer list *)
		кон Remove;

		(** Overriden method: Remove all occurrences of obj from collection. *)
		проц {перекрыта}RemoveAll*(obj: динамическиТипизированныйУкль);
		перем p, n: FinalizerNode;
		нач {единолично}
			p := root; n := p.nextObj;
			нцПока n # НУЛЬ делай
				если n.objWeak = obj то
					p.nextObj := n.nextObj;
				иначе
					p := n;
				всё;
				n := n.nextObj
			кц
		кон RemoveAll;

		(** Enumerate all objects in the collection (Enumerator may not call Remove, Add, Enumerate or Clear). *)
		проц Enumerate*(enum: Enumerator);
		перем fn, next: FinalizerNode; cont: булево;
		нач {единолично}
			fn := root.nextObj; cont := истина;
			нцПока fn # НУЛЬ делай
				next := fn.nextObj;	(* current (or other) object may be removed by enum call *)
				enum(fn.objWeak, cont);
				если cont то fn := next иначе fn := НУЛЬ всё
			кц
		кон Enumerate;

		(** Enumerate all objects in the collection not being finalized (Enumerator may not call Remove, Add, Enumerate or Clear). *)
		проц EnumerateN*( enum: Enumerator );
		перем fn, next: FinalizerNode; cont: булево; obj: динамическиТипизированныйУкль;
		нач {единолично}
			fn := root.nextObj; cont := истина;
			нцПока fn # НУЛЬ делай
				next := fn.nextObj;	(* current (or other) object may be removed by enum call *)
				obj := НУЛЬ;

				ЭВМ.ЗапросиБлокировку(ЭВМ.ЯдернаяБлокировкаДляСборщикаМусора);	(* prevent GC from running *)

				если (fn.objWeak # НУЛЬ ) и (fn.objStrong = НУЛЬ ) то (* object is not yet on the finalizers list *)
					obj := fn.objWeak; (* now object is locally referenced, will therefore not be GCed *)
				всё;

				ЭВМ.ОтпустиБлокировку(ЭВМ.ЯдернаяБлокировкаДляСборщикаМусора);

				если obj # НУЛЬ то enum( obj, cont ); всё;
				если cont то fn := next иначе fn := НУЛЬ всё
			кц
		кон EnumerateN;

		(** Initialize new collection. May also be called to clear an existing collection. *)
		проц &Clear*;
		нач {единолично}
			нов(root); root.nextObj := НУЛЬ	(* head *)
		кон Clear;

	кон FinalizedCollection;

перем
	second- : цел32; (** number of ticks per second (Hz) *)

	(*
	PROCEDURE Watch;
	VAR free, total1, total2, largest, low, high: SIGNED32;
	BEGIN
		IF TraceFin THEN
			Heaps.GetHeapInfo( total1, free, largest );  total1 := (total1 + 512) DIV 1024;  free := (free + 512) DIV 1024;
			largest := (largest + 512) DIV 1024;  Machine.GetFreeK( total2, low, high );  KernelLog.Enter;  KernelLog.String( "Heap: " );
			KernelLog.Int( total1, 1 );  KernelLog.String( " total, " );  KernelLog.Int( free, 1 );  KernelLog.String( " free, " );  KernelLog.Int( largest, 1 );
			KernelLog.String( " largest, Mem: " );  KernelLog.Int( total2, 1 );  KernelLog.String( " total, " );  KernelLog.Int( low, 1 );  KernelLog.String( " low, " );
			KernelLog.Int( high, 1 );  KernelLog.String( " high" );  KernelLog.Exit
		END
	END Watch;
	*)

	(** Return the number of ticks since system start. For timeouts, time measurements, etc, please use Kernel.MilliTimer instead.
		Ticks increment rate is stored in "second" variable in Hz. *)
	проц GetTicks*() : цел32;
	нач
		возврат Kernel32.GetTickCount()
	кон GetTicks;

	(** -- Garbage collection -- *)

	(** Activate the garbage collector immediately. *)

	проц GC*;
	нач
		Heaps.LazySweepGC;
	кон GC;

	(** -- Timers -- *)

(** Set timer to expire in approximately "ms" milliseconds. *)
	проц SetTimer*( перем t: MilliTimer;  ms: цел32 );
	нач
		если ЭВМ.ЧастотаКвантовВремени˛Гц # 1000 то  (* convert to ticks *)
			утв ( (ms >= 0) и (ms <= матМаксимум( цел32 ) DIV ЭВМ.ЧастотаКвантовВремени˛Гц) );
			ms := ms * ЭВМ.ЧастотаКвантовВремени˛Гц DIV 1000
		всё;
		если ms < 5 то увел( ms ) всё;   (* Nyquist adjustment *)
		t.start := Kernel32.GetTickCount();  t.target := t.start + ms
	кон SetTimer;

(** Test whether a timer has expired. *)
	проц Expired*( перем t: MilliTimer ): булево;
	нач
		возврат Kernel32.GetTickCount() - t.target >= 0
	кон Expired;

(** Return elapsed time on a timer in milliseconds. *)
	проц Elapsed*( перем t: MilliTimer ): цел32;
	нач
		возврат (Kernel32.GetTickCount() - t.start) * (1000 DIV ЭВМ.ЧастотаКвантовВремени˛Гц)
	кон Elapsed;

(** Return time left on a timer in milliseconds. *)
	проц Left*( перем t: MilliTimer ): цел32;
	нач
		возврат (t.target - Kernel32.GetTickCount()) * (1000 DIV ЭВМ.ЧастотаКвантовВремени˛Гц)
	кон Left;

	проц Nothing;
	нач

	кон Nothing;


нач
	утв (1000 остОтДеленияНа ЭВМ.ЧастотаКвантовВремени˛Гц = 0);   (* for Elapsed *)
	second := ЭВМ.ЧастотаКвантовВремени˛Гц;
	Heaps.GC := Heaps.InvokeGC; (* must be done after all processors have started *)
	(*
	Heaps.GC := Nothing;
	*)
кон Kernel.

(**
Notes:
o The FinalizedCollection object implements collections of finalized objects.
o Objects added to a finalized collection (with Add) are removed automatically by the garbage collector when no references to them exist any more.  They can also be removed explicitly with Remove.
o All the objects currently in a collection can be enumerated by Enumerate, which takes an enumerator procedure as parameter.  The enumerator can also be a method in an object, which is useful when state information is required during the enumeration.  The enumerator may not call other methods of the same collection.
o An object in a finalized collection can have an finalizer procedure associated with it, which gets called by a separate process when there are no references left to the object any more.  A finalizer is usually used for some cleanup functions, e.g. releasing external resources. It is executed exactly once per object.  During the next garbage collector cycle the object is finally removed.
*)

(*
to do:
o cancel finalizer when removing object
o fix module free race: module containing finalizer is freed.  although the finalizer list is cleared, the FinalizerCaller has already taken a reference to a finalizer, but hasn't called it yet.
o consider: a module has a FinalizedCollection, without finalizers (NIL).  when the module is freed, the objects are still in the finalization list, and will get finalized in the next garbage collection.  The FinalizedCollection will survive the first collection, as the objects all have references to it through their c field.  After all objects have been finalized, the FinalizedCollection itself is collected.  No dangling pointers occur, except the untraced module field references from the type descriptors, which are only used for tracing purposes.
o check cyclic dependencies between finalized objects.
o GetTime(): SIGNED32 - return current time in ms
o Delay(td: SIGNED32) - wait td ms
o AwaitTime(t: SIGNED32) - wait at least until time t
o Wakeup(obj: ANY) - wake up object that is waiting
*)

