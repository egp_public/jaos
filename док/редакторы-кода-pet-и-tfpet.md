## Две IDE

Существует простой PET, который поддерживается в A2, и продвинутый TFPET, который был создан в 2012 году, потом заброшен, в 2019 году починен мною и теперь называется ИСР. Основное отличие TFPET* от PET - в TFPET есть семантический парсер и переход к определению по F12. Этот функционал работает только в TFPET/ИСР и только в Яос (не работает в оригинальной A2). Но в чём-то PET может быть лучше. Например, я открываю ЯОС.фок с помощью PET. Кроме того, он хорош как запасной редактор в те времена, когда TFPET находится в переработке.
Для открытия документа можно подать команду

``` 
PET.Open ИмяФайла~
или 
TFPET.Open ИмяФайла~
```

Некоторые инструменты (например, TFXRef) выдают место ошибки в виде числовой позиции в файле. Внизу окна PET есть поле Position, в котором можно ввести это число и попасть к нужному месту. 

Также см. [[Как найти в исходном тексте место ошибки с красного экрана - trap, find pc, FindPC]]

Некоторые сочетания клавиш по состоянию на 2012-10-22:
``` 
 * Shortcuts:
 *
 *	CTRL-F:		Show search panel of current page, set focus to its editor and clear the content of the editor
 *	CTRL-N:	If search panel is visible: Find next occurence downwards
 *				elsif error grid is visible: Jump to next error position
 *	CTRL-P:		If search panel is visible: Find next occurence upwards
 *				elsif error grid is visible: Jump to previous error position
 *
 *	CTRL-0:		Set cursor to position zero
 *	CTRL-9:		Move cursor to end of text
 *
 *	CTRL-O:	Set focus to filename editor and clear its content
 *	CTRL-S:		Store current page
 *
 *	CTRL-M:	Toggle visibility of sidepanel
 *
 *	CTRL-H:		Compile file opened in current page
 *	CTRL-U:		Unload module opened in current  page
 *	CTRL-D:	Diff current tab to file
 *
 *	CTRL-DEL	Delete end-of-line whitespace in current page
 *
 *	ALT-INS	Uncomment selected code of commented
 *	ALT-DEL	Comment selected code
 *
 *	CTRL-PgUp	Load PET state from file
 * 	CTRL-PgDn	Save PET state to file
 *
 *	CTRL-SHIFT-Tab		Select previous tab
 *	CTRL-Tab			Select next tab
 *
 *	SHIFT-F1..F3	Store current cursor position
 *	CTRL-F1..F3	Recall cursor position
 *
 * When the filename editor has the keyboard focus:
 *
 *	ESC				Reset filename to filename of currently opened tab
 *	ENTER			Open  file
 *	SHIFT_ENTER	Open file in new PET window instance
*
```

По поводу выгрузки модуля, к-рый вы скомпилировали, см. [инкрементная-разработка-win32.md](инкрементная-разработка-win32.md) .

