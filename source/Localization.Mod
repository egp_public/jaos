модуль Localization; (** AUTHOR "staubesv"; PURPOSE "Localization interface"; *)

использует
	Configuration;

тип
	Language* = запись
		code* : массив 3 из симв8; (* ISO 936-1 language code *)
	кон;

	Languages* = укль на массив из Language;

перем
	currentLanguage : Language;
	preferenceList : Languages; (* {(preferenceList # NIL) & (LEN(preferenceList) >= 0) } *)

проц GetCurrentLanguage*() : Language;
нач {единолично}
	возврат currentLanguage;
кон GetCurrentLanguage;

проц GetLanguagePreferences*() : Languages;
нач {единолично}
	возврат preferenceList;
кон GetLanguagePreferences;

проц SetLanguage*(конст language : Language);
нач {единолично}
	currentLanguage := language;
	preferenceList[0] := currentLanguage;
кон SetLanguage;

проц GetDefaultLanguage;
перем
	res: целМЗ;
нач
	Configuration.Get("Local.Language", currentLanguage.code, res);
	если res # Configuration.Ok то
		currentLanguage.code := "en"
	всё
кон GetDefaultLanguage;

нач
	GetDefaultLanguage;
	нов(preferenceList, 1);
	preferenceList[0] := currentLanguage;
кон Localization.

System.Free Localization ~
