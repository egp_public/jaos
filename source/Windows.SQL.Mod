(* OBERON System 3, Release 2.3.

Copyright 1999 ETH Zürich Institute for Computer Systems,
ETH Center, CH-8092 Zürich.  e-mail: oberon@inf.ethz.ch.

This module may be used under the conditions of the general Oberon
System 3 license contract.  The full text can be downloaded from

	"ftp://ftp.inf.ethz.ch/pub/software/Oberon/System3/license.txt;A"

Under the license terms stated it is in particular (a) prohibited to modify
the interface of this module in any way that disagrees with the style
or content of the system and (b) requested to provide all conversions
of the source code to another platform with the name OBERON. *)

модуль SQL; (** non-portable / source: Windows.SQL.Mod *)	(** MAD  **)

использует ODBC, Modules, ЛогЯдра, НИЗКОУР, Files, Kernel, Heaps;

конст
	NTS = -3;
	DataAtExec = -2;
	BlockSize = 1024;

	(** result codes **)
	Error* = -1;	(** error occured while executin last operation **)
	Success* = 0;	(** last operation completed successfully **)
	SuccessWithInfo* = 1;	(** last operation completed successfully, but information available with EnumErrors **)
	NeedData* = 99;	(** execution of statement needs more data, eg not all paramters bound to statement **)
	NoDataFound* = 100;	(** Fetch could not retrieve more data **)

	(** parameter types used for ParamType.inOut **)
	InParam* = 1;	(** parameter used only for input, ie to put data into database **)
	InOutParam* = 2;	(** parameter used both for input and output **)
	OutParam* = 4;	(** parameter used only for output, ie. to retrieve data from database **)

	(** SQL data types **)
	SqlBitType* = -7;	(** single bit binary data **)
	SqlTinyIntType* = -6;	(** SIGNED8 **)
	SqlBigIntType* = -5;	(** 64 bit integer data **)
	SqlLongVarBinaryType* = -4;	(** variable length binary data, maximum length is data source dependant **)
	SqlVarBinaryType* = -3;	(** variable length binary data of maximum length n (1 <= n <= 255) **)
	SqlBinaryType* = -2;	(** binary data of fixed length n (1<= n <=255) **)
	SqlLongVarCharType* = -1;	(** variable length character data, maximum length is data source dependant **)
	SqlCharType* = 1;	(** character string of fixed length n (1 <= n <= 254) **)
	SqlNumericType* = 2;	(** signed exact numeric value with a precision p and scale s (1<=p<=15, 0<=s<=p) **)
	SqlDecimalType* = 3;	(** signed exact numeric value with a precision p and scale s (1<=p<=15, 0<=s<=p) **)
	SqlLongIntType* = 4;	(** SIGNED32 **)
	SqlIntType* = 5;	(** SIGNED16 **)
	SqlFloatType* = 6;	(** FLOAT64 **)
	SqlRealType* = 7;	(** FLOAT32 **)
	SqlLongRealType* = 8;	(** FLOAT64 **)
	SqlDateType* = 9;	(** date data (yyyy-mm-dd) **)
	SqlTimeType* = 10;	(** time data (hh:mm:ss) **)
	SqlTimeStampType* = 11;	(** date/time data (yyyy-mm-dd hh:mm:ss[f..]) **)
	SqlVarCharType* = 12;	(** variable length character string with maximum string length n (1 <= n <= 254) **)

	(** Oberon data types **)
	CharType* = 1;	(* SqlCharType *)
	LongIntType* = 4;	(* SqlLongIntType *)
	IntType* = 5;	(* SqlIntType *)
	ShortIntType* = -6;	(* SqlTinyIntType *)
	RealType* = 7;	(* SqlRealType *)
	LongRealType* = 8;	(* SqlLongRealType *)
	DateType* = 9;	(* SqlDateType *)
	TimeType* = 10;	(*SqlTimeType *)
	TimeStampType* = 11;	(* SqlTimeStampType *)
	FileType* = -4;	(* SqlLongVarBinaryType *)
	BinaryType* = -2;	(* SqlBinaryType *)
	BooleanType* = -7;	(* SqlBitType *)

	(** All these came from FreePascal  *)

    FetchNext* = 1;
    FetchFirst* = 2;
    FetchLast* = 3;
    FetchPrevious* = 4;
    FetchAbsolute* = 5;
    FetchRelative* = 6;

    SQLAttrCursorScrollable = -(1);
	SQLNonScrollable = 0;
    SQLScrollable = 1;

    SQLAttrODBCCursors = 110;
    SQLCursorUseIfNeeded = 0;
    SQLCursorUseODBC = 1;
    SQLCursorUseDriver = 2;


тип
	(** enumerate procedure used for EnumDataSources and EnumDrivers **)
	SourcesHandler* = проц(name, desc: массив из симв8);

	(** enumerate procedure used for EnumErrors **)
	ErrorProc* = проц(state, msg: массив из симв8; errorcode: цел32);

	(** database connection handle **)
	Connection* = укль на ConnectionDesc;

	(** SQL statement handle **)
	Statement* = укль на StatementDesc;

	(** row handle
	Whenever you prepare a SQL statement that delievers any data (SELECT statement) a Row is created. A Row contains
	a list of records which are extensions of a base type Field. Each such Field in the Row represents a column in the
	result set of the SQL statement, so they are used to access the data. The data in a result row only valid after
	executing the statement (Execute) and fetching data (Fetch).
	A Row must also be created if you want to execute a SQL statement which needs some parameters, ie. if you call
	a stored procedure or execute an INSERT statement with data at execution (for example"INSERT INTO person
	VALUES (?, ?)" where the question marks signal that the corresponding data is set in the parameter row). This
	parameter row must be created with the rocedure BindParamaters after a call to PrepareStataement and before
	you execute the statement with with procedure Execute. The data in a parameter row muts be set before executing
	the statement, and if a parameter is used to get it data the retrieved data is valid after executing the statement **)
	Row* = укль на RowDesc;

	ErrBuff = укль на ErrBuffDesc;

	ConnectionDesc = запись
			hdbc: ODBC.HDBC;
			closed: булево;
			stmt: Statement;
			res*: цел16;
		кон;

	StatementDesc = запись
			next: Statement;
			hstmt: ODBC.HSTMT;
			c: Connection;
			results, params: Row;
			firstExec, errBuffered: булево;
			error: ErrBuff;
			res*: цел16;
		кон;

	ErrBuffDesc = запись
			state: массив 6 из симв8;
			msg: массив 512 из симв8;
			native: цел32;
			next: ErrBuff
		кон;

	(** base type of each field in a result set or a parameter list **)
	Field* = укль на FieldDesc;
	FieldDesc* = запись
			next, prev: Field;
			dir: цел16;	(* in/out/inout *)
			name*: массив 32 из симв8;	(** name of column **)
			len*: цел32;	(** maximum number of characters needed to represent data, only valid for Fields in a result set
												(ie. not for parameters), and only between a call to PrepareStatement and Execute **)
			sqlType*: цел16;	(** SQL type of column (needed because different SQL types are mapped into same
													field type) **)
			isNull*: булево;	(** get/set if field is NULL **)
			nullable*: булево	(** determine if field is nullable **)
		кон;

	(** field extension to hold integer data **)
	IntField* = укль на IntFieldDesc;
	IntFieldDesc* = запись(FieldDesc)
			i*: цел32
		кон;

	(*ALEX 2005.10.20*)
	(** field extension to hold character data for numeric(n,m) fields **)
	NumericField* = укль на NumericFieldDesc;
	NumericFieldDesc* = запись(FieldDesc)
			str*: массив 256 из симв8
		кон;


	(** field extension to hold character data (SQLCharType and SQLVarCharType) **)
	StringField* = укль на StringFieldDesc;
	StringFieldDesc* = запись(FieldDesc)
			str*: массив 256 из симв8
		кон;

	(** field extension to hold floating point data (SQLFloatType, SQLRealType and SQLLongRealType) **)
	RealField* = укль на RealFieldDesc;
	RealFieldDesc* = запись(FieldDesc)
			r*: вещ64
		кон;

	(** field extension to hold date (SQLDateType) **)
	DateField* = укль на DateFieldDesc;
	DateFieldDesc* = запись(FieldDesc)
			year*, month*, day*: цел16
		кон;

	(** field extension to hold time (SQLTimeType) **)
	TimeField* = укль на TimeFieldDesc;
	TimeFieldDesc* = запись(FieldDesc)
			hour*, minute*, second*: цел16
		кон;

	(** field extension to hold time stamp (SQLTimeStampType) **)
	TimeStampField* = укль на TimeStampFieldDesc;
	TimeStampFieldDesc* = запись(FieldDesc)
			year*, month*, day*, hour*, minute*, second*: цел16;
			fraction*: цел32
		кон;

	(** field extension to hold long data like SQLLongVarCharType or SQLLongBinaryType **)
	FileField* = укль на FileFieldDesc;
	FileFieldDesc* = запись(FieldDesc)
			f*: Files.File
		кон;

	(** field extension to hold binary data **)
	BinaryField* = укль на BinaryFieldDesc;
	BinaryFieldDesc* = запись(FieldDesc)
			b*: массив 256 из НИЗКОУР.октет
		кон;

	(** field extension to hold boolean data **)
	BooleanField* = укль на BooleanFieldDesc;
	BooleanFieldDesc* = запись(FieldDesc)
			b*: булево
		кон;

	SentinelField = укль на SentinelFieldDesc;
	SentinelFieldDesc = запись(FieldDesc)
		кон;

	(** handle for SQL statement results and parameters **)
	RowDesc* = запись
			dsc: Field;
			cols*: цел16
		кон;

	(** parameter description record **)
	ParamType* = запись
			oberonType*, sqlType*, inOut*: цел16;
			name*: массив 32 из симв8;
		кон;

перем
	(** result code of last operation **)
	(*res*: SIGNED16;*)

(* ----------------------- Row handling ----------------------- *)

проц AllocRow(перем row: Row);
	перем sentinel: SentinelField;
нач
	нов(row); нов(sentinel); row.dsc:= sentinel;
	sentinel.next:= sentinel; sentinel.prev:= sentinel
кон AllocRow;

проц AppendField(r: Row; f: Field);
нач
	r.dsc.prev.next:= f; f.prev:= r.dsc.prev; f.next:= r.dsc; r.dsc.prev:= f
кон AppendField;

(** set f to first field in row r **)
проц FirstField*(r: Row; перем f: Field);
нач
	если r.dsc.next = r.dsc то f:= НУЛЬ иначе f:= r.dsc.next всё
кон FirstField;

(** get next field in row containing f, NIL if there are no more fields **)
проц NextField*(перем f: Field);
нач
	если f.next суть SentinelField то f:= НУЛЬ иначе f:=f.next всё
кон NextField;

(** get previous field in row containing f, NIL if there is no previous field **)
проц PrevField*(перем f: Field);
нач
	если f.prev суть SentinelField то f:= НУЛЬ иначе f:= f.prev всё
кон PrevField;

(** find field named name in row r **)
проц FindField*(r: Row; name: массив из симв8; перем f: Field);
	перем cur: Field;
нач
	cur:= r.dsc.next;
	нцПока ~(cur суть SentinelField) делай
		если cur.name = name то f:= cur; возврат всё;
		cur:= cur.next
	кц;
	f:= НУЛЬ
кон FindField;

(* -------------------------------------------------------------------------- *)

проц PrintError(state, msg: массив из симв8; errorCode: цел32);
нач
	ЛогЯдра.пСтроку8(state); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС
кон PrintError;

проц DummyEnum(state, msg: массив из симв8; code: цел32);
кон DummyEnum;

проц InsertError(stmt: Statement; state, msg: массив из симв8; nativeCode: цел32);
	перем err: ErrBuff;
нач
	нов(err); stmt.errBuffered:= истина; err.next:= stmt.error; stmt.error:= err;
	копируйСтрокуДо0(state, err.state); копируйСтрокуДо0(msg, err.msg); err.native:= nativeCode
кон InsertError;

(** enumerate all errors belonging to connection c and statement s. IF s = NIL and c # NIL then all errors for
	connection c are enumerated. If both s and c are NIL then all errors belonging to SQL and ODBC themselves
	are enumerated **)
проц EnumErrors*(c: Connection; s: Statement; enum: ErrorProc);
	перем localErr: ErrBuff; errorState: массив 6 из симв8; errorMsg: массив 512 из симв8; nativeError: цел32;
нач
	если s # НУЛЬ то
		если s.errBuffered то
			localErr:= s.error;
			нцПока localErr # НУЛЬ делай
				enum(s.error.state, s.error.msg, s.error.native); localErr:= localErr.next
			кц;
			s.errBuffered:= ложь; s.error:= НУЛЬ
		всё;
		ODBC.StatementError(s.hstmt, errorState, nativeError, errorMsg, s.res);
		нцПока s.res # 100 делай
			enum(errorState, errorMsg, nativeError);
			ODBC.StatementError(s.hstmt, errorState, nativeError, errorMsg, s.res);
		кц;
		ODBC.ConnectionError(s.c.hdbc, errorState, nativeError, errorMsg, s.res);
		нцПока s.res # 100 делай
			enum(errorState, errorMsg, nativeError);
			ODBC.ConnectionError(s.c.hdbc, errorState, nativeError, errorMsg, s.res);
		кц
	аесли c # НУЛЬ то
		ODBC.ConnectionError(c.hdbc, errorState, nativeError, errorMsg, c.res);
		нцПока c.res # 100 делай
			enum(errorState, errorMsg, nativeError);
			ODBC.ConnectionError(c.hdbc, errorState, nativeError, errorMsg, c.res);
		кц
	всё
кон EnumErrors;

проц FinalizeConnection(obj: динамическиТипизированныйУкль);
	перем c: Connection;
нач
	c:= obj(Connection); ЛогЯдра.пСтроку8("SQL.FinalizeConnection: ");
	если ~c.closed то
		ЛогЯдра.пСтроку8("closing connection"); ЛогЯдра.пВК_ПС;
		ODBC.Commit(c.hdbc, c.res);
		ODBC.Disconnect(c.hdbc, c.res);
		ODBC.FreeConnect(c.hdbc, c.res)
	иначе
		ЛогЯдра.пСтроку8("connection already closed"); ЛогЯдра.пВК_ПС
	всё
кон FinalizeConnection;

проц Terminate;
нач
	(* call garbage collector to finalize all registered connections *)
	ЛогЯдра.пСтроку8("SQL.Terminate: calling Kernel.GC"); ЛогЯдра.пВК_ПС;
	Kernel.GC;
	ЛогЯдра.пСтроку8("SQL.Terminate: calling Kernel.GC a second time"); ЛогЯдра.пВК_ПС;
	Kernel.GC
кон Terminate;

(** open a connection to database source  **)
проц Open*(source, user, passwd: массив из симв8): Connection;
	перем connect: ODBC.HDBC; connection: Connection;
	finalizerNode: Heaps.FinalizerNode;
нач
	нов(connection); нов(connection.hdbc); connection.stmt:= НУЛЬ; connection.closed:= ложь;
	ODBC.AllocConnect(connection.hdbc, connection.res);
	ODBC.SetConnectAttr(connection.hdbc, SQLAttrODBCCursors, SQLCursorUseODBC, 0, connection.res);
	ODBC.Connect(connection.hdbc, source, user, passwd, connection.res);
	(*ALEX 2005.11.14*)
	нов(finalizerNode); finalizerNode.finalizer := FinalizeConnection;
	Heaps.AddFinalizer(connection, finalizerNode);

	возврат connection
кон Open;

(** close a connection **)
проц Close*(c: Connection);
	перем stmt: Statement;
нач
	(* Free all statements, disconnect and free connection *)
	stmt:= c.stmt; c.closed:= истина;
	(*
	WHILE stmt # NIL DO
		ODBC.FreeStmt(stmt.hstmt, 1); stmt:= stmt.next	(* opt = 1: drop statement and all resources associated with it *)
	END;
	*)
	ODBC.Disconnect(c.hdbc, c.res);
	ODBC.FreeConnect(c.hdbc, c.res);
кон Close;

(** bind parameter fields to statement s. The array types contains a description of each paramete:
		types[i].oberonType determines the type to be used in Obeorn, ie. what sort of Field (IntField, FielField, etc) should
			be added to paramRow
		types[i].sqlType determines the type the parameter uses in the data source (SqlInt, SqlLongVarBinary, etc)
		types[i].inOut determines for which operation the parameter is used: to put data into the DB -> InParam (parameter
			is used in an INSERT statement or in a stored procedure), to retrieve data from the DB -> OutParam (parameter is
			used in a stored procedure the get data), or to put and get data -> InOutParam (parameter is used in a stored
			procedure for input an output)
		types[i].name can be used to name the parameter, ie you can find the corresponding Field in paramRow with
			the procedure FindField
	A row containing a Field for each parameter is returned in paramRow **)
проц BindParameters*(s: Statement; types: массив из ParamType; numParams: цел16; перем paramRow: Row);
	тип arr6 = массив 6 из симв8; arr16 = массив 16 из симв8;
	перем i, parType: цел16; if: IntField; sf: StringField; rf: RealField; df: DateField; tf: TimeField; tsf: TimeStampField;
		ff: FileField; bf: BinaryField; boolf: BooleanField; buffer: массив BlockSize из симв8;
нач
	AllocRow(s.params); s.params.cols:= numParams;
	нцДля i:= 0 до numParams-1 делай
		(* insert field into params row of statement *)
		(* Out.String("binding param "); Out.Int(i+1, 1); Out.Ln; *)
		просей types[i].oberonType из
			CharType:
				нов(sf); AppendField(s.params, sf);
				ODBC.BindParameter(s.hstmt, i+1, types[i].inOut, types[i].oberonType, types[i].sqlType, 255, 0, sf.str, sf.len, s.res)
				(* sf.len will be set to NTS before executing the statement *)

			| LongIntType, IntType, ShortIntType:
				нов(if); AppendField(s.params, if);
				ODBC.BindParameter(s.hstmt, i+1, types[i].inOut, types[i].oberonType, types[i].sqlType, 0, 0, if.i, if.len, s.res)

			| RealType, LongRealType:
				нов(rf); AppendField(s.params, rf);
				ODBC.BindParameter(s.hstmt, i+1, types[i].inOut, types[i].oberonType, types[i].sqlType, 0, 0, rf.r, rf.len, s.res)

			| DateType:
				нов(df); AppendField(s.params, df);
				ODBC.BindParameter(s.hstmt, i+1, types[i].inOut, types[i].oberonType, types[i].sqlType, 0, 0,
					df, df.len, s.res)

			| TimeType:
				нов(tf); AppendField(s.params, tf);
				ODBC.BindParameter(s.hstmt, i+1, types[i].inOut, types[i].oberonType, types[i].sqlType, 0, 0,
					tf, tf.len, s.res)

			| TimeStampType:
				нов(tsf); AppendField(s.params, tsf);
				ODBC.BindParameter(s.hstmt, i+1, types[i].inOut, types[i].oberonType, types[i].sqlType, 0, 0,
					tsf, tsf.len, s.res)

			| FileType:
				нов(ff); ff.f:= Files.New(""); AppendField(s.params, ff);
				(* to be compatible with drivers which don't have convert functions, require that the data in file
					is already in right format *)
				если (types[i].sqlType = SqlVarCharType) или (types[i].sqlType = SqlLongVarCharType) то
					parType:= CharType
				аесли (types[i].sqlType = SqlVarBinaryType) или (types[i].sqlType = SqlLongVarBinaryType) то
					parType:= BinaryType
				аесли (types[i].sqlType = SqlFloatType) то
					parType:= RealType
				аесли (types[i].sqlType = SqlNumericType) или (types[i].sqlType = SqlDecimalType) то
					parType:= CharType
				аесли types[i].sqlType = SqlBigIntType то
					parType:= CharType
				иначе
					parType:= types[i].sqlType
				всё;
				ODBC.BindParameter(s.hstmt, i+1, types[i].inOut, parType, types[i].sqlType, (*BlockSize*) 0, 0, buffer, ff.len, s.res)
				(* ff.len will be set to DataAtExec before executing the statement *)

			| BinaryType:
				нов(bf); AppendField(s.params, bf);
				ODBC.BindParameter(s.hstmt, i+1, types[i].inOut, types[i].oberonType, types[i].sqlType, 255, 0, bf.b, bf.len, s.res)

			| BooleanType:
				нов(boolf); AppendField(s.params, boolf);
				ODBC.BindParameter(s.hstmt, i+1, types[i].inOut, types[i].oberonType, types[i].sqlType, 255, 0, boolf.b, boolf.len, s.res)

		иначе СТОП(99)
		всё;
		s.params.dsc.prev.dir:= types[i].inOut; копируйСтрокуДо0(types[i].name, s.params.dsc.prev.name);
		если s.res < 0 то s.params:= НУЛЬ; paramRow:= НУЛЬ; (* EnumErrors(s.c, s, PrintError); *) возврат всё
	кц;
	paramRow:= s.params
кон BindParameters;

(** prepares a SQL statement for execution. If sqlStatement returns any results (eg. a SELECT-statement) resultRow
	will point to row conataining a field for every element of the result set, else row is set to NIL. After PrepareStatement
	the record field len of each element of resultRow contains the maximum number of characters needed to represent
	the corresponding data. The value in this record field len will only be valid until a call to Execute, so if you need
	this data you must check it between the calls to PrepareStatement and to Execute. **)
проц PrepareStatement*(c: Connection; sqlStatement: массив из симв8; перем resultRow: Row): Statement;
	тип arr6 = массив 6 из симв8; arr16 = массив 16 из симв8;
	перем stmt: Statement; cols, i, type, scale, oldres: цел16; name: массив 256 из симв8; nullable: булево;
		prec, nativeBuf: цел32; stateBuf: массив 6 из симв8; msgBuf: массив 512 из симв8;
		if: IntField; sf: StringField; rf: RealField; df: DateField; tf: TimeField; tsf: TimeStampField; cur: Field;
		bf: BinaryField; ff: FileField; boolf: BooleanField;
		nf: NumericField;
нач
	нов(stmt); нов(stmt.hstmt); ODBC.AllocStmt(c.hdbc, stmt.hstmt, stmt.res);
	stmt.next:= c.stmt; c.stmt:= stmt;
	stmt.c:= c; stmt.results:= НУЛЬ; stmt.params:= НУЛЬ; stmt.firstExec:= истина; stmt.errBuffered:= ложь; resultRow:= НУЛЬ;

	(*ALEX 2005.12.06 - commented out because of sybase not suporting this property*)
	(*
	ODBC.SetStmtAttr(stmt.hstmt, SQLAttrCursorScrollable, SQLScrollable, 0, stmt.res);
	IF stmt.res #  Success THEN
		KernelLog.String('SetStmtAttr:ODBC.res: '); KernelLog.Int(stmt.res,0); KernelLog.Ln;
		EnumErrors(c, stmt, PrintError)
	END;
	*)

	ODBC.Prepare(stmt.hstmt, sqlStatement, stmt.res);
	если stmt.res < 0 то
		возврат stmt
	иначе
		(* As the Prepare function is not guaranteed to do the same work on all the different ODBC database drivers
			it is not always sufficient calling ODBC.Prepare to get all needed information about the result set. So we also
			need to make a call to ODBC.Execute. Afterwards a call to ODBC.NumResultCols and ODBC.DescribeCol
			should work with every ODBC database (except MicroSoft included some other features) *)
		ODBC.Execute(stmt.hstmt, stmt.res);
		если stmt.res < 0 то
			(* check why error occured: if parameters are missing (state = 07001) just ignore error, else buffer error and
			exit prepare *)
			(* Out.String("ODBC.res = "); Out.Int(ODBC.res, 1); Out.Ln; *)
			oldres:= stmt.res; ODBC.StatementError(stmt.hstmt, stateBuf, nativeBuf, msgBuf, stmt.res);
			(* Out.String("Error in PrepareStatement: "); Out.Ln; Out.String(stateBuf); Out.String(msgBuf); Out.Ln; *)
			если stateBuf = "07001" то
				(* ignore error, but read remaining errors of statement *)
				(* Out.String("error ignored, continuing PrepareStatement"); Out.Ln; *)
				EnumErrors(c, stmt, DummyEnum); stmt.errBuffered:= ложь; stmt.firstExec:= ложь
			иначе
				(* 'real' error *)
				(* Out.String("aborting PrepareStatement"); Out.Ln; *)
				stmt.errBuffered:= истина; нов(stmt.error);
				копируйСтрокуДо0(stateBuf, stmt.error.state); копируйСтрокуДо0(msgBuf, stmt.error.msg); stmt.error.native:= nativeBuf;
				stmt.firstExec:= ложь; stmt.res:= oldres; возврат stmt
			всё
		всё;
		ODBC.NumResultCols(stmt.hstmt, cols, stmt.res);
		если cols > 0 то
			AllocRow(resultRow); resultRow.cols:= cols;
			(* Out.String("Anzahl ResultCols: "); Out.Int(cols, 5); Out.Ln; *)
			нцДля i:= 1 до cols делай
				ODBC.DescribeCol(stmt.hstmt, i, name, type, prec, scale, nullable, stmt.res);
				(* Out.String(name); Out.Char(9X); Out.Int(type, 5); Out.Char(9X); Out.Int(prec, 5); Out.Char(9X);
				Out.Int(scale, 5); Out.Char(9X); IF nullable THEN Out.String("nullable") ELSE Out.String("not nullable") END;
				Out.Ln; *)
				просей type из
					1, 12: нов(sf); AppendField(resultRow, sf); sf.len:= prec;
						ODBC.BindCol(stmt.hstmt, i, CharType, sf.str, sf.len, stmt.res);

					| 2, 3: нов(nf); AppendField(resultRow, nf); nf.len:= prec + 2; (* prec digits, sign, decimal point *)
						ODBC.BindCol(stmt.hstmt, i, CharType, nf.str, nf.len, stmt.res);
						(*ALEX 2005.10.20 modified from StringField to NumericField*)

					| 4, 5, -6: нов(if); AppendField(resultRow, if);
						если type = 4 то if.len:= 11 аесли type = 5 то if.len:= 6 иначе if.len:= 4 всё;
						ODBC.BindCol(stmt.hstmt, i, LongIntType, if.i, if.len, stmt.res);

					| 6, 7, 8: нов(rf); AppendField(resultRow, rf);
						если type = 7 то rf.len:= 13 иначе rf.len:= 22 всё;
						ODBC.BindCol(stmt.hstmt, i, LongRealType, rf.r, rf.len, stmt.res);

					| 9: нов(df); AppendField(resultRow, df); df.len:= prec;
						ODBC.BindCol(stmt.hstmt, i, DateType, df, df.len, stmt.res);

					| 10: нов(tf); AppendField(resultRow, tf); tf.len:= prec;
						ODBC.BindCol(stmt.hstmt, i, TimeType, tf, tf.len, stmt.res);

					| 11: нов(tsf); AppendField(resultRow, tsf); если scale > 0 то tsf.len:= 20+scale иначе tsf.len:= 19 всё;
						ODBC.BindCol(stmt.hstmt, i, TimeStampType, tsf, tsf.len, stmt.res);

					| -2, -3: нов(bf); AppendField(resultRow, bf); bf.len:= prec;
						ODBC.BindCol(stmt.hstmt, i, LongIntType, bf.b, bf.len, stmt.res);

					| -1, -4: нов(ff); ff.f:= Files.New(""); AppendField(resultRow, ff); ff.len:= prec;
						(* don't bind this column, but get the data with ODBC.GetData *)
						(* ODBC.BindCol(stmt.hstmt, i, LongIntType, ff.f, ff.len); *)

					| -5: нов(sf); AppendField(resultRow, sf); sf.len:= 20;
						ODBC.BindCol(stmt.hstmt, i, CharType, sf.str, sf.len, stmt.res);

					| -7: нов(boolf); AppendField(resultRow, boolf); boolf.len:= 5;
						ODBC.BindCol(stmt.hstmt, i, LongIntType, boolf.b, boolf.len, stmt.res);

					иначе СТОП(99)
				всё;
				resultRow.dsc.prev.sqlType:= type; копируйСтрокуДо0(name, resultRow.dsc.prev.name);
				resultRow.dsc.prev.nullable:= nullable;
				если stmt.res < 0 то
					resultRow:= НУЛЬ; возврат stmt
				всё
			кц;	(* FOR *)
			stmt.results:= resultRow;
			(* cur:= resultRow.dsc.next; FOR i:= 1 TO resultRow.cols DO Out.String(cur.name); Out.Ln; cur:= cur.next END; *)
		всё	(* IF *)
	всё;
	возврат stmt
кон PrepareStatement;

(** execute a previously prepared statement. If the statement delievers any data it can now be retrieved using procedure
	Fetch.
	IMPORTANT: a statement which does not need parameters and does not return any data (for example
	"DROP TABLE MyTable") will take effect even if you don't call Execute for this statement handle **)
проц Execute*(s: Statement);
	перем parDesc, cur: Field; buffer: массив BlockSize из симв8; r: Files.Rider; cnt: цел32;
нач
	если s.firstExec то
		(* Out.String("discarding Execute"); Out.Ln; *)
		s.firstExec:= ложь; возврат
	всё;
	(* Out.String("doing Execute"); Out.Ln; *)
	cur:= s.params.dsc.next;
	нцПока ~(cur суть SentinelField) делай
		если cur.isNull то cur.len:= -1
		аесли cur суть StringField то cur.len:= NTS
		аесли cur суть FileField то cur.len:= DataAtExec
		иначе cur.len:= 0
		всё;
		cur:= cur.next
	кц;
	ODBC.Execute(s.hstmt, s.res);
	cur:= s.params.dsc.next;
	(* must also put data with ODBC.PutData if there are any FileFields *)
	нцПока s.res = NeedData делай
		ODBC.ParamData(s.hstmt, parDesc^, s.res);
		если s.res = NeedData то
			(* Out.String("needing data"); Out.Ln; *)
			нцПока ~(cur суть SentinelField) и ~(cur суть FileField) делай cur:= cur.next кц;
			если cur суть SentinelField то
				ЛогЯдра.пСтроку8("Warning: Field used for PutData is not a FileField"); ЛогЯдра.пВК_ПС; СТОП(99)
			аесли cur суть FileField то
				cnt:= 0; cur(FileField).f.Set(r, 0);
				нцДо увел(cnt);
					cur(FileField).f.ReadBytes(r, buffer, 0, BlockSize); ODBC.PutData(s.hstmt, buffer, BlockSize-r.res, s.res);
					если s.res < 0 то возврат всё;
					(* Out.Int(cnt, 5); Out.Int(BlockSize-r.res, 5); Out.Ln; Out.String(buffer); *)
				кцПри r.eof;
				cur:= cur.next;
				(* Out.Ln *)
			всё
		иначе
			(* Out.String("don't need more data, ODBC.res = "); Out.Int(ODBC.res, 1); Out.Ln *)
		всё
	кц;	(* WHILE *)
	cur:= s.params.dsc.next;
	нцПока ~(cur суть SentinelField) делай
		если cur.len = -1 то cur.isNull:= истина иначе cur.isNull:= ложь всё;
		cur:= cur.next
	кц;
кон Execute;

(** returns the number of rows affected by the execution of statement s. This is not the number of rows which are
	delievered by a SELECT Statement but rather the number of rows affected by an UPDATE or DELETE statement.
	If you want the number of rows in the result set of a SELECT statement use "SELECT COUNT( * ) .." or increment
	a counter variable after each call to Fetch **)
проц RowCount*(s: Statement; перем rows: цел32);
нач
	ODBC.RowCount(s.hstmt, rows, s.res);
кон RowCount;

(** fetch the next result row for statement s (of course statement s must have been executed before you can call
	the procedure Fetch) **)
проц Fetch*(s: Statement);
	перем cur: Field; col: цел16; resSize, actSize: цел32; buffer: массив BlockSize из симв8; r: Files.Rider;
нач
	ODBC.Fetch(s.hstmt, s.res);
	если (s.res < 0) или (s.res = 100) то возврат всё;
	(* now get all unbound columns with ODBC.GetData *)
	FirstField(s.results, cur); col:= 1;
	нцПока ~(cur суть SentinelField) делай
		если cur суть FileField то
			cur(FileField).f := Files.New( "" );	(* Dan 12.10.04 *)
			cur(FileField).f.Set(r,  0);
			нцДо
				ODBC.GetData(s.hstmt, col, -2, buffer, resSize, s.res);
				если s.res < 0 то
					(* Out.String("Error while fetching data from res col "); Out.Int(col, 1); Out.Ln; *)
					(* EnumErrors(s.c, s, PrintError); *) возврат
				всё;
				если resSize > длинаМассива(buffer) то actSize:= длинаМассива(buffer) иначе actSize:= resSize всё;
				(* Out.Int(actSize, 5); Out.Ln; *)
				если actSize > 0 то cur(FileField).f.WriteBytes(r, buffer, 0, actSize) всё; (*ALEX 2006.02.15 added the IF*)
			кцПри s.res # 1;
			(* Out.String("Filesize: "); Out.Int(Files.Length(cur(FileField).f), 1); Out.Ln *)
		иначе
		всё;
		увел(col); cur:= cur.next
	кц;
	cur:= s.results.dsc.next;
	нцПока ~(cur суть SentinelField) делай
		если cur.len = -1 то cur.isNull:= истина иначе cur.isNull:= ложь всё;
		cur:= cur.next
	кц;
кон Fetch;

(** JSS: fetch the result row for statement s (s must have been executed before you can call
	the procedure ExtendedFetch), based on fetchType and rowToFetch  **)
проц FetchExtended*(s: Statement; fetchType: цел16; rowToFetch: цел32; перем numFetchedRows: цел32; перем rowStatus: цел16 );
	перем
		cur: Field;
		col: цел16;
		resSize,
			actSize: цел32;
		(*buffer: ARRAY BlockSize OF SYSTEM.BYTE; *)
		buffer: массив BlockSize из симв8;
		r: Files.Rider;

нач
	ODBC.ExtendedFetch(s.hstmt, fetchType, rowToFetch, numFetchedRows, rowStatus, s.res);

	если (s.res < 0) или (s.res = 100) то возврат всё;
	(* now get all unbound columns with ODBC.GetData *)
	FirstField(s.results, cur); col:= 1;
	нцПока ~(cur суть SentinelField) делай
		если cur суть FileField то
			cur(FileField).f.Set(r, 0);
			нцДо
				ODBC.GetData(s.hstmt, col, -2, buffer, resSize, s.res);
				если s.res < 0 то
					(* Out.String("Error while fetching data from res col "); Out.Int(col, 1); Out.Ln; *)
					(* EnumErrors(s.c, s, PrintError); *) возврат
				всё;
				если resSize > длинаМассива(buffer) то actSize:= длинаМассива(buffer) иначе actSize:= resSize всё;
				(* Out.Int(actSize, 5); Out.Ln; *)
				если actSize > 0 то cur(FileField).f.WriteBytes(r, buffer, 0, actSize) всё; (*ALEX 2006.02.15 added the IF*)
			кцПри s.res # 1;
			(* Out.String("Filesize: "); Out.Int(Files.Length(cur(FileField).f), 1); Out.Ln *)
		иначе
		всё;
		увел(col); cur:= cur.next
	кц;
	cur:= s.results.dsc.next;
	нцПока ~(cur суть SentinelField) делай
		если cur.len = -1 то cur.isNull:= истина иначе cur.isNull:= ложь всё;
		cur:= cur.next
	кц;
кон FetchExtended;

(** commits all statements for connection c (works only if supported by the database) **)
проц Commit*(c: Connection);
нач
	ODBC.Commit(c.hdbc, c.res)
кон Commit;

(** rolls back all statements executed since last Commit for connection c (works only if supported by the database)  **)
проц Rollback*(c: Connection);
нач
	ODBC.Rollback(c.hdbc, c.res)
кон Rollback;

(** enumerates all data sources **)
проц EnumDataSources*(enum: SourcesHandler; перем res: цел16);
	перем name, desc: массив 256 из симв8;
нач
	ODBC.DataSources(2, name, desc, res);
	если res < 0 то возврат всё;
	нцПока res = 0 делай
		enum(name, desc);
		ODBC.DataSources(1, name, desc, res);

		если res < 0 то возврат всё;
	кц;
кон EnumDataSources;

(** enumerates all installed ODBC database drivers **)
проц EnumDrivers*(enum: SourcesHandler; перем res: цел16);
	перем name, desc: массив 256 из симв8;
нач
	ODBC.Drivers(2, name, desc, res);
	если res < 0 то возврат всё;
	нцПока res = 0 делай
		enum(name, desc);
		ODBC.Drivers(1, name, desc, res);
		если res < 0 то возврат всё;
	кц;
кон EnumDrivers;

(** prepares statement to retrieve all tables accessible within connection c
	result row:	field name	field type
		TABLE_QUALIFIER	StringField
		TABLE_OWNER	StringField
		TABLE_NAME	StringField
		TABLE_TYPE	StringField	("TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY"
								"LOCAL TEMPORARY", "ALIAS", "SYNONYM", or data-source specific)
		REMARKS	StringField
**)
проц Tables*(c: Connection; перем row: Row): Statement;
	перем stmt: Statement; i, cols, type, scale: цел16; if: IntField; sf: StringField; rf: RealField; cur: Field;
		name: массив 256 из симв8; prec: цел32; nullable: булево;
нач
	нов(stmt); нов(stmt.hstmt); ODBC.AllocStmt(c.hdbc, stmt.hstmt, stmt.res); stmt.c:= c; row:= НУЛЬ; stmt.firstExec:= истина;
	ODBC.Tables(stmt.hstmt, ODBC.nullString, ODBC.nullString, ODBC.nullString, ODBC.nullString, stmt.res);
	если stmt.res < 0 то
		row:= НУЛЬ; возврат stmt
	иначе
		ODBC.NumResultCols(stmt.hstmt, cols, stmt.res);
		AllocRow(row); row.cols:= cols; stmt.results:= row;
		нцДля i:= 1 до cols делай
			ODBC.DescribeCol(stmt.hstmt, i, name, type, prec, scale, nullable, stmt.res);
			просей type из
				1, 12: нов(sf); AppendField(row, sf);
					ODBC.BindCol(stmt.hstmt, i, CharType, sf.str, sf.len, stmt.res);
				иначе СТОП(99)
			всё;
			row.dsc.prev.sqlType:= type; копируйСтрокуДо0(name, row.dsc.prev.name); row.dsc.prev.nullable:= nullable;
			если stmt.res < 0 то
				row:= НУЛЬ; возврат stmt
			всё
		кц
	всё;
	возврат stmt
кон Tables;

(** retrieve the name of a SQL data type as it is used in the data source connected to by Connection c.
	For example some DBs call a 4 byte integer "LONG", and others call it "INT4". **)
проц GetTypeName*(c: Connection; sqlType: цел16; перем typeName: массив из симв8; перем res: цел16);
	перем stmt: Statement; len: цел32;
нач
	нов(stmt); нов(stmt.hstmt); ODBC.AllocStmt(c.hdbc, stmt.hstmt, stmt.res);
	typeName[0]:= 0X;
	ODBC.GetTypeInfo(stmt.hstmt, sqlType, stmt.res);
	если stmt.res < 0 то
		res:= stmt.res; возврат
	всё;
	ODBC.BindCol(stmt.hstmt, 1, CharType, typeName, len, stmt.res);
	ODBC.Fetch(stmt.hstmt, stmt.res);
	res:= stmt.res
кон GetTypeName;

(*ALEX 2006.03.22 added this function*)
(**closes a statement and removes it from the connection statements list*)
проц CloseStatement*(s: Statement);
перем prevStmt, stmt: Statement;
нач
	ODBC.FreeStmt(s.hstmt, 1, s.res);	(* opt = 1: drop statement and all resources associated with it *)
	stmt := s.c.stmt;

	нцПока (stmt # НУЛЬ) и (stmt # s) делай
		 prevStmt := stmt; stmt:= stmt.next
	кц;
	если prevStmt # НУЛЬ то prevStmt.next := s.next иначе s.c.stmt := s.next всё;
кон CloseStatement;

нач
	Modules.InstallTermHandler(Terminate)
кон SQL.


(**
Remarks:

Before you can execute a SQL statement you need to open a connection to the data source. Use procedure
Open to get a connection. When you no longer need the connection you can close it with Close.

In the following the basic steps used in commonly routines are described. It is assumed that there is already
an open connection

1. A simple SQL statement which needs no input and delievers no output:
	example: "INSERT INTO addresses VALUES ('Markus', 'Dätwyler')"
	- Prepare the statement with PrepareStatement. As the statement retrieves no data, resultRow is NIL.
	- Execute the statement with procedure Execute
	- The number of rows affected by the execution of this statement can be get with procedure RowCount.

2. A SQL statement with retrieves data:
	example: "SELECT * FROM addresses WHERE name = 'Dätwyler' "
	- Prepare the statement with PrepareStatement. A result row containig a field for each column in the table
		addresses is generated.
	- Execute the statement.
	- Retrieve data from the result set with the procedure Fetch. Each call to Fetch delievers the next row in the
		result set. When there is no more data to get when calling Fetch the variable res will be set to NoDataFound.
		After fetching a row the fields containing the data can be accessed using resultRow generated by
		PrepareStatement and the procedures FirstField, NextField, PrevField and FindField.

3. A SQL statement which needs input:
	example: "INSERT INTO addresses VALUES (?, ?)"
	- Prepare the statement with PrepareStatement. As the statement retrieves no data, resultRow is NIL.
	- Create a row to hold the input data of the parameters with procedure BindParameters. paramRow
		will now contain this row.
	- Set the values of the parameters. To access the fields representing the parameters use paramRow and the
		procedures FirstField, NextField, PrevField and FindField.
	- Execute the statement.
	- The number of rows affected by the execution of this statement can be get with procedure RowCount.

4. A SQL statement which needs input and retrieves data:
	example: "SELECT * FROM addresses WHERE name LIKE ?"
	- Prepare the statement with procedure PrepareStatement. resultRow will be created and contain a field
		for each column in the result set.
	- Create a row to hold the parameter values with BindParameters.
	- Set the values of the parameters. Access the parameters using paramRow and FirstField, NextField,
		PrevField and FindField.
	- Execute the statement.
	- Fetch the data from the result set.
**)
