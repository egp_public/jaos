модуль SVGLoader;

использует SVG, SVGColors, SVGGradients, SVGFilters, SVGRenderer, XML, XMLObjects, XMLLoader, Strings, Math, WMGraphics;

(* Constants that determine what to do if an attribute was omitted *)
конст 	OnOmittedParseDefault=0;
		OnOmittedDontChange=1;
		OnOmittedError=2;

тип
	SVGLoader=окласс
		перем
			ppi: вещ64;	(* pixels per inch of the documents *)
			state: SVG.State; (* the current state *)
			mainDocument: SVG.Document; (* the main target *)

			renderer: SVGRenderer.Renderer; (* the object doing the rendering *)
			fePrev: SVGFilters.FilterElement; (* the previous filter element *)

			sizeForced: булево; (* should the output be forced to size widthForced * heightForced? *)
			widthForced, heightForced: цел32;

		проц &New*;
		нач
			sizeForced := ложь;
		кон New;

		(* Force the output to be of size width * height *)
		проц ForceSize(width, height: цел32);
		нач
			sizeForced := истина;
			widthForced := width;
			heightForced := height;
		кон ForceSize;

		(* Load the root svg element e *)
		проц LoadRoot(e: XML.Element);
		нач
			ppi := 90; (* Default assumption *)
			нов(state);
			SVG.InitDefaultStyle(state.style);

			нов(renderer);

			LoadElement(e)
		кон LoadRoot;

		(* Get the produced document *)
		проц GetDocument():SVG.Document;
		нач
			возврат mainDocument;
		кон GetDocument;

		(* Get the width of the viewport *)
		проц GetActualWidth(): SVG.Length;
		нач
			возврат state.viewport.width;
		кон GetActualWidth;

		(* Get the height of the viewport *)
		проц GetActualHeight(): SVG.Length;
		нач
			возврат state.viewport.height;
		кон GetActualHeight;

		(* Get the diagonal size of the viewport *)
		проц GetActualDiagonal(): SVG.Length;
		нач
			возврат Math.sqrt(устарПреобразуйКБолееУзкомуЦел( GetActualWidth()*GetActualWidth()+GetActualHeight()*GetActualHeight() )) / Math.sqrt(2.0);
		кон GetActualDiagonal;

		(* Get the name of an xml element *)
		проц GetName(e: XML.Element):XML.String;
		перем name: XML.String;
		нач
			name := e.GetName();
			Strings.LowerCase(name^);
			если Strings.StartsWith2("svg:",name^) то name := Strings.Substring2(4,name^) всё;
			возврат name;
		кон GetName;

		(* Load the svg element e *)
		проц LoadElement(e: XML.Element);
		перем name: XML.String;
		нач
			name := GetName(e);
			если name^ = "svg" то LoadSVG(e)
			аесли name^ = "g" то LoadGroup(e)
			аесли name^ = "desc" то (* Ignore *)	SVG.Log("<desc />")

			аесли name^ = "defs" то LoadDefs(e)

			аесли name^ = "image" то LoadImage(e)

			аесли name^ = "rect" то LoadRect(e)
			аесли name^ = "circle" то LoadCircle(e)
			аесли name^ = "ellipse" то LoadEllipse(e)
			аесли name^ = "line" то LoadLine(e)
			аесли name^ = "polyline" то LoadPoly(e, ложь)
			аесли name^ = "polygon" то LoadPoly(e, истина)
			аесли name^ = "path" то LoadPath(e)

			иначе SVG.Log("Unknown element: "); SVG.Log(name^)
			всё
		кон LoadElement;

		(* Get the subelements of element e *)
		проц LoadSubElements(e: XML.Element);
		перем contents: XMLObjects.Enumerator; content: динамическиТипизированныйУкль;
		нач
			contents := e.GetContents();
			нцПока contents.HasMoreElements() делай
				content := contents.GetNext();
				если content суть XML.Element то LoadElement(content(XML.Element))
				иначе (* Ignore comments *)
				всё
			кц
		кон LoadSubElements;

		(* Load the <svg> element e *)
		проц LoadSVG(e: XML.Element);
		нач
			SVG.Log("<svg>");
			state.Push();

			LoadAttrLength(e, "x", state.viewport.x, 640.0, OnOmittedParseDefault, "0");
			LoadAttrLength(e, "y", state.viewport.y, 480.0, OnOmittedParseDefault, "0");
			LoadAttrLength(e, "width", state.viewport.width, 640.0, OnOmittedParseDefault, "100%");
			LoadAttrLength(e, "height", state.viewport.height, 480.0, OnOmittedParseDefault, "100%");

			если mainDocument = НУЛЬ то
				нов(state.userToWorldSpace);
				state.userToWorldSpace.SetIdentity();

				если sizeForced то
					state.userToWorldSpace := state.userToWorldSpace.Scale(widthForced / state.viewport.width,	heightForced / state.viewport.height);
					state.viewport.width := widthForced;
					state.viewport.height := heightForced;
				всё;

				LoadViewBoxAttributes(e,state.userToWorldSpace, state.viewport.width, state.viewport.height);

				state.target := SVG.NewDocument(state.viewport.width,state.viewport.height);
				renderer.FillWhite(state);
				mainDocument := state.target;
				state.transparencyUsed := ложь;

				LoadSubElements(e);
			иначе
				state.userToWorldSpace:= state.userToWorldSpace.Translate(state.viewport.x, state.viewport.y);
				LoadViewBoxAttributes(e,state.userToWorldSpace, state.viewport.width, state.viewport.height);
				LoadSubElements(e);
			всё;

			state.Pop();
			SVG.Log("</svg>")
		кон LoadSVG;

		(* Load the <g> element e *)
		проц LoadGroup(e: XML.Element);
		нач
			SVG.Log("<g>");
			state.Push();

			LoadPaintAttributes(e);
			LoadAttrTransform(e, "transform", state.userToWorldSpace);
			LoadFilterAttributeBegin(e);
			LoadSubElements(e);
			LoadFilterAttributeEnd();

			state.Pop();
			SVG.Log("</g>")
		кон LoadGroup;

		(* Load the <defs> element e *)
		проц LoadDefs(e: XML.Element);
		перем contents: XMLObjects.Enumerator; content: динамическиТипизированныйУкль;
			defEl: XML.Element;
			id, name: XML.String;
		нач
			SVG.Log("<defs>");
			contents := e.GetContents();
			нцПока contents.HasMoreElements() делай
				content := contents.GetNext();
				если content суть XML.Element то

					defEl := content(XML.Element);
					id := defEl.GetAttributeValue("id");
					если id # НУЛЬ то
						name := GetName(defEl);
						если name^ = "lineargradient" то LoadLinearGradient(defEl, id)
						аесли name^ = "radialgradient" то LoadRadialGradient(defEl, id)
						аесли name^ = "filter" то LoadFilter(defEl, id)
						всё
					всё
				иначе (* Ignore comments *)
				всё
			кц;
			SVG.Log("</defs>")
		кон LoadDefs;

		(* Load the <linearGradient> element e *)
		проц LoadLinearGradient(e: XML.Element; id: XML.String);
		перем gradient: SVGGradients.LinearGradient;
			onOmitted: цел8;
		нач
			SVG.Log("<linearGradient>");

			нов(gradient);
			если LoadGradientParentAttribute(e, gradient) то onOmitted := OnOmittedDontChange
			иначе onOmitted := OnOmittedParseDefault всё;
			LoadAttrParsed(e, "gradientUnits", gradient.gradientUnits, onOmitted, "objectBoundingBox", SVG.ParseUnits);
			LoadAttrParsed(e, "spreadMethod", gradient.spreadMethod, onOmitted, "pad", SVGGradients.ParseSpreadMethod);
			LoadAttrTransform(e, "gradientTransform", gradient.transform);

			LoadAttrLength(e, "x1", gradient.p1.x, GetActualWidth(), onOmitted, "0%");
			LoadAttrLength(e, "y1", gradient.p1.y, GetActualHeight(), onOmitted, "0%");
			LoadAttrLength(e, "x2", gradient.p2.x, GetActualWidth(), onOmitted, "100%");
			LoadAttrLength(e, "y2", gradient.p2.y, GetActualHeight(), onOmitted, "0%");

			LoadGradientStops(e,gradient);
			renderer.gradients.AddGradient(gradient,id);
			SVG.Log("</ linearGradient>");
		кон LoadLinearGradient;

		(* Load the <radialGradient> element e *)
		проц LoadRadialGradient(e: XML.Element; id: XML.String);
		перем gradient: SVGGradients.RadialGradient;
			onOmitted: цел8;
		нач
			SVG.Log("<radialGradient>");

			нов(gradient);
			если LoadGradientParentAttribute(e, gradient) то onOmitted := OnOmittedDontChange
			иначе onOmitted := OnOmittedParseDefault всё;
			LoadAttrParsed(e, "gradientUnits", gradient.gradientUnits, onOmitted, "objectBoundingBox", SVG.ParseUnits);
			LoadAttrParsed(e, "spreadMethod", gradient.spreadMethod, onOmitted, "pad", SVGGradients.ParseSpreadMethod);
			LoadAttrTransform(e, "gradientTransform", gradient.transform);

			LoadAttrLength(e, "cx", gradient.center.x, GetActualWidth(), onOmitted, "50%");
			LoadAttrLength(e, "cy", gradient.center.y, GetActualHeight(), onOmitted, "50%");
			LoadAttrLength(e, "r", gradient.radius, GetActualDiagonal(), onOmitted, "50%");
			если onOmitted=OnOmittedParseDefault то
				gradient.focal.x := gradient.center.x;
				gradient.focal.y := gradient.center.y;
			всё;
			LoadAttrLength(e, "fx", gradient.focal.x, GetActualWidth(), OnOmittedDontChange, "0");
			LoadAttrLength(e, "fy", gradient.focal.y, GetActualHeight(), OnOmittedDontChange, "0");

			LoadGradientStops(e,gradient);
			renderer.gradients.AddGradient(gradient,id);
			SVG.Log("</radialGradient>");
		кон LoadRadialGradient;

		(* Load the parent attribute of some gradient element e *)
		проц LoadGradientParentAttribute(e: XML.Element; gradient:SVGGradients.Gradient):булево;
		перем parent: SVGGradients.Gradient;
			linearGradient, linearParent: SVGGradients.LinearGradient;
			radialGradient, radialParent: SVGGradients.RadialGradient;
			href: XML.String;
		нач
			href := e.GetAttributeValue("xlink:href");
			если href # НУЛЬ то
				если SVG.ParseURI(href,href) то
					parent := renderer.gradients.GetGradient(href);
				всё
			всё;
			если parent#НУЛЬ то
				если (parent суть SVGGradients.LinearGradient) и (gradient суть SVGGradients.LinearGradient) то
					linearParent := parent(SVGGradients.LinearGradient);
					linearGradient := gradient(SVGGradients.LinearGradient);
					linearGradient.CopyLinear(linearParent);
				аесли (parent суть SVGGradients.RadialGradient) и (gradient суть SVGGradients.RadialGradient) то
					radialParent := parent(SVGGradients.RadialGradient);
					radialGradient := gradient(SVGGradients.RadialGradient);
					radialGradient.CopyRadial(radialParent);
				иначе
					gradient.Copy(parent);
				всё;
				возврат истина
			иначе
				возврат ложь
			всё
		кон LoadGradientParentAttribute;

		(* Load the <stop> elements of a gradient *)
		проц LoadGradientStops(e: XML.Element; gradient: SVGGradients.Gradient);
		перем contents: XMLObjects.Enumerator; content: динамическиТипизированныйУкль;
			foundStops: булево;
			name: XML.String;
		нач
			foundStops := ложь;
			contents := e.GetContents();
			нцПока contents.HasMoreElements() делай
				content := contents.GetNext();
				если content суть XML.Element то
					просейТип content:XML.Element делай
						name := GetName(content);
						если name^ = "stop" то
							если ~foundStops то
								(* Don't inherit the parents stop elements if this element defines stop elements *)
								foundStops := истина;
								gradient.ClearStops();
							всё;
							LoadGradientStop(content(XML.Element), gradient)
						иначе
							SVG.Warning("stop element expected, found element: ");
							SVG.Warning(name^);
						всё
					всё
				иначе (* Ignore comments *)
				всё
			кц
		кон LoadGradientStops;

		(* Load one <stop> element of a gradient *)
		проц LoadGradientStop(e: XML.Element; gradient: SVGGradients.Gradient);
		перем offset: SVG.Length;
			color: SVG.Color;
			colorStr: XML.String;
		нач
			SVG.Log("<stop />");

			LoadAttrNumber(e, "offset", offset, SVG.AllowPercentages, 1.0, OnOmittedError, "stop element omitted offset attribute");

			colorStr := LoadAttribute(e,"stop-color");
			если colorStr = НУЛЬ то
				color := SVGColors.Black;
			всё;
			если ~SVGColors.Parse(colorStr, color) то
				color := SVGColors.Black;
				SVG.Warning("stop element specifies invalid stop-color element");
				SVG.Warning(colorStr^)
			всё;

			LoadOpacity(e, "stop-opacity", color,  OnOmittedParseDefault, "1");

			gradient.AddStop(offset, color);
		кон LoadGradientStop;

		(* Load the <filter> element e *)
		проц LoadFilter(e: XML.Element; id: XML.String);
		перем filter: SVGFilters.Filter;
		нач
			SVG.Log("<filter>");

			нов(filter);

			LoadAttrLength(e, "x", filter.window.x, GetActualWidth(), OnOmittedParseDefault, "-10%");
			LoadAttrLength(e, "y", filter.window.y, GetActualHeight(), OnOmittedParseDefault, "-10%");
			LoadAttrLength(e, "width", filter.window.width, GetActualWidth(), OnOmittedParseDefault, "120%");
			LoadAttrLength(e, "height", filter.window.height, GetActualHeight(), OnOmittedParseDefault, "120%");

			LoadFilterElements(e,filter);
			renderer.filters.AddFilter(filter,id);
			SVG.Log("</ filter>");
		кон LoadFilter;

		(* Load the <fe*> subelements of a filter *)
		проц LoadFilterElements(e: XML.Element; filter: SVGFilters.Filter);
		перем contents: XMLObjects.Enumerator; content: динамическиТипизированныйУкль;
			name: XML.String;
		нач
			fePrev := НУЛЬ;

			contents := e.GetContents();
			нцПока contents.HasMoreElements() делай
				content := contents.GetNext();
				если content суть XML.Element то
					просейТип content:XML.Element делай
						name := GetName(content);
						если name^ = "feblend" то LoadFEBlend(content(XML.Element), filter)
						аесли name^ = "feoffset" то LoadFEOffset(content(XML.Element), filter)
						аесли name^ = "fecolormatrix" то LoadFEColorMatrix(content(XML.Element), filter)
						аесли name^ = "fegaussianblur" то LoadFEGaussianBlur(content(XML.Element), filter)
						аесли name^ = "femerge" то LoadFEMerge(content(XML.Element), filter)
						аесли name^ = "feflood" то LoadFEFlood(content(XML.Element), filter)
						аесли name^ = "feimage" то LoadFEImage(content(XML.Element), filter)
						аесли name^ = "fetile" то LoadFETile(content(XML.Element), filter)
						иначе
							SVG.Warning("fe* element expected, found element: ");
							SVG.Warning(name^);
						всё
					всё
				иначе (* Ignore comments *)
				всё
			кц
		кон LoadFilterElements;

		(* Load some common attributes of <fe*> elements *)
		проц LoadFECommonAttributes(e: XML.Element; fe: SVGFilters.FilterElement; filter: SVGFilters.Filter; loadIn: булево);
		перем
			result: SVG.String;
		нач
			filter.rootElement := fe;

			result := LoadAttribute(e, "result");
			если result#НУЛЬ то
				filter.AddFilterElement(fe, result);
			всё;

			если loadIn то
				LoadFilterInAttribute(e, filter, "in", fe.in)
			всё;

			fe.x := filter.window.x;
			fe.y := filter.window.y;
			fe.width := filter.window.width;
			fe.height := filter.window.height;

			LoadAttrLength(e, "x", fe.x, GetActualWidth(), OnOmittedDontChange, "");
			LoadAttrLength(e, "y", fe.y, GetActualHeight(), OnOmittedDontChange, "");
			LoadAttrLength(e, "width", fe.width, GetActualWidth(), OnOmittedDontChange, "");
			LoadAttrLength(e, "height", fe.height, GetActualHeight(), OnOmittedDontChange, "");

			fePrev := fe
		кон LoadFECommonAttributes;

		(* Load the <feBlend> element e *)
		проц LoadFEBlend(e: XML.Element; filter: SVGFilters.Filter);
		перем
			fe: SVGFilters.feBlend;
		нач
			SVG.Log("<feBlend />");
			нов(fe);

			LoadFilterInAttribute(e, filter, "in2", fe.in2);

			LoadAttrParsed(e,"mode",fe.mode,OnOmittedParseDefault,"normal",SVGFilters.ParseBlendMode);

			LoadFECommonAttributes(e,fe,filter,истина)
		кон LoadFEBlend;

		(* Load the <feOffset> element e *)
		проц LoadFEOffset(e: XML.Element; filter: SVGFilters.Filter);
		перем
			fe: SVGFilters.feOffset;
		нач
			SVG.Log("<feOffset />");
			нов(fe);

			LoadAttrLength(e, "dx", fe.dx, filter.window.width, OnOmittedParseDefault, "0");
			LoadAttrLength(e, "dy", fe.dy, filter.window.height, OnOmittedParseDefault, "0");

			LoadFECommonAttributes(e,fe,filter,истина)
		кон LoadFEOffset;

		(* Load the <feColorMatrix> element e *)
		проц LoadFEColorMatrix(e: XML.Element; filter: SVGFilters.Filter);
		перем
			fe: SVGFilters.feColorMatrix;
			values: SVG.String;
		нач
			SVG.Log("<feColorMatrix />");
			нов(fe);

			LoadAttrParsed(e, "type", fe.type, OnOmittedError, "feColorMatrix omitted type attribute", SVGFilters.ParseColorMatrixType);
			values := LoadAttribute(e, "values");
			если ~SVGFilters.ParseColorMatrixValues(values, fe.type, fe.matrix) то
				SVG.Error("feColorMatrix has invalid values attribute:");
				SVG.Error(values^)
			всё;

			LoadFECommonAttributes(e,fe,filter,истина)
		кон LoadFEColorMatrix;

		(* Load the <feGaussianBlur> element e *)
		проц LoadFEGaussianBlur(e: XML.Element; filter: SVGFilters.Filter);
		перем
			fe: SVGFilters.feGaussianBlur;
			value: SVG.String;
		нач
			SVG.Log("<feGaussianBlur />");
			нов(fe);

			value := LoadAttribute(e,"stdDeviation");
			если value = НУЛЬ то
				fe.stdDeviationX := 0;
				fe.stdDeviationY := 0;
			иначе
				SVG.ParseLengthOptional2(value, ppi, filter.window.width, fe.stdDeviationX, fe.stdDeviationY)
			всё;

			LoadFECommonAttributes(e,fe,filter,истина)
		кон LoadFEGaussianBlur;

		(* Load the <feMerge> element e *)
		проц LoadFEMerge(e: XML.Element; filter: SVGFilters.Filter);
		перем
			fe: SVGFilters.feMerge;
		нач
			SVG.Log("<feMerge>");
			нов(fe);

			LoadFEMergeNodes(e,fe,filter);

			LoadFECommonAttributes(e,fe,filter,ложь);
			SVG.Log("</feMerge>")
		кон LoadFEMerge;

		(* Load all <feMergeNodes> elements of <feMerge> element e*)
		проц LoadFEMergeNodes(e: XML.Element; fe: SVGFilters.feMerge; filter: SVGFilters.Filter);
		перем contents: XMLObjects.Enumerator; content: динамическиТипизированныйУкль;
			name: XML.String;
			in: SVGFilters.In;
			first: булево;
		нач
			fePrev := НУЛЬ;
			first := истина;
			contents := e.GetContents();
			нцПока contents.HasMoreElements() делай
				content := contents.GetNext();
				если content суть XML.Element то
					просейТип content:XML.Element делай
						name := GetName(content);
						если name^ = "femergenode" то
							SVG.Log("<feMergeNode />");
							LoadFilterInAttribute(content, filter, "in", in);
							если first то
								fe.in := in;
								first := ложь
							иначе
								fe.AddNode(in);
							всё
						иначе
							SVG.Warning("feMergeNode element expected, found element: ");
							SVG.Warning(name^);
						всё
					всё
				иначе (* Ignore comments *)
				всё
			кц;
			если first то
				SVG.Warning("empty feMerge element. feMergeNode elements expected.");
			всё
		кон LoadFEMergeNodes;

		(* Load the <feFlood> element e*)
		проц LoadFEFlood(e: XML.Element; filter: SVGFilters.Filter);
		перем
			fe: SVGFilters.feFlood;
			color: SVG.Color;
			value: SVG.String;
		нач
			SVG.Log("<feFlood />");
			нов(fe);

			value := LoadAttribute(e, "flood-color");
			если value=НУЛЬ то value :=Strings.NewString("black") всё;
			если ~SVGColors.Parse(value, color) то
				SVG.Error("expected color, found:");
				SVG.Log(value^);
			всё;
			LoadOpacity(e,"flood-opacity",color, OnOmittedParseDefault, "1");
			SVGColors.ColorToPixel(color,fe.pix);

			LoadFECommonAttributes(e,fe,filter,ложь)
		кон LoadFEFlood;

		(* Load the <feImage> element e*)
		проц LoadFEImage(e: XML.Element; filter: SVGFilters.Filter);
		перем
			fe: SVGFilters.feImage;
			href: SVG.String;
		нач
			SVG.Log("<feImage />");
			нов(fe);

			href := e.GetAttributeValue("xlink:href");
			если href = НУЛЬ то
				SVG.Error("feImage element omitted xlink:href attribute");
				возврат
			всё;

			fe.image :=WMGraphics.LoadImage(href^, истина);
			если fe.image = НУЛЬ то
				SVG.Error("feImage element specifies invalid xlink:href attribute");
				возврат
			всё;

			LoadFECommonAttributes(e,fe,filter,ложь)
		кон LoadFEImage;

		(* Load the <feTile> element e*)
		проц LoadFETile(e: XML.Element; filter: SVGFilters.Filter);
		перем
			fe: SVGFilters.feTile;
		нач
			SVG.Log("<feTile />");
			нов(fe);
			LoadFECommonAttributes(e,fe,filter,истина)
		кон LoadFETile;

		(* Load the in attribute of <fe*> element e*)
		проц LoadFilterInAttribute(e: XML.Element; filter: SVGFilters.Filter; name: массив из симв8; перем in: SVGFilters.In);
		перем
			value: XML.String;
			feIn: SVGFilters.FilterElement;
		нач
			value := LoadAttribute(e, name);
			нов(in);
			если value=НУЛЬ то
				in.type := SVGFilters.InFilterElement;
				in.fe := fePrev;
				если fePrev=НУЛЬ то
					SVG.Error("filter element defaults to previous element, but no previous element available")
				всё
			иначе
				SVGFilters.ParseIn(value, in.type);
				если in.type=SVGFilters.InFilterElement то
					feIn := filter.GetFilterElement(value);
					если feIn#НУЛЬ то
						in.fe := feIn
					иначе
						SVG.Error("Couldn't find filter element with result= ");
						SVG.Error(value^);
					всё
				всё
			всё
		кон LoadFilterInAttribute;

		(* Load the filter attribute of some element e*)
		проц LoadFilterAttribute(e: XML.Element):SVGFilters.Filter;
		перем filterAttr, name: XML.String;
			filter: SVGFilters.Filter;
		нач
			filterAttr := e.GetAttributeValue("filter");
			если filterAttr # НУЛЬ то
				если SVG.ParseURI(filterAttr, name) то
					filter := renderer.filters.GetFilter(name);

					state.transparencyUsed := истина;
				всё;
				если filter # НУЛЬ то
					возврат filter;
				иначе
					 SVG.Error("Couldn't find filter with specified id");
					 SVG.Error(name^);
					возврат НУЛЬ
				всё
			иначе
				возврат НУЛЬ
			всё
		кон LoadFilterAttribute;

		(* Load the filter attribute of some element e and prepare for rendering using this filter *)
		проц LoadFilterAttributeBegin(e: XML.Element);
		перем filter: SVGFilters.Filter;
		нач
			filter := LoadFilterAttribute(e);
			renderer.BeginFilter(filter, state);
		кон LoadFilterAttributeBegin;

		(* Cleanup rendering using some filter *)
		проц LoadFilterAttributeEnd;
		нач
			renderer.EndFilter(state)
		кон LoadFilterAttributeEnd;

		(* Load the <image> element e *)
		проц LoadImage(e: XML.Element);
		перем x, y, width, height: SVG.Length;
			image: SVG.Document;
			href: SVG.String;
		нач
			SVG.Log("<image />");

			state.Push();
			LoadAttrTransform(e, "transform", state.userToWorldSpace);

			LoadAttrLength(e, "x", x, GetActualWidth(), OnOmittedParseDefault, "0");
			LoadAttrLength(e, "y", y, GetActualHeight(), OnOmittedParseDefault, "0");
			LoadAttrLength(e, "width", width, GetActualWidth(), OnOmittedError, "image element omitted width attribute");
			LoadAttrLength(e, "height", height, GetActualHeight(), OnOmittedError, "image element omitted height attribute");
			href := e.GetAttributeValue("xlink:href");
			если href = НУЛЬ то
				SVG.Error("image element omitted xlink:href attribute");
				возврат
			всё;

			image :=WMGraphics.LoadImage(href^, истина);
			если image = НУЛЬ то
				SVG.Error("image element specifies invalid xlink:href attribute");
				возврат
			всё;

			LoadFilterAttributeBegin(e);
			renderer.RenderImage(x, y, width, height, image, state);
			LoadFilterAttributeEnd();

			state.Pop();
		кон LoadImage;

		(* Load the <rect> element e *)
		проц LoadRect(e: XML.Element);
		перем x, y, width, height, rx, ry: SVG.Length;
			rxe, rye: XML.Attribute;
		нач
			SVG.Log("<rect />");

			state.Push();
			LoadPaintAttributes(e);
			LoadAttrTransform(e, "transform", state.userToWorldSpace);

			LoadAttrLength(e, "x", x, GetActualWidth(), OnOmittedParseDefault, "0");
			LoadAttrLength(e, "y", y, GetActualHeight(), OnOmittedParseDefault, "0");
			LoadAttrLength(e, "width", width, GetActualWidth(), OnOmittedError, "rect element omitted width attribute");
			LoadAttrLength(e, "height", height, GetActualHeight(), OnOmittedError, "rect element omitted width attribute");

			rxe := e.GetAttribute("rx");
			rye := e.GetAttribute("ry");
			если (rxe=НУЛЬ) и (rxe=НУЛЬ) то
				LoadFilterAttributeBegin(e);
				renderer.RenderRect(x, y, width, height, state);
				LoadFilterAttributeEnd();
			иначе
				если rxe=НУЛЬ то
					LoadAttrLength(e, "ry", ry, GetActualDiagonal(), OnOmittedParseDefault, "0");
					rx := ry;
				аесли rye=НУЛЬ то
					LoadAttrLength(e, "rx", rx, GetActualDiagonal(), OnOmittedParseDefault, "0");
					ry := rx;
				иначе
					LoadAttrLength(e, "rx", rx, GetActualWidth(), OnOmittedParseDefault, "0");
					LoadAttrLength(e, "ry", ry, GetActualHeight(), OnOmittedParseDefault, "0");
				всё;
				LoadFilterAttributeBegin(e);
				renderer.RenderRoundedRect(x, y, width, height, rx, ry, state);
				LoadFilterAttributeEnd();
			всё;

			state.Pop();
		кон LoadRect;

		(* Load the <circle> element e *)
		проц LoadCircle(e: XML.Element);
		перем cx, cy, r: SVG.Length;
		нач
			SVG.Log("<circle />");

			state.Push();
			LoadPaintAttributes(e);
			LoadAttrTransform(e, "transform", state.userToWorldSpace);

			LoadAttrLength(e, "cx", cx, GetActualWidth(), OnOmittedParseDefault, "0");
			LoadAttrLength(e, "cy", cy, GetActualHeight(), OnOmittedParseDefault,  "0");
			LoadAttrLength(e, "r", r, GetActualDiagonal(), OnOmittedError, "circle element omitted r attribute");

			LoadFilterAttributeBegin(e);
			renderer.RenderCircle(cx, cy, r, state);
			LoadFilterAttributeEnd();

			state.Pop();
		кон LoadCircle;

		(* Load the <ellipse> element e *)
		проц LoadEllipse(e: XML.Element);
		перем cx, cy, rx, ry: SVG.Length;
		нач
			SVG.Log("<ellipse />");

			state.Push();
			LoadPaintAttributes(e);
			LoadAttrTransform(e, "transform", state.userToWorldSpace);

			LoadAttrLength(e, "cx", cx, GetActualWidth(), OnOmittedParseDefault, "0");
			LoadAttrLength(e, "cy", cy, GetActualHeight(), OnOmittedParseDefault, "0");
			LoadAttrLength(e, "rx", rx, GetActualWidth(), OnOmittedError, "ellipse element omitted rx attribute");
			LoadAttrLength(e, "ry", ry, GetActualHeight(), OnOmittedError, "ellipse element omitted ry attribute");

			LoadFilterAttributeBegin(e);
			renderer.RenderEllipse(cx, cy, rx, ry, state);
			LoadFilterAttributeEnd();

			state.Pop();
		кон LoadEllipse;

		(* Load the <line> element e *)
		проц LoadLine(e: XML.Element);
		перем x1, y1, x2, y2: SVG.Length;
		нач
			SVG.Log("<line />");

			state.Push();
			LoadPaintAttributes(e);
			LoadAttrTransform(e, "transform", state.userToWorldSpace);

			LoadAttrLength(e, "x1", x1, GetActualWidth(), OnOmittedParseDefault, "0");
			LoadAttrLength(e, "y1", y1, GetActualHeight(), OnOmittedParseDefault, "0");
			LoadAttrLength(e, "x2", x2, GetActualWidth(), OnOmittedParseDefault, "0");
			LoadAttrLength(e, "y2", y2, GetActualHeight(), OnOmittedParseDefault, "0");

			LoadFilterAttributeBegin(e);
			renderer.RenderLine(x1, y1, x2, y2, state);
			LoadFilterAttributeEnd();

			state.Pop();
		кон LoadLine;

		(* Load the <polyline> or <polygon> element e *)
		проц LoadPoly(e: XML.Element; closed: булево);
		перем points: SVG.String;
		нач
			если closed то SVG.Log("<polygon />")
			иначе SVG.Log("<polyline />") всё;

			state.Push();
			LoadPaintAttributes(e);
			LoadAttrTransform(e, "transform", state.userToWorldSpace);

			points := e.GetAttributeValue("points");
			если points=НУЛЬ то
				если closed то SVG.Error("polygon element omitted points attribute")
				иначе SVG.Error("polyline element omitted points attribute") всё;
				возврат
			иначе
				LoadFilterAttributeBegin(e);
				renderer.RenderPoly(points, closed, state);
				LoadFilterAttributeEnd();
			всё;

			state.Pop();
		кон LoadPoly;

		(* Load the <path> element e *)
		проц LoadPath(e: XML.Element);
		перем d: SVG.String;
		нач
			SVG.Log("<path />");

			state.Push();
			LoadPaintAttributes(e);
			LoadAttrTransform(e, "transform", state.userToWorldSpace);

			d := e.GetAttributeValue("d");
			если d=НУЛЬ то
				SVG.Error("path element omitted d attribute");
			иначе
				LoadFilterAttributeBegin(e);
				renderer.RenderPath(d, state);
				LoadFilterAttributeEnd();
			всё;

			state.Pop();
		кон LoadPath;

		(* Load the paint attributes of element e *)
		проц LoadPaintAttributes(e: XML.Element);
		перем
			value: XML.String;
		нач
			value := LoadAttribute(e, "fill");
			если value#НУЛЬ то SVG.ParsePaint(value,state.style.fill) всё;
			value := LoadAttribute(e, "stroke");
			если value#НУЛЬ то SVG.ParsePaint(value, state.style.stroke) всё;
			value := LoadAttribute(e, "stroke-width");
			если value#НУЛЬ то SVG.ParseLength(value, ppi, GetActualDiagonal(), state.style.strokeWidth) всё;

			LoadOpacity(e, "fill-opacity", state.style.fill.color,  OnOmittedParseDefault, "1");
			LoadOpacity(e, "stroke-opacity", state.style.stroke.color,  OnOmittedParseDefault, "1");

		кон LoadPaintAttributes;

		(* Load the viewBox and preserceAspectRatio attributes of element e *)
		проц LoadViewBoxAttributes(e: XML.Element; перем transform: SVG.Transform; width0, height0: SVG.Length);
		перем viewBox, preserveAR: XML.String;
			minx, miny, width, height, ratioX, ratioY: SVG.Length;
			xAlign, yAlign: цел32;
			meet: булево;
		нач
			viewBox := e.GetAttributeValue("viewBox");
			если viewBox # НУЛЬ то
				SVG.ParseViewBox(viewBox, minx, miny, width, height);
				ratioX := width0/width;
				ratioY := height0/height;

				preserveAR := e.GetAttributeValue("preserveAspectRatio");
				если (preserveAR = НУЛЬ) или (Strings.StartsWith2("none",preserveAR^)) то
					xAlign := -1;
					yAlign := -1;
				иначе
					SVG.ParsePreserveAspect(preserveAR, xAlign, yAlign, meet);
					если meet = (width0/height0 < width/height) то
						ratioY := ratioX
					иначе ratioX := ratioY всё
				всё;
				transform := transform.TransformBy(ratioX, 0.0, 0.0, ratioY,
					(1+xAlign)*width0/2-(minx+(1+xAlign)*width/2)*ratioX,
					(1+yAlign)*height0/2 -(miny+(1+yAlign)*height/2)*ratioY)
			всё
		кон LoadViewBoxAttributes;

		(* Load some numeric attribute of element e *)
		проц LoadAttrNumber(e: XML.Element; name: массив из симв8; перем number: SVG.Number;
			percentageAllowed: булево; percent100: SVG.Number; onOmitted: цел8; default: массив из симв8);
		перем value: XML.String;
		нач
			value := LoadAttribute(e,name);
			если value = НУЛЬ то
				просей onOmitted из
				OnOmittedParseDefault:
					value := Strings.NewString(default);
					SVG.ParseNumber(value, number, percentageAllowed, percent100)
				| OnOmittedDontChange:
				| OnOmittedError:
					SVG.Error(default);
				всё
			иначе
				SVG.ParseNumber(value, number, percentageAllowed, percent100)
			всё
		кон LoadAttrNumber;

		(* Load some attribute of element e of type length *)
		проц LoadAttrLength(e: XML.Element; name: массив из симв8; перем length: SVG.Length;
			percent100: SVG.Number; onOmitted: цел8; default: массив из симв8);
		перем value: XML.String;
		нач
			value := LoadAttribute(e,name);
			если value = НУЛЬ то
				просей onOmitted из
				OnOmittedParseDefault:
					value := Strings.NewString(default);
					SVG.ParseLength(value, ppi, percent100, length)
				| OnOmittedDontChange:
				| OnOmittedError:
					SVG.Error(default);
				всё
			иначе
				SVG.ParseLength(value, ppi, percent100, length)
			всё
		кон LoadAttrLength;

		(* Load the transform attribute of element e *)
		проц LoadAttrTransform(e: XML.Element; name: массив из симв8; перем transform: SVG.Transform);
		перем value: XML.String;
		нач
			value := LoadAttribute(e,name);
			если value # НУЛЬ то
				SVG.ParseTransformList(value, transform)
			всё
		кон LoadAttrTransform;

		(* Load some attribute of element e using a custom parse procedure*)
		проц LoadAttrParsed(e: XML.Element; name: массив из симв8; перем parsed: цел8;
			onOmitted: цел8; default: массив из симв8;
			parser: проц(value: XML.String; перем parsed: цел8));
		перем value: XML.String;
		нач
			value := LoadAttribute(e,name);
			если value = НУЛЬ то
				просей onOmitted из
				OnOmittedParseDefault:
					value := Strings.NewString(default);
					parser(value,parsed)
				| OnOmittedDontChange:
				| OnOmittedError:
					SVG.Error(default);
				всё
			иначе
				parser(value,parsed)
			всё
		кон LoadAttrParsed;

		(* Load some attribute of element e *)
		проц LoadAttribute(e: XML.Element; name: массив из симв8): XML.String;
		перем value: XML.String;
		нач
			value := e.GetAttributeValue(name);
			если value=НУЛЬ то
				value := e.GetAttributeValue("style");
				если value=НУЛЬ то возврат НУЛЬ всё;
				value := SVG.ParseStyle(value,name);
			всё;
			возврат value
		кон LoadAttribute;

		(* Load some opacity attribute of element e *)
		проц LoadOpacity(e: XML.Element; name: массив из симв8; перем color: SVG.Color; onOmitted: цел8; default: массив из симв8);
		перем opacity: SVG.Length;
			r,g,b,a: цел16;
		нач
			LoadAttrNumber(e,name, opacity,SVG.DisallowPercentages, 0.0,  onOmitted, default);
			если opacity#1 то
				SVGColors.Split(color, r,g,b,a);
				a:=устарПреобразуйКБолееУзкомуЦел(округлиВниз(255*opacity));
				SVGColors.Unsplit(color, r,g,b,a);

				state.transparencyUsed := истина;
			всё;
		кон LoadOpacity;

	кон SVGLoader;

(* Load some svg file *)
проц LoadSVG*(svgName: массив из симв8): SVG.Document;
перем
	xml: XML.Document;
	root: XML.Element;
нач
	xml := XMLLoader.LoadXML(svgName);
	если xml = НУЛЬ то возврат НУЛЬ всё;

	root := xml.GetRoot();
	если root = НУЛЬ то возврат НУЛЬ всё;

	возврат LoadSVGEmbedded(root)
кон LoadSVG;

(* Load some svg elements in parsed xml form *)
проц LoadSVGEmbedded*(root: XML.Element): SVG.Document;
перем loader: SVGLoader;
нач
	нов(loader);
	loader.LoadRoot(root);
	возврат loader.GetDocument()
кон LoadSVGEmbedded;

(* Load some svg file and force the resulting size to be width * height *)
проц LoadSizedSVG*(svgName: массив из симв8; width, height: цел32): SVG.Document;
перем
	xml: XML.Document;
	root: XML.Element;
	loader: SVGLoader;
нач
	xml := XMLLoader.LoadXML(svgName);
	если xml = НУЛЬ то возврат НУЛЬ всё;

	root := xml.GetRoot();
	если root = НУЛЬ то возврат НУЛЬ всё;

	нов(loader);
	loader.ForceSize(width, height);
	loader.LoadRoot(root);
	возврат loader.GetDocument()
кон LoadSizedSVG;

кон SVGLoader.
