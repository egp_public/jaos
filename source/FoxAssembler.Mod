модуль FoxAssembler;   (**  AUTHOR "fof"; PURPOSE "Oberon Assembler: Generic Part";  **)
(* (c) fof ETH Zürich, 2009 *)

использует Потоки, Strings, Diagnostics,D := Debugging, Commands, BinaryCode := FoxBinaryCode, SyntaxTree := FoxSyntaxTree, Global := FoxGlobal,
	IntermediateCode := FoxIntermediateCode, Sections := FoxSections, Scanner := FoxScanner, Basic := FoxBasic, ObjectFile,
	ПЭК := ПереводыЭлементовКода, УДП := LisУтилитыДляПереводаКода, UCS32;

конст
	Trace* = ложь;   (* debugging output *)

	MaxOperands* = 3;

	(*
		currently there is conceptual support for one-pass assembly with a fixup mechanism for section-local references
		disadvantages of one-pass assembly:
			- expressions with labels would not work
			- fixup mechanism complicated and not generic

	*)
	MaxPasses* = 2;

	ConstantInteger* = 0;
	ConstantFloat* = 1;
	Fixup* = 2;
	Offset* = 3;
	ConstantIntegerOrOffset* = {ConstantInteger, Offset};

тип
	OperandString=массив 256 из симв8;
	Position= Basic.Position;

	FixupElement=укль на запись
		fixup: BinaryCode.Fixup; next: FixupElement;
	кон;

	NamedLabel*= окласс
	перем
		section: IntermediateCode.Section;
		offset, displacement: BinaryCode.Unit; (* in contrast to offset, displacement will be reset each round of assembling. This is to make sure that GetFixup generates the right displacement in the fixup *)
		name-: Scanner.СтрокаИдентификатора;
		nextNamedLabel-: NamedLabel;
		fixupList: FixupElement;

		проц &InitNamedLabel(l0section: IntermediateCode.Section; конст l1name: массив из симв8);
		нач
			fixupList := НУЛЬ;
			сам.offset := 0; (* must be zero to be able to track local displacement *)
			сам.section := l0section;
			копируйСтрокуДо0(l1name,сам.name);
			nextNamedLabel := НУЛЬ;
		кон InitNamedLabel;

		проц GetFixup(): BinaryCode.Fixup;
		перем fixup: BinaryCode.Fixup; element: FixupElement; identifier: ObjectFile.Identifier;
		нач
			identifier.name := section.name;
			fixup := BinaryCode.NewFixup(BinaryCode.Absolute,0,identifier,0,displacement,0,НУЛЬ);
			нов(element); element.fixup := fixup; element.next := fixupList; fixupList := element;
			возврат fixup;
		кон GetFixup;

		проц ResetDisplacements;
		перем element: FixupElement;
		нач
			displacement := 0;
			element := fixupList;
			нцПока element # НУЛЬ делай
				element.fixup.SetSymbol(section.name,0,0,0);
				element := element.next;
			кц;
		кон ResetDisplacements;

		проц SetOffset*(ofs: BinaryCode.Unit);
		перем element: FixupElement;
		нач
			сам.offset := ofs;
			displacement := ofs;
			element := fixupList;
			нцПока element # НУЛЬ делай
				element.fixup.SetSymbol(section.name,0,0,element.fixup.displacement (* must be here to take into account modifications of code emission *) +displacement);
				element := element.next;
			кц;
		кон SetOffset;

	кон NamedLabel;

	NamedLabelList*=окласс
	перем first-,last-: NamedLabel;

		проц & InitNamedLabelList;
		нач first := НУЛЬ; last := НУЛЬ
		кон InitNamedLabelList;

		проц Add*(n: NamedLabel);
		нач
			если first = НУЛЬ то first := n иначе last.nextNamedLabel := n; last.nextNamedLabel := n;  всё; last := n;
		кон Add;

		проц ResetDisplacements;
		перем label: NamedLabel;
		нач
			label := first;
			нцПока label # НУЛЬ делай label.ResetDisplacements; label := label.nextNamedLabel кц;
		кон ResetDisplacements;

		проц Find*(конст name: массив из симв8): NamedLabel;
		перем label: NamedLabel;
		нач
			label := first;
			нцПока (label # НУЛЬ) и (label.name # name)  делай
				label := label.nextNamedLabel;
			кц;
			возврат label
		кон Find;

	кон NamedLabelList;

	Result*= запись
		type*: цел16; (* ConstantInteger, ConstantFloat, Fixup, Offset *)
		sizeInBits*: размерМЗ;
		value*: цел64;
		valueR*: вещ64;
		fixup*: BinaryCode.Fixup;
	кон;

	NamedResult*=укль на запись (Result)
		name: Scanner.СтрокаИдентификатора;
		nextResult: NamedResult;
	кон;

	NamedResultList*=окласс
	перем first, last: NamedResult; number: цел32;

		проц & InitNamedResultList;
		нач first := НУЛЬ; last := НУЛЬ; number := 0;
		кон InitNamedResultList;

		проц Add*(n: NamedResult);
		нач
			если first = НУЛЬ то first := n иначе last.nextResult := n всё; last := n; увел(number);
		кон Add;

		проц Find*(конст name: массив из симв8): NamedResult;
		перем result: NamedResult;
		нач
			result := first;
			нцПока (result # НУЛЬ) и (result.name # name)  делай
				result := result.nextResult;
			кц;
			возврат result
		кон Find;

	кон NamedResultList;

	Assembler*= окласс
	перем
		diagnostics: Diagnostics.Diagnostics;
		error-: булево;
		errorPosition-: Position;
		token-: Scanner.Лексема;
		scanner: Scanner.AssemblerScanner;
		orgOffset: цел32;
		section-: IntermediateCode.Section;
		code-: BinaryCode.Section;
		labels: NamedLabelList;
		results: NamedResultList;
		scope: SyntaxTree.ВнутренняяОбластьВидимости;
		module: Sections.Module;
		pass-: цел32;

		проц &Init*(l2diagnostics: Diagnostics.Diagnostics);
		нач
			сам.diagnostics := l2diagnostics; errorPosition := Basic.invalidPosition; orgOffset := 0;
		кон Init;

		проц SetContext(конст context: Scanner.Context);
		нач
			scanner.SetContext(context); NextToken;
		кон SetContext;

		проц Error*(pos: SyntaxTree.МестоВ_тексте; конст msg: массив из симв8);
		нач
			error := истина;
			Basic.Error(diagnostics, scanner.source^,pos, msg);
		кон Error;

		проц ErrorSS*(pos: SyntaxTree.МестоВ_тексте; конст s1,s2: массив из симв8);
		перем msg: Basic.MessageString;
		нач копируйСтрокуДо0(s1,msg); Strings.Append(msg,s2); Error(pos, msg);
		кон ErrorSS;

		проц NextToken*;
		нач error := error или ~scanner.ДочитайЛексему(token); errorPosition := token.местоВТексте;
		кон NextToken;

		проц ThisSymbol*(x: Scanner.ВидЛексемы): булево;
		нач
			если ~error и (token.видЛексемы = x) то NextToken; возврат истина иначе возврат ложь всё;
		кон ThisSymbol;

		проц GetIdentifier*(перем pos: Position; перем identifier: массив из симв8): булево;
		нач
			pos := token.местоВТексте;
			если token.видЛексемы # Scanner.Идентификатор то возврат ложь
			иначе копируйСтрокуДо0(token.строкаИдентификатора,identifier); NextToken; возврат истина
			всё;
		кон GetIdentifier;

		проц ThisIdentifier*(конст this: массив из симв8): булево;
		нач
			если ~error и (token.видЛексемы = Scanner.Идентификатор) и (this = token.строкаИдентификатора) то NextToken; возврат истина иначе возврат ложь всё;
		кон ThisIdentifier;

		проц ExpectIdentifier*(перем pos: Position; перем identifier: массив из симв8): булево;
		нач
			если ~GetIdentifier(pos,identifier)то Error(errorPosition,"identifier expected"); возврат ложь
			иначе возврат истина
			всё;
		кон ExpectIdentifier;

		проц ExpectSymbol*(x: Scanner.ВидЛексемы): булево;
		перем s: Basic.MessageString;
		нач
			если ThisSymbol(x) то возврат истина
			иначе
				s := "expected token "; Strings.Append(s,Scanner.symbols[x]); Strings.Append(s," but got "); Strings.Append(s,Scanner.symbols[token.видЛексемы]);
				Error(errorPosition,s);возврат ложь
			всё;
		кон ExpectSymbol;

		проц ExpectConstantInteger*(перем x: Result; critical: булево): булево;
		перем result: Result;
		нач
			если ~Expression(result,critical) или (result.type # ConstantInteger) то
				result.value := 0;
				если critical то Error(errorPosition,"constant integer expected") всё;
				возврат ~critical
			иначе возврат истина
			всё
		кон ExpectConstantInteger;

		проц Section;
		перем sectionType: Scanner.СтрокаИдентификатора; pos: Position;
		нач
			если ExpectSymbol(Scanner.ЗнакПрепинанияТочка) то
				если ExpectIdentifier(pos,sectionType) то
					если sectionType = "data" то
						если Trace то D.String("data section"); D.Ln всё;
						(*! generate section here, if allowed *)
					аесли sectionType = "code" то
						если Trace то D.String("code section"); D.Ln всё;
						(*! generate section here, if allowed *)
					иначе Error(pos,"expected data or code");
					всё;
				всё;
			всё;
		кон Section;

		проц DefineLabel(pos: Position; конст name: массив из симв8);
		перем label: NamedLabel;
		нач
			если Trace то D.String("define label: "); D.String(name); D.Ln всё;
			если labels.Find(name) # НУЛЬ то
				Error(pos,"multiply declared identifier")
			иначе
				нов(label,section,name);
				labels.Add(label);
				утв(labels.Find(name) =label);
			всё;
		кон DefineLabel;

		проц SetLabel(pos: Position; конст name: массив из симв8);
		перем label: NamedLabel;
		нач
			если Trace то D.String("set label: "); D.String(name); D.String(" "); D.Int(code.pc,1); D.Ln всё;
			label := labels.Find(name);
			label.SetOffset(code.pc);
		кон SetLabel;

		проц CopyResult(конст from: Result; перем to: Result);
		нач
			to.type := from.type;
			to.sizeInBits := from.sizeInBits;
			to.value := from.value;
			to.valueR := from.valueR;
			to.fixup := from.fixup;
		кон CopyResult;

		проц DefineResult(pos: Position; конст name: массив из симв8; конст r: Result);
		перем result: NamedResult;
		нач
			если Trace то D.String("define result: "); D.String(name); D.Ln всё;
			если results.Find(name) # НУЛЬ то
				Error(pos,"multiply declared identifier")
			иначе
				нов(result); копируйСтрокуДо0(name,result.name);
				CopyResult(r,result^);
				results.Add(result);
				утв(results.Find(name) =result);
			всё;
		кон DefineResult;

		проц SetResult(конст name: массив из симв8; конст r: Result);
		перем result: NamedResult;
		нач
			если Trace то D.String("define result: "); D.String(name); D.Ln всё;
			result := results.Find(name);
			CopyResult(r,result^);
		кон SetResult;

		проц SymbolInScope(конст ident: массив из симв8): SyntaxTree.ОбъявлениеИменованнойСущности;
		перем sym: SyntaxTree.ОбъявлениеИменованнойСущности; localScope: SyntaxTree.ВнутренняяОбластьВидимости;  identifier: SyntaxTree.ИдентификаторВ_исходнике;
			этотЖеМодульЛи¿ : булево; квоВидимыхСегментов: размерМЗ;
		конст l3Trace=ложь;
		нач
			если scope = НУЛЬ то возврат НУЛЬ всё;
			localScope := scope;
			identifier := SyntaxTree.ЯвиИдентификаторВ_исходнике(ident);
			если l3Trace то D.String("GetScopeSymbol:"); D.String(ident); D.Ln; всё;
			нцПока (sym = НУЛЬ) и (localScope # НУЛЬ) делай
				sym := localScope.НайдиОбъявление(identifier);
				localScope := localScope.охватывающаяОбластьВидимости
			кц;
			квоВидимыхСегментов := 1;

			если (sym # НУЛЬ) и (sym суть SyntaxTree.ОбъявлениеИмпортаМодуля)  то
				NextToken;
				если ExpectSymbol(Scanner.ЗнакПрепинанияТочка) и (token.видЛексемы = Scanner.Идентификатор) то
					увел(квоВидимыхСегментов);
					identifier := SyntaxTree.ЯвиИдентификаторВ_исходнике(token.строкаИдентификатора);
					если l3Trace то D.String("GetScopeSymbol  :"); D.String(token.строкаИдентификатора); D.Ln; всё;
					localScope := sym(SyntaxTree.ОбъявлениеИмпортаМодуля).деревоМодуля.внутренняяОбластьВидимостиМодуля;
					sym := НУЛЬ;
					нцПока (sym = НУЛЬ) и (localScope # НУЛЬ) делай
						sym := localScope.НайдиОбъявление(identifier);
						если (sym # НУЛЬ) и (sym.праваДоступа * SyntaxTree.ВиденВсем = {}) то sym := НУЛЬ всё;
						(* это очевидный бред, но трогать страшно *)
						localScope := localScope.охватывающаяОбластьВидимости
					кц;
				иначе возврат НУЛЬ
				всё;
			всё;
			если l3Trace то если sym = НУЛЬ то D.String("not found") иначе D.String("found"); всё; D.Ln; всё;
			если (sym # НУЛЬ) и Global.СимволБоитсяЛиМашКода¿(sym, квоВидимыхСегментов) то
				Error(errorPosition,"нельзя ссылаться из машкода на символ, у которого есть перевод") всё;
			возврат sym
		кон SymbolInScope;

		проц ConstantSymbol(pos: Position; constant: SyntaxTree.ОбъявлениеКонстанты; перем result: Result): булево;
		нач
			если constant.Тип.резОсмысления суть SyntaxTree.ВыражˉтипИзРодаЛитер то
				result.value := кодСимв8(constant.выражениеˉзначение.резОсмысления(SyntaxTree.Значение˛известноеВоВремяКомпиляции˛символ).значение);
				result.valueR := result.value;
				result.type := ConstantInteger;
			аесли constant.Тип.резОсмысления суть SyntaxTree.ВыражˉтипИзРодаЧиселЦел то
				result.value := constant.выражениеˉзначение.резОсмысления(SyntaxTree.Значение˛известноеВоВремяКомпиляции˛целое).значение;
				result.valueR := result.value;
				result.type := ConstantInteger;
			аесли constant.Тип.резОсмысления суть SyntaxTree.ВыражˉтипИзРодаЧиселВещ то
				result.valueR := constant.выражениеˉзначение.резОсмысления(SyntaxTree.Значение˛известноеВоВремяКомпиляции˛вещ).значение;
				result.type := ConstantFloat;
			иначе
				Error(pos,"incompatible constant");
				возврат ложь;
			всё;
			result.sizeInBits := module.system.SizeOf(constant.Тип);
			возврат истина
		кон ConstantSymbol;

		проц GetFingerprint(symbol: SyntaxTree.ОбъявлениеИменованнойСущности): Basic.Fingerprint;
		нач
			если (symbol # НУЛЬ) то возврат symbol.контрольнаяСумма.Мелкая всё;
		кон GetFingerprint;

		проц NonConstantSymbol(pos: Position; symbol: SyntaxTree.ОбъявлениеИменованнойСущности; перем result: Result): булево;
		перем
			name: Basic.SegmentedName; moduleScope: SyntaxTree.ВнутренняяОбластьВидимости; 
			fixupPatternList: ObjectFile.FixupPatterns; identifier: ObjectFile.Identifier;
		нач
			если scope = НУЛЬ то возврат ложь всё;
			moduleScope := scope.модульˉвладелец.внутренняяОбластьВидимостиМодуля;
			Global.GetSymbolSegmentedName(symbol,name);
			identifier.name := name;
			identifier.fingerprint := GetFingerprint(symbol);

			если symbol.областьВидимостиГдеОбъявлено суть SyntaxTree.ВнутренняяОбластьВидимостиМодуля то (* symbol in module scope *)
				если symbol суть SyntaxTree.ОбъявлениеПеременной то (* global variable *)
					result.type := Fixup;
					result.sizeInBits := module.system.SizeOf(symbol.Тип);

					(* generic fixup pattern list for generic implementation of data instruction etc. -- otherwise replaced during encoding *)
					нов(fixupPatternList, 1);
					fixupPatternList[0].bits := result.sizeInBits;
					fixupPatternList[0].offset := 0;
					result.fixup := BinaryCode.NewFixup(BinaryCode.Absolute, 0, identifier, 0, 0, 0, fixupPatternList);

				аесли symbol суть SyntaxTree.ОбъявлениеПроцедуры то (* procedure *)
					если symbol(SyntaxTree.ОбъявлениеПроцедуры).встраиваемаяЛи¿ то
						Error(pos,"forbidden reference to inline procedure"); возврат ложь
					иначе
						result.type := Fixup;
						result.sizeInBits := module.system.SizeOf(symbol.Тип);
						(* generic fixup pattern list for generic implementation of data instruction etc. -- otherwise replaced during encoding *)
						нов(fixupPatternList, 1);
						fixupPatternList[0].bits := result.sizeInBits;
						fixupPatternList[0].offset := 0;
						result.fixup := BinaryCode.NewFixup(BinaryCode.Absolute, 0, identifier, 0, 0, 0, fixupPatternList);
					всё;
				иначе СТОП(100);
				всё;
			аесли symbol.областьВидимостиГдеОбъявлено суть SyntaxTree.ВнутренняяОбластьВидимостиПроцедуры то (* symbol in procedure (local) scope *)
				если symbol.областьВидимостиГдеОбъявлено # scope то
					Error(pos,"local symbol not in current scope");
				иначе
					возврат ложь;
					если (symbol суть SyntaxTree.ОбъявлениеПеременной) или (symbol суть SyntaxTree.ОбъявлениеПараметра) то
						result.type := Offset;
						result.value := symbol.смещение˛бит DIV module.system.dataUnit;
						утв(symbol.смещение˛бит остОтДеленияНа module.system.dataUnit = 0);
						result.sizeInBits := module.system.SizeOf(symbol.Тип);
					иначе Error(pos,"forbidden symbol in local scope");
					всё;
				всё
			аесли symbol.областьВидимостиГдеОбъявлено суть SyntaxTree.ВнутренняяОбластьВидимостиТипаИзРодаЗаписей то (* symbol in record scope *)
			иначе Error(pos,"symbol in forbidden scope"); возврат ложь
			всё;
			возврат истина
		кон NonConstantSymbol;

		проц GetNonConstant*(pos: Position; конст ident: массив из симв8; перем result: Result): булево;
		перем symbol: SyntaxTree.ОбъявлениеИменованнойСущности; namedLabel: NamedLabel;
			name: Basic.SegmentedName;fixupPatternList: ObjectFile.FixupPatterns;
			string: массив 256 из симв8;
			identifier: ObjectFile.Identifier;
		нач
			namedLabel := labels.Find(ident);
			если (namedLabel # НУЛЬ) то
				result.type := Fixup;
				result.fixup := namedLabel.GetFixup();
				возврат истина
			всё;
			если ident[0] = "@" то
				result.type := Fixup;
				копируйСтрокуДо0(ident, string);
				Strings.Delete(string,0,1);
				Basic.ToSegmentedName(string, name);
				result.sizeInBits := 32;
				нов(fixupPatternList, 1);
				fixupPatternList[0].bits := result.sizeInBits;
				fixupPatternList[0].offset := 0;
				identifier.name := name;
				identifier.fingerprint := 0;
				result.fixup := BinaryCode.NewFixup(BinaryCode.Absolute, 0, identifier, 0, 0, 0, fixupPatternList);
				возврат истина
			всё;
			symbol := SymbolInScope(ident);
			если symbol = НУЛЬ то возврат ложь
			аесли symbol суть SyntaxTree.ОбъявлениеКонстанты то возврат ложь
			иначе возврат NonConstantSymbol(pos,symbol,result)
			всё;
		кон GetNonConstant;

		проц LocalOffset(pos: Position; symbol: SyntaxTree.ОбъявлениеИменованнойСущности; перем result: Result): булево;
		нач
			если symbol.областьВидимостиГдеОбъявлено суть SyntaxTree.ВнутренняяОбластьВидимостиПроцедуры то (* symbol in procedure (local) scope *)
				если symbol.областьВидимостиГдеОбъявлено = scope то
					если (symbol суть SyntaxTree.ОбъявлениеПеременной) или (symbol суть SyntaxTree.ОбъявлениеПараметра) то
						result.type := ConstantInteger;
						result.value := symbol.смещение˛бит DIV module.system.dataUnit;
						утв(symbol.смещение˛бит остОтДеленияНа module.system.dataUnit = 0);
						result.sizeInBits := module.system.SizeOf(symbol.Тип);
						возврат истина
					всё;
				всё;
			всё;
			возврат ложь
		кон LocalOffset;

		проц GetConstant*(pos: Position; конст ident: массив из симв8; перем result: Result): булево;
		перем symbol: SyntaxTree.ОбъявлениеИменованнойСущности; namedResult: NamedResult;
		нач
			namedResult := results.Find(ident);
			если namedResult # НУЛЬ то CopyResult(namedResult^,result); возврат истина всё;
			symbol := SymbolInScope(ident);
			если symbol = НУЛЬ то возврат ложь
			аесли symbol суть SyntaxTree.ОбъявлениеКонстанты то возврат ConstantSymbol(pos,symbol(SyntaxTree.ОбъявлениеКонстанты),result)
			аесли LocalOffset(pos,symbol,result) то возврат истина
			иначе возврат ложь
			всё;
		кон GetConstant;

		проц Factor (перем x: Result; critical: булево): булево;
		перем label: NamedLabel; identifier: Scanner.СтрокаИдентификатора; pos: Position;
		нач
			если token.видЛексемы = Scanner.Число то
				(* ASSERT(symbol.numberType = Scanner.Integer); *)
				x.value := token.integer;
				x.type := ConstantInteger;
				NextToken;
				возврат истина;
			аесли ThisSymbol(Scanner.PC) то (* pc IN units ! *)
				x.value := code.pc;
				x.type := ConstantInteger; (* TODO: should it be 'x.type := Offset'? *)
				возврат истина;
			аесли ThisSymbol(Scanner.PCOffset) то
				x.value := code.pc-orgOffset;
				x.type := ConstantInteger; (* TODO: should it be 'x.type := Offset'? *)
				возврат истина;
			аесли GetIdentifier(pos,identifier) то
				label := labels.Find (identifier);
				если label # НУЛЬ то
					x.value := label.offset;
					x.type := Offset;
					(*! deal with fixups ? / enter fixup ? *)
					возврат истина;
				аесли GetConstant(errorPosition, identifier,x) то возврат истина
				аесли ~critical и (pass # MaxPasses) то
					x.value := 0; x.type := ConstantInteger; возврат истина
				иначе Error(pos,"undefined symbol"); возврат ложь
				всё;
			аесли ThisSymbol(Scanner.ОткрывающаяКруглаяСкобка)  то
				возврат Expression (x, critical) и ExpectSymbol(Scanner.ЗакрывающаяКруглаяСкобка);
			всё;
			возврат ложь
		кон Factor;

		(* term = Factor { ( "*" | "/" | "%" ) Factor } *)
		проц Term (перем x: Result; critical: булево): булево;
		перем y: Result; op : цел32;
		нач
			если Factor (x, critical) то
				нцПока (token.видЛексемы = Scanner.ЗнакУмножить) или (token.видЛексемы = Scanner.Div) или (token.видЛексемы = Scanner.Mod) делай
					op := token.видЛексемы; NextToken;
					если Factor (y, critical) то
						если (x.type в ConstantIntegerOrOffset) и (y.type в ConstantIntegerOrOffset) то
							если op = Scanner.ЗнакУмножить то x.value := x.value * y.value
							аесли op = Scanner.Div то x.value := x.value DIV y.value
							иначе x.value := x.value остОтДеленияНа y.value
							всё;
						аесли (x.type = ConstantFloat) или (y.type = ConstantFloat) то
							если op = Scanner.ЗнакУмножить то x.valueR := x.valueR * y.valueR
							аесли op = Scanner.Div то x.valueR := x.valueR / y.valueR
							иначе возврат ложь
							всё;
						иначе возврат ложь
						всё;
					иначе
						возврат ложь;
					всё;
				кц;
				возврат истина;
			иначе
				возврат ложь;
			всё;
		кон Term;

		(* Expression = [ "-" | "+" | "~" ] Term { ( "+" | "-" ) Term } *)
		проц Expression*(перем x: Result; critical: булево): булево;
		перем y: Result; op : цел32;
		нач
			op := token.видЛексемы;
			если ThisSymbol(Scanner.ЗнакМинус) то
				если Term (x, critical) то
					если x.type в ConstantIntegerOrOffset то
						x.value := -x.value; x.valueR := x.value
					аесли x.type = ConstantFloat то
						x.valueR := -x.valueR
					иначе
						возврат ложь
					всё;
				иначе
					возврат ложь;
				всё;
			аесли ThisSymbol(Scanner.ЗнакПлюс) то
				если ~Term (x, critical) то возврат ложь
				иначе
					возврат (x.type в ConstantIntegerOrOffset) или (x.type = ConstantFloat)
				всё;
			аесли ThisSymbol(Scanner.Не) то
				если Term (x, critical) то
					если x.type в ConstantIntegerOrOffset то
						x.value := -x.value-1; x.valueR := x.value
					иначе
						возврат ложь
					всё
				всё;
			аесли ~Term (x, critical) то возврат ложь
			всё;
			нцПока (token.видЛексемы = Scanner.ЗнакПлюс) или (token.видЛексемы = Scanner.ЗнакМинус) делай
				op := token.видЛексемы; NextToken;
				если Term (y, critical) то
					если op = Scanner.ЗнакПлюс то
						если (x.type в ConstantIntegerOrOffset) и (y.type в ConstantIntegerOrOffset) то
							x.value := x.value+y.value; x.valueR := x.value;
						аесли (x.type = ConstantFloat) и (y.type = ConstantFloat) то
							x.valueR := x.valueR + y.valueR;
						иначе возврат ложь
						всё;
					иначе
						если (x.type в ConstantIntegerOrOffset) и (y.type в ConstantIntegerOrOffset) то
							x.value := x.value-y.value; x.valueR := x.value;
						аесли (x.type = ConstantFloat) и (y.type = ConstantFloat) то
							x.valueR := x.valueR - y.valueR;
						иначе возврат ложь
						всё;
					всё;
				иначе
					возврат ложь;
				всё;
			кц;
			возврат истина;
		кон Expression;

		проц Data(конст ident: массив из симв8): булево;
		перем size,i,nr: цел32; x: Result; pos: Position; result: Result; patterns: ObjectFile.FixupPatterns;
			проц Number(ch: симв8; перем l4nr: цел32): булево;
			нач
				если (ch >= "0") и (ch <="9") то
					l4nr := кодСимв8(ch)-кодСимв8("0");
					возврат истина
				иначе
					возврат ложь
				всё;
			кон Number;

		нач
			size := -1;
			если (ident = "DB") или (ident = "db") то size := 8
			аесли (ident="DW") или (ident = "dw") то size := 16
			аесли (ident="DD") или (ident = "dd") то size := 32
			аесли (ident="DQ") или (ident = "dq") то size := 64
			аесли (ASCII_вЗаглавную(ident[0]) ="D") то
				size := 0;i := 1;
				нцПока Number(ident[i],nr) делай
					size := size*10+nr; увел(i);
				кц;
				если ident[i] # 0X то size := -1 всё;
			всё;
			если size = -1 то возврат ложь
			иначе
				если Trace то D.String("Data"); D.Ln; всё;
				нцДо
					pos := errorPosition;
					если (token.видЛексемы = Scanner.СтрокиЛитералМульти) или (token.видЛексемы = Scanner.СтрокиЛитерал) то
						если (pass = MaxPasses) и (code.comments # НУЛЬ) то
							code.comments.пСтроку8(ident); section.comments.пСтроку8(' "');
							code.comments.пСтроку8(token.string^);
							code.comments.пСтроку8('"');
							code.comments.пВК_ПС;
							code.comments.ПротолкниБуферВПоток
						всё;
						i := 0;
						нцПока token.string[i] # 0X делай
							PutBitsIfLastPass(кодСимв8(token.string[i]),size);
							увел(i);
						кц;
						NextToken;

					аесли (token.видЛексемы = Scanner.Идентификатор) и GetNonConstant(errorPosition,token.строкаИдентификатора,result) то
						если (pass = MaxPasses) и (code.comments # НУЛЬ) то
							code.comments.пСтроку8(ident);
							code.comments.пСтроку8(" ");
							code.comments.пСтроку8(token.строкаИдентификатора);
							code.comments.пВК_ПС;
							code.comments.ПротолкниБуферВПоток
						всё;

						(* if this is the last pass then enter the fixup to the generated code section *)
						если pass = MaxPasses то
							result.fixup.SetFixupOffset(code.pc);
							code.fixupList.AddFixup(result.fixup);
							(* set fixup width *)
							нов(patterns, 1);
							patterns[0].offset := 0; patterns[0].bits := size;
							result.fixup.InitFixup(result.fixup.mode, result.fixup.offset, result.fixup.symbol, result.fixup.symbolOffset, result.fixup.displacement, 0, patterns);
						всё;

						PutBitsIfLastPass(0,size);
						NextToken;

					аесли Expression(x,ложь) то
						если x.type # ConstantInteger то Error(pos,"forbidden non-constant value") всё;
						если (pass = MaxPasses) и (code.comments # НУЛЬ) то
							code.comments.пСтроку8(ident);
							code.comments.пСтроку8(" ");
							(* code.comments.Int(x.value,1); *)

							(* print number in hexadecimal form *)
							code.comments.пСтроку8("0");
							code.comments.п16ричное(x.value, -size DIV 4);
							code.comments.пСтроку8("H");

							code.comments.пВК_ПС;
							code.comments.ПротолкниБуферВПоток
						всё;

						PutBitsIfLastPass(x.value,size);

					иначе Error(pos,"expected string or expression");
					всё;
				кцПри error или ~ThisSymbol(Scanner.Запятая);
			всё;
			возврат истина
		кон Data;

		проц Reserve(конст ident: массив из симв8): булево;
		нач возврат ложь
		кон Reserve;

		(** if the assembler is at the last pass: put bits into the binary code section, otherwise only increment the PC **)
		проц PutBitsIfLastPass(data: цел64; size: BinaryCode.Bits);
		перем
			oldPC: BinaryCode.Unit;
		нач
			если pass = MaxPasses то
				code.PutBits(data, size)
			иначе
				oldPC := code.pc;
				утв(size остОтДеленияНа code.os.unit = 0);
				code.SetPC(oldPC + size DIV code.os.unit)
			всё
		кон PutBitsIfLastPass;

		проц Instruction*(конст mnemonic: массив из симв8);
		перем numberOperands: цел32;

			проц ParseOperand(pos: Position; numberOperand: цел32);
			(* stub, must be overwritten by implementation *)
			перем operand: OperandString;
				result: Result; first: булево; str: массив 256 из симв8;
			нач
				first := истина;
				нцПока ~error и (token.видЛексемы # Scanner.Ln) и (token.видЛексемы # Scanner.Запятая) делай
					если (token.видЛексемы = Scanner.Идентификатор) и GetNonConstant(errorPosition,token.строкаИдентификатора,result) то
							D.String("(* non constant ");
							D.String(token.строкаИдентификатора); D.String("="); DumpResult(D.Log,result);
							D.String("*)");
					аесли (token.видЛексемы = Scanner.Идентификатор) и GetConstant(errorPosition,token.строкаИдентификатора,result) то
							D.String("(* constant ");
							DumpResult(D.Log,result);
							D.String("*)");
					всё;
					если first то first := ложь иначе Strings.Append(operand," ") всё;
					Scanner.TokenToString(token, scanner.вариантПеревода, str);
					Strings.Append(operand, str);
					NextToken;
				кц;
				если Trace то
					D.String("operand= ");
					D.String(operand); если token.видЛексемы = Scanner.Запятая то D.String(" , ") всё;
				всё;
			кон ParseOperand;
		нач
			если Trace то
				D.String("Instruction= "); D.String(mnemonic);  D.String(" ");
			всё;
			numberOperands := 0;
			если ~ThisSymbol(Scanner.Ln) то
				нцДо
					ParseOperand(errorPosition,numberOperands);
					увел(numberOperands);
				кцПри error или ~ThisSymbol(Scanner.Запятая);
				если ~error и ExpectSymbol(Scanner.Ln) то всё;
			всё;
			если Trace то D.Ln всё
		кон Instruction;

		проц IgnoreNewLines;
		нач
			нцПока ThisSymbol(Scanner.Ln) делай кц;
		кон IgnoreNewLines;

		проц DoAssemble();
		перем result: Result; pos: Position; line: Потоки.ТипМестоВПотоке; orgCodePos: размерМЗ; identifier: Scanner.СтрокаИдентификатора; context: Scanner.Context;
		нач
			если Trace то
				D.Str("DoAssemble: ");
				если section # НУЛЬ то Basic.WriteSegmentedName(D.Log,section.name); D.Ln всё;
			всё;

			нов(labels);
			нов(results);

			scanner.GetContext(context);
			NextToken;
			IgnoreNewLines;
			нцПока ~error и (token.видЛексемы # Scanner.ЗнакПрепинанияТочка) и (token.видЛексемы # Scanner.КонецТекста) делай
				если ThisSymbol(Scanner.Число) то
					line := token.integer;
					если ThisSymbol(Scanner.Двоеточие) то (* line number *)
					иначе Error(token.местоВТексте,"Identifier expected");
					всё;
				всё;
				если ExpectIdentifier(pos,identifier) то
					если ThisSymbol(Scanner.Двоеточие) то (* label *)
						DefineLabel(pos,identifier)
					аесли ThisIdentifier("equ") или ThisSymbol(Scanner.Equal) то
						если Expression(result,ложь) то DefineResult(pos,identifier,result) всё;
					иначе scanner.SkipToEndOfLine; NextToken;
					всё;
				всё;
				IgnoreNewLines;
			кц;

			orgCodePos := code.pc;
			нцДля pass := 1 до MaxPasses делай
				labels.ResetDisplacements; (* this is important as the displacement is corrected by code emission in a cummulative way *)
				code.SetPC(orgCodePos);
				SetContext(context);
				IgnoreNewLines;
				нцПока ~error и (token.видЛексемы # Scanner.КонецТекста) и (token.видЛексемы # Scanner.ЗнакПрепинанияТочка) делай
					если ThisSymbol(Scanner.Число) то
						line := token.integer;
						если ThisSymbol(Scanner.Двоеточие) то (* line number *)
						иначе Error(token.местоВТексте,"Identifier expected");
						всё;
					всё;
					если ExpectIdentifier(pos,identifier) то
						если ThisSymbol(Scanner.Двоеточие) то (* label *)
							SetLabel(pos,identifier);
						аесли ThisIdentifier("equ") или ThisSymbol(Scanner.Equal) то (* constant definition *)
							если Expression(result,ложь) то SetResult(identifier,result) всё;
						иначе
							если identifier = "section" то
								Section()
							аесли Data(identifier) то
							аесли Reserve(identifier) то
							аесли identifier = "fixed" то
								если ExpectConstantInteger(result,истина) то
									code.SetAlignment(истина,цел32(result.value))
								всё;
							аесли ~error то
								errorPosition := pos;
								Instruction(identifier);
								(*
								IF ~error & ExpectSymbol(Scanner.Ln) THEN END;
								*)
							всё;
						всё;
					всё;
					IgnoreNewLines;
				кц;
			кц;
			если Trace то
				D.Str("END Assemble"); D.Ln;
			всё
		кон DoAssemble;

		проц InlineAssemble*(l5scanner: Scanner.AssemblerScanner; l6section: IntermediateCode.Section; l7scope: SyntaxTree.ВнутренняяОбластьВидимости; l8module: Sections.Module);
		нач
			утв(l8module # НУЛЬ); утв(l5scanner # НУЛЬ); утв(l6section # НУЛЬ);
			утв(l6section.resolved # НУЛЬ);
			сам.scope := l7scope;
			сам.module := l8module;
			сам.scanner := l5scanner;
			сам.section := l6section;
			сам.code := l6section.resolved;
			DoAssemble;
		кон InlineAssemble;

		проц Assemble*(l9scanner: Scanner.AssemblerScanner);
		нач
			утв(l9scanner # НУЛЬ);
			сам.scanner := l9scanner;
			module := НУЛЬ; section := НУЛЬ; scope := НУЛЬ;
			l9scanner.SetContext(l9scanner.startContext);
			DoAssemble;
		кон Assemble;

		проц AllSections*;
		перем pos: Position; sectionType, sectionName: Scanner.СтрокаИдентификатора;
		нач
			если Trace то D.String("AllSections"); D.Ln всё;
			SetContext(scanner.startContext);
			IgnoreNewLines;
			нцПока ThisSymbol(Scanner.ЗнакПрепинанияТочка) и ExpectIdentifier(pos,sectionType) и ExpectIdentifier(pos,sectionName) делай
				D.String("section "); D.String(sectionType); D.String(" "); D.String(sectionName); D.Ln;
				DoAssemble;
			кц;
		кон AllSections;

		проц Text*(l10scanner: Scanner.AssemblerScanner);
		нач
			утв(l10scanner # НУЛЬ);
			сам.scanner := l10scanner;
			module := НУЛЬ; section := НУЛЬ; scope := НУЛЬ;
			AllSections;
		кон Text;

	кон Assembler;

	проц DumpResult*(w: Потоки.Писарь; result: Result);
	нач
		просей result.type из
			ConstantInteger: w.пСтроку8("i"); w.пЦел64(result.sizeInBits,1);w.пСтроку8(" ");w.пЦел64(result.value,1);
			|ConstantFloat: w.пСтроку8("f");w.пЦел64(result.sizeInBits,1);w.пСтроку8(" ");w.пВещ64(result.value,20);
			|Offset: w.пСтроку8("ofs "); w.пЦел64(result.value,1);
			|Fixup: w.пСтроку8("i"); w.пЦел64(result.sizeInBits,1);w.пСтроку8(" "); w.пСтроку8("fixup ");
				result.fixup.Dump(w);
		всё;
	кон DumpResult;

	проц Test*(context: Commands.Context);
	перем scanner: Scanner.AssemblerScanner;  diagnostics: Diagnostics.StreamDiagnostics; assembler: Assembler;
	нач
		нов(diagnostics,context.out);
		scanner := Scanner.NewAssemblerScanner("command",context.arg,0,diagnostics);
		нов(assembler,diagnostics);
		assembler.Text(scanner);
		(*
		assembler.Assemble(scanner);
		*)
	кон Test;

	проц TestScanner*(context: Commands.Context);
	перем scanner: Scanner.AssemblerScanner;  diagnostics: Diagnostics.StreamDiagnostics; token: Scanner.Лексема;
	нач
		нов(diagnostics,context.out);
		scanner := Scanner.NewAssemblerScanner("command",context.arg,0,diagnostics);
		нцПока scanner.ДочитайЛексему(token) и (token.видЛексемы # Scanner.КонецТекста)  делай
			Scanner.PrintToken(context.out, token,Scanner.ВариантыПеревода.Uppercase); context.out.пВК_ПС;
		кц;
	кон TestScanner;

кон FoxAssembler.
(*
FoxAssembler.Test
;---------------- intermediate code -----------------
.module BitSets

.imports SYSTEM

.const BitSets.@moduleSelf offset=0
	0: data u32 0

.const BitSets.BitSet offset=0
	0: data u32 0

.code BitSets.BitSet.InitBitSet offset=0
	0: enter  0,  0
	1: mov u32 r1, u32 [fp+8]
	2: mov s32 [r1], s32 [fp+12]
	3: push s32 [fp+12]
	4: mov u32 r2, u32 [fp+8]
	5: add u32 r3, u32 [r2-4], u32 -88
	6: push u32 r2
	7: call u32 [r3],  8
	8: leave  0
	9: exit  8

.code BitSets.BitSet.Zero offset=0
	0: enter  0,  8
	1: mov s32 [fp-4], s32 0
	2: mov u32 r1, u32 [fp+8]
	3: mov u32 r2, u32 [r1+4]
	4: conv s32 r3, u32 [r2+12]
	5: sub s32 r3, s32 r3, s32 1
	6: mov s32 [fp-8], s32 r3
	7: brlt u32 BitSets.BitSet.Zero:21, s32 [fp-8], s32 [fp-4]
	8: br u32 BitSets.BitSet.Zero:9
	9: conv u32 r4, s32 [fp-4]
	10: mov u32 r5, u32 r4
	11: mov u32 r6, u32 [fp+8]
	12: mov u32 r7, u32 [r6+4]
	13: brlt u32 BitSets.BitSet.Zero:15, u32 r4, u32 [r7+12]
	14: trap  7
	15: mul u32 r5, u32 r5, u32 4
	16: add u32 r5, u32 r5, u32 r7+16
	17: mov u32 [r5], u32 0
	18: add s32 r8, s32 [fp-4], s32 1
	19: mov s32 [fp-4], s32 r8
	20: br u32 BitSets.BitSet.Zero:7
	21: leave  0
	22: exit  4

.code BitSets.BitSet.Resize offset=0
	0: enter  0,  12
	1: brlt u32 BitSets.BitSet.Resize:3, s32 [fp+12], s32 0
	2: br u32 BitSets.BitSet.Resize:4
	3: trap  8
	4: mov u32 r1, u32 [fp+8]
	5: mov s32 [r1], s32 [fp+12]
	6: sub s32 r2, s32 [fp+12], s32 1
	7: brlt u32 BitSets.BitSet.Resize:10, s32 r2, s32 0
	8: mov s32 r2, s32 r2
	9: br u32 BitSets.BitSet.Resize:11
	10: mov s32 r2, s32 0, s32 r2
	11: shr s32 r2, s32 r2, s32 5
	12: add s32 r2, s32 r2, s32 1
	13: mov s32 [fp+12], s32 r2
	14: mov u32 r3, u32 [fp+8]
	15: breq u32 BitSets.BitSet.Resize:35, u32 [r3+4], u32 0
	16: br u32 BitSets.BitSet.Resize:17
	17: mov u32 r4, u32 [fp+8]
	18: mov u32 r5, u32 [r4+4]
	19: conv s32 r6, u32 [r5+12]
	20: brlt u32 BitSets.BitSet.Resize:25, s32 r6, s32 [fp+12]
	21: br u32 BitSets.BitSet.Resize:22
	22: leave  0
	23: exit  8
	24: br u32 BitSets.BitSet.Resize:25
	25: mov u32 r7, u32 [fp+8]
	26: mov u32 r8, u32 [r7+4]
	27: conv s32 r9, u32 [r8+12]
	28: shl s32 r9, s32 r9, s32 1
	29: brlt u32 BitSets.BitSet.Resize:32, s32 [fp+12], s32 r9
	30: mov s32 r9, s32 [fp+12]
	31: br u32 BitSets.BitSet.Resize:33
	32: mov s32 r9, s32 r9, s32 r9
	33: mov s32 [fp+12], s32 r9
	34: br u32 BitSets.BitSet.Resize:35
	35: brge u32 BitSets.BitSet.Resize:37, s32 [fp+12], s32 0
	36: trap  9
	37: push s32 [fp+12]
	38: mov s32 r10, s32 [fp+12]
	39: conv u32 r10, s32 r10
	40: mul u32 r10, u32 r10, u32 4
	41: add u32 r10, u32 r10, u32 16
	42: push u32 fp-4
	43: push u32 fp-4
	44: push u32 r10
	45: push u8 0
	46: call u32 $SystemCall2:0,  0
	47: pop u32 r11
	48: mov u32 r12, u32 [r11]
	49: breq u32 BitSets.BitSet.Resize:53, u32 r12, u32 0
	50: pop u32 r13
	51: mov u32 [r12+12], u32 r13
	52: br u32 BitSets.BitSet.Resize:54
	53: add u32 sp, u32 sp, u32 4
	54: mov u32 r14, u32 [fp+8]
	55: breq u32 BitSets.BitSet.Resize:85, u32 [r14+4], u32 0
	56: br u32 BitSets.BitSet.Resize:57
	57: mov s32 [fp-8], s32 0
	58: mov u32 r15, u32 [fp+8]
	59: mov u32 r16, u32 [r15+4]
	60: conv s32 r17, u32 [r16+12]
	61: sub s32 r17, s32 r17, s32 1
	62: mov s32 [fp-12], s32 r17
	63: brlt u32 BitSets.BitSet.Resize:84, s32 [fp-12], s32 [fp-8]
	64: br u32 BitSets.BitSet.Resize:65
	65: conv u32 r18, s32 [fp-8]
	66: mov u32 r19, u32 r18
	67: mov u32 r20, u32 [fp+8]
	68: mov u32 r21, u32 [r20+4]
	69: brlt u32 BitSets.BitSet.Resize:71, u32 r18, u32 [r21+12]
	70: trap  7
	71: mul u32 r19, u32 r19, u32 4
	72: add u32 r19, u32 r19, u32 r21+16
	73: conv u32 r22, s32 [fp-8]
	74: mov u32 r23, u32 r22
	75: mov u32 r24, u32 [fp-4]
	76: brlt u32 BitSets.BitSet.Resize:78, u32 r22, u32 [r24+12]
	77: trap  7
	78: mul u32 r23, u32 r23, u32 4
	79: add u32 r23, u32 r23, u32 r24+16
	80: mov u32 [r23], u32 [r19]
	81: add s32 r25, s32 [fp-8], s32 1
	82: mov s32 [fp-8], s32 r25
	83: br u32 BitSets.BitSet.Resize:63
	84: br u32 BitSets.BitSet.Resize:85
	85: mov u32 r26, u32 [fp+8]
	86: mov u32 [r26+4], u32 [fp-4]
	87: leave  0
	88: exit  8

.code BitSets.BitSet.GetSize offset=0
	0: enter  0,  0
	1: mov u32 r1, u32 [fp+8]
	2: return s32 [r1]
	3: leave  0
	4: exit  4
	5: trap  3

*)
