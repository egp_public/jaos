echo Сейчас запустится A2 и откроется файл
echo zapusti-vnutri-a2host-exe.Tool .
echo Выполни команды в нём.

set -xeuo pipefail 

arg_avto="0"

for ((i=1; i <= "$#"; ++i))
do
    if [ "${!i}" == "--avto" ]; then
        ((i=i+1))	
        arg_avto=${!i}
        continue  
    fi
   
    echo "Неизвестный аргумент: ${!i}";
    exit;
done    

if [ "$arg_avto" == "1" ]; then
    imja_fajjla_komand=a2-dlja-avto-sborki.txt
else
    imja_fajjla_komand=a2-dlja-sborki.txt
fi

cd NewLinux64
cmd.exe /c "chcp 65001 && .\oberonhost.exe run $imja_fajjla_komand"

