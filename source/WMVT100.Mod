модуль WMVT100; (* ejz,  ph *)
(* to do: nicer, dialog-free GUI*)

	использует WMWindowManager, WMComponents, WMStandardComponents, WMGraphics, WMPopups, WMEditors, WMDialogs, WMMessages,
		Strings, Texts, Inputs, Потоки, Commands, IP, DNS, TCP, Telnet, ЛогЯдра, Modules;

	конст
		Border = 2; BoxW = 8; BoxH = 18;
		Left = 0; Right = 2;
		Underscore = 0; Blink = 1;
		CursorKeyMode = 0; AppKeypadMode = 1; AutoWrapMode = 2; WindowSize = 31;

	тип
		KillerMsg=окласс кон KillerMsg;

	тип
		Connection = окласс (Telnet.Connection)
			перем frame: Frame; mode: мнвоНаБитахМЗ;

			проц {перекрыта}Do*(option: симв8);
			нач
				если option = Telnet.OptTerminalType то
					W.пСимв8(Telnet.CmdIAC); W.пСимв8(Telnet.CmdWILL); W.пСимв8(Telnet.OptTerminalType)
				аесли option = Telnet.OptWindowSize то
					утв((frame.cols < 255) и (frame.rows < 255));
					включиВоМнвоНаБитах(mode, WindowSize);
					W.пСимв8(Telnet.CmdIAC); W.пСимв8(Telnet.CmdSB);
					W.пСимв8(Telnet.OptWindowSize);
					W.пСимв8(симв8ИзКода(frame.cols DIV 256));
					W.пСимв8(симв8ИзКода(frame.cols остОтДеленияНа 256));
					W.пСимв8(симв8ИзКода(frame.rows DIV 256));
					W.пСимв8(симв8ИзКода(frame.rows остОтДеленияНа 256));
					W.пСимв8(Telnet.CmdIAC); W.пСимв8(Telnet.CmdSE)
				иначе
					Do^(option)
				всё
			кон Do;

			проц {перекрыта}SB*(option: симв8);
				перем ch: симв8;
			нач
				если (option = Telnet.OptTerminalType) и (R.ПодглядиСимв8() = 01X) то (* SEND *)
					включиВоМнвоНаБитах(flags, Telnet.VT100);
					R.чСимв8(ch); (* 01X *)
					R.чСимв8(ch); утв(ch = Telnet.CmdIAC);
					R.чСимв8(ch); утв(ch = Telnet.CmdSE);
					W.пСимв8(Telnet.CmdIAC); W.пСимв8(Telnet.CmdSB);
					W.пСимв8(Telnet.OptTerminalType);
					W.пСимв8(0X); (* IS *)
					W.пСтроку8("VT100");
					W.пСимв8(Telnet.CmdIAC); W.пСимв8(Telnet.CmdSE)
				иначе
					SB^(option)
				всё
			кон SB;

			проц ESC(ch: симв8);
				перем par: массив 4 из цел32; i, n: цел32; done: булево;
			нач
				если ~(Telnet.VT100 в flags) то возврат всё;
				R.чСимв8(ch);
				если ch = "[" то
					ch := R.ПодглядиСимв8(); n := 0;
					если ch = "?" то
						R.чСимв8(ch); ch := R.ПодглядиСимв8();
						если (ch >= "0") и (ch <= "9") то
							нцДо
								R.чЦел32(par[n], ложь); увел(n); R.чСимв8(ch)
							кцПри (n >= 4) или (ch # " ")
						всё
					аесли (ch >= "0") и (ch <= "9") то
						нцДо
							R.чЦел32(par[n], ложь); увел(n); R.чСимв8(ch)
						кцПри (n >= 4) или (ch # ";")
					иначе
						утв(ch < 07FX);
						R.чСимв8(ch)
					всё;
					done := ложь;
					просей ch из
						"A": если n = 1 то
										frame.Goto(frame.GetCol(), frame.GetRow()-par[0], истина);done := истина
									иначе
										frame.Goto(frame.GetCol(), frame.GetRow()-1, истина);done := n = 0
									всё;done := n = 0
						|"B": если n = 1 то
										frame.Goto(frame.GetCol(), frame.GetRow()+par[0], истина);done := истина
									иначе
										frame.Goto(frame.GetCol(), frame.GetRow()+1, истина);done := n = 0
									всё
						|"C": если n = 1 то
										frame.Goto(frame.GetCol()+par[0], frame.GetRow(), ложь);done := истина
									иначе
										frame.Goto(frame.GetCol()+1, frame.GetRow(), ложь);done := n = 0
									всё
						|"D": если n = 1 то
										frame.Goto(frame.GetCol()-par[0], frame.GetRow(), ложь);done := истина
									иначе
										frame.Goto(frame.GetCol()-1, frame.GetRow(), ложь);done := n = 0
									всё
						|"H": если n = 2 то
										frame.Goto(par[1]-1, par[0]-1, ложь);done := истина
									иначе
										frame.Goto(0, 0, ложь);done := n = 0
									всё
						|"J", "K":  frame.Erase(ch, par, n);done := истина
						|"h": если n = 1 то
									если par[0] = 1 то
										включиВоМнвоНаБитах(mode, CursorKeyMode);done := истина
									аесли par[0] = 7 то
										включиВоМнвоНаБитах(mode, AutoWrapMode);done := истина
									всё
								всё
						|"l": если n = 1 то
									если par[0] = 1 то
										исключиИзМнваНаБитах(mode, CursorKeyMode);done := истина
									аесли par[0] = 7 то
										исключиИзМнваНаБитах(mode, AutoWrapMode);done := истина
									всё
								всё
						|"m": frame.SetAttributes(par, n);done := истина
					иначе
					всё;
					если ~done то
						ЛогЯдра.пСтроку8("ESC [ ");
						i := 0;
						нцПока i < n делай
							ЛогЯдра.пЦел64(par[i], 0); увел(i);
							если i < n то ЛогЯдра.пСтроку8(" ; ") всё
						кц;
						ЛогЯдра.пСтроку8(" "); ЛогЯдра.пСимв8(ch);
						ЛогЯдра.пСтроку8(" "); ЛогЯдра.пВК_ПС()
					всё
				иначе
					просей ch из
						"=": включиВоМнвоНаБитах(mode, AppKeypadMode)
						|">": исключиИзМнваНаБитах(mode, AppKeypadMode)
						|"D": frame.SetTop(frame.GetRow()+1)
						|"M": frame.SetTop(frame.GetRow()-1)
					иначе ЛогЯдра.пСтроку8("ESC "); ЛогЯдра.п16ричное(кодСимв8(ch), 0); ЛогЯдра.пВК_ПС()
					всё
				всё
			кон ESC;

			проц {перекрыта}Consume*(ch: симв8);
				перем buf: массив 128 из симв32; i, n: размерМЗ;
			нач
				просей ch из
					0X: (* NUL *)
					|07X: (* BEL *)
					|08X: frame.Goto(frame.GetCol()-1, frame.GetRow(), ложь)
					|09X: frame.RightTab()
					|0AX: frame.Goto(0, frame.GetRow()+1, истина) (* was fused with below, but lone LFs may occur in some textual responses like HTML from some sites*)
					|0BX, 0CX: frame.Goto(frame.GetCol(), frame.GetRow()+1, истина)
					|0DX: если R.ПодглядиСимв8() = 0AX то
									R.чСимв8(ch); frame.Goto(0, frame.GetRow()+1, истина)
								иначе
									frame.Goto(0, frame.GetRow(), ложь)
								всё
					|01BX: ESC(ch)
					|07FX: frame.Delete()
				иначе (* iso-8859-1 *)
					buf[0] := симв32ИзКода(кодСимв8(ch)); i := 1; n := R.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество();
					если n > 0 то
						если n > 127 то n := 127 всё; ch := R.ПодглядиСимв8();
						нцПока (n > 0) и (ch >= 020X) и (ch <= 07EX) делай
							R.чСимв8(ch); умень(n);
							buf[i] := симв32ИзКода(кодСимв8(ch)); увел(i);
							если n > 0 то ch := R.ПодглядиСимв8() всё
						кц
					всё;
					frame.WriteChars(buf, i)
				всё
			кон Consume;

			проц &{перекрыта}Init*(C: Потоки.ФункциональныйИнтерфейсКПотоку);
			нач
				Init^(C); включиВоМнвоНаБитах(flags, Telnet.Telnet);
				mode := {}; frame := НУЛЬ
			кон Init;

			проц SetFrame(frame: Frame);
			нач {единолично}
				сам.frame := frame
			кон SetFrame;

			проц {перекрыта}Setup*;
			нач {единолично}
				дождись(frame # НУЛЬ);
				если Telnet.Telnet в flags то
					W.пСимв8(Telnet.CmdIAC); W.пСимв8(Telnet.CmdDO); W.пСимв8(Telnet.OptSupGoAhead);
					W.пСимв8(Telnet.CmdIAC); W.пСимв8(Telnet.CmdDO); W.пСимв8(Telnet.OptEcho);
					W.пСимв8(Telnet.CmdIAC); W.пСимв8(Telnet.CmdWILL); W.пСимв8(Telnet.OptTerminalType);
					W.пСимв8(Telnet.CmdIAC); W.пСимв8(Telnet.CmdWILL); W.пСимв8(Telnet.OptWindowSize);
					W.ПротолкниБуферВПоток()
				всё
			кон Setup;
		кон Connection;

		Attribute = укль на запись
			fnt: WMGraphics.Font;
			bg, fg: WMGraphics.Color;
			special: мнвоНаБитахМЗ (* 0: underscore *)
		кон;

		Char = запись
			attr: Attribute;
			char: симв32
		кон;

		Data = укль на массив из Char;

		Line = укль на запись
			data: Data;
			t, b: размерМЗ;
			next: Line
		кон;

		Position = запись
			line: Line; ofs: размерМЗ
		кон;

		Frame = окласс (WMComponents.VisualComponent)
			перем
				C: Connection;
				first, last, top: Line; bg: WMGraphics.Color;
				rows, cols, boxW, boxH, dX, dY: размерМЗ;
				tabs: укль на массив из булево;
				attr: Attribute; cursor: Position;
				sel: запись beg, end: Position кон;
				popup: WMPopups.Popup;
				doEcho:булево;

			проц GetCol(): размерМЗ;
			нач {единолично}
				возврат cursor.ofs
			кон GetCol;

			проц GetRow(): цел32;
				перем l: Line; row: цел32;
			нач {единолично}
				l := top; row := 0;
				нцПока l # cursor.line делай
					l := l.next; увел(row)
				кц;
				возврат row
			кон GetRow;

			проц appendLine(): Line;
				перем line: Line; i: цел32; ch: Char;
			нач
				нов(line); line.next := НУЛЬ;
				нов(line.data, cols);
				ch.attr := attr; ch.char := Texts.Симв32Нулевой;
				i := 0;
				нцПока i < cols делай
					line.data[i] := ch; увел(i)
				кц;
				если last # НУЛЬ то
					last.next := line; line.t := last.b
				иначе
					line.t := dY
				всё;
				last := line; line.b := line.t + boxH;
				возврат line
			кон appendLine;

			проц AppendLine(): Line;
			нач {единолично}
				возврат appendLine()
			кон AppendLine;

			проц UpdateBox(line: Line; ofs: размерМЗ);
				перем update: WMGraphics.Rectangle;
			нач
				update.l := dX + ofs*boxW; update.r := update.l + boxW;
				update.t := line.t; update.b := line.b;
				InvalidateRect(update)
			кон UpdateBox;

			проц UpdateRect(al, bl: Line; aofs, bofs: размерМЗ; cur: мнвоНаБитахМЗ);
				перем tl: Line; tofs: размерМЗ; update: WMGraphics.Rectangle; swapl, swapo: булево;
			нач
				swapl := ложь; swapo := ложь;
				если al # bl то
					tl := al;
					нцПока (tl # НУЛЬ) и (tl # bl) делай
						tl := tl.next
					кц;
					если tl = НУЛЬ то swapl := истина; tl := al; al := bl; bl := tl всё
				всё;
				если aofs > bofs то swapo := истина; tofs := aofs; aofs := bofs; bofs := tofs всё;
				update.l := dX + aofs*boxW; update.r := dX + bofs*boxW + boxW;
				update.t := al.t; update.b := bl.b;
				если cur # {} то
					если 1 в cur то
						если swapl то cursor.line := bl иначе cursor.line := al всё
					аесли 2 в cur то
						если swapl то cursor.line := al иначе cursor.line := bl всё
					всё;
					если 3 в cur то
						если swapo то cursor.ofs := bofs иначе cursor.ofs := aofs всё
					аесли 4 в cur то
						если swapo то cursor.ofs := aofs иначе cursor.ofs := bofs всё
					всё
				всё;
				InvalidateRect(update)
			кон UpdateRect;

			проц UpdateAll;
				перем update: WMGraphics.Rectangle;
			нач
				update.l := 0; update.r := bounds.GetWidth();
				update.t := 0; update.b := bounds.GetHeight();
				InvalidateRect(update)
			кон UpdateAll;



			проц writeChars(перем buf: массив из симв32; n: размерМЗ);
				перем l: Line; i, ofs: размерМЗ; wrap: булево;
			нач
				wrap := ложь;
				l := cursor.line; ofs := cursor.ofs; i := 0;
				нц
					нцПока (i < n) и (ofs < cols) делай
						l.data[ofs].attr := attr; l.data[ofs].char := buf[i];
						увел(ofs); увел(i)
					кц;
					если (i < n) и (AutoWrapMode в C.mode) то
						l := l.next; ofs := 0; wrap := истина;
						если l = НУЛЬ то l := appendLine() всё
					иначе
						прервиЦикл
					всё
				кц;
				если wrap то
					cursor.ofs := ofs;
					UpdateRect(cursor.line, l, 0, cols-1, {2})
				иначе
					UpdateRect(cursor.line, l, cursor.ofs, ofs, {4})
				всё
			кон writeChars;

			проц WriteChars(перем buf: массив из симв32; n: размерМЗ);
			нач {единолично}
				writeChars(buf,n);
			кон WriteChars;

			проц Delete;
				перем l: Line; ofs: размерМЗ;
			нач {единолично}
				l := cursor.line; ofs := cursor.ofs;
				если ofs > 0 то
					умень(ofs); l.data[ofs].attr := attr; l.data[ofs].char := Texts.Симв32Нулевой;
					UpdateRect(l, l, ofs, cursor.ofs, {3})
				всё
			кон Delete;

			проц goto(col, row: размерМЗ; scroll: булево);
				перем l: Line; y, b: размерМЗ;
			нач
				(* top = row 0, < 0 => scroll up *)
				утв(row >= 0);
				если col < 0 то col := 0 аесли col >= cols то col := cols-1 всё;
				y := dY + boxH; b := dY + rows*boxH; l := top;
				нцПока row > 0 делай
					l := l.next; умень(row); увел(y, boxH);
					если l = НУЛЬ то l := appendLine() всё
				кц;
				если scroll и (y > b) и (top.next # НУЛЬ) то
					top := top.next; cursor.line := l; cursor.ofs := col;
					UpdateAll()
				иначе
					UpdateRect(cursor.line, l, cursor.ofs, col, {2, 4})
				всё
			кон goto;


			проц Goto(col, row: размерМЗ; scroll: булево);
			нач {единолично}
				goto(col,row,scroll)
			кон Goto;

			проц SetTop(row: цел32);
			нач {единолично}
				ЛогЯдра.пСтроку8("SetTop "); ЛогЯдра.пЦел64(row, 0); ЛогЯдра.пВК_ПС();
				если row < 0 то

				аесли row > 0 то

				всё
			кон SetTop;

			проц RightTab;
				перем l: Line; ofs: размерМЗ; char: Char;
			нач {единолично}
				char.attr := attr; char.char := ЛитСимв32(" ");
				l := cursor.line; ofs := cursor.ofs+1;
				нцПока (ofs < cols) и ~tabs[ofs] делай
					l.data[ofs] := char; увел(ofs)
				кц;
				если ofs = cursor.ofs то возврат всё;
				UpdateRect(l, l, cursor.ofs, ofs, {4})
			кон RightTab;

			проц EraseLine(l: Line; from, to: размерМЗ);
				перем i: размерМЗ;
			нач
				i := from;
				нцПока i <= to делай
					l.data[i].attr := attr; l.data[i].char := Texts.Симв32Нулевой;
					увел(i)
				кц
			кон EraseLine;

			проц Erase(mode: симв8; par: массив из цел32; n: цел32);
			нач {единолично}
				просей mode из
					"J": sel.beg.line := НУЛЬ; cursor.line := last; cursor.ofs := 0;
							top := last; EraseLine(top, 0, cols-1);
							UpdateAll()
					|"K": если n = 0 то
								EraseLine(cursor.line, cursor.ofs, cols-1);
								UpdateRect(cursor.line, cursor.line, cursor.ofs, cols-1, {})
							аесли (n = 1) и (par[0] = 1) то
								EraseLine(cursor.line, 0, cursor.ofs);
								UpdateRect(cursor.line, cursor.line, 0, cursor.ofs, {})
							аесли (n = 1) и (par[0] = 2) то
								EraseLine(cursor.line, 0, cols-1);
								UpdateRect(cursor.line, cursor.line, 0, cols-1, {})
							всё
				всё
			кон Erase;

			проц NewAttr;
			нач
				нов(attr); attr.special := {};
				attr.fnt := WMGraphics.GetFont("Courier", 10, {});
				attr.bg := WMGraphics.RGBAToColor(255, 255, 255, 255);
				attr.fg := WMGraphics.RGBAToColor(0, 0, 0, 255)
			кон NewAttr;


			проц Bright;
				перем style: мнвоНаБитахМЗ;
			нач
				style := attr.fnt.style;
				если ~(WMGraphics.FontBold в style) то
					включиВоМнвоНаБитах(style, WMGraphics.FontBold);
					attr.fnt := WMGraphics.GetFont(attr.fnt.name, attr.fnt.size, style)
				иначе ЛогЯдра.пСтроку8("Bright"); ЛогЯдра.пВК_ПС()
				всё
			кон Bright;

			проц Dim;
				перем style: мнвоНаБитахМЗ;
			нач
				style := attr.fnt.style;
				если WMGraphics.FontBold в style то
					исключиИзМнваНаБитах(style, WMGraphics.FontBold);
					attr.fnt := WMGraphics.GetFont(attr.fnt.name, attr.fnt.size, style)
				иначе ЛогЯдра.пСтроку8("Dim"); ЛогЯдра.пВК_ПС()
				всё
			кон Dim;

			проц SetAttributes(attrs: массив из цел32; n: цел32);
				перем c: WMGraphics.Color; i: цел32;
			нач {единолично}
				NewAttr();
				i := 0;
				нцПока i < n делай
					просей attrs[i] из
						0: (* Reset *) NewAttr()
						|1: (* Bright *) Bright()
						|2: (* Dim *) Dim()
						|4: (* Underscore *) включиВоМнвоНаБитах(attr.special, Underscore)
						|5: (* Blink *) включиВоМнвоНаБитах(attr.special, Blink )
						|7: (* Reverse *) c := attr.bg; attr.bg := attr.fg; attr.fg := c
						|8: (* Hidden *) attr.fg := attr.bg
					иначе
						ЛогЯдра.пСтроку8("attr "); ЛогЯдра.пЦел64(attrs[i], 0); ЛогЯдра.пВК_ПС()
					всё;
					увел(i)
				кц
			кон SetAttributes;

			проц {перекрыта}Draw*(canvas: WMGraphics.Canvas);
				перем l: Line; i, j, dy, bottom: размерМЗ; attr: Attribute; char: Char; box: WMGraphics.Rectangle;
			нач {единолично}
				canvas.Fill(canvas.clipRect, bg, WMGraphics.ModeCopy);
				l := first;
				нцПока l # top делай
					l.t := матМинимум(цел16); l.b := матМинимум(цел16); l := l.next
				кц;
				attr := НУЛЬ; bottom := dY + rows*boxH;
				box.t := dY; box.b := dY + boxH; j := 0;
				нцПока (l # НУЛЬ) и (j < rows) и (box.b <= bottom) делай
					l.t := box.t; l.b := box.b;
					box.l := dX; box.r := dX + boxW; i := 0;
					нцПока i < cols делай
						char := l.data[i];
						если char.attr # attr то
							attr := char.attr;
							canvas.SetColor(attr.fg);
							canvas.SetFont(attr.fnt);
							dy := attr.fnt.GetDescent()
						всё;
						если attr.bg # bg то
							canvas.Fill(box, attr.bg, WMGraphics.ModeCopy)
						всё;
						если char.char # Texts.Симв32Нулевой то
							attr.fnt.RenderChar(canvas, box.l, box.b-dy, char.char)
						всё;
						если Underscore в attr.special то
							canvas.Line(box.l, box.b-dy+1, box.r-1, box.b-dy+1, attr.fg, WMGraphics.ModeCopy)
						всё;
						увел(i); увел(box.l, boxW); увел(box.r, boxW)
					кц;
					увел(j); l := l.next;
					увел(box.t, boxH); увел(box.b, boxH)
				кц;
				нцПока l # НУЛЬ делай
					l.t := матМаксимум(цел16); l.b := матМаксимум(цел16); l := l.next
				кц;
				если hasFocus и (cursor.ofs >= 0) и (cursor.ofs < cols) то
					l := cursor.line; box.t := l.t; box.b := l.b;
					если box.t < box.b то
						box.l := dX + cursor.ofs*boxW; box.r := box.l + boxW;
						canvas.Fill(box, WMGraphics.RGBAToColor(255, 0, 0, 192), WMGraphics.ModeSrcOverDst)
					иначе
						FocusLost
					всё
				всё;
				если sel.beg.line # НУЛЬ то
					если sel.beg.line = sel.end.line то
						box.l := dX + sel.beg.ofs * boxW; box.r := dX + sel.end.ofs * boxW + boxW;
						box.t := sel.beg.line.t; box.b := sel.end.line.b;
						canvas.Fill(box, WMGraphics.RGBAToColor(0, 0, 255, 32), WMGraphics.ModeSrcOverDst)
					иначе
						box.l := dX + sel.beg.ofs * boxW; box.r := dX + cols * boxW;
						box.t := sel.beg.line.t; box.b := sel.beg.line.b;
						canvas.Fill(box, WMGraphics.RGBAToColor(0, 0, 255, 32), WMGraphics.ModeSrcOverDst);
						l := sel.beg.line.next;
						нцПока l # sel.end.line делай
							box.l := dX; box.r := dX + cols * boxW;
							box.t := l.t; box.b := l.b;
							canvas.Fill(box, WMGraphics.RGBAToColor(0, 0, 255, 32), WMGraphics.ModeSrcOverDst);
							l := l.next
						кц;
						box.l := dX; box.r := dX + sel.end.ofs * boxW + boxW;
						box.t := sel.end.line.t; box.b := sel.end.line.b;
						canvas.Fill(box, WMGraphics.RGBAToColor(0, 0, 255, 32), WMGraphics.ModeSrcOverDst)
					всё
				всё
			кон Draw;

			проц {перекрыта}FocusReceived*;
			нач
				FocusReceived^();
				UpdateBox(cursor.line, cursor.ofs)
			кон FocusReceived;

			проц {перекрыта}FocusLost*;
			нач
				FocusLost^();
				UpdateBox(cursor.line, cursor.ofs)
			кон FocusLost;

			проц LocateBox(x, y: размерМЗ; перем pos: Position);
				перем l: Line; ofs, i: размерМЗ;
			нач
				если x < dX то x := dX аесли x >= (dX + cols*boxW) то x := dX + cols*boxW-1 всё;
				если y < dY то y := dY аесли y >= (dY + rows*boxH) то y := dY + rows*boxH-1 всё;
				pos.line := НУЛЬ; pos.ofs := -1;
				l := top;
				нцПока (l # НУЛЬ) и ~((l.t <= y) и (l.b > y)) делай
					l := l.next
				кц;
				если l # НУЛЬ то
					ofs := 0; i := dX;
					нцПока (ofs < cols) и ~((i <= x) и ((i+boxW) > x)) делай
						увел(ofs); увел(i, boxW)
					кц;
					если ofs < cols то
						pos.line := l; pos.ofs := ofs
					всё
				всё
			кон LocateBox;

			проц Copy;
				перем
					l: Line; apos, pos, ofs, end: размерМЗ; buf: массив 2 из симв32;
					attr: Attribute; tattr: Texts.Attributes;
			нач {единолично}
				если sel.beg.line = НУЛЬ то возврат всё;
				Texts.clipboard.AcquireRead();
				end := Texts.clipboard.GetLength();
				Texts.clipboard.ReleaseRead();
				Texts.clipboard.AcquireWrite();
				Texts.clipboard.Delete(0, end);
				pos := 0; buf[1] := Texts.Симв32Нулевой; l := sel.beg.line;
				attr := НУЛЬ; tattr := НУЛЬ; apos := -1;
				нц
					если l = sel.beg.line то
						ofs := sel.beg.ofs
					иначе
						ofs := 0
					всё;
					если l = sel.end.line то
						end := sel.end.ofs+1
					иначе
						end := cols
					всё;
					нцПока ofs < end делай
						если l.data[ofs].char # Texts.Симв32Нулевой то
							buf[0] := l.data[ofs].char;
							если attr # l.data[ofs].attr то
								если tattr # НУЛЬ то
									Texts.clipboard.SetAttributes(apos, pos-apos, tattr)
								всё;
								apos := pos; attr := l.data[ofs].attr;
								нов(tattr); нов(tattr.fontInfo);
								tattr.color := attr.fg; tattr.bgcolor := attr.bg;
								копируйСтрокуДо0(attr.fnt.name, tattr.fontInfo.name);
								tattr.fontInfo.size := attr.fnt.size; tattr.fontInfo.style := attr.fnt.style
							всё;
							Texts.clipboard.InsertUCS32(pos, buf); увел(pos)
						всё;
						увел(ofs)
					кц;
					если l = sel.end.line то
						прервиЦикл
					иначе
						l := l.next;
						buf[0] := симв32ИзКода(0AH);
						Texts.clipboard.InsertUCS32(pos, buf); увел(pos)
					всё
				кц;
				если tattr # НУЛЬ то
					Texts.clipboard.SetAttributes(apos, pos-apos, tattr)
				всё;
				Texts.clipboard.ReleaseWrite()
			кон Copy;

			проц Paste;
				перем R: Texts.TextReader; ch: симв32;
			нач {единолично}
				Texts.clipboard.AcquireRead();
				нов(R, Texts.clipboard);
				R.SetPosition(0);
				R.SetDirection(1);
				R.ReadCh(ch);
				нцПока ~R.eot делай
					если (кодСимв32(ch) DIV 256) = 0 то C.W.пСимв8(симв8ИзКода(кодСимв32(ch))) всё;
					R.ReadCh(ch)
				кц;
				Texts.clipboard.ReleaseRead();
				C.W.ПротолкниБуферВПоток()
			кон Paste;

			проц ClickHandler(sender, par: динамическиТипизированныйУкль);
				перем b: WMStandardComponents.Button; str: Strings.String;
			нач
				popup.Close();
				b := sender(WMStandardComponents.Button);
				str := b.caption.Get();
				если str^ = "Copy" то
					Copy()
				аесли str^ = "Paste" то
					Paste()
				всё
			кон ClickHandler;

			проц {перекрыта}PointerDown*(x, y: размерМЗ; keys: мнвоНаБитахМЗ);
			нач
				если (Left в keys) и hasFocus то
					LocateBox(x, y, sel.beg); sel.end := sel.beg
				аесли Right в keys то
					ToWMCoordinates(x, y, x, y);
					popup.Popup(x, y)
				иначе
					sel.beg.line := НУЛЬ; sel.beg.ofs := -1;
					sel.end := sel.beg
				всё;
				UpdateAll()
			кон PointerDown;

			проц {перекрыта}PointerMove*(x, y: размерМЗ; keys: мнвоНаБитахМЗ);
				перем pos: Position;
			нач
				если (Left в keys) и (sel.beg.line # НУЛЬ) то
					LocateBox(x, y, pos);
					если pos.line # НУЛЬ то
						если pos.line.t > sel.beg.line.t то
							sel.end := pos
						аесли (pos.line = sel.beg.line) и (pos.ofs >= sel.beg.ofs) то
							sel.end := pos
						всё;
						UpdateAll()
					всё
				всё
			кон PointerMove;

			проц {перекрыта}WheelMove*(dz: размерМЗ);
				перем l: Line;
			нач
				если (dz > 0) и (top.next # НУЛЬ) то
					top := top.next; UpdateAll()
				аесли (dz < 0) и (top # first) то
					l := first;
					нцПока l.next # top делай
						l := l.next
					кц;
					top := l; UpdateAll()
				всё
			кон WheelMove;

			проц {перекрыта}PointerUp*(x, y: размерМЗ; keys: мнвоНаБитахМЗ);
			кон PointerUp;

			проц CursorKey(keySym: размерМЗ);
			нач
				C.W.пСимв8(01BX);
				если CursorKeyMode в C.mode то
					C.W.пСимв8("O")
				иначе
					C.W.пСимв8("[")
				всё;
				просей keySym из
					0FF51H: C.W.пСимв8("D")
					|0FF52H: C.W.пСимв8("A")
					|0FF53H: C.W.пСимв8("C")
					|0FF54H: C.W.пСимв8("B")
				всё;
				C.W.ПротолкниБуферВПоток()
			кон CursorKey;

			проц {перекрыта}KeyEvent*(ucs: размерМЗ; flags: мнвоНаБитахМЗ; перем keySym: размерМЗ);
			перем echo: массив 1 из симв32; col: WMGraphics.Color;
			нач
				если ~(Inputs.Release в flags) и hasFocus то
					если (keySym DIV 256) = 0 то
						C.W.пСимв8(симв8ИзКода(keySym)); C.W.ПротолкниБуферВПоток();

						если doEcho то
							col:=attr.fg;
							attr.fg:=WMGraphics.Red;
							echo[0]:=симв32ИзКода(keySym);
							writeChars(echo,1);
							attr.fg:=col;
						всё;

					аесли (keySym DIV 256) = 0FFH то
						просей keySym из
							0FF51H .. 0FF54H: CursorKey(keySym)
							|0FF50H: (* Home *)
							|0FF57H: (* End *)
							|0FFFFH: (* Delete *)
							|0FF08H: C.W.пСимв8(07FX); C.W.ПротолкниБуферВПоток()
							|0FF0DH:
								C.W.пСимв8(0DX); C.W.пСимв8(0AX);	C.W.ПротолкниБуферВПоток(); (* insertion of 0AX after 0DX is not strictly raw TCP ! *)
								если doEcho то goto(GetCol(), GetRow()+1, истина); goto(0, GetRow(), истина) всё; (* if you do it in one step, the color is not currently handled well*)
							|0FF1BH: C.W.пСимв8(01BX); C.W.ПротолкниБуферВПоток()
							|0FF8DH:
								если AppKeypadMode в C.mode то
										C.W.пСимв8(01BX); C.W.пСимв8("O"); C.W.пСимв8("M")
								иначе	C.W.пСимв8(0DX); C.W.пСимв8(0AX)
								всё;
								C.W.ПротолкниБуферВПоток()
						иначе
						всё
					всё
				всё
			кон KeyEvent;

			проц resized;
				перем l: Line; W, H, b, t, c, r, i: размерМЗ; d: Data; ch: Char;
			нач {единолично}
				W := bounds.GetWidth() - 2*Border;
				H := bounds.GetHeight() - 2*Border;
				c := W DIV BoxW; r := H DIV BoxH;
				boxW := W DIV c; boxH := H DIV r;
				dX := Border + (W - c*boxW) DIV 2;
				dY := Border + (H - r*boxH) DIV 2;
				l := top; t := dY; b := dY + boxH;
				нцПока l # НУЛЬ делай
					l.t := t; l.b := b; l := l.next;
					увел(t, boxH); увел(b, boxH)
				кц;
				если c # cols то
					ch.attr := attr; ch.char := Texts.Симв32Нулевой;
					l := first;
					нцПока l # НУЛЬ делай
						нов(d, c);
						i := 0;
						нцПока (i < c) и (i < cols) делай
							d[i] := l.data[i]; увел(i)
						кц;
						нцПока i < c делай
							d[i] := ch; увел(i)
						кц;
						l.data := d; l := l.next
					кц
				всё;
				если (c # cols) или (r # rows) то
					если cursor.ofs >= c то cursor.ofs := c-1 всё;
					l := cursor.line;
					если l.b > (dY + r*boxH) то
						i := (l.b - (dY + r*boxH)) DIV boxH;
						l := top.next;
						нцПока (l # НУЛЬ) и (i > 0) делай
							top := l; l := l.next; умень(i)
						кц
					всё;
					sel.beg.line := НУЛЬ; cols := c; rows := r;
					если WindowSize в C.mode то
						C.Do(Telnet.OptWindowSize)
					иначе
						C.W.пСимв8(Telnet.CmdIAC); C.W.пСимв8(Telnet.CmdWILL); C.W.пСимв8(Telnet.OptWindowSize)
					всё
				всё
			кон resized;

			проц {перекрыта}Resized*;
			нач
				Resized^();
				resized()
			кон Resized;

			проц {перекрыта}Initialize*;
			нач
				Initialize^();
				takesFocus.Set(истина);
			кон Initialize;

			проц &New*(C: Connection; cols, rows: цел32);
				перем i: цел32;
			нач
				Init();
				SetNameAsString(Strings.NewString("VT100Frame"));
				(*SetGenerator("WMVT100.GenFrame");*)
				сам.C := C;
				сам.rows := rows; сам.cols := cols;
				NewAttr(); bg := WMGraphics.RGBAToColor(255, 255, 255, 255);
				last := НУЛЬ; first := AppendLine(); top := first;
				cursor.line := top; cursor.ofs := 0;
				boxW := 0; boxH := 0; dX := 0; dY := 0;
				нов(tabs, cols+1);
				tabs[0] := ложь; i := 1;
				нцПока i <= cols делай
					tabs[i] := (i остОтДеленияНа 8) = 0; увел(i)
				кц;
				C.SetFrame(сам);
				нов(popup);
				popup.Add("Copy", ClickHandler);
				popup.Add("Paste", ClickHandler);
				doEcho:=истина;
			кон New;


		кон Frame;

		Window = окласс (WMComponents.FormWindow)
			перем
				panel: WMStandardComponents.Panel;
				frame: Frame;



			проц &New*(C: Connection);
				перем
			нач
				нов(panel);
				panel.bounds.SetWidth(2*Border + 80*BoxW); panel.bounds.SetHeight(2*Border + 24*BoxH);
				нов(frame, C, 80, 24); panel.AddContent(frame);
				frame.alignment.Set(WMComponents.AlignClient);
				Init(panel.bounds.GetWidth(), panel.bounds.GetHeight(), ложь);
				SetContent(panel);
				manager := WMWindowManager.GetDefaultManager();
				manager.Add(100, 100, сам, {WMWindowManager.FlagFrame, WMWindowManager.FlagClose})
			кон New;

			проц {перекрыта}Handle*(перем x: WMMessages.Message);
			нач
				если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) и (x.ext суть KillerMsg) то Close
				иначе Handle^(x)
				всё;
			кон Handle;

			проц {перекрыта}Close*;
			нач
				frame.C.Close();
				Close^();
				DecCount;
			кон Close;
		кон Window;


	перем nofWindows:цел32;



	проц Open*(конст name: массив из симв8; port:цел32; type:цел32; error:Потоки.Писарь):Window;
	перем
		inst: Window;
		title: Strings.String; adr: IP.Adr; res: целМЗ;
		C: TCP.Connection; TC: Connection;
	нач
		если port = 0 то port := 23 всё;
		DNS.HostByName(name, adr, res);
		если res # DNS.Ok то
			error.пСтроку8(name); error.пСтроку8(" invalid address");
			error.пВК_ПС(); возврат НУЛЬ;
		всё;
		нов(C); C.Open(TCP.NilPort, adr, port, res);
		если res # TCP.Ok то
			error.пСтроку8(name); error.пСтроку8(" open failed");
			error.пВК_ПС(); возврат НУЛЬ;
		всё;
		нов(TC, C);
			если type=Telnet.Tcp то TC.flags:={Telnet.Echo}; title:=WMWindowManager.NewString("TCP Terminal");
			аесли type=Telnet.Telnet то TC.flags:={Telnet.Telnet}; title:=WMWindowManager.NewString("Telnet Terminal");
			аесли type=Telnet.VT100 то TC.flags:={Telnet.Telnet};  (*! add VT100?*) title:=WMWindowManager.NewString("VT100 Terminal");
			иначе СТОП(200)
			всё;
		нов(inst, TC); inst.SetTitle(title);
		IncCount();
		возврат inst
	кон Open;

	проц GetRemote(context : Commands.Context; перем name: массив из симв8; перем port:цел32);
	перем
		 sr: Потоки.ЧтецИзСтроки;
	нач
		если (context=НУЛЬ) или  ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name) или ~ context.arg.ПропустиБелоеПолеИЧитайЦел32(port,ложь) то
			если WMDialogs.QueryString("Remote address and port", name) = WMDialogs.ResOk то
				нов(sr,256); sr.ПримиСтроку8ДляЧтения(name);
				если ~sr.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name) или ~ sr.ПропустиБелоеПолеИЧитайЦел32(port,ложь) то
					WMDialogs.Information("Connecting failed", "insufficient info on address, port"); возврат
				всё;
			иначе
				WMDialogs.Information("Connect failed", "insufficient info on address, port"); возврат
			всё;
		всё;
	кон GetRemote;

	проц Start*(context : Commands.Context);
	перем
		name,protocol: массив 256 из симв8; port: цел32;
		window:Window;
	нач
		если (context=НУЛЬ) или ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(protocol) или ((protocol#"Tcp") и  (protocol#"Telnet") и  (protocol#"VT100")) то
			если ~(WMDialogs.QueryString("Please give protocol {Tcp,Telnet,VT100}", protocol) = WMDialogs.ResOk) то возврат всё;
		всё;
		GetRemote(context, name, port);
		если protocol="Tcp" то window:=Open(name, port, Telnet.Tcp, context.error);
		аесли protocol="Telnet" то window:=Open(name, port, Telnet.Telnet, context.error);
		аесли protocol="VT100" то window:=Open(name, port, Telnet.VT100, context.error);
		всё;
	кон Start;

	проц IncCount;
	нач {единолично}
		увел(nofWindows);
	кон IncCount;

	проц DecCount;
	нач {единолично}
		умень(nofWindows);
	кон DecCount;

	проц Cleanup;
	перем die : KillerMsg;
		 msg : WMMessages.Message;
		 m : WMWindowManager.WindowManager;
	нач {единолично}
		нов(die); msg.ext := die; msg.msgType := WMMessages.MsgExt;
		m := WMWindowManager.GetDefaultManager();
		m.Broadcast(msg);
		дождись(nofWindows = 0)
	кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
кон WMVT100.

WMVT100.Open~

WMVT100.Start Tcp "127.0.0.1" 25~ (* TCP terminal, e.g. to test a SMTP server *)
WMVT100.Start Tcp "127.0.0.1" 80~ (* manual access to HTTP server *)
WMVT100.Start Telnet <server> <portNr>~ (* will include Telnet control characters *)
WMVT100.Start VT100 <server> <portNr>~ (* will include Telnet/VT100 control characters *)

System.Free WMVT100 ~

from wikipedia: Telnet
All data octets except 0xff are transmitted over Telnet as is.
Therefore, a Telnet client application may also be used to establish an interactive raw TCP session,
and it is commonly believed that such session which does not use the IAC (0xff, or 255 in decimal) is functionally identical.
This is not the case, however, because there are other network virtual terminal (NVT) rules,
such as the requirement for a bare carriage return character (CR, ASCII 13) to be followed by a NUL (ASCII 0) character or a LF character,
that distinguish the telnet protocol from raw TCP sessions.
Another difference of Telnet from a raw TCP session is that Telnet is not 8-bit clean by default.
8-bit mode may be negotiated, but high-bit-set octets may be garbled until this mode was requested, and it obviously will not be requested in non-Telnet connection.
The standard suggests the interpretation of codes 0000-0176 as ASCII, but does not offer any meaning for high-bit-set data octets.
