модуль StdIO;	(** AUTHOR gf;  PURPOSE "Unix standard IO and argument channels *)

(* Commands.Context for programs running outside Aos *)

использует Modules, Commands, Потоки, Pipes, WinTrace, Kernel32, UCS2, UCS2_2;

конст
	Echo* = ложь;
	пределДлиныКоманднойСтроки˛считаяЗавершающийНоль = 4096;
тип
	String=укль на массив из симв8;
перем
	env-: Commands.Context;

	hin-, hout-, herr-: Kernel32.HANDLE;
	stdInDecoupler: Pipes.ReadDecoupler;


(* См. также Machine.ПолучиКоманднуюСтрокуИзОС *)
проц ПолучиКоманднуюСтрокуИзОС*(перем рез: массив из симв8);	
перем адр: адресВПамяти; строкаUCS2 : UCS2.PString; ю : укль на массив из симв8;
перем пределДлины:цел32;
нач
	адр := Kernel32.GetCommandLineW();
	(* пределДлины для чтения из ОС задаём с запасом; при наличии кириллицы это не влезет в приёмник *)
	пределДлины := длинаМассива(рез)(цел32); 
	(* прочитать с пределом и преобразовать в UCS2 *)
	строкаUCS2 := UCS2_2.LPWSTR_вНовUCS2(адр, пределДлины);
	(* преобразовать в UTF8 в приёмник, падая при переполнении *)
	ю := UCS2.U2vNovU8(строкаUCS2^);
	утв(длинаМассива(рез)>=длинаМассива(ю^));
	копируйСтрокуДо0(ю^,рез) кон ПолучиКоманднуюСтрокуИзОС;


проц Args(): String;
перем size, pos: размерМЗ; str: String;
	cmdLine : массив пределДлиныКоманднойСтроки˛считаяЗавершающийНоль из симв8;
нач
	ПолучиКоманднуюСтрокуИзОС(cmdLine);
	pos := 0;
	нцПока cmdLine[pos] # 0X делай
		увел(pos);
	кц;
	size := pos + 1;
	нов(str, size);
	pos := 0;
	нцПока cmdLine[pos] # 0X делай
		str[pos] := cmdLine[pos];
		увел(pos);
	кц;
	str[pos] := 0X;
	возврат str;
кон Args;

проц Cleanup;
нач
	env.error.ПротолкниБуферВПоток;
	env.out.ПротолкниБуферВПоток
кон Cleanup;

проц Setup;
перем
	arg: Потоки.ЧтецИзСтроки;
	stdin: Потоки.Чтец;
	stdout: Потоки.Писарь;
	errout: Потоки.Писарь;
	str: String;
нач
	WinTrace.OpenConsole;
	str := Args();
	нов( arg, длинаМассива(str) ); arg.ПримиСтроку8ДляЧтения(str^);
	нов(stdInDecoupler, WinTrace.Receive);
	нов( stdin, stdInDecoupler.Receive, 1024 );
	нов( stdout, WinTrace.Send, 1024 );
	нов( errout, WinTrace.SendError, 512 );
	нов( env, stdin, arg, stdout, errout, НУЛЬ );
	Modules.InstallTermHandler( Cleanup )
кон Setup

нач
	Setup
кон  StdIO.

System.DoCommands

	Compiler.Compile  -b=AMD --bits=32 --objectFileExtension=.Gox --symbolFileExtension=.Syx --mergeSections
		I386.Builtins.Mod Trace.Mod Windows.I386.Kernel32.Mod Windows.I386.Machine.Mod Heaps.Mod
		Modules.Mod Windows.I386.Objects.Mod Windows.Kernel.Mod KernelLog.Mod Streams.Mod Commands.Mod
		I386.Reals.Mod Reflection.Mod Locks.Mod Windows.Clock.Mod Files.Mod Dates.Mod Strings.Mod

		Windows.WinTrace.Mod Windows.WinFS.Mod

		Diagnostics.Mod StringPool.Mod BitSets.Mod ObjectFile.Mod
		GenericLinker.Mod Loader.Mod Options.Mod Debugging.Mod

		Windows.StdIO.Mod 		Pipes.Mod Shell.Mod TestStdIO.Mod



		~


	Linker.Link --fileFormat=PE32CUI --fileName=fox.exe --extension=Gox --displacement=401000H
		Builtins Trace Kernel32 Machine Heaps Modules Objects Kernel KernelLog Streams Commands Files
		WinFS Clock Dates Reals Strings Diagnostics BitSets StringPool ObjectFile GenericLinker Reflection Loader
		WinTrace StdIO Pipes Shell TestStdIO ~

		FSTools.CloseFiles fox.exe ~

	~

