модуль UsbPrinter;  (** AUTHOR "staubesv"; PURPOSE "USB Printing Device Class Driver"; *)
(**
 * This is the Bluebottle implementation of the USB Printing Device Class driver. Note that this driver is mainly
 * for transport data between the host and a USB printer device. You will need approriate Page Description Language (PDL)
 * Printer Control Protocol (PCP) support to be able to actually print anything.
 *
 * Usage:
 *	UsbPrinter.Install ~ loads this driver
 *	System.Free UsbPrinter ~ unloads it
 *
 * References:
 *	Universal Serial Bus Device Class Definition for Printing Devices 1.0, www.usb.org
 *
 * History:
 *	01.12.2005 	First release (staubesv)
 *	09.01.2006	Adapted to Usb.Mod changes (staubesv)
 *	05.07.2006	Adapted to Usbdi (staubesv)
 *)

использует НИЗКОУР, ЛогЯдра, Modules, Usbdi;

конст

	Name = "UsbPrinter";
	Description = "USB Printing Device Class Driver";
	Priority = 10;

	Debug = истина;
	Trace = истина;

	StatusPaperEmpty = {5};
	StatusSelected = {4};
	StatusNoError = {3};

	(* Printing devices class specific request codes *)
	PrGetDeviceId = 0;
	PrGetPortStatus = 1;
	PrSoftReset = 2;

	(* Printer interface type *)
	PitUnidirectional = 01H;
	PitBidirectional = 02H;
	Pit1284 = 03H; (* IEEE 1284.4 compatible bi-directional interface *)
	PitVendorSpecific = 0FFH;

тип

	Printer= окласс (Usbdi.Driver)
	перем
		defaultpipe, bulkInPipe, bulkOutPipe : Usbdi.Pipe;

		(* Printer info *)
		interfaceType : цел32;
		deviceId : массив 256 из симв8;

		проц {перекрыта}Connect() : булево;
		нач
			defaultpipe := device.GetPipe(0);
			если defaultpipe = НУЛЬ то
				если Debug то ЛогЯдра.пСтроку8("UsbPrinter: Could not get default pipe."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			bulkOutPipe := device.GetPipe(1);
			если bulkOutPipe = НУЛЬ то
				если Debug то ЛогЯдра.пСтроку8("UsbPrinter: Could not get bulk out pipe."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			если interfaceType # PitUnidirectional то
				bulkInPipe := device.GetPipe(82H);
				если bulkInPipe = НУЛЬ то
					если Debug то ЛогЯдра.пСтроку8("UsbPrinter: Could not get bulk in pipe."); ЛогЯдра.пВК_ПС; всё;
					возврат ложь;
				всё;
			всё;

			если ~GetDeviceId(deviceId, 0,0,0) то
				если Debug то ЛогЯдра.пСтроку8("UsbPrinter: Could not get device ID."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			возврат истина;
		кон Connect;

		проц {перекрыта}Disconnect;
		нач
			ЛогЯдра.пСтроку8("UsbPrinter: "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(" disconnected."); ЛогЯдра.пВК_ПС;
		кон Disconnect;

		(*
		 * This class-specific request returns the device ID strings that is compatible with IEEE1284 for
		 * the specified interface.
		 * @param config Zero-based USB device configuration index
		 * @param intf Zero-based USB device interface index
		 * @param alt Zero-based USB device alternate interface index
		 *)
		проц GetDeviceId(перем deviceId : массив из симв8; config, intf, alt : цел32) : булево;
		перем
			buffer : Usbdi.BufferPtr;
			wValue, wIndex, len, i : цел32;
			string : укль на массив из симв8;
		нач
			wValue := config;
			wIndex := логСдвиг(intf, 8) + alt;
			нов(buffer, 2);
			(* First we get the length of the device ID string (first 2 bytes) *)
			если device.Request(Usbdi.ToHost + Usbdi.Class + Usbdi.Interface, PrGetDeviceId, wValue, wIndex, 2, buffer^) = Usbdi.Ok то
				(* Length is encoded in big endian *)
				len := НИЗКОУР.подмениТипЗначения(цел32, кодСимв8(buffer[0]))*100H + НИЗКОУР.подмениТипЗначения(цел32, кодСимв8(buffer[1]));
				нов(buffer, len);
				если device.Request(Usbdi.ToHost + Usbdi.Class + Usbdi.Interface, PrGetDeviceId, wValue, wIndex, len, buffer^) = Usbdi.Ok то
					нов(string, len-2);
					нцДля i := 0 до len-3 делай string[i] := buffer[i+2]; кц;
					копируйСтрокуДо0(string^, deviceId);
					если Trace то ЛогЯдра.пСтроку8("UsbPrinter: DeviceID: "); ЛогЯдра.пСтроку8(deviceId); ЛогЯдра.пВК_ПС; всё;
					возврат истина;
				аесли Debug то ЛогЯдра.пСтроку8("UsbPrinter: Could not read device ID."); ЛогЯдра.пВК_ПС;
				всё;
			аесли Debug то ЛогЯдра.пСтроку8("UsbPrinter: GetDeviceID's first two bytes  failed."); ЛогЯдра.пВК_ПС;
			всё;
			возврат ложь;
		кон GetDeviceId;

		(*
		 * This class-specific request returns the printer's current status in a format which is compatible with the
		 * status register of a standard parallel port printer.
		 * @param portstatus Will be set to the retrieved status
		 * @return TRUE, if operation succeeded, FALSE otherwise
		 *)
		проц GetPortStatus(перем portstatus : мнвоНаБитахМЗ) : булево;
		перем buffer : Usbdi.BufferPtr;
		нач
			нов(buffer, 1);
			если device.Request(Usbdi.ToHost + Usbdi.Class + Usbdi.Interface, PrGetPortStatus, 0, interface.bInterfaceNumber, 1, buffer^) = Usbdi.Ok то
				portstatus := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, buffer[0]);
				если Trace то ShowPrinterStatus(portstatus); всё;
				возврат истина;
			иначе
				если Debug то ЛогЯдра.пСтроку8("UsbPrinter: GetPortStatus failed."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;
		кон GetPortStatus;

		(*
		 * This class-specific request flushes all buffers and resets the bulk out and bulk in pipes to their
		 * default states. This request will cleas all stall conditions.
		 * @return TRUE, if operation succeeded, FALSE otherwise
		 *)
		проц SoftReset() : булево;
		перем dummy : Usbdi.BufferPtr;
		нач
			нов(dummy, 1);
			если device.Request(Usbdi.ToDevice + Usbdi.Class + Usbdi.Interface, PrSoftReset, 0, interface.bInterfaceNumber, 0, dummy^) = Usbdi.Ok то
				если Trace то ЛогЯдра.пСтроку8("UsbPrinter: Printer resetted."); ЛогЯдра.пВК_ПС; всё;
				возврат истина;
			иначе
				если Debug то ЛогЯдра.пСтроку8("UsbPrinter: SoftReset failed."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;
		кон SoftReset;

	кон Printer;

(* Display textual representation of the printer status register *)
проц ShowPrinterStatus(status : мнвоНаБитахМЗ);
нач
	ЛогЯдра.пСтроку8("UsbPrinter: Printer status: Paper: ");
	если status * StatusPaperEmpty # {} то ЛогЯдра.пСтроку8("Empty"); иначе ЛогЯдра.пСтроку8("Not Empty"); всё;
	ЛогЯдра.пСтроку8(", Selected: ");
	если status * StatusSelected # {} то ЛогЯдра.пСтроку8("Selected"); иначе ЛогЯдра.пСтроку8("Not Selected"); всё;
	ЛогЯдра.пСтроку8(", ");
	если status * StatusNoError # {} то ЛогЯдра.пСтроку8("No Error"); иначе ЛогЯдра.пСтроку8("Error"); всё;
	ЛогЯдра.пВК_ПС;
кон ShowPrinterStatus;

проц Probe(dev : Usbdi.UsbDevice; id : Usbdi.InterfaceDescriptor) : Usbdi.Driver;
перем driver : Printer;
нач
	(* check whether the probed device is a supported USB mouse *)
	если id.bInterfaceClass # 7 то возврат НУЛЬ всё; (* Base class for Printers *)
	если id.bInterfaceSubClass # 1 то возврат НУЛЬ всё; (* Printer *)

	нов(driver); driver.interfaceType := id.bInterfaceProtocol;
	если Trace то ЛогЯдра.пСтроку8("USB Printer found."); ЛогЯдра.пВК_ПС; всё;
	возврат driver;
кон Probe;

проц Cleanup;
нач
	Usbdi.drivers.Remove(Name);
кон Cleanup;

проц Install*;
кон Install;

нач
	Modules.InstallTermHandler(Cleanup);
	Usbdi.drivers.Add(Probe, Name, Description, Priority)
кон UsbPrinter.
