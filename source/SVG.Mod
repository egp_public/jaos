модуль SVG;

использует SVGUtilities, SVGColors, SVGMatrix, XML, CSS2, Strings, Raster, GfxFonts;

конст
(* Constants that determine the type of some paint attribute *)
	PaintNone* = 0;
	PaintCurrentColor* = 1;
	PaintColor* = 2;
	PaintURI* = 3;
(* Constants that determine the type of some units *)
	UnitsUserSpaceOnUse* = 0;
	UnitsObjectBoundingBox* = 1;
(* Constants that determine if percentage values are allowed *)
	AllowPercentages*=истина;
	DisallowPercentages*=ложь;

тип
	Document*=Raster.Image;

	String*=XML.String;

	Number*=SVGMatrix.Number;
	Length*=Number;
	Color*=SVGColors.Color;

	Box*=запись
		x*, y*, width*, height*: Length;
	кон;

	Coordinate*=SVGMatrix.Point;
	Transform*=SVGMatrix.Matrix;

	Paint*=запись
		type*: цел8;
		color*: Color;
		uri*: String;
	кон;

	Style*=запись
		fill*: Paint;
		stroke*: Paint;
		strokeWidth*: Length;
	кон;

	State*=окласс
		перем
			style*: Style;

			target*: Document;

			transparencyUsed*: булево;

			viewport*: Box;
			userToWorldSpace*: Transform;

			next: State;

		(* Push a new copy of the states on a stack *)
		проц Push*;
		перем pushed: State;
		нач
			нов(pushed);

			pushed^ := сам^;
			next := pushed;
		кон Push;

		(* Pop the top state from the stack *)
		проц Pop*;
		нач
			сам^ := next^;
		кон Pop;

	кон State;

(* Load the default style attributes *)
проц InitDefaultStyle*(перем style: Style);
нач
	style.fill.type := PaintColor;
	style.fill.color := SVGColors.Black;
	style.stroke.type := PaintNone;
	style.strokeWidth := 1;
кон InitDefaultStyle;

(* Parse a number at the specified position in the string *)
проц ParseNumber2(value: String; перем number: Number; percentageAllowed: булево; percent100: Length; перем i: размерМЗ; перем unitStr: String);
нач
	SVGUtilities.StrToFloatPos(value^, number, i);
	unitStr := Strings.Substring2(i, value^);
	если unitStr^ = '%' то
		если percentageAllowed то
			number := number*percent100/100.0;
		иначе
			Error("expected number, found percentage: ");
			Error(value^);
		всё
	всё
кон ParseNumber2;

(* Parse a number *)
проц ParseNumber*(value: String; перем number: Number; percentageAllowed: булево; percent100: Length);
перем i: размерМЗ;
	unitStr: String;
нач
	i := 0;
	ParseNumber2(value, number, percentageAllowed, percent100, i, unitStr);
кон ParseNumber;

(* Parse an attribute of type length at the specified position in the string *)
проц ParseLength2(value:String; ppi: вещ64;  percent100: Length; перем length: Length; перем i: размерМЗ);
перем
	term: CSS2.Term;
	unit: цел8;
	unitStr: String;
нач
	ParseNumber2(value, length, AllowPercentages, percent100, i, unitStr);
	если unitStr^ # '%' то
		unit := GetTermUnit(unitStr^);
		если unit # CSS2.Undefined то
			term := ChangeToPixel(length);
			term.SetUnit(unit);
			length := GetPixels(term, ppi, GfxFonts.Default)			(* Use DefaultFont for now... *)
		всё
	всё
кон ParseLength2;

(* Parse an attribute of type length *)
проц ParseLength*(value:String; ppi: вещ64;  percent100: Length; перем length: Length);
перем
	i: размерМЗ;
нач
	i := 0;
	ParseLength2(value,ppi,percent100,length,i)
кон ParseLength;

(* Parse one or optionally two attributes of type length *)
проц ParseLengthOptional2*(value:String; ppi: вещ64;  percent100: Length; перем length, length2: Length);
перем
	i: размерМЗ;
нач
	i := 0;
	ParseLength2(value,ppi,percent100,length,i);
	SVGUtilities.SkipCommaWhiteSpace(i, value);
	если value[i]=0X то
		length2 := length
	иначе
		ParseLength2(value,ppi,percent100,length2,i)
	всё
кон ParseLengthOptional2;

(* Parse a coordinate pair *)
проц ParseCoordinate*(value: String; перем i: размерМЗ;  перем current: Coordinate; relative: булево);
перем x, y: Length;
нач
	SVGUtilities.SkipCommaWhiteSpace(i, value);
	SVGUtilities.StrToFloatPos(value^, x, i);
	SVGUtilities.SkipCommaWhiteSpace(i, value);
	SVGUtilities.StrToFloatPos(value^, y, i);
	если relative то
		current.x := current.x + x;
		current.y := current.y + y;
	иначе
		current.x := x;
		current.y := y;
	всё
кон ParseCoordinate;

(* Parse a single coordinate value *)
проц ParseCoordinate1*(value: String; перем i: размерМЗ;  перем current: Length; relative: булево);
перем l: Length;
нач
	SVGUtilities.SkipCommaWhiteSpace(i, value);
	SVGUtilities.StrToFloatPos(value^, l, i);
	если relative то
		current := current + l;
	иначе
		current := l;
	всё
кон ParseCoordinate1;

(* Parse a paint style attribute *)
проц ParsePaint*(value: String; перем paint: Paint);
нач
	если value^ = "none" то paint.type := PaintNone
	аесли value^ = "currentColor" то paint.type := PaintCurrentColor
	аесли SVGColors.Parse(value, paint.color)  то paint.type := PaintColor
	аесли ParseURI(value, paint.uri) то paint.type := PaintURI
	иначе
		Error("expected paint, found :");
		Error(value^);
		paint.type := PaintNone
	всё
кон ParsePaint;

(* Parse the contents of the xlink:href attribute *)
проц ParseURI*(value: String; перем uri: String):булево;
нач
	если Strings.StartsWith2("url(#", value^) и Strings.EndsWith(")", value^) то
		uri := Strings.Substring(5, Strings.Length(value^)-1, value^);
		возврат истина
	аесли Strings.StartsWith2("#", value^) то
		uri := Strings.Substring2(1, value^);
		возврат истина
	иначе возврат ложь
	всё
кон ParseURI;

(* Parse the contents of the gradientUnits attribute *)
проц ParseUnits*(value: String; перем units: цел8);
нач
	если value^ = "userSpaceOnUse" то units := UnitsUserSpaceOnUse
	аесли value^ = "objectBoundingBox" то units := UnitsObjectBoundingBox
	иначе
		Error("expected userSpaceOnUse or objectBoundingBox, found: ");
		Error(value^)
	всё
кон ParseUnits;

(* Parse the contents of the fill or stroke attributes *)
проц ParseStyle*(style: String; конст name: массив из симв8): String;
перем i, end: размерМЗ;
	id: String;
нач
	i := 0;
	SVGUtilities.SkipWhiteSpace(i, style);
	нцПока style[i] # 0X делай
		end:=Strings.IndexOfByte(':', i, style^);
		если end=-1 то возврат НУЛЬ всё;
		id := Strings.Substring(i, end, style^);
		i := end+1;
		SVGUtilities.SkipWhiteSpace(i, style);

		end:=Strings.IndexOfByte(';', i, style^);
		если end=-1 то
			если id^ = name то
				возврат Strings.Substring2(i, style^);
			всё;
			возврат НУЛЬ
		всё;
		если id^ = name то
			возврат Strings.Substring(i, end, style^);
		всё;
		i := end+1;
		SVGUtilities.SkipWhiteSpace(i, style);
	кц;
	возврат НУЛЬ;
кон ParseStyle;

(* Parse the contents of the transform and gradientTransform attributes *)
проц ParseTransformList*(value: String; перем transform: Transform);
перем
	i, len: размерМЗ;
	a, b, c, d, e, f: Length;
нач
	i := 0;
	len := Strings.Length(value^);
	SVGUtilities.SkipWhiteSpace(i, value);
	нцПока i#len делай
		если Strings.StartsWith("matrix(", i, value^) то
			i := i + 7;
			SVGUtilities.StrToFloatPos(value^, a, i);
			SVGUtilities.SkipCommaWhiteSpace(i, value);
			SVGUtilities.StrToFloatPos(value^, b, i);
			SVGUtilities.SkipCommaWhiteSpace(i, value);
			SVGUtilities.StrToFloatPos(value^, c, i);
			SVGUtilities.SkipCommaWhiteSpace(i, value);
			SVGUtilities.StrToFloatPos(value^, d, i);
			SVGUtilities.SkipCommaWhiteSpace(i, value);
			SVGUtilities.StrToFloatPos(value^, e, i);
			SVGUtilities.SkipCommaWhiteSpace(i, value);
			SVGUtilities.StrToFloatPos(value^, f, i);
			transform := transform.TransformBy(a, b, c, d, e, f)
		аесли Strings.StartsWith("translate(", i, value^) то
			i := i + 10;
			SVGUtilities.StrToFloatPos(value^, a, i);
			SVGUtilities.SkipCommaWhiteSpace(i, value);
			если value[i] # ")" то SVGUtilities.StrToFloatPos(value^, b, i)
			иначе b := 0.0 всё;
			transform := transform.Translate(a, b)
		аесли Strings.StartsWith("scale(", i, value^) то
			i := i + 6;
			SVGUtilities.StrToFloatPos(value^, a, i);
			SVGUtilities.SkipCommaWhiteSpace(i, value);
			если value[i] # ")" то SVGUtilities.StrToFloatPos(value^, b, i)
			иначе b := a всё;
			transform := transform.Scale(a, b)
		аесли Strings.StartsWith("rotate(", i, value^) то
			i := i + 7;
			SVGUtilities.StrToFloatPos(value^, a, i);
			SVGUtilities.SkipCommaWhiteSpace(i, value);
			если value[i] # ")" то
				SVGUtilities.StrToFloatPos(value^, b, i);
				SVGUtilities.SkipCommaWhiteSpace(i, value);
				SVGUtilities.StrToFloatPos(value^, c, i)
			иначе b := 0.0; c := 0.0 всё;
			transform := transform.Rotate(a, b, c)
		аесли Strings.StartsWith("skewX(", i, value^) то
			i := i + 6;
			SVGUtilities.StrToFloatPos(value^, a, i);
			transform := transform.SkewX(a)
		аесли Strings.StartsWith("skewY(", i, value^) то
			i := i + 6;
			SVGUtilities.StrToFloatPos(value^, a, i);
			transform := transform.SkewY(a)
		иначе
			Error("unknown transform command");
			Error(value^);
			возврат
		всё;

		SVGUtilities.SkipWhiteSpace(i, value);
		SVGUtilities.SkipChar(i, value, ')');
		SVGUtilities.SkipCommaWhiteSpace(i,value)
	кц
кон ParseTransformList;

(* Parse the contents of the viewBoxattribute *)
проц ParseViewBox*(value: String; перем minx, miny, width, height: Length);
перем i: размерМЗ;
нач
	i := 0;
	SVGUtilities.SkipWhiteSpace(i, value);
	SVGUtilities.StrToFloatPos(value^, minx, i);
	SVGUtilities.SkipCommaWhiteSpace(i, value);
	SVGUtilities.StrToFloatPos(value^, miny, i);
	SVGUtilities.SkipCommaWhiteSpace(i, value);
	SVGUtilities.StrToFloatPos(value^, width, i);
	SVGUtilities.SkipCommaWhiteSpace(i, value);
	SVGUtilities.StrToFloatPos(value^, height, i);
кон ParseViewBox;

(* Parse the contents of the preserveAspectRatio attribute *)
проц ParsePreserveAspect*(value: String; перем xAlign, yAlign: цел32; перем meet: булево);
перем i: размерМЗ;
нач
	i := 0;

	если Strings.StartsWith("xMin", i, value^) то i := i + 4; xAlign := -1
	аесли Strings.StartsWith("xMid", i, value^) то i := i + 4; xAlign := 0
	аесли Strings.StartsWith("xMax", i, value^) то i := i + 4; xAlign := +1
	иначе
		Error("expected xMin, xMid or xMax, found: ");
		Error(value^);
	всё;

	если Strings.StartsWith("YMin", i, value^) то i := i + 4; yAlign := -1
	аесли Strings.StartsWith("YMid", i, value^) то i := i + 4; yAlign := 0
	аесли Strings.StartsWith("YMax", i, value^) то i := i + 4; yAlign := +1
	иначе
		Error("expected yMin, yMid or yMax, found: ");
		Error(value^);
	всё;

	SVGUtilities.SkipWhiteSpace(i, value);

	если Strings.StartsWith("slice", i, value^) то
		meet := ложь
	аесли Strings.StartsWith("meet", i, value^) то
		meet := истина
	аесли i=Strings.Length(value^) то
		meet := истина
	иначе
		Error("expected meet or slive, found: ");
	всё
кон ParsePreserveAspect;

(* Create a new, empty SVG.Document *)
проц NewDocument*(width, height: Length):Document;
перем
	doc: Document;
нач
	нов(doc);
	Raster.Create(doc,округлиВниз(width),округлиВниз(height),Raster.BGRA8888);
	Raster.Clear(doc);
	возврат doc;
кон NewDocument;

(* The following procedures are forwarded to SVGUtilities for convenience *)
проц Log*(конст msg: массив из симв8);
нач
	SVGUtilities.Log(msg)
кон Log;

проц Warning*(конст msg: массив из симв8);
нач
	SVGUtilities.Warning(msg)
кон Warning;

проц Error*(конст msg: массив из симв8);
нач
	SVGUtilities.Error(msg)
кон Error;

(* The following procedures are defined in, but not exported from the Modules CSS2Properties and CSS2Parser *)
проц GetPixels(term: CSS2.Term; ppi: вещ64; font: GfxFonts.Font): вещ64;
перем fact, pixels: вещ64; x, y, dx, dy: вещ32; map: Raster.Image;
нач
	если (term # НУЛЬ) и term.IsLength() то
		просей term.GetUnit() из
		| CSS2.em: fact := font.ptsize / ppi
		| CSS2.ex: GfxFonts.GetMap(font, 'x', x, y, dx, dy, map); fact := -y / ppi
		| CSS2.px: fact := 1.0 / ppi
		| CSS2.in: fact := 1.0
		| CSS2.cm: fact := 1.0 / 2.54
		| CSS2.mm: fact := 1.0 / 25.4
		| CSS2.pt: fact := 1.0 / 72.0
		| CSS2.pc: fact := 1.0 / 6.0
		всё;
		если term.GetType() = CSS2.IntDimension то pixels := term.GetIntVal() * ppi * fact
		аесли term.GetType() = CSS2.RealDimension то pixels := term.GetRealVal() * ppi * fact
		всё
	аесли (term # НУЛЬ) и (((term.GetType() = CSS2.IntNumber) и (term.GetIntVal() = 0))
			или ((term.GetType() = CSS2.RealNumber) и (term.GetRealVal() = 0.0))) то
		pixels := 0.0
	иначе
		pixels := 0.0
	всё;
	возврат pixels
кон GetPixels;

проц ChangeToPixel(pixelVal: вещ64): CSS2.Term;
перем term: CSS2.Term;
нач
	нов(term); term.SetType(CSS2.RealDimension); term.SetRealVal(pixelVal); term.SetUnit(CSS2.px); возврат term
кон ChangeToPixel;

проц GetTermUnit(конст unitStr: массив из симв8): цел8;
нач
	если unitStr = 'em' то возврат CSS2.em
	аесли unitStr = 'ex' то возврат CSS2.ex
	аесли unitStr = 'px' то возврат CSS2.px
	аесли unitStr = 'in' то возврат CSS2.in
	аесли unitStr = 'cm' то возврат CSS2.cm
	аесли unitStr = 'mm' то возврат CSS2.mm
	аесли unitStr = 'pt' то возврат CSS2.pt
	аесли unitStr = 'pc' то возврат CSS2.pc
	аесли unitStr = 'deg' то возврат CSS2.deg
	аесли unitStr = 'grad' то возврат CSS2.grad
	аесли unitStr = 'rad' то возврат CSS2.rad
	аесли unitStr = 'ms' то возврат CSS2.ms
	аесли unitStr = 's' то возврат CSS2.s
	аесли unitStr = 'Hz' то возврат CSS2.Hz
	аесли unitStr = 'kHz' то возврат CSS2.kHz
	иначе возврат CSS2.Undefined
	всё
кон GetTermUnit;

кон SVG.
