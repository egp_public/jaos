модуль YMF754Util;

использует
	Commands, Потоки, Codecs, Files, Strings;

конст
	FNameInstRAM = "YMF754.Bin";

проц Open* (context : Commands.Context);
перем
	name, str: массив 256 из симв8;
	in: Потоки.Чтец;
	w, binW: Files.Writer;
	token: массив 1024 из симв8;
	f, binF: Files.File;
	c, l: цел32; res: целМЗ;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name);
	c := 1;

	in := Codecs.OpenInputStream (name);
	если in # НУЛЬ то

		Strings.Concat (name, ".Bin", str);
		binF := Files.New (FNameInstRAM);
		Files.OpenWriter (binW, binF, 0);

		нцДо

			in.ПропустиБелоеПоле;
			in.чЦепочкуСимв8ДоБелогоПоля (token);

			если Strings.Pos ("{", token) # -1 то
				Strings.IntToStr (c, str); увел (c);
				Strings.Concat (name, str, str);
				Strings.Concat (str, ".txt", str);
				f := Files.New (str);
				Files.OpenWriter (w, f, 0);

				Strings.Concat ("Creating file: ", str, str);
				context.out.пСтроку8(str); context.out.пВК_ПС;
			всё;

			если Strings.Pos ("0x", token) = 0 то
				Strings.Delete (token, 0, 2);
				Strings.TrimRight (token, ",");

				если w # НУЛЬ то
					w.пСтроку8 (token); w.пВК_ПС;
				всё;

				если (w # НУЛЬ) и (binW # НУЛЬ) то
					Strings.HexStrToInt (token, l, res);
					если res = Strings.Ok то
						binW.пЦел32_мз (l);
					иначе
						context.error.пСтроку8("Error!!!"); context.error.пВК_ПС;
					всё;
				всё;

			всё;

			если Strings.Pos ("}", token) # -1 то
				context.out.пСтроку8("Closing file"); context.out.пВК_ПС;
				w.ПротолкниБуферВПоток;
				Files.Register (f);
			всё;

		кцПри in.кодВозвратаПоследнейОперации = Потоки.КонецФайла;

		binW.ПротолкниБуферВПоток;
		Files.Register (binF);
	всё;
кон Open;

кон YMF754Util.

System.Free YMF754Util~
YMF754Util.Open 724hwmcode.c~

Tar.Create Sage.YMF754.tar
	Sage.YMF754.Mod
	YMF754.Bin
	Sage.DriverDatabase.XML
	Sage.YMF754Util.Mod
	724hwmcode.c
	Sage.YMF754.txt
~


