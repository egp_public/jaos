(* Runtime support for high precision timer *)
(* Copyright (C) Florian Negele *)

модуль Timer;

использует Unix;

конст Clock = Unix.CLOCK_MONOTONIC;

тип Counter* = длинноеЦелМЗ;

проц GetCounter- (): Counter;
перем timespec: Unix.Timespec; result: Counter;
нач {безКооперации, безОбычныхДинПроверок}
	утв (Unix.clock_gettime (Clock, операцияАдресОт timespec) = 0);
	result := timespec.tv_sec; result := result * 1000000; увел (result, timespec.tv_nsec DIV 1000); возврат result;
кон GetCounter;

проц GetFrequency- (): Counter;
перем timespec: Unix.Timespec;
нач {безКооперации, безОбычныхДинПроверок}
	если Unix.clock_getres (Clock, операцияАдресОт timespec) # 0 то возврат 0 всё;
	утв ((timespec.tv_sec = 0) и (timespec.tv_nsec = 1)); возврат 1000000;
кон GetFrequency;

кон Timer.
