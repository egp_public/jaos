модуль VirtualDisks; (** AUTHOR "staubesv"; PURPOSE "Virtual disk driver"; *)
(**
 * The virtual disk driver installs disk images as virtual disk drives.
 *
 * Usage:
 *
 *	VirtualDisks.Create [Options] filename nbrOfBlocks ~ creates a empty file for being use as file disk
 *
 *	VirtualDisks.Install [Options] diskname filename  ~ installs file <filename> as file disk
 *	VirtualDisks.InstallRamdisk [Options] diskname size ~ installs and creates a ram disk
 *	VirtualDisks.Uninstall diskname ~
 *
 *	System.Free VirtualDisks ~
 *
 *)

использует
	НИЗКОУР,
	Commands, Options, Plugins, Modules, Потоки, Disks, Files, Strings;

конст
	BlockNumberInvalid* = 101;
	ShortTransfer* = 102;

	DefaultBlocksize = 512;

тип

	VirtualDisk = окласс(Disks.Device)
	перем
		size : цел32;

		(* virtual disk geometry CHS *)
		cyls, hds, spt : цел32;

		проц {перекрыта}Transfer*(op, block, num: цел32; перем data: массив из симв8; ofs: размерМЗ; перем res: целМЗ);
		нач {единолично}
			если (block < 0) или (num < 1) или (block + num > size) то res := BlockNumberInvalid; возврат; всё;
			утв((ofs >= 0) и (ofs + num * blockSize <= длинаМассива(data)));
			утв( num * blockSize > 0);

			TransferOperation(op, block, num, data, ofs, res);

			если Disks.Stats то
				если op = Disks.Read то
					увел(NnofReads);
					если (res = Disks.Ok) то увел(NbytesRead, num * blockSize);
					иначе увел(NnofErrors);
					всё;
				аесли op = Disks.Write то
					увел(NnofWrites);
					если (res = Disks.Ok) то увел(NbytesWritten, num * blockSize);
					иначе увел(NnofErrors);
					всё;
				иначе
					увел(NnofOthers);
				всё;
			всё;
		кон Transfer;

		проц TransferOperation(op, block, num: цел32; перем data: массив из симв8; ofs: размерМЗ; перем res: целМЗ);
		нач
			СТОП(301); (* abstract *)
		кон TransferOperation;

		проц {перекрыта}GetSize*(перем size: цел32; перем res: целМЗ);
		нач
			size := сам.size;	res := Disks.Ok;
		кон GetSize;

		проц {перекрыта}Handle*(перем msg : Disks.Message; перем res : целМЗ);
		нач
			если (msg суть Disks.GetGeometryMsg) и (cyls > 0) то
				просейТип msg: Disks.GetGeometryMsg делай
					msg.cyls := сам.cyls; msg.hds := сам.hds; msg.spt := сам.spt; res := Disks.Ok
				всё
			иначе
				res := Disks.Unsupported
			всё
		кон Handle;

		проц &Init(конст name : массив из симв8; blockSize, cyls, hds, spt : цел32);
		нач
			сам.blockSize := blockSize;
			сам.cyls := cyls; сам.hds := hds; сам.spt := spt;
			включиВоМнвоНаБитах(flags, Disks.Removable);
			SetName(name);
		кон Init;

	кон VirtualDisk;

тип

	FileDisk = окласс(VirtualDisk)
	перем
		file : Files.File;
		rider : Files.Rider;

		проц {перекрыта}TransferOperation(op, block, num: цел32; перем data: массив из симв8; ofs: размерМЗ; перем res: целМЗ);
		нач
			file.Set(rider, НИЗКОУР.подмениТипЗначения (цел32, block * blockSize));
			если rider.res # Files.Ok то res := BlockNumberInvalid; возврат; всё;

			если op = Disks.Read то
				file.ReadBytes(rider, data, ofs, num * blockSize);
				если rider.res # 0 то res := ShortTransfer; иначе res := Disks.Ok; всё;
			аесли op = Disks.Write то
				file.WriteBytes(rider, data, ofs, num * blockSize);
				file.Update;
				если rider.res # 0 то res := ShortTransfer; иначе res := Disks.Ok; всё;
			иначе
				res := Disks.Unsupported;
			всё;
		кон TransferOperation;

		проц &New*(file : Files.File; конст name, filename : массив из симв8; blockSize, cyls, hds, spt : цел32);
		нач
			утв(file # НУЛЬ);
			утв(file.Length() остОтДеленияНа blockSize = 0);
			Init(name, blockSize, cyls, hds, spt);
			сам.file := file;
			сам.size := file.Length()(цел32) DIV blockSize;
			desc := "Virtual Disk for file "; Strings.Append(desc, filename);
		кон New;

	кон FileDisk;

тип

	MemoryBlock = укль на массив из симв8;

	RamDisk = окласс(VirtualDisk)
	перем
		memory : MemoryBlock;

		проц {перекрыта}TransferOperation(op, block, num: цел32; перем data: массив из симв8; ofs: размерМЗ; перем res: целМЗ);
		нач
			если op = Disks.Read то
				утв((block  + num) * blockSize <= длинаМассива(memory));
				НИЗКОУР.копируйПамять(адресОт(memory[0]) + block * blockSize, адресОт(data[ofs]), num * blockSize);
				res := Disks.Ok;
			аесли op = Disks.Write то
				утв((block + num) * blockSize <=длинаМассива(memory));
				НИЗКОУР.копируйПамять(адресОт(data[ofs]), адресОт(memory[0]) + block * blockSize, num * blockSize);
				res := Disks.Ok;
			иначе
				res := Disks.Unsupported;
			всё;
		кон TransferOperation;

		проц &New*(memory : MemoryBlock;  конст name : массив из симв8; blockSize, cyls, hds, spt : цел32);
		нач
			утв(memory # НУЛЬ);
			утв(длинаМассива(memory) остОтДеленияНа blockSize = 0);
			Init(name, blockSize, cyls, hds, spt);
			сам.memory := memory;
			сам.size := длинаМассива(memory)(цел32) DIV blockSize;
			desc := "Ramdisk";
		кон New;

	кон RamDisk;

(** Create an empty virtual disk *)
проц Create*(context : Commands.Context); (** [Options] filename nbrOfBlocks ~ *)
перем
	options : Options.Options;
	filename : массив 256 из симв8; nbrOfBlocks, blocksize : цел32;
	file : Files.File; rider : Files.Rider;
	buffer : укль на массив из симв8;
	i : цел32;
нач
	нов(options);
	options.Add("b", "blocksize", Options.Integer);
	если options.Parse(context.arg, context.error) то
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename);
		context.arg.ПропустиБелоеПоле; context.arg.чЦел32(nbrOfBlocks, ложь);
		если ~options.GetInteger("blocksize", blocksize) то blocksize := DefaultBlocksize; всё;
		если (filename # "") то
			если (nbrOfBlocks > 0) то
				file := Files.New(filename);
				если file # НУЛЬ то
					context.out.пСтроку8("Creating virtual disk '"); context.out.пСтроку8(filename); context.out.пСтроку8("' ... ");
					context.out.ПротолкниБуферВПоток;
					нов(buffer, blocksize);
					file.Set(rider, 0);
					нцДля i := 0 до nbrOfBlocks - 1 делай
						file.WriteBytes(rider, buffer^, 0, blocksize);
						если rider.res # 0 то
							context.error.пСтроку8("Error: Could not write bytes to file"); context.error.пВК_ПС;
							context.result := Commands.CommandError;
							возврат;
						всё;
					кц;
					Files.Register(file);
					context.out.пСтроку8("done."); context.out.пВК_ПС;
				иначе
					context.error.пСтроку8("Could not create file '"); context.error.пСтроку8(filename); context.error.пСтроку8("'"); context.error.пВК_ПС;
					context.result := Commands.CommandError;
				всё;
			иначе
				context.error.пСтроку8("nbrOfBlocks parameter expected."); context.error.пВК_ПС;
				context.result := Commands.CommandParseError;
			всё;
		иначе
			context.error.пСтроку8("filename parameter expected."); context.error.пВК_ПС;
			context.result := Commands.CommandParseError;
		всё;
	иначе
		context.result := Commands.CommandParseError;
	всё;
кон Create;

проц GetOptions(context : Commands.Context; перем blocksize, cylinders, heads, sectors : цел32) : булево;
перем options : Options.Options;
нач
	нов(options);
	options.Add("b", "blocksize", Options.Integer);
	options.Add("c", "cylinders", Options.Integer);
	options.Add("h", "heads", Options.Integer);
	options.Add("s", "sectors", Options.Integer);
	если options.Parse(context.arg, context.error) то
		(* disk geometry in CHS *)
		если ~options.GetInteger("blocksize", blocksize) то blocksize := DefaultBlocksize; всё;
		если ~options.GetInteger("cylinders", cylinders) то cylinders := 0; всё;
		если ~options.GetInteger("heads", heads) то heads := 0; всё;
		если ~options.GetInteger("sectors", sectors) то sectors := 0; всё;
		возврат истина;
	иначе
		возврат ложь;
	всё;
кон GetOptions;

(** Add file as virtual disk *)
проц Install*(context : Commands.Context); (** [Options] diskname filename ~ *)
перем
	diskname, filename : массив 256 из симв8;
	blocksize, c, h, s: цел32; res: целМЗ;
	file : Files.File;
	disk : FileDisk;

	проц ShowUsage(out : Потоки.Писарь);
	нач
		out.пСтроку8("VirtualDisks.Install [Options] diskname filename ~"); out.пВК_ПС;
	кон ShowUsage;

нач
	если GetOptions(context, blocksize, c, h, s) то
		diskname := "";
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(diskname);
		filename := "";
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename);
		если (diskname = "") или (filename = "") то ShowUsage(context.out); возврат; всё;

		file := Files.Old(filename);
		если file # НУЛЬ то
			если file.Length() остОтДеленияНа blocksize # 0 то
				context.error.пСтроку8("File size must be multiple of blocksize"); context.error.пВК_ПС;
				возврат;
			всё;
			нов(disk, file, diskname, filename, blocksize, c, h, s);
			Disks.registry.Add(disk, res);
			если res = Plugins.Ok то
				context.out.пСтроку8("Disk "); context.out.пСтроку8(diskname); context.out.пСтроку8(" registered");
				если (s # 0) то
					context.out.пСтроку8(" (CHS: ");
					context.out.пЦел64(c, 0); context.out.пСимв8("x"); context.out.пЦел64(h, 0); context.out.пСимв8("x");
					context.out.пЦел64(s, 0); context.out.пСимв8(")");
				всё;
				context.out.пВК_ПС;
			иначе
				context.error.пСтроку8("Could not register disk, res: "); context.error.пЦел64(res, 0); context.error.пВК_ПС;
				context.result := Commands.CommandError;
			всё;
		иначе
			context.error.пСтроку8(filename); context.error.пСтроку8(" not found"); context.out.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	иначе
		context.result := Commands.CommandParseError;
	всё;
кон Install;

(** Add file as virtual disk *)
проц InstallRamdisk*(context : Commands.Context); (** [Options] diskname size  ~ *)
перем
	diskname  : массив 256 из симв8;
	size, blocksize, c, h, s: цел32; res: целМЗ;
	memory : MemoryBlock;
	disk : RamDisk;

	проц ShowUsage(out : Потоки.Писарь);
	нач
		out.пСтроку8("VirtualDisks.InstallRamdisk [Options] diskname size ~"); out.пВК_ПС;
	кон ShowUsage;

нач
	если GetOptions(context, blocksize, c, h, s) то
		diskname := "";
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(diskname);
		context.arg.ПропустиБелоеПоле; context.arg.чЦел32(size, ложь);
		если (diskname = "") или (size < 10)  то ShowUsage(context.out); возврат; всё;

		нов(memory, size * blocksize);
		нов(disk, memory, diskname, blocksize, c, h, s);
		Disks.registry.Add(disk, res);
		если res = Plugins.Ok то
			context.out.пСтроку8("Disk "); context.out.пСтроку8(diskname); context.out.пСтроку8(" registered");
			если (s # 0) то
				context.out.пСтроку8(" (CHS: ");
				context.out.пЦел64(c, 0); context.out.пСимв8("x"); context.out.пЦел64(h, 0); context.out.пСимв8("x");
				context.out.пЦел64(s, 0); context.out.пСимв8(")");
			всё;
			context.out.пВК_ПС;
		иначе
			context.error.пСтроку8("Could not register disk, res: "); context.error.пЦел64(res, 0); context.error.пВК_ПС;
			context.result := Commands.CommandError;
		всё;
	иначе
		context.result := Commands.CommandParseError;
	всё;
кон InstallRamdisk;

(** Remove virtual disk *)
проц Uninstall*(context : Commands.Context); (** diskname ~ *)
перем diskname : Plugins.Name; plugin : Plugins.Plugin;

	проц IsMounted(dev: Disks.Device): булево;
	перем i: размерМЗ;
	нач
		если dev.table # НУЛЬ то
			нцДля i := 0 до длинаМассива(dev.table)-1 делай
				если Disks.Mounted в dev.table[i].flags то возврат истина всё
			кц
		всё;
		возврат ложь
	кон IsMounted;

нач
	context.arg.ПропустиБелоеПоле;
	context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(diskname); context.out.пСтроку8(diskname);
	plugin := Disks.registry.Get(diskname);
	если plugin # НУЛЬ то
		если ~IsMounted(plugin(VirtualDisk)) то
			Disks.registry.Remove(plugin);
			context.out.пСтроку8(" removed");
		иначе
			context.out.пСтроку8(" is mounted.");
		всё;
	иначе
		context.out.пСтроку8(" not found");
		context.result := Commands.CommandError;
	всё;
	context.out.пВК_ПС;
кон Uninstall;

проц Cleanup;
перем disks : Plugins.Table; i : размерМЗ;
нач
	Disks.registry.GetAll(disks);
	если (disks # НУЛЬ) то
		нцДля i := 0 до длинаМассива(disks)-1 делай
			если (disks[i] суть VirtualDisk) то
				Disks.registry.Remove(disks[i]);
			всё;
		кц;
	всё;
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
кон VirtualDisks.

VirtualDisks.Create Test.Dsk 163840 ~
VirtualDisks.Install VDISK0 Test.Dsk 512 ~
VirtualDisks.Uninstall VDISK0 ~

VirtualDisks.InstallRamdisk RAMDISK 120000 ~
VirtualDisks.Uninstall RAMDISK ~

VirtualDisks.Install AosCD.iso 2048 ~

System.Free VirtualDisks ~
