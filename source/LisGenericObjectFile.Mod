модуль LisGenericObjectFile; (** AUTHOR "negelef"; PURPOSE "Generic Object File Writer"; *)

использует
	Потоки, Commands, Basic := FoxBasic, Formats := LisFormats, Sections := LisSections, IntermediateCode := LisIntermediateCode,
	SyntaxTree := LisSyntaxTree, BinaryCode := LisBinaryCode,
	Fingerprinter := LisFingerprinter, Files, Options, ObjectFile, SymbolFileFormat := LisTextualSymbolFile, Strings, ЛогЯдра, D := Debugging;

конст
	Trace = ложь;
	TraceAliases = ложь;
	WarnDuplicateFingerprints = ложь;

	(* optimizations *)
	PatchFixups = истина; (* patch all fixups that can be processed during object file generation *)
	AliasOnlyExported = истина;  (* create an alias only for exported sections *)

тип
	Unit = BinaryCode.Unit;

	ObjectFileFormat* = окласс (Formats.ObjectFileFormat)

		перем binary: булево; mergeSections: булево;

		проц & InitObjectFileFormat;
		нач
			Init; SetExtension(ObjectFile.DefaultExtension);
		кон InitObjectFileFormat;

		проц {перекрыта}Export* (module: Formats.GeneratedModule; symbolFileFormat: Formats.SymbolFileFormat): булево;
		перем fileName: Files.FileName; file: Files.File; writer: Files.Writer; fingerprinter: Fingerprinter.Fingerprinter; poolMap: ObjectFile.PoolMap;

			проц ExportSection (section: IntermediateCode.Section): булево;
			перем name: массив 256 из симв8; (* debugging *)
			нач
				(*
				IF section.IsExternal() OR (section.symbol # NIL) & (section.symbol.scope # NIL) & (section.symbol.scope.ownerModule # module(Sections.Module).module) THEN
					(* nothing to do *)
				ELSE
				*)
					если section.resolved = НУЛЬ то
						Basic.SegmentedNameToString(section.name, name);
						D.String('"section.resolved = NIL" for '); D.String(name); D.Ln;
						возврат ложь
					всё;
					section.resolved.os.identifier.fingerprint := GetFingerprint (section, fingerprinter);
					CopyFixups (НУЛЬ, section.resolved, section.resolved, 0);
					ObjectFile.WriteSection(writer,section.resolved.os,binary, poolMap);
				(*
				END;
				*)
				возврат истина
			кон ExportSection;

			проц MergeSections (l0module: Sections.Module): булево;
			перем
				section: Sections.Section;
				i: размерМЗ;
				sname: Basic.SegmentedName;
				codeAlign, dataAlign, constAlign: Unit;
				codeUnit, dataUnit, constUnit: BinaryCode.Bits;
				resolved, codeSection, dataSection, constSection: BinaryCode.Section;
				alias: BinaryCode.Alias;
				irSection: IntermediateCode.Section;
				exported: булево;
				sections: Sections.SectionList;
				prefix: SyntaxTree.СтрокаИдентификатора;
				export: размерМЗ;
			нач
				sections := l0module.allSections;
				нцДля export := 0 до l0module.exports.Length()-1 делай
					prefix := l0module.exports.GetName(export);
					codeSection := НУЛЬ; dataSection := НУЛЬ; constSection := НУЛЬ;
					codeAlign := 0; dataAlign := 0; constAlign := 0;
					codeUnit := 0; dataUnit := 0; constUnit := 0;
					нцДля i := 0 до sections.Length() - 1 делай

						section := sections.GetSection(i);
						sname := prefix;
						если Basic.IsPrefix(sname, section.name) то
							resolved := section(IntermediateCode.Section).resolved;
							если resolved = НУЛЬ то возврат ложь всё;

							если (resolved # НУЛЬ)  и (resolved.pc # 0) и (~resolved.os.fixed) то
								если section.type = ObjectFile.Code то
									codeAlign := CommonAlignment(codeAlign, resolved.os.alignment);
									утв((codeUnit=0) или (codeUnit = resolved.os.unit));
									codeUnit := resolved.os.unit;
								аесли section.type = ObjectFile.Data то
									dataAlign := CommonAlignment(dataAlign, resolved.os.alignment);
									утв((dataUnit=0) или (dataUnit = resolved.os.unit));
									dataUnit := resolved.os.unit;
								аесли section.type = ObjectFile.Const то
									constAlign := CommonAlignment(constAlign, resolved.os.alignment);
									утв((constUnit=0) или (constUnit = resolved.os.unit));
									constUnit := resolved.os.unit;
								всё;
							всё;
						всё;
					кц;
					если codeUnit > 0 то
						sname := prefix;
						Basic.AppendToSegmentedName(sname,".@CodeSections");
						codeSection := BinaryCode.NewBinarySection(ObjectFile.Code, codeUnit, sname, ложь, ложь);
						codeSection.SetAlignment(ложь,codeAlign);
					всё;
					если dataUnit > 0 то
						sname := prefix;
						Basic.AppendToSegmentedName(sname,".@DataSections");
						dataSection := BinaryCode.NewBinarySection(ObjectFile.Data, dataUnit, sname, ложь, ложь);
						dataSection.SetAlignment(ложь,dataAlign);
					всё;
					если constUnit > 0 то
						sname := prefix;
						Basic.AppendToSegmentedName(sname,".@ConstSections");
						constSection := BinaryCode.NewBinarySection(ObjectFile.Const, constUnit, sname, ложь, ложь);
						constSection.SetAlignment(ложь,constAlign);
					всё;
					(*TRACE(codeAlign, dataAlign, constAlign);*)
					(*codeAlign := 0; dataAlign := 0; constAlign := 0;*)
					нцДля i := 0 до sections.Length() - 1 делай
						section := sections.GetSection(i);
						sname := prefix;
						если Basic.IsPrefix(sname, section.name) то
							resolved := section(IntermediateCode.Section).resolved;
							exported := section(IntermediateCode.Section).exported;
							если (resolved # НУЛЬ) и (resolved.pc # 0) и (~resolved.os.fixed)  то
								если section.type = ObjectFile.Code то
									если resolved.os.alignment # 0 то
									codeSection.Align(resolved.os.alignment);
									всё;
									resolved.os.identifier.fingerprint := GetFingerprint (section, fingerprinter);

									нов(alias, resolved.os.identifier, codeSection.pc);
									если ~AliasOnlyExported или exported то  codeSection.aliasList.AddAlias(alias) всё;
									section(IntermediateCode.Section).SetAlias(codeSection, codeSection.pc);
									codeSection.CopyBits(resolved.os.bits,0, resolved.pc*codeUnit);

								аесли section.type = ObjectFile.Data то
									если resolved.os.alignment # 0 то
									dataSection.Align(resolved.os.alignment);
									всё;
									resolved.os.identifier.fingerprint := GetFingerprint (section, fingerprinter);
									нов(alias, resolved.os.identifier, dataSection.pc);
									если ~AliasOnlyExported или exported то dataSection.aliasList.AddAlias(alias) всё;
									section(IntermediateCode.Section).SetAlias(dataSection, dataSection.pc);
									dataSection.CopyBits(resolved.os.bits,0, resolved.pc*dataUnit );
								аесли section.type = ObjectFile.Const то
									если resolved.os.alignment # 0 то
									constSection.Align(resolved.os.alignment);
									всё;
									resolved.os.identifier.fingerprint := GetFingerprint (section, fingerprinter);
									нов(alias, resolved.os.identifier, constSection.pc);
									если ~AliasOnlyExported или exported то constSection.aliasList.AddAlias(alias) всё;
									section(IntermediateCode.Section).SetAlias(constSection, constSection.pc);
									constSection.CopyBits(resolved.os.bits,0, resolved.pc*constUnit);
								всё;
							всё;
						всё;
					кц;

					нцДля i := 0 до sections.Length() - 1 делай
						section := sections.GetSection(i);
						sname := prefix;
						если Basic.IsPrefix(sname, section.name) то
							resolved := section(IntermediateCode.Section).resolved;
							exported := section(IntermediateCode.Section).exported;
							если (section(IntermediateCode.Section).alias # НУЛЬ) то
								CopyFixups(sections, resolved, section(IntermediateCode.Section).alias, section(IntermediateCode.Section).aliasOffset);
							иначе
								CopyFixups(sections, resolved, resolved,0);
							всё;
						всё;
					кц;

					если codeSection # НУЛЬ то
						UpdateAliases (codeSection, fingerprinter);
						irSection := IntermediateCode.NewSection(sections, цел8(codeSection.os.type), codeSection.os.identifier.name, НУЛЬ, ложь);
						irSection.SetResolved(codeSection);
					всё;
					если dataSection # НУЛЬ то
						UpdateAliases (dataSection, fingerprinter);
						irSection := IntermediateCode.NewSection(sections, цел8(dataSection.os.type), dataSection.os.identifier.name, НУЛЬ, ложь);
						irSection.SetResolved(dataSection);
					всё;
					если constSection # НУЛЬ то
						UpdateAliases (constSection, fingerprinter);
						irSection := IntermediateCode.NewSection(sections, цел8(constSection.os.type), constSection.os.identifier.name, НУЛЬ, ложь);
						irSection.SetResolved(constSection);
					всё;

				кц;
				возврат истина;
			кон MergeSections;

			проц ExportSections (sections: Sections.SectionList): булево;
			перем
				section, test: Sections.Section;
				i, j: размерМЗ;
				name: ObjectFile.SectionName;
				msg: массив 256 из симв8;
			нач

				нцДля i := 0 до sections.Length() - 1 делай
					section := sections.GetSection(i);
					если (section(IntermediateCode.Section).resolved # НУЛЬ) и (section(IntermediateCode.Section).alias = НУЛЬ)  то
						если ~ExportSection(section(IntermediateCode.Section)) то возврат ложь всё;
						если  WarnDuplicateFingerprints и  (section(IntermediateCode.Section).resolved.os.identifier.fingerprint # 0) то
							нцДля j := 0 до i - 1 делай
								test := sections.GetSection(j);
								если (test(IntermediateCode.Section).resolved # НУЛЬ)
								и  (test(IntermediateCode.Section).resolved.os.identifier.fingerprint = section(IntermediateCode.Section).resolved.os.identifier.fingerprint) то
									msg := "duplicate fingerprints: ";
									ObjectFile.SegmentedNameToString(section(IntermediateCode.Section).resolved.os.identifier.name,name);
									Strings.Append(msg, name);
									Strings.Append(msg, ", ");
									ObjectFile.SegmentedNameToString(test(IntermediateCode.Section).resolved.os.identifier.name,name);
									Strings.Append(msg, name);
									Basic.Warning(diagnostics, module.moduleName,Basic.invalidPosition, msg);
								всё
							кц
						всё
					всё
				кц;
				возврат истина
			кон ExportSections;

			проц ExportModule (l1module: Sections.Module): булево;
			перем result: булево;
				offers, requires: ObjectFile.NameList;
				numImports: цел32;
				name: ObjectFile.SectionName;
				import: SyntaxTree.ОбъявлениеИмпортаМодуля;
				i: размерМЗ;
			нач
				если mergeSections и ~MergeSections(l1module) то возврат ложь всё;

				нов (offers, l1module.exports.Length());
				нцДля i := 0 до l1module.exports.Length()-1 делай
					offers[i] := l1module.exports.GetName(i);
				кц;

				если l1module.module#НУЛЬ то
					import := l1module.module.внутренняяОбластьВидимостиМодуля.первыйИмпорт;
					numImports := 0;
					нцПока import # НУЛЬ делай
						если import.непосредственноИмпортируетсяЛи¿ и (import.параметрыШаблона = НУЛЬ) то увел(numImports) всё;
						import := import.следщОИМ;
					кц;
					нов(requires, numImports);
					numImports := 0;
					import := l1module.module.внутренняяОбластьВидимостиМодуля.первыйИмпорт;
					нцПока import # НУЛЬ делай
						если import.непосредственноИмпортируетсяЛи¿ и (import.параметрыШаблона = НУЛЬ) то
							import.деревоМодуля.ДайИмяВВидеСтроки(name);
							requires[numImports] := name;
							увел(numImports);
						всё;
						import := import.следщОИМ;
					кц;


				всё;
				WriteHeader(writer,binary,l1module.allSections, poolMap, offers, requires, fingerprinter);
				result := ExportSections (l1module.allSections);
				возврат result
			кон ExportModule;

		нач
			если Trace то D.String(">>> export generic object file"); D.Ln всё;

			если ~(module суть Sections.Module) то
				Basic.Error (diagnostics, module.moduleName, Basic.invalidPosition,  "generated module format does not match object file format");
				возврат ложь;
			всё;

			если path # "" то Files.JoinPath (path, module.moduleName, fileName); иначе копируйСтрокуДо0 (module.moduleName, fileName); всё;
			Files.JoinExtension (fileName, extension, fileName);

			если Trace то D.String(">>> filename: "); D.String(fileName); D.Ln всё;

			file := Files.New (fileName);
			если file = НУЛЬ то
				Basic.Error(diagnostics, module.moduleName,Basic.invalidPosition, "failed to open object file");
				возврат ложь;
			всё;

			нов (fingerprinter);
			Files.OpenWriter (writer, file, 0);
			если ExportModule (module(Sections.Module)) то
				writer.ПротолкниБуферВПоток;
				Files.Register (file);
				возврат истина;
			иначе
				возврат ложь
			всё
		кон Export;

		проц {перекрыта}DefineOptions* (options: Options.Options);
		нач
			options.Add(0X,"objectFileExtension",Options.String);
			options.Add(0X,"textualObjectFile",Options.Flag);
			options.Add(0X,"mergeSections",Options.Flag);
		кон DefineOptions;

		проц {перекрыта}GetOptions* (options: Options.Options);
		перем l2extension: Files.FileName;
		нач
			если options.GetString("objectFileExtension",l2extension) то
				SetExtension(l2extension);
			всё;
			binary := ~options.GetFlag("textualObjectFile");
			mergeSections := options.GetFlag("mergeSections");
		кон GetOptions;

		проц {перекрыта}DefaultSymbolFileFormat*(): Formats.SymbolFileFormat;
		нач возврат SymbolFileFormat.Get();
		кон DefaultSymbolFileFormat;

	кон ObjectFileFormat;

	проц GetFingerprint (section: Sections.Section; fingerprinter: Fingerprinter.Fingerprinter): ObjectFile.Fingerprint;
	перем fingerprint: SyntaxTree.КонтрольнаяСумма; fp: ObjectFile.Fingerprint; string: Basic.SectionName;
	нач
		если section.fingerprint # 0 то
			fp := section.fingerprint
		аесли (section.symbol = НУЛЬ) или (section.symbol.областьВидимостиГдеОбъявлено = НУЛЬ) то
			fp := 0;
			если (section(IntermediateCode.Section).resolved # НУЛЬ) то
				Basic.SegmentedNameToString(section.name, string);
				Fingerprinter.FPString(fp, string)
			всё
		аесли fingerprinter # НУЛЬ то
			fingerprint := fingerprinter.SymbolFP (section.symbol);
			fp := fingerprint.Мелкая;
		всё;
		возврат fp
	кон GetFingerprint;

	проц CheckAlias(sections: Sections.SectionList; перем identifier: ObjectFile.Identifier; перем offset: размерМЗ);
	перем section: Sections.Section; alias: BinaryCode.Section;
	нач
		offset := 0;
		если sections = НУЛЬ то возврат всё;
		section := sections.FindByName(identifier.name);
		если (section # НУЛЬ) то
			alias := section(IntermediateCode.Section).alias;
			если alias # НУЛЬ то
				offset := section(IntermediateCode.Section).aliasOffset;
				если TraceAliases то
					Basic.WriteSegmentedName(D.Log, identifier.name); D.String(" => ");
					Basic.WriteSegmentedName(D.Log, alias.os.identifier.name);
					D.Ln;
				всё;
				identifier := alias.os.identifier;
			всё;
		всё;
	кон CheckAlias;

	проц CopyFixups(sections: Sections.SectionList; from, to: BinaryCode.Section; offset: Unit);
	перем fixup: BinaryCode.Fixup; i, index, fixups: размерМЗ; fixupList: ObjectFile.Fixups;
		aliasSymbol: ObjectFile.Identifier; aliasOffset: размерМЗ;

		проц PatchFixup (l3fixup: BinaryCode.Fixup; fixupOffset, targetOffset: Unit);
		перем target, address: Unit; j: размерМЗ;

			проц PatchPattern (конст pattern: ObjectFile.FixupPattern);
			нач
				to.os.bits.SetBits (target * to.os.unit + pattern.offset, pattern.bits, address); address := арифмСдвиг (address, -pattern.bits);
			кон PatchPattern;

			проц CheckBits(value, l4offset: Unit);
			перем l5i: размерМЗ; nobits,remainder: ObjectFile.Bits; minval, maxval: Unit; name: ObjectFile.SectionName; number: массив 32 из симв8;
			нач
				nobits := 0;
				нцДля l5i := 0 до l3fixup.patterns-1 делай
					увел(nobits,l3fixup.pattern[l5i].bits);
				кц;

				remainder := арифмСдвиг(address,-nobits);

				если  (nobits <32) и ((remainder > 0) или (remainder < -1)) то
					если l3fixup.mode = ObjectFile.Relative то (* negative values allowed *)
						maxval := арифмСдвиг(1,nobits-1)-1; minval := -maxval-1
					иначе
						minval := 0; maxval := арифмСдвиг(1,nobits);
					всё;
					ObjectFile.SegmentedNameToString(to.os.identifier.name,name);
					Strings.Append(name,":");
					Strings.IntToStr(l4offset,number);
					Strings.Append(name,number);
					D.String(name); D.String("fixup out of range"); D.Ln;
					СТОП(100);
				всё;
			кон CheckBits;

		нач
			target := fixupOffset + l3fixup.offset  ;
			address := targetOffset + l3fixup.displacement;
			если l3fixup.mode = ObjectFile.Relative то
				умень(address,target)
			всё;
			address := арифмСдвиг (address, l3fixup.scale);
			CheckBits(address, l3fixup.offset);
			нцДля j := 0 до l3fixup.patterns-1 делай PatchPattern(l3fixup.pattern[j]) кц;
		кон PatchFixup;

	нач
		fixup := from.fixupList.firstFixup; i := 0; fixups := to.os.fixups; fixupList := to.os.fixup;
		нцПока fixup # НУЛЬ делай
			(*! fingerprint := GetFingerprint(fixup.symbol, fingerprinter);  *)
			aliasSymbol := fixup.symbol;
			CheckAlias(sections, aliasSymbol, aliasOffset);
			если PatchFixups и (aliasSymbol.name = to.os.identifier.name) и (fixup.mode = BinaryCode.Relative) то
				PatchFixup(fixup, offset, aliasOffset);
			иначе
				index := ObjectFile.AddFixup(fixups, fixupList, aliasSymbol.name, aliasSymbol.fingerprint, fixup.mode,fixup.scale, fixup.patterns, fixup.pattern);
				ObjectFile.AddPatch(fixupList[index].patches, fixupList[index].patch, fixup.displacement+aliasOffset,  fixup.offset+offset);
			всё;
			fixup := fixup.nextFixup; увел (i);
		кц;
		ObjectFile.SetFixups(to.os, fixups, fixupList);
		from.fixupList.InitFixupList; (* delete the source list *)
	кон CopyFixups;

	проц UpdateAliases (section: BinaryCode.Section;  fingerprinter: Fingerprinter.Fingerprinter);
	перем alias: BinaryCode.Alias; aliasList: ObjectFile.Aliases; i, aliases, index: размерМЗ;
	нач
		alias := section.aliasList.firstAlias; i := 0; aliases := 0; aliasList := НУЛЬ;
		нцПока alias # НУЛЬ делай
			(*! fingerprint := GetFingerprint(alias.symbol, fingerprinter);  *)
			index := ObjectFile.AddAlias(aliases, aliasList, alias.identifier.name, alias.identifier.fingerprint, alias.offset);
			alias := alias.nextAlias; увел (i);
		кц;
		ObjectFile.SetAliases(section.os, aliases, aliasList);
		section.aliasList.InitAliasList;
	кон UpdateAliases;

	проц Get*(): Formats.ObjectFileFormat;
	перем objectFileFormat: ObjectFileFormat;
	нач нов(objectFileFormat); возврат objectFileFormat
	кон Get;

	проц ReadHeader(reader: Потоки.Чтец; перем binary: булево; перем poolMap: ObjectFile.PoolMap; перем offers, requires: ObjectFile.NameList);
	перем ch: симв8; version: целМЗ; string: массив 32 из симв8;
	нач
		reader.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string);
		binary := string="FoxOFB";
		если ~binary то утв(string="FoxOFT") всё;
		reader.ПропустиБелоеПоле;
		reader.чСимв8(ch); утв(ch='v');
		reader.чЦел32(version,ложь);
		если version # ObjectFile.Version то ЛогЯдра.пСтроку8("warning: invalid object file encountered, recompile all sources"); ЛогЯдра.пВК_ПС всё;
		reader.чСимв8(ch); утв(ch='.');
		если ~binary то reader.ПропустиБелоеПоле
		иначе
			нов(poolMap,64);
			poolMap.Read(reader);
		всё;
		offers := НУЛЬ;
		requires := НУЛЬ;
		если ~binary то
			reader.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string); ObjectFile.ReadNameList(reader, offers, binary, poolMap);
			reader.ПропустиБелоеПоле;
			reader.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string); ObjectFile.ReadNameList(reader, requires, binary, poolMap);
			reader.ПропустиБелоеПоле;
		иначе
			ObjectFile.ReadNameList(reader, offers, binary, poolMap);
			ObjectFile.ReadNameList(reader, requires, binary, poolMap);
		всё
	кон ReadHeader;

	проц WriteHeader(writer: Потоки.Писарь; binary: булево; sections: Sections.SectionList; перем poolMap: ObjectFile.PoolMap; offers, requires: ObjectFile.NameList; fingerprinter:Fingerprinter.Fingerprinter);
	перем i: размерМЗ; section: Sections.Section;

		проц ProcessSection(l6section: IntermediateCode.Section);
		перем l7i: размерМЗ; fixup: BinaryCode.Fixup; alias: BinaryCode.Alias;
		нач
			если (l6section.resolved # НУЛЬ)  и (l6section.alias = НУЛЬ)  то
				poolMap.PutSegmentedName(l6section.resolved.os.identifier.name);
				(* for those sections that have been already resolved *)
				нцДля l7i := 0 до l6section.resolved.os.fixups-1 делай
					poolMap.PutSegmentedName(l6section.resolved.os.fixup[l7i].identifier.name);
				кц;
				нцДля l7i := 0 до l6section.resolved.os.aliases-1 делай
					poolMap.PutSegmentedName(l6section.resolved.os.alias[l7i].identifier.name);
				кц;
				fixup := l6section.resolved.fixupList.firstFixup; l7i := 0;
				нцПока fixup # НУЛЬ делай
					poolMap.PutSegmentedName(fixup.symbol.name);
					fixup := fixup.nextFixup;
				кц;
				alias:= l6section.resolved.aliasList.firstAlias; l7i := 0;
				нцПока alias # НУЛЬ делай
					poolMap.PutSegmentedName(alias.identifier.name);
					alias := alias.nextAlias;
				кц;
			всё;
		кон ProcessSection;

		проц NameList(конст names: ObjectFile.NameList);
		нач
			если names # НУЛЬ то
				нцДля i := 0 до длинаМассива(names)-1 делай
					poolMap.PutSegmentedName(names[i]);
				кц;
			всё;
		кон NameList;

	нач
		если binary то writer.пСтроку8("FoxOFB");
		иначе writer.пСтроку8("FoxOFT");
		всё;
		writer.пСимв8(' ');
		writer.пСимв8('v'); writer.пЦел64(ObjectFile.Version,0); writer.пСимв8(".");
		если ~binary то
				writer.пВК_ПС;
				writer.пСтроку8("offers "); ObjectFile.WriteNameList(writer, offers, binary, poolMap);
				writer.пСтроку8("requires "); ObjectFile.WriteNameList(writer, requires, binary, poolMap);
				writer.пВК_ПС;
		иначе
			нов(poolMap,512);
			poolMap.BeginWriting(writer);
			нцДля i := 0 до sections.Length()-1 делай
				section := sections.GetSection(i);
				ProcessSection(section(IntermediateCode.Section));
			кц;
			NameList(offers); NameList(requires);
			poolMap.EndWriting;
			ObjectFile.WriteNameList(writer, offers, binary, poolMap);
			ObjectFile.WriteNameList(writer, requires, binary, poolMap);
			(*
			FOR i := 0 TO fixups-1 DO
				D.String("fingerprint: "); Basic.WriteSegmentedName(D.Log, fixupList[i].identifier.name); D.Ln;
			END;
			*)
		всё;
	кон WriteHeader;

	проц GCD(a,b: Unit): Unit;
	перем h: Unit;
	нач
		нцПока b # 0 делай
			h := a остОтДеленияНа b;
			a := b;
			b := h;
		кц;
		возврат a
	кон GCD;

	проц SCM(a,b: Unit): Unit;
	нач
		возврат a*b DIV GCD(a,b)
	кон SCM;

	проц CommonAlignment(a,b: Unit): Unit;
	нач
		(*TRACE(a,b);*)
		если a = 0 то возврат b
		аесли b = 0 то возврат a
		иначе возврат SCM(a,b)
		всё;
	кон CommonAlignment;

	проц Show*(context: Commands.Context);
	перем
		fileName: Files.FileName; file: Files.File; reader: Files.Reader; writer: Потоки.Писарь;
		section: ObjectFile.Section; binary: булево; poolMap, poolMapDummy: ObjectFile.PoolMap;
		offers, requires: ObjectFile.NameList;
	нач
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fileName) то
			file := Files.Old(fileName);
			если file # НУЛЬ то
				нов(reader,file,0);
				writer := Basic.GetWriter(Basic.GetDebugWriter(fileName));
				ReadHeader(reader, binary, poolMap, offers, requires);
				WriteHeader(writer, ложь, НУЛЬ, poolMapDummy, offers, requires, НУЛЬ);
				нцПока reader.ПодглядиСимв8 () # 0X делай
					ObjectFile.ReadSection (reader, section,binary, poolMap);
					ObjectFile.WriteSection(writer, section, ложь, НУЛЬ); (* textual *)
					reader.ПропустиБелоеПоле;
				кц;
				writer.ПротолкниБуферВПоток;
			иначе
				context.error.пСтроку8("file not found "); context.error.пСтроку8(fileName); context.error.пВК_ПС
			всё;
		иначе
			context.error.пСтроку8("no file specificed"); context.error.пВК_ПС
		всё;
	кон Show;

	проц MakeLibrary*(context: Commands.Context);
	перем
		fileName: Files.FileName; file: Files.File; reader: Files.Reader; (*writer: Streams.Writer;*)
		binary: булево; poolMap, poolMapDummy: ObjectFile.PoolMap;
		bs: BinaryCode.Section;
		is: IntermediateCode.Section;
		sectionList: Sections.SectionList;
		section: ObjectFile.Section;
		i: размерМЗ;
		dest: Files.FileName;
		writer: Files.Writer;
		name: ObjectFile.SegmentedName;
		offers, requires: ObjectFile.NameList;
	нач
		нов(sectionList);
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(dest) то
			(*writer := Basic.GetWriter(Basic.GetDebugWriter(fileName));*)
			нцПока context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fileName) делай
				file := Files.Old(fileName);
				если file # НУЛЬ то
					нов(reader,file,0);
					ReadHeader(reader, binary, poolMap,offers, requires);
					нцПока reader.ПодглядиСимв8 () # 0X делай
						ObjectFile.InitSection(section);
						ObjectFile.ReadSection (reader, section, binary, poolMap);
						нов(bs, цел8(section.type) ,section.unit,name, ложь, ложь);
						bs.os := section;
						нов(is, цел8(bs.os.type),  bs.os.identifier.name,НУЛЬ, ложь);
						is.SetResolved(bs);
						sectionList.AddSection(is);
						reader.ПропустиБелоеПоле;
					кц;
				иначе
					context.error.пСтроку8("file not found "); context.error.пСтроку8(fileName); context.error.пВК_ПС;
					возврат;
				всё;
			кц;
			file := Files.New(dest);
			Files.OpenWriter(writer, file, 0);
			WriteHeader(writer, истина, sectionList, poolMapDummy, НУЛЬ, НУЛЬ, НУЛЬ);

			нцДля i := 0 до sectionList.Length()-1 делай
				is := sectionList.GetSection(i)(IntermediateCode.Section);
				ObjectFile.WriteSection(writer, is.resolved.os, истина, poolMapDummy); (* binary *)
			кц;
			writer.ПротолкниБуферВПоток;
			Files.Register(file);
			context.out.пСтроку8("Created library "); context.out.пСтроку8(dest); context.out.пВК_ПС;
		всё;
	кон MakeLibrary;

кон LisGenericObjectFile.
