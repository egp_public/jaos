модуль srvoxels;
использует srBase , srVoxel, srVoxel2, srVoxel3, srVoxel4,  srVoxel5, srLifeVox, srTexVox,
	Random,  Objects, srMath, MSpace:=srM2Space, sr3DTexture, srTree, srRotaVox;

тип Voxel=srBase.Voxel;
тип PT=srBase.PT;
тип COLOR=srBase.COLOR;

перем
	block*: srBase.Voxel;

тип SREAL=srBase.SREAL;

тип marshrunner=окласс(srBase.V);
перем
	i,j,k,ii,jj,kk,iii,jjj,kkk,t: цел16;
	cost: SREAL;
	p,q,qq: srBase.PT;
	corridor, pillar,sky,cage,building,FLOWER, CRIMSONFLOWER, DAISY, BLOOM: MSpace.cell;
	five: MSpace.cell;
	six,sixsix, sixsixsix: MSpace.cell;
	world, mesh: srVoxel2.Bloc10;
	b3, b33: srVoxel2.Bloc3;
	r,g,b: srBase.COLOR;
	red,blue,green: SREAL;
	EARTH, SKY: srVoxel.DiffuseVox;
	serp: srVoxel3.SlantBloc;
	rand: Random.Generator;
	EMPTY: srBase.Voxel;
	cell:MSpace.cell;
	X, Y: MSpace.cell;
	neomarsh:  MSpace.cell;
	b5, b55: srVoxel2.Bloc5;
	blok: массив 10 из srVoxel2.Bloc10;
	b10,tile1, tile2, tile3, tile4: srVoxel2.Bloc10;
	whitegreenmirror: srVoxel2.Bloc10;
	WHITE: srVoxel.DiffuseVox;
	BLUE, RED: srVoxel.DiffuseVox;
	PINK, GREEN: srVoxel.DiffuseVox;
	GREY, BROWN, PURPLE, YELLOW: srVoxel.DiffuseVox;
	POLKA1, POLKA2: srVoxel.PolkaVox;
	NIGHT, TGREEN, TYELLOW, TBROWN, TORANGE, E: srVoxel.RainbowVox;
	TBLACK: srVoxel.TransparaVox;
	RDRED, RDBLUE: srVoxel.TransparaVox;
	RDYELLOW: srVoxel.TransparaVox;
	LW, LG: srVoxel.TransparaVox;
	TRED, TPURPLE: srVoxel.TransparaVox;
	TBLUE, TWHITE: srVoxel.TransparaVox;
	SERP: массив 10 из srVoxel3.Serp;
	MSPHERE: srVoxel4.MirrorSphereInVox;
	SPHERE: массив 10 из srVoxel4.SphereInVox;
	ELLIPS: srVoxel4.Ellipsoid;
	HYPE:srVoxel4.Hyperboloid;
	sphere, sphere2: srVoxel. DiffuseSphVox;
	LIFE: srLifeVox.RDVox;
	MCUBE: srVoxel.DiffuseMVox;
	MPURPLE, MGREEN: srVoxel.ColoredMVox;
	HEX: srVoxel.HexaVox;
	HEX0: srVoxel.HexaVox;
	TOWER: srVoxel3.Tower;
	TEX, STORMFRONT, APO: srTexVox.TexVox;
	RPNF, TENTACLE, ATENEY, STARSTIKA, EUROPIA,CAPTION: srTexVox.TexVox;
	BITEX: srVoxel.BiVox;
	GONSWITCH, GOFFSWITCH: srVoxel.BiVox;
	GRID: srVoxel.GridVox;
	CHEX, SWEX: srVoxel5.SwastiVox;
	vege: srVoxel3.VegeBloc;
	SPH5: srVoxel2.Bloc5;
	SLANT: srVoxel3.SlantBloc;
	cosmos, megacosmos: srVoxel2.Bloc10;
	FIVE, FIVE1,FIVE2,FIVECOSM,AXES,AX2: MSpace.cell;
	SIX: MSpace.cell;
	RPNF5, PINE, TRIGROLLARD, DANGER: sr3DTexture.image3D2;
	tree: srTree.tree;
	azure,crimson,yellow: srBase.COLOR;cp: srBase.PT;
	avatar: массив 10 из srBase.Voxel;
	av: цел32;
	rota: srRotaVox.RVox;
	mrmkitty: массив 77 из srTexVox.TexVox;
	title: srBase.Name;
	frame: массив 8 из симв8;

проц& init*;
нач
	rand:=srBase.rand;
	нов(EMPTY);
(*	Strings.Append(title,"mrmkitty0000");
	FOR i:=0 TO 76 DO
		Strings.IntToStr(i,frame);
		Strings.Append(title,frame);
		Strings.Append(title,".jpg  ");
		NEW(mrmkitty[i],title);
	END; *)
	нов(MCUBE);
	нов(MGREEN);
	MGREEN.SetColor(0,1/2,0,1/2);
	нов(CHEX);
	CHEX.SetSwastiColor(0,0,0);
	CHEX.SetBackColor(1,1/3,1/3);
	нов(SWEX);
	SWEX.SetSwastiColor(0,0,1);
	SWEX.SetBackColor(1/3,1,1/3);
	нцДля i := 0 до 9 делай нов(blok[i]) кц;
	нов(b3);
	нов(b33);
	нов(b5);
	нов(b55);
	нов(b10); нов(tile1); нов(tile2); нов(tile3);
	нов(BLUE); нов(GREEN); нов(YELLOW); нов(BROWN); нов(RED); нов(PURPLE);
	BLUE.SetColor(0,0,1); 	GREEN.SetColor(0,5/10,0); 	YELLOW.SetColor(1,1,0);
	BROWN.SetColor(2/3,2/3,0); 	RED.SetColor(1,0,0); 	PURPLE.SetColor(1,0,1/3);
	нов(GREY);
	GREY.SetColor(5/6, 5/6, 5/6);
	нов(WHITE); WHITE.SetColor(0.9,0.9,0.9);
	нов(PINK); PINK.SetColor(1.1,1,1);
	нов(TWHITE); 	TWHITE.SetColor(5,5,5,0);
	нов(TBLUE); TBLUE.SetColor(0,0,3,1);
	нов(TORANGE); TORANGE.SetColor(5,5,2,1/2);
	нов(TGREEN); TGREEN.SetColor(0,3,0,5);
	нов(TRED); TRED.SetColor(3,0.1,0.1, 1/5);
	нов(TPURPLE); TPURPLE.SetColor(2,0,2,1);
	нов(TYELLOW); TYELLOW.SetColor(1,1,0,2);
	нов(TBROWN); TBROWN.SetColor(2/3,2/3,0,10);
	нов(TPURPLE); TPURPLE.SetColor(1,0,1,1/3);
	нов(TBLACK);
	TBLACK.SetColor(0,0,0,3);
	нов(NIGHT);
	NIGHT.SetColor(1/3,1/3,1/3,0);
	NIGHT.passable:=истина;
	нов(MCUBE);
	нов(HEX);
	HEX.setVox(EMPTY);
	HEX.passable:=истина;
	нов(HEX0);
	HEX0.setVox(BLUE);
	нов(TEX,"Sarah and Octopussmall.jpg");
	TEX.transparent := ложь;

	нов(STORMFRONT, "diversity.gif");
	нов(APO,"diversity.jpeg");
	нов(BITEX);
	BITEX.set(TEX,WHITE);
	нов(HYPE,HEX);
	HYPE.passable:=ложь;
	нов(RDRED); RDRED.SetColor(1,0,0,1);
	нов(RDBLUE); RDBLUE.SetColor(0,0,1,1);
	нов(RDYELLOW); RDYELLOW.SetColor(1/2,1/2,1/15,1);
	нов(LW); LW.SetColor(1,1,1,1/2);
	нов(LG); LG.SetColor(0,0,1,1/2);

	нов(SPHERE[0], WHITE);
	SPHERE[0].setSize(5/8);
	SPHERE[0].set2(RED);
	SPHERE[0].setCenter(0.5, 0.5, 0.5);

	нов(sphere);
	sphere.SetColor(1,1,0);
	нов(SPHERE[1], sphere);
	SPHERE[1].setSize(3);
	SPHERE[0].setCenter(-2.5, 0.5, 0.5);

	нов(sphere);
	sphere.SetColor(1,0,1);
	нов(SPHERE[2], sphere);
	SPHERE[2].setSize(1/4);

	нов(sphere);
	sphere.SetColor(1,0,1);
	нов(SPHERE[3], sphere);
	SPHERE[3].setSize(3/4);

	нов(sphere);
	sphere.SetColor(1,0,0);
	нов(SPHERE[4], GREEN);
	SPHERE[4].set2(sphere);
	SPHERE[4].setSize(3/4);

	нов(sphere);
	sphere.SetColor(0,1,1/4);
	нов(SPHERE[5], sphere);
	SPHERE[5].setSize(5/8);

	нов(MSPHERE);
	нов(SPHERE[6], RED);
	SPHERE[6].setSize(9/16);

	нов(sphere);
	sphere.SetColor(1,1/3,1);
	нов(SPHERE[7], sphere);
	SPHERE[7].setSize(1/2);

	нов(sphere);
	sphere.SetColor(1,1,0);
	нов(SPHERE[8], sphere);
	SPHERE[8].setSize(1/2);


	нов(SERP[0],  GREY, TWHITE);
	нов(SERP[1], SPHERE[5], EMPTY);

	нов(SERP[2],  RED, EMPTY);
	нов(SERP[3],  RED ,BLUE);
	нов(SERP[4],  HEX, НУЛЬ);

	нов(MPURPLE);
	MPURPLE.SetColor(0,0,1,1/2);
	нов(MGREEN);
	MGREEN.SetColor(1/2, 1, 1/2, 1/2);
	нов(MSPHERE);
	MSPHERE.passable := ложь;
	srBase.setCOLOR(r,1,0,0);
	srBase.setCOLOR(g,0,1,0);
	srBase.setCOLOR(b,0,0,1);
	нов(neomarsh);
	TORANGE.passable:=истина;
	TBLUE.passable:=истина;
	p.x := 1/2; p.y := 1/2; p.z := 1/2;
	нов(FLOWER);
	нов(CRIMSONFLOWER);
	нов(DAISY);
	нов(BLOOM);
	srBase.setCOLOR(azure,2/3,2/3,2/3);
	srBase.setCOLOR(crimson,1/3,1/3,1/3);
	srBase.setCOLOR(yellow,1,1,0);
	нцДля i:=0 до 13000 делай
		srBase.randnormPT(q);
		qq:=q;
		q.x :=1/2 + q.x/2;
		q.y := 1/2 + q.y/2;
		q.z := 1/2 + q.z/2;
		FLOWER.stroke(q,5,qq,azure,ложь);
		CRIMSONFLOWER.stroke(q,5,qq,crimson,ложь);
	кц;
	нцДля i:=0 до 1390 делай
		srBase.randnormPT(q);
		qq:=q;
		q.x :=1/2 + q.x/3;
		q.y := 1/2+ q.y/3;
		q.z := 1/2 + q.z/3;
		DAISY.stroke(q,5,qq,yellow,ложь);
	кц;
	нцДля i:=0 до 330 делай
		srBase.randnormPT(q);
		qq:=q;
		q.x :=rand.Uniform();
		q.y := rand.Uniform();
		q.z := rand.Uniform();
		если нечётноеЛи¿(i) то BLOOM.strokevoxel(q,5,FLOWER) иначе BLOOM.strokevoxel(q,5,CRIMSONFLOWER) всё
	кц;
	нов(LIFE);
	нов(five);
	нов(six);
	нов(sixsix);
	нов(sixsixsix);
	sixsixsix.SetColor(1/7,1/7,0,0);
	нов(world);
	world.SetColor(0,0,1/2,0);
	нов(serp,SPHERE[5],SPHERE[2]);
	нов(building);
	srBase.setPT(p,1/2,1/2,0);

	нов(AXES);
	нов(AX2);
	AXES.SetColor(1/4,0,1/4,1/4);
	AX2.SetColor(0,1/4,0,1/4);
	srBase.setPT(p,1/2,1/2,0);
	srBase.setPT(q,1/2,1/2,1);
	AXES.linevoxel(p,q,5,BLOOM);
	AX2.linevoxel(p,q,5,FLOWER);

	srBase.setPT(p,1/2,0,1/2);
	srBase.setPT(q,1/2,1,1/2);
	AXES.linevoxel(p,q,5,RED);
	AX2.linevoxel(p,q,5,CRIMSONFLOWER);

	srBase.setPT(p,0,1/2,1/2);
	srBase.setPT(q,1,1/2,1/2);
	AXES.linevoxel(p,q,5,PURPLE);
	AX2.linevoxel(p,q,5,BLOOM);

	AXES.passable:=истина;
	AX2.passable:=истина;

	нов(mesh);

	mesh.fill(BLOOM);
	six.fillchequer(mesh,DAISY);
	sixsix.fillchequer(EMPTY,six);
	sixsixsix.fillchequer(sixsix,EMPTY);
	five.passable:=истина;
	six.passable:=истина;	sixsix.passable:=истина;	sixsixsix.passable:=истина;

	b10.SetColor(0,0,1,0);
	b10.blox[3,3,8]:=MSPHERE;
	MSPHERE.passable:=истина;
	b10.blox[3,7,8]:=SERP[3];
	b10.SetColor(0,0,0,0);
(*	NEW(rota,mesh,0.01); *)
	cp.x:=0; cp.y:=0; cp.z:=0;
	нцДля i:=0 до 4 делай нцДля j:=0 до 4 делай нцДля k:=0 до 4 делай
		t:=0;
		если i=2 то увел(t) всё;
		если j=2 то увел(t) всё;
		если k=2 то увел(t) всё;
		если t<2 то b5.blox[i,j,k]:=WHITE иначе b5.blox[i,j,k]:=TPURPLE всё
	кц кц кц;
	нцДля i:=0 до 4 делай нцДля j:=0 до 4 делай нцДля k:=0 до 4 делай
		t:=0;
		если i=2 то увел(t) всё;
		если j=2 то увел(t) всё;
		если k=2 то увел(t) всё;
		если t<2 то b55.blox[i,j,k]:=b5 иначе b55.blox[i,j,k]:=NIGHT всё
	кц кц кц;
	нцДля i:=0 до 2 делай нцДля j:=0 до 2 делай нцДля k:=0 до 2 делай
		t:=0;
		если i=1 то увел(t) всё;
		если j=1 то увел(t) всё;
		если k=1 то увел(t) всё;
		если t<2 то b3.blox[i,j,k]:=b3 иначе b3.blox[i,j,k]:=НУЛЬ всё
	кц кц кц;
		нцДля i:=0 до 2 делай нцДля j:=0 до 2 делай нцДля k:=0 до 2 делай
		t:=0;
		если i=1 то увел(t) всё;
		если j=1 то увел(t) всё;
		если k=1 то увел(t) всё;
		если t<2 то b33.blox[i,j,k]:=b3 иначе b33.blox[i,j,k]:=НУЛЬ всё
	кц кц кц;
	WHITE.passable:=ложь;
	b3.passable:=истина;
	b33.passable:=истина;
	b5.passable:=истина;
	нов(cage);
	нов(SLANT,cage,НУЛЬ);
	нов(SERP[0], WHITE,AXES);
	SLANT.register;

	нцДля i:=1 до 7 делай нцДля j:=1 до 7 делай нцДля k:=1 до 7 делай
		если (i<2)или(i>5)или(j<2)или(j>5)или(k<2)или(k>5)то
			srBase.setPT(p,i/8,j/8,k/8);
		нов(b55);
		нов(corridor);
		corridor.SetColor(0,0,0,0);
		нов(WHITE);
		нов(RED);
		WHITE.SetColor(1-rand.Uniform()/10,1-rand.Uniform()/10,1- rand.Uniform()/10);
		RED.SetColor(1-rand.Uniform()/10,1-rand.Uniform()/10, 1-rand.Uniform()/10);
		нцДля ii:=0 до 4 делай нцДля jj:=0 до 4 делай нцДля kk:=0 до 4 делай
			t:=0;
			если ii=2 то увел(t) всё;
			если jj=2 то увел(t) всё;
			если kk=2 то увел(t) всё;
			если t<2 то
				нов(b5);
				нцДля iii:=0 до 4 делай нцДля jjj:=0 до 4 делай нцДля kkk:=0 до 4 делай
					t:=0;
					если iii=2 то увел(t) всё;
					если jjj=2 то увел(t) всё;
					если kkk=2 то увел(t) всё;
					если t=3 то  b5.blox[iii,jjj,kkk]:=AX2
					аесли нечётноеЛи¿(iii+jjj+kkk) то b5.blox[iii,jjj,kkk]:=WHITE иначе b5.blox[iii,jjj,kkk]:=RED
					всё;
				кц кц кц;
				b55.blox[ii,jj,kk]:=b5;
			иначе b55.blox[ii,jj,kk]:=AX2 всё
		кц кц кц;
		b10.blox[i,j,k]:=b55;
		cage.strokevoxel(p,3,b55);
		всё
	кц кц кц;


	srBase.setPT(p,1/2,1/2,1/2);
	cage.strokevoxel(p,3,SERP[0]);
	srBase.world:=cage;
кон init;

проц cylinder(target: MSpace.cell; start, end: PT;  radius: вещ32; t:цел16; color:COLOR);
перем
	i: цел16;
	sin,cos: вещ32;
	p,q,normal: PT;
нач
	нцДля i:= 0 до t делай
		normal.x:=srMath.sin(6.2832*i/t);
		normal.y:=srMath.cos(6.2832*i/t);
		normal.z:=0;
		sin:=radius*normal.x;
		cos:=radius*normal.y;
		p.x:= start.x+sin;
		p.y:= start.y+cos;
		p.z:=start.z;
		q.x:= end.x+sin;
		q.y:= end.y+cos;
		q.z:= end.z;
	(*	target.nline(p,q,8,normal,color,FALSE); *)
		target.linevoxel(p,q,11,WHITE);
	кц
кон cylinder;

проц cameratrail(v: Voxel; a,b: PT);
нач
	если v#НУЛЬ то v.linevoxel(a,b,12,DAISY) всё
кон cameratrail;

проц {перекрыта}tick*;
нач
(*	FOR i:=0 TO 10 DO
		srBase.randPT(p);
		cage.strokevoxel(p,4,FLOWER);
	END;
	FOR i:=0 TO 10 DO
		srBase.randPT(p);
		cage.strokevoxel(p,4,CRIMSONFLOWER);
	END;	*)
	SLANT.tick;
кон tick;

проц fill5cosm;
перем
	i, j: цел32;
	theta, phi: SREAL;
	a: srBase.PT;
	sky:srBase.COLOR;
нач
	нов(FIVECOSM);
	sky.red := 1/3;
	sky.green := 1/3;
	sky.blue := 1;
(*	FIVECOSM.SetColor(1,1,0,0); *)
	нцДля i := 0 до 100 делай
		theta:=i*6.2832/100;
		нцДля j := 0 до 100 делай
			phi:=j*3.14159/100;
			a.x:=1/2 +srMath.cos(theta)*srMath.sin(phi)/12;
			a.y:=1/2 +srMath.sin(theta)*srMath.sin(phi)/12;
			a.z:= 1/2+srMath.cos(phi)/12;
			FIVECOSM.strokevoxel(a, 3, megacosmos);
		кц
	кц;
кон fill5cosm;

проц fillcell;
перем
	i, j: цел32;
	theta, phi: SREAL;
	a,normal: srBase.PT;
	reddishgray:srBase.COLOR;
нач
	нов(cell);
	reddishgray.red:=1;
	reddishgray.green:=1;
	reddishgray.blue:=1;
	нцДля i := 0 до 100 делай
		theta:=i*6.2832/100;
		нцДля j := 0 до 100 делай
			phi:=j*6.2832/100;
			a.x:=1/2 +srMath.cos(theta)*srMath.sin(phi)/9;
			a.y:=1/2 +srMath.sin(theta)*srMath.sin(phi)/9;
			a.z:=1/2 + srMath.cos(phi)/9;
			normal:=srMath.norm(theta,phi);
			cell.stroke(a, 3,normal,reddishgray,ложь);

		кц
	кц;
(*	srBase.setPT(a,1/2,1/2,1/2);
	cell.strokevoxel(a, 2,RED);
	cell.SetColor(0,0,0,1/4);
	cell.passable := TRUE; *)
кон fillcell;
проц fillX;
перем
	i, j,k: цел32;
	a,b: SREAL;
	v: массив 8 из srBase.PT;
нач
	нов(X); нов(Y);
	X.passable:=истина;
	Y.passable:=истина;
	a:=0.1; b:=0.9;
	srBase.setPT(v[0],a,a,a);
	srBase.setPT(v[1],a,a,b);
	srBase.setPT(v[2],a,b,a);
	srBase.setPT(v[3],a,b,b);
	srBase.setPT(v[4],b,a,a);
	srBase.setPT(v[5],b,a,b);
	srBase.setPT(v[6],b,b,a);
	srBase.setPT(v[7],b,b,b);
	srBase.setPT(p,1/2,1/2,1/2);
	нцДля i := 0 до 7 делай
		нцДля j := 0 до 7 делай
			X.linevoxel(v[i],v[j],2,cell);
		кц
	кц;
	нцДля i := 0 до 15 делай
		srBase.randPT(p);
		X.strokevoxel(p,1,TWHITE)
	кц;
	нцДля i :=0 до 8 делай
		нцДля j :=0 до 8 делай
			нцДля k :=0 до 8 делай
				srBase.setPT(p,i/25,j/25,k/25);
		 		Y.strokevoxel(p, 2, SWEX);
		 	кц
		кц
	кц;
кон fillX;


нач{активное, приоритет(Objects.Low)}
	нцДо
		tick;
	кцПри  ~srBase.worldalive;
кон marshrunner;

перем
	MARSH: marshrunner;

проц cameratrail*(v: Voxel; a,b: PT);
нач
	MARSH.cameratrail(v,a,b);
кон cameratrail;

проц trailswitch*; (*remove*)
кон trailswitch;

нач
	нов(MARSH);
кон srvoxels.



