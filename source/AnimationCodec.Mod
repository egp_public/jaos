модуль AnimationCodec; (** AUTHOR "staubesv"; PURPOSE "Codec for proprietary animation format"; *)

(*

	STATUS: APLHA, don't rely on (undocumented) animation description format

*)

использует
	Потоки, ЛогЯдра, Strings, Files, Codecs, XML, XMLScanner, XMLParser, WMGraphics;

конст
	Version = "ANI09a";

	HeaderMissing* = 20001;
	HeaderError* = 20002;
	WrongVersion* = 20003;
	FormatError* = 20004;
	ImageNotFound* = 20010;

	XmlHeader = "Header";
	XmlVersion = "version";
	XmlWidth = "width";
	XmlHeight = "height";
	XmlBackgroundColor = "bgcolor";
	XmlFrames = "Frames";
	XmlFrame = "Frame";
	XmlImageName = "image";
	XmlLeft = "x";
	XmlTop = "y";
	XmlDelayTime = "time";
	XmlDisposeMode = "mode";
	XmlFrom = "from";
	XmlTo = "to";

	Debug = истина;

тип

	Settings = запись
		x, y : цел32;
		time, mode : цел32;
	кон;

тип

	Decoder* = окласс(Codecs.AnimationDecoder)
	перем
		animation : XML.Element;
		width, height, bgcolor : цел32;
		default : Settings;
		error : булево;

		проц &Init*;
		нач
			animation := НУЛЬ;
			width := 0; height := 0; bgcolor := 0;
			RestoreDefaultSettings;
			error := ложь;
		кон Init;

		проц RestoreDefaultSettings;
		нач
			default.x := 0; default.y := 0; default.time := 20; default.mode := Codecs.Unspecified;
		кон RestoreDefaultSettings;

		проц ReportError(pos, line, col: Потоки.ТипМестоВПотоке; конст msg: массив из симв8);
		нач
			error := истина;
		кон ReportError;

		(* open the decoder on an InputStream *)
		проц {перекрыта}Open*(in : Потоки.Чтец; перем res : целМЗ);
		перем
			scanner : XMLScanner.Scanner; parser : XMLParser.Parser; document : XML.Document;
			content : XML.Content;

			проц CheckHeader(header : XML.Element; перем res : целМЗ);
			перем name, version : Strings.String; ignore : булево;
			нач
				утв(header # НУЛЬ);
				name := header.GetName();
				если (name # НУЛЬ) и (name^ = XmlHeader) то
					version := header.GetAttributeValue(XmlVersion);
					если (version # НУЛЬ) то
						если (version^ = Version) то
							если GetInteger(header, XmlWidth, width) и GetInteger(header, XmlHeight, height) и (width > 0) и (height > 0) то
								ignore := GetInteger(header, XmlBackgroundColor, bgcolor);
								res := Codecs.ResOk;
							иначе
								res := HeaderError;
							всё;
						иначе
							res := WrongVersion;
						всё;
					иначе
						res := HeaderError;
					всё;
				иначе
					res := HeaderMissing;
				всё;
			кон CheckHeader;

		нач
			утв(in # НУЛЬ);
			нов(scanner, in);
			нов(parser, scanner);
			parser.reportError := ReportError;
			document := parser.Parse();
			если ~error и (document # НУЛЬ) то
				animation := document.GetRoot();
				если (animation # НУЛЬ) то
					content := animation.GetFirst();
					если (content # НУЛЬ) и (content суть XML.Element) то
						CheckHeader(content(XML.Element), res);
						если (res # Codecs.ResOk) то animation := НУЛЬ; всё;
					иначе
						res := Codecs.ResFailed;
					всё;
				иначе
					res := Codecs.ResFailed;
				всё;
			иначе
				animation := НУЛЬ;
				res := Codecs.ResFailed;
			всё;
			если Debug и (res # Codecs.ResOk) то
				ЛогЯдра.пСтроку8("AnimationCodec: Could not open animation, res = "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
			всё;
		кон Open;

		проц ProcessFrame(frame : XML.Element; перем desc : Codecs.ImageDescriptor; перем res : целМЗ);
		перем string, imageName : Strings.String; left, top, from, to, current : цел32; minDigits : размерМЗ; filename : Files.FileName; last, d : Codecs.ImageDescriptor;
		нач
			нов(desc);
			если ~GetInteger(frame, XmlLeft, left) то desc.left := default.x; иначе desc.left := left всё;
			если ~GetInteger(frame, XmlTop, top) то desc.top := default.y; иначе desc.top := top всё;
			если ~GetInteger(frame, XmlDelayTime, desc.delayTime) то desc.delayTime := default.time; всё;
			если ~GetInteger(frame, XmlDisposeMode, desc.disposeMode) то desc.disposeMode := default.mode; всё;
			если ~GetInteger(frame, XmlFrom, from) то from := 0 всё;
			если ~GetInteger(frame, XmlTo, to) то to := 0; всё;
			imageName := frame.GetAttributeValue(XmlImageName);
			если (imageName # НУЛЬ) то
				если (from = 0) и (to = 0) то
					desc.image := WMGraphics.LoadImage(imageName^, истина);
					если (desc.image # НУЛЬ) то
						res := Codecs.ResOk;
					иначе
						res := ImageNotFound;
					всё;
				иначе
					string := frame.GetAttributeValue(XmlFrom);
					если (string # НУЛЬ) то
						Strings.TrimWS(string^);
						minDigits := Strings.Length(string^);
					иначе
						minDigits := 0;
					всё;

					last := НУЛЬ; d := desc;
					res := Codecs.ResOk;
					current := from;
					нцДо
						GenerateFilename(imageName^, filename, current, minDigits);
						d.image := WMGraphics.LoadImage(filename, истина);
						если (d.image = НУЛЬ) то
							res := ImageNotFound;
						всё;
						если (last = НУЛЬ) то
							last := d;
						иначе
							d.previous := last;
							last.next := d;
							last := d;
						всё;
						увел(current);
						если (current <= to) то
							нов(d);
							d.left := last.left; d.top := last.top; d.delayTime := last.delayTime; d.disposeMode := last.disposeMode;
						всё;
					кцПри (res # Codecs.ResOk) или (current > to);
				всё;
			иначе
				res := FormatError;
			всё;
		кон ProcessFrame;

		проц ProcessFrames(frames : XML.Element; перем sequence : Codecs.ImageSequence; перем res : целМЗ);
		перем frame : XML.Content; name : Strings.String; desc, last : Codecs.ImageDescriptor; value : цел32;
		нач
			утв(frames # НУЛЬ);
			last := sequence.images;
			если (last # НУЛЬ) то нцПока (last.next # НУЛЬ) делай last := last.next; кц; всё;
			RestoreDefaultSettings;
			если GetInteger(frames, XmlLeft, value) то default.x := value; всё;
			если GetInteger(frames, XmlTop, value) то default.y := value; всё;
			если GetInteger(frames, XmlDelayTime, value) то default.time := value; всё;
			если GetInteger(frames, XmlDisposeMode,value) то default.mode := value; всё;
			res := Codecs.ResOk;
			frame := frames.GetFirst();
			нцПока (res = Codecs.ResOk) и (frame # НУЛЬ) делай
				если (frame суть XML.Element) то
					name := frame(XML.Element).GetName();
					если (name # НУЛЬ) и (name^ = XmlFrame) то
						ProcessFrame(frame(XML.Element), desc, res);
						если (res = Codecs.ResOk) то
							если (last = НУЛЬ) то
								утв(sequence.images = НУЛЬ);
								sequence.images := desc;
							иначе
								desc.previous := last;
								last.next := desc;
							всё;
							last := desc;
						иначе
							sequence.images := НУЛЬ;
						всё;
					всё;
				всё;
				frame := frames.GetNext(frame);
			кц;
		кон ProcessFrames;

		проц {перекрыта}GetImageSequence*(перем sequence : Codecs.ImageSequence; перем res : целМЗ);
		перем content : XML.Content; frames : XML.Element; string : Strings.String;
		нач
			если (animation = НУЛЬ) то res := Codecs.ResFailed; возврат; всё;
			sequence.width := width; sequence.height := height; sequence.bgColor := bgcolor; sequence.images := НУЛЬ;
			res := Codecs.ResOk;
			content := animation.GetFirst();
			нцПока (res = Codecs.ResOk) и (content # НУЛЬ) делай
				если (content суть XML.Element) то
					frames := content(XML.Element);
					string := frames.GetName();
					если (string # НУЛЬ) и (string^ = XmlFrames) то
						ProcessFrames(frames, sequence, res);
						если (res # Codecs.ResOk) то
							sequence.images := НУЛЬ;
						всё;
					всё;
				всё;
				content := animation.GetNext(content);
			кц;
			если Debug и (res # Codecs.ResOk) то
				ЛогЯдра.пСтроку8("AnimationCodec: Could not decode image sequence, res = "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
			всё;
		кон GetImageSequence;

	кон Decoder;

проц GenerateFilename(конст base : массив из симв8; перем filename : массив из симв8; suffix, minDigits : размерМЗ);
перем name, extension : Files.FileName; temp, digits : размерМЗ; nbr : массив 32 из симв8;
нач
	Files.SplitExtension(base, name, extension);
	копируйСтрокуДо0(name, filename);
	digits := 0; temp := suffix;
	нцДо
		увел(digits); temp := temp DIV 10;
	кцПри temp = 0;
	нцПока (digits < minDigits) делай Strings.Append(filename, "0"); увел(digits); кц;
	Strings.IntToStr(suffix, nbr);
	Strings.Append(filename, nbr);
	Strings.Append(filename, ".");
	Strings.Append(filename, extension);
	ЛогЯдра.пСтроку8(filename); ЛогЯдра.пВК_ПС;
кон GenerateFilename;

проц GetInteger(element : XML.Element; конст attributeName : массив из симв8; перем value : цел32) : булево;
перем valueStr : Strings.String;
нач
	утв(element # НУЛЬ);
	value := 0;
	valueStr := element.GetAttributeValue(attributeName);
	если (valueStr # НУЛЬ) то
		Strings.TrimWS(valueStr^);
		Strings.StrToInt(valueStr^, value);
		возврат истина;
	иначе
		возврат ложь;
	всё;
кон GetInteger;

проц GenDecoder*() : Codecs.AnimationDecoder;
перем d : Decoder;
нач
	нов(d); возврат d;
кон GenDecoder;

кон AnimationCodec.
