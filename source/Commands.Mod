(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Commands; (** AUTHOR "pjm"; PURPOSE "Commands and parameters"; *)

использует Objects, Modules, Потоки, ЛогЯдра, Трассировка, ЭВМ, UCS32;

конст

	(** Activate flags. *)
	Wait* = 0;	(** Wait until the activated command returns. *)
	InheritContext*= 1; (** Inherit context (as far as not overwritten) of the caller *)
	Silent*= 2;

	Ok* = 0;
	CommandNotFound* = 3901;
	CommandError* = 3902;
	CommandParseError* = 3903;
	CommandTrapped* = 3904;

	(* Separates module name from procedure name *)
	Delimiter* = ".";

	(* Runner states *)
	Started = 0; Loaded = 1; Finished = 2;

тип
	ModulesNameJQ* = массив Modules.ширинаИмениВключаяЛексему0 из UCS32.CharJQ;

	Context* = окласс
	перем
		in-, arg- : Потоки.Чтец;
		out-, error- : Потоки.Писарь;
		caller-: окласс;
		result*: целМЗ;

		проц &Init*(in, arg : Потоки.Чтец; out, error : Потоки.Писарь; caller: окласс);
		нач
			если (in = НУЛЬ) то in := GetEmptyReader(); всё;
			если (arg = НУЛЬ) то arg := GetEmptyReader()всё;
			если (out = НУЛЬ) то нов(out, ЛогЯдра.ЗапишиВПоток, 128); всё;
			если (error = НУЛЬ) то нов(error, ЛогЯдра.ЗапишиВПоток, 128); всё;
			сам.in := in; сам.arg := arg; сам.out := out; сам.error := error; сам.caller := caller; сам.result := Ok;
			утв((in # НУЛЬ) и (arg # НУЛЬ) и (out # НУЛЬ) и (error # НУЛЬ));
		кон Init;
		
		(* нужно для случая, когда поток хотим заменить на обёртку. Но лучше
		   именно и использовать обёртку *)
		проц ПодмениПотокЧтения*(новЧтец : Потоки.Чтец);
		нач
			arg := новЧтец кон ПодмениПотокЧтения;

	кон Context;

	(* Procedure types that can be called be runner thread *)
	CommandProc = проц;
	CommandContextProc = проц(context : Context);

тип

	Runner = окласс(Modules.ЛХА)
	перем
		moduleName, commandName : Modules.Name;
		context : Context;

		proc : CommandProc;
		commandProc : CommandContextProc;

		msg : массив 128 из симв8; res : целМЗ;

		module : Modules.Module;
		state : целМЗ;
		exception : булево;

		проц &Init*(конст moduleName, commandName : Modules.Name; context : Context);
		перем origin: Потоки.ТипМестоВПотоке; char: симв8;
		нач
			сам.moduleName := moduleName; сам.commandName := commandName;

			если (context = НУЛЬ) то нов(context, НУЛЬ, НУЛЬ, НУЛЬ, НУЛЬ, НУЛЬ); всё;
			если trace то
				Трассировка.пСтроку8("Commands.Activate ");
				Трассировка.пСтроку8(moduleName); Трассировка.пСтроку8(Delimiter); Трассировка.пСтроку8(commandName);
				если context.arg.можноЛиПерейтиКМестуВПотоке¿ () то
					origin := context.arg.МестоВПотоке ();
					нц context.arg.чСимв8 (char); если context.arg.кодВозвратаПоследнейОперации # Потоки.Успех то прервиЦикл всё; Трассировка.пСимв8 (char); кц;
					context.arg.ПерейдиКМестуВПотоке (origin);
				всё;
				Трассировка.пСимв8 ("~"); Трассировка.пВК_ПС;
			всё;
			сам.context := context;
			res := CommandError; копируйСтрокуДо0("Error starting command", msg);
			exception := ложь;
			state := Started;
		кон Init;

		проц Join(this : целМЗ; перем res : целМЗ; перем msg : массив из симв8);
		нач {единолично}
			дождись(state >= this);
			res := сам.res; копируйСтрокуДо0(сам.msg, msg);
		кон Join;

	нач {активное, SAFE}
		Objects.SetContext(context);
		если ~exception то
			exception := истина; (* catch exceptions from now on *)
			module := Modules.ThisModule(moduleName, res, msg);
			если (res = Ok) то
				если commandName # "" то
					дайПроцПоИмени(moduleName, commandName, proc);
					если (proc = НУЛЬ) то
						дайПроцПоИмени(moduleName, commandName, commandProc);
					всё;
					если (proc = НУЛЬ) и (commandProc = НУЛЬ) то
						res := CommandNotFound;
						msg := "Command ";
						Modules.Append(moduleName, msg); Modules.Append(Delimiter, msg); Modules.Append(commandName, msg);
						Modules.Append(" not found", msg);
					всё;
				всё;
			всё;
			нач {единолично} state := Loaded; кон;
			если (res = Ok) то
				утв((proc # НУЛЬ) или (commandProc # НУЛЬ) или (commandName = ""));
				если (proc # НУЛЬ) то
					proc();
					context.out.ПротолкниБуферВПоток; context.error.ПротолкниБуферВПоток;
				аесли (commandProc # НУЛЬ) то
					утв(context # НУЛЬ);
					commandProc(context);
					context.out.ПротолкниБуферВПоток; context.error.ПротолкниБуферВПоток;
					res := context.result;
					если res # Ok то msg := "Command not successful"; всё;
				всё;
			всё;
		иначе
			res := CommandTrapped; копируйСтрокуДо0("Exception during command execution", msg);
		всё;
		нач {единолично} state := Finished; кон;
	кон Runner;

перем
	emptyString : массив 1 из симв8;
	silentWriter: Потоки.Писарь;
	trace: булево;
	defaultContext: Context; (* Fallback. Note that this context would be shared by different users -- may be used for tracing though *)

(* Create a ready on a empty string *)

проц GetEmptyReader() : Потоки.Чтец;
перем reader : Потоки.ЧтецИзСтроки;
нач
	нов(reader, 1); reader.ПримиСрезСтроки8ВСтрокуДляЧтения(emptyString, 0, 1);
	возврат reader;
кон GetEmptyReader;

проц SendNothing(конст buf: массив из симв8; ofs, len: размерМЗ; propagate: булево; перем res: целМЗ);
кон SendNothing;

(* То же, что Split, но для UCS32 *)
проц SplitJQ*(конст команднаяСтрока : UCS32.StringJQ; перем имяМодуля, имяПроц : ModulesNameJQ; перем рез : целМЗ; перем сообщение : UCS32.StringJQ);
перем i, j, maxlen, cmdlen : размерМЗ;
нач
	рез := CommandParseError;
	имяМодуля := Лит32(""); 
	имяПроц := Лит32(""); 
	сообщение := Лит32("");
	maxlen := длинаМассива(имяМодуля); cmdlen := длинаМассива(команднаяСтрока);
	i := 0; 
	нцПока (i < cmdlen) и (i < maxlen-1) и
			(команднаяСтрока[i] # UCS32.CHARvCharJQ(Delimiter)) и
			(команднаяСтрока[i] # UCS32.CHARvCharJQ(0X)) делай 
		имяМодуля[i] := команднаяСтрока[i]; увел(i); кц;
	если (i >= maxlen)  то
		сообщение := Лит32("Слишком длинное имя модуля");
	аесли (i >= cmdlen) то
		сообщение := Лит32("Строка команды не завершается литерой 0X");
	аесли (команднаяСтрока[i] # UCS32.CHARvCharJQ(Delimiter)) то
		сообщение := Лит32('Ожидалось ИмяМодуля "." [ИмяПроцедуры]');
	иначе
		(* We allow команднаяСтрока[i] = 0X. That means the module will be loaded but not command procedure will be started *)
		имяМодуля[i] := UCS32.CHARvCharJQ(0X);
		увел(i); (* Skip Delimiter *)
		j := 0;
		нцПока (i < cmdlen) и (j < maxlen-1) и
			 (команднаяСтрока[i] # симв32ИзКода(0)) делай 
			имяПроц[j] := команднаяСтрока[i]; увел(j); увел(i); кц;
		если (i >= cmdlen) то
			сообщение := Лит32("Строка команды не завершается литерой 0X");
		аесли (j >= maxlen-1) то
			сообщение := Лит32("Слишком длинная строка команды");
		иначе
			имяПроц[j] := UCS32.CHARvCharJQ(0X);
			рез := Ok; сообщение := Лит32("");
		всё;
	всё;
кон SplitJQ;


(** Splits a command string of the form moduleName.commandProcName into its components. Can be used to check whether a
	command string is syntactically correct, i.e. is of the form 'ModuleName "." [ProcedureName]' *)

проц Split*(конст cmdstr : массив из симв8; перем moduleName, procedureName : Modules.Name; перем res : целМЗ; перем msg : массив из симв8);
перем i, j, maxlen, cmdlen : размерМЗ;
нач
	res := CommandParseError;
	moduleName := ""; procedureName := ""; msg := "";
	maxlen := длинаМассива(moduleName); cmdlen := длинаМассива(cmdstr);
	i := 0; нцПока (i < cmdlen) и (i < maxlen-1) и (cmdstr[i] # Delimiter) и (cmdstr[i] # 0X) делай moduleName[i] := cmdstr[i]; увел(i); кц;
	если (i >= maxlen)  то
		копируйСтрокуДо0("Слишком длинное имя модуля", msg);
	аесли (i >= cmdlen) то
		копируйСтрокуДо0("Строка команды не завершается литерой 0X", msg);
	аесли (cmdstr[i] # Delimiter) то
		копируйСтрокуДо0('Ожидалось ИмяМодуля "." [ИмяПроцедуры]', msg);
	иначе
		(* We allow cmdstr[i] = 0X. That means the module will be loaded but not command procedure will be started *)
		moduleName[i] := 0X;
		увел(i); (* Skip Delimiter *)
		j := 0;
		нцПока (i < cmdlen) и (j < maxlen-1) и (cmdstr[i] # 0X) делай procedureName[j] := cmdstr[i]; увел(j); увел(i); кц;
		если (i >= cmdlen) то
			копируйСтрокуДо0("Строка команды не завершается литерой 0X", msg);
		аесли (j >= maxlen-1) то
			копируйСтрокуДо0("Слишком длинная строка команды", msg);
		иначе
			procedureName[j] := 0X;
			res := Ok; копируйСтрокуДо0("", msg);
		всё;
	всё;
кон Split;



(**	Can be called by a command to retrieve the context associated with its active object. *)

проц GetContext*() : Context;
перем context: динамическиТипизированныйУкль;
нач
	context := Objects.CurrentContext();
	если (context # НУЛЬ) и (context суть Context) то возврат context(Context)
	иначе возврат defaultContext
	всё;
кон GetContext;

(**	Activate a command in its own active object.
	Returns res = Ok if successful, otherwise msg contains error message.
	The command can call GetConext() to get its context, which is also passed directly. 
	См. также ActivateJQ
	*)

проц Activate*(конст cmd : массив из симв8; context : Context; flags : мнвоНаБитахМЗ; перем res : целМЗ; перем msg : массив из симв8);
перем moduleName, commandName : Modules.Name; run : Runner;
нач

	Split(cmd, moduleName, commandName, res, msg);
	если (res = Ok) то
		нов(run, moduleName, commandName, context);
		run.Join(Loaded, res, msg); (* Avoid race condition described in Modules.Mod *)
		если (res = Ok) и (Wait в flags) то run.Join(Finished, res, msg); всё
	всё;
кон Activate;


(* ActivateJQ - это аналог Activate для StringJQ *)
проц ActivateJQ*(конст cmd : UCS32.StringJQ; context : Context; flags : мнвоНаБитахМЗ; перем res : целМЗ; перем msg : UCS32.StringJQ);
перем cmdA, msgA : укль на массив из симв8;
нач
	cmdA := UCS32.JQvNovU8(cmd);
	нов(msgA, длинаМассива(msg)*UCS32.JQLenToU8Len);
	Activate(cmdA^, context, flags, res, msgA^);
	UCS32.COPYU8vJQ(msgA^, msg);	
кон ActivateJQ;


(** Activate a string of commands, including their parameters.
	The string is parsed from left to right and Activate is called for every command.
	Parsing stops at the end of the string, or when Activate returns an error.
	The flags are applied to every command, i.e., for sequential execution,
	use the Wait flag (the caller waits until all commands return).
	Syntax:
		cmds = [mode " " ] cmd {";" cmd} .
		mode = "PAR" | "SEQ" .
		cmd = mod ["." proc] [" " params] .
		params = {<any character except ";">} .
		
		
	См. также CallJQ	
	
	2021-07-18 - я не совсем понимаю, зачем тут использован разделитель „;“, а не „~“. 
	Это выносит мне мозг. Хорошо хотя бы то, что внутри строкового аргумента ; не ломает ничего. 
	Хотя в конце строкового аргумента - уже ломает.
*)

проц Call*(cmds : массив из симв8; flags : мнвоНаБитахМЗ; перем res : целМЗ; перем msg : массив из симв8);
перем  outer, context : Context; arg : Потоки.ЧтецИзСтроки; i, j, k : размерМЗ; mode : массив 5 из симв8;
par : укль на массив из симв8;
нач
	если trace то Трассировка.пСтроку8("Commands.Call "); Трассировка.пСтроку8(cmds); Трассировка.пСтроку8("~"); Трассировка.пВК_ПС всё;
	если cmds = "" то
		ЛогЯдра.пСтроку8("Commands.Call: пустая команда");
		ЛогЯдра.пВК_ПС;
		res := -1; 
		возврат всё;
	нов(par,длинаМассива(cmds));
	i := 0; нцПока (i # 4) и (i # длинаМассива(cmds)) делай mode[i] := cmds[i]; увел(i); кц;
	mode[i] := 0X;	(* copy at most first 4 characters *)
	если mode = "PAR " то исключиИзМнваНаБитах(flags, Wait);
	аесли mode = "SEQ " то включиВоМнвоНаБитах(flags, Wait);
	иначе i := 0;	(* reset to start *)
	всё;
	нц
		k := 0;
		нцПока (cmds[i] # " ") и (cmds[i] # 09X) и (cmds[i] # 0DX) и (cmds[i] # 0AX) и (cmds[i] # 0X) и (cmds[i] # ";") делай cmds[k] := cmds[i]; увел(k); увел(i); кц;
		если k = 0 то прервиЦикл; всё;	(* end of string *)
		j := 0;
		если (cmds[i] # ";") и (cmds[i] # 0X) то (* parameters *)
			увел(i); нцПока (cmds[i] # 0X) и (cmds[i] # ";") делай par[j] := cmds[i]; увел(i); увел(j); кц;
		всё;
		если cmds[i] = ";" то увел(i); всё;
		par[j] := 0X; cmds[k] := 0X;
		нов(arg, j+1); arg.ПримиСрезСтроки8ВСтрокуДляЧтения(par^, 0, j+1);
		если InheritContext в flags то
			outer := GetContext();
			нов(context, outer.in, arg, outer.out, outer.error, outer.caller);
		аесли Silent в flags то
			нов(context,НУЛЬ, arg, silentWriter, silentWriter, НУЛЬ);
		иначе
			нов(context, НУЛЬ, arg, НУЛЬ, НУЛЬ, НУЛЬ)
		всё;
		Activate(cmds, context, flags, res, msg);
		если (res # Ok) то прервиЦикл; всё;
	кц;
кон Call;

(* CallJQ - это аналог Call для StringJQ *)
проц CallJQ*(cmds : UCS32.StringJQ; flags : мнвоНаБитахМЗ ; перем res : целМЗ; перем msg : UCS32.StringJQ);
перем cmdsA, msgA : укль на массив из симв8;
нач
	cmdsA := UCS32.JQvNovU8(cmds);
	нов(msgA, длинаМассива(msg)*UCS32.JQLenToU8Len);
	Call(cmdsA^, flags, res, msgA^);
	UCS32.COPYU8vJQ(msgA^, msg);	
кон CallJQ;

проц ДайЛХА*():Modules.ЛХА;
перем ао: динамическиТипизированныйУкль;
нач
	ао := Objects.ActiveObject();
	если ао суть Modules.ЛХА то 
		возврат ао(Modules.ЛХА)
	иначе
		возврат НУЛЬ всё кон ДайЛХА;
	

проц Init;
перем s: массив 4 из симв8;
нач
	emptyString[0] := 0X;
	ЭВМ.ДайЗначениеКлючаКонфигурацииЯОС("TraceCommands", s);
	trace := (s[0] = "1");
	нов(silentWriter, SendNothing, 128);
	нов(defaultContext,НУЛЬ,НУЛЬ,НУЛЬ,НУЛЬ,НУЛЬ);
кон Init;


нач
	Init;

кон Commands.
