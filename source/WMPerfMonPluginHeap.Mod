модуль WMPerfMonPluginHeap; (** AUTHOR "staubesv"; PURPOSE "Performance Monitor plugin for Heaps statistics"; *)

использует
	НИЗКОУР, ЭВМ, Heaps, Modules, WMGraphics, WMPerfMonPlugins;

конст
	ModuleName = "WMPerfMonPluginHeap";

	BlockSize = 8*размер16_от(адресВПамяти); (* {BlockSize = Heaps.BlockSize} *)

тип
	SizeArray = массив 27 из цел32;

	BlocksHelper = окласс(WMPerfMonPlugins.Helper)
	перем
		blocks, recblks, sysblks, arrblks, protrecblks, freeblks, unknowns : цел32;
		blockssize, recblksize, sysblksize, arrblksize, protrecblksize, freeblksize, unknownsize : размерМЗ;
		heapsize : размерМЗ;
		blockSizes, recblkSizes, sysblkSizes, arrblkSizes, protrecblkSizes, freeblkSizes : SizeArray;

		проц {перекрыта}Update*;
		перем p : адресВПамяти; memBlock {неОтслСборщиком}: ЭВМ.УкльНаЗаголовокБлокаПамяти; heapBlock: Heaps.HeapBlock;
		нач
			blocks := 0; recblks := 0; sysblks := 0; arrblks := 0;
			protrecblks := 0; freeblks := 0;
			blockssize := 0; recblksize := 0; sysblksize := 0; arrblksize := 0;
			protrecblksize := 0; freeblksize := 0;
			ClearSizeArrays;
			ЭВМ.ЗапросиБлокировку(ЭВМ.ЯдернаяБлокировкаДляРаботыСКучей);
			heapsize := 0;
			memBlock := ЭВМ.первыйБлокПамятиДляКучи;
			нцПока memBlock # НУЛЬ делай
				p := memBlock.адресНачалаДанных;
				нцПока p # memBlock.адресЗаКонцомДанных делай
					heapBlock := НИЗКОУР.подмениТипЗначения(Heaps.HeapBlock, p + Heaps.BlockHeaderSize); (* get heap block *)
					увел(blocks);
					увел(blockssize, heapBlock.size);
					если ~(heapBlock суть Heaps.FreeBlock) то
						AddSize(heapBlock.size, blockSizes);
					всё;
					если heapBlock суть Heaps.SystemBlock то
						увел(sysblks); увел(sysblksize, heapBlock.size);
						AddSize(heapBlock.size, sysblkSizes);
					аесли heapBlock суть Heaps.ProtRecBlock то
						увел(protrecblks); увел(protrecblksize, heapBlock.size);
						AddSize(heapBlock.size, protrecblkSizes);
					аесли heapBlock суть Heaps.RecordBlock то
						увел(recblks); увел(recblksize, heapBlock.size);
						AddSize(heapBlock.size, recblkSizes);
					аесли heapBlock суть Heaps.ArrayBlock то
						увел(arrblks); увел(arrblksize, heapBlock.size);
						AddSize(heapBlock.size, arrblkSizes);
					аесли heapBlock суть Heaps.FreeBlock то
						увел(freeblks); увел(freeblksize, heapBlock.size);
						AddSize(heapBlock.size, freeblkSizes);
					всё;
					p := p + heapBlock.size
				кц;
				heapsize := heapsize + memBlock.адресЗаКонцомДанных - memBlock.адресНачалаДанных;
				memBlock := memBlock.следщ
			кц;
			ЭВМ.ОтпустиБлокировку(ЭВМ.ЯдернаяБлокировкаДляРаботыСКучей);
		кон Update;

		проц ClearSizeArrays;
		перем i : цел32;
		нач
			нцДля i := 0 до 26 делай
				blockSizes[i] := 0;
				recblkSizes[i] := 0;
				sysblkSizes[i] := 0;
				arrblkSizes[i] := 0;
				protrecblkSizes[i] := 0;
				freeblkSizes[i] := 0;
			кц;
		кон ClearSizeArrays;

		проц AddSize(size : размерМЗ; перем array : SizeArray);
		конст K = 1024; M = 1024*1024;
		нач
			если (size <= 320) то
				увел(array[(size DIV 32) - 1]);
			аесли (size < 1*K) то увел(array[10]);
			аесли (size < 2*K) то увел(array[11]);
			аесли (size < 4*K) то увел(array[12]);
			аесли (size < 8*K) то увел(array[13]);
			аесли (size < 16*K) то увел(array[14]);
			аесли (size < 32*K) то увел(array[15]);
			аесли (size < 64*K) то увел(array[16]);
			аесли (size < 128*K) то увел(array[17]);
			аесли (size < 256*K) то увел(array[18]);
			аесли (size < 512*K) то увел(array[19]);
			аесли (size < 1*M) то увел(array[20]);
			аесли (size < 2*M) то увел(array[21]);
			аесли (size < 4*M) то увел(array[22]);
			аесли (size < 8*M) то увел(array[23]);
			аесли (size < 16*M) то увел(array[24]);
			аесли (size < 32*M) то увел(array[25]);
			иначе
				увел(array[26]);
			всё;
		кон AddSize;

	кон BlocksHelper;

тип

	Blocks = окласс(WMPerfMonPlugins.Plugin)
	перем
		h : BlocksHelper;

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		перем ds : WMPerfMonPlugins.DatasetDescriptor;
		нач
			p.name := "Blocks"; p.description := "Number and types of blocks on the heap"; p.modulename := ModuleName;
			p.autoMax := истина; p.noSuperSampling := истина; (* since expensive *)
			p.minDigits := 9; p.fraction := 0;
			p.helper := blocksHelper; h := blocksHelper;

			нов(ds, 7);
			ds[0].name := "blocks"; ds[0].color := WMGraphics.White; включиВоМнвоНаБитах(ds[0].flags, WMPerfMonPlugins.Sum);
			ds[1].name := "recblocks"; ds[1].color := WMGraphics.Green;
			ds[2].name := "sysblocks"; ds[2].color := WMGraphics.Red;
			ds[3].name := "arrblocks"; ds[3].color := WMGraphics.Blue;
			ds[4].name := "protrecblocks"; ds[4].color := WMGraphics.Magenta;
			ds[5].name := "freeblocks"; ds[5].color := WMGraphics.Yellow;
			ds[6].name := "unknown"; ds[6].color := WMGraphics.Black;
			p.datasetDescriptor := ds;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		нач
			dataset[0] := h.blocks;
			dataset[1] := h.recblks;
			dataset[2] := h.sysblks;
			dataset[3] := h.arrblks;
			dataset[4] := h.protrecblks;
			dataset[5] := h.freeblks;
			dataset[6] := h.unknowns;
		кон UpdateDataset;

	кон Blocks;

тип

	BlockTotalSizes = окласс(WMPerfMonPlugins.Plugin)
	перем
		h : BlocksHelper;

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		перем ds : WMPerfMonPlugins.DatasetDescriptor;
		нач
			p.name := "TotalBlockSizes"; p.description := "Total sizes of blocks on the heap"; p.modulename := ModuleName;
			p.autoMax := истина; p.noSuperSampling := истина; (* since expensive *)
			p.minDigits := 9; p.fraction := 0; p.unit := "KB";
			p.helper := blocksHelper; h := blocksHelper;

			нов(ds, 7);
			ds[0].name := "blocks"; ds[0].color := WMGraphics.White; включиВоМнвоНаБитах(ds[0].flags, WMPerfMonPlugins.Sum);
			ds[1].name := "recblocks"; ds[1].color := WMGraphics.Green;
			ds[2].name := "sysblocks"; ds[2].color := WMGraphics.Red;
			ds[3].name := "arrblocks"; ds[3].color := WMGraphics.Blue;
			ds[4].name := "protrecblocks"; ds[4].color := WMGraphics.Magenta;
			ds[5].name := "freeblocks"; ds[5].color := WMGraphics.Yellow;
			ds[6].name := "unknown"; ds[6].color := WMGraphics.Black;
			p.datasetDescriptor := ds;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		нач
			dataset[0] := h.blockssize;
			dataset[1] := h.recblksize;
			dataset[2] := h.sysblksize;
			dataset[3] := h.arrblksize;
			dataset[4] := h.protrecblksize;
			dataset[5] := h.freeblksize;
			dataset[6] := h.unknownsize;
		кон UpdateDataset;

	кон BlockTotalSizes;

тип

	BlockSizesBase = окласс(WMPerfMonPlugins.Plugin)
	перем
		h : BlocksHelper;

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		перем ds : WMPerfMonPlugins.DatasetDescriptor;
		нач
			p.modulename := ModuleName;
			p.autoMax := истина; p.noSuperSampling := истина;
			p.minDigits := 9; p.fraction := 0; p.unit := "KB";
			p.helper := blocksHelper; h := blocksHelper;

			нов(ds, 27);
			ds[0].name := "32B";
			ds[1].name := "64B";
			ds[2].name := "92B";
			ds[3].name := "128B";
			ds[4].name := "160B";
			ds[5].name := "192B";
			ds[6].name := "224B";
			ds[7].name := "256B";
			ds[8].name := "288B";
			ds[9].name := "320B";
			ds[10].name := "320B-1KB";
			ds[11].name := "1-2KB";
			ds[12].name := "2-4KB";
			ds[13].name := "4-8KB";
			ds[14].name := "8-16KB";
			ds[15].name := "16-32KB";
			ds[16].name := "32-64KB";
			ds[17].name := "64-128KB";
			ds[18].name := "128-256KB";
			ds[19].name := "256-512KB";
			ds[20].name := "0.5-1MB";
			ds[21].name := "1-2MB";
			ds[22].name := "2-4MB";
			ds[23].name := "4-8MB";
			ds[24].name := "8-16MB";
			ds[25].name := "16-32MB";
			ds[26].name := ">32MB";
			p.datasetDescriptor := ds;
		кон Init;

	кон BlockSizesBase;

тип

	BlockSizes = окласс(BlockSizesBase)

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		нач
			Init^(p);
			p.name := "BlockSizes"; p.description := "Sizes of non-free blocks on the heap";
		кон Init;

		проц {перекрыта}UpdateDataset*;
		перем i : цел32;
		нач
			нцДля i := 0 до длинаМассива(h.blockSizes)-1 делай
				dataset[i] := h.blockSizes[i];
			кц;
		кон UpdateDataset;

	кон BlockSizes;

	SysBlockSizes = окласс(BlockSizesBase)

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		нач
			Init^(p);
			p.name := "SysBlockSizes"; p.description := "Sizes of system blocks on the heap";
		кон Init;

		проц {перекрыта}UpdateDataset*;
		перем i : цел32;
		нач
			нцДля i := 0 до длинаМассива(h.sysblkSizes)-1 делай
				dataset[i] := h.sysblkSizes[i];
			кц;
		кон UpdateDataset;

	кон SysBlockSizes;

	RecBlockSizes = окласс(BlockSizesBase)

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		нач
			Init^(p);
			p.name := "RecBlockSizes"; p.description := "Sizes of record blocks on the heap";
		кон Init;

		проц {перекрыта}UpdateDataset*;
		перем i : цел32;
		нач
			нцДля i := 0 до длинаМассива(h.recblkSizes)-1 делай
				dataset[i] := h.recblkSizes[i];
			кц;
		кон UpdateDataset;

	кон RecBlockSizes;

	ProtRecBlockSizes = окласс(BlockSizesBase)

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		нач
			Init^(p);
			p.name := "ProtRecBlockSizes"; p.description := "Sizes of protected record  blocks on the heap";
		кон Init;

		проц {перекрыта}UpdateDataset*;
		перем i : цел32;
		нач
			нцДля i := 0 до длинаМассива(h.protrecblkSizes)-1 делай
				dataset[i] := h.protrecblkSizes[i];
			кц;
		кон UpdateDataset;

	кон ProtRecBlockSizes;

	ArrBlockSizes = окласс(BlockSizesBase)

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		нач
			Init^(p);
			p.name := "ArrayBlockSizes"; p.description := "Sizes of array blocks on the heap";
		кон Init;

		проц {перекрыта}UpdateDataset*;
		перем i : цел32;
		нач
			нцДля i := 0 до длинаМассива(h.arrblkSizes)-1 делай
				dataset[i] := h.arrblkSizes[i];
			кц;
		кон UpdateDataset;

	кон ArrBlockSizes;

	FreeBlockSizes = окласс(BlockSizesBase)

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		нач
			Init^(p);
			p.name := "FreeBlockSizes"; p.description := "Sizes of free blocks on the heap";
		кон Init;

		проц {перекрыта}UpdateDataset*;
		перем i : цел32;
		нач
			нцДля i := 0 до длинаМассива(h.freeblkSizes)-1 делай
				dataset[i] := h.freeblkSizes[i];
			кц;
		кон UpdateDataset;

	кон FreeBlockSizes;

тип

	GcRuns = окласс(WMPerfMonPlugins.Plugin)

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		нач
			p.name := "GcRuns"; p.description := "Number of times the GC has been run since system start"; p.modulename := ModuleName;
			p.noSuperSampling := истина;
			p.autoMax := истина; p.minDigits := 5;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		нач
			dataset[0] := Heaps.Ngc;
		кон UpdateDataset;

	кон GcRuns;

тип

	NewBlockCalls = окласс(WMPerfMonPlugins.Plugin)

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		нач
			p.name := "NewBlocks"; p.description := "Number of times Heaps.NewBlock has been called since system startup";
			p.modulename := ModuleName;
			p.noSuperSampling := истина;
			p.autoMax := истина; p.minDigits := 5;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		нач
			dataset[0] := Heaps.Nnew;
		кон UpdateDataset;

	кон NewBlockCalls;

тип

	BytesAllocated = окласс(WMPerfMonPlugins.Plugin)

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		нач
			p.name := "KBAllocated"; p.description := "KBytes allocated since system start"; p.modulename := ModuleName;
			p.noSuperSampling := истина; p.unit := "KB";
			p.autoMax := истина; p.minDigits := 5;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		нач
			dataset[0] := Heaps.NnewBytes DIV 1024;
		кон UpdateDataset;

	кон BytesAllocated;

перем
	blocksHelper : BlocksHelper;

проц Install*;
кон Install;

проц InitPlugins;
перем
	par : WMPerfMonPlugins.Parameter;
	blocks : Blocks; blockTotalSizes : BlockTotalSizes;
	blockSizes : BlockSizes;
	sysBlockSizes : SysBlockSizes;
	recBlockSizes : RecBlockSizes;
	protRecBlockSizes : ProtRecBlockSizes;
	arrBlockSizes : ArrBlockSizes;
	freeBlockSizes : FreeBlockSizes;
	gcRuns : GcRuns;
	newBlockCalls : NewBlockCalls; bytesAllocated : BytesAllocated;
нач
	нов(par); нов(blocks, par);
	нов(par); нов(blockTotalSizes, par);
	нов(par); нов(blockSizes, par);
	нов(par); нов(sysBlockSizes, par);
	нов(par); нов(recBlockSizes, par);
	нов(par); нов(protRecBlockSizes, par);
	нов(par); нов(arrBlockSizes, par);
	нов(par); нов(freeBlockSizes, par);
	нов(par); нов(gcRuns, par);
	нов(par); нов(newBlockCalls, par);
	нов(par); нов(bytesAllocated, par);
кон InitPlugins;

проц Cleanup;
нач
	если Heaps.Stats то
		WMPerfMonPlugins.updater.RemoveByModuleName(ModuleName);
	всё;
кон Cleanup;

нач
	утв(BlockSize = Heaps.BlockSize);
	нов(blocksHelper);
	InitPlugins;
	Modules.InstallTermHandler(Cleanup);
кон WMPerfMonPluginHeap.

WMPerfMonPluginHeap.Install ~   System.Free WMPerfMonPluginHeap ~
