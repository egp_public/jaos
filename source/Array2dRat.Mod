(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль Array2dRat;   (**AUTHOR "adf, fof"; PURPOSE "Basic operations on type ARRAY OF ARRAY OF Rational **)

использует НИЗКОУР, Array1dBytes, NbrInt, NbrRat, NbrRe, Array1d := Array1dRat, ArrayXd := ArrayXdRat, Array1dInt, Array2dInt,
	DataErrors;

тип
	Value* = Array1d.Value;  RealValue* = NbrRe.Real;  Array* = ArrayXd.Array2;  Index* = NbrInt.Integer;

	проц Copy*( перем src: массив из массив из Value;  перем dest: массив из массив из Value;  srcx, srcy, destx, desty, w, h: Index );
	(** ccordinates: A[y,x] *)
	перем y: Index;
	нач
		Array1dBytes.RangeCheck2( srcx, srcy, w, h, длинаМассива( src[0] ), длинаМассива( src ) );
		Array1dBytes.RangeCheck2( destx, desty, w, h, длинаМассива( dest[0] ), длинаМассива( dest ) );

		если (адресОт( src[0] ) = адресОт( dest[0] )) (* same array *) и (srcy < desty) то  (*reverse copy order *)
			y := h - 1;
			нцПока (y >= 0) делай Array1d.Copy( src[srcy + y], dest[desty + y], srcx, destx, w );  умень( y ) кц;
		иначе
			y := 0;
			нцПока (y < h) делай Array1d.Copy( src[srcy + y], dest[desty + y], srcx, destx, w );  увел( y );  кц;
		всё;
	кон Copy;

	проц Fill*( val: Value;  перем res: массив из массив из Value;  x, y, w, h: Index );
	перем i: Index;
	нач
		Array1dBytes.RangeCheck2( x, y, w, h, длинаМассива( res[0] ), длинаМассива( res ) );

		i := 0;
		нцПока (i < h) делай Array1d.Fill( val, res[i + y], x, w );  увел( i );  кц;
	кон Fill;

	проц MinMax*( перем s: массив из массив из Value;  x, y, w, h: Index;  перем min, max: Value;
										 перем minx, miny, maxx, maxy: Index );
	перем cmin, cmax: Value;  cminpos, cmaxpos, i: Index;
	нач
		Array1dBytes.RangeCheck2( x, y, w, h, длинаМассива( s[0] ), длинаМассива( s ) );

		min := s[y, x];  max := s[y, x];  minx := x;  miny := y;  maxx := x;  maxy := y;
		нцДля i := y до y + h - 1 делай
			Array1d.MinMax( s[i], x, w, cmin, cmax, cminpos, cmaxpos );
			если cmin < min то min := cmin;  minx := cminpos;  miny := i;  всё;
			если cmax > max то max := cmax;  maxx := cmaxpos;  maxy := i;  всё
		кц
	кон MinMax;

	проц kSmallest*( k: Index;  перем s: массив из массив из Value;  x, y, w, h: Index ): Value;
	(** does not modify S*)
	перем values: Array1d.Array;  i: Index;
	нач
		Array1dBytes.RangeCheck2( x, y, w, h, длинаМассива( s[0] ), длинаМассива( s ) );

		нов( values, w * h );
		нцДля i := y до y + h - 1 делай Array1d.Copy( s[i], values^, x, i * w, w );  кц;
		возврат Array1d.kSmallestModify( k, values^, w * h )
	кон kSmallest;

	проц Median*( перем s: массив из массив из Value;  x, y, w, h: Index ): Value;
	нач
		возврат kSmallest( w * h DIV 2, s, x, y, w, h )
	кон Median;

	проц MeanSsq*( перем s: массив из массив из Value;  x, y, w, h: Index;  перем mean, ssq: RealValue );
	(* mean and ssq distance of mean by provisional means algorithm *)
	перем d: RealValue;  val: Value;  i, j: Index;
	нач
		Array1dBytes.RangeCheck2( x, y, w, h, длинаМассива( s[0] ), длинаМассива( s ) );

		mean := 0;  ssq := 0;
		нцДля i := 0 до h - 1 делай
			нцДля j := 0 до w - 1 делай
				val := s[i + y, j + x];  d := val - mean;  mean := mean + d / ((i * w + j) + 1);  ssq := ssq + d * (val - mean);
			кц
		кц;
	кон MeanSsq;

	проц CopyRow*( y: Index;  перем s: массив из массив из Value;  перем res: массив из Value;  srcoffset, destoffset, len: Index );
	нач
		(* asserts in Array1d *)
		Array1d.Copy( s[y], res, srcoffset, destoffset, len );
	кон CopyRow;

	проц CopyCol*( x: Index;  перем s: массив из массив из Value;  перем res: массив из Value;  srcoffset, destoffset, len: Index );
	нач
		Array1dBytes.RangeCheck2( x, srcoffset, 1, len, длинаМассива( s[0] ), длинаМассива( s ) );  Array1dBytes.RangeCheck( destoffset, len, длинаМассива( res ) );

		увел( len, srcoffset );
		нцПока (srcoffset < len) делай res[destoffset] := s[srcoffset, x];  увел( srcoffset );  увел( destoffset );  кц;
	кон CopyCol;

	проц CopyToRow*( перем s: массив из Value;  y: Index;  перем res: массив из массив из Value;
											  srcoffset, destoffset, len: Index );
	нач
		(* asserts in Array1d *)
		Array1d.Copy( s, res[y], srcoffset, destoffset, len );
	кон CopyToRow;

	проц CopyToCol*( перем s: массив из Value;  x: Index;  перем res: массив из массив из Value;  srcoffset, destoffset, len: Index );
	нач
		Array1dBytes.RangeCheck2( x, destoffset, 1, len, длинаМассива( res[0] ), длинаМассива( res ) );  Array1dBytes.RangeCheck( srcoffset, len, длинаМассива( s ) );

		увел( len, srcoffset );
		нцПока (srcoffset < len) делай res[destoffset, x] := s[srcoffset];  увел( srcoffset );  увел( destoffset );  кц;
	кон CopyToCol;

	проц Row*( y: Index;  перем s: массив из массив из Value ): Array1d.Array;
	перем res: Array1d.Array;  len: Index;
	нач
		len := длинаМассива( s[0] );  нов( res, len );  CopyRow( y, s, res^, 0, 0, len );  возврат res;
	кон Row;

	проц Col*( x: Index;  перем s: массив из массив из Value ): Array1d.Array;
	перем res: Array1d.Array;  len: Index;
	нач
		len := длинаМассива( s );  нов( res, len );  CopyCol( x, s, res^, 0, 0, len );  возврат res;
	кон Col;

	проц Transposed*( перем s: массив из массив из Value ): Array;
	перем res: Array;  x, y, w, h: Index;
	нач
		h := длинаМассива( s );  w := длинаМассива( s[0] );  нов( res, w, h );
		нцДля y := 0 до h - 1 делай
			нцДля x := 0 до w - 1 делай res[x, y] := s[y, x];  кц;
		кц;
		возврат res;
	кон Transposed;

	проц SwapRows*( перем s: массив из массив из Value;  y1, y2: Index );
	перем temp: Value;  w, i: Index;
	нач
		Array1dBytes.RangeCheck2( y1, y2, 0, 0, длинаМассива( s ), длинаМассива( s ) );

		w := длинаМассива( s[0] );
		нцДля i := 0 до w - 1 делай temp := s[y1, i];  s[y1, i] := s[y2, i];  s[y2, i] := temp кц
	кон SwapRows;

	проц SwapCols*( перем s: массив из массив из Value;  x1, x2: Index );
	перем temp: Value;  h, i: Index;
	нач
		Array1dBytes.RangeCheck2( x1, x2, 0, 0, длинаМассива( s[0] ), длинаМассива( s[0] ) );

		h := длинаМассива( s );
		нцДля i := 0 до h - 1 делай temp := s[i, x1];  s[i, x1] := s[i, x2];  s[i, x2] := temp кц
	кон SwapCols;


(** Monadic Operator - does not overwrite the argument *)
	операция "-"*( x: Array ): Array;
	перем i, k, cols, rows: Index;  minus: Array;
	нач
		если x # НУЛЬ то
			rows := длинаМассива( x, 0 );  cols := длинаМассива( x, 1 );  нов( minus, rows, cols );
			нцДля i := 0 до rows - 1 делай
				нцДля k := 0 до cols - 1 делай minus[i, k] := -x[i, k] кц
			кц
		иначе DataErrors.Error( "The supplied Array matrix was NIL." )
		всё;
		возврат minus
	кон "-";

	операция ":="*( перем l: Array;  r: Value );
	нач
		если l # НУЛЬ то Fill( r, l^, 0, 0, длинаМассива( l[0] ), длинаМассива( l ) );  иначе DataErrors.Error( "The supplied instance of Array2dRe.Array was NIL." ) всё
	кон ":=";

	операция ":="*( перем l: Array;  перем r: массив из массив из Value );
	перем i, k, cols, rows: Index;
	нач
		rows := длинаМассива( r, 0 );  cols := длинаМассива( r, 1 );
		если l = НУЛЬ то нов( l, rows, cols )
		аесли (длинаМассива( l, 0 ) # rows) или (длинаМассива( l, 1 ) # cols) то нов( l, rows, cols )
		иначе  (* matrix l is properly dimensioned *)
		всё;
		нцДля i := 0 до rows - 1 делай
			нцДля k := 0 до cols - 1 делай l[i, k] := r[i, k] кц
		кц
	кон ":=";

	операция ":="*( перем l: Array;  r: Array2dInt.Array );
	перем i, k, cols, rows: NbrInt.Integer;
	нач
		если r # НУЛЬ то
			rows := длинаМассива( r, 0 );  cols := длинаМассива( r, 1 );
			если l = НУЛЬ то нов( l, rows, cols )
			аесли (длинаМассива( l, 0 ) # rows) или (длинаМассива( l, 1 ) # cols) то нов( l, rows, cols )
			иначе  (* matrix l is properly dimensioned *)
			всё;
			нцДля i := 0 до rows - 1 делай
				нцДля k := 0 до cols - 1 делай l[i, k] := r[i, k] кц
			кц
		иначе DataErrors.Error( "The supplied instance of Array2dInt.Array was NIL." )
		всё
	кон ":=";

	операция ":="*( перем l: Array;  перем r: массив из массив из NbrInt.Integer );
	перем i, k, cols, rows: NbrInt.Integer;
	нач
		rows := длинаМассива( r, 0 );  cols := длинаМассива( r, 1 );
		если l = НУЛЬ то нов( l, rows, cols )
		аесли (длинаМассива( l, 0 ) # rows) или (длинаМассива( l, 1 ) # cols) то нов( l, rows, cols )
		иначе  (* matrix l is properly dimensioned *)
		всё;
		нцДля i := 0 до rows - 1 делай
			нцДля k := 0 до cols - 1 делай l[i, k] := r[i, k] кц
		кц
	кон ":=";

(** Arithmetic. Operators do not overwrite the arguments. *)
	операция "+"*( l, r: Array ): Array;
	перем i, k, cols, rows: Index;  sum: Array;
	нач
		если (l # НУЛЬ ) и (r # НУЛЬ ) то
			если (длинаМассива( l, 0 ) = длинаМассива( r, 0 )) и (длинаМассива( l, 1 ) = длинаМассива( r, 1 )) то
				rows := длинаМассива( r, 0 );  cols := длинаМассива( r, 1 );  нов( sum, rows, cols );
				нцДля i := 0 до rows - 1 делай
					нцДля k := 0 до cols - 1 делай sum[i, k] := l[i, k] + r[i, k] кц
				кц
			иначе DataErrors.Error( "The sizes of the two supplied Array matrices were not equal." )
			всё
		иначе DataErrors.Error( "One or both of the two supplied Array matrices was NIL." )
		всё;
		возврат sum
	кон "+";

	операция "+"*( l: Array;  r: Array2dInt.Array ): Array;
	перем i, k, cols, rows: NbrInt.Integer;  sum: Array;
	нач
		если (l # НУЛЬ ) и (r # НУЛЬ ) то
			если (длинаМассива( l, 0 ) = длинаМассива( r, 0 )) и (длинаМассива( l, 1 ) = длинаМассива( r, 1 )) то
				rows := длинаМассива( r, 0 );  cols := длинаМассива( r, 1 );  нов( sum, rows, cols );
				нцДля i := 0 до rows - 1 делай
					нцДля k := 0 до cols - 1 делай sum[i, k] := l[i, k] + r[i, k] кц
				кц
			иначе DataErrors.Error( "The sizes of the two supplied mixed-type Array matrices were not equal." )
			всё
		иначе DataErrors.Error( "One or both of the two supplied mixed-type Array matrices was NIL." )
		всё;
		возврат sum
	кон "+";

	операция "+"*( l: Array2dInt.Array;  r: Array ): Array;
	перем i, k, cols, rows: NbrInt.Integer;  sum: Array;
	нач
		если (l # НУЛЬ ) и (r # НУЛЬ ) то
			если (длинаМассива( l, 0 ) = длинаМассива( r, 0 )) и (длинаМассива( l, 1 ) = длинаМассива( r, 1 )) то
				rows := длинаМассива( r, 0 );  cols := длинаМассива( r, 1 );  нов( sum, rows, cols );
				нцДля i := 0 до rows - 1 делай
					нцДля k := 0 до cols - 1 делай sum[i, k] := l[i, k] + r[i, k] кц
				кц
			иначе DataErrors.Error( "The sizes of the two supplied mixed-type Array matrices were not equal." )
			всё
		иначе DataErrors.Error( "One or both of the two supplied mixed-type Array matrices was NIL." )
		всё;
		возврат sum
	кон "+";

	операция "-"*( l, r: Array ): Array;
	перем i, k, cols, rows: Index;  diff: Array;
	нач
		если (l # НУЛЬ ) и (r # НУЛЬ ) то
			если (длинаМассива( l, 0 ) = длинаМассива( r, 0 )) и (длинаМассива( l, 1 ) = длинаМассива( r, 1 )) то
				rows := длинаМассива( r, 0 );  cols := длинаМассива( r, 1 );  нов( diff, rows, cols );
				нцДля i := 0 до rows - 1 делай
					нцДля k := 0 до cols - 1 делай diff[i, k] := l[i, k] - r[i, k] кц
				кц
			иначе DataErrors.Error( "The sizes of the two supplied Array matrices were not equal." )
			всё
		иначе DataErrors.Error( "One or both of the two supplied Array matrices was NIL." )
		всё;
		возврат diff
	кон "-";

	операция "-"*( l: Array;  r: Array2dInt.Array ): Array;
	перем i, k, cols, rows: NbrInt.Integer;  diff: Array;
	нач
		если (l # НУЛЬ ) и (r # НУЛЬ ) то
			если (длинаМассива( l, 0 ) = длинаМассива( r, 0 )) и (длинаМассива( l, 1 ) = длинаМассива( r, 1 )) то
				rows := длинаМассива( r, 0 );  cols := длинаМассива( r, 1 );  нов( diff, rows, cols );
				нцДля i := 0 до rows - 1 делай
					нцДля k := 0 до cols - 1 делай diff[i, k] := l[i, k] - r[i, k] кц
				кц
			иначе DataErrors.Error( "The sizes of the two supplied mixed-type Array matrices were not equal." )
			всё
		иначе DataErrors.Error( "One or both of the two supplied mixed-type Array matrices was NIL." )
		всё;
		возврат diff
	кон "-";

	операция "-"*( l: Array2dInt.Array;  r: Array ): Array;
	перем i, k, cols, rows: NbrInt.Integer;  diff: Array;
	нач
		если (l # НУЛЬ ) и (r # НУЛЬ ) то
			если (длинаМассива( l, 0 ) = длинаМассива( r, 0 )) и (длинаМассива( l, 1 ) = длинаМассива( r, 1 )) то
				rows := длинаМассива( r, 0 );  cols := длинаМассива( r, 1 );  нов( diff, rows, cols );
				нцДля i := 0 до rows - 1 делай
					нцДля k := 0 до cols - 1 делай diff[i, k] := l[i, k] - r[i, k] кц
				кц
			иначе DataErrors.Error( "The sizes of the two supplied mixed-type Array matrices were not equal." )
			всё
		иначе DataErrors.Error( "One or both of the two supplied mixed-type Array matrices was NIL." )
		всё;
		возврат diff
	кон "-";

(** Array dot product *)
	операция "*"*( l, r: Array ): Array;
	(** Caution:  Use brackets to ensure proper matrix multiplication when contracting three or more matrices,
				e.g., A*(B*C) is correct, whereas A*B*C is not.  This is because matrix multiplician is from right to left;
				whereas, the Oberon programming languages multiplies from left to right. *)
	перем i, j, k, cols, dummy, rows: Index;  dot: Array;  sum: Value;
	нач
		если (l # НУЛЬ ) и (r # НУЛЬ ) то
			если длинаМассива( l, 1 ) = длинаМассива( r, 0 ) то
				rows := длинаМассива( l, 0 );  cols := длинаМассива( r, 1 );  dummy := длинаМассива( r, 0 );  нов( dot, rows, cols );
				нцДля i := 0 до rows - 1 делай
					нцДля j := 0 до cols - 1 делай
						sum := 0;
						нцДля k := 0 до dummy - 1 делай sum := sum + l[i, k] * r[k, j] кц;
						dot[i, j] := sum
					кц
				кц
			иначе DataErrors.Error( "The sizes were incompatible, i.e., LEN(l,1) # LEN(r,0)." )
			всё
		иначе DataErrors.Error( "One or both of the two supplied Array matrices was NIL." )
		всё;
		возврат dot
	кон "*";

	операция "*"*( l: Array;  r: Array2dInt.Array ): Array;
	перем i, j, k, cols, dummy, rows: NbrInt.Integer;  sum: NbrRat.Rational;  dot: Array;
	нач
		если (l # НУЛЬ ) и (r # НУЛЬ ) то
			если длинаМассива( l, 1 ) = длинаМассива( r, 0 ) то
				rows := длинаМассива( l, 0 );  cols := длинаМассива( r, 1 );  dummy := длинаМассива( r, 0 );  нов( dot, rows, cols );
				нцДля i := 0 до rows - 1 делай
					нцДля j := 0 до cols - 1 делай
						sum := 0;
						нцДля k := 0 до dummy - 1 делай sum := sum + l[i, k] * r[k, j] кц;
						dot[i, j] := sum
					кц
				кц
			иначе DataErrors.Error( "The sizes were incompatible, i.e., LEN(l,1) # LEN(r,0)." )
			всё
		иначе DataErrors.Error( "One or both of the two supplied mixed-type Array matrices was NIL." )
		всё;
		возврат dot
	кон "*";

	операция "*"*( l: Array2dInt.Array;  r: Array ): Array;
	(** Caution:  Use brackets to ensure proper matrix multiplication when contracting three or more matrices,
				e.g., A*(B*C) is correct, whereas A*B*C is not.  This is because matrix multiplician is from right to left;
				whereas, the Oberon programming languages multiplies from left to right. *)
	перем i, j, k, cols, dummy, rows: NbrInt.Integer;  sum: NbrRat.Rational;  dot: Array;
	нач
		если (l # НУЛЬ ) и (r # НУЛЬ ) то
			если длинаМассива( l, 1 ) = длинаМассива( r, 0 ) то
				rows := длинаМассива( l, 0 );  cols := длинаМассива( r, 1 );  dummy := длинаМассива( r, 0 );  нов( dot, rows, cols );
				нцДля i := 0 до rows - 1 делай
					нцДля j := 0 до cols - 1 делай
						sum := 0;
						нцДля k := 0 до dummy - 1 делай sum := sum + l[i, k] * r[k, j] кц;
						dot[i, j] := sum
					кц
				кц
			иначе DataErrors.Error( "The sizes were incompatible, i.e., LEN(l,1) # LEN(r,0)." )
			всё
		иначе DataErrors.Error( "One or both of the two supplied mixed-type Array matrices was NIL." )
		всё;
		возврат dot
	кон "*";

(** Array-Vector contraction,  returns  x = A v  or x[i] = A[i, k] v[k] *)
	операция "*"*( l: Array;  r: Array1d.Array ): Array1d.Array;
	перем i, k, dummy, rows: Index;  dot: Array1d.Array;  sum: Value;
	нач
		если (l # НУЛЬ ) и (r # НУЛЬ ) то
			если длинаМассива( l, 1 ) = длинаМассива( r ) то
				rows := длинаМассива( l, 0 );  dummy := длинаМассива( l, 1 );  нов( dot, rows );
				нцДля i := 0 до rows - 1 делай
					sum := 0;
					нцДля k := 0 до dummy - 1 делай sum := sum + l[i, k] * r[k] кц;
					dot[i] := sum
				кц
			иначе DataErrors.Error( "The sizes were incompatible, i.e., LEN(l,1) # LEN(r)." )
			всё
		иначе DataErrors.Error( "Either the Array matrix or Vector vector supplied was NIL." )
		всё;
		возврат dot
	кон "*";

(** Vector-Array contraction,  returns  x = ATv = vTA  or x[i] = A[k, i] v[k] = v[k] A[k, i]  *)
	операция "*"*( l: Array1d.Array;  r: Array ): Array1d.Array;
	перем i, k, cols, dummy: Index;  dot: Array1d.Array;  sum: Value;
	нач
		если (l # НУЛЬ ) и (r # НУЛЬ ) то
			если длинаМассива( l ) = длинаМассива( r, 0 ) то
				cols := длинаМассива( r, 1 );  dummy := длинаМассива( r, 0 );  нов( dot, cols );
				нцДля i := 0 до cols - 1 делай
					sum := 0;
					нцДля k := 0 до dummy - 1 делай sum := sum + l[k] * r[k, i] кц;
					dot[i] := sum
				кц
			иначе DataErrors.Error( "The sizes were incompatible, i.e., LEN(l,0) # LEN(r)." )
			всё
		иначе DataErrors.Error( "Either the Vector vector or Array matrix supplied was NIL." )
		всё;
		возврат dot
	кон "*";

	операция "*"*( l: Array;  r: Array1dInt.Array ): Array1d.Array;
	перем i, k, dummy, rows: NbrInt.Integer;  sum: NbrRat.Rational;  dot: Array1d.Array;
	нач
		если (l # НУЛЬ ) и (r # НУЛЬ ) то
			если длинаМассива( l, 1 ) = длинаМассива( r ) то
				rows := длинаМассива( l, 0 );  dummy := длинаМассива( l, 1 );  нов( dot, rows );
				нцДля i := 0 до rows - 1 делай
					sum := 0;
					нцДля k := 0 до dummy - 1 делай sum := sum + l[i, k] * r[k] кц;
					dot[i] := sum
				кц
			иначе DataErrors.Error( "The sizes were incompatible, i.e., LEN(l,1) # LEN(r)." )
			всё
		иначе DataErrors.Error( "Either the Array matrix or Vector vector supplied was NIL." )
		всё;
		возврат dot
	кон "*";

	операция "*"*( l: Array1dInt.Array;  r: Array ): Array1d.Array;
	перем i, k, cols, dummy: NbrInt.Integer;  sum: NbrRat.Rational;  dot: Array1d.Array;
	нач
		если (l # НУЛЬ ) и (r # НУЛЬ ) то
			если длинаМассива( l ) = длинаМассива( r, 0 ) то
				cols := длинаМассива( r, 1 );  dummy := длинаМассива( r, 0 );  нов( dot, cols );
				нцДля i := 0 до cols - 1 делай
					sum := 0;
					нцДля k := 0 до dummy - 1 делай sum := sum + l[k] * r[k, i] кц;
					dot[i] := sum
				кц
			иначе DataErrors.Error( "The sizes were incompatible, i.e., LEN(l,0) # LEN(r)." )
			всё
		иначе DataErrors.Error( "Either the Vector vector or Array matrix supplied was NIL." )
		всё;
		возврат dot
	кон "*";


(** Scalar multiplication *)
	операция "*"*( l: Value;  r: Array ): Array;
	перем i, k, cols, rows: Index;  prod: Array;
	нач
		если r # НУЛЬ то
			rows := длинаМассива( r, 0 );  cols := длинаМассива( r, 1 );  нов( prod, rows, cols );
			нцДля i := 0 до rows - 1 делай
				нцДля k := 0 до cols - 1 делай prod[i, k] := l * r[i, k] кц
			кц
		иначе DataErrors.Error( "The supplied Array matrix was NIL." )
		всё;
		возврат prod
	кон "*";

	операция "*"*( l: Array;  r: Value ): Array;
	перем i, k, cols, rows: Index;  prod: Array;
	нач
		если l # НУЛЬ то
			rows := длинаМассива( l, 0 );  cols := длинаМассива( l, 1 );  нов( prod, rows, cols );
			нцДля i := 0 до rows - 1 делай
				нцДля k := 0 до cols - 1 делай prod[i, k] := l[i, k] * r кц
			кц
		иначе DataErrors.Error( "The supplied Array matrix was NIL." )
		всё;
		возврат prod
	кон "*";

	операция "*"*( l: NbrInt.Integer;  r: Array ): Array;
	перем i, k, cols, rows: NbrInt.Integer;  left: NbrRat.Rational;  prod: Array;
	нач
		если r # НУЛЬ то
			rows := длинаМассива( r, 0 );  cols := длинаМассива( r, 1 );  нов( prod, rows, cols );  left := l;
			нцДля i := 0 до rows - 1 делай
				нцДля k := 0 до cols - 1 делай prod[i, k] := left * r[i, k] кц
			кц
		иначе DataErrors.Error( "The supplied Array matrix was NIL." )
		всё;
		возврат prod
	кон "*";

	операция "*"*( l: Array;  r: NbrInt.Integer ): Array;
	перем i, k, cols, rows: NbrInt.Integer;  right: NbrRat.Rational;  prod: Array;
	нач
		если l # НУЛЬ то
			rows := длинаМассива( l, 0 );  cols := длинаМассива( l, 1 );  нов( prod, rows, cols );  right := r;
			нцДля i := 0 до rows - 1 делай
				нцДля k := 0 до cols - 1 делай prod[i, k] := l[i, k] * right кц
			кц
		иначе DataErrors.Error( "The supplied Array matrix was NIL." )
		всё;
		возврат prod
	кон "*";

(** Scalar division *)
	операция "/"*( l: Array;  r: Value ): Array;
	перем i, k, cols, rows: Index;  div: Array;
	нач
		если l # НУЛЬ то
			если r # 0 то
				rows := длинаМассива( l, 0 );  cols := длинаМассива( l, 1 );  нов( div, rows, cols );
				нцДля i := 0 до rows - 1 делай
					нцДля k := 0 до cols - 1 делай div[i, k] := l[i, k] / r кц
				кц
			иначе DataErrors.Error( "Division by zero." )
			всё
		иначе DataErrors.Error( "The supplied Array matrix was NIL." )
		всё;
		возврат div
	кон "/";

	операция "/"*( l: Array;  r: NbrInt.Integer ): Array;
	перем i, k, cols, rows: NbrInt.Integer;  right: NbrRat.Rational;  div: Array;
	нач
		если l # НУЛЬ то
			right := r;
			если right # 0 то
				rows := длинаМассива( l, 0 );  cols := длинаМассива( l, 1 );  нов( div, rows, cols );
				нцДля i := 0 до rows - 1 делай
					нцДля k := 0 до cols - 1 делай div[i, k] := l[i, k] / right кц
				кц
			иначе DataErrors.Error( "Division by Integer zero." )
			всё
		иначе DataErrors.Error( "The supplied Array matrix was NIL." )
		всё;
		возврат div
	кон "/";

кон Array2dRat.
