(* Paco, Copyright 2000 - 2002, Patrik Reali, ETH Zurich *)

модуль PCBT; (** AUTHOR "prk / be"; PURPOSE "Parallel Compiler: back-end common structures"; *)

использует
		НИЗКОУР, PCM, PCT;

конст
	(*static buffer for constants*)
	MaxConstSize = 2147483647; (* 2^31 - 1, i.e. the maximum positive SIGNED32 number *)


	(** system calls *)
	DefaultNofSysCalls* = 12;
	newrec* = 0;  newarr* = 1;  newsys* = 2;  casetable* = 3;  procaddr* = 4;
	lock* = 5;  unlock* = 6;  start* = 7;  passivate* = 8; interfacelookup* = 9;
	registerinterface* = 10; getprocedure* = 11;

	(** Fixup, last address in a fixup chain *)
	FixupSentinel* = цел32(0FFFFFFFFH);
	UndefEntryNo* = -1;	(** GlobalVariable.EntryNo *)

	(** Calling Conventions *)
	OberonCC* = 1;  OberonPassivateCC* = 2;  WinAPICC* = 3; (* ejz *) CLangCC*= 4; (* fof for Linux *)

перем
	init: булево;

тип
	ConstArray* = укль на массив из симв8;

	(** ----------- Size - type related information -------------- *)

	Size* = окласс (PCM.Attribute)
	перем
		size*: цел32;	(** size in bytes, used for allocation*)
		align*: цел32;	(** on which boundary should this type be aligned *)
		type*: цел8;	(** Back-End Type *)
		signed*: булево;
		containPtrs*: булево;
		needsrecursion*: булево;
	кон Size;

	RecSize* = окласс (Size)
	перем
		td*: GlobalVariable;
		level*: цел32;
		nofMethods*, nofLocalMethods*: цел32;
	кон RecSize;



	(** ----------- Address - allocation related information ------- *)

	(** Fixup - offset to be patched at a later time *)
	Fixup* = укль на запись
		offset-: цел32;
		next-: Fixup
	кон;

	Variable* = окласс (PCM.Attribute)
	перем
		offset*: цел32
	кон Variable;

	GlobalVariable* = окласс (Variable)
		перем
			owner-: Module;
			entryNo*: цел16;	(** object-file information set by PCOF *)

			link-: Fixup;	(** occourencies of var in code *)
			next-: GlobalVariable;	(** next GVar in the list *)

		проц AddFixup(offset: цел32);
			перем l: Fixup;
		нач
			нов(l); l.offset := offset; l.next := link; link := l
		кон AddFixup;

		(** Init - Initialize structure *)

		проц & Init*(owner: Module);
		нач
			сам.owner := owner;
			entryNo := UndefEntryNo;
			утв((owner # НУЛЬ) или (сам = sentinel) или (sentinel = НУЛЬ));
		кон Init;
	кон GlobalVariable;

	(* ug *)
	Attribute* = окласс(PCM.Attribute)	(* common part of Procedure and Module *)
		перем
			codeoffset-: цел32;  (* entry point relative to code base of owner*)
			beginOffset- : цел32;	 (* entry point relative to code base of owner*)	(* ug *)
			endOffset- : цел32;  (* offset of statement block end relative to code base *)	(* ug *)

		проц SetBeginOffset*(offset : цел32);  (* ug *)
		нач
			beginOffset := offset
		кон SetBeginOffset;

		проц SetEndOffset*(offset : цел32); (* ug *)
		нач
			endOffset := offset
		кон SetEndOffset;

	кон Attribute;

	Procedure* = окласс (Attribute)
		перем
			owner-: Module;
			public-: булево;
			locsize*: цел32;	(* local variables size *)
			parsize*: цел32;	(* parameters size incl. self/sl, return addr and dynamic link *)
			entryNr*, fixlist*: цел32;	(* set/used by PCOF *)
			next-: Procedure;	(* entrylist *)
			link-: Fixup;
			finallyOff*: цел32; (* Offset of the handled region. Relative to the codeoffset. *)

		проц AddFixup(offset: цел32);
			перем l: Fixup;
		нач
			нов(l); l.offset := offset; l.next := link; link := l
		кон AddFixup;

		проц & Init*(owner: Module;  public: булево);
		нач
			утв((owner # НУЛЬ) или init);
			сам.owner := owner;
			сам.public := public;
			fixlist := FixupSentinel;
			finallyOff := -1;
		кон Init;

	кон Procedure;

	Method* = окласс (Procedure)
	перем
		mthNo*: цел32
	кон Method;

	Module* = окласс (Attribute)
		перем
			locsize*: цел32;			(** data section size*)
			constsize*: цел16;			(** const section size*)
			casetablesize*: цел16;  	(** length of case table within const section *) (* ug *)
			nr*: цел16;

			const*: ConstArray;

			OwnProcs-: Procedure;		(** this module's procedures; terminated by psentinel *)
			ExtProcs-: Procedure;			(** external procedures used; terminated by psentinel *)

			OwnVars-: GlobalVariable;	(** this module's used variables; terminated by sentinel *)
			ExtVars-: GlobalVariable;	(** external used variables; terminated by sentinel *)

			syscalls-: укль на массив из Fixup;	(** syscalls' fixup lists *)

			finallyOff*: цел32; (* offset of the finally in the module init code *)

		проц & Init*;
		нач
			нов(syscalls, NofSysCalls);
			нов(const, 128);
			ResetLists;
			constsize := 0;
			nr := 0;
			finallyOff := -1;
		кон Init;

		(** ResetLists - remove entries from OwnVars, ExtVars, OwnProcs, ExtProcs *)

		проц ResetLists*;
			перем i: цел32;

			проц KillPList(перем root: Procedure);
				перем p, q: Procedure;
			нач
				p := root; root := psentinel;
				нцПока p # НУЛЬ делай
					q := p;
					p := p.next;
					q.link := НУЛЬ;
					q.next := НУЛЬ
				кц;
			кон KillPList;

			проц KillVList(перем root: GlobalVariable);
				перем p, q: GlobalVariable;
			нач
				p := root; root := sentinel;
				нцПока p # НУЛЬ делай
					q := p;
					p := p.next;
					q.entryNo := UndefEntryNo;
					q.link := НУЛЬ;
					q.next := НУЛЬ;
				кц;
			кон KillVList;

		нач
			KillPList(OwnProcs); KillPList(ExtProcs);
			KillVList(OwnVars); KillVList(ExtVars);
			нцДля i := 0 до NofSysCalls-1 делай  syscalls[i] := НУЛЬ  кц;
		кон ResetLists;

		(*fof: increase constant section size - for case tables - thread safe version for PCG386*)
		проц AddCasetable*(tablesize: цел32): цел32;
		перем size,base: цел32; c: ConstArray;
		нач{единолично}
			size := constsize+tablesize*4;
			утв(size < MaxConstSize);
			если size >= длинаМассива(const^) то
				увел(size,(-size) остОтДеленияНа 256); (* align to 255 bytes to prevent from allocating too often *)
				нов(c, size);
				НИЗКОУР.копируйПамять(адресОт(const[0]), адресОт(c[0]), длинаМассива(const));
				const := c
			всё;
			size := constsize;
			увел(constsize,устарПреобразуйКБолееУзкомуЦел(tablesize*4));
			увел(casetablesize,устарПреобразуйКБолееУзкомуЦел(tablesize));
			возврат size;
		кон AddCasetable;

		(** NewConst - Create a new constant *)

		проц NewConst*(перем a: массив из НИЗКОУР.октет;  len: цел32): цел32;
		перем  base: цел32; c: ConstArray;
		нач {единолично}
			утв(len <= длинаМассива(a));
			base := constsize;
			утв(base+len < MaxConstSize);
			если base+len >= длинаМассива(const^) то
				нов(c, длинаМассива(const) + 256);
				НИЗКОУР.копируйПамять(адресОт(const[0]), адресОт(c[0]), длинаМассива(const));
				const := c
			всё;

			если PCM.bigEndian то
				если len = 8 то (* const is 64 bits (e.g. SIGNED64, FLOAT64)-> swap low and high 4-byte words separately *)
					PCM.SwapBytes(a, 0, 4); PCM.SwapBytes(a, 4, 4);
				иначе
					PCM.SwapBytes(a, 0, len);
				всё;
			всё;

			НИЗКОУР.копируйПамять(адресОт(a[0]), адресОт(const[base]), len);
			увел(constsize, устарПреобразуйКБолееУзкомуЦел(len + (-len) остОтДеленияНа 4));
			возврат base
		кон NewConst;

		(** NewStringConst - Create a new string constant; used for big endian: do not change endianness
				of string constants *)

		проц NewStringConst*(перем a: массив из НИЗКОУР.октет;  len: цел32): цел32;
		перем  base: цел32; c: ConstArray;
		нач {единолично}
			утв(len <= длинаМассива(a));
			base := constsize;
			утв(base+len < MaxConstSize);
			если base+len >= длинаМассива(const^) то
				нов(c, длинаМассива(const) + 256);
				НИЗКОУР.копируйПамять(адресОт(const[0]), адресОт(c[0]), длинаМассива(const));
				const := c
			всё;

			НИЗКОУР.копируйПамять(адресОт(a[0]), адресОт(const[base]), len);

			увел(constsize, устарПреобразуйКБолееУзкомуЦел(len + (-len) остОтДеленияНа 4));
			возврат base
		кон NewStringConst;


	(** fof >> *)
	(** NewArrayConst - Create a new array constant;  *)
		проц NewArrayConst*( перем a: массив из симв8;  перем len: массив из цел32;  blen: цел32 ): цел32;
		перем base: цел32;  c: ConstArray;  tlen, tdim: цел32;  clen: размерМЗ; alen, tbase: цел32;  dim: размерМЗ;
		нач {единолично}
			base := constsize;  dim := длинаМассива( len );  tlen := blen;  tdim := 0;
			нцПока (tdim < dim) делай tlen := tlen * len[tdim];  увел( tdim );  кц;
			утв ( tlen <= длинаМассива( a ) );
			alen := tlen;
			утв ( base + alen < MaxConstSize );
			clen := длинаМассива( const^ );
			если base + alen >= clen то
				нов( c, clen + 256 + 256 * (base + alen - clen) DIV 256 );
				НИЗКОУР.копируйПамять( адресОт( const[0] ), адресОт( c[0] ), clen );  const := c;
			всё;
			tbase := base;
			НИЗКОУР.копируйПамять( адресОт( a[0] ), адресОт( const[tbase] ), tlen );   (* data copy *)
			увел( constsize, устарПреобразуйКБолееУзкомуЦел( alen + (-alen) остОтДеленияНа 4 ) );
			возврат base;
		кон NewArrayConst;
		(** << fof  *)

		(** UseVar - insert a fixup, add to ExtVars or OwnVars *)

		проц UseVariable*(v: GlobalVariable; offset: цел32);
		нач
			v.AddFixup(offset);
			если v.next = НУЛЬ то
				если v.owner = сам то
					v.next := OwnVars; OwnVars := v
				иначе
					v.next := ExtVars; ExtVars := v
				всё
			всё
		кон UseVariable;

		(** AddOwnProc - Insert local procedure into OwnProcs list *)

		проц AddOwnProc*(p: Procedure; codeOffset: цел32);
		нач (*{EXCLUSIVE}*)
			утв(p.owner = context, 500);	(* must be local *)
			утв((p.next = НУЛЬ), 501);			(* not inserted yet *)
			p.next := OwnProcs; OwnProcs := p;
			p.codeoffset := codeOffset
		кон AddOwnProc;

		(** UseProcedure - insert a fixup entry, add to ExtProcs list (if external) *)

		проц UseProcedure*(p: Procedure; offset: цел32);
		нач
			p.AddFixup(offset);
			если (p.owner # сам) и (p.next = НУЛЬ) то
				нач {единолично}
					p.next := ExtProcs; ExtProcs := p
				кон
			всё
		кон UseProcedure;

		(** UseSyscall - Add a syscall fixup *)

		проц UseSyscall*(syscall, offset: цел32);
			перем l: Fixup;
		нач
			нов(l); l.offset := offset;
			нач {единолично}
				l.next := syscalls[syscall]; syscalls[syscall] := l
			кон;
		кон UseSyscall;
	кон Module;

	ObjFGeneratorProc* = проц (перем R: PCM.Rider; scope: PCT.ModScope;  перем codeSize: цел32);

перем
	NofSysCalls-: цел32;
	sentinel-: GlobalVariable;	(*last element of the VarEntries list *)
	psentinel-: Procedure;	(** last element of the OwnProcs list *)
	context*: Module;
	generate*: ObjFGeneratorProc;

(** --------------- Miscellaneous ------------------ *)

проц SetNumberOfSyscalls*(nofsyscalls: цел32);
нач
	утв(nofsyscalls >= DefaultNofSysCalls, 100);
	NofSysCalls := nofsyscalls
кон SetNumberOfSyscalls;

(** ------------ Resource Allocation ----------------- *)

проц AllocateTD*(size: RecSize);	(** td => record.td *)
перем  zero: цел64; ga: GlobalVariable; (* lk *)
нач {единолично}
	если size.td = НУЛЬ то
		zero := 0;
		нов(ga, context);
		ga.offset := context.NewConst(zero, PCT.AddressSize); (* lk *)
		size.td := ga;
	всё
кон AllocateTD;

нач
	init := истина;
	sentinel := НУЛЬ;
	нов(sentinel, НУЛЬ);
	нов(psentinel, НУЛЬ , ложь);
	NofSysCalls := DefaultNofSysCalls;
	init := ложь;
кон PCBT.

(*
	15.11.06	ug	FixupSentinel extended to 32 bits, MaxConstSize adapted, additional information in type Procedure for GC
	18.03.02	prk	PCBT code cleanup and redesign
	11.08.01	prk	Fixup and use lists for procedures in PCBT cleaned up
	10.08.01	prk	PCBT.Procedure: imported: BOOLEAN replaced by owner: Module
	10.08.01	prk	PCBT.Module.imported removed
	09.08.01	prk	Symbol Table Loader Plugin
	06.08.01	prk	make code generator and object file generator indipendent
	29.05.01    be	syscall structures moved to backend (PCLIR & code generators)
	07.05.01	prk	Installable code generators moved to PCLIR; debug function added
	03.05.01	be	Installable code generators
	26.04.01	prk	separation of RECORD and OBJECT in the parser
*)

(**

PCBT use:

1. Procedure Entry Points
	When a procedure implemented in the compilation unit is emitted, it must register itself as an entry point using
		PCBT.context.AddOwnProc(procaddr, codeoffset)
	procaddr is added to the entries list, procaddr.codeoffset is set.



Invariants:
	mod.entries:
		- all entries have owner = mod
		- list terminated by PCBT.psentinel

	mod.ExtProcs:
		- all procs have owner # mod
		- list terminated by PCBT.psentinel
*)
