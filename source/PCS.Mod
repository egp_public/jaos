(* Paco, Copyright 2000 - 2002, Patrik Reali, ETH Zurich *)

модуль PCS; (** AUTHOR "prk"; PURPOSE "Parallel Compiler: scanner"; *)

	использует
		Потоки, Texts, UTF8Strings, StringPool, PCM;

	конст
		Trace = ложь;

		MaxStrLen* = 256;
		MaxIdLen = 32;

	тип
		Name* = StringPool.Index;
		String* = массив MaxStrLen из симв8;

		Buffer = укль на массив из симв8;

		Token* = цел8;

	конст
		Eot* = 0X;
		ObjectMarker = 020X;

		(* numtyp values *)
		char* = 1; integer* = 2; longinteger* = 3; real* = 4; longreal* = 5;
(*	Oberon-1
	ProgTools.Enum 0 *
		null
		times slash div mod and
		plus minus or eql neq  lss leq gtr geq in is
		arrow period comma
		colon upto rparen rbrak rbrace
		of then do to by
		lparen lbrak lbrace
		not
		becomes
		number nil true false string
		ident semicolon bar end else
		elsif until if case while
		repeat for loop with exit passivate return
		refines implements
		array definition object record pointer begin code
		const type var procedure import
		module eof
		~

	OberonX
	ProgTools.Enum 0 *
		null

		times times0 times1 times2 times3 times4 times5 times6 times7
		slash slash0 slash1 slash2 slash3 slash4 slash5 slash6 slash7
		div mod and

		plus plus0 plus1 plus2 plus3 plus4 plus5 plus6 plus7
		minus minus0 minus1 minus2 minus3 minus4 minus5 minus6 minus7
		or

		eql neq  lss leq gtr geq in is

		arrow period comma
		colon upto rparen rbrak rbrace
		of then do to by
		lparen lbrak lbrace
		not percent backslash
		becomes
		number nil self true false string
		ident semicolon bar end else
		elsif until if case while
		repeat for loop with exit passivate
		return array record pointer begin code
		const type var procedure import
		module eof
		~

*)
	null* =   0; times* =   1; slash* =   2; div* =   3; mod* =   4; and* =   5;
	plus* =   6; minus* =   7; or* =   8; eql* =   9; neq* =  10; lss* =  11;
	leq* =  12; gtr* =  13; geq* =  14; in* =  15; is* =  16; arrow* =  17;
	period* =  18; comma* =  19; colon* =  20; upto* =  21; rparen* =  22;
	rbrak* =  23; rbrace* =  24; of* =  25; then* =  26; do* =  27; to* =  28;
	by* =  29; lparen* =  30; lbrak* =  31; lbrace* =  32; not* =  33;
	becomes* =  34; number* =  35; nil* =  36; true* =  37; false* =  38;
	string* =  39; ident* =  40; semicolon* =  41; bar* =  42; end* =  43;
	else* =  44; elsif* =  45; until* =  46; if* =  47; case* =  48; while* =  49;
	repeat* =  50; for* =  51; loop* =  52; with* =  53; exit* =  54;
	passivate* =  55; return* =  56; refines* =  57; implements* =  58;
	array* =  59; definition* =  60; object* =  61; record* =  62; pointer* =  63;
	begin* =  64; code* =  65; const* =  66; type* =  67; var* =  68;
	procedure* =  69; import* =  70; module* =  71; eof* =  72; finally* = 73;
	(** fof special operators on arrays >> *)
	backslash* = 74; (* a \ b *)
	scalarproduct* = 75; (*  a +* b  *)
	elementproduct* = 76;   (* a .* b *)
	elementquotient* = 77;  (* a ./ b *)
	dtimes*=78;  (* a ** b *)
	transpose*=79;  (* A` *)
	eeql*=80;  (* a .= b *)
	eneq*=81; (* a .# b *)
	elss* = 82; (* a .< b *)
	eleq* = 83;  (* a .<= b *)
	egtr* = 84; (* a .> b *)
	egeq* = 85;  (* a .>= b *)
	qmark*=86;   (* ? *)
	(** << fof  *)

перем
	opTable: массив 86 из Name; (* opTable: ARRAY 73 OF Name;*)
	reservedChar-, newChar: массив 256 из булево;

тип
	Scanner* = окласс
		перем
			buffer: Buffer;
			pos: цел32;	(*pos in buffer*)
			ch-: симв8;	(**look-ahead *)
			name-: Name;
			str*: String;
			numtyp-: цел16; (* 1 = char, 2 = integer, 3 = real, 4 = longreal *)
			intval-: цел32;	(* integer value or string length *)
			longintval-: цел64;
			realval-: вещ32;
			lrlval-: вещ64;
			numStartPos, numEndPos: цел32;
			curpos-, errpos-: цел32;	(*pos in text*)
			isNummer: булево;

			(* fof 070731 *)
			lcase-,ucase-: булево;  (* lcase=true: recognize lowercase keywords , ucase=true: recognize uppercase keywords*)
			firstId: булево;  n1: симв8;

		проц err(n: цел16);
		нач PCM.Error(n, errpos, "")
		кон err;

		проц NextChar*;
		нач
			(*REPEAT*)
				если pos < длинаМассива(buffer) то
					ch := buffer[pos]; увел(pos)
				иначе
					ch := Eot
				всё;
			(*UNTIL (ch # ObjectMarker);*)

			если newChar[кодСимв8(ch)] то увел(curpos) всё; (* curpos := pos; *)
		кон NextChar;

		проц SkipUntilNextEnd*(перем sym: цел8);
		нач
			нцПока (sym # eof) и (sym # end) делай
				если ch = Eot то sym := eof
				аесли ch <= ' ' то NextChar;
				иначе Identifier (sym, ложь);
					если ucase и (str = "END") или ~ucase и (str = "end") то sym := end всё;
				всё;
			кц;
		кон SkipUntilNextEnd;

		проц Str(перем sym: цел8);
			перем i: цел16; och: симв8;
		нач i := 0; och := ch;
			нц NextChar;
				если ch = och то прервиЦикл всё ;
				если ch < " " то err(3); прервиЦикл всё ;
				если i = MaxStrLen-1 то err(241); прервиЦикл всё ;
				str[i] := ch; увел(i)
			кц ;
			NextChar; str[i] := 0X; intval := i + 1;
			если intval = 2 то
				sym := number; numtyp := 1; intval := кодСимв8(str[0])
			иначе sym := string
			всё
		кон Str;

		проц Identifier(перем sym: цел8; check: булево);
			перем i: цел32;
		нач i := 0;
			нцДо
				str[i] := ch; увел(i); NextChar
(*			UNTIL ((ch < "0") OR ("9" < ch)) & (CAP(ch) < "A") OR ("Z" < CAP(ch)) & (ch # '_') OR (i = MaxIdLen); *)
			кцПри reservedChar[кодСимв8(ch)] или (i = MaxIdLen);
			если i = MaxIdLen то если check то err(240) всё; умень(i) всё ;
			str[i] := 0X; sym := ident;
		кон Identifier;

		проц Number;
		перем i, m, n, d, e: цел16; dig: массив 24 из симв8; f: вещ64; expCh: симв8; neg: булево; longintval: цел64;

			проц Ten(e: цел16): вещ64;
				перем x, p: вещ64;
			нач x := 1; p := 10;
				нцПока e > 0 делай
					если нечётноеЛи¿(e) то x := x*p всё;
					e := e DIV 2;
					если e > 0 то p := p*p всё (* prevent overflow *)
				кц;
				возврат x
			кон Ten;

			проц Ord(ch: симв8; hex: булево): цел16;
			нач (* ("0" <= ch) & (ch <= "9") OR ("A" <= ch) & (ch <= "F") *)
				если ch <= "9" то возврат кодСимв8(ch) - кодСимв8("0")
				аесли hex то возврат кодСимв8(ch) - кодСимв8("A") + 10
				иначе err(2); возврат 0
				всё
			кон Ord;

		нач (* ("0" <= ch) & (ch <= "9") *)
			i := 0; m := 0; n := 0; d := 0;
			нц (* read mantissa *)
				если ("0" <= ch) и (ch <= "9") или (d = 0) и ("A" <= ch) и (ch <= "F") то
					если (m > 0) или (ch # "0") то (* ignore leading zeros *)
						если n < длинаМассива(dig) то dig[n] := ch; увел(n) всё;
						увел(m)
					всё;
					NextChar; увел(i)
				аесли ch = "." то NextChar;
					если ch = "." то (* ellipsis *) ch := 7FX; прервиЦикл
					аесли d = 0 то (* i > 0 *) d := i
					иначе err(2)
					всё
				иначе прервиЦикл
				всё
			кц; (* 0 <= n <= m <= i, 0 <= d <= i *)
			если d = 0 то (* integer *)
				если n = m то intval := 0; i := 0; longintval := 0;
					если ch = "X" то (* character *) NextChar; numtyp := char;
						если PCM.LocalUnicodeSupport и (n <= 8) то
							если (n = 8) и (dig[0] > "7") то (* prevent overflow *) intval := -1 всё;
							нцПока i < n делай intval := intval*10H + Ord(dig[i], истина); увел(i) кц
						аесли ~PCM.LocalUnicodeSupport и (n <= 2) то
							нцПока i < n делай intval := intval*10H + Ord(dig[i], истина); увел(i) кц
						иначе err(203)
						всё
					аесли ch = "H" то (* hexadecimal *) NextChar; numtyp := longinteger;
						если n > PCM.MaxHHDig то err (203) всё;
						нцПока i < n делай d := Ord(dig[i], истина); увел(i);
							longintval := longintval * 10H + d
						кц;
						intval := устарПреобразуйКБолееУзкомуЦел (longintval);
						если intval = longintval то numtyp := integer всё;
					иначе (* decimal *) numtyp := longinteger;
						нцПока i < n делай d := Ord(dig[i], ложь); увел(i);
							longintval := longintval * 10 + d;
							если longintval < 0 то err(203) всё;
						кц;
						intval := устарПреобразуйКБолееУзкомуЦел (longintval);
						если intval = longintval то numtyp := integer всё;
					всё
				иначе err(203)
				всё
			иначе (* fraction *)
				f := 0; e := 0; expCh := "E";
				нцПока n > 0 делай (* 0 <= f < 1 *) умень(n); f := (Ord(dig[n], ложь) + f)/10 кц;
				если (ch = "E") или (ch = "D") то expCh := ch; NextChar; neg := ложь;
					если ch = "-" то neg := истина; NextChar
					аесли ch = "+" то NextChar
					всё;
					если ("0" <= ch) и (ch <= "9") то
						нцДо n := Ord(ch, ложь); NextChar;
							если e <= (матМаксимум(цел16) - n) DIV 10 то e := e*10 + n
							иначе err(203)
							всё
						кцПри (ch < "0") или ("9" < ch);
						если neg то e := -e всё
					иначе err(2)
					всё
				всё;
				умень(e, i-d-m); (* decimal point shift *)
				если expCh = "E" то numtyp := real;
					если (1-PCM.MaxRExp < e) и (e <= PCM.MaxRExp) то
						если e < 0 то realval := устарПреобразуйКБолееУзкомуЦел(f / Ten(-e))
						иначе realval := устарПреобразуйКБолееУзкомуЦел(f * Ten(e))
						всё
					иначе err(203)
					всё
				иначе numtyp := longreal;
					если (1-PCM.MaxLExp < e) и (e <= PCM.MaxLExp) то
						если e < 0 то lrlval := f / Ten(-e)
						иначе lrlval := f * Ten(e)
						всё
					иначе err(203)
					всё
				всё
			всё;
			сам.longintval := longintval;
		кон Number;

		проц GetNumAsString*(перем val: массив из симв8);
		перем i, l: размерМЗ;
		нач
			(*Strings.Copy(buffer^, numStartPos, numEndPos-numStartPos, val);*)
			если isNummer то
				i := 0; l := длинаМассива(val)-1;
				нцПока (i < numEndPos-numStartPos) и (i < l) делай
					val[i] := buffer[numStartPos + i];
					увел(i);
				кц;
			всё;
			val[i] := 0X
		кон GetNumAsString;

		проц Get*(перем s: цел8);

			проц Comment;	(* do not read after end of file *)
				перем dump: булево;
			нач NextChar;
				если ch = "#" то dump := истина; PCM.LogWLn всё;	(* implementation-specific feature *)
				нц
					нц
						нцПока ch = "(" делай NextChar;
							если ch = "*" то Comment аесли dump то PCM.LogW ("(") всё
						кц ;
						если ch = "*" то NextChar; прервиЦикл всё ;
						если ch = Eot то прервиЦикл всё ;
						если dump то PCM.LogW (ch) всё;
						NextChar
					кц ;
					если ch = ")" то NextChar; прервиЦикл всё ;
					если dump то PCM.LogW ("*") всё;
					если ch = Eot то err(5); прервиЦикл всё
				кц
			кон Comment;

		нач
			нцДо
				нцПока ch <= " " делай (*ignore control characters*)
					если ch = Eot то
						если Trace то
							PCM.LogWLn; PCM.LogWStr("Scan ");
							PCM.LogWNum((*curpos*)pos); 			(*reader version*)
							PCM.LogWHex(eof)
						всё;
						s := eof; возврат
					иначе NextChar
					всё
				кц ;
				(* errpos := (*curpos*)pos-1;			(*reader version*) *)
				errpos := curpos - 1;
				isNummer := ложь;
				просей ch из   (* ch > " " *)
					| 22X, 27X  : Str(s)
					| "#"  : s := neq; NextChar
					| "&"  : s :=  and; NextChar
					| "("  : NextChar;
									 если ch = "*" то Comment; (*GlobalGet; RETURN*) s := -1;		(*allow recursion without reentrancy*)
										 иначе s := lparen
									 всё
					| ")"  : s := rparen; NextChar
					| "*"  : NextChar; если ch = "*" то NextChar;  s := dtimes;  иначе s := times всё; (* fof *)
					| "+"  : NextChar;  если ch = "*" то NextChar;  s := scalarproduct;  иначе s := plus всё;  (* fof *)
					| ","  : s := comma; NextChar
					| "-"  : s :=  minus; NextChar
					| "."  : NextChar;
						если ch = "." то NextChar; s := upto
						(** fof >> *)
						аесли ch = "*" то
							NextChar;  s := elementproduct (*fof*)
						аесли ch = "/" то
							NextChar;  s := elementquotient
						аесли ch="=" то
							NextChar; s := eeql
						аесли ch="#" то
							NextChar; s := eneq
						аесли ch=">" то
							NextChar;
							если ch="=" то
							s := egeq; NextChar;
							иначе
							s := egtr
							всё;
						аесли ch="<" то
							NextChar;
							если ch="=" то
							s := eleq; NextChar;
							иначе
							s := elss
							всё;
							(** << fof  *)
						иначе s := period всё
					| "/"  : s :=  slash; NextChar
					(** fof >> *)
					| "\":     s := backslash;  NextChar
					| "`":	s := transpose; NextChar;
					| "?": s := qmark; NextChar;
					(** << fof  *)
					| "0".."9": isNummer := истина; numStartPos := pos-1; Number; numEndPos := pos-1; s := number
					| ":"  : NextChar;
									 если ch = "=" то NextChar; s := becomes иначе s := colon всё
					| ";"  : s := semicolon; NextChar
					| "<"  : NextChar;
									 если ch = "=" то NextChar; s := leq; иначе s := lss; всё
					| "="  : s :=  eql; NextChar
					| ">"  : NextChar;
									 если ch = "=" то NextChar; s := geq; иначе s := gtr; всё
						(* fof 070731 *)
					| "A".."Z":
					Identifier(s, истина);  n1 := str[0];
					если ucase то
					n1 := str[0];
						просей n1 из
						| "A":
								если str = "ARRAY" то s := array
								аесли str = "AWAIT" то s := passivate
								всё
						| "B":
								если str = "BEGIN" то s := begin
								аесли str = "BY" то s := by
								всё
						| "C":
								если str = "CONST" то s := const
								аесли str = "CASE" то s := case
								аесли str = "CODE" то s := code
								всё
						| "D":
								если str = "DO" то s := do
								аесли str = "DIV" то s := div
								аесли str = "DEFINITION" то s := definition
								всё
						| "E":
								если str = "END" то s := end
								аесли str = "ELSE" то s := else
								аесли str = "ELSIF" то s := elsif
								аесли str = "EXIT" то s := exit
								всё
						| "F":
								если str = "FALSE" то s := false
								аесли str = "FOR" то s := for
								аесли str = "FINALLY" то s := finally
								всё
						| "I":
								если str = "IF" то s := if
								аесли str = "IN" то s := in
								аесли str = "IS" то s := is
								аесли str = "IMPORT" то s := import
								аесли str = "IMPLEMENTS" то s := implements
								всё
						| "L":
								если str = "LOOP" то s := loop всё
						| "M":
								если str = "MOD" то s := mod
								аесли str = "MODULE" то s := module;  lcase := ложь; (* fof *)
								всё
						| "N":
								если str = "NIL" то s := nil
								всё
						| "O":
								если str = "OR" то s := or
								аесли str = "OF" то s := of
								аесли str = "OBJECT" то s := object
								всё
						| "P":
								если str = "PROCEDURE" то s := procedure
								аесли str = "POINTER" то s := pointer
								всё
						| "R":
								если str = "RECORD" то s := record
								аесли str = "REPEAT" то s := repeat
								аесли str = "RETURN" то s := return
								аесли str = "REFINES" то s := refines
								всё
						| "T":
								если str = "THEN" то s := then
								аесли str = "TRUE" то s := true
								аесли str = "TO" то s := to
								аесли str = "TYPE" то s := type
								всё
						| "U":
								если str = "UNTIL" то s := until всё
						| "V":
								если str = "VAR" то s := var всё
						| "W":
								если str = "WHILE" то s := while
								аесли str = "WITH" то s := with
								всё
					(* fof 070731 *)
						иначе
						всё;
					всё;
					| "a".."z":
				Identifier(s, истина);
				если lcase то
					n1 := str[0];
					просей n1 из
									| "a":   если str = "array" то s := array
											аесли str = "await" то s := passivate
											всё
								| "b":   если str = "begin" то s := begin
											аесли str = "by" то s := by
											всё
								| "c":   если str = "const" то s := const
											аесли str = "case" то s := case
											аесли str = "code" то s := code
											всё
								| "d":   если str = "do" то s := do
											аесли str = "div" то s := div
											аесли str = "definition" то s := definition
											всё
								| "e":   если str = "end" то s := end
											аесли str = "else" то s := else
											аесли str = "elsif" то s := elsif
											аесли str = "exit" то s := exit
											всё
								| "f":    если str = "false" то s := false
											аесли str = "for" то s := for
											аесли str = "finally" то s := finally
											всё
								| "i":    если str = "if" то s := if
											аесли str = "in" то s := in
											аесли str = "is" то s := is
											аесли str = "import" то s := import
											аесли str = "implements" то s := implements
											всё
								| "l":    если str = "loop" то s := loop всё
								| "m":  если str = "mod" то s := mod
											аесли str = "module" то s := module; ucase := ложь;
											всё
								| "n":   если str = "nil" то s := nil
											всё
								| "o":   если str = "or" то s := or
											аесли str = "of" то s := of
											аесли str = "object" то s := object
											всё
								| "p":   если str = "procedure" то s := procedure
											аесли str = "pointer" то s := pointer
											всё
								| "r":    если str = "record" то s := record
											аесли str = "repeat" то s := repeat
											аесли str = "return" то s := return
											аесли str = "refines" то s := refines
											всё
								| "t":   если str = "then" то s := then
											аесли str = "true" то s := true
											аесли str = "to" то s := to
											аесли str = "type" то s := type
											всё
								| "u":   если str = "until" то s := until всё
								| "v":   если str = "var" то s := var всё
								| "w":  если str = "while" то s := while
											аесли str = "with" то s := with
											всё
								иначе
								всё;
								если firstId и (s # module) то  lcase := ложь;  s := ident  всё;
							всё
					| "["  : s := lbrak; NextChar
					| "]"  : s := rbrak; NextChar
					| "^"  : s := arrow; NextChar
					| "{"  : s := lbrace; NextChar
					| "|"  : s := bar; NextChar
					| "}"  : s := rbrace; NextChar
					| "~"  : s := not; NextChar
					| 7FX  : s := upto; NextChar
				иначе s := null; NextChar;
				всё ;
			кцПри s >= 0;
			firstId := ложь; (*fof*)

			если s = ident то StringPool.GetIndex(str, name) всё;

			если Trace то
				PCM.LogWLn; PCM.LogWStr("Scan ");
				PCM.LogWNum(errpos); PCM.LogWHex(s);
			всё;
		кон Get;

		проц IsOperatorValid*(): булево;
		перем
			ch0, ch1, ch2: симв8;
		нач
			ch0 := str[0]; ch1 := str[1]; ch2 := str[2];
			просей str[0] из
			| "=", "#", "&": возврат ch1 = 0X
			| "<", ">": возврат (ch1 = 0X) или ((ch1 = "=") и (ch2 = 0X))  	(* <, <=, >, >= *)
			| "I": возврат str= "IN"  	(* IN *)
			| "D": возврат str="DIV"  	(* DIV *)
			| "M": возврат str="MOD"  	(* MOD *)
			| "O": возврат str="OR"  	(* OR *)
			| "+":  возврат (ch1=0X) или (ch2=0X) и (ch1="*")
			| "-":  возврат (ch1=0X)
			| "*":  возврат (ch1=0X) или (ch2=0X) и (ch1="*")
			| "/" : возврат (ch1=0X)
			| "~": возврат (ch1=0X)
			| ":": возврат str=":="
			| "[": возврат str = "[]" (* Indexer *)
			| "\": возврат ch1=0X
			| "`": возврат ch1=0X
			| ".": возврат (str=".=") или (str=".#") или (str=".<") или (str=".>") или (str=".<=") или (str=".>=") или (str=".*") или (str = "./");
			иначе возврат ложь
			всё;
		кон IsOperatorValid;

	кон Scanner;

	проц GetOpName*(op: цел8; перем name: Name);
	нач
		name := opTable[op];
	кон GetOpName;

	(** Create a new scanner at the same position *)
	проц ForkScanner* (s: Scanner): Scanner;
	перем t: Scanner;
	нач
		нов(t);
		t^ := s^;
		возврат t
	кон ForkScanner;
(*
	PROCEDURE SaveBuffer(b: Buffer);
		VAR f: Files.File; r: Files.Rider;
	BEGIN
		f := Files.New("SillyFile.bin");
		f.Set(r, 0);
		f.WriteBytes(r, b^, 0, LEN(b^));
		Files.Register(f);
	END SaveBuffer;
*)
	проц NewScanner(b: Buffer;  pos, curpos: цел32): Scanner;
	перем s: Scanner;
	нач
(*
		SaveBuffer(b);
*)
		нов(s);
		s.buffer := b;
		s.pos := pos;
		s.curpos := curpos;
		s.ch := " ";
		s.lcase := истина;  s.ucase := истина; s.firstId := истина; (* fof 070731 *)
		возврат s
	кон NewScanner;

	проц InitWithText*(t: Texts.Text; pos: цел32): Scanner;
		перем buffer: Buffer; len, i, j: размерМЗ; ch: симв32; r: Texts.TextReader;
		bytesPerChar: цел32;
	нач
		t.AcquireRead;
		len := t.GetLength();
		bytesPerChar := 2;
		нов(buffer, len * bytesPerChar);	(* UTF8 encoded characters use up to 5 bytes *)
		нов(r, t);
		r.SetPosition(pos);
		j := 0;
		нцДля i := 0 до len-1 делай
			r.ReadCh(ch);
			нцПока ~UTF8Strings.EncodeChar(ch, buffer^, j) делай
					(* buffer too small *)
				увел(bytesPerChar);
				ExpandBuf(buffer, bytesPerChar * len);
			кц;
		кц;
		t.ReleaseRead;
		возврат NewScanner(buffer, pos, 0);
	кон InitWithText;

	проц ExpandBuf(перем oldBuf: Buffer; newSize: размерМЗ);
	перем newBuf: Buffer; i: размерМЗ;
	нач
		если длинаМассива(oldBuf^) >= newSize то возврат всё;
		нов(newBuf, newSize);
		нцДля i := 0 до длинаМассива(oldBuf^)-1 делай
			newBuf[i] := oldBuf[i];
		кц;
		oldBuf := newBuf;
	кон ExpandBuf;

	проц InitWithReader*(r: Потоки.Чтец; size: размерМЗ; pos: Потоки.ТипМестоВПотоке): Scanner;
		перем buffer: Buffer; read: размерМЗ;
	нач
		нов(buffer, size);
		r.чБайты(buffer^, 0, size, read);
		возврат NewScanner(buffer, 0, pos(цел32))
	кон InitWithReader;

(*
	PROCEDURE InitReservedCharsOld;
	VAR i: SIGNED32;
	BEGIN
		FOR i := 0 TO LEN(reservedChar)-1 DO
			IF (CHR(i) < "0") OR ("9" < CHR(i)) & (CHR(i) < "A") OR ("Z"< CHR(i)) & (CHR(i) < "a") OR ("z" < CHR(i)) THEN
				reservedChar[i] := TRUE;
			ELSE
				reservedChar[i] := FALSE;
			END;
		END;
	END InitReservedCharsOld;
*)

	проц InitReservedChars;
	перем
		i: цел32;
	нач
		нцДля i := 0 до длинаМассива(reservedChar)-1 делай
			reservedChar[i] := ((симв8ИзКода(i) < '0') или ('9' < симв8ИзКода(i))) и (ASCII_вЗаглавную(симв8ИзКода(i)) < "A") или ("Z" < ASCII_вЗаглавную(симв8ИзКода(i))) и (симв8ИзКода(i) # '_')
		кц;
	кон InitReservedChars;

	проц InitNewChar;
	перем
		i: цел32;
	нач
		нцДля i := 0 до длинаМассива(newChar)-1 делай
			(* UTF-8 encoded characters with bits 10XXXXXX do not start a new unicode character *)
			если (i < 80H) или (i > 0BFH) то
				newChar[i] := истина;
			иначе
				newChar[i] := ложь;
			всё
		кц
	кон InitNewChar;

	проц CreateOperatorTable;
	нач
		opTable[becomes] := StringPool.GetIndex1(":=");
		opTable[times] := StringPool.GetIndex1("*");
		opTable[slash] := StringPool.GetIndex1("/");
		opTable[div] := StringPool.GetIndex1("DIV");
		opTable[mod] := StringPool.GetIndex1("MOD");
		opTable[and] := StringPool.GetIndex1("&");
		opTable[plus] := StringPool.GetIndex1("+");
		opTable[minus] := StringPool.GetIndex1("-");
		opTable[or] := StringPool.GetIndex1("OR");
		opTable[eql] := StringPool.GetIndex1("=");
		opTable[neq] := StringPool.GetIndex1("#");
		opTable[lss] := StringPool.GetIndex1("<");
		opTable[leq] := StringPool.GetIndex1("<=");
		opTable[gtr] := StringPool.GetIndex1(">");
		opTable[geq] := StringPool.GetIndex1(">=");
		opTable[in] := StringPool.GetIndex1("IN");
		opTable[not] := StringPool.GetIndex1("~");
		(** fof >> *)
		opTable[backslash] := StringPool.GetIndex1( "\" );
		opTable[scalarproduct] := StringPool.GetIndex1( "+*" );
		opTable[elementproduct] := StringPool.GetIndex1( ".*" );
		opTable[elementquotient] := StringPool.GetIndex1( "./" );
		opTable[dtimes] := StringPool.GetIndex1( "**");
		opTable[eeql] := StringPool.GetIndex1( ".=");
		opTable[eneq] := StringPool.GetIndex1( ".#");
		opTable[elss] := StringPool.GetIndex1( ".<");
		opTable[eleq] := StringPool.GetIndex1( ".<=");
		opTable[egtr] := StringPool.GetIndex1( ".>");
		opTable[egeq] := StringPool.GetIndex1( ".>=");
		(** << fof  *)
	кон CreateOperatorTable;

нач
	если Trace то PCM.LogWLn; PCM.LogWStr("PCS.Trace on") всё;
	CreateOperatorTable;
	InitReservedChars;
	InitNewChar;
кон PCS.
(*
	28.12.02	prk	InitWithReader, remove VAR (reader is passed as reference anyway)
	05.02.02	prk	PCS takes Streams.Reader as parameter, let PC handle the Oberon Text format
	18.01.02	prk	AosFS used instead of Files
	27.06.01	prk	StringPool cleaned up
	21.06.01	prk	using stringpool index instead of array of char
	12.06.01	prk	Interfaces
	26.04.01	prk	separation of RECORD and OBJECT in the parser
*)
