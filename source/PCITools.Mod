модуль PCITools; (** AUTHOR "pjm/staubesv"; PURPOSE "PCI Bus Tools"; *)
(**
 * This module provide two kinds of services related to PCI:
 *	- Display information about all PCI busses/devices installed in the system
 *	- Map PCI function to PCI device drivers
 *
 * Usage:
 *
 *	PCITools.Scan ~				displays information about all installed PCI busses/devices
 *	PCITools.Scan details ~		displays excessively detailed information
 *	PCITools.DetectHardware ~	performs a PCI function to device driver mapping for all devices found
 *
 *	System.Free PCITools ~
 *
 * Port of Oberon PCITools.Mod from "pjm" which is based on Linux pci.c and PCI Local Bus Specification Revision 2.0
 *
 * History:
 *
 * 	18.06.2003	Fix for non-continuously numbered device functions (tf)
 *	15.09.2005 	Ported to Bluebottle, added DetectHardware(), InstallPCIDrivers() & Extract(), removed ShowInterrupts() (staubesv)
 *	17.01.2006	Added WriteRegs & WriteCmdSts (staubesv)
 *)

использует
	НИЗКОУР, PCI, ЛогЯдра, Потоки, Files, Commands, Options, DriverDatabase;

конст
	HdrType = 0EH;

	Verbose = ложь;
	ShowDrivers = истина;				(* Also show available device/class drivers *)

	PCIIDS = "pci.ids";					(* File containing PCI vendor ID to vendor string mapping *)

тип
	Device = укль на запись
		bus: Bus;						(* bus this device is on *)
		sibling: Device;					(* next device on this bus *)
		next, prev: Device;				(* chain of all devices *)
		devfn: цел32;				(* dev = top 5 bits, fn = lower 3 bits *)
		device: цел32;				(* device id *)
		vendor: цел32;				(* vendor id *)
		class: цел32;					(* base, sub, prog-if bytes *)
		revision : цел32;				(* device revision *)
		irq, pin: цел32
	кон;

	Bus = укль на запись
		parent : Bus;					(* parent bus this bridge is on *)
		children : Bus;					(* chain of P2P bridges on this bus *)
		next : Bus;						(* chain of all PCI buses *)
		self: Device;						(* bridge device as seen by parent *)
		devices: Device;					(* devices behind this bridge *)
		number: цел32;				(* bus number *)
		primary, secondary: цел32;	(* bridge numbers *)
		subordinate: цел32			(* max number of subordinate buses *)
	кон;

проц ScanBus(перем bus: Bus; перем devices : Device): цел32;
перем devfn, max, x, hdrtype, ht, buses: цел32;  ismulti: булево;  dev: Device;  child: Bus;
нач (* Only call from within EXCLUSIVE regions *)
	max := bus.secondary;  ismulti := ложь;
	нцДля devfn := 0 до 0FEH делай
		если (devfn остОтДеленияНа 8 = 0) или ismulti то
			ReadConfigByte(bus.number, devfn, HdrType, hdrtype);
			если devfn остОтДеленияНа 8 = 0 то ismulti := нечётноеЛи¿(hdrtype DIV 80H) всё;
			ReadConfigDword(bus.number, devfn, PCI.DevReg, x);
			если (x # -1) и (x # 0) то (* some boards return 0 instead of -1 for empty slot, according to Linux *)
				нов(dev);
				dev.bus := bus;  dev.devfn := devfn;
				dev.vendor := x остОтДеленияНа 10000H;
				dev.device := арифмСдвиг(x, -16) остОтДеленияНа 10000H;
				ReadConfigByte(bus.number, devfn, PCI.IntlReg, dev.irq);
				ReadConfigByte(bus.number, devfn, PCI.IntlReg+1, dev.pin);
				ReadConfigDword(bus.number, devfn, PCI.RevIdReg, x);
				dev.class := арифмСдвиг(x, -8) остОтДеленияНа 1000000H;	(* upper 3 bytes *)
				dev.revision := x остОтДеленияНа 100H; (* lowest byte *)
				просей арифмСдвиг(dev.class, -8) из
					604H: ht := 1	(* bridge pci *)
					|607H: ht := 2	(* bridge cardbus *)
					иначе ht := 0
				всё;
				если ht = hdrtype остОтДеленияНа 80H то
					dev.next := devices;  devices := dev;  dev.prev := НУЛЬ;
					dev.sibling := bus.devices;  bus.devices := dev;
					если арифмСдвиг(dev.class, -8) = 604H то (* bridge pci *)
						нов(child);
						child.next := bus.children;  bus.children := child;
						child.self := dev;  child.parent := bus;
						увел(max);  child.secondary := max;  child.number := max;
						child.primary := bus.secondary;  child.subordinate := 0FFH;

						ReadConfigDword(bus.number, devfn, 18H, buses);
						если buses остОтДеленияНа 1000000 # 0 то
							child.primary := buses остОтДеленияНа 100H;
							child.secondary := арифмСдвиг(buses, -8) остОтДеленияНа 100H;
							child.subordinate := арифмСдвиг(buses, -16) остОтДеленияНа 100H;
							child.number := child.secondary;
							max := ScanBus(child, devices)
						иначе (* configure bus numbers for this bridge *)
							ЛогЯдра.пСтроку8("PCI: Warning: Bus numbers not configured."); ЛогЯдра.пВК_ПС;
						всё
					всё
				иначе
					ЛогЯдра.пСтроку8("PCI: Warning: Unknown header type (Bus: "); ЛогЯдра.пЦел64(bus.number, 0);
					ЛогЯдра.пСтроку8(", device: "); ЛогЯдра.пЦел64(dev.devfn DIV 8, 0);
					ЛогЯдра.пСтроку8(", function: "); ЛогЯдра.пЦел64(dev.devfn остОтДеленияНа 8, 0);
					ЛогЯдра.пСтроку8(", Header Type: "); ЛогЯдра.пЦел64(hdrtype, 0); ЛогЯдра.пВК_ПС;
				всё;
		(*	ELSE
				ismulti := FALSE *) (* not all functions are continuously numbered *)
			всё
		всё
	кц;
	возврат max
кон ScanBus;

проц Extract(classcode : цел32; перем class, subclass, protocol : цел32);
нач
	class := логСдвиг(НИЗКОУР.подмениТипЗначения(цел32, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, classcode) * {16..23}), -16);
	subclass := логСдвиг(НИЗКОУР.подмениТипЗначения(цел32, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, classcode) * {8..15}), -8);
	protocol := НИЗКОУР.подмениТипЗначения(цел32, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, classcode) * {0..7});
кон Extract;

проц InstallPCIDrivers;
перем root : Bus; device : Device; nbrOfDevices : цел32; class, subclass, protocol : цел32;
нач {единолично}
	ЛогЯдра.пСтроку8("Looking for PCI devices..."); ЛогЯдра.пВК_ПС;
	нов(root);
	nbrOfDevices := ScanBus(root, device);
	нцПока(device # НУЛЬ) делай
		если Verbose то
			ЛогЯдра.пСтроку8("Bus: "); если device.bus = НУЛЬ то ЛогЯдра.пСтроку8("n/a"); иначе ЛогЯдра.пЦел64(device.bus.number, 2); всё;
			ЛогЯдра.пСтроку8(", Device: "); ЛогЯдра.пЦел64(арифмСдвиг(device.devfn, -3) остОтДеленияНа 20H, 2); ЛогЯдра.пСтроку8(", Function: "); ЛогЯдра.пЦел64(device.devfn остОтДеленияНа 8, 2);
			ЛогЯдра.пСтроку8(": "); ЛогЯдра.пСтроку8("VendorID: "); ЛогЯдра.п16ричное(device.vendor, -4); ЛогЯдра.пСтроку8(", DeviceID: "); ЛогЯдра.п16ричное(device.device, -4);
			ЛогЯдра.пВК_ПС;
		всё;
		если DriverDatabase.InstallDeviceDriver(DriverDatabase.PCI, device.vendor, device.device, device.revision) то
		иначе
			Extract(device.class, class, subclass, protocol);
			если DriverDatabase.InstallClassDriver(DriverDatabase.PCI, class, subclass, protocol, device.revision) то
			всё;
		всё;
		device := device.next;
	кц;
кон InstallPCIDrivers;

проц HexDigit(ch: симв8): булево;
нач
	возврат (ch >= "0") и (ch <= "9") или (ASCII_вЗаглавную(ch) >= "A") и (ASCII_вЗаглавную(ch) <= "F")
кон HexDigit;

проц Read(перем r: Files.Reader; перем ch: симв8);
нач
	если ch = Потоки.символКП то ch := 0X иначе r.чСимв8(ch) всё
кон Read;

проц WriteDevice(w: Потоки.Писарь; class: булево; p1, p2, p3: цел32; конст l1, l2, l3: массив из симв8; pciids : Files.File);
перем r: Files.Reader; ch: симв8; level, value: цел32;

	проц SkipLine(write: булево);
	нач
		нцПока (ch # 0X) и (ch # 0DX) и (ch # 0AX) делай
			если write то w.пСимв8(ch) всё;
			Read(r, ch)
		кц;
		нцДо Read(r, ch) кцПри (ch # 0DX) и (ch # 0AX)
	кон SkipLine;

	проц ReadHex(перем x: цел32);
	нач
		x := 0;
		нц
			если (ch >= "0") и (ch <= "9") то
				x := x * 16 + (кодСимв8(ch)-кодСимв8("0"))
			аесли (ASCII_вЗаглавную(ch) >= "A") и (ASCII_вЗаглавную(ch) <= "F") то
				x := x * 16 + (кодСимв8(ASCII_вЗаглавную(ch))-кодСимв8("A")+10)
			иначе
				прервиЦикл
			всё;
			Read(r, ch)
		кц
	кон ReadHex;

	проц GetLine(перем level, value: цел32);
	нач
		если class то
			если ch = "C" то Read(r, ch); Read(r, ch) всё
		всё;
		нцПока (ch # 0X) и (ch # 9X) и ~HexDigit(ch) делай SkipLine(ложь) кц;
		level := 0; нцПока ch = 9X делай увел(level); Read(r, ch) кц;
		ReadHex(value);
		нцПока ch = " " делай Read(r, ch) кц
	кон GetLine;

	проц Label(конст l: массив из симв8);
	нач
		w.пСтроку8(l); w.пСтроку8(": ");
	кон Label;

нач
	если pciids = НУЛЬ то
		Label(l1); w.пСтроку8("Unknown");
		Label(l2); w.пСтроку8("Unknown");
	иначе
		нов(r, pciids, 0); Read(r, ch);
		если class то нцПока (ch # 0X) и (ch # "C") делай SkipLine(ложь) кц; всё;

		нц
			GetLine(level, value);
			если (ch = 0X) или (level = 0) и (value = p1) то прервиЦикл всё;
			SkipLine(ложь)
		кц;
		Label(l1);
		если (ch # 0X) и (level = 0) и (value = p1) то
			SkipLine(истина); w.пСтроку8(", ");
			нц
				GetLine(level, value);
				если (ch = 0X) или (level = 0) или (level = 1) и (value = p2) то прервиЦикл всё;
				SkipLine(ложь)
			кц;
			Label(l2);
			если (ch # 0X) и (level = 1) и (value = p2) то
				SkipLine(истина);
				нц
					GetLine(level, value);
					если (ch = 0X) или (level < 2) или (level = 2) и (value = p3) то прервиЦикл всё;
					SkipLine(ложь)
				кц;
				если (ch # 0X) и (level = 2) и (value = p3) то
					w.пСтроку8(", "); Label(l3); SkipLine(истина)
				всё
			иначе
				w.пСтроку8("Unknown")
			всё
		иначе
			w.пСтроку8("Unknown")
		всё
	всё;
кон WriteDevice;

проц WriteB(w: Потоки.Писарь; x: цел32);
конст K = 1024; M = K*K; G = K*M;
перем mult: симв8;
нач
	если x остОтДеленияНа K # 0 то
		w.пЦел64(x, 1)
	иначе
		если x остОтДеленияНа M # 0 то mult := "K"; x := x DIV K
		аесли x остОтДеленияНа G # 0 то mult := "M"; x := x DIV M
		иначе mult := "G"; x := x DIV G
		всё;
		w.пЦел64(x, 1); w.пСимв8(mult)
	всё;
	w.пСтроку8("B")
кон WriteB;

проц WriteBase(w: Потоки.Писарь; bus, devfn, reg: цел32; перем double: булево);
перем base, basehi, type, size: цел32; mask: мнвоНаБитахМЗ;
нач
	double := ложь; basehi := 0; size := 0;
	ReadConfigDword(bus, devfn, reg, base);
	если base # 0 то
		WriteConfigDword(bus, devfn, reg, -1);
		ReadConfigDword(bus, devfn, reg, size);
		WriteConfigDword(bus, devfn, reg, base);
		если нечётноеЛи¿(base) то (* I/O *)
			если арифмСдвиг(base, -16) = 0 то mask := {2..15} иначе mask := {2..31} всё;
			type := base остОтДеленияНа 4
		иначе (* memory *)
			mask := {4..31}; type := base остОтДеленияНа 10H
		всё;
		size := НИЗКОУР.подмениТипЗначения(цел32, -(НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, size) * mask))+1;
		size := НИЗКОУР.подмениТипЗначения(цел32, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, size) * mask);
		если type остОтДеленияНа 8 = 4 то	(* 64-bit *)
			ReadConfigDword(bus, devfn, reg+4, basehi); double := истина
		всё;
		умень(base, type);
			(* write *)
		w.пСимв8(9X); w.пСимв8(9X);
		WriteB(w, size); w.пСтроку8(" ");
		просей type из
			0: w.пСтроку8("32-bit memory")
			|1: w.пСтроку8("I/O")
			|4: w.пСтроку8("64-bit memory")
			|8: w.пСтроку8("prefetchable 32-bit memory")
			|12: w.пСтроку8("prefetchable 64-bit memory")
			иначе w.пСтроку8("type "); w.пЦел64(type, 1)
		всё;
		w.пСтроку8(" at ");
		если basehi # 0 то w.п16ричное(basehi, -2) всё;
		w.п16ричное(base, -8); w.пСтроку8("-");
		если basehi # 0 то w.п16ричное(basehi, -8) всё;
		w.п16ричное(base + size - 1, -8); w.пВК_ПС;
	всё
кон WriteBase;

проц WriteDriver(w : Потоки.Писарь; dev : Device);
перем d, c : DriverDatabase.Driver; class, subclass, progintf : цел32;
нач
	w.пСтроку8("Driver: ");
	d := DriverDatabase.GetDeviceSpecific(DriverDatabase.PCI, dev.vendor, dev.device, dev.revision);
	Extract(dev.class, class, subclass, progintf);
	c := DriverDatabase.GetClassSpecific(DriverDatabase.PCI, class, subclass, progintf, dev.revision);
	если (c = НУЛЬ) и (d = НУЛЬ) то w.пСтроку8("n/a");
	аесли (d # НУЛЬ) то
		w.пСтроку8(d.commands^);
	аесли (c # НУЛЬ) то
		w.пСтроку8(c.commands^);
	всё;
кон WriteDriver;

проц WriteDev(w: Потоки.Писарь;  dev: Device; pciids : Files.File; details : булево);
перем bus, devfn, hdrtype, classrev, vendor, device, cmd, status, lastreg, reg, base: цел32; double: булево;
нач
	bus := dev.bus.number;  devfn := dev.devfn;
	ReadConfigByte(bus, devfn, HdrType, hdrtype);
	ReadConfigDword(bus, devfn, PCI.RevIdReg, classrev);
	ReadConfigWord(bus, devfn, PCI.DevReg, vendor);
	ReadConfigWord(bus, devfn, PCI.DevReg+2, device);
	ReadConfigWord(bus, devfn, PCI.CmdReg+2, status);
	ReadConfigWord(bus, devfn, PCI.CmdReg, cmd);

	w.пСтроку8("Bus ");  w.пЦел64(bus, 1);
	w.пСтроку8(", device ");  w.пЦел64(арифмСдвиг(devfn, -3) остОтДеленияНа 20H, 1);
	w.пСтроку8(", function ");  w.пЦел64(devfn остОтДеленияНа 8, 1);
	w.пСтроку8(": class/rev ");  w.п16ричное(classrev, -8);
	w.пСтроку8(", vendor/device ");  w.п16ричное(арифмСдвиг(vendor, 16) + device, -8);
	w.пСтроку8(", status/cmd ");  w.п16ричное(арифмСдвиг(status, 16) + cmd, -8);
	w.пВК_ПС;

	w.пСимв8(9X);
	WriteDevice(w, истина, арифмСдвиг(classrev, -24) остОтДеленияНа 100H, арифмСдвиг(classrev, -16) остОтДеленияНа 100H, арифмСдвиг(classrev, -8) остОтДеленияНа 100H, "Class", "Sub-class", "ProgIntfc", pciids);
	w.пВК_ПС;
	w.пСимв8(9X);
	WriteDevice(w, ложь, vendor, device, -1, "Vendor", "Device", "", pciids);
	w.пВК_ПС;
	если ShowDrivers то
		w.пСимв8(9X); WriteDriver(w, dev); w.пВК_ПС;
	всё;
	если (dev.irq # 0) или (dev.pin # 0) то
		w.пСимв8( 9X); w.пСимв8( 9X);
		w.пСтроку8("IRQ");  w.пЦел64( dev.irq, 1);
		если dev.pin # 0 то
			w.пСтроку8(", INT");  w.пСимв8( симв8ИзКода(кодСимв8("A")+dev.pin-1))
		всё;
		w.пВК_ПС;
	всё;
	просей hdrtype остОтДеленияНа 80H из
		0: lastreg := PCI.Adr5Reg
		|1: lastreg := PCI.Adr1Reg
		иначе lastreg := 0
	всё;
	нцДля reg := PCI.Adr0Reg до lastreg шаг 4 делай
		WriteBase(w, bus, devfn, reg, double);
		если double то увел(reg, 4) всё	(* modifying FOR variable *)
	кц;
	если hdrtype остОтДеленияНа 80H = 0 то
		ReadConfigDword(bus, devfn, PCI.ROMReg, base);
		если base # 0 то
			w.пСимв8(9X); w.пСимв8(9X);
			w.пСтроку8("ROM at");
			w.п16ричное(base, -8);  w.пВК_ПС;
		всё
	всё;
	если details то WriteRegs(w, dev); WriteCmdSts(w, dev); всё;
	w.пВК_ПС;
кон WriteDev;

(* Dump PCI configuration space *)
проц WriteRegs(w : Потоки.Писарь; dev : Device);
перем value, offset : цел32;
нач
	w.пСимв8(9X); w.пСтроку8("PCI Configuration Space Registers: "); w.пВК_ПС;
	w.пСимв8(0EX); (* KernelLog: non-proportional font *)
	нцДля offset := 0 до 3CH шаг 4 делай
		w.пСимв8(9X); w.пСимв8(9X); w.п16ричное(offset, -2); w.пСимв8("h"); w.пСимв8(9X); w.пСимв8(9X);
		ReadConfigDword(dev.bus.number, dev.devfn, PCI.DevReg + offset, value);
		w.п16ричное(value, -8); w.пВК_ПС;
	кц;
	w.пСимв8(0FX); (* KernelLog: proportional font *)
кон WriteRegs;

(* Decode & display the command and the status register *)
проц WriteCmdSts(w : Потоки.Писарь; dev : Device);
перем value : цел32; dword : мнвоНаБитахМЗ;
нач
	ReadConfigDword(dev.bus.number, dev.devfn, PCI.CmdReg, value); dword := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, value);
	w.пСимв8(9X); w.пСтроку8("Command Register: "); w.пВК_ПС;
	w.пСимв8(9X); w.пСимв8(9X);
	w.пСтроку8("IO Space: "); если 0 в dword то w.пСтроку8("On"); иначе w.пСтроку8("Off"); всё;
	w.пСтроку8(", Memory Space: "); если 1 в dword то w.пСтроку8("On"); иначе w.пСтроку8("Off"); всё;
	w.пСтроку8(", Bus Master: "); если 2 в dword то w.пСтроку8("On"); иначе w.пСтроку8("Off"); всё;
	w.пСтроку8(", Special Cycles:  "); если 3 в dword то w.пСтроку8("On"); иначе w.пСтроку8("Off"); всё;
	w.пСтроку8(", Memory Write and Invalidate: "); если 4 в dword то w.пСтроку8("On"); иначе w.пСтроку8("Off"); всё;
	w.пВК_ПС;
	w.пСимв8(9X); w.пСимв8(9X);
	w.пСтроку8("VGA Palette Snoop: "); если 5 в dword то w.пСтроку8("On"); иначе w.пСтроку8("Off"); всё;
	w.пСтроку8(", Parity Error Response: "); если 6 в dword то w.пСтроку8("On"); иначе w.пСтроку8("Off"); всё;
	w.пСтроку8(", Stepping Control: "); если 7 в dword то w.пСтроку8("On"); иначе w.пСтроку8("Off"); всё;
	w.пСтроку8(", SERR#: "); если 8 в dword то w.пСтроку8("On"); иначе w.пСтроку8("Off"); всё;
	w.пСтроку8(", Fast Back-to-Back: "); если 9 в dword то w.пСтроку8("On"); иначе w.пСтроку8("Off"); всё;
	w.пВК_ПС;
	w.пСимв8(9X); w.пСтроку8("Status Register: "); w.пВК_ПС;
	w.пСимв8(9X); w.пСимв8(9X);
	w.пСтроку8("Capabilities List: "); если 4 + 16 в dword то w.пСтроку8("Yes"); иначе w.пСтроку8("No"); всё;
	w.пСтроку8(", 66MHz Capable: "); если 5 + 16 в dword то w.пСтроку8("Yes"); иначе w.пСтроку8("No"); всё;
	w.пСтроку8(", Fast Back-to-Back Capable: "); если 7 + 16 в dword то w.пСтроку8("Yes"); иначе w.пСтроку8("No"); всё;
	w.пСтроку8(", Master Data Parity Error: "); если 8 + 16 в dword то w.пСтроку8("Yes"); иначе w.пСтроку8("No"); всё;
	w.пВК_ПС;
	w.пСимв8(9X); w.пСимв8(9X);
	w.пСтроку8("DEVSEL timing: ");
	если {9+16, 10+16} * dword = {} то w.пСтроку8("Fast");
	аесли {9+16, 10+16} * dword = {9+16} то w.пСтроку8("Medium");
	аесли {9+16, 10+16} * dword = {10+16} то w.пСтроку8("Slow");
	иначе w.пСтроку8("ERROR");
	всё;
	w.пВК_ПС;
	w.пСимв8(9X); w.пСимв8(9X);
	w.пСтроку8("Signaled Target Abort: "); если 11 + 16 в dword то w.пСтроку8("Yes"); иначе w.пСтроку8("No"); всё;
	w.пСтроку8(", Received Target Abort: "); если 12 + 16 в dword то w.пСтроку8("Yes"); иначе w.пСтроку8("No"); всё;
	w.пСтроку8(", Received Master Abort: "); если 13 + 16 в dword то w.пСтроку8("Yes"); иначе w.пСтроку8("No"); всё;
	w.пСтроку8(", Signaled System Error: "); если 14 + 16 в dword то w.пСтроку8("Yes"); иначе w.пСтроку8("No"); всё;
	w.пСтроку8(", Detected Parity Error: "); если 15 + 16 в dword то w.пСтроку8("Yes"); иначе w.пСтроку8("No"); всё;
	w.пВК_ПС;
кон WriteCmdSts;

проц ReadConfigByte(bus, devfn, ofs: цел32;  перем val: цел32);
перем res: целМЗ;
нач
	res := PCI.ReadConfigByte(bus, арифмСдвиг(devfn, -3) остОтДеленияНа 20H, devfn остОтДеленияНа 8, ofs, val);
	утв(res = PCI.Done)
кон ReadConfigByte;

проц ReadConfigWord(bus, devfn, ofs: цел32;  перем val: цел32);
перем res: целМЗ;
нач
	res := PCI.ReadConfigWord(bus, арифмСдвиг(devfn, -3) остОтДеленияНа 20H, devfn остОтДеленияНа 8, ofs, val);
	утв(res = PCI.Done)
кон ReadConfigWord;

проц ReadConfigDword(bus, devfn, ofs: цел32;  перем val: цел32);
перем res: целМЗ;
нач
	res := PCI.ReadConfigDword(bus, арифмСдвиг(devfn, -3) остОтДеленияНа 20H, devfn остОтДеленияНа 8, ofs, val);
	утв(res = PCI.Done)
кон ReadConfigDword;

проц WriteConfigDword(bus, devfn, ofs, val: цел32);
перем res: целМЗ;
нач
	res := PCI.WriteConfigDword(bus, арифмСдвиг(devfn, -3) остОтДеленияНа 20H, devfn остОтДеленияНа 8, ofs, val);
	утв(res = PCI.Done)
кон WriteConfigDword;

(** Exported commands *)

(** Perform bus enumeration and display information about found PCI busses/devices *)
проц Scan*(context : Commands.Context); (** ["-d"|"--details"] ~ *)
перем
	options : Options.Options;
	root : Bus; dev, prev: Device;
	version, lastPCIBus, hw, count: цел32;
	pciids : Files.File;
нач {единолично}
	нов(options);
	options.Add("d", "details", Options.Flag);
	если options.Parse(context.arg, context.error) то
		context.out.пСтроку8("PCITools: PCI bus enumeration:"); context.out.пВК_ПС;
		если PCI.PCIPresent(version, lastPCIBus, hw) = PCI.Done то
			context.out.пСтроку8("PCI Bus Information: "); context.out.пЦел64(lastPCIBus + 1, 0); context.out.пСтроку8(" bus(ses) found, PCI version: ");
			context.out.п16ричное(version DIV 256, -2); context.out.пСимв8("."); context.out.п16ричное(version остОтДеленияНа 256, -2); context.out.пВК_ПС;
			context.out.пВК_ПС;
			нов(root);
			root.subordinate := ScanBus(root, dev);
			count := 0; prev := НУЛЬ;
			нцПока dev # НУЛЬ делай
				dev.prev := prev; prev := dev;
				dev := dev.next; увел(count)
			кц;
			pciids := Files.Old(PCIIDS);
			нцПока prev # НУЛЬ делай
				WriteDev(context.out, prev, pciids, options.GetFlag("details"));
				prev := prev.prev
			кц;
			context.out.пЦел64(count, 1); context.out.пСтроку8(" devices found"); context.out.пВК_ПС;
		иначе
			context.out.пСтроку8("PCI not present"); context.out.пВК_ПС;
		всё;
	всё;
кон Scan;

(** Perform bus enumeration and install appropriate device drivers if available *)
проц DetectHardware*(context : Commands.Context);
перем ver, last, hw : цел32;
нач
	если DriverDatabase.enabled то
		если (PCI.PCIPresent(ver, last, hw) = PCI.Done) то
			InstallPCIDrivers;
		иначе
			context.out.пСтроку8("PCITools: No PCI bus found."); context.out.пВК_ПС;
		всё;
	иначе
		context.out.пСтроку8("PCITools: Automatic hardware detedtion is disabled."); context.out.пВК_ПС;
	всё;
кон DetectHardware;

кон PCITools.

PCITools.Scan ~  System.Free PCITools DriverDatabase ~

PCITools.Scan details ~

PCITools.DetectHardware ~
