модуль WMDiagramComponents;	(** AUTHOR "TF"; PURPOSE "Diagram components"; *)

использует
	Objects, Strings, WMRectangles, WMGraphics, WMEvents, WMProperties, WMStandardComponents,
	WMComponents, Modules, WMGraphicUtilities;

конст
	(* MultiPointView.style *)
	StyleAuto = 0; 		(* default; StyleAreas if Sum or Max is available, StyleLines otherwise *)
	StyleLines = 1;
	StyleAreas = 2;

	(* DataDescriptor.flags *)
	Hidden* = 0;		(* Don't visualize! *)
	Sum* = 1;			(* This value is the sum of all other values in the same dataset *)
	Maximum* = 2;		(* This value is the maximum that other values in the same dataset can reach *)
	Standalone* =3; 	(* Indicate that this value is not affected by Sum/Maximum of dataset *)

	Invalid = -1;

тип

	DataPointModel* = окласс
	перем
		lockedBy : динамическиТипизированныйУкль;
		lockLevel : цел32;
		viewChanged : булево;
		onChanged- : WMEvents.EventSource; (** does not hold the lock, if called *)

		проц &New*;
		нач
			нов(onChanged, сам, WMComponents.NewString("DataPointModelChanged"), НУЛЬ, НУЛЬ);
			lockLevel := 0;
		кон New;

		(** acquire a read/write lock on the object *)
		проц Acquire*;
		перем me : динамическиТипизированныйУкль;
		нач {единолично}
			me := Objects.ActiveObject();
			если lockedBy = me то
				утв(lockLevel # -1);	(* overflow *)
				увел(lockLevel);
			иначе
				дождись(lockedBy = НУЛЬ); viewChanged := ложь;
				lockedBy := me; lockLevel := 1;
			всё;
		кон Acquire;

		(** release the read/write lock on the object *)
		проц Release*;
		перем hasChanged : булево;
		нач
			нач {единолично}
				утв(lockedBy = Objects.ActiveObject(), 3000);
				hasChanged := ложь;
				умень(lockLevel);
				если lockLevel = 0 то lockedBy := НУЛЬ; hasChanged := viewChanged; всё;
			кон;
			если hasChanged то onChanged.Call(НУЛЬ); всё;
		кон Release;

	кон DataPointModel;

тип

	DataDescriptor* = запись
		name* : массив 32 из симв8;
		unit* : массив 16 из симв8;
		color* : WMGraphics.Color;
		flags* : мнвоНаБитахМЗ;
	кон;

	DatasetDescriptor* = укль на массив из DataDescriptor;

	Dataset* = укль на массив из вещ32;
	LongintDataset* = укль на массив из цел32;

	Statistics* = запись
		valid- : булево;
		cur*, min*, max*, avg*, sum* : Dataset;
		nbrOfSamples- : цел32;
	кон;

тип

	MultiPointModel* = окласс(DataPointModel)
	перем
		descriptor : DatasetDescriptor;

		buffer : укль на массив из Dataset;
		pos, nofItems, dimensions : размерМЗ;
		bufferSize : размерМЗ;

		viewSampleCount : цел32;

		(* Lifetime statistics - min, max, sum of all value ever received (not just those in buffer) *)
		statistics : булево;
		valid : булево;
		nbrOfValues : цел32;
		cur, min, max, sum : Dataset;

		проц &Init*(bufferSize, dimensions : размерМЗ);
		перем i : размерМЗ;
		нач
			утв(dimensions >= 1);
			New;
			сам.bufferSize := bufferSize;
			сам.dimensions := dimensions;

			(* Initialize buffer *)
			нов(buffer, bufferSize);
			нцДля i := 0 до bufferSize - 1 делай
				нов(buffer[i], dimensions);
			кц;

			statistics := истина; nbrOfValues := 0; valid := ложь;
			нов(cur, dimensions); нов(min, dimensions); нов(max, dimensions); нов(sum, dimensions);
		кон Init;

		проц Reset*;
		перем i : размерМЗ;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			nofItems := 0; pos := 0; viewSampleCount := 0; viewChanged := истина;
			(* Reset statistics *)
			если statistics то
				nbrOfValues := 0; valid := ложь;
				нцДля i := 0 до dimensions-1 делай
					min[i] := матМаксимум(вещ32); max[i] := матМинимум(вещ32);
					sum[i] := 0; cur[i] := 0;
				кц;
			всё;
		кон Reset;

		проц GetStatistics*(перем statistics : Statistics);
		перем i : размерМЗ;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			утв(длинаМассива(statistics.cur) = dimensions);
			утв((длинаМассива(statistics.min) = dimensions) и (длинаМассива(statistics.max) = dimensions));
			утв((длинаМассива(statistics.sum) = dimensions) и (длинаМассива(statistics.avg) = dimensions));
			если valid то
				statistics.valid := истина;
				statistics.nbrOfSamples := nbrOfValues;
				нцДля i := 0 до dimensions-1 делай
					statistics.cur[i] := cur[i];
					statistics.min[i] := min[i];
					statistics.max[i] := max[i];
					statistics.sum[i] := sum[i];
					statistics.avg[i] := sum[i] / nbrOfValues;
				кц;
			иначе
				statistics.valid := ложь;
			всё;
		кон GetStatistics;

		(* Find the max and min value in the model dataset. Model lock must be held *)
		проц FindMinMax*(from, len : размерМЗ; перем min, max : вещ32);
		перем bufferIndex, dim : размерМЗ; v : вещ32; points : Dataset;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			утв((from >= 0) и (len > 0) и (from + len < nofItems));
			min := матМаксимум(вещ32); max := матМинимум(вещ32);

			bufferIndex := (pos + bufferSize - nofItems + from) остОтДеленияНа bufferSize;

			нцПока len > 0 делай
				points := buffer[bufferIndex];
				нцДля dim := 0 до dimensions-1 делай
					если (descriptor = НУЛЬ) или ((descriptor # НУЛЬ) и ~(Hidden в descriptor[dim].flags)) то
						v := points[dim];
						min := RMin(min, v); max := RMax(max, v);
					всё;
				кц;
				bufferIndex := (bufferIndex + 1) остОтДеленияНа bufferSize;
				умень(len);
			кц;
		кон FindMinMax;

		(* Find the max and min value in the model dataset for each dimension. Model lock must be held *)
		проц FindAllMinMax*(from, len : размерМЗ; перем min, max : вещ32);
		перем bufferIndex, dim : размерМЗ; sum : вещ32; points : Dataset;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			утв((from >= 0) и (len > 0) и (from + len < nofItems));
			min := матМаксимум(вещ32); max := матМинимум(вещ32);

			bufferIndex := (pos + bufferSize - nofItems + from) остОтДеленияНа bufferSize;

			нцПока len > 0 делай
				points := buffer[bufferIndex];

				sum := 0.0;
				нцДля dim := 0 до dimensions-1 делай
					если (descriptor = НУЛЬ) или ((descriptor # НУЛЬ) и ~(Hidden в descriptor[dim].flags)) то
						sum := sum + points[dim];
					всё;
				кц;
				min := RMin(min, sum);
				max := RMax(max, sum);

				bufferIndex := (bufferIndex + 1) остОтДеленияНа bufferSize;
				умень(len);
			кц;
		кон FindAllMinMax;

		проц SetDescriptor*(ds : DatasetDescriptor);
		нач
			Acquire; descriptor := ds; Release;
		кон SetDescriptor;

		проц PutValues*(values : Dataset);
		перем i : размерМЗ; value : вещ32;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			утв(длинаМассива(values) = dimensions);
			если nofItems < bufferSize то увел(nofItems) всё;
			нцДля i := 0 до dimensions - 1 делай
				buffer[pos][i] := values[i];
			кц;
			pos := (pos + 1) остОтДеленияНа bufferSize;
			увел(viewSampleCount);
			viewChanged := истина;

			если statistics то
				valid := истина; увел(nbrOfValues);
				нцДля i := 0 до dimensions - 1 делай
					value := values[i];
					cur[i] := value;
					если value < min[i] то min[i] := value; всё;
					если value > max[i] то max[i] := value; всё;
					sum[i] := sum[i] + value;
				кц;
			всё;
		кон PutValues;

		проц GetValues*(index : размерМЗ; перем dataset : Dataset);
		перем dim, bufferIndex : размерМЗ;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			утв((dataset # НУЛЬ) и (длинаМассива(dataset) = dimensions));
			bufferIndex := (pos + bufferSize - nofItems + index) остОтДеленияНа bufferSize;
			нцДля dim := 0 до dimensions-1 делай
				dataset[dim] := buffer[bufferIndex][dim];
			кц;
		кон GetValues;

		проц GetNofDimensions*() : размерМЗ;
		нач (* don't need lock here *)
			возврат dimensions;
		кон GetNofDimensions;

		проц GetNofPoints*() : размерМЗ;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			возврат nofItems;
		кон GetNofPoints;

	кон MultiPointModel;

тип

	ExtUpdateHandler* = проц {делегат};

тип

	MultiPointView* = окласс(WMComponents.VisualComponent)
	перем
		model- : MultiPointModel;

		(* Note: Grid is not rendered when deltaXGrid/deltaXGridSmall are zero *)
		min-, max-, deltax-, deltaXGrid-, deltaXGridSmall-, glassShade-, valueWidth- : WMProperties.Int32Property;
		minI, maxI, deltaxI, deltaXGridI, deltaXGridSmallI, glassShadeI, valueWidthI : цел32;

		color-, gridColor- : WMProperties.ColorProperty;
		colorI, gridColorI : WMGraphics.Color;

		autoMin-, autoMax-, showValues-: WMProperties.BooleanProperty;
		autoMinI, autoMaxI, showValuesI : булево;

		unit- : WMProperties.StringProperty;

		style- : WMProperties.Int32Property;
		styleI : цел32;

		extUpdate : ExtUpdateHandler;

		dimensions : размерМЗ;

		points : Dataset;
		points0, points1 : LongintDataset;

		проц &New*;
		нач
			Init;
			SetNameAsString(StrMultiPointView);
			dimensions := 1; нов(points0, 1); нов(points1, 1); нов(points, 1);
			нов(model, 1024, 1);
			нов(min, PrototypeMin, НУЛЬ, НУЛЬ); properties.Add(min);
			нов(max, PrototypeMax,  НУЛЬ, НУЛЬ); properties.Add(max);
			нов(deltax, PrototypeDeltax,  НУЛЬ, НУЛЬ); properties.Add(deltax);
			нов(deltaXGrid, PrototypeDeltaXGrid,  НУЛЬ, НУЛЬ); properties.Add(deltaXGrid);
			нов(deltaXGridSmall, PrototypeDeltaXGridSmall,  НУЛЬ, НУЛЬ); properties.Add(deltaXGridSmall);
			нов(color, PrototypeColor,  НУЛЬ, НУЛЬ); properties.Add(color);
			нов(gridColor, PrototypeGridColor,  НУЛЬ, НУЛЬ); properties.Add(gridColor);
			нов(glassShade, PrototypeGlassShade,  НУЛЬ, НУЛЬ); properties.Add(glassShade);
			нов(valueWidth, PrototypeValueWidth,  НУЛЬ, НУЛЬ); properties.Add(valueWidth);
			нов(autoMin, PrototypeAutoMin,  НУЛЬ, НУЛЬ); properties.Add(autoMin);
			нов(autoMax, PrototypeAutoMax,  НУЛЬ, НУЛЬ); properties.Add(autoMax);
			нов(showValues, PrototypeShowValues,  НУЛЬ, НУЛЬ); properties.Add(showValues);
			нов(unit, PrototypeUnit,  НУЛЬ, НУЛЬ); properties.Add(unit);
			нов(style, PrototypeStyle, НУЛЬ, НУЛЬ); properties.Add(style);
			CacheProperties;
			SetFont(WMGraphics.GetFont("Oberon", 8, {}));
			model.onChanged.Add(Update);
		кон New;

		проц CacheProperties;
		нач
			minI := min.Get(); maxI := max.Get();
			deltaxI := deltax.Get(); deltaXGridI := deltaXGrid.Get(); deltaXGridSmallI := deltaXGridSmall.Get();
			glassShadeI := glassShade.Get();
			valueWidthI := valueWidth.Get();
			colorI := color.Get(); gridColorI := gridColor.Get();
			autoMinI := autoMin.Get(); autoMaxI := autoMax.Get(); showValuesI := showValues.Get();
			styleI := style.Get();
		кон CacheProperties;

		проц {перекрыта}PropertyChanged*(property, data : динамическиТипизированныйУкль);
		нач
			если (property = min) то minI := min.Get(); Invalidate;
			аесли (property = max) то maxI := max.Get(); Invalidate;
			аесли (property = deltax) или (property = deltaXGrid) или (property = deltaXGridSmall) то
				deltaxI := deltax.Get(); deltaXGridI := deltaXGrid.Get(); deltaXGridSmallI := deltaXGridSmall.Get(); Invalidate;
			аесли (property = glassShade) то glassShadeI := glassShade.Get(); Invalidate;
			аесли (property = valueWidth) то valueWidthI := valueWidth.Get(); Invalidate;
			аесли (property = color) или (property = gridColor) то colorI := color.Get(); gridColorI := gridColor.Get(); Invalidate;
			аесли (property = autoMin) или (property = autoMax) или (property = showValues) то
				autoMinI := autoMin.Get(); autoMaxI := autoMax.Get(); showValuesI := showValues.Get(); Invalidate;
			аесли (property = style) то
				styleI := style.Get(); Invalidate;
			иначе
				PropertyChanged^(property, data);
			всё;
		кон PropertyChanged;

		проц {перекрыта}Initialize*;
		нач
			Initialize^;
			CacheProperties;
		кон Initialize;

		проц {перекрыта}RecacheProperties*;
		нач
			RecacheProperties^;
			CacheProperties;
		кон RecacheProperties;

		проц SetExtModel*(model : MultiPointModel);
		нач
			утв(model # НУЛЬ);
			Acquire;
			если model # НУЛЬ то model.onChanged.Remove(Update); всё;
			сам.model := model;
			сам.dimensions := model.GetNofDimensions();
			нов(points0, dimensions);
			нов(points1, dimensions);
			нов(points, dimensions);
			model.onChanged.Add(Update);
			Release;
			Invalidate;
		кон SetExtModel;

		проц SetExtUpdate*(extUpdate : ExtUpdateHandler);
		нач
			Acquire; сам.extUpdate := extUpdate; Release;
		кон SetExtUpdate;

		проц Update(sender, data : динамическиТипизированныйУкль);
		нач
			если extUpdate # НУЛЬ то
				extUpdate();
			иначе
				Invalidate;
			всё;
		кон Update;

		проц Scale(factor, min : вещ32; srcPoints : Dataset; tarPoints : LongintDataset; height, border : размерМЗ);
		перем dim : размерМЗ;
		нач
			нцДля dim := 0 до dimensions-1 делай
				tarPoints[dim] := округлиВниз(((srcPoints[dim] - min) * factor) * (height - 2 * border));
			кц;
		кон Scale;

		проц DrawLines(canvas : WMGraphics.Canvas; w, h, border : размерМЗ; перем tmin, tmax : вещ32);
		перем
			nofPoints, nofVisible, index, dim : размерМЗ;
			x, v0, v1 : размерМЗ;
			factor : вещ32;
		нач
			model.Acquire;
			nofPoints := model.GetNofPoints();
			nofVisible := матМинимум(nofPoints - 1, w DIV deltaxI + 1);

			если nofVisible >= 2 то
				index := nofPoints - 1;
				model.FindMinMax(index - nofVisible, nofVisible, tmin, tmax);
				если ~autoMinI то tmin := minI; всё;
				если ~autoMaxI то tmax := maxI; всё;
				если (tmax - tmin) = 0 то factor := 1; иначе factor := 1 / (tmax - tmin); всё;

				model.GetValues(index, points); умень(index);
				Scale(factor, tmin, points, points0, h, border);

				x := w;
				нцПока (index > 0) и (x >= 0) делай
					нцДля dim := 0 до dimensions-1 делай
						points1[dim] := points0[dim];
					кц;

					model.GetValues(index, points);
					Scale(factor, tmin, points, points0, h, border);

					нцДля dim := 0 до dimensions-1 делай
						v0 := points0[dim]; v1 := points1[dim];
						если (model.descriptor = НУЛЬ) то
							 canvas.Line(x - deltaxI, (h - border) - v0, x, (h - border) - v1, colorI, WMGraphics.ModeCopy);
						аесли ~(Hidden в model.descriptor[dim].flags) то
							 canvas.Line(x - deltaxI, (h - border) - v0, x, (h - border) - v1, model.descriptor[dim].color, WMGraphics.ModeCopy);
						всё;
					кц;
					x := x - deltaxI;
					умень(index);
				кц;
			всё;
			model.Release;
		кон DrawLines;

		проц DrawAreas(canvas : WMGraphics.Canvas; w, h, border : размерМЗ; перем tmin, tmax : вещ32);
		перем
			nofPoints, nofVisible, nofStandalone, index, dim : размерМЗ;
			x, y0, y1: размерМЗ; colorLine, color : WMGraphics.Color;
			maxSumIdx : размерМЗ;
			v0, v1 : цел32;
			factor : вещ32;
			poly : массив 4 из WMGraphics.Point2d;
		нач
			model.Acquire;
			nofPoints := model.GetNofPoints();
			nofVisible := матМинимум(nofPoints - 1, w DIV deltaxI + 1);
			nofStandalone := GetNumberOf(Standalone, 0, матМаксимум(размерМЗ), model.descriptor);

			если nofVisible >= 2 то
				maxSumIdx := FindIndexOf(Maximum, model.descriptor);
				если (maxSumIdx = Invalid) то
					maxSumIdx := FindIndexOf(Sum, model.descriptor);
				всё;

				index := nofPoints - 1;
				если (autoMinI или autoMaxI) то
					если (maxSumIdx # Invalid) то
						model.FindMinMax(index - nofVisible, nofVisible, tmin, tmax);
					иначе
						model.FindAllMinMax(index - nofVisible, nofVisible, tmin, tmax);
					всё;
				всё;
				если ~autoMinI то tmin := minI; всё;
				если ~autoMaxI то tmax := maxI; всё;

				(* use min and max for internal computation *)
				если (tmax - tmin) = 0 то factor := 1; иначе factor := 1 / (tmax - tmin); всё;

				model.GetValues(index, points); умень(index);
				Scale(factor, tmin, points, points0, h, border);

				x := w;
				нцПока (index > 0) и (x >= 0) делай
					нцДля dim := 0 до dimensions-1 делай
						points1[dim] := points0[dim];
					кц;
					model.GetValues(index, points);
					Scale(factor, tmin, points, points0, h, border);

					y0 := h - border; y1 := y0;
					нцДля dim := 0 до dimensions-1 делай
						если (model.descriptor = НУЛЬ) или ({Hidden, Maximum, Sum, Standalone} * model.descriptor[dim].flags = {}) то
							v0 := points0[dim]; v1 := points1[dim];
							poly[0].x := x - deltaxI;
							poly[0].y := y0;
							poly[1].x := x - deltaxI;
							poly[1].y := y0 - v0;
							poly[2].x := x;
							poly[2].y := y1 - v1;
							poly[3].x := x;
							poly[3].y := y1;
							если (model.descriptor = НУЛЬ) то
								colorLine := colorI;
							иначе
								colorLine := model.descriptor[dim].color;
							всё;
							color := (colorLine - (colorLine остОтДеленияНа 100H)) + 60H;
							canvas.FillPolygonFlat(poly, 4, color, WMGraphics.ModeSrcOverDst);
							canvas.Line(x - deltaxI, y0 - v0, x, y1 - v1, colorLine, WMGraphics.ModeCopy);
							y0 := y0 - v0;
							y1 := y1 - v1;
						всё;
					кц;
					если (maxSumIdx # Invalid) то
						утв(model.descriptor # НУЛЬ);
						если ~(Hidden в model.descriptor[maxSumIdx].flags) то
							v0 := points0[maxSumIdx]; v1 := points1[maxSumIdx];
							если (Sum в model.descriptor[maxSumIdx].flags) то
								canvas.Line(x - deltaxI, y0 , x, y1, model.descriptor[maxSumIdx].color, WMGraphics.ModeCopy);
							иначе (* Maximum *)
								poly[0].x := x - deltaxI;
								poly[0].y := y0;
								poly[1].x := x - deltaxI;
								poly[1].y := h - border - v0;
								poly[2].x := x;
								poly[2].y := h - border - v1;
								poly[3].x := x;
								poly[3].y := y1;
								colorLine := model.descriptor[maxSumIdx].color;
								color := (colorLine - (colorLine остОтДеленияНа 100H)) + 70H;
								canvas.FillPolygonFlat(poly, 4, color, WMGraphics.ModeSrcOverDst);
								canvas.Line(x - deltaxI, h - border - v0 , x, h - border - v1, model.descriptor[maxSumIdx].color, WMGraphics.ModeCopy);
							всё;
						всё;
					всё;
					(* Standalone values are independet of other values in the same dataset and area plotted as curves in front of areas *)
					если (nofStandalone > 0) то
						нцДля dim := 0 до dimensions-1 делай
							v0 := points0[dim]; v1 := points1[dim];
							если (Standalone в model.descriptor[dim].flags) то
								если (model.descriptor # НУЛЬ) то
									colorLine := colorI;
								иначе
									colorLine := model.descriptor[dim].color;
								всё;
								canvas.Line(x - deltaxI, h - border - v0, x, h - border - v1, colorLine, WMGraphics.ModeCopy);
							всё;
						кц;
					всё;
					x := x - deltaxI;
					умень(index);
				кц;
			всё;
			model.Release;
		кон DrawAreas;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем
			w, h, yborder, t, virtp : размерМЗ; color: WMGraphics.Color;
			mode : цел32;
			tmin, tmax : вещ32;
			str : массив 16 из симв8;
			s : Strings.String;
		нач
			canvas.SetFont(GetFont());
			yborder := 5;
			если fillColor.Get() # 0 то canvas.Fill(GetClientRect(), fillColor.Get(), WMGraphics.ModeCopy); всё;
			color := WMGraphicUtilities.ScaleColor(gridColorI, 80H);
			w := bounds.GetWidth(); h := bounds.GetHeight();
			если showValuesI то умень(w, valueWidthI); всё;
			canvas.Line(0, yborder, w, yborder, color, WMGraphics.ModeSrcOverDst);
			canvas.Line(0, h - yborder, w, h - yborder, color, WMGraphics.ModeSrcOverDst);

			virtp := model.viewSampleCount;

			если deltaXGridSmallI > 0 то
				t := w -  (virtp остОтДеленияНа deltaXGridSmallI) * deltaxI;
				нцПока t > 0 делай
					canvas.Line(t, 0, t, h, color, WMGraphics.ModeSrcOverDst);
					умень(t, deltaxI* deltaXGridSmallI);
				кц;
			всё;

			если deltaXGridI > 0 то
				t := w - (virtp остОтДеленияНа deltaXGridI) * deltaxI;
				если virtp остОтДеленияНа deltaXGridI = 0 то model.viewSampleCount := 0; всё;
				нцПока t > 0 делай
					canvas.Line(t, 0, t, h, gridColorI, WMGraphics.ModeSrcOverDst);
					умень(t, deltaxI* deltaXGridI);
				кц;
			всё;

			mode := styleI;
			если (styleI = StyleAuto) то
				если (GetNumberOf(Maximum, 0, матМаксимум(размерМЗ), model.descriptor) > 0) или (GetNumberOf(Sum, 0, матМаксимум(размерМЗ), model.descriptor) > 0) то
					mode := StyleAreas;
				иначе
					mode := StyleLines;
				всё;
			всё;

			просей mode из
				|StyleLines: DrawLines(canvas, w, h, yborder, tmin, tmax);
				|StyleAreas: DrawAreas(canvas, w, h, yborder, tmin, tmax);
			иначе
				DrawLines(canvas, w, h, yborder, tmin, tmax);
			всё;

			если showValuesI то
				canvas.Fill(WMRectangles.MakeRect(w, 0, w + 4, h), 080H, WMGraphics.ModeSrcOverDst);
				canvas.SetColor(colorI);
				Strings.FloatToStr(tmax, 0, 1, 0, str); canvas.DrawString(w + 5, 8 + 5, str);
				Strings.FloatToStr(tmin, 0, 1, 0, str); canvas.DrawString(w + 5, h - 5, str);
				s := unit.Get();
				если s # НУЛЬ то canvas.DrawString(w + 10, h DIV 2 + 4, s^); всё;
			всё;
			если glassShadeI # 0 то
				WMGraphicUtilities.RectGlassShade(canvas, GetClientRect(), glassShadeI, ложь);
			всё;
		кон DrawBackground;

		проц {перекрыта}Finalize*;
		нач
			если model # НУЛЬ то model.onChanged.Remove(Update); всё;
			Finalize^;
		кон Finalize;

	кон MultiPointView;

тип

	(** Graphical represenation of a dataset descriptor *)
	DescriptorView* = окласс (WMComponents.VisualComponent)
	перем
		descriptor : DatasetDescriptor;

		names, colors : укль на массив из WMStandardComponents.Label;
		checkboxes : укль на массив из WMStandardComponents.Checkbox;

		checkAllBtn, checkNoneBtn : WMStandardComponents.Button;

		(** Optimum width and height *)
		optWidth-, optHeight- : цел32;

		(** onClick handler called by each checkbox *)
		проц HandleClick(sender, data : динамическиТипизированныйУкль);
		перем i : размерМЗ; state : цел32;
		нач
			нцДля i := 0 до длинаМассива(checkboxes)-1 делай
				state := checkboxes[i].state.Get();
				если (state = WMStandardComponents.Checked) то
					исключиИзМнваНаБитах(descriptor[i].flags, Hidden);
				иначе
					включиВоМнвоНаБитах(descriptor[i].flags, Hidden);
				всё;
			кц;
		кон HandleClick;

		проц HandleButton(sender, data : динамическиТипизированныйУкль);
		перем i : размерМЗ;
		нач
			если sender = checkAllBtn то
				нцДля i := 0 до длинаМассива(checkboxes)-1 делай
					исключиИзМнваНаБитах(descriptor[i].flags, Hidden);
					checkboxes[i].state.Set(WMStandardComponents.Checked);
				кц;
			аесли sender = checkNoneBtn то
				нцДля i := 0 до длинаМассива(checkboxes)-1 делай
					включиВоМнвоНаБитах(descriptor[i].flags, Hidden);
					checkboxes[i].state.Set(WMStandardComponents.Unchecked);
				кц;
			иначе
			всё;
		кон HandleButton;

		проц &New*(ds : DatasetDescriptor);
		перем i : размерМЗ; panel : WMStandardComponents.Panel;
		нач
			утв((ds # НУЛЬ) и (длинаМассива(ds) >=1));
			descriptor := ds;
			Init;
			SetNameAsString(StrDatasetDescriptorView);
			нов(names, длинаМассива(ds));
			нов(colors, длинаМассива(ds));
			нов(checkboxes, длинаМассива(ds));
			optWidth := 200; optHeight := длинаМассива(ds)(цел32) * 20;
			нцДля i := 0 до длинаМассива(ds)-1 делай
				нов(panel);
				panel.alignment.Set(WMComponents.AlignTop); panel.bounds.SetHeight(20);
				panel.fillColor.Set(WMGraphics.White);

				нов(checkboxes[i]);
				checkboxes[i].alignment.Set(WMComponents.AlignLeft); checkboxes[i].bounds.SetExtents(20, 20);
				checkboxes[i].fillColor.Set(WMGraphics.White);
				checkboxes[i].onClick.Add(HandleClick);
				если (Hidden в ds[i].flags) то checkboxes[i].state.Set(WMStandardComponents.Unchecked);
				иначе checkboxes[i].state.Set(WMStandardComponents.Checked);
				всё;
				panel.AddInternalComponent(checkboxes[i]);

				нов(colors[i]);
				colors[i].alignment.Set(WMComponents.AlignLeft); colors[i].bounds.SetWidth(40);
				colors[i].fillColor.Set(WMGraphics.Black);
				colors[i].caption.SetAOC(" __________ "); colors[i].textColor.Set(ds[i].color);
				panel.AddInternalComponent(colors[i]);

				нов(names[i]);
				names[i].alignment.Set(WMComponents.AlignClient);
				names[i].fillColor.Set(WMGraphics.White);
				names[i].caption.SetAOC(ds[i].name);
				panel.AddInternalComponent(names[i]);

				AddInternalComponent(panel);
			кц;

			нов(panel);
			panel.alignment.Set(WMComponents.AlignBottom); panel.bounds.SetHeight(20);
			panel.fillColor.Set(WMGraphics.White);
			AddInternalComponent(panel);

			нов(checkAllBtn);
			checkAllBtn.alignment.Set(WMComponents.AlignLeft); checkAllBtn.bounds.SetWidth(optWidth DIV 2);
			checkAllBtn.caption.SetAOC("ALL");
			checkAllBtn.onClick.Add(HandleButton);
			panel.AddInternalComponent(checkAllBtn);

			нов(checkNoneBtn);
			checkNoneBtn.alignment.Set(WMComponents.AlignClient);
			checkNoneBtn.caption.SetAOC("NONE");
			checkNoneBtn.onClick.Add(HandleButton);
			panel.AddInternalComponent(checkNoneBtn);
		кон New;

	кон DescriptorView;

тип

	(* coordinate system as preparation for more flexible handling of plots *)
	CoordinateSystem= запись
		l,t,r,b: вещ64;
	кон;

	BarChart* = окласс(WMComponents.VisualComponent)
	перем
		barColor-,lineColor- ,textColor-,backgroundColor-: WMProperties.ColorProperty;

		width-,offset-: вещ64; (* width and offset of histogram, initialized to width=1, offset = -0.5  *)
		numberData-: цел32; (* number of box plot data *)
		heights-: укль на массив из вещ64; (* bin height *)
		labels-: укль на массив из Strings.String; (* bin label (if any) *)
		colors- : укль на массив из WMGraphics.Color;
		c: CoordinateSystem;
		vertical: булево;

		проц &{перекрыта}Init*;
		нач
			Init^;
			нов(barColor, НУЛЬ, НУЛЬ, НУЛЬ); properties.Add(barColor);
			barColor.Set(101010A0H);

			нов(lineColor, НУЛЬ, НУЛЬ, НУЛЬ); properties.Add(lineColor);
			lineColor.Set(0FFH);

			нов(textColor, НУЛЬ, НУЛЬ, НУЛЬ); properties.Add(textColor);
			textColor.Set(цел32(0FF0000FFH));

			нов(backgroundColor, НУЛЬ, НУЛЬ, НУЛЬ); properties.Add(backgroundColor);
			backgroundColor.Set(цел32(0FFFF00FFH));

			heights := НУЛЬ; labels := НУЛЬ; colors := НУЛЬ;
			width := 1; offset := -0.5; numberData := 0;
			c.l := -1; c.t := -1; c.r := 2; c.b := 2;
		кон Init;

		проц UpdateCoordinateSystem;
		нач
			если vertical то
				c.l := -1; c.r := numberData;
				c.b := 0; c.t := 1;
			иначе
				c.l := 0; c.r := 1;
				c.b := -1; c.t := numberData;
			всё;
		кон UpdateCoordinateSystem;

		проц SetData*(конст heights: массив из вещ64; numberData: цел32);
		перем i: цел32;
		нач
			если длинаМассива(heights) < numberData то возврат всё;
			Acquire();
			сам.numberData := numberData;
			нов(сам.heights,numberData);
			нцДля i := 0 до numberData-1 делай
				сам.heights[i] := heights[i];
			кц;
			если (labels # НУЛЬ) и (длинаМассива(labels) # numberData) то labels := НУЛЬ всё;
			UpdateCoordinateSystem;
			Release();
			Invalidate;
		кон SetData;

		проц SetLabels*(конст labels: массив из Strings.String);
		перем i: цел32;
		нач
			Acquire();
			если длинаМассива(labels) < numberData то возврат всё;
			нов(сам.labels,numberData);
			нцДля i := 0 до numberData-1 делай
				сам.labels[i] := labels[i]
			кц;
			Release();
			Invalidate;
		кон SetLabels;

		проц SetColors*(конст colors: массив из WMGraphics.Color);
		перем i: цел32;
		нач
			Acquire();
			если длинаМассива(colors) < numberData то возврат всё;
			нов(сам.colors, numberData);
			нцДля i := 0 до numberData-1 делай
				сам.colors[i] := colors[i]
			кц;
			Release();
			Invalidate;
		кон SetColors;

		проц SetWidthOffset*(width,offset: вещ64);
		нач
			Acquire();
			сам.width := width;
			сам.offset := offset;
			Release();
			Invalidate;
		кон SetWidthOffset;

		проц SetVertical*(vertical: булево);
		нач
			Acquire;
			сам.vertical := vertical;
			UpdateCoordinateSystem;
			Release;
			Invalidate;
		кон SetVertical;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		нач
			если (property = textColor) или (property = barColor) или (property = lineColor) или (property = backgroundColor) то Invalidate
			иначе PropertyChanged^(sender, property)
			всё;
		кон PropertyChanged;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем boundary,rect: WMRectangles.Rectangle; i: цел32;

			проц Point2dToPoint(xr,yr: вещ64; перем x,y: размерМЗ);
			нач
				x := округлиВниз(boundary.l + (xr-c.l)/(c.r-c.l) * (boundary.r-boundary.l) +0.5);
				y := округлиВниз(boundary.b + (yr-c.b)/(c.t-c.b) * (boundary.t-boundary.b) +0.5);
			кон Point2dToPoint;

			проц Rect2dToRect(xr,yr,wr,hr: вещ64; перем rect: WMRectangles.Rectangle);
			нач
				Point2dToPoint(xr,yr,rect.l,rect.b);
				Point2dToPoint(xr+wr,yr+hr,rect.r,rect.t);
			кон Rect2dToRect;

		нач
			DrawBackground^(canvas);
			boundary := GetClientRect();
			canvas.Fill(boundary,backgroundColor.Get(),WMGraphics.ModeSrcOverDst);

			canvas.SetFont(WMGraphics.GetFont("Oberon", 10, {}));
			нцДля i := 0 до numberData-1 делай
				если vertical то
					Rect2dToRect(offset+i*width,0,width,heights[i],rect);
				иначе
					Rect2dToRect(0,offset+i*width,heights[i],width,rect);
				всё;
				если (colors # НУЛЬ) то
					canvas.Fill(rect,colors[i],WMGraphics.ModeSrcOverDst);
				иначе
					canvas.Fill(rect,barColor.Get(),WMGraphics.ModeSrcOverDst);
				всё;
				canvas.Line(rect.l,rect.t,rect.r,rect.t,lineColor.Get(),WMGraphics.ModeSrcOverDst);
				canvas.Line(rect.l,rect.b,rect.r,rect.b,lineColor.Get(),WMGraphics.ModeSrcOverDst);
				canvas.Line(rect.l,rect.t,rect.l,rect.b,lineColor.Get(),WMGraphics.ModeSrcOverDst);
				canvas.Line(rect.r,rect.t,rect.r,rect.b,lineColor.Get(),WMGraphics.ModeSrcOverDst);
			кц;

			если ~vertical и (labels # НУЛЬ) то (* labels would better be organized as separate panel but for the time being ... *)
				нцДля i := 0 до numberData-1 делай
					если  (labels[i] # НУЛЬ) то
						canvas.SetColor(textColor.Get());
						Rect2dToRect(0,offset+i*width,1,width,rect);
						WMGraphics.DrawStringInRect(canvas, rect, ложь, WMGraphics.AlignRight, WMGraphics.AlignCenter, labels[i]^)
					всё;
				кц;
			всё;

		кон DrawBackground;

	кон BarChart;


перем
	PrototypeUnit : WMProperties.StringProperty;
	PrototypeMin, PrototypeMax, PrototypeDeltax, PrototypeDeltaXGrid, PrototypeDeltaXGridSmall,
	PrototypeGlassShade, PrototypeValueWidth : WMProperties.Int32Property;
	PrototypeColor, PrototypeGridColor : WMProperties.ColorProperty;
	PrototypeAutoMin, PrototypeAutoMax, PrototypeShowValues  : WMProperties.BooleanProperty;
	PrototypeStyle : WMProperties.Int32Property;

	StrMultiPointView, StrDatasetDescriptorView : Strings.String;

проц InitStrings;
нач
	StrMultiPointView := Strings.NewString("MultiPointView");
	StrDatasetDescriptorView := Strings.NewString("DatasetDescriptorView");
кон InitStrings;

проц InitProtoTypes;

	проц S(конст s : массив из симв8) : Strings.String;
	нач
		возврат Strings.NewString(s);
	кон S;

нач
	нов(PrototypeUnit, НУЛЬ, S("Unit"), S("unit string for the diagram, if any"));
	нов(PrototypeMin, НУЛЬ, S("Min"), S("minimum to assume if not autoMin")); PrototypeMin.Set(0);
	нов(PrototypeMax, НУЛЬ, S("Max"), S("maximum to assume if not autoMax")); PrototypeMax.Set(100);
	нов(PrototypeDeltax, НУЛЬ, S("Deltax"), S("pixel between samples")); PrototypeDeltax.Set(2);
	нов(PrototypeDeltaXGrid, НУЛЬ, S("DeltaXGrid"), S("samples between separator lines")); PrototypeDeltaXGrid.Set(60);
	нов(PrototypeDeltaXGridSmall, НУЛЬ, S("DeltaXGridSmall"),
		S("samples between small separator lines")); PrototypeDeltaXGridSmall.Set(10);

	нов(PrototypeColor, НУЛЬ, S("Color"), S("color of the graph")); PrototypeColor.Set(0CC00FFH);
	нов(PrototypeGridColor, НУЛЬ, S("GridColor"), S("color of the grid")); PrototypeGridColor.Set(цел32(0FFCC00FFH));

	нов(PrototypeGlassShade, НУЛЬ, S("GlassShade"), НУЛЬ); PrototypeGlassShade.Set(8);
	нов(PrototypeValueWidth, НУЛЬ, S("ValueWidth"), НУЛЬ); PrototypeValueWidth.Set(50);
	нов(PrototypeAutoMin, НУЛЬ, S("AutoMin"), НУЛЬ); PrototypeAutoMin.Set(истина);
	нов(PrototypeAutoMax, НУЛЬ, S("AutoMax"), НУЛЬ); PrototypeAutoMax.Set(истина);
	нов(PrototypeShowValues, НУЛЬ, S("ShowValues"), НУЛЬ); PrototypeShowValues.Set(ложь);
	нов(PrototypeStyle, НУЛЬ, S("Style"), НУЛЬ); PrototypeStyle.Set(StyleAuto);
кон InitProtoTypes;

проц FindIndexOf*(flag : цел32; ds : DatasetDescriptor) : размерМЗ;
перем index, length : размерМЗ;
нач
	если (ds # НУЛЬ) то
		index := 0; length := длинаМассива(ds);
		нцПока (index < length) и (~(flag в ds[index].flags) или (Hidden в ds[index].flags)) делай увел(index); кц;
		если (index = length) то index := Invalid; всё;
	иначе
		index := Invalid;
	всё;
	возврат index;
кон FindIndexOf;

проц GetNumberOf*(flag : цел32; startIndex, endIndex : размерМЗ; ds : DatasetDescriptor) : размерМЗ;
перем result, i : размерМЗ;
нач
	result := 0;
	если (ds # НУЛЬ) и (0 <= startIndex) и (startIndex <= endIndex) и ((endIndex < длинаМассива(ds)) или (endIndex = матМаксимум(размерМЗ))) то
		если (endIndex = матМаксимум(размерМЗ)) то endIndex := длинаМассива(ds) - 1; всё;
		нцДля i := startIndex до endIndex делай
			если (flag в ds[i].flags) то увел(result); всё;
		кц;
	всё;
	возврат result;
кон GetNumberOf;

проц ClearFlag*(flag : цел32; ds : DatasetDescriptor);
перем i : размерМЗ;
нач
	если (ds # НУЛЬ) то
		нцДля i := 0 до длинаМассива(ds)-1 делай
			исключиИзМнваНаБитах(ds[i].flags, flag);
		кц;
	всё;
кон ClearFlag;

проц RMin(a, b : вещ32) : вещ32;
нач
	если a < b то возврат a; иначе возврат b; всё;
кон RMin;

проц RMax(a, b : вещ32) : вещ32;
нач
	если a> b то возврат a; иначе возврат b; всё;
кон RMax;

проц CopyDatasetDescriptor*(ds : DatasetDescriptor) : DatasetDescriptor;
перем result : DatasetDescriptor; i : размерМЗ;
нач
	если ds # НУЛЬ то
		нов(result, длинаМассива(ds));
		нцДля i := 0 до длинаМассива(ds)-1 делай
			копируйСтрокуДо0(ds[i].name, result[i].name);
			result[i].color := ds[i].color;
			result[i].flags := ds[i].flags;
		кц;
	всё;
	возврат result;
кон CopyDatasetDescriptor;

проц Cleanup;
кон Cleanup;

нач
	InitStrings;
	InitProtoTypes;
	Modules.InstallTermHandler(Cleanup);
кон WMDiagramComponents.

System.Free WMDiagramComponents ~

