модуль UTF8Strings;	(** pjm *)

(** UTF-8 encoded 0X-terminated UCS-32 strings. *)

(* Based on UTFStrings.Mod of TF and ISOStrings.Mod of jaco *)

использует Strings, UCS32;

конст
	CmpLess* = -1; CmpEqual* = 0; CmpGreater* = 1;	 CmpError* = 2; (** results for Compare. *)
	Симв32Нулевой = симв32ИзКода(0); (* подобное есть и в UCS32 *)

(** -- Help procedures -- *)

(** Encode one UCS-32 value in ucs into one well-formed UTF-8 character at str[p..]. The parameter p is incremented by the number of
	characters needed to encode the UCS-32 value. Sm takzhe Streams.Writer.UTF8Char, UCS32(s).CharJQvUTF8 *)
проц EncodeChar*(Сим: симв32; перем str: массив из симв8; перем i: размерМЗ): булево;
перем len, j: размерМЗ; byte, mask, max: цел16;
	buf: массив 6 из симв8; ucs : цел32;
нач
	ucs := кодСимв32(Сим);
	len := длинаМассива(str);

	если (ucs <= 7FH) то
		если (i + 1 < len) то str[i] := симв8ИзКода(устарПреобразуйКБолееУзкомуЦел(ucs));
			str[i+1] := 0X;
			увел(i)
		иначе
			str[i] := 0X;
			возврат ложь всё
	иначе
		byte := 0; mask := 7F80H; max := 3FH;

		нцПока (ucs > max) делай
			buf[byte] := симв8ИзКода(80H + устарПреобразуйКБолееУзкомуЦел(ucs остОтДеленияНа 40H)); увел(byte); (* CHR(80H + SHORT(AND(ucs, 3FH))) *)
			ucs := ucs DIV 64; (* LSH(ucs, -6) *)
			mask := mask DIV 2; (* 80H + LSH(mask, -1). Left-most bit remains set after DIV (mask is negative) *)
			max := max DIV 2 (* LSH(max, -1) *)	кц;
		buf[byte] := симв8ИзКода(mask + устарПреобразуйКБолееУзкомуЦел(ucs));

		если (i + byte + 1 < len) то
			нцДля j := 0 до byte делай
				str[i + j] := buf[byte - j] кц;
			str[i+byte+1] := 0X;
			i := i + byte + 1
		иначе
			str[i] := 0X;
			возврат ложь всё всё;
	возврат истина кон EncodeChar;

(** Decode one UTF-8 character at str[i..] and return its UCS-32 value in ucs.
  Return TRUE iff decoding succeeds. On error, returns FALSE and a replacement char.
  In attempt to read past the end of line, returns 0 and FALSE. Sm takzhe
  Streams.Reader.UTF8Char, UCS32.UTF8CharJQIzMassiva *)

проц DecodeChar*(конст str: массив из симв8; перем i: размерМЗ; перем рез: симв32): булево;
перем len: размерМЗ; ch, min: цел32; ucs: цел32;
нач
	если i < длинаМассива(str) то
		ch := устарПреобразуйКБолееШирокомуЦел(кодСимв8(str[i]));
		если ch < 80H то	(* ASCII *)
			рез := симв32ИзКода(ch); увел(i);
			возврат истина
		иначе
			просей UCS32.CodeLength[ch] из
				2X: ucs := ch остОтДеленияНа 20H; len := 2; min := 80H
				|3X: ucs := ch остОтДеленияНа 10H; len := 3; min := 800H
				|4X: ucs := ch остОтДеленияНа 8; len := 4; min := 10000H
				|5X: ucs := ch остОтДеленияНа 4; len := 5; min := 200000H
				|6X: ucs := ch остОтДеленияНа 2; len := 6; min := 4000000H
				иначе (* Hence CodeCharBad *)
					рез := UCS32.ReplacementChar; увел(i); возврат ложь
			всё;
			нц
				увел(i); умень(len);
				если len = 0 то рез := симв32ИзКода(ucs); возврат ucs >= min всё;
				если i = длинаМассива(str) то прервиЦикл всё;
				ch := устарПреобразуйКБолееШирокомуЦел(кодСимв8(str[i]));
				если арифмСдвиг(ch, -6) # 2 то прервиЦикл всё;
				ucs := арифмСдвиг(ucs, 6) + ch остОтДеленияНа 40H
			кц;
			рез := UCS32.ReplacementChar; возврат ложь;
		всё
	всё;
	рез := UCS32.ZeroChar;
	возврат ложь
кон DecodeChar;

(** Check whether str is a 0X-terminated well-formed UTF-8 string. *)

проц Valid*(конст str: массив из симв8): булево;
перем i: размерМЗ; ucs: симв32;
нач
	i := 0;
	нцПока DecodeChar(str, i, ucs) делай
		если ucs = Симв32Нулевой то возврат истина всё
		(* i already incremented *)
	кц;
	возврат ложь
кон Valid;

(** Return the size of a string in bytes, excluding the terminating 0X.  Another way of seeing this is that the function returns the byte offset of the terminating 0X. *)

проц Size*(конст str: массив из симв8): размерМЗ;
перем i: размерМЗ;
нач
	i := 0; нцПока str[i] # 0X делай увел(i) кц;
	возврат i
кон Size;

(** Set the size (in bytes) of a UTF-8 string by setting the terminating 0X.  If the specified size is larger than LEN(str)-1, the size will be set to this.  If the 0X falls in the middle of a UTF-8 character, the string is truncated before this character. *)

проц SetSize*(перем str: массив из симв8; size: размерМЗ);
перем i: размерМЗ;
нач
	если size > длинаМассива(str)-1 то size := длинаМассива(str)-1 всё;
	если size > 0 то	(* make sure last character is valid *)
		i := size-1;	(* last byte of last character *)
		если str[i] >= 80X то	(* starting or middle byte *)
			нцПока арифмСдвиг(устарПреобразуйКБолееШирокомуЦел(кодСимв8(str[i])), -6) = 2 делай умень(i) кц;	(* find starting byte *)
			если i + кодСимв8(UCS32.CodeLength[кодСимв8(str[i])]) > size то size := i всё	(* if not complete, truncate *)
		всё
	всё;
	str[size] := 0X
кон SetSize;

(** Return the byte offset of the UTF-8 character with index idx, counting from 0.  If idx is past the end of the string, return the offset of the terminating 0X, and if idx is negative, return 0.  This is also the definition of an UTF-8 index used by the other procedures. *)

проц OffsetOfIndex*(конст str: массив из симв8; idx: размерМЗ): размерМЗ;
перем i: размерМЗ; ch: цел32;
нач
	i := 0;
	нц	(* find position *)
		если idx <= 0 то прервиЦикл всё;
		ch := устарПреобразуйКБолееШирокомуЦел(кодСимв8(str[i]));
		если ch = 0 то прервиЦикл всё;
		умень(idx); увел(i, устарПреобразуйКБолееШирокомуЦел(кодСимв8(UCS32.CodeLength[ch])))
	кц;
	возврат i
кон OffsetOfIndex;

(** -- String manipulation. -- *)

(** Return the number of UTF-8 characters in str, excluding the terminating 0X. *)

проц Length*(конст str: массив из симв8): размерМЗ;
перем i, len: размерМЗ; ch: цел32;
нач
	i := 0; len := 0;
	нц	(* count characters *)
		ch := устарПреобразуйКБолееШирокомуЦел(кодСимв8(str[i]));
		если ch = 0 то прервиЦикл всё;
		увел(i, устарПреобразуйКБолееШирокомуЦел(кодСимв8(UCS32.CodeLength[ch])));
		увел(len)
	кц;
	возврат len
кон Length;

(** Copy src to dst.  Unlike COPY, ensure that dst is not truncated in the middle of a UTF-8 character.*)

проц Assign*(конст src: массив из симв8; перем dst: массив из симв8);
перем i: размерМЗ; ch: симв8;
нач
	i := 0;
	нц	(* copy characters *)
		ch := src[i];
		если (ch = 0X) или (i = длинаМассива(dst)) то прервиЦикл всё;
		dst[i] := ch; увел(i)
	кц;
	SetSize(dst, i)
кон Assign;

(** Copy at most num UTF-8 characters from src to dst, starting at UTF-8 index idx.  If num is negative, treat it as 0.  *)

проц Extract*(конст src: массив из симв8; idx, num: размерМЗ; перем dst: массив из симв8);
перем i, j: размерМЗ; ch: симв8;
нач
	i := OffsetOfIndex(src, idx); j := 0;
	нц	(* copy characters *)
		ch := src[i];
		если (ch = 0X) или (j = длинаМассива(dst)) то прервиЦикл всё;
		если арифмСдвиг(устарПреобразуйКБолееШирокомуЦел(кодСимв8(ch)), -6) # 2 то умень(num) всё;	(* about to copy a starting byte *)
		если num < 0 то прервиЦикл всё;
		dst[j] := ch; увел(i); увел(j)
	кц;
	SetSize(dst, j)
кон Extract;

(** Delete num UTF-8 characters from str, starting at UTF-8 index idx.  The characters following the deleted characters are shifted down to index idx.  If less than num characters are present at idx, delete all characters up to the end of the string. *)

проц Delete*(перем str: массив из симв8; idx, num: размерМЗ);
перем i, j: размерМЗ; ch: симв8;
нач
	i := OffsetOfIndex(str, idx); j := i;
	нц	(* skip over deleted characters *)
		ch := str[j];
		если (num <= 0) или (ch = 0X) то прервиЦикл всё;
		увел(j, кодСимв8(UCS32.CodeLength[кодСимв8(ch)]));
		умень(num)
	кц;
	нц	(* copy remaining characters over *)
		str[i] := ch;
		если ch = 0X то прервиЦикл всё;
		увел(i); увел(j);
		ch := str[j]
	кц
кон Delete;

(** Append src to dst. *)

проц Append*(конст src: массив из симв8; перем dst: массив из симв8);
перем i, j: размерМЗ; ch: симв8;
нач
	i := 0; j := Size(dst);
	нц	(* copy characters *)
		ch := src[i];
		если (ch = 0X) или (j = длинаМассива(dst)) то прервиЦикл всё;
		dst[j] := ch; увел(i); увел(j)
	кц;
	SetSize(dst, j)
кон Append;

(* Computes the string size with making sure that the last character is valid *)

проц ValidSize(конст str: массив из симв8): размерМЗ;
перем i, size: размерМЗ;
нач
	size := Size(str);
	если size > 0 то	(* make sure last character is valid *)
		i := size-1;	(* last byte of last character *)
		если str[i] >= 80X то	(* starting or middle byte *)
			нцПока арифмСдвиг(устарПреобразуйКБолееШирокомуЦел(кодСимв8(str[i])), -6) = 2 делай умень(i) кц;	(* find starting byte *)
			если i + кодСимв8(UCS32.CodeLength[кодСимв8(str[i])]) > size то size := i всё	(* if not complete, truncate *)
		всё
	всё;
	возврат size;
кон ValidSize;

(** Concatenate src2 onto src1 and copy the result to dst. *)

проц Concat*(конст src1, src2: массив из симв8; перем dst: массив из симв8);
перем n1, n2: размерМЗ;
нач
	n1 := ValidSize(src1);
	n2 := ValidSize(src2);
	Strings.Move(src2, 0, n2, dst, n1);
	Strings.Move(src1, 0, n1, dst, 0);
	SetSize(dst,n1+n2);
кон Concat;

(** Insert src into dst at UTF-8 index idx.  The characters from idx onwards are shifted up to make space for the inserted characters. *)

проц Insert*(конст src: массив из симв8; idx: размерМЗ; перем dst: массив из симв8);
перем i, j, m, n: размерМЗ; ch: симв8;
нач
		(* make space in dst for src *)
	n := Size(src); m := Size(dst);
	j := OffsetOfIndex(dst, idx); i := m-1;
	нцПока i >= j делай	(* move dst[j..] up n bytes *)
		если i+n < длинаМассива(dst) то dst[i+n] := dst[i] всё;
		умень(i)
	кц;
		(* copy src into space *)
	i := 0;
	нц
		ch := src[i];
		если (ch = 0X) или (j = длинаМассива(dst)) то прервиЦикл всё;
		dst[j] := ch; увел(i); увел(j)
	кц;
	SetSize(dst, m+n)
кон Insert;

(*
operation too obscure
(** Replace the characters of dst starting at UTF-8 index idx with the characters from src. *)

PROCEDURE Replace*(src: ARRAY OF CHAR; idx: SIZE; VAR dst: ARRAY OF CHAR);
BEGIN
	Delete(dst, idx, Length(src)); Insert(src, idx, dst)
END Replace;
*)

(** Transform a well-formed UTF-8 string into an 8-bit ASCII representation. Characters not present in the ASCII format are replaced
	by 'substitute' if it is # 0X. Вообще процедурка левоватая, неясно, зачем она нужна. *)

проц UTF8toASCII*(конст src: массив из симв8; substitute: симв8; перем dst: массив из симв8): размерМЗ;
перем count, i, len, pos: размерМЗ; ucs: симв32;
нач
	len := длинаМассива(dst); ucs := UCS32.ReplacementChar (* а было -1, так что код поменялся! *);
	нцПока (ucs # Симв32Нулевой) и DecodeChar(src, pos, ucs) и (i < len) делай
		если (ucs # UCS32.ReplacementChar) и (кодСимв32(ucs) < 100H) то dst[i] := симв8ИзКода(кодСимв32(ucs)); увел(i)
		аесли (substitute # 0X) то dst[i] := substitute; увел(i); увел(count)
		всё
	кц;
	возврат count
кон UTF8toASCII;

(** Transform an 8-bit ASCII string into a well-formed UTF-8 string. *)

проц ASCIItoUTF8*(конст ascii: массив из симв8; перем utf8: массив из симв8);
перем i,j: размерМЗ; dummy: булево;
нач
	i := 0; j := 0;
	нцПока (ascii[i] # 0X) и EncodeChar(симв32ИзКода(кодСимв8(ascii[i])), utf8, j) делай увел(i) кц;
	dummy := EncodeChar(Симв32Нулевой, utf8, j)
кон ASCIItoUTF8;

(** Transform a well-formed UTF-8 string into an array of UCS-32 values, ucs[idx...]. The UCS-32 representation is terminated with a 0 value.
	The parameter idx points to the next free entry after the conversion. *)

проц UTF8toUnicode*(конст utf8: массив из симв8; перем ucs: массив из симв32; перем idx: размерМЗ);
перем p, l: размерМЗ;
нач
	p := 0; l := длинаМассива(ucs)-1;
	нцПока (idx < l) и DecodeChar(utf8, p, ucs[idx]) и (кодСимв32(ucs[idx]) > 0)  делай увел(idx) кц;
	ucs[idx] := Симв32Нулевой; увел(idx)
кон UTF8toUnicode;

(** Transform a 4-byte, 0-terminated unicode character 'ucs' into a well-formed UTF-8 representation.
  См. также UCS32(u).COPYJQvU8 *)

проц UnicodetoUTF8*(конст ucs: массив из симв32; перем utf8: массив из симв8);
перем b: булево; i, p, l: размерМЗ;
нач
	b := истина; i := 0; p := 0; l := длинаМассива(ucs);
	нцПока (i < l) и b делай
		b := EncodeChar(ucs[i], utf8, p);
		увел(i)
	кц
кон UnicodetoUTF8;

(** Convert all simple lower-case letters in a well-formed UTF-8 string to upper case. The resulting string is still in UTF-8 form *)

проц UpperCase*(конст src: массив из симв8; перем dst: массив из симв8);
перем i: размерМЗ;
нач
	нцПока (src[i] # 0X) делай
		если (src[i] >= "a") и (src[i] <= "z") то dst[i] := ASCII_вЗаглавную(src[i]) (* this works because non-ASCII chars have bit 7 set *)
		иначе dst[i] := src[i]
		всё;
		увел(i)
	кц;
	dst[i] := 0X
кон UpperCase;

(** -- Test procedures -- *)

(** Return TRUE iff Assign can be performed without truncation. *)

проц CanAssign*(конст src, dst : массив из симв8): булево;
нач
	возврат Size(src)+1 <= длинаМассива(dst)
кон CanAssign;

(** todo: Return TRUE iff Extract can be performed without truncation, and src contains at least num UTF-8 characters. *)

проц CanExtract*(конст src: массив из симв8; idx, num: размерМЗ; конст dst: массив из симв8): булево;
нач
	СТОП(99)
кон CanExtract;

(** todo: Return TRUE iff there are num UTF-8 characters to delete at UTF-8 index idx in str. *)

проц CanDelete*(конст str: массив из симв8; idx, num: размерМЗ);
нач
	СТОП(99)
кон CanDelete;

(** todo: Return TRUE iff Append can be performed without truncation. *)

проц CanAppend*(конст src: массив из симв8; конст dst: массив из симв8): булево;
нач
	СТОП(99)
кон CanAppend;

(** todo: Return TRUE iff Concat can be performed without truncation. *)

проц CanConcat*(конст src1, src2, dst : массив из симв8): булево;
нач
	СТОП(99)
кон CanConcat;

(** todo: Return TRUE iff Insert can be performed without truncation. *)

проц CanInsert*(конст src: массив из симв8; idx: размерМЗ; конст dst: массив из симв8): булево;
нач
	СТОП(99)
кон CanInsert;

(*
(** todo: Return TRUE iff Replace can be performed without truncation. *)

PROCEDURE CanReplace*(src: ARRAY OF CHAR; idx: SIZE; VAR dst: ARRAY OF CHAR): BOOLEAN;
BEGIN
	HALT(99)
END CanReplace;
*)

(** -- Comparison and searching. -- *)

(** Compare str1 and str2 lexically, and return CmpLess if str1 is less than str2, CmpEqual if str1 and str2 are equal, or CmpGreater if str1 is greater than str2. *)

проц Compare*(конст str1, str2: массив из симв8): целМЗ;
перем i: размерМЗ; ch: симв8;
нач
	i := 0;
	нц
		ch := str1[i];
		если ch # str2[i] то прервиЦикл всё;
		если ch = 0X то возврат CmpEqual всё;
		увел(i)
	кц;
	если ch < str2[i] то возврат CmpLess иначе возврат CmpGreater всё
кон Compare;

проц CompareToUnicode*(конст utf8 : массив из симв8; конст unicode : массив из симв32) : целМЗ;
перем len, i, j: размерМЗ; char, ucs: симв32; result: целМЗ; valid, abort : булево;
нач
	len := длинаМассива(unicode);
	i := 0; j := 0; valid := истина; abort := ложь;
	нцПока valid и ~abort и (j < len) делай
		valid := DecodeChar(utf8, i, char);
		ucs := unicode[j];
		abort := (char # ucs) или (char = UCS32.ZeroChar);
		увел(j);
	кц;
	если valid то
		если (char = UCS32.ZeroChar) и (ucs = UCS32.ZeroChar) то result := CmpEqual;
		аесли (char < ucs) то result := CmpLess;
		иначе result := CmpGreater;
		всё;
	иначе
		result := CmpError;
	всё;
	возврат result;
кон CompareToUnicode;

(*
rather use Oberon = operator
(** Return TRUE iff str1 and str2 are lexically equal. *)

PROCEDURE Equal*(str1, str2: ARRAY OF CHAR): BOOLEAN;
VAR i: SIZE; ch: CHAR;
BEGIN
	i := 0;
	LOOP
		ch := str1[i];
		IF ch # str2[i] THEN RETURN FALSE END;
		IF ch = 0X THEN RETURN TRUE END;
		INC(i)
	END
END Equal;
*)

(** todo: Search forward for the next occurrance of UTF-8 string pat in UTF-8 string str, starting at UTF-8 index startidx.  Return found = TRUE iff the pattern was found.  If found, patidx is the UTF-8 index where the pattern was found, otherwise it is unchanged. *)

проц FindNext*(конст pat, str: массив из симв8; startidx: размерМЗ; перем found: булево; перем patidx: размерМЗ);
нач
	СТОП(99)
кон FindNext;

(** todo: Search backward for the previous occurrance of UTF-8 string pat in UTF-8 string str, starting at UTF-8 index startidx.  Return found = TRUE iff the pattern was found.  If found, patidx is the UTF-8 index where the pattern was found (in the range 0..startidx), otherwise it is unchanged. *)

проц FindPrev*(конст pat, str: массив из симв8; startidx: размерМЗ; перем found: булево; перем patidx: размерМЗ);
нач
	СТОП(99)
кон FindPrev;

(** Compare str1 and str2 and return different = TRUE iff a difference was found.  If the strings are different, idx is set to the UTF-8 index of the first mismatching character, otherwise it is unchanged. *)

проц FindDiff*(конст str1, str2: массив из симв8; перем different: булево; перем idx: размерМЗ);
перем i, j: размерМЗ; ch: симв8;
нач
	i := 0; j := -1;	(* i is byte position, j is current character index *)
	нц
		ch := str1[i];
		если арифмСдвиг(устарПреобразуйКБолееШирокомуЦел(кодСимв8(ch)), -6) # 2 то увел(j) всё;	(* about to compare a starting byte *)
		если ch # str2[i] то
			different := истина; idx := j;
			прервиЦикл
		всё;
		если ch = 0X то
			different := ложь;
			прервиЦикл
		всё;
		увел(i)
	кц
кон FindDiff;

кон UTF8Strings.

(**
Notes:

This module manages UCS-32 0X-terminated character strings encoded as multi-byte UTF-8 strings.  The UTF-8 encoding is decribed in RFC2279.

A CHAR value in a UTF-8 string can have one of three roles.  First, it can be a 7-bit ASCII character directly encoded as one byte.  Second, it can be the starting byte of a UCS-32 character encoded as 2 to 6 bytes.  Third, it can be a non-starting byte of a UCS-32 character encoded as 2 to 6 bytes.

The role of a CHAR ch is encoded in its top two bits, as follows:
	ASH(ORD(ch), -6) < 2, role is ASCII character (can also test ch < 80X).
	ASH(ORD(ch), -6) = 2, role is non-starting byte of a multi-byte sequence.
	ASH(ORD(ch), -6) = 3, role is starting byte of a multi-byte sequence.

The CodeLength string can be used to find the length of an encoding.  Assuming ch is the starting byte of an encoding, ORD(CodeLength[ORD(ch)]) is the total number of bytes in the encoding.  If ch is not a starting byte, this expression will return 7, indicating an error.

All string input parameters (except in DecodeChar, Valid and SetSize) are assumed to be 0X-terminated, well-formed UTF-8 strings.  All string output parameters produced are also 0X-terminated, well-formed UTF-8 strings.  It is assumed that the LEN of all ARRAY OF CHAR parameters is positive.  Violations of these assumptions may cause run-time exceptions, but not endless loops or memory corruption.

In a secure network application, UTF-8 strings received over the network MUST first be validated, and only used if found to be valid.  The reason is some invalid encodings can be used to code characters in alternate ways, which may bypass security checks, or cause other problems.  See the RFC for more details.

All the procedures truncate the destination string at a UTF-8 character boundary if enough space is not available.  A CanX function can be used to check whether truncation will occur if operation X is performed with the specified parameters.

String constants in Oberon programs are not necessarily well-formed UTF-8 strings, unless they contain only ASCII characters (below 80X).

The Oberon built-in procedure COPY does not necessarily produce a well-formed UTF-8 string, because it can truncate the destination string in the middle of a multi-byte character.  Rather use the Assign procedure from this module.

The Oberon string comparison operators <, <=, =, >=, > can be used on UTF-8 strings for lexical comparisons.
*)

(*
o assume indexes are inside string?  except lengths
*)

Backup.WriteFiles UTF8Strings.Mod ~
