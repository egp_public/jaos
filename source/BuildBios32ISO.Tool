# This script is used to generate a bootable ISO image for A2
#
# Path: In this example, the destination path for the object files and all files generated (ZIP, ISO) is ../Test/.
# This folder must exist. You can change it by search all occurences of "../Test/" and replace them by your preferred path..
#
# To create the CD image, do the following three steps:

# Step 1: Compile modules and generate ZIP packages

Release.Build --path="../Test/" --build --zip --xml A2 ~

# Step 3b: Create image for bootable CD (A2mini.iso)

System.DoCommands

FSTools.Mount RAMDISK RamFS 2000000 4096 ~

VirtualDisks.Create RAMDISK:A2.Dsk 80000 512 ~
VirtualDisks.Install -c=80 -h=2 -s=18 -b=512 VDISK0 RAMDISK:A2.Dsk ~

Partitions.Format VDISK0#0 AosFS 1024 ../Test/CD.Bin ~
VirtualDisks.Uninstall VDISK0 ~
IsoImages.Make ../Test/A2mini.iso RAMDISK:A2.Dsk ~
FSTools.CloseFiles ../Test/A2mini.iso ~
FSTools.Unmount RAMDISK ~

~
