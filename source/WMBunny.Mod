модуль WMBunny;	(** AUTHOR "TF"; PURPOSE "Visual active objects"; *)

использует
	Commands, WMMessages, WMGraphics, Modules,
	Raster, Random, Kernel, ЛогЯдра, Rectangles := WMRectangles,
	WM := WMWindowManager;

конст
	Width = 1024; Height = 768;	(* temp hack *)

тип
	KillerMsg = окласс
	кон KillerMsg;

	Bunny = окласс (WM.Window)
	перем
		posX, posY : размерМЗ;
		alive : булево;
		timer : Kernel.Timer;
		random : Random.Generator;
		nofFrames, frame, step : цел32;
		movie : Raster.Image;
		keyPressed : мнвоНаБитахМЗ;

		проц &New*(movie : Raster.Image; frames, step : цел32);
		нач
			bounds := Rectangles.MakeRect(0, 0, movie.width DIV frames, movie.height);
			useAlpha := истина;
			isVisible := истина;
			нов(random);
			nofFrames := frames; frame := 0; сам.movie := movie; сам.step := step;
			нов(timer);
			posX := (posX+ step) остОтДеленияНа (Width * 2 + movie.width);
			posY := random.Dice((Height - movie.height)(цел32) DIV 2);
			manager := WM.GetDefaultManager();
			manager.Add(posX, posY, сам, {});
		кон New;

		проц Move;
		перем opx : размерМЗ;
		нач
			opx := posX;
			posX := (posX+ step) остОтДеленияНа (Width * 2 + movie.width);
			если ((step < 0) и (posX > opx)) или ((step > 0) и (posX < opx)) то
				posY := random.Dice((Height - movie.height)(цел32))
			всё;
			frame := (frame + 1) остОтДеленияНа nofFrames;
			manager.SetWindowPos(сам, posX - movie.width, posY)
		кон Move;

		проц {перекрыта}PointerDown*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
		нач
			keyPressed := keyPressed + (keys * {0, 1, 2})
		кон PointerDown;

		проц {перекрыта}PointerUp*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
		нач
			если keys={} то
				если keyPressed#{0, 1, 2} то
					если keyPressed={0} то manager.ToFront(сам)
					аесли keyPressed={0, 2} то Close всё
				всё;
				keyPressed := {}
			всё
		кон PointerUp;

		проц {перекрыта}Draw*(canvas : WMGraphics.Canvas; w, h : размерМЗ; q : целМЗ);
		нач
			если movie # НУЛЬ то
				если (w = GetWidth()) и (h = GetHeight()) то
					canvas.ScaleImage(movie, Rectangles.MakeRect(frame * GetWidth(), 0, (frame + 1) * GetWidth(), GetHeight()),
						Rectangles.MakeRect(0, 0, w, h), WMGraphics.ModeSrcOverDst, 0);
				иначе
					canvas.ScaleImage(movie, Rectangles.MakeRect(frame * GetWidth(), 0, (frame + 1) * GetWidth(), GetHeight()),
						Rectangles.MakeRect(0, 0, w, h), WMGraphics.ModeSrcOverDst, q)
				всё
			всё
		кон Draw;

		проц {перекрыта}IsHit*(x, y  : размерМЗ) : булево;
		нач
			возврат WMGraphics.IsBitmapHit(frame * GetWidth() + x, y, 128, movie)
		кон IsHit;

		проц {перекрыта}Close*;
		нач alive := ложь
		кон Close;

		проц {перекрыта}Handle*(перем x : WMMessages.Message);
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) и (x.ext суть KillerMsg) то Close
			иначе Handle^(x)
			всё
		кон Handle;

	нач {активное}
		alive := истина;
		нцПока alive делай timer.Sleep(100); Move кц;
	выходя
		manager.Remove(сам);
		DecCount;
	кон Bunny;

перем
	nofWindows : цел32;

проц IncCount;
нач {единолично}
	увел(nofWindows)
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows)
кон DecCount;

проц Free*;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WM.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WM.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0);
кон Free;

проц InternalInsert(конст fname : массив из симв8; frames, step : цел32);
перем b : Bunny;
	img : Raster.Image;
нач {единолично}
	img := WMGraphics.LoadImage(fname, истина);
	если img # НУЛЬ то
		нов(b, img, frames, step)
	иначе
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8(fname); ЛогЯдра.пСтроку8(" not found."); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё
кон InternalInsert;

проц Insert*(context : Commands.Context);
перем fn : массив 65 из симв8;	(* a filename equivalent to Files.FileName - do not import Files *)

	(* Assign an integer value to a variable: extracted from the command parameter if present or
		else a default one *)
	проц SetVal(def : цел32) : цел32;
	перем int : цел32;
	нач
		int := def;
		context.arg.ПропустиБелоеПоле;
		если (context.arg.ПодглядиСимв8() >= '0') и (context.arg.ПодглядиСимв8() <= '9') или (context.arg.ПодглядиСимв8() = '-') то
			context.arg.чЦел32(int, ложь);
		всё;
		возврат int;
	кон SetVal;

нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fn);
	IncCount;
	InternalInsert(fn, SetVal(8), SetVal(32));
кон Insert;

проц Cleanup;
нач
	Free;
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон WMBunny.

WMBunny.Insert WMBunnyImages.tar://BunnyLinear.gif 8 40 ~
WMBunny.Insert WMBunnyImages.tar://bones.gif 25 10 ~
WMBunny.Insert WMBunnyImages.tar://phantom.png 25 10 ~
WMBunny.Insert WMBunnyImages.tar://SisiphusLinear.gif 5 8 ~
WMBunny.Insert WMBunnyImages.tar://frog.gif 17 -4 ~
WMBunny.Insert WMBunnyImages.tar://aos1.gif 25 -15 ~
WMBunny.Insert WMBunnyImages.tar://aos2.gif 25 10 ~
WMBunny.Insert WMBunnyImages.tar://aos3.gif 25 15 ~

WMBunny.Free~
System.Free WMBunny
