(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль CalcD1;   (** AUTHOR "adf"; PURPOSE "Computes a first-order derivative"; *)

использует NbrInt, NbrRe, NbrCplx, MathRe, CalcFn;

конст
	(** Admissible parameters to be passed for establishing the differencing scheme used to compute a derivative. *)
	Forward* = 9;  Central* = 10;  Backward* = 11;

перем
	epsilon, zero: NbrRe.Real;

	(* Force the argument in and out of addressable memory to minimize round-off error. *)
	проц DoNothing( x: NbrRe.Real );
	кон DoNothing;

	проц DoCplxNothing( z: NbrCplx.Complex );
	кон DoCplxNothing;

	(** Computes  df(x)/dx *)
	проц Solve*( f: CalcFn.ReArg;  atX: NbrRe.Real;  differencing: NbrInt.Integer ): NbrRe.Real;
	перем h, hOpt, hMin, power, result, temp: NbrRe.Real;
	нач
		(*  Select an optimum step size.  See v5.7 on Numerical Derivatives in Press et al., Numerical Recipes. *)
		power := 4 / 5;  hMin := MathRe.Power( NbrRe.Epsilon, power );  power := 1 / 3;
		hOpt := NbrRe.Abs( atX ) * MathRe.Power( epsilon, power );  h := NbrRe.Max( hOpt, hMin );
		(* Refine h so that  x + h and x differ by an exactly representable number in memory. *)
		temp := atX + h;  DoNothing( temp );  h := temp - atX;
		(* Compute an approximate value for the derivative. *)
		если differencing = Forward то
			result := f( atX + h );
			result := (result - f( atX )) / h
		аесли differencing = Backward то
			result := f( atX );
			result := (result - f( atX - h )) / h
		иначе  (* differencing = Central *)
			result := f( atX + h );
			result := (result - f( atX - h )) / (2 * h)
		всё;
		возврат result
	кон Solve;

	(** Computes  df(z)/dz *)
	проц SolveCplx*( f: CalcFn.CplxArg;  atZ: NbrCplx.Complex;  differencing: NbrInt.Integer ): NbrCplx.Complex;
	перем h, hOpt, hMin, power: NbrRe.Real;  ch, result, temp: NbrCplx.Complex;
	нач
		(*  Select an optimum step size. *)
		power := 4 / 5;  hMin := MathRe.Power( NbrRe.Epsilon, power );  power := 1 / 3;
		hOpt := NbrCplx.Abs( atZ ) * MathRe.Power( epsilon, power );  h := NbrRe.Max( hOpt, hMin );
		NbrCplx.Set( h, h, ch );
		(* Refine h so that  z + ch and z differ by an exactly representable number in memory. *)
		temp := atZ + ch;  DoCplxNothing( temp );  ch := temp - atZ;
		(* Compute an approximate value for the derivative. *)
		если differencing = Forward то
			result := f( atZ + ch );
			result := (result - f( atZ )) / ch
		аесли differencing = Backward то
			result := f( atZ );
			result := (result - f( atZ - ch )) / ch
		иначе  (* differencing = Central *)
			result := f( atZ + ch );
			result := (result - f( atZ - ch )) / (2 * ch)
		всё;
		возврат result
	кон SolveCplx;

	(** Computes  6f(z)/6x,  z = x + i y  *)
	проц SolveCplxRe*( f: CalcFn.CplxArg;  atZ: NbrCplx.Complex;  differencing: NbrInt.Integer ): NbrCplx.Complex;
	перем h, hOpt, hMin, power: NbrRe.Real;  ch, result, temp: NbrCplx.Complex;
	нач
		(*  Select an optimum step size. *)
		power := 4 / 5;  hMin := MathRe.Power( NbrRe.Epsilon, power );  power := 1 / 3;
		hOpt := NbrCplx.Abs( atZ ) * MathRe.Power( epsilon, power );  h := NbrRe.Max( hOpt, hMin );
		NbrCplx.Set( h, zero, ch );
		(* Refine h so that  z + ch and z differ by an exactly representable number in memory. *)
		temp := atZ + ch;  DoCplxNothing( temp );  ch := temp - atZ;
		(* Compute an approximate value for the derivative. *)
		если differencing = Forward то
			result := f( atZ + ch );
			result := (result - f( atZ )) / ch
		аесли differencing = Backward то
			result := f( atZ );
			result := (result - f( atZ - ch )) / ch
		иначе  (* differencing = Central *)
			result := f( atZ + ch );
			result := (result - f( atZ - ch )) / (2 * ch)
		всё;
		возврат result
	кон SolveCplxRe;

	(** Computes  6f(z)/6y,  z = x + i y  *)
	проц SolveCplxIm*( f: CalcFn.CplxArg;  atZ: NbrCplx.Complex;  differencing: NbrInt.Integer ): NbrCplx.Complex;
	перем h, hOpt, hMin, power: NbrRe.Real;  ch, result, temp: NbrCplx.Complex;
	нач
		(*  Select an optimum step size. *)
		power := 4 / 5;  hMin := MathRe.Power( NbrRe.Epsilon, power );  power := 1 / 3;
		hOpt := NbrCplx.Abs( atZ ) * MathRe.Power( epsilon, power );  h := NbrRe.Max( hOpt, hMin );
		NbrCplx.Set( zero, h, ch );
		(* Refine h so that  z + ch and z differ by an exactly representable number in memory. *)
		temp := atZ + ch;  DoCplxNothing( temp );  ch := temp - atZ;
		(* Compute an approximate value for the derivative. *)
		если differencing = Forward то
			result := f( atZ + ch );
			result := (result - f( atZ )) / ch
		аесли differencing = Backward то
			result := f( atZ );
			result := (result - f( atZ - ch )) / ch
		иначе  (* differencing = Central *)
			result := f( atZ + ch );
			result := (result - f( atZ - ch )) / (2 * ch)
		всё;
		возврат result
	кон SolveCplxIm;

	(** Computes  6f(z)/6r,  z = r exp( i f )  *)
	проц SolveCplxAbs*( f: CalcFn.CplxArg;  atZ: NbrCplx.Complex;  differencing: NbrInt.Integer ): NbrCplx.Complex;
	перем h, hOpt, hMin, power: NbrRe.Real;  ch, result, temp: NbrCplx.Complex;
	нач
		(*  Select an optimum step size. *)
		power := 4 / 5;  hMin := MathRe.Power( NbrRe.Epsilon, power );  power := 1 / 3;
		hOpt := NbrCplx.Abs( atZ ) * MathRe.Power( epsilon, power );  h := NbrRe.Max( hOpt, hMin );
		NbrCplx.SetPolar( h, zero, ch );
		(* Refine h so that  z + ch and z differ by an exactly representable number in memory. *)
		temp := atZ + ch;  DoCplxNothing( temp );  ch := temp - atZ;
		(* Compute an approximate value for the derivative. *)
		если differencing = Forward то
			result := f( atZ + ch );
			result := (result - f( atZ )) / ch
		аесли differencing = Backward то
			result := f( atZ );
			result := (result - f( atZ - ch )) / ch
		иначе  (* differencing = Central *)
			result := f( atZ + ch );
			result := (result - f( atZ - ch )) / (2 * ch)
		всё;
		возврат result
	кон SolveCplxAbs;

	(** Computes  6f(z)/6f,  z = r exp( i f )  *)
	проц SolveCplxArg*( f: CalcFn.CplxArg;  atZ: NbrCplx.Complex;  differencing: NbrInt.Integer ): NbrCplx.Complex;
	перем h, hOpt, hMin, power: NbrRe.Real;  ch, result, temp: NbrCplx.Complex;
	нач
		(*  Select an optimum step size. *)
		power := 4 / 5;  hMin := MathRe.Power( NbrRe.Epsilon, power );  power := 1 / 3;
		hOpt := NbrCplx.Arg( atZ ) * MathRe.Power( epsilon, power );  h := NbrRe.Max( hOpt, hMin );
		NbrCplx.SetPolar( zero, h, ch );
		(* Refine h so that  z + ch and z differ by an exactly representable number in memory. *)
		temp := atZ + ch;  DoCplxNothing( temp );  ch := temp - atZ;
		(* Compute an approximate value for the derivative. *)
		если differencing = Forward то
			result := f( atZ + ch );
			result := (result - f( atZ )) / ch
		аесли differencing = Backward то
			result := f( atZ );
			result := (result - f( atZ - ch )) / ch
		иначе  (* differencing = Central *)
			result := f( atZ + ch );
			result := (result - f( atZ - ch )) / (2 * ch)
		всё;
		возврат result
	кон SolveCplxArg;

нач
	epsilon := 100 * NbrRe.Epsilon;  zero := 0
кон CalcD1.
