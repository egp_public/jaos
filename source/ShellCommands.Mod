модуль ShellCommands; (* ejz,  *)
использует ЭВМ, Потоки, Pipes, AosModules := Modules, Files, Dates, Strings, Commands;

конст
	MaxString = 256;

тип
	String = массив MaxString из симв8;

	CmdAlias = укль на запись
		alias, cmd, help: String;
		next: CmdAlias
	кон;

	CmdParameters = укль на массив из симв8;

	AliasList = окласс
		перем alias: CmdAlias;

		проц Alias(alias, cmd, help: массив из симв8);
			перем a: CmdAlias;
		нач {единолично}
			a := сам.alias;
			нцПока (a # НУЛЬ) и (a.alias # alias) делай
				a := a.next
			кц;
			если a = НУЛЬ то
				нов(a); a.next := сам.alias; сам.alias := a; копируйСтрокуДо0(alias, a.alias)
			всё;
			копируйСтрокуДо0(cmd, a.cmd); копируйСтрокуДо0(help, a.help)
		кон Alias;

		проц Find(alias: массив из симв8): CmdAlias;
			перем a: CmdAlias;
		нач {единолично}
			a := сам.alias;
			нцПока (a # НУЛЬ) и (a.alias # alias) делай
				a := a.next
			кц;
			возврат a
		кон Find;

		проц List(out: Потоки.Писарь);
			перем a: CmdAlias;
		нач {единолично}
			a := alias;
			нцПока a # НУЛЬ делай
				out.пСтроку8(a.alias); out.пСимв8(09X); out.пСтроку8(a.cmd); out.пВК_ПС();
				если a.help # "" то
					out.пСимв8(09X); out.пСтроку8(a.help); out.пВК_ПС()
				всё;
				a := a.next
			кц
		кон List;

		проц &Init*;
		нач
			alias := НУЛЬ;
			Alias("alias", "ShellCommands.Alias", "alias [ [ alias cmd ] help ]");
			Alias("del", "ShellCommands.Delete", "del { file }");
			Alias("dir", "ShellCommands.Directory", "dir [ pattern ]");
			Alias("echo", "ShellCommands.Echo", "echo { par }");
			Alias("exit", "ShellCommands.Exit", "exit");
			Alias("free", "ShellCommands.Free", "free mod");
			Alias("help", "ShellCommands.Help", "help [ alias ]");
			Alias("mods", "ShellCommands.Modules", "mods");
			Alias("start", "ShellCommands.Start", "start cmd { pars }");
			Alias("ver", "ShellCommands.Version", "ver")
		кон Init;
	кон AliasList;

	Context* = окласс (Commands.Context)
	перем
		alias: AliasList;
		C: динамическиТипизированныйУкль;

		проц &New*(C: динамическиТипизированныйУкль; in: Потоки.Чтец; out, err: Потоки.Писарь);
		нач
			Init(in, НУЛЬ, out, err, НУЛЬ);
			сам.C := C; alias := НУЛЬ
		кон New;

	кон Context;

	Command = окласс
	перем
		ctx: Context;
		cmd: String;
		next: Command;

		проц SetContext(C: динамическиТипизированныйУкль; in: Потоки.Чтец; out, err: Потоки.Писарь);
		перем a: AliasList;
		нач
			если (C # ctx.C) или (in # ctx.in) или (out # ctx.out) или (err # ctx.error) то
				a := ctx.alias;
				нов(ctx, C, in, out, err);
				ctx.alias := a
			всё
		кон SetContext;

		проц &Init*(ctx: Context);
		нач
			утв(ctx # НУЛЬ);
			сам.ctx := ctx; cmd := ""; next := НУЛЬ
		кон Init;

	кон Command;

проц GetPar(par: динамическиТипизированныйУкль; перем p: CmdParameters; перем w: Потоки.Писарь);
перем arg : Потоки.ЧтецИзСтроки; len, length : размерМЗ;
нач
	p := НУЛЬ; w := НУЛЬ;
	если (par # НУЛЬ) и (par суть Commands.Context) и (par(Commands.Context).arg суть Потоки.ЧтецИзСтроки) то
		arg := par(Commands.Context).arg (Потоки.ЧтецИзСтроки);
		len := arg.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество();
		если (len > 0) то
			нов(p, len);
			arg.чБайты(p^, 0, len, length);
			w := par(Commands.Context).out;
		всё;
	всё
кон GetPar;

проц GetAliasList(p: Commands.Context): AliasList;
нач
	если (p # НУЛЬ) и (p суть Context) то
		возврат p(Context).alias
	всё;
	возврат НУЛЬ
кон GetAliasList;

проц Close(p: Commands.Context);
перем ctx: Context;
нач
	если (p # НУЛЬ) и (p суть Context) то
		ctx := p(Context);
		если ctx.C суть Pipes.Pipe то
			ctx.out.ПротолкниБуферВПоток(); ctx.C(Pipes.Pipe).Close()
		всё
	всё
кон Close;

проц Alias*(context : Commands.Context);
перем al: AliasList; alias, cmd, help: String;
нач
	al := GetAliasList(context);
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(alias) и context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(cmd) то
		context.out.пСтроку8(alias); context.out.пСимв8(09X); context.out.пСтроку8(cmd); context.out.пВК_ПС;
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(help) то
			context.out.пСимв8(09X); context.out.пСтроку8(help); context.out.пВК_ПС()
		иначе help := "";
		всё;
		al.Alias(alias, cmd, help);
	иначе
		al.List(context.out)
	всё;
	Close(context);
кон Alias;

проц Delete*(context : Commands.Context);
перем filename: Files.FileName; res: целМЗ;
нач
	нцПока context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename) делай
		context.out.пСтроку8(filename); context.out.пСимв8(09X);
		Files.Delete(filename, res);
		если res = Files.Ok то
			context.out.пСтроку8("done");
		иначе
			context.out.пСтроку8("error: "); context.out.пЦел64(res, 0)
		всё;
		context.out.пВК_ПС;
	кц;
	Close(context);
кон Delete;

проц Directory*(context : Commands.Context);
перем
	enum: Files.Enumerator;
	name: Files.FileName; flags: мнвоНаБитахМЗ; time, date: цел32; size: Files.Size; tdrec: Dates.DateTime; str: массив 32 из симв8;
нач
	если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(str) то str := "*"; всё;
	нов(enum);
	enum.Open(str, {Files.EnumSize, Files.EnumTime});
	нцПока enum.GetEntry(name, flags, time, date, size) делай
		context.out.пСтроку8(name); context.out.пСимв8(09X); context.out.пЦел64(size, 0); context.out.пСимв8(09X);
		tdrec := Dates.OberonToDateTime(date, time);
		Strings.TimeToStr(tdrec, str);
		context.out.пСтроку8(str); context.out.пВК_ПС()
	кц;
	Close(context);
кон Directory;

проц Echo*(context : Commands.Context);
перем in : Потоки.Чтец;
нач
	если (context.arg.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() > 0) то
		in := context.arg;
	иначе
		in := context.in;
	всё;
	Потоки.СкопируйВсёОтЧтецаКПисарю (in, context.out);
	Close(context);
кон Echo;

проц Exit*(context : Commands.Context);
перем ctx: Context;
нач
	если (context # НУЛЬ) и (context суть Context) то
		context.out.пВК_ПС(); context.out.пСтроку8("logout"); context.out.пВК_ПС();
		ctx := context(Context);
		если ctx.C суть Потоки.ФункциональныйИнтерфейсКПотоку то
			ctx.C(Потоки.ФункциональныйИнтерфейсКПотоку).Закрой()
		всё
	всё;
	Close(context);
кон Exit;

проц Free*(context : Commands.Context);
перем name: AosModules.Name; msg: String; res: целМЗ;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name) то
		context.out.пСтроку8(name); context.out.пСимв8(09X);
		AosModules.FreeModule(name, res, msg);
		если res = 0 то
			context.out.пСтроку8("done")
		иначе
			context.out.пЦел64(res, 0); context.out.пСтроку8(": "); context.out.пСтроку8(msg)
		всё;
		context.out.пВК_ПС;
	всё;
	Close(context);
кон Free;

проц Help*(context : Commands.Context);
перем name: String; al: AliasList; a: CmdAlias;
нач
	если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name) то name := "help" всё;
	al := GetAliasList(context);
	a := al.Find(name);
	если a # НУЛЬ то
		context.out.пСтроку8(a.help);
	иначе
		context.out.пСтроку8(name); context.out.пСимв8(09X); context.out.пСтроку8("no such alias");
	всё;
	context.out.пВК_ПС;
	Close(context);
кон Help;

проц Modules*(context : Commands.Context);
перем mod: AosModules.Module;
нач
	mod := AosModules.root;
	нцПока mod # НУЛЬ делай
		context.out.пСтроку8(mod.name); context.out.пСимв8(09X);
		context.out.пЦел64(mod.refcnt, 0); context.out.пВК_ПС();
		mod := mod.next
	кц;
	Close(context);
кон Modules;

(*
	cmdline = cmdpar { "|" cmdpar } [ ( ">" | ">>" ) file ] .
	cmdpar = cmd { par } [ "<" file ] .
*)
проц execute(context : Commands.Context; перем cmdline: массив из симв8; flags: мнвоНаБитахМЗ; перем res: целМЗ; перем msg: массив из симв8);
перем
	ctx: Context; R: Потоки.ЧтецИзСтроки; cmd, prev, cmds: Command; i, j, p, ofs, len: размерМЗ; ch, filter: симв8;
	in, pR: Потоки.Чтец; out, err, pW: Потоки.Писарь; file: Files.FileName; F, newF: Files.File; fR: Files.Reader;
	fW: Files.Writer; pipe: Pipes.Pipe; a: CmdAlias; cmdString : укль на массив из симв8;
нач
	ctx := context(Context);
	len := длинаМассива(cmdline); newF := НУЛЬ; cmds := НУЛЬ; prev := НУЛЬ;
	если ctx.alias = НУЛЬ то нов(ctx.alias) всё; ofs := 0;
	in := ctx.in; out := ctx.out; err := ctx.error; pipe := НУЛЬ;
	нов(R, len); R.ПримиСтроку8ДляЧтения(cmdline); R.ПропустиБелоеПоле();
	нц
		нов(cmd, ctx); R.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(cmd.cmd);
		если prev # НУЛЬ то
			prev.next := cmd;
			Потоки.НастройЧтеца(pR, pipe.Receive)
		иначе
			cmds := cmd; pR := in
		всё;
		prev := cmd;
		R.ПропустиБелоеПоле(); p := ofs + размерМЗ(R.МестоВПотоке());
		если p < len то
			i := p; ch := cmdline[i];
			нцПока (ch # 0X) и (ch # "|") и (ch # "<") и (ch # ">") делай
				увел(i); ch := cmdline[i]
			кц;
			filter := ch
		иначе
			i := len; filter := 0X
		всё;
		если p < i то
			нов(cmdString, i-p+2); j := 0;
			ch := cmdline[p]; j := 0;
			нцПока (ch # 0X) и (p < i) делай
				cmdString[j] := ch; увел(j);
				увел(p); ch := cmdline[p]
			кц;
			cmdString[j] := " "; увел(j);
			cmdString[j] := 0X;

			если (cmd.ctx.arg суть Потоки.ЧтецИзСтроки) то
				cmd.ctx.arg(Потоки.ЧтецИзСтроки).ПримиСрезСтроки8ВСтрокуДляЧтения(cmdString^, 0, j +1);
			всё;
		иначе
			cmdString := НУЛЬ
		всё;
		просей filter из
		"|": ofs := i+1; R.ПримиСрезСтроки8ВСтрокуДляЧтения(cmdline, ofs, len-ofs);
				нов(pipe, 1024);
				Потоки.НастройПисаря(pW, pipe.Send);
				cmd.SetContext(pipe, pR, pW, err)
		|"<": ofs := i+1; R.ПримиСрезСтроки8ВСтрокуДляЧтения(cmdline, ofs, len-ofs);
				R.ПропустиБелоеПоле(); R.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(file);
				R.ПропустиБелоеПоле(); R.чСимв8(ch);
				если ch = "|" то
					нов(pipe, 1024);
					Потоки.НастройПисаря(pW, pipe.Send)
				иначе
					pipe := НУЛЬ
				всё;
				F := Files.Old(file);
				если F # НУЛЬ то
					Files.OpenReader(fR, F, 0);
					если pR # in то
						res := -1; копируйСтрокуДо0("invalid command syntax", msg); возврат
					всё;
					если pipe # НУЛЬ то
						cmd.SetContext(pipe, fR, pW, err)
					иначе
						cmd.SetContext(ctx.C, fR, out, err)
					всё
				иначе
					res := -1; копируйСтрокуДо0("input file not found", msg); возврат
				всё;
				если pipe = НУЛЬ то
					R.ПропустиБелоеПоле();
					если R.кодВозвратаПоследнейОперации # Потоки.КонецФайла то
						res := -1; копируйСтрокуДо0("invalid command syntax", msg); возврат
					всё;
					прервиЦикл
				всё
		|">": если cmdline[i+1] = ">" то
					ofs := i+2; R.ПримиСрезСтроки8ВСтрокуДляЧтения(cmdline, ofs, len-ofs);
					R.ПропустиБелоеПоле(); R.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(file);
					F := Files.Old(file);
					если F # НУЛЬ то
						Files.OpenWriter(fW, F, F.Length());
						cmd.SetContext(ctx.C, pR, fW, err)
					иначе
						res := -1; копируйСтрокуДо0("ouput file not found", msg); возврат
					всё
				иначе
					ofs := i+1; R.ПримиСрезСтроки8ВСтрокуДляЧтения(cmdline, ofs, len-ofs);
					R.ПропустиБелоеПоле(); R.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(file);
					F := Files.New(file);
					если F # НУЛЬ то
						Files.OpenWriter(fW, F, 0);
						cmd.SetContext(ctx.C, pR, fW, err);
						newF := F
					иначе
						res := -1; копируйСтрокуДо0("ouput file not created", msg); возврат
					всё
				всё;
				R.ПропустиБелоеПоле();
				если R.кодВозвратаПоследнейОперации # Потоки.КонецФайла то
					res := -1; копируйСтрокуДо0("invalid command syntax", msg); возврат
				всё;
				прервиЦикл
		иначе
			cmd.SetContext(ctx.C, pR, out, err); прервиЦикл
		всё;
		R.ПропустиБелоеПоле();
		если R.кодВозвратаПоследнейОперации # Потоки.Успех то
			res := -1; копируйСтрокуДо0("invalid command syntax", msg); возврат
		всё
	кц;
	prev := НУЛЬ; cmd := cmds;
	нцПока cmd # НУЛЬ делай
		a := ctx.alias.Find(cmd.cmd);
		если a # НУЛЬ то копируйСтрокуДо0(a.cmd, cmd.cmd) всё;
		если cmd.next = НУЛЬ то
			Commands.Activate(cmd.cmd, cmd.ctx, flags, res, msg)
		иначе
			Commands.Activate(cmd.cmd, cmd.ctx, {}, res, msg)
		всё;
		если res # 0 то возврат всё;
		prev := cmd; cmd := cmd.next
	кц;
	если newF # НУЛЬ то
		prev.ctx.out.ПротолкниБуферВПоток();
		Files.Register(newF)
	всё
кон execute;

проц Start*(context : Commands.Context);
перем msg: String; len: размерМЗ; res: целМЗ; string : укль на массив из симв8;
нач
	если (context.arg суть Потоки.ЧтецИзСтроки) и (context.arg.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() > 0) то
		нов(string, context.arg.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() +1);
		context.arg.чБайты(string^, 0, context.arg.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество(), len);
		string[длинаМассива(string)-1] := 0X;
		execute(context, string^, {}, res, msg);
		если res # 0 то
			context.out.пЦел64(res, 0); context.out.пСтроку8(": "); context.out.пСтроку8(msg); context.out.пВК_ПС()
		всё
	всё;
	Close(context);
кон Start;

проц Version*(context : Commands.Context);
нач
	context.out.пСтроку8(ЭВМ.версияЯОС); context.out.пВК_ПС;
	Close(context);
кон Version;

проц Execute*(par: Commands.Context;  перем cmdline: массив из симв8; перем res: целМЗ; перем msg: массив из симв8);
нач
	execute(par, cmdline, {Commands.Wait}, res, msg)
кон Execute;

кон ShellCommands.
