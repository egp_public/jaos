модуль WMRepositories; (** AUTHOR "staubesv"; PURPOSE "Repository inspection tool"; *)

использует
	Modules, Kernel, ЛогЯдра, Commands, Strings, XML, XMLObjects, Repositories, Texts,
	Raster, WMRectangles, WMMessages, WMRestorable, WMGraphics, WMWindowManager, WMProperties, WMComponents, WMStandardComponents,
	WMEditors, WMGrids, WMStringGrids, WMDialogs, WMDropTarget;

конст
	WindowWidth = 640; WindowHeight = 480;

	State_NotInitialized = 0;
	State_Running = 1;
	State_Finalizing = 9;
	State_Finalized = 10;

тип

	(* Drag'n'Drop for components *)

	AddComponentProcedure* = проц {делегат} (component : Repositories.Component; перем res : целМЗ);

	DropTarget* = окласс(WMDropTarget.DropTarget)
	перем
		originator : динамическиТипизированныйУкль;
		addComponent : AddComponentProcedure;

		проц &Init*(originator : динамическиТипизированныйУкль; addComponent : AddComponentProcedure);
		нач
			утв(addComponent # НУЛЬ);
			сам.originator := originator;
			сам.addComponent := addComponent;
		кон Init;

		проц {перекрыта}GetInterface*(type : цел32) : WMDropTarget.DropInterface;
		перем dc : DropComponent;
		нач
			если (type = WMDropTarget.TypeUser) то
				нов(dc, originator, addComponent); возврат dc;
			иначе
				возврат НУЛЬ;
			всё;
		кон GetInterface;

	кон DropTarget;

	DropComponent* = окласс(WMDropTarget.DropInterface)
	перем
		originator : динамическиТипизированныйУкль;
		addComponent : AddComponentProcedure;

		проц &Init*(originator : динамическиТипизированныйУкль; addComponent : AddComponentProcedure);
		нач
			утв(addComponent # НУЛЬ);
			сам.originator := originator;
			сам.addComponent := addComponent;
		кон Init;

		проц Set(component : Repositories.Component; перем res : целМЗ);
		нач
			addComponent(component, res);
		кон Set;

	кон DropComponent;

тип

	RepositoriesView* = окласс(WMComponents.VisualComponent)
	перем
		grid- : WMStringGrids.StringGrid;

		repositories : Repositories.Repositories;

		lastTimestamp : цел32;
		state : цел32;

		проц &{перекрыта}Init*;
		перем i : размерМЗ;
		нач
			Init^;
			нов(repositories, 20);
			нцДля i := 0 до длинаМассива(repositories)-1 делай repositories := НУЛЬ; кц;
			InitGrid;
			lastTimestamp := Repositories.GetTimestamp();
			state := State_NotInitialized;
		кон Init;

		проц {перекрыта}Initialize*;
		нач
			Initialize^;
			нач {единолично} state := State_Running; кон;
		кон Initialize;

		проц SelectByRepository*(repository : Repositories.Repository);
		перем row, nofRows, selectRow : размерМЗ; data : динамическиТипизированныйУкль;
		нач
			selectRow:= -1;
			если (repository # НУЛЬ) то
				grid.Acquire;
				grid.model.Acquire;
				nofRows := grid.model.GetNofRows();
				row := 0;
				нцПока (selectRow = -1) и (row < nofRows) делай
					data := grid.model.GetCellData(0, row);
					если (data = repository) то
						selectRow := row;
					всё;
					увел(row);
				кц;
				grid.model.Release;
				grid.Release;
			всё;
			grid.SetSelection(0, selectRow, 0, selectRow);
		кон SelectByRepository;

		проц InitGrid;
		нач
			нов(grid); grid.alignment.Set(WMComponents.AlignClient);
			grid.SetSelectionMode(WMGrids.GridSelectSingleRow);
			grid.alwaysShowScrollX.Set(ложь); grid.showScrollX.Set(истина);
			grid.alwaysShowScrollY.Set(ложь); grid.showScrollY.Set(истина);
			grid.Acquire;
			grid.model.Acquire;
			grid.model.SetNofCols(1);
			grid.model.SetNofRows(0);
			grid.model.Release;
			grid.Release;
			AddContent(grid);
		кон InitGrid;

		проц UpdateGrid;
		перем nofRepositories, row : цел32;

			проц CountRepositories(repositories : Repositories.Repositories) : цел32;
			перем nofRepositories : цел32;
			нач
				nofRepositories := 0;
				если (repositories # НУЛЬ) то
					нцПока (nofRepositories < длинаМассива(repositories)) и (repositories[nofRepositories] # НУЛЬ) делай
						увел(nofRepositories);
					кц;
				всё;
				возврат nofRepositories;
			кон CountRepositories;

		нач
			Repositories.GetAll(repositories);
			nofRepositories := CountRepositories(repositories);
			grid.Acquire;
			grid.model.Acquire;
			grid.model.SetNofRows(nofRepositories);
			нцДля row := 0 до nofRepositories-1 делай
				grid.model.SetCellText(0, row, Strings.NewString(repositories[row].name));
				grid.model.SetCellData(0, row, repositories[row]);
			кц;
			grid.model.Release;
			grid.Release;
		кон UpdateGrid;

		проц {перекрыта}Finalize*;
		перем ignore : цел32;
		нач
			Finalize^;
			нач {единолично} state := State_Finalizing; кон;
			Repositories.IncrementTimestamp(ignore); (* Unblock Repositories.AwaitChange in body *)
			нач {единолично} дождись(state = State_Finalized); кон;
		кон Finalize;

	нач {активное}
		нач {единолично} дождись(state > State_NotInitialized); кон;
		нцПока (state = State_Running) делай
			UpdateGrid;
			Repositories.AwaitChange(lastTimestamp);
		кц;
		нач {единолично} state := State_Finalized; кон;
	кон RepositoriesView;

тип

	EntryWrapper* = окласс
	перем
		repository- : Repositories.Repository;
		element- : XML.Element;

		проц &Init*(repository : Repositories.Repository; конст element : XML.Element);
		нач
			сам.repository := repository;
			сам.element := element;
		кон Init;

	кон EntryWrapper;

	RepositoryView* = окласс(WMComponents.VisualComponent)
	перем
		showDetails- : WMProperties.BooleanProperty;
		showDetailsI : булево;

		grid- : WMStringGrids.StringGrid;
		spacings : WMGrids.Spacings;

		thisRepository : Repositories.Repository;

		enum : XMLObjects.Enumerator;
		draggedComponent : Repositories.Component;

		lastTimestamp, lastRepositoryTimestamp : цел32;
		state : цел32;

		проц &{перекрыта}Init*;
		нач
			Init^;
			нов(showDetails, НУЛЬ, НУЛЬ, НУЛЬ); properties.Add(showDetails);
			showDetails.Set(истина);
			showDetailsI := showDetails.Get();
			InitGrid;
			thisRepository := НУЛЬ;
			enum := НУЛЬ;
			draggedComponent := НУЛЬ;
			lastTimestamp := Repositories.GetTimestamp();
			lastRepositoryTimestamp := 0;
			state := State_NotInitialized;
		кон Init;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		нач
			если (property = showDetails) то
				showDetailsI := showDetails.Get();
				SetDetails(showDetailsI);
				Invalidate;
			иначе
				PropertyChanged^(sender, property);
			всё;
		кон PropertyChanged;

		проц {перекрыта}Initialize*;
		нач
			Initialize^;
			нач {единолично} state := State_Running; кон;
		кон Initialize;

		проц InitGrid;
		нач
			нов(grid); grid.alignment.Set(WMComponents.AlignClient);
			grid.onStartDrag.Add(MyStartDrag);
			grid.SetSelectionMode(WMGrids.GridSelectSingleRow);
			grid.adjustFocusPosition.Set(ложь);
			grid.alwaysShowScrollX.Set(ложь);
			grid.alwaysShowScrollY.Set(ложь);
			grid.allowColResize.Set(истина); grid.allowRowResize.Set(ложь);
			нов(spacings, 4); spacings[0] := 30; spacings[1] := 130; spacings[2] := 200; spacings[3] := 20;
			SetDetails(истина);
			AddContent(grid);
		кон InitGrid;

		проц GetSelectedComponent() : Repositories.Component;
		перем
			component : Repositories.Component; string : Strings.String; id : цел32;
			wrapper : EntryWrapper;
			scol, srow, ecol, erow : размерМЗ;
			ptr : динамическиТипизированныйУкль;
		нач
			component := НУЛЬ;
			wrapper := НУЛЬ;
			grid.Acquire;
			grid.model.Acquire;
			grid.GetSelection(scol, srow, ecol, erow);
			если (srow >= 0) и (srow = erow) то
				ptr := grid.model.GetCellData(0, srow);
				если (ptr # НУЛЬ) и (ptr суть EntryWrapper) то
					wrapper := ptr (EntryWrapper);
				всё;
			всё;
			grid.model.Release;
			grid.Release;
			если (wrapper # НУЛЬ) и (wrapper.repository # НУЛЬ) и (wrapper.element # НУЛЬ) то
				string := wrapper.element.GetAttributeValue("id");
				если (string # НУЛЬ) то Strings.StrToInt(string^, id); иначе id := 0; всё;
				string := wrapper.element.GetAttributeValue("name");
				если (string # НУЛЬ) то
					component := wrapper.repository.GetComponent(string^, id);
				всё;
			всё;
			возврат component;
		кон GetSelectedComponent;

		(* Drag away operations *)
		проц MyStartDrag(sender, data : динамическиТипизированныйУкль);
		перем
			component : Repositories.Component;
			vc : WMComponents.VisualComponent;
			name : Strings.String;
			bounds : WMRectangles.Rectangle;
			image : WMGraphics.Image; canvas : WMGraphics.BufferCanvas;
			font : WMGraphics.Font;
			w, h : размерМЗ;
		нач
			component := GetSelectedComponent();
			если (component # НУЛЬ) то
				image := НУЛЬ;
				если (component суть WMComponents.VisualComponent) то
					vc := component (WMComponents.VisualComponent);
					bounds := vc.bounds.Get();
					если (bounds.r - bounds.l < 10) то bounds.r := bounds.l + 100; vc.bounds.Set(bounds); всё;
					если (bounds.b - bounds.t < 10) то bounds.b := bounds.t + 50; vc.bounds.Set(bounds); всё;
					(* render to bitmap *)
					нов(image); Raster.Create(image, bounds.r - bounds.l, bounds.b - bounds.t, Raster.BGRA8888);
					нов(canvas, image);
					canvas.Fill(WMRectangles.MakeRect(0, 0, bounds.r-bounds.l, bounds.b-bounds.t), 0FF60H, WMGraphics.ModeSrcOverDst);
					vc.Draw(canvas);
				иначе
					name := component.GetName();
					если (name = НУЛЬ) то name := Strings.NewString("NoName"); всё;
					утв(name # НУЛЬ);
					w := 0; h := 0;
					font := GetFont();
					font.GetStringSize(name^, w, h);
					если (w =0) то w := 100 всё;
					если (h=0) то h := 50 всё;
					если (w > 0) и (h > 0) то
						(* render to bitmap *)
						нов(image); Raster.Create(image, w, h, Raster.BGRA8888);
						нов(canvas, image);
						canvas.Fill(WMRectangles.MakeRect(0, 0, w, h), 0FF60H, WMGraphics.ModeSrcOverDst);
						canvas.SetColor(WMGraphics.Black);
						WMGraphics.DrawStringInRect(canvas, WMRectangles.MakeRect(0, 0, w, h), ложь, WMGraphics.AlignCenter, WMGraphics.AlignCenter, name^)
					всё;
				всё;
				если ~StartDrag(component, image, 0,0,DragWasAccepted, НУЛЬ) то component := НУЛЬ; всё;
			всё;
			draggedComponent := component;
		кон MyStartDrag;

		проц DragWasAccepted(sender, data : динамическиТипизированныйУкль);
		перем di : WMWindowManager.DragInfo;
			dt : WMDropTarget.DropTarget;
			itf : WMDropTarget.DropInterface;
			dropText : WMDropTarget.DropText;
			op : Texts.ObjectPiece;
			res : целМЗ;
		нач
			если (draggedComponent = НУЛЬ) то возврат всё;

			если (data # НУЛЬ) и (data суть WMWindowManager.DragInfo) то
				di := data(WMWindowManager.DragInfo);
				если (di.data # НУЛЬ) и (di.data суть WMDropTarget.DropTarget) то
					dt := di.data(WMDropTarget.DropTarget);
					itf := dt.GetInterface(WMDropTarget.TypeUser);
					если (itf # НУЛЬ) и (itf суть DropComponent) то
						itf(DropComponent).Set(draggedComponent(Repositories.Component), res);
					иначе
						itf := dt.GetInterface(WMDropTarget.TypeText);
						если (itf # НУЛЬ) и (itf суть WMDropTarget.DropText) то
							dropText := itf(WMDropTarget.DropText);
							нов(op); op.object := draggedComponent;
							dropText.text.AcquireWrite;
							dropText.text.InsertPiece(dropText.pos.GetPosition(), op);
							dropText.text.ReleaseWrite;
						всё;
					всё;
				всё;
			всё;
		кон DragWasAccepted;

		проц SetDetails(details : булево);
		нач
			grid.Acquire;
			grid.model.Acquire;
			если details то
				grid.showScrollX.Set(истина);
				grid.showScrollY.Set(истина);
				grid.fixedRows.Set(1);
				grid.SetColSpacings(spacings);
				grid.model.SetNofCols(4);
				grid.model.SetNofRows(1);
				grid.model.SetCellText(0, 0, StrName);
				grid.model.SetCellText(1, 0, StrID);
				grid.model.SetCellText(2, 0, StrParameter);
				grid.model.SetCellText(3, 0, StrInstance);
			иначе
				grid.showScrollX.Set(ложь);
				grid.showScrollY.Set(истина);
				grid.SetColSpacings(НУЛЬ);
				grid.fixedRows.Set(0);
				grid.model.SetNofCols(1);
				grid.model.SetNofRows(0);
			всё;
			grid.model.Release;
			grid.Release;
		кон SetDetails;

		проц {перекрыта}Resized*;
		нач
			UpdateGridSpacings;
			Resized^;
		кон Resized;

		проц UpdateGridSpacings*;
		перем sum, w : размерМЗ; i: размерМЗ; rect : WMRectangles.Rectangle;
		нач
			rect := bounds.Get(); w := rect.r - rect.l;
			нцДля i := 0 до длинаМассива(spacings)-1 делай sum := sum + spacings[i]; кц;
			если w > sum то
				grid.Acquire;
				grid.model.Acquire;
				spacings[0] := spacings[0] + (w - sum) DIV 2;
				spacings[2] := spacings[2] + (w - sum) DIV 2 + (w - sum) остОтДеленияНа 2;
				grid.model.Release;
				grid.Release;
			аесли (w > 190) то
				spacings[0] := 130; spacings[1] := 30; spacings[2] := w - 180; spacings[3] := 20;
			иначе
				spacings[0] := 130; spacings[1] := 30; spacings[2] := 200; spacings[3] := 20;
			всё;
		кон UpdateGridSpacings;

		проц SetThisRepository*(repository : Repositories.Repository);
		перем ignore : цел32;
		нач
			сам.thisRepository := repository;
			если (repository # НУЛЬ) то
				lastTimestamp := Repositories.GetTimestamp();
				lastRepositoryTimestamp := repository.timestamp - 1;
			иначе
				lastRepositoryTimestamp := 0;
			всё;
			Repositories.IncrementTimestamp(ignore);
		кон SetThisRepository;

		проц UpdateGrid;
		перем
			repository : Repositories.Repository; wrapper : EntryWrapper;
			element : XML.Element; image : WMGraphics.Image;
			row, nofEntries : цел32;

			проц CountElements(enum : XMLObjects.Enumerator) : цел32;
			перем nofElements : цел32; ptr : динамическиТипизированныйУкль;
			нач
				утв(enum # НУЛЬ);
				nofElements := 0;
				нцПока enum.HasMoreElements() делай
					ptr := enum.GetNext();
					если (ptr суть XML.Element) то
						увел(nofElements);
					всё;
				кц;
				возврат nofElements;
			кон CountElements;

			проц GetNext(enum : XMLObjects.Enumerator) : XML.Element;
			перем element : XML.Element; ptr : динамическиТипизированныйУкль;
			нач
				element := НУЛЬ;
				нцПока (element = НУЛЬ) и (enum.HasMoreElements()) делай
					ptr := enum.GetNext();
					если (ptr суть XML.Element) то
						element := ptr (XML.Element);
					всё;
				кц;
				возврат element;
			кон GetNext;

			проц GetImage(element : XML.Element) : WMGraphics.Image;
			перем image : WMGraphics.Image; imageName : Strings.String;
			нач
				imageName := element.GetAttributeValue("image");
				если (imageName # НУЛЬ) то
					image := WMGraphics.LoadImage(imageName^, истина);
				иначе
					image := НУЛЬ;
				всё;
				возврат image;
			кон GetImage;

		нач
			repository := thisRepository;
			если ((repository = НУЛЬ) и (lastRepositoryTimestamp = 0)) или ((repository # НУЛЬ) и (repository.timestamp # lastRepositoryTimestamp)) то
				если (repository # НУЛЬ) то
					lastRepositoryTimestamp := repository.timestamp;
					enum := repository.GetComponentEnumerator();
					nofEntries := CountElements(enum);
					enum.Reset;
				иначе
					lastRepositoryTimestamp := 1;
					nofEntries := 0;
				всё;
				если (nofEntries > 0) то
					grid.Acquire;
					grid.model.Acquire;
					grid.model.SetNofRows(nofEntries + 1);
					enum.Reset;
					если showDetailsI то
						нцДля row := 1 до nofEntries делай
							element := GetNext(enum);
							grid.model.SetCellText(0, row, element.GetAttributeValue("name"));
							image := GetImage(element);
							grid.model.SetCellImage(0, row, image);
							grid.model.SetCellText(1, row, element.GetAttributeValue("id"));
							grid.model.SetCellText(2, row, element.GetAttributeValue("source"));
(*							IF (index[row-1].instance # NIL) THEN
								grid.model.SetCellText(3, row, StrYes);
							ELSE
								grid.model.SetCellText(3, row, StrNo);
							END; *)
							нов(wrapper, repository, element);
							grid.model.SetCellData(0, row, wrapper);
							grid.model.SetCellData(1, row, wrapper);
							grid.model.SetCellData(2, row, wrapper);
							grid.model.SetCellData(3, row, wrapper);
						кц;
					иначе
						нцДля row := 0 до nofEntries-1 делай
							element := GetNext(enum);
							grid.model.SetCellText(0, row, element.GetAttributeValue("name"));
							image := GetImage(element);
							grid.model.SetCellImage(0, row, image);
							нов(wrapper, repository, element);
							grid.model.SetCellData(0, row, wrapper);
						кц;
					всё;
					grid.model.Release;
					grid.Release;
				всё;
			всё;
		кон UpdateGrid;

		проц {перекрыта}Finalize*;
		перем ignore : цел32;
		нач
			Finalize^;
			нач {единолично} state := State_Finalizing; кон;
			Repositories.IncrementTimestamp(ignore); (* Unblock Repositories.AwaitChange in body *)
			нач {единолично} дождись(state = State_Finalized); кон;
		кон Finalize;

	нач {активное}
		нач {единолично} дождись(state > State_NotInitialized); кон;
		нцПока (state = State_Running) делай
			UpdateGrid;
			Repositories.AwaitChange(lastTimestamp);
		кц;
		нач {единолично} state := State_Finalized; кон;
	кон RepositoryView;


	KillerMsg = окласс
	кон KillerMsg;

	Window = окласс(WMComponents.FormWindow)
	перем
		repositories : RepositoriesView;
		repository : RepositoryView;

		loadBtn, storeBtn, unloadBtn : WMStandardComponents.Button;
		filenameEditor : WMEditors.Editor;
		statusLabel : WMStandardComponents.Label;

		opNum : цел32;

		проц &New(context : WMRestorable.Context);
		нач
			Init(WindowWidth, WindowHeight, ложь);
			IncCount;
			SetContent(CreateForm());
			repository.UpdateGridSpacings;
			SetTitle(Strings.NewString("Repositories"));
			SetIcon(WMGraphics.LoadImage("WMRepositories.tar://WMRepositories.png", истина));
			opNum := 1;
		кон New;

		проц CreateForm() : WMComponents.VisualComponent;
		перем panel, treePanel, toolbar : WMStandardComponents.Panel; resizer : WMStandardComponents.Resizer;
		нач
			нов(panel); panel.alignment.Set(WMComponents.AlignClient);
			panel.fillColor.Set(WMGraphics.White);

			нов(statusLabel); statusLabel.alignment.Set(WMComponents.AlignBottom);
			statusLabel.bounds.SetHeight(20);
			statusLabel.fillColor.Set(0CCCCCCCCH);
			statusLabel.caption.SetAOC("0: OK");
			panel.AddContent(statusLabel);

			нов(toolbar); toolbar.alignment.Set(WMComponents.AlignTop);
			toolbar.bounds.SetHeight(20);
			panel.AddContent(toolbar);

			нов(loadBtn); loadBtn.alignment.Set(WMComponents.AlignLeft);
			loadBtn.caption.SetAOC("Load");
			loadBtn.onClick.Add(HandleButtons);
			toolbar.AddContent(loadBtn);

			нов(filenameEditor); filenameEditor.alignment.Set(WMComponents.AlignClient);
			filenameEditor.tv.textAlignV.Set(WMGraphics.AlignCenter);
			filenameEditor.multiLine.Set(ложь);
			filenameEditor.tv.showBorder.Set(истина);
			toolbar.AddContent(filenameEditor);

			нов(unloadBtn); unloadBtn.alignment.Set(WMComponents.AlignRight);
			unloadBtn.caption.SetAOC("Unload");
			unloadBtn.onClick.Add(HandleButtons);
			toolbar.AddContent(unloadBtn);

			нов(storeBtn); storeBtn.alignment.Set(WMComponents.AlignRight);
			storeBtn.caption.SetAOC("Store");
			storeBtn.onClick.Add(HandleButtons);
			toolbar.AddContent(storeBtn);

			нов(treePanel); treePanel.alignment.Set(WMComponents.AlignLeft);
			treePanel.bounds.SetWidth(150);
			panel.AddContent(treePanel);

			нов(resizer); resizer.alignment.Set(WMComponents.AlignRight);
			resizer.bounds.SetWidth(5);
			treePanel.AddContent(resizer);

			нов(repositories); repositories.alignment.Set(WMComponents.AlignClient);
			treePanel.AddContent(repositories);
			repositories.grid.onClick.Add(OnRepositoriesClicked);

			нов(repository); repository.alignment.Set(WMComponents.AlignClient);
			panel.AddContent(repository);
			repository.grid.onClickSelected.Add(OnComponentClicked);

			возврат panel;
		кон CreateForm;

		проц SetStatusLabel(конст m1, m2, m3 : массив из симв8);
		перем caption : массив 256 из симв8; nbr : массив 8 из симв8;
		нач
			caption := " ";
			Strings.IntToStr(opNum, nbr); увел(opNum);
			Strings.Append(caption, nbr);
			Strings.Append(caption, ": ");
			Strings.Append(caption, m1);
			Strings.Append(caption, m2);
			Strings.Append(caption, m3);
			statusLabel.caption.SetAOC(caption);
		кон SetStatusLabel;

		проц LoadRepository(конст filename : массив из симв8);
		перем repository : Repositories.Repository; timer : Kernel.Timer;
		нач
			repository := Repositories.ThisRepository(filename);
			если (repository # НУЛЬ) то
				нов(timer); timer.Sleep(200);
				repositories.SelectByRepository(repository);
				SetStatusLabel("Repository '", filename, "' loaded");
			иначе
				SetStatusLabel("Repository '", filename, "' not found");
			всё;
		кон LoadRepository;

		проц HandleButtons(sender, data : динамическиТипизированныйУкль);
		перем filename : массив 256 из симв8; res : целМЗ;
		нач
			если (sender = loadBtn) то
			filenameEditor.GetAsString(filename);
				LoadRepository(filename);
			аесли (sender = storeBtn) то
				filenameEditor.GetAsString(filename);
				Repositories.StoreRepository(filename, res);
				если (res # Repositories.Ok) то
					SetStatusLabel("Could not store repository '", filename, "'");
					WMDialogs.Error("Error", "Could not store repository");
				иначе
					SetStatusLabel("Repository '", filename, "' stored");
				всё;
			аесли (sender = unloadBtn) то
				filenameEditor.GetAsString(filename);
				Repositories.UnloadRepository(filename, res);
				если (res # Repositories.Ok) то
					SetStatusLabel("Could not unload repository '", filename, "'");
					WMDialogs.Error("Error", "Could not unload repository");
				иначе
					SetStatusLabel("Repository '", filename, "' unloaded");
				всё;
			всё;
		кон HandleButtons;

		проц OnRepositoriesClicked(sender, data : динамическиТипизированныйУкль);
		нач
			если (data # НУЛЬ) и (data суть Repositories.Repository) то
				repository.SetThisRepository(data(Repositories.Repository));
				filenameEditor.SetAsString(data(Repositories.Repository).name);
			всё;
		кон OnRepositoriesClicked;

		проц OnComponentClicked(sender, data : динамическиТипизированныйУкль);
		перем command, msg : массив 384 из симв8; res : целМЗ; string : Strings.String;
		нач
			если (data # НУЛЬ) и (data суть EntryWrapper) то
				если (data(EntryWrapper).repository # НУЛЬ) и (data(EntryWrapper).element # НУЛЬ) то
					string := data(EntryWrapper).element.GetAttributeValue("source");
					если (string # НУЛЬ) то
						command := "PET.Open ";
						Strings.Append(command, data(EntryWrapper).repository.filename);
						Strings.Append(command, "://");
						Strings.Append(command, string^);
						Commands.Call(command, {}, res, msg);
						если (res # Commands.Ok) то ЛогЯдра.пСтроку8(msg); всё;
					всё;
				всё;
			всё;
		кон OnComponentClicked;

		проц {перекрыта}Handle*(перем x : WMMessages.Message);
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) то
				если (x.ext суть KillerMsg) то Close
				аесли (x.ext суть WMRestorable.Storage) то
					x.ext(WMRestorable.Storage).Add("WMRepositories", "WMRepositories.Restore", сам, НУЛЬ);
				иначе Handle^(x)
				всё
			иначе Handle^(x)
			всё
		кон Handle;

		проц {перекрыта}Close*;
		нач
			Close^;
			DecCount;
		кон Close;

	кон Window;

перем
	nofWindows : цел32;

	StrID, StrName, StrType, StrParameter, StrInstance : Strings.String;
	StrYes, StrNo : Strings.String;

проц Open*;
перем window : Window;
нач
	нов(window, НУЛЬ);
	WMWindowManager.AddWindow(window, 100, 100);
кон Open;

проц Restore*(context : WMRestorable.Context);
перем window : Window;
нач
	нов(window, context);
	WMRestorable.AddByContext(window, context);
кон Restore;

проц InitStrings;
нач
	StrID := Strings.NewString("ID");
	StrName := Strings.NewString("Name");
	StrType := Strings.NewString("Type");
	StrParameter := Strings.NewString("Parameter");
	StrInstance := Strings.NewString("Instance");
	StrYes := Strings.NewString("Yes");
	StrNo := Strings.NewString("No");
кон InitStrings;

проц IncCount;
нач {единолично}
	увел(nofWindows);
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows);
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WMWindowManager.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WMWindowManager.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0)
кон Cleanup;

нач
	nofWindows := 0;
	InitStrings;
	Modules.InstallTermHandler(Cleanup);
кон WMRepositories.

WMRepositories.Open ~

System.Free WMRepositories ~
