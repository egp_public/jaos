(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль CalcDiethelm;   (** AUTHOR "adf"; PURPOSE "Diethelm's algorithms for the fractional calculus"; *)

использует NbrRe, DataErrors, MathRe, MathGamma, CalcFn, CalcD1;

(**  Arguments of differ-integration are:
		f(x)		the function being differ-integrated,
		x		the argument of f, also the upper limit of differ-integration,
		order	(> 0)  the fractional order of differ-integration, i.e., a,
		tol		the requested and achieved error tolerances.

	Arguments for fractional-order differential equations are:
		y		the dependent variable, i.e., the function whose solution is being sought,
		y0		the vector of initial conditions
					if  0  < a <= 1 then y0[0] =  y(0+)
					if  1 < a < 2 then y0[0] =  y(0+) and y0[1] =  y'(0+),
		f(x,y)	the forcing function, i.e., the right-hand side of the differential equation,
		x		the independent variable,
		order	(0 < order < 2)  the fractional order of differentiation, i.e., a,
		tol		the requested and achieved error tolerances.
	*)

тип
	Romberg = окласс
	перем index: цел32;
		error, soln: NbrRe.Real;
		factors: укль на массив из NbrRe.Real;
		tableau: массив 8 из укль на массив из NbrRe.Real;

		проц & Initialize*;
		перем i: цел32;
		нач
			index := -1;  error := 0;  soln := 0;  нов( factors, 8 );
			нцДля i := 0 до 7 делай нов( tableau[i], i + 1 ) кц
		кон Initialize;

		проц Update( newSolution: NbrRe.Real );
		(* Insert a new row into the Romberg tableau and perform Richardson extrapolation on that row.
			If the tableau is full, then ratchet the first column up one row, throwing away the first entry,
			insert the newSolution into the last row, and perform Richardon extrapolation over the entire tablueau.  *)
		перем i: цел32;

			проц RichardsonExtrapolation;
			перем k: цел32;  term: NbrRe.Real;
			нач
				нцДля k := 1 до index делай
					term := tableau[index - 1, k - 1];  term := term - factors[k - 1] * tableau[index, k - 1];
					term := term / (1 - factors[k - 1]);  tableau[index, k] := term
				кц
			кон RichardsonExtrapolation;

		нач
			если index < 7 то
				(* The tableau is not full yet.  Add another row.  Build this row of the tableau. *)
				увел( index );  tableau[index, 0] := newSolution;
				если index > 0 то RichardsonExtrapolation всё
			иначе
				(* The tableau is full.  Ratchet back the rows.  Insert the newSolution.  And rebuild the entire tableau. *)
				нцДля i := 0 до 6 делай tableau[i, 0] := tableau[i + 1, 0] кц;
				tableau[7, 0] := newSolution;  index := 0;
				нцДля i := 1 до 7 делай увел( index );  RichardsonExtrapolation кц
			всё;
			(* Update the error. *)
			если index < 2 то error := NbrRe.MaxNbr
			иначе
				(* Compute an absolute error. *)
				error := NbrRe.Abs( tableau[index, index - 1] - tableau[index - 1, index - 1] );
				если NbrRe.Abs( tableau[index, index] ) > 1 то
					(* Compute its reletive error. *)
					error := NbrRe.Abs( error / tableau[index, index] )
				всё
			всё;
			(* Assign the enhanced solution. *)
			soln := tableau[index, index]
		кон Update;

	кон Romberg;

перем
	minTol, maxTol: NbrRe.Real;

	проц CreateDiffFactors( alpha: NbrRe.Real;  x: Romberg );
	(* Create the factors, i.e., 2^(r[k]), for Richardson extrapolation for differentiation.
		To be called after UpdateAWeights.  *)
	нач
		если alpha < 1 то
			x.factors[0] := MathRe.Power( 2, 2 - alpha );  x.factors[1] := 4;
			x.factors[2] := MathRe.Power( 2, 3 - alpha );  x.factors[3] := MathRe.Power( 2, 4 - alpha );
			x.factors[4] := 16;  x.factors[5] := MathRe.Power( 2, 5 - alpha );
			x.factors[6] := MathRe.Power( 2, 6 - alpha );  x.factors[7] := 64
		иначе
			(* These are guesses as to what they probably are.  Their actual values have not yet been derived. *)
			x.factors[0] := MathRe.Power( 2, 2 - alpha );  x.factors[1] := MathRe.Power( 2, 3 - alpha );
			x.factors[2] := 4;  x.factors[3] := MathRe.Power( 2, 4 - alpha );
			x.factors[4] := MathRe.Power( 2, 5 - alpha );  x.factors[5] := 16;
			x.factors[6] := MathRe.Power( 2, 6 - alpha );  x.factors[7] := MathRe.Power( 2, 7 - alpha )
		всё
	кон CreateDiffFactors;

	проц CreateIntFactors( alpha: NbrRe.Real;  x: Romberg );
	(* Create the factors, i.e., 2^(r[k]), for Richardson extrapolation for integration.
		To be called after UpdateCWeights.  *)
	нач
		если alpha < 1 то
			x.factors[0] := 4;  x.factors[1] := MathRe.Power( 2, 2 + alpha );  x.factors[2] := 8;
			x.factors[3] := MathRe.Power( 2, 3 + alpha );  x.factors[4] := 16;
			x.factors[5] := MathRe.Power( 2, 4 + alpha );  x.factors[6] := 32;
			x.factors[7] := MathRe.Power( 2, 5 + alpha )
		аесли alpha < 2 то
			x.factors[0] := 4;  x.factors[1] := 8;  x.factors[2] := MathRe.Power( 2, 2 + alpha );  x.factors[3] := 16;
			x.factors[4] := MathRe.Power( 2, 3 + alpha );  x.factors[5] := 32;
			x.factors[6] := MathRe.Power( 2, 4 + alpha );  x.factors[7] := 64
		аесли alpha < 3 то
			x.factors[0] := 4;  x.factors[1] := 8;  x.factors[2] := 16;  x.factors[3] := MathRe.Power( 2, 2 + alpha );
			x.factors[4] := 32;  x.factors[5] := MathRe.Power( 2, 3 + alpha );  x.factors[6] := 64;
			x.factors[7] := MathRe.Power( 2, 4 + alpha )
		аесли alpha < 4 то
			x.factors[0] := 4;  x.factors[1] := 8;  x.factors[2] := 16;  x.factors[3] := 32;
			x.factors[4] := MathRe.Power( 2, 2 + alpha );  x.factors[5] := 64;
			x.factors[6] := MathRe.Power( 2, 3 + alpha );  x.factors[7] := 128
		аесли alpha < 5 то
			x.factors[0] := 4;  x.factors[1] := 8;  x.factors[2] := 16;  x.factors[3] := 32;  x.factors[4] := 64;
			x.factors[5] := MathRe.Power( 2, 2 + alpha );  x.factors[6] := 128;
			x.factors[7] := MathRe.Power( 2, 3 + alpha )
		аесли alpha < 6 то
			x.factors[0] := 4;  x.factors[1] := 8;  x.factors[2] := 16;  x.factors[3] := 32;  x.factors[4] := 64;
			x.factors[5] := 128;  x.factors[6] := MathRe.Power( 2, 2 + alpha );  x.factors[7] := 256
		аесли alpha < 7 то
			x.factors[0] := 4;  x.factors[1] := 8;  x.factors[2] := 16;  x.factors[3] := 32;  x.factors[4] := 64;
			x.factors[5] := 128;  x.factors[6] := 256;  x.factors[7] := MathRe.Power( 2, 2 + alpha )
		иначе
			x.factors[0] := 4;  x.factors[1] := 8;  x.factors[2] := 16;  x.factors[3] := 32;  x.factors[4] := 64;
			x.factors[5] := 128;  x.factors[6] := 256;  x.factors[7] := 512
		всё
	кон CreateIntFactors;

	проц CreateDiffEqnFactors( alpha: NbrRe.Real;  x: Romberg );
	(* Create the factors, i.e., 2^(r[k]), for Richardson extrapolation for differential equations.
		To be called after UpdateBWeights and UpdateCWeights.  *)
	нач
		если alpha < 1 то
			x.factors[0] := MathRe.Power( 2, 1 + alpha );  x.factors[1] := 4;
			x.factors[2] := MathRe.Power( 2, 2 + alpha );  x.factors[3] := MathRe.Power( 2, 3 + alpha );
			x.factors[4] := 16;  x.factors[5] := MathRe.Power( 2, 4 + alpha );
			x.factors[6] := MathRe.Power( 2, 5 + alpha );  x.factors[7] := 64
		иначе
			x.factors[0] := 4;  x.factors[1] := MathRe.Power( 2, 1 + alpha );
			x.factors[2] := MathRe.Power( 2, 2 + alpha );  x.factors[3] := 16;
			x.factors[4] := MathRe.Power( 2, 3 + alpha );  x.factors[5] := MathRe.Power( 2, 4 + alpha );
			x.factors[6] := 64;  x.factors[7] := MathRe.Power( 2, 5 + alpha )
		всё
	кон CreateDiffEqnFactors;

	проц VerifyTolerance( перем tol: NbrRe.Real );
	нач
		tol := NbrRe.Abs( tol );
		если tol < minTol то tol := minTol аесли tol > maxTol то tol := maxTol иначе (* tol is okay *) всё
	кон VerifyTolerance;

	(** Computes a Riemann-Liouville fractional-order integral.
		Iaf(x) = (1/G(a)) x0x (x-y)a-1 f(y) dy,  where  0 < a.  *)
	проц SolveI*( f: CalcFn.ReArg;  x, order: NbrRe.Real;  перем tol: NbrRe.Real ): NbrRe.Real;
	перем i, n: размерМЗ;  firstWeight, gamma, h, solution: NbrRe.Real;
		cWeights, fn, save: укль на массив из NbrRe.Real;  pWeights: массив 3 из NbrRe.Real;
		romberg: Romberg;

		проц UpdateCWeights;
		перем len, newLen: размерМЗ;  m: NbrRe.Real;
		нач
			m := 1 + order;
			если cWeights = НУЛЬ то
				нов( cWeights, 5 );
				pWeights[0] := MathRe.Power( 2, m );
				pWeights[1] := MathRe.Power( 3, m );
				pWeights[2] := MathRe.Power( 4, m );
				cWeights[0] := 1;
				cWeights[1] := pWeights[0] - 2;
				cWeights[2] := pWeights[1] - 2 * pWeights[0] + 1;
				cWeights[3] := pWeights[2] - 2 * pWeights[1] + pWeights[0];
				pWeights[0] := pWeights[1];
				pWeights[1] := pWeights[2];
				pWeights[2] := MathRe.Power( 5, m );
				cWeights[4] := pWeights[2] - 2 * pWeights[1] + pWeights[0];
				firstWeight := (m / 4 - 1 ) * pWeights[1] + pWeights[0]
			иначе
				save := cWeights;  len := длинаМассива( cWeights^ );  cWeights := НУЛЬ;
				newLen := 2 * (len - 1) + 1;  нов( cWeights, newLen );
				нцДля i := 0 до len - 1 делай cWeights[i] := save[i] кц;
				нцДля i := len до newLen - 1 делай
					pWeights[0] := pWeights[1];
					pWeights[1] := pWeights[2];
					pWeights[2] := MathRe.Power( i + 1, m );
					cWeights[i] := pWeights[2] - 2 * pWeights[1] + pWeights[0]
				кц;
				firstWeight := (m / (newLen - 1) - 1) * pWeights[1] + pWeights[0]
			всё
		кон UpdateCWeights;

	нач
		если order > 0 то
			нов( romberg );  VerifyTolerance( tol );
			если x < 0 то solution := -SolveI( f, -x, order, tol )
			аесли x = 0 то solution := 0
			иначе  (* integrate *)
				CreateIntFactors( order, romberg );  gamma := MathGamma.Fn( 2 + order );
				(* The algorithmic loop. *)
				нцДо
					UpdateCWeights;  n := длинаМассива( cWeights^ ) - 1;  h := x / n;
					(* Function evaluations over the history. *)
					если n = 4 то
						нов( fn, 5 );
						нцДля i := 0 до n делай fn[i] := f( h * i ) кц
					иначе
						save := fn;  fn := НУЛЬ;  нов( fn, n + 1 );
						нцДля i := 0 до n шаг 2 делай fn[i] := save[i DIV 2] кц;
						нцДля i := 1 до n-1 шаг 2 делай fn[i] := f( h * i ) кц;
					всё;
					(* The quadrature algorithm. *)
					solution := firstWeight * fn[0];
					нцДля i := 1 до n делай solution := solution + cWeights[n - i] * fn[i] кц;
					romberg.Update( MathRe.Power( h, order ) * solution / gamma )
				кцПри romberg.error < tol;
				solution := romberg.soln;  tol := romberg.error
			всё
		иначе solution := 0;  DataErrors.ReError( order, "The requested order of integration is not allowed." )
		всё;
		возврат solution
	кон SolveI;

	(** Computes a Caputo fractional-order derivative.
		D*af(x) = IaDnf(x) = (1/G(n-a)) x0x (x-y)n-1-a [dnf(y)/dyn] dy,  f(k)(0+) = f0[k],  where  0 < a < 2, a # 1. *)
	проц SolveD*( f: CalcFn.ReArg;  x, order: NbrRe.Real;  перем tol: NbrRe.Real ): NbrRe.Real;
	перем i, n: размерМЗ;  df0, f0, gamma, h, lastWeight, solution: NbrRe.Real;
		aWeights, fn, save: укль на массив из NbrRe.Real;  mWeights: массив 3 из NbrRe.Real;
		romberg: Romberg;

		проц UpdateAWeights;
		перем len, newLen: размерМЗ;  m: NbrRe.Real;
		нач
			m := 1 - order;
			если aWeights = НУЛЬ то
				нов( aWeights, 5 );
				mWeights[0] := MathRe.Power( 2, m );
				mWeights[1] := MathRe.Power( 3, m );
				mWeights[2] := MathRe.Power( 4, m );
				aWeights[0] := 1;
				aWeights[1] := mWeights[0] - 2;
				aWeights[2] := mWeights[1] - 2 * mWeights[0] + 1;
				aWeights[3] := mWeights[2] - 2 * mWeights[1] + mWeights[0];
				mWeights[0] := mWeights[1];
				mWeights[1] := mWeights[2];
				mWeights[2] := MathRe.Power( 5, m );
				aWeights[4] := mWeights[2] - 2 * mWeights[1] + mWeights[0];
				lastWeight := (m / 4 - 1) * mWeights[1] + mWeights[0]
			иначе
				save := aWeights;  len := длинаМассива( aWeights^ );  aWeights := НУЛЬ;
				newLen := 2 * (len - 1) + 1;  нов( aWeights, newLen );
				нцДля i := 0 до len - 1 делай aWeights[i] := save[i] кц;
				нцДля i := len до newLen - 1 делай
					mWeights[0] := mWeights[1];
					mWeights[1] := mWeights[2];
					mWeights[2] := MathRe.Power( i + 1, m );
					aWeights[i] := mWeights[2] - 2 * mWeights[1] + mWeights[0]
				кц;
				lastWeight := (m / (newLen - 1) - 1) * mWeights[1] + mWeights[0]
			всё
		кон UpdateAWeights;

	нач
		если ((0 < order) и (order # 1) и (order < 2)) то
			нов( romberg );  VerifyTolerance( tol );
			если x < 0 то solution := -SolveD( f, -x, order, tol )
			аесли x = 0 то solution := 0
			иначе  (* differentiate *)
				CreateDiffFactors( order, romberg );  gamma := MathGamma.Fn( 2 - order );
				(* Account for the initial conditions. *)
				f0 := f( NbrRe.Epsilon * x );
				если order > 1 то df0 := CalcD1.Solve( f, 0, CalcD1.Forward ) иначе df0 := 0 всё;
				(* The algorithmic loop. *)
				нцДо
					UpdateAWeights;  n := длинаМассива( aWeights^ ) - 1;  h := x / n;  solution := 0;
					(* Function evaluations over the history. *)
					если n = 4 то
						нов( fn, 5 );
						нцДля i := 0 до n делай fn[i] := f( h * (n - i) ) кц
					иначе
						save := fn;  fn := НУЛЬ;  нов( fn, n + 1 );
						нцДля i := 0 до n шаг 2 делай fn[i] := save[i DIV 2] кц;
						нцДля i := 1 до n-1 шаг 2 делай fn[i] := f( h * (n - i) ) кц;
					всё;
					(* The quadrature algorithm. *)
					нцДля i := 0 до n - 1 делай
						solution := solution + aWeights[i] * (fn[i] - f0 - h * (n - i) * df0)
					кц;
					solution := solution + lastWeight * (fn[n] - f0);
					romberg.Update( solution / (gamma * MathRe.Power( h, order )) )
				кцПри romberg.error < tol;
				solution := romberg.soln;  tol := romberg.error
			всё
		иначе solution := 0;  DataErrors.ReError( order, "The requested order of differentiation is not allowed." )
		всё;
		возврат solution
	кон SolveD;

	(** Solves a fractional-order differential equation of the Caputo type.
		D*ay(x) = f(x,y(x)),  y(k)(0+) = y0[k],  where  0  < a < 2. *)
	проц SolveFODE*( f: CalcFn.Re2Arg;  y0: массив из NbrRe.Real;  x, order: NbrRe.Real;
							перем tol: NbrRe.Real ): NbrRe.Real;
	перем i, k, n: размерМЗ;  cCoef, pCoef, cSum, pSum, firstWeight, fn, gamma1, gamma2, h,
		h2alpha, initialCondition, p, predictor, solution: NbrRe.Real;
		bWeights, cWeights, save, y: укль на массив из NbrRe.Real;
		weights: массив 2 из NbrRe.Real;  pWeights: массив 3 из NbrRe.Real;
		romberg: Romberg;

		проц UpdateBWeights;
		перем len, newLen: размерМЗ;  m: NbrRe.Real;
		нач
			m := order;
			если bWeights = НУЛЬ то
				нов( bWeights, 5 );
				weights[0] := MathRe.Power( 2, m );
				weights[1] := MathRe.Power( 3, m );
				bWeights[0] := 0;
				bWeights[1] := 1;
				bWeights[2] := weights[0] - 1;
				bWeights[3] := weights[1] - weights[0];
				weights[0] := weights[1];
				weights[1] := MathRe.Power( 4, m );
				bWeights[4] := weights[1] - weights[0]
			иначе
				save := bWeights;  len := длинаМассива( bWeights^ );  bWeights := НУЛЬ;
				newLen := 2 * (len - 1) + 1;  нов( bWeights, newLen );
				нцДля i := 0 до len - 1 делай bWeights[i] := save[i] кц;
				нцДля i := len до newLen - 1 делай
					weights[0] := weights[1];
					weights[1] := MathRe.Power( i, m );
					bWeights[i] := weights[1] - weights[0]
				кц
			всё
		кон UpdateBWeights;

		проц UpdateCWeights;
		перем len, newLen: размерМЗ;  m: NbrRe.Real;
		нач
			m := 1 + order;
			если cWeights = НУЛЬ то
				нов( cWeights, 5 );
				pWeights[0] := MathRe.Power( 2, m );
				pWeights[1] := MathRe.Power( 3, m );
				pWeights[2] := MathRe.Power( 4, m );
				cWeights[0] := 1;
				cWeights[1] := pWeights[0] - 2;
				cWeights[2] := pWeights[1] - 2 * pWeights[0] + 1;
				cWeights[3] := pWeights[2] - 2 * pWeights[1] + pWeights[0];
				pWeights[0] := pWeights[1];
				pWeights[1] := pWeights[2];
				pWeights[2] := MathRe.Power( 5, m );
				cWeights[4] := pWeights[2] - 2 * pWeights[1] + pWeights[0]
			иначе
				save := cWeights;  len := длинаМассива( cWeights^ );  cWeights := НУЛЬ;
				newLen := 2 * (len - 1) + 1;  нов( cWeights, newLen );
				нцДля i := 0 до len - 1 делай cWeights[i] := save[i] кц;
				нцДля i := len до newLen - 1 делай
					pWeights[0] := pWeights[1];
					pWeights[1] := pWeights[2];
					pWeights[2] := MathRe.Power( i + 1, m );
					cWeights[i] := pWeights[2] - 2 * pWeights[1] + pWeights[0]
				кц
			всё
		кон UpdateCWeights;

	нач
		если ((0 < order) и (order < 2)) то
			нов( romberg );  VerifyTolerance( tol );
			если order <= 1 то
				если длинаМассива( y0 ) < 1 то
					DataErrors.Error( "Initial-condition vector is of insufficient length." ); возврат 0
				всё
			иначе
				если длинаМассива( y0 ) < 2 то
					DataErrors.Error( "Initial-condition vector is of insufficient length." ); возврат 0
				всё
			всё;
			если x < 0 то solution := -SolveFODE( f, y0, -x, order, tol )
			аесли x = 0 то solution := y0[0]
			иначе  (* solve *)
				CreateDiffEqnFactors( order, romberg );  p := 1 + order;
				gamma1 := MathGamma.Fn( p );  gamma2 := MathGamma.Fn( 1 + p );
				(* The algorithmic loop. *)
				нцДо
					UpdateBWeights;  UpdateCWeights;  n := длинаМассива( cWeights^ ) - 1;
					h := x / n;  h2alpha := MathRe.Power( h, order );
					pCoef := h2alpha / gamma1;  cCoef := h2alpha / gamma2;
					y := НУЛЬ;  нов( y, n + 1 );  y[0] := y0[0];
					нцДля i := 1 до n делай
						(* Integrate along the solution path to get y(i*h) for i = 1...n. *)
						initialCondition := y0[0];  firstWeight := (p / i - 1) * MathRe.Power( i, p );
						если order > 1 то initialCondition := initialCondition + i * h * y0[1]  всё;
						если i > 1 то firstWeight := firstWeight + MathRe.Power( i - 1, p ) всё;
						fn := f( 0, y[0] );  pSum := bWeights[i] * fn;  cSum := firstWeight * fn;
						нцДля k := 1 до i - 1 делай
							fn := f( k * h, y[k] );
							pSum := pSum + bWeights[i - k] * fn;  cSum := cSum + cWeights[i - k] * fn
						кц;
						predictor := initialCondition + pCoef * pSum;
						y[i] := initialCondition + cCoef * (cSum + cWeights[0]* f( i * h, predictor ))
					кц;
					romberg.Update( y[n] )
				кцПри romberg.error < tol;
				solution := romberg.soln;  tol := romberg.error
			всё
		иначе solution := 0;  DataErrors.ReError( order, "The requested order of differentiation is not allowed." )
		всё;
		возврат solution
	кон SolveFODE;

нач
	minTol := MathRe.Sqrt( NbrRe.Epsilon );  maxTol := 0.1
кон CalcDiethelm.
