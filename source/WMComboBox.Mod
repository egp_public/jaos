модуль WMComboBox; (** AUTHOR "BohdanT"; PURPOSE "ComboBox visual component"; *)
использует
	ЛогЯдра, Modules, Strings,  UTF8Strings, WMMessages, 
	(* visual part *)
	
	(* WMRectangles,*) WMWindowManager, WMProperties, WMComponents, WMStandardComponents, WMEditors, 
	WMGraphics, WMEvents,
	WMStringGrids, WMGrids,WM := WMWindowManager;


тип
	String = Strings.String;
	(* generic sortable list *)
	ListElement* = укль на запись
		text*:String;
		data*:динамическиТипизированныйУкль;
		visible-:булево;
		next- : ListElement;
	кон;

	(* return -1, if a < b; 0, if a = b; 1, if a > b *)
	CompareProc = проц {делегат} (a, b : ListElement) : цел32;

	Table* = окласс
	перем 		
		list:ListElement;
		count-:цел32;

		проц &New*;
		нач
			count:=0;
			list:=НУЛЬ;
		кон New;
		
		проц Add*(s:String;d:динамическиТипизированныйУкль);
		перем
			p:ListElement;
		нач
			нов(p);
			p.text:=s;
			p.data:=d;
			p.next:=list;
			p.visible:=истина;
			list:=p;
			увел(count);
		кон Add;
	кон Table;

тип
	ComboWindow*  = окласс (WMComponents.FormWindow)
	перем 
		list : WMStringGrids.StringGrid;
		spacings : WMGrids.Spacings;
	
		filter:  String;
		table : Table;
		active:булево;
		owner:ComboBox;
		проц CreateForm(): WMComponents.VisualComponent;
		перем
			panel : WMStandardComponents.Panel;
			ep: WMStandardComponents.Panel;
		нач

			нов(panel); panel.bounds.SetExtents(500, 160); panel.fillColor.Set(0); panel.takesFocus.Set(истина);
			нов(table);
			
			(* edit panel *)
			нов(ep); ep.alignment.Set(WMComponents.AlignClient); ep.fillColor.Set(цел32(0DDDD00EEH));
			panel.AddContent(ep);


			нов(list); list.alignment.Set(WMComponents.AlignClient); 
			нов(spacings, 1); spacings[0] := 500;
			list.SetExtKeyEventHandler(ListKeyPressed);
			list.Acquire;
			list.defaultRowHeight.Set(25);
			list.cellDist.Set(0);
			list.clCell.Set(цел32(0FFFFFFA0H));
			list.SetColSpacings(spacings);
			list.SetFont(WMGraphics.GetFont("Courier", 12, {}));
			list.model.Acquire;
			list.model.SetNofCols(1);
			
			list.model.Release;	
			list.Release;
			list.onClickSelected.Add(ClickSelected);
			ep.AddContent(list);
			возврат panel
		кон CreateForm;

		проц &New*( owner : ComboBox);
		перем vc : WMComponents.VisualComponent;
		нач
			сам.owner:=owner;
			vc := CreateForm();
			active:=ложь;
			
			Init(vc.bounds.GetWidth(), vc.bounds.GetHeight(), истина);
			SetContent(vc);
			list.Invalidate;
		кон New;

		проц ClickSelected(sender, data : динамическиТипизированныйУкль);
		перем 
			p : ListElement;
		нач
			ЛогЯдра.пСтроку8("ClickSelected"); ЛогЯдра.пВК_ПС;
			если (data # НУЛЬ) и (data суть ListElement) то
				p:=data(ListElement);
				owner.onClick.Call(p.data);
			всё;
			ScheduleHide;
		кон ClickSelected;


		проц ListKeyPressed(ucs : размерМЗ; flags : мнвоНаБитахМЗ; перем keySym : размерМЗ; перем handled : булево);
		нач
			если keySym = 0FF0DH то handled := истина; 
					ScheduleHide;
			 всё;
		кон ListKeyPressed;
		
		проц ScheduleHide;
		перем msg : WMMessages.Message;
		нач
			msg.msgType := WMMessages.MsgExt;
			msg.ext := сам;
			если ~sequencer.Add(msg) то ЛогЯдра.пСтроку8("ComboWindowsr out of sync") всё;
		кон ScheduleHide;
		
		проц {перекрыта}FocusLost;
		нач
			FocusLost^;
			ScheduleHide
		кон FocusLost;
		
		проц Hide;
		нач
			manager := WMWindowManager.GetDefaultManager();
			manager.Remove(сам);
			active:=ложь;
		кон Hide;
		
		проц {перекрыта}Handle(перем x: WMMessages.Message);
		нач
			если (x.msgType = WMMessages.MsgExt) то
				если (x.ext = сам) то Hide
				всё
			иначе Handle^(x)
			всё
		кон Handle;
		
		проц StartNewFilter*(filter : Strings.String);
		нач
			сам.filter := filter;
			PrepareList
		кон StartNewFilter;

		проц PrepareList;
		перем i, vis : цел32; mask : массив 128 из симв8; s : Strings.String;
		l:ListElement;
		нач
			s := сам.filter;
			mask := "";
			если s # НУЛЬ то копируйСтрокуДо0(s^, mask) всё;
			если mask = "" то
				l:=table.list;i:=0;
				нцПока l#НУЛЬ делай l.visible := истина;увел(i);l:=l.next кц;
				vis := i;
			иначе
				если mask[Strings.Length(mask)] # "*" то Strings.Append(mask, "*") всё;
				если mask[0] # "*" то Strings.Concat("*",mask,mask) всё;
				
				vis := 0;
				l:=table.list;
				нцПока l#НУЛЬ делай 
					если Strings.Match(mask, l.text^) то
						l.visible := истина;
						увел(vis)
					иначе l.visible := ложь
					всё;
					l:=l.next;
				кц;
			всё;

			list.model.Acquire;
			list.model.SetNofRows(vis);

			vis := 0;
			l:=table.list;
			нцПока l#НУЛЬ делай 
				если l.visible то
					list.model.SetCellText(0, vis, l.text);
					list.model.SetCellData(0, vis, l);

					увел(vis)
				всё;
				l:=l.next;
			кц;
			list.SetTopPosition(0, 0, истина);
			list.model.Release;
		кон PrepareList;


	кон ComboWindow;

	ComboBox* = окласс(WMEditors.TextField)
	перем
		tb:WMStandardComponents.Button;
		filterProp : WMProperties.StringProperty;
		cw:ComboWindow;
		text:массив 255 из симв8;
		onClick- : WMEvents.EventSource;

		проц OnClickDown*(sender, data:динамическиТипизированныйУкль);
		нач
			ЛогЯдра.пСтроку8("OnClickDown"); ЛогЯдра.пВК_ПС;
			если ~cw.active то
				сам.SetFocus;
				TextChanged(НУЛЬ,НУЛЬ);
			иначе
				cw.ScheduleHide 
			всё;
		кон OnClickDown;
		
		проц Show;
		перем manager :WM.WindowManager;
			x,y:размерМЗ;

		нач
			manager := WMWindowManager.GetDefaultManager();
			ToWMCoordinates(0, bounds.GetTop(), x, y);
			manager.Add(x, y+bounds.GetHeight() , cw, {WMWindowManager.FlagHidden, WMWindowManager.FlagStayOnTop,WM.FlagNoFocus});
			cw.active:=истина;
		кон Show;

		проц AddItem*(s:String;d:динамическиТипизированныйУкль);
		нач
			cw.table.Add(s,d);
			Invalidate;
		кон AddItem;
		

		проц {перекрыта}FocusReceived*;
		нач
			FocusReceived^;
		кон FocusReceived;

		
		проц {перекрыта}FocusLost;
		нач
			FocusLost^;
			если cw.active то
				cw.ScheduleHide 
			всё;
		кон FocusLost;
		
		проц &{перекрыта}Init*;
		нач
			Init^;
(*			takesFocus.Set(TRUE);*)

			fillColor.Set(WMGraphics.White);
			нов(filterProp, ListFilterProt, НУЛЬ, НУЛЬ); properties.Add(filterProp);

			нов(tb);
			tb.bounds.SetExtents(20, 20);
			tb.alignment.Set(WMComponents.AlignRight);
(*					tb.imageName.Set(img);*)
			tb.imageName.Set(Strings.NewString("WMDebugger.zip://Down.png"));
			tb.onClick.Add (OnClickDown);
			AddContent(tb);

			сам.onChanged.Add(TextChanged);

			сам.onEnter.Add(Ok);
			text:="";
			сам.SetAsString(text);
			
			(* events *)
			нов(onClick, сам, Strings.NewString("onClick"), НУЛЬ, сам.StringToCompCommand); events.Add(onClick);

			
			нов(cw, сам);
		кон Init;
		
		проц TextChanged*(sender, data:динамическиТипизированныйУкль);
		перем
			l:ListElement;
			str : массив 128 из симв8;
		нач
			нов(l);
			l.next:=cw.table.list;
			Sort(l, Compare);
			cw.table.list:=l.next;
			сам.GetAsString(str);
			filterProp.Set(Strings.NewString(str));
			если ~cw.active то
				Show;
			всё;
			(* avoid recursion 
			edit.text.onTextChanged.Remove(TextChanged);
			edit.text.onTextChanged.Add(TextChanged)*)
		кон TextChanged;

		проц Ok*(sender, data:динамическиТипизированныйУкль);
		нач
			cw.ScheduleHide
		кон Ok;

		проц {перекрыта}PointerLeave;
		нач
			PointerLeave^;
		кон PointerLeave;

		проц {перекрыта}KeyEvent(ucs : размерМЗ; flags : мнвоНаБитахМЗ; перем keySym : размерМЗ);
		нач
(*			IF keySym = 0FF54H THEN Show END; Cursor Down *)
			KeyEvent^(ucs, flags, keySym);
			PointerLeave;
		кон KeyEvent;
		проц {перекрыта}PropertyChanged*(sender, data : динамическиТипизированныйУкль);
		нач

			если (data = filterProp) то
				cw.StartNewFilter(filterProp.Get())
			иначе PropertyChanged^(sender, data)
			всё
		кон PropertyChanged;
		проц {перекрыта}Finalize*; (** PROTECTED *)
		нач
			если cw.active то
				cw.ScheduleHide
			всё;
			Finalize^;
		кон Finalize;

	кон ComboBox;

(** Merge-sort a single-linked list. The root element is a dummy node *)
(* Algorithm by Simon Tatham *)
проц Sort(root: ListElement; compare : CompareProc);	(* root is dummy node *)
перем m, n, np, nq: цел32; p, q, tail: ListElement;
нач
	n := 1;
	нцДо
		p := root.next; q := p; tail := root; m := 0;
		нцПока p # НУЛЬ делай	(* merge sorted lists of length n into sorted lists of length 2*n (sort of) *)
			np := 0;	(* step q over <= n nodes *)
			нцДо q := q.next; увел(np) кцПри (q = НУЛЬ) или (np = n);
			nq := n; увел(m);
			нц	(* merge list p with np nodes and list q with <= nq nodes at end of tail *)
				если (np # 0) и ((nq = 0) или (q = НУЛЬ) или (compare(p, q) <= 0)) то
					tail.next := p; tail := p; p := p.next; умень(np)
				аесли (nq # 0) и (q # НУЛЬ) то
					tail.next := q; tail := q; q := q.next; умень(nq)
				иначе	(* (np = 0) & ((nq = 0) OR (q = NIL)) *)
					прервиЦикл
				всё
			кц;
			tail.next := НУЛЬ; p := q
		кц;
		n := n*2
	кцПри m <= 1
кон Sort;

проц Compare(a, b: ListElement) : цел32;
нач
	возврат UTF8Strings.Compare(a.text^, b.text^);
кон Compare;

(*Test section*)
тип
	Window* = окласс (WMComponents.FormWindow)
	перем
		cb:ComboBox;
		проц CreateForm() : WMComponents.VisualComponent;
		перем
			panel : WMStandardComponents.Panel;
			
		нач
			нов(panel);
			panel.bounds.SetExtents(800, 700);
			panel.fillColor.Set(цел32(0FFFFFFFFH));
			нов(cb);
			cb.bounds.SetWidth (300);
			cb.bounds.SetHeight (20);
						
			cb.AddItem(Strings.NewString("Noah"),НУЛЬ);
			cb.AddItem(Strings.NewString("Ethan"),НУЛЬ);
			cb.AddItem(Strings.NewString("Mason"),НУЛЬ);
			cb.AddItem(Strings.NewString("Logan"),НУЛЬ);
			cb.AddItem(Strings.NewString("Lucas"),НУЛЬ);
			cb.AddItem(Strings.NewString("Jacob"),НУЛЬ);
			cb.AddItem(Strings.NewString("Aiden"),НУЛЬ);
			cb.AddItem(Strings.NewString("Jackson "),НУЛЬ);
			cb.AddItem(Strings.NewString("Jack"),НУЛЬ);
			cb.AddItem(Strings.NewString("Elijah"),НУЛЬ);
			cb.AddItem(Strings.NewString("Benjamin"),НУЛЬ);
			cb.AddItem(Strings.NewString("James"),НУЛЬ);
			cb.AddItem(Strings.NewString("Luke"),НУЛЬ);
			cb.AddItem(Strings.NewString("William "),НУЛЬ);

			cb.AddItem(Strings.NewString("Olivia"),НУЛЬ);
			cb.AddItem(Strings.NewString("Sophia"),НУЛЬ);
			cb.AddItem(Strings.NewString("Ava"),НУЛЬ);
			cb.AddItem(Strings.NewString("Isabella "),НУЛЬ);
			cb.AddItem(Strings.NewString("Mia"),НУЛЬ);
			cb.AddItem(Strings.NewString("Charlotte"),НУЛЬ);
			cb.AddItem(Strings.NewString("Emily"),НУЛЬ);
			cb.AddItem(Strings.NewString("Abigail  "),НУЛЬ);
			cb.AddItem(Strings.NewString("Harper"),НУЛЬ);
			cb.AddItem(Strings.NewString("Avery"),НУЛЬ);
			cb.AddItem(Strings.NewString("Madison"),НУЛЬ);
			cb.AddItem(Strings.NewString("Ella"),НУЛЬ);
			cb.AddItem(Strings.NewString("Amelia"),НУЛЬ);
			cb.AddItem(Strings.NewString("Lily"),НУЛЬ);

			panel.AddContent(cb);
			возврат panel
		кон CreateForm;

		проц &New*;
		перем vc : WMComponents.VisualComponent;
		нач
			vc := CreateForm ();
			Init (vc.bounds.GetWidth (), vc.bounds.GetHeight (), ложь);
			SetContent (vc);
			WM.DefaultAddWindow (сам);
			SetTitle (Strings.NewString ("GUIPatW1"));
		кон New;


	кон Window;

перем 
	winstance : Window;
	ListFilterProt : WMProperties.StringProperty;

проц InitPrototypes;
нач
	нов(ListFilterProt, НУЛЬ, Strings.NewString("Filter"), Strings.NewString("display list filter"));
кон InitPrototypes;

проц Open*;
нач
	если winstance = НУЛЬ то нов (winstance); всё;	(* Only one window may be instantiated. *)
кон Open;

проц Cleanup;
нач
	если 	winstance # НУЛЬ то
		winstance.Close ();
		winstance := НУЛЬ
	всё;
кон Cleanup;

нач
	InitPrototypes;
	Modules.InstallTermHandler(Cleanup)
кон WMComboBox.


System.FreeDownTo WMComboBox ~
WMComboBox.Open ~
