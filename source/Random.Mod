(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Random; (** AUTHOR "ecarter/bsm/pjm"; PURPOSE "Pseudo-random number generator"; *)

(* Based on the ADA version by Everett F. Carter Jr., ported to Aos by Ben Smith-Mannschott. *)

использует НИЗКОУР, Math;

конст
	max       = 2147483647;
	msbit     = 40000000H;
	allbits   = 7FFFFFFFH;
	halfrange = 20000000H;
	step      = 7;

	allbitsInv = 1 / вещ32(allbits);

тип
		(** A pseudo-random number generator.  This object is not reentrant. *)
	Generator* = окласс
		перем
			buffer: массив 250 из мнвоНаБитах32;
			index: цел32;
			Z: цел32;	(* seed for Rand() *)

		проц Rand(): цел32;
			(* for Init. Same as used by RandomNumbers *)
			конст a = 16807; q = 127773; r = 2836;
			перем t: цел32;
		нач
			t := a * (Z остОтДеленияНа q) - r * (Z DIV q);
			если t > 0 то Z := t иначе Z := t + max всё;
			возврат Z;
		кон Rand;

		(** Set the seed. *)

		проц InitSeed*(seed: цел32);
			перем
				k, i: цел32;
				mask, msb: цел32;
		нач
			Z := seed; index := 0;
			нцДля i := 0 до 249 делай
				buffer[i] := мнвоНаБитах32(Rand())
			кц;
			нцДля i := 0 до 249 делай
				если Rand() > halfrange то
					buffer[i] := buffer[i] + мнвоНаБитах32(msbit);
				всё;
			кц;
			msb := msbit; mask := allbits;
			нцДля i := 0 до 30 делай
				k := step * i + 3;
				buffer[k] := buffer[k] * мнвоНаБитах32(mask);
				buffer[k] := buffer[k] + мнвоНаБитах32(msb);
				msb := msb DIV 2;
				mask := mask DIV 2;
			кц;
		кон InitSeed;

		(** The default seed is 1. *)
		проц & Init*;
		нач
			InitSeed(1)
		кон Init;

		(** Return a pseudo-random 32-bit integer. *)

		проц Integer*(): цел32;
			перем newRand, j: цел32;
		нач
			если index >= 147 то j := index - 147 иначе j := index + 103 всё;
			buffer[index] := buffer[index] / buffer[j];
			newRand := НИЗКОУР.подмениТипЗначения(цел32, buffer[index]);
			если index >= 249 то index := 0 иначе увел(index) всё;
			возврат newRand
		кон Integer;

		(** Return a pseudo-random number from 0..sides-1. *)

		проц Dice*(sides: цел32): цел32;
		нач
			возврат Integer() остОтДеленияНа sides;
		кон Dice;

		(** Return a pseudo-random real number, uniformly distributed. *)

		проц Uniform*(): вещ32;
		нач
			возврат Integer() * allbitsInv;
		кон Uniform;

		(** Return a pseudo-random real number, exponentially distributed. *)

		проц Exp*(mu: вещ32): вещ32;
		нач
			возврат -Math.ln(Uniform())/mu
		кон Exp;

		проц Gaussian*(): вещ32; (*generates a normal distribution with mean 0, variance 1 using the Box-Muller Transform*)
		перем
			x1,x2,w,y1: вещ32;
		нач
			нцДо
				x1:=2.0*Uniform()-1;
				x2:=2.0*Uniform()-1;
				w:=x1*x1+x2*x2;
			кцПри w<1;
			w:=Math.sqrt( (-2.0* Math.ln(w) ) /w);
			y1:=x1*w;
			(*y2:=x2*w*)
			возврат y1;
		кон Gaussian;

	кон Generator;

тип
		(** This is a protected wrapper for the Generator object.  It synchronizes concurrent calls and is therefore slower. *)
	Sequence* = окласс
		перем r: Generator;

		проц InitSeed*(seed: цел32);
		нач {единолично}
			r.InitSeed(seed)
		кон InitSeed;

		проц &Init*;
		нач
			нов(r)
		кон Init;

		проц Integer*(): цел32;
		нач {единолично}
			возврат r.Integer()
		кон Integer;

		проц Dice*(sides: цел32): цел32;
		нач {единолично}
			возврат r.Dice(sides)
		кон Dice;

		проц Uniform*(): вещ32;
		нач {единолично}
			возврат r.Uniform()
		кон Uniform;

		проц Exp*(mu: вещ32): вещ32;
		нач {единолично}
			возврат r.Exp(mu)
		кон Exp;

		проц Gaussian*(): вещ32; (*generates a normal distribution with mean 0, variance 1 using the Box-Muller Transform*)
		нач{единолично}
			возврат r.Gaussian();
		кон Gaussian;


	кон Sequence;

кон Random.

(*
   from the ADA version:
   (c) Copyright 1997 Everett F. Carter Jr.   Permission is
   granted by the author to use this software for any
   application provided this copyright notice is preserved.

   The algorithm was originally described by
   Kirkpatrick, S., and E. Stoll, 1981;
       A Very Fast Shift-Register Sequence Random Number Generator,
       Journal of Computational Physics, V. 40. pp. 517-526

   Performance:

   Its period is 2^249. This implementation is about 25% faster than
   RandomNumbers.Uniform().  It also offers direct generation of
   integers which is even faster (2x on PowerPC) and especially
   good for FPU-challenged machines like the Shark NCs.
 *)
