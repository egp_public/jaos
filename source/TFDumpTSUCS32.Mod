модуль TFDumpTSUCS32;

использует
	TS := TFTypeSysUCS32, MultiLogger, Потоки, Трассировка, UCS32, UCS32u, StringsUCS32;

перем w* : Потоки.Писарь;
	ml : MultiLogger.LogWindow;
	indent : цел32;

проц Indent;
перем i : цел32;
нач
	нцДля i := 0 до indent - 1 делай w.пСимв8(09X) кц
кон Indent;


проц DumpConst(c : TS.Const);
нач
	Indent; w.пСтроку32_вЮ8(c.name^); w.пВК_ПС;
кон DumpConst;

проц DumpObject(o : TS.Class);
нач
	Indent; w.пСтроку32_вЮ8(Лит32("OBJECT "));
	если o.scope.super # НУЛЬ то
		w.пСимв8("(");
	(*	DumpDesignator(o.super); *)
		w.пСимв8(")");
	всё;
	w.пВК_ПС;
	увел(indent); DumpDeclarations(o.scope); умень(indent);

	Indent; w.пСтроку32_вЮ8(Лит32("END ")); w.пВК_ПС
кон DumpObject;

проц DumpArray(a : TS.Array);
нач
	Indent; w.пСтроку32_вЮ8(Лит32("ARRAY OF "));
	DumpType(a.base)
кон DumpArray;

проц DumpRecord(r : TS.Record);
нач
	Indent; w.пСтроку32_вЮ8(Лит32("RECORD"))
кон DumpRecord;

проц DumpProcedure(p : TS.ProcedureType);
нач

	Indent; w.пСтроку32_вЮ8(Лит32("PROCEDURE"));
кон DumpProcedure;


проц DumpDesignator*(d : TS.Designator);
перем s : массив 64 из симв32;
нач
	если d = НУЛЬ то w.пСтроку32_вЮ8(Лит32("NIL")); возврат всё;
	если d суть TS.Ident то TS.s.GetString(d(TS.Ident).name, s); w.пСтроку32_вЮ8(s)
	аесли d суть TS.Index то
		w.пСтроку32_вЮ8(Лит32("["));
		DumpExpressionList(d(TS.Index).expressionList);
		w.пСтроку32_вЮ8(Лит32("]"));
	аесли d суть TS.ActualParameters то
		w.пСтроку32_вЮ8(Лит32("("));
		DumpExpressionList(d(TS.ActualParameters).expressionList);
		w.пСтроку32_вЮ8(Лит32(")"));
	всё;
	если (d.next # НУЛЬ) то
		если (d суть TS.Ident) то w.пСтроку32_вЮ8(Лит32(".")) иначе всё;
		DumpExpression(d.next) (* bylq DumpDesignator *)
	всё
кон DumpDesignator;

проц DumpExpressionList(e : TS.ExpressionList);
нач
	нцПока e # НУЛЬ делай
		DumpExpression(e.expression);
		если e.next # НУЛЬ то w.пСтроку32_вЮ8(Лит32(", ")) всё;
		e := e.next
	кц
кон DumpExpressionList;


проц DumpExpression(e : TS.Expression);
нач
w.ПротолкниБуферВПоток;
	если e = НУЛЬ то w.пСтроку32_вЮ8(Лит32("NIL")); w.ПротолкниБуферВПоток; возврат всё;

	если e.kind = TS.ExpressionPrimitive то
		w.пСтроку32_вЮ8(Лит32("Primitive")); w.пЦел64(e.basicType, 0); w.пЦел64(устарПреобразуйКБолееУзкомуЦел(e.intValue), 0); w.ПротолкниБуферВПоток
	аесли e.kind = TS.ExpressionUnary то
		просей e.op из
			|TS.OpNegate: w.пСтроку32_вЮ8(Лит32("-"))
			|TS.OpInvert: w.пСтроку32_вЮ8(Лит32("~"))
		иначе
			Трассировка.пСтроку8("Internal error :"); Трассировка.пСтроку8("e.op= "); Трассировка.пЦел64(e.op, 0); Трассировка.пВК_ПС;
		всё;
		DumpExpression(e.a);
	аесли e.kind = TS.ExpressionBinary то
		w.пСтроку32_вЮ8(Лит32("("));
		DumpExpression(e.a);
		просей e.op из
			|TS.OpAdd: w.пСтроку32_вЮ8(Лит32("+"))
			|TS.OpSub: w.пСтроку32_вЮ8(Лит32("-"))
			|TS.OpOr: w.пСтроку32_вЮ8(Лит32("OR"))
			|TS.OpMul: w.пСтроку32_вЮ8(Лит32("*"))
			|TS.OpAnd: w.пСтроку32_вЮ8(Лит32("&"))
			|TS.OpIntDiv: w.пСтроку32_вЮ8(Лит32("DIV"))
			|TS.OpMod: w.пСтроку32_вЮ8(Лит32("MOD"))
			|TS.OpDiv: w.пСтроку32_вЮ8(Лит32("/"))

			|TS.OpEql: w.пСтроку32_вЮ8(Лит32("="))
			|TS.OpNeq: w.пСтроку32_вЮ8(Лит32("#"))
			|TS.OpLss: w.пСтроку32_вЮ8(Лит32("<"))
			|TS.OpLeq: w.пСтроку32_вЮ8(Лит32("<="))
			|TS.OpGtr: w.пСтроку32_вЮ8(Лит32(">"))
			|TS.OpGeq: w.пСтроку32_вЮ8(Лит32(">="))
			|TS.OpIn: w.пСтроку32_вЮ8(Лит32("IN"))
			|TS.OpIs: w.пСтроку32_вЮ8(Лит32("IS"))
		всё;
		DumpExpression(e.b);
		w.пСтроку32_вЮ8(Лит32(")"));
	аесли e.kind = TS.ExpressionDesignator то
		DumpDesignator(e.designator)
	всё;
кон DumpExpression;


проц DumpType*(t : TS.Type);
нач
	просей t.kind из
		|TS.TAlias : DumpDesignator(t.qualident)
		|TS.TObject : DumpObject(t.object)
		|TS.TArray : DumpArray(t.array);
		|TS.TPointer : w.пСтроку32_вЮ8(Лит32("POINTER TO ")); DumpType(t.pointer.type)
		|TS.TRecord : DumpRecord(t.record);
		|TS.TProcedure : DumpProcedure(t.procedure)
		|TS.TBasic : w.пСтроку32_вЮ8(Лит32("XXXX")); (* Ne znaem, kak vyvesti, no ehto ne povod dlja vyvoda soobshhenija "unknown type" *)
	иначе
		w.пСтроку32_вЮ8(Лит32("XXXX")); Трассировка.пСтроку8("Unknown Type"); Трассировка.пСтроку8("t.kind= "); Трассировка.пЦел64(t.kind, 0); Трассировка.пВК_ПС;
	всё

кон DumpType;

проц DumpCases(case : TS.Case);
перем cr : TS.CaseRange;
нач
	Indent;
	нцПока case # НУЛЬ делай
		cr := case.caseRanges;
		нцПока cr # НУЛЬ делай
			DumpExpression(cr.a);
			если cr.b # НУЛЬ то w.пСтроку32_вЮ8(Лит32("..")); DumpExpression(cr.b) всё;
			если cr.next # НУЛЬ то w.пСтроку32_вЮ8(Лит32(", ")) всё;
			cr := cr.next
		кц;
		w.пСтроку32_вЮ8(Лит32(" :")); w.пВК_ПС;
		если case.statements # НУЛЬ то DumpStatementSequence(case.statements) всё;
		если case.next # НУЛЬ то Indent; w.пСтроку32_вЮ8(Лит32("|")) всё;
		case := case.next
	кц;
кон DumpCases;


проц DumpTypeDecl(t : TS.TypeDecl);
нач
	Indent; w.пСтроку32_вЮ8(t.name^); w.пСтроку32_вЮ8(Лит32(" = ")); DumpType(t.type);
	w.пВК_ПС;
кон DumpTypeDecl;

проц DumpVar(v : TS.Var);
нач
	Indent; w.пСтроку32_вЮ8(v.name^); w.пСтроку32_вЮ8(Лит32(" : ")); DumpType(v.type); w.пВК_ПС
кон DumpVar;

проц DumpComments(comments : TS.Comments);
перем cur : TS.Comment;
нач
	cur := comments.first;
	нцПока cur # НУЛЬ делай
		w.ПротолкниБуферВПоток;
		ml.tw.SetFontStyle({0});
		w.пСтроку32_вЮ8(Лит32("(*"));
		w.пСтроку32_вЮ8(cur.str^);
		w.пСтроку32_вЮ8(Лит32("*)"));
		w.ПротолкниБуферВПоток;
		ml.tw.SetFontStyle({});
		cur := cur.next
	кц
кон DumpComments;


проц DumpStatementSequence(s : TS.Statement);
перем ts : TS.Statement; wsp : TS.WITHStatementPart; 
нач
	увел(indent);
	нцПока s # НУЛЬ делай
		если s.preComment # НУЛЬ то Indent; DumpComments(s.preComment); w.пВК_ПС всё;
		если s суть TS.Assignment то
			Indent;
			DumpDesignator(s(TS.Assignment).designator);
			w.пСтроку32_вЮ8(Лит32(" := "));
			DumpExpression(s(TS.Assignment).expression);
		аесли s суть TS.ProcedureCall то
			Indent;
			DumpDesignator(s(TS.ProcedureCall).designator);
		аесли s суть TS.IFStatement то
			Indent;
			w.пСтроку32_вЮ8(Лит32("IF "));
			DumpExpression(s(TS.IFStatement).expression);
			w.пСтроку32_вЮ8(Лит32("THEN ")); w.пВК_ПС;
			DumpStatementSequence(s(TS.IFStatement).then);
			ts := s(TS.IFStatement).else;
			если ts # НУЛЬ то
				Indent; w.пСтроку32_вЮ8(Лит32("ELSE ")); w.пВК_ПС;
				DumpStatementSequence(ts);
			всё;
			Indent; w.пСтроку32_вЮ8(Лит32("END"));
		аесли s суть TS.WHILEStatement то
			Indent; w.пСтроку32_вЮ8(Лит32("WHILE "));DumpExpression(s(TS.WHILEStatement).expression);
			w.пСтроку32_вЮ8(Лит32("DO")); w.пВК_ПС;
			DumpStatementSequence(s(TS.WHILEStatement).statements);
			Indent; w.пСтроку32_вЮ8(Лит32("END"))
		аесли s суть TS.REPEATStatement то
			Indent; w.пСтроку32_вЮ8(Лит32("REPEAT ")); w.пВК_ПС;
			DumpStatementSequence(s(TS.REPEATStatement).statements);
			Indent; w.пСтроку32_вЮ8(Лит32("UNTIL ")); DumpExpression(s(TS.REPEATStatement).expression);
		аесли s суть TS.LOOPStatement то
			Indent; w.пСтроку32_вЮ8(Лит32("LOOP")); w.пВК_ПС;
			DumpStatementSequence(s(TS.LOOPStatement).statements);
			Indent; w.пСтроку32_вЮ8(Лит32("END"))
		аесли s суть TS.FORStatement то
			Indent; w.пСтроку32_вЮ8(Лит32("FOR "));
			DumpDesignator(s(TS.FORStatement).variable);
			w.пСтроку32_вЮ8(Лит32(" := ")); DumpExpression(s(TS.FORStatement).fromExpression);
			w.пСтроку32_вЮ8(Лит32(" TO ")); DumpExpression(s(TS.FORStatement).toExpression);
			если s(TS.FORStatement).byExpression # НУЛЬ то
				w.пСтроку32_вЮ8(Лит32(" BY ")); DumpExpression(s(TS.FORStatement).byExpression);
			всё;
			w.пСтроку32_вЮ8(Лит32(" DO")); w.пВК_ПС;
			DumpStatementSequence(s(TS.FORStatement).statements);
			Indent; w.пСтроку32_вЮ8(Лит32("END"))
		аесли s суть TS.EXITStatement то
			Indent; w.пСтроку32_вЮ8(Лит32("EXIT"));
		аесли s суть TS.RETURNStatement то
			Indent; w.пСтроку32_вЮ8(Лит32("RETURN "));
			если s(TS.RETURNStatement).expression # НУЛЬ то DumpExpression(s(TS.RETURNStatement).expression) всё;
		аесли s суть TS.IGNOREStatement то
			Indent; w.пСтроку32_вЮ8(Лит32("IGNORE "));
			DumpExpression(s(TS.IGNOREStatement).expression); 
		аесли s суть TS.AWAITStatement то
			Indent; w.пСтроку32_вЮ8(Лит32("AWAIT("));
			DumpExpression(s(TS.AWAITStatement).expression); w.пСтроку32_вЮ8(Лит32("))"))
		аесли s суть TS.StatementBlock то
			Indent; w.пСтроку32_вЮ8(Лит32("BEGIN"));
			DumpStatementSequence(s(TS.StatementBlock).statements);
			Indent; w.пСтроку32_вЮ8(Лит32("END"))
		аесли s суть TS.WITHStatement то
			Indent; w.пСтроку32_вЮ8(Лит32("WITH "));
			wsp := s(TS.WITHStatement).typedClauses; 
			нцПока wsp # НУЛЬ делай
			DumpDesignator(s(TS.WITHStatement).variable);
				w.пСтроку32_вЮ8(Лит32(" : ")); 
				DumpDesignator(wsp.type.qualident);
			w.пСтроку32_вЮ8(Лит32(" DO")); w.пВК_ПС;
				DumpStatementSequence(wsp.statements);
				wsp := wsp.next;
				если wsp # НУЛЬ то
					Indent; w.пСтроку32_вЮ8(Лит32("| ")) всё кц;
			если s(TS.WITHStatement).elseClause # НУЛЬ то
				w.пВК_ПС; Indent; w.пСтроку32_вЮ8(Лит32("ELSE"));
				DumpStatementSequence(s(TS.WITHStatement).elseClause) всё;
			Indent; w.пСтроку32_вЮ8(Лит32("END"))
		аесли s суть TS.CASEStatement то
			Indent; w.пСтроку32_вЮ8(Лит32("CASE ")); DumpExpression(s(TS.CASEStatement).expression); w.пСтроку32_вЮ8(Лит32(" OF")); w.пВК_ПС;
			DumpCases(s(TS.CASEStatement).cases);
			если s(TS.CASEStatement).else # НУЛЬ то
				Indent; w.пСтроку32_вЮ8(Лит32("ELSE")); w.пВК_ПС;
				DumpStatementSequence(s(TS.CASEStatement).else)
			всё;
			Indent; w.пСтроку32_вЮ8(Лит32("END"))
		всё;
		если (s.next # НУЛЬ) и ~(s.next суть TS.EmptyStatement) то w.пСтроку32_вЮ8(Лит32(";")) всё;
		если s.postComment # НУЛЬ то DumpComments(s.postComment); всё;
		если ~(s суть TS.EmptyStatement) то w.пВК_ПС всё;
		s := s.next
	кц
	;умень(indent)

кон DumpStatementSequence;


проц DumpProcDecl(p : TS.ProcDecl);
перем s : TS.Statement;
	cur : TS.NamedObject; i : цел32;
нач
	если p.preComment # НУЛЬ то
		DumpComments(p.preComment);
	всё;
	Indent; w.пСтроку32_вЮ8(Лит32("PROCEDURE ")); w.пСтроку32_вЮ8(p.name^);

	если (p.signature # НУЛЬ) и (p.signature.params # НУЛЬ) то
		нцДля i := 0 до p.signature.params.nofObjs - 1 делай
			cur := p.signature.params.objs[i];
			w.пСтроку32_вЮ8(cur.name^);
		кц
	всё;
	w.пВК_ПС;

	увел(indent); DumpDeclarations(p.scope); умень(indent);

	если p.scope.ownerBody # НУЛЬ то
		w.пСтроку32_вЮ8(Лит32("BEGIN")); w.пВК_ПС;
		s := p.scope.ownerBody;
		DumpStatementSequence(s)
	всё;
	Indent; w.пСтроку32_вЮ8(Лит32("END ")); w.пСтроку32_вЮ8(p.name^); w.пВК_ПС; w.пВК_ПС;
кон DumpProcDecl;

проц DumpDeclarations(d : TS.Scope);
перем i : цел32;
	last, cur : TS.NamedObject;
нач
	если d = НУЛЬ то возврат всё;
	нцДля i := 0 до d.elements.nofObjs - 1 делай
		cur := d.elements.objs[i];
		если cur суть TS.Const то
			если (last = НУЛЬ) или ~(last суть TS.Const) то
				если last # НУЛЬ то w.пВК_ПС всё;
				Indent;	w.пСтроку32_вЮ8(Лит32("CONST")); w.пВК_ПС
			всё;
			w.пСимв8(09X); DumpConst(cur(TS.Const))
		аесли cur суть TS.TypeDecl то
			если (last = НУЛЬ) или ~(last суть TS.TypeDecl) то
				если last # НУЛЬ то w.пВК_ПС всё;
				Indent;	w.пСтроку32_вЮ8(Лит32("TYPE")); w.пВК_ПС
			всё;
			w.пСимв8(09X);DumpTypeDecl(cur(TS.TypeDecl));
		аесли cur суть TS.Var то
			если (last = НУЛЬ) или ~(last суть TS.Var) то
				если last # НУЛЬ то w.пВК_ПС всё;
				Indent;	w.пСтроку32_вЮ8(Лит32("VAR")); w.пВК_ПС
			всё;
			w.пСимв8(09X); DumpVar(cur(TS.Var))
		аесли cur суть TS.ProcDecl то
			если last # НУЛЬ то w.пВК_ПС всё;
			DumpProcDecl(cur(TS.ProcDecl))
		аесли cur суть TS.Import то
		всё;
		last := cur;
	кц
кон DumpDeclarations;

проц DumpM*(m : TS.Module);
перем i : цел32;
нач
	w.пСтроку32_вЮ8(Лит32("MODULE ")); w.пСтроку32_вЮ8(m.name^); w.пВК_ПС;
	w.пВК_ПС;
(*	IF m.imports.nofObjs > 0 THEN
		w.UCS32StringJQvUTF8(Лит32("IMPORT "));
		FOR i := 0 TO m.imports.nofObjs - 1 DO
			w.UCS32StringJQvUTF8(m.imports.objs[i](TS.Import).import^);
			IF i < m.imports.nofObjs - 1 THEN w.UCS32StringJQvUTF8(Лит32(", ")) END
		END;
		w.UCS32StringJQvUTF8(Лит32(";")); w.Ln;
	END; *)

	DumpDeclarations(m.scope);

	w.пСтроку32_вЮ8(Лит32("END ")); w.пСтроку32_вЮ8(m.name^ ); w.пВК_ПС; w.ПротолкниБуферВПоток
кон DumpM;


проц Dump*(par : динамическиТипизированныйУкль) : динамическиТипизированныйУкль;
нач

	возврат НУЛЬ
кон Dump;


проц Open*(name : массив из симв8);
нач
	нов(ml, name, w)
кон Open;


проц OpenUCS32*(name : массив из симв32);
перем nameUTF8 : укль на массив из симв8;
нач
	nameUTF8 := UCS32.JQvNovU8(name);
	нов(ml, nameUTF8^, w)
кон OpenUCS32;



кон TFDumpTSUCS32.
