модуль WMMessages; (** AUTHOR "TF"; PURPOSE "Support for messages and events"; *)

использует
	Strings, ЛогЯдра, Objects, Kernel, Locks, Modules, Reflection, НИЗКОУР, D:= Debugging;

конст
	InitialMsgQSize = 64;
	MaxMsgQSize = 32*1024; (* this is too huge anyway *)
	(** Predefined Messages *)
	MsgPointer* = 0; MsgKey* = 2; MsgClose* = 3; MsgStyleChanged* = 4;
	MsgFocus* = 5; MsgExt* = 6; MsgDrag* = 7;
	MsgInvokeEvent* = 8;
	MsgResized* = 9;
	MsgSetLanguage* = 10;
	MsgInvalidate*= 11;
	MsgSerialize*=12;
	MsgMerged*=15;

	MsgSubPointerMove* = 0; MsgSubPointerDown* = 1; MsgSubPointerUp* = 2; MsgSubPointerLeave* = 3;
	MsgSubFocusGot* = 0; MsgSubFocusLost* = 1; MsgSubMasterFocusGot* = 2; MsgSubMasterFocusLost* = 3;

	MsgSubAll*=0; MsgSubRectangle*=1; MsgSubNothing*=2; (* regions: all or rectangle as defined by x, y, dx, dy *)

	MsgDragOver* = 0; MsgDragDropped* = 1;

	MsgSubSerializeView*=0; MsgSubSerializeComponent*=1; MsgSubSerializeData*=2;

	(** Gather statistics about added/discarded messages? *)
	Statistics* = ложь;
	TraceQueue = ложь;
	MsgTypeMax* = 13;


тип
	(** Generic Component Command *)
	CompCommand* = проц  { делегат } (sender, par : динамическиТипизированныйУкль);
	String* = Strings.String;

	(** Generic message structure *)
	Message* = запись
		originator*, (** the originator if # NIL passes information about the view that directly or indirectely lead to the msg *)
		sender* : динамическиТипизированныйУкль; (** is the sender component. If the message is originated form a component *)
		token- : AsyncToken;
		event* : CompCommand;
		msgType*, msgSubType* : цел32; (** generic message type *)
		x*, y*, z*, dx*, dy*, dz* : размерМЗ; (** in keyboard messages : ucs value in x, keysym in y *)
		flags* : мнвоНаБитахМЗ; (** in pointer messages : keys in flags *)
		ext* : динамическиТипизированныйУкль; (** extended message *)
	кон;

	MessageExtension* = укль на запись кон;

	(** AsyncToken can be used to synchronize asynchronous method invocation *)
	AsyncToken* = окласс
	перем
		ready : булево;
		result* : динамическиТипизированныйУкль;

		(** Reset is called in case the token was recycled *)
		проц Reset*;
		нач {единолично}
			ready := ложь;
		кон Reset;

		(** wait until the result is completed *)
		проц AwaitCompletion*;
		нач {единолично}
			дождись(ready)
		кон AwaitCompletion;

		(** Return if the result is completed *)
		проц IsCompleted*():булево;
		нач {единолично}
			возврат ready
		кон IsCompleted;

		(** Called by the asynchronous process to indicate the result is available *)
		проц Completed*;
		нач {единолично}
			ready := истина
		кон Completed;
	кон AsyncToken;
	
проц GetAsyncToken*() : AsyncToken;
перем рез : AsyncToken;
нач 
	нов(рез);
	возврат рез кон GetAsyncToken;


тип
	(** Message handler that can be called from the sequencer *)
	MessageHandler* = проц {делегат} (перем msg : Message);

	(** The TrapHandler must return TRUE if the process should restart. Otherwise the process is stopped *)
	TrapHandler* = проц {делегат} () : булево;

	MsgQ = окласс
	перем
		head, num: размерМЗ;
		msgQ: укль на массив из Message;
		owner: MsgSequencer;

		проц &InitQ(o: MsgSequencer; size: размерМЗ);
		нач
			head := 0; num := 0;
			нов(msgQ, size);
			сам.owner := o;
		кон InitQ;

		проц Grow(trace: булево);
		перем new: укль на массив (* MsgQSize*)  из Message; i: размерМЗ; name: массив 128 из симв8; перем pc: адресВПамяти;
			type: Modules.TypeDesc; msg: Message;
		нач
			нов(new, длинаМассива(msgQ) * 3 DIV 2);
			нцДля i := 0 до длинаМассива(msgQ)-1 делай
				new[i] := msgQ[(head+i) остОтДеленияНа длинаМассива(msgQ)];
				если trace то
					msg := new[i];
					если msg.msgType < длинаМассива(MsgName) то копируйСтрокуДо0(MsgName[msg.msgType], name) иначе name := "" всё;
					трассируй(i,"***************", name);
					трассируй(i, msg.msgType, msg.msgSubType);
					трассируй(msg.x, msg.y, msg.dx, msg.dy, msg.flags);
					если msg.sender # НУЛЬ то
						type := Modules.TypeOf(msg.sender);
						если (type # НУЛЬ) то
							копируйСтрокуДо0(type.mod.name, name); Strings.Append(name, "."); Strings.Append(name, type.name);
							трассируй(msg.sender, name);
						иначе
							трассируй(msg.sender);
						всё;
					всё;
					если msg.msgType = MsgInvokeEvent то
						Reflection.GetProcedureName(адресВПамяти(msg.event), name, pc );
						трассируй("Event procedure ", name);
					всё;
					если msg.ext # НУЛЬ то
						type := Modules.TypeOf(msg.ext);
						если (type # НУЛЬ) то
							копируйСтрокуДо0(type.mod.name, name); Strings.Append(name, "."); Strings.Append(name, type.name);
							трассируй(msg.ext, name);
						иначе
							трассируй(msg.ext);
						всё;
					всё;
				всё;
			кц;
			msgQ := new;	head := 0;
			ЛогЯдра.пСтроку8("MessageQ increased: "); ЛогЯдра.пЦел64(длинаМассива(msgQ),1); ЛогЯдра.пВК_ПС;
		кон Grow;

		(** Add a message to a queue. Discards the message if the queue is full *)
		проц Add*(перем msg : Message; debug:булево): булево;
		перем i, pos: размерМЗ; name: массив 256 из симв8; pc: адресВПамяти;
		type: Modules.TypeDesc;
		конст
			MergePointers = истина;
			MergeInvalidates = истина;
			MergeInvokeEvents = ложь;

			проц Merge(перем x,y,dx,dy: размерМЗ; X,Y,dX,dY: размерМЗ);
			перем nx, ny, ndx, ndy: размерМЗ;
			нач
				nx := матМинимум(x,X);
				ny := матМинимум(y,Y);
				ndx := матМаксимум(x+dx, X+dX) - nx;
				ndy := матМаксимум(y+dy, Y+dY) - ny;
				x := nx;
				y := ny;
				dx := ndx;
				dy := ndy;
			кон Merge;


		нач
			если debug то
				ЛогЯдра.пСтроку8("<----");
				если msg.msgType < длинаМассива(MsgName) то копируйСтрокуДо0(MsgName[msg.msgType], name) иначе name := "" всё;
				трассируй("WMMessages.MsgSequencer.Add", name);
				трассируй(num, msg.msgType, msg.msgSubType);
				трассируй(msg.x, msg.y, msg.dx, msg.dy);
				если msg.sender # НУЛЬ то
					type := Modules.TypeOf(msg.sender);
					если (type # НУЛЬ) то
						копируйСтрокуДо0(type.mod.name, name); Strings.Append(name, "."); Strings.Append(name, type.name);
						трассируй(msg.sender, name);
					иначе
						трассируй(msg.sender);
					всё;
				всё;
				если msg.msgType = MsgInvokeEvent то
					Reflection.GetProcedureName(адресВПамяти(msg.event), name, pc );
					трассируй("Event procedure ", name);
				всё;
				если msg.ext # НУЛЬ то
					type := Modules.TypeOf(msg.ext);
					если (type # НУЛЬ) то
						копируйСтрокуДо0(type.mod.name, name); Strings.Append(name, "."); Strings.Append(name, type.name);
						трассируй(msg.ext, name);
					иначе
						трассируй(msg.ext);
					всё;
				всё;
				(*D.TraceBack;*)
			всё;

			если MergePointers и (msg.msgType = MsgPointer) и (msg.msgSubType = MsgSubPointerMove) и (num > 0) то  (* reduce pointer moves in buffer *)
				i := num - 1;
				нцПока i >= 0 делай
					pos := (head + i) остОтДеленияНа длинаМассива(msgQ);
					если (msgQ[pos].msgType = MsgPointer) и (msgQ[pos].msgSubType = MsgSubPointerMove) и (msgQ[pos].flags = msg.flags) то
						msgQ[pos].x := msg.x;
						msgQ[pos].y := msg.y;
						msgQ[pos].z := msg.z;
						возврат истина
					всё;
					умень(i)
				кц
			всё;



			(* filter out duplicate MsgInvokeEvents - was a hack (but effecting in avoiding MessageQ congestion)*)
			если MergeInvokeEvents и (msg.msgType = MsgInvokeEvent) и (msg.msgSubType = 0) и (num > 0) то
				i := num - 1;
				нцПока i >= 0 делай
					pos := (head + i) остОтДеленияНа длинаМассива(msgQ);
					если (msgQ[pos].msgType = MsgInvokeEvent) и (msgQ[pos].msgSubType = 0) и (msgQ[pos].event = msg.event) и (msgQ[pos].sender = msg.sender) и (msgQ[pos].ext = msg.ext) то
						msgQ[pos].msgType := MsgMerged;
					всё;
					умень(i)
				кц
			всё;

			если MergeInvalidates и (msg.msgType = MsgInvalidate) и (num > 0)  то
				i := num-1;
				pos := (head + i) остОтДеленияНа длинаМассива(msgQ);
				если (msgQ[pos].sender = msg.sender) и (msgQ[pos].msgType = MsgInvalidate) и (msgQ[pos].msgSubType = msg.msgSubType) то
					если msg.msgSubType= MsgSubRectangle то
						если Contained(msgQ[pos], msg) то
							если TraceQueue или debug то
								трассируй("container first ", msg.x, msg.dx, msg.y, msg.dy);
								трассируй(msgQ[pos].x, msgQ[pos].dx, msgQ[pos].y, msgQ[pos].dy);
								ЛогЯдра.пВК_ПС;
							всё;
							(* replace *)
							msgQ[pos].x := msg.x; msgQ[pos].y := msg.y; msgQ[pos].dx := msg.dx; msgQ[pos].dy := msg.dy;
							возврат истина;
						аесли Contained(msg, msgQ[pos]) то
							если TraceQueue или debug то
								трассируй("contained first ", msg.x, msg.dx, msg.y, msg.dy);
								трассируй(msgQ[pos].x, msgQ[pos].dx, msgQ[pos].y, msgQ[pos].dy);
								ЛогЯдра.пВК_ПС;
							всё;
							(* keep *)
							возврат истина;
						иначе (* we assume that invaidates on the same component
									that immediately follow each other are very close to each other
									If this turns out to be untrue, we could add a heuristics here *)
								Merge(msgQ[pos].x, msgQ[pos].y, msgQ[pos].dx, msgQ[pos].dy, msg.x, msg.y, msg.dx, msg.dy);
								(* keep *)
								возврат истина;
						всё;
					аесли msg.msgSubType = MsgSubAll то
						(* keep *)
						если TraceQueue или debug то
							трассируй("keep first");
							ЛогЯдра.пВК_ПС;
						всё;
						возврат истина;
					всё;
				всё;

				умень(i);
				нцПока i >= 0 делай
					pos := (head + i) остОтДеленияНа длинаМассива(msgQ);
					если (msgQ[pos].sender = msg.sender) и (msgQ[pos].msgType = MsgInvalidate) и (msgQ[pos].msgSubType = msg.msgSubType) то
						если msg.msgSubType= MsgSubRectangle то
							если Contained(msgQ[pos], msg) то
								если TraceQueue или debug то
									трассируй("container  ", pos);
									трассируй( msg.x, msg.dx, msg.y, msg.dy);
									трассируй(msgQ[pos].x, msgQ[pos].dx, msgQ[pos].y, msgQ[pos].dy);
								всё;

								msgQ[pos].msgType := MsgMerged;
								i := 0;
							аесли Contained(msg, msgQ[pos]) то
								если TraceQueue или debug  то
									трассируй("contained  ", pos);
									трассируй(msg.x, msg.dx, msg.y, msg.dy);
									трассируй(msgQ[pos].x, msgQ[pos].dx, msgQ[pos].y, msgQ[pos].dy);
								всё;

								msg.x := msgQ[pos].x; msg.y := msgQ[pos].y; msg.dx := msgQ[pos].dx; msg.dy := msgQ[pos].dy;
								msgQ[pos].msgType := MsgMerged;
								i := 0;
							(*ELSE
								Merge(msg.x, msg.y, msg.dx, msg.dy, msgQ[pos].x, msgQ[pos].y, msgQ[pos].dx, msgQ[pos].dy);
								msgQ[pos].msgSubType := MsgSubNothing;
							*)
							всё;
						аесли msgQ[pos].msgSubType = MsgSubAll то
							если TraceQueue или debug  то
								трассируй("replace ", pos);
							всё;
								msgQ[pos].msgType := MsgMerged;
							i := 0;
						всё;
					всё;
					умень(i);
				кц;
			всё;


			если num >= MaxMsgQSize то возврат ложь всё;
			если num >= длинаМассива(msgQ) то
				Grow(debug)
			всё;
			если Statistics то
				увел(messagesAdded);
				если (msg.msgType >= 0) и (msg.msgType < MsgTypeMax) то
					увел(messagesAddedByType[msg.msgType]);
				всё;
			всё;
			msgQ[(head + num) остОтДеленияНа длинаМассива(msgQ)] := msg; увел(num);

			если debug  то
				ЛогЯдра.пВК_ПС;
			всё;

			возврат истина;
		кон Add;

		(* Remove a message from the queue. Block if no message is available but awake if queue is terminated by call to Stop *)
		(* return if alive *)
		проц Get(перем msg : Message; debug: булево) : булево;
		перем i: цел32; name: массив 256 из симв8; pc: адресВПамяти;
			type: Modules.TypeDesc;
		нач
			msg := msgQ[head];
			(* clear references from the queue *)
			msgQ[head].originator := НУЛЬ;
			msgQ[head].sender := НУЛЬ;
			msgQ[head].ext := НУЛЬ;

			head := (head + 1)  остОтДеленияНа длинаМассива(msgQ);
			умень(num);
			owner.originator := msg.originator;

			если debug то
				ЛогЯдра.пСтроку8("---->");
				если msg.msgType < длинаМассива(MsgName) то копируйСтрокуДо0(MsgName[msg.msgType], name) иначе name := "" всё;
				трассируй("WMMessages.MsgSequencer.Get", name);
				трассируй(i, msg.msgType, msg.msgSubType);
				трассируй(msg.x, msg.y, msg.dx, msg.dy);
				если msg.sender # НУЛЬ то
					type := Modules.TypeOf(msg.sender);
					если (type # НУЛЬ) то
						копируйСтрокуДо0(type.mod.name, name); Strings.Append(name, "."); Strings.Append(name, type.name);
						трассируй(msg.sender, name);
					иначе
						трассируй(msg.sender);
					всё;
				всё;
				если msg.msgType = MsgInvokeEvent то
					Reflection.GetProcedureName(адресВПамяти(msg.event), name, pc );
					трассируй("Event procedure ", name);
				всё;
				если msg.ext # НУЛЬ то
					type := Modules.TypeOf(msg.ext);
					если (type # НУЛЬ) то
						копируйСтрокуДо0(type.mod.name, name); Strings.Append(name, "."); Strings.Append(name, type.name);
						трассируй(msg.ext, name);
					иначе
						трассируй(msg.ext);
					всё;
				всё;
				ЛогЯдра.пВК_ПС;
			всё;


			возврат истина
		кон Get;

	кон MsgQ;

	(** Message sequencer *)
	MsgSequencer* = окласс(Modules.ЛХА)
	перем
		msgQ: MsgQ;
		invalidateQ: MsgQ;
		alive, continue, waiting, stopped: булево;
		msg : Message;
		handler : MessageHandler;
		originator : динамическиТипизированныйУкль;
		me : динамическиТипизированныйУкль; (* Thread for caller identification *)
		lock- : Locks.RWLock;
		th, traphandler : TrapHandler;
		name- : String;

		проц &New*(handler : MessageHandler; вхИмя : Strings.String);
		нач
			сам.handler := handler;
			нов(lock);
			originator := НУЛЬ; me := НУЛЬ; th := НУЛЬ; traphandler := НУЛЬ;
			name := вхИмя;
			alive := ложь; continue := истина;
			waiting := ложь; stopped := ложь;
			нов(invalidateQ, сам, InitialMsgQSize);
			нов(msgQ, сам, InitialMsgQSize);
		кон New;

		(** Add a trap handler for this process. This handler only decides whether to continue or to abort the process.
			If continued, the lock will be reset *)
		проц SetTrapHandler*(th : TrapHandler);
		нач {единолично}
			traphandler := th
		кон SetTrapHandler;

		(** Return true if called from (this) sequencer *)
		проц IsCallFromSequencer*() : булево;
		нач
			возврат Objects.ActiveObject() = me
		кон IsCallFromSequencer;

		(** RETURN the originator (view) of the message that lead directly or indirectly to this request.
			Returns NIL if the call is not from the sequencer  *)
		проц GetOriginator*() : динамическиТипизированныйУкль;
		нач
			если Objects.ActiveObject() = me то возврат originator
			иначе возврат НУЛЬ
			всё
		кон GetOriginator;

		(** Add a message to a queue. Discards the message if the queue is full *)
		проц Add*(перем msg : Message): булево;
		нач {единолично}
			если msg.msgType = MsgInvalidate то
				возврат invalidateQ.Add(msg, TraceQueue или (debug = сам));
			иначе
				возврат msgQ.Add(msg, TraceQueue или (debug = сам));
			всё;
		кон Add;

		проц Handle(перем msg : Message) : булево;
		нач
			(* if asynchronous call --> synchronize *)
			если ~IsCallFromSequencer() то
				если Add(msg) то возврат истина всё;
			иначе
				(*
				IF debug = SELF THEN
					D.Enter;
					D.Ln;
					D.String("-- WMMessages.MsgSequencer.Handle --"); D.Ln;
					D.String("msg type "); D.Int(msg.msgType,1); D.Ln;
					D.String("time "); D.Int(Kernel.GetTicks(),1);D.Ln;
					D.Exit;
				END;
				*)

				если msg.msgType = MsgInvokeEvent то (* MsgInvokeEvent *)
					если msg.event # НУЛЬ то
						msg.event(msg.sender, msg.ext);
						если msg.token # НУЛЬ то msg.token.Completed всё
					всё
				иначе handler(msg) (* Generic message *)
				всё;
				(* clear references *)
				msg.originator := НУЛЬ;
				msg.sender := НУЛЬ;
				msg.ext := НУЛЬ;
				originator := НУЛЬ;
				возврат истина
			всё;
			возврат ложь
		кон Handle;

		(* put event into message queue. См. также WMComponents.Component.ДождисьОбработкиСообщенияВСвоёмДиспетчереСообщений и WMWindowManager.ДождисьОбработкиСообщенияВСвоёмДиспетчереСообщений *)
		проц ScheduleEvent*(event : CompCommand; sender, par : динамическиТипизированныйУкль);
		перем invokeMsg : Message;
		нач
			invokeMsg.msgType := MsgInvokeEvent;
			invokeMsg.sender := sender; invokeMsg.ext := par;
			invokeMsg.event := event;
			если ~Handle(invokeMsg) то всё
		кон ScheduleEvent;
		
		(* возвращает AsyncToken, у которого надо вызвать AwaitCompletion - и тогда можно дождаться обработки сообщения.
		Возврат НУЛЬ означает, что не удалось добавить сообщение в очередь *)
		проц ScheduleEventWithAsyncToken*(event: CompCommand; sender, par : динамическиТипизированныйУкль): AsyncToken;
		перем рез: AsyncToken; invokeMsg : Message;
		нач
			утв(~IsCallFromSequencer());
			рез := GetAsyncToken();
			invokeMsg.msgType := MsgInvokeEvent;
			invokeMsg.sender := sender; invokeMsg.ext := par;
			invokeMsg.event := event;
			invokeMsg.token := рез;
			если ~Add(invokeMsg) то
				возврат НУЛЬ
			иначе
				возврат рез всё кон ScheduleEventWithAsyncToken;

		(** Stop the message sequencer. Must be called if the queue is no longer needed *)
		проц Stop*;
		нач {единолично}
			alive := ложь; stopped := истина;
		кон Stop;

		проц WaitFree*;
		нач {единолично}
			дождись (waiting и (msgQ.num = 0) и (invalidateQ.num = 0) или ~alive)
		кон WaitFree;

		(* Remove a message from the queue. Block if no message is available but awake if queue is terminated by call to Stop *)
		(* return if alive *)
		проц Get(перем msg : Message) : булево;
		перем b: булево;
		нач {единолично}
			waiting := истина;
			нцДо
				дождись((msgQ.num # 0) или (invalidateQ.num # 0) или ~alive);
				waiting := ложь;
				если ~alive то возврат ложь всё;
				если (msgQ.num # 0) то
					b := msgQ.Get(msg, TraceQueue или (debug = сам));
				иначе
					 b := invalidateQ.Get(msg, TraceQueue или (debug = сам));
				всё;
			кцПри msg.msgType # MsgMerged;
			возврат b;
		кон Get;

	нач {активное, SAFE}
		(* trap occured *)
		если name # НУЛЬ то
			ПримиВ_ЛХА(Modules.ключЛХА_имяОбъекта, Modules.ЯвиКонтейнерСтроки8(name^)) всё;
		
		если alive то
			th := traphandler; ЛогЯдра.пСтроку8("WMMessages: [TRAP]"); ЛогЯдра.пВК_ПС;
			если th # НУЛЬ то continue := th() иначе continue := ложь всё;
			если continue то lock.Reset иначе возврат всё;
		всё;
		alive := истина; me := Objects.ActiveObject();
		(* Message processing loop *)
		нцПока Get(msg) делай
			lock.AcquireWrite;
			(* Check alive again for the case that the sequencer has been stopped just after Get(msg) returned
			but before the lock could be acquired (WMComponents.FormWindow holds that lock when calling Sequencer.Stop) *)
			если alive то
				если ~Handle(msg) то ЛогЯдра.пСтроку8("WMMessages: A msg was not handled... "); ЛогЯдра.пВК_ПС; всё;
			всё;
			lock.ReleaseWrite
		кц;
		ПримиВ_ЛХА(Modules.ключЛХА_имяОбъекта, Modules.ЯвиКонтейнерСтроки8("MsgSequencer Закончился"));
	кон MsgSequencer;

перем
	 (* Statistics *)
	 messagesAddedByType- : массив MsgTypeMax из цел32;
	 messagesAdded- : цел32;
	 messagesDiscarded- : цел32;

	 debug*: динамическиТипизированныйУкль;
	 MsgName: массив 32 из массив 32 из симв8;

проц Contained(конст this, container: Message): булево;
нач
	возврат (container.x <= this.x) и (container.dx >= this.dx) и (container.y <= this.y) и (container.dy >= this.dy)
кон Contained;

нач
	MsgName[MsgPointer] := "MsgPointer";
	MsgName[MsgKey] := "MsgKey";
	MsgName[MsgClose] := "MsgClose";
	MsgName[MsgStyleChanged] := "MsgStyleChanged";
	MsgName[MsgFocus] := "MsgFocus";
	MsgName[MsgExt] := "MsgExt";
	MsgName[MsgDrag] := "MsgDrag";
	MsgName[MsgInvokeEvent] := "MsgInvokeEvent";
	MsgName[MsgResized] := "MsgResized" ;
	MsgName[MsgSetLanguage] := "MsgSetLanguage";
	MsgName[MsgInvalidate] := "MsgInvalidate";
	MsgName[MsgSerialize] := "MsgSerialize";
кон WMMessages.

