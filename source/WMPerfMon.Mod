модуль WMPerfMon; (** AUTHOR "TF/staubesv"; PURPOSE "Performance Monitor"; *)

использует
	ЛогЯдра, Modules, Plugins, Strings, Network,
	WMRectangles, WMGraphics, WMMessages, WMWindowManager, WMRestorable, WMComponents, WMStandardComponents,
	WMTabComponents, WMProcessInfo,
	WMPerfMonPlugins, WMPerfMonComponents, WMPerfMonTabAlerts, WMPerfMonTabSystem,
	WMPerfMonPluginMemory, WMPerfMonPluginCpu, WMPerfMonPluginNetwork;

конст

	(* Startup width and height of Performance Monitor window *)
	DefaultWidth = 750; DefaultHeight = 400;

	(* Scale window if its smaller than this size *)
	MinWidth = 150; MinHeight = 150;

	NbrOfTabs = 6;

тип

	(* Container containing CpuLoad plugins for each CPU and the memory statistics plugin *)
	CpuMemoryComponent =  окласс(WMPerfMonComponents.PluginContainer)
	перем
		cpu : укль на массив из WMPerfMonPluginCpu.CpuLoad;
		ml : WMPerfMonPluginMemory.MemoryLoad;

		проц {перекрыта}LocatePlugins*() : булево;
		перем
			par : WMPerfMonPlugins.Parameter;
			cpar : WMPerfMonPluginCpu.CpuParameter;
			i : размерМЗ;
		нач
			нов(cpu, WMPerfMonPluginCpu.nbrOfCpus);
			нцДля i := 0 до длинаМассива(cpu)-1 делай
				нов(cpar); cpar.processorID := i; cpar.hide := истина; нов(cpu[i], cpar);
				AddPlugin(cpu[i], WMPerfMonComponents.GraphView);
			кц;
			нов(par); par.hide := истина; нов(ml, par); AddPlugin(ml, WMPerfMonComponents.GraphView);
			возврат истина;
		кон LocatePlugins;

		проц {перекрыта}Finalize*;
		перем i : размерМЗ;
		нач
			Finalize^;
			нцДля i := 0 до длинаМассива(cpu)-1 делай WMPerfMonPlugins.updater.RemovePlugin(cpu[i]); кц;
			WMPerfMonPlugins.updater.RemovePlugin(ml);
		кон Finalize;

	кон CpuMemoryComponent;

	(* Container containing NetSend/NetReceive plugins for each link device *)
	NetworkComponent = окласс(WMPerfMonComponents.PluginContainer)
	перем
		netspeed : укль на массив из WMPerfMonPluginNetwork.NetworkSpeed;

		проц {перекрыта}LocatePlugins*() : булево;
		перем
			npar : WMPerfMonPluginNetwork.NetParameter;
			table : Plugins.Table; dev : Network.LinkDevice;
			i : размерМЗ;
		нач
			Network.registry.GetAll(table);
			если table # НУЛЬ то
				нов(netspeed, длинаМассива(table));
				нцДля i := 0 до длинаМассива(table)-1 делай
					dev := table[i] (Network.LinkDevice);
					нов(npar); npar.hide := истина; npar.device := dev; нов(netspeed[i], npar); AddPlugin(netspeed[i], WMPerfMonComponents.GraphView);
				кц;
				возврат истина;
			иначе
				netspeed := НУЛЬ;
				возврат ложь;
			всё;
		кон LocatePlugins;

		проц {перекрыта}Finalize*;
		перем i : размерМЗ;
		нач
			Finalize^;
			если (netspeed # НУЛЬ) то
				нцДля i := 0 до длинаМассива(netspeed)-1 делай
					WMPerfMonPlugins.updater.RemovePlugin(netspeed[i]);
				кц;
			всё;
		кон Finalize;

	кон NetworkComponent;

тип

	KillerMsg = окласс
	кон KillerMsg;

	Window = окласс (WMComponents.FormWindow)
	перем
		tabs : WMTabComponents.Tabs;
		tabList : массив NbrOfTabs из WMTabComponents.Tab;
		tabPanels : массив NbrOfTabs из WMComponents.VisualComponent;

		tabPanel : WMStandardComponents.Panel;
		curTabPanel : WMComponents.VisualComponent;
		curTab : WMTabComponents.Tab;

		cpuMemory : CpuMemoryComponent;
		network : NetworkComponent;
		selection : WMPerfMonComponents.SelectionComponent;

		width, height : размерМЗ;

		проц {перекрыта}Resized*(width, height : размерМЗ);
		нач
			если (width >= MinWidth) и (height >= MinHeight) то
				scaling := ложь;
				сам.width := width; сам.height := height;
			иначе
				scaling := истина;
			всё;
			Resized^(width, height);
		кон Resized;

		проц CreateForm(): WMComponents.VisualComponent;
		перем
			panel : WMStandardComponents.Panel;
			alerts : WMPerfMonTabAlerts.AlertsTab; systemPanel : WMPerfMonTabSystem.SystemTab;
			processManager : WMProcessInfo.ProcessManager;
			caption : Strings.String; i : цел32;
		нач
			panel := WMPerfMonComponents.NewPanel(WMComponents.AlignClient, 0, 0);

			нов(tabs);  tabs.bounds.SetHeight(20); tabs.alignment.Set(WMComponents.AlignTop);
			tabs.onSelectTab.Add(TabSelected);
			если ~WMPerfMonComponents.UseSkinColors то
				tabs.clTextDefault.Set(WMGraphics.White);
				tabs.clTextHover.Set(WMGraphics.Black);
				tabs.clTextSelected.Set(WMGraphics.Black);
				tabs.clHover.Set(WMGraphics.White);
				tabs.clSelected.Set(WMGraphics.White);
			всё;
			panel.AddContent(tabs);

			нов(tabPanel); tabPanel.alignment.Set(WMComponents.AlignClient);
			panel.AddContent(tabPanel);

			нов(cpuMemory); tabPanels[0] := cpuMemory; cpuMemory.bearing.Set(WMRectangles.MakeRect(2,5,2,2));
			нов(network); tabPanels[1] := network; network.bearing.Set(WMRectangles.MakeRect(2,5,2,2));
			нов(systemPanel); tabPanels[2] := systemPanel;
			нов(processManager); tabPanels[3] := processManager;
			нов(selection); tabPanels[4] := selection;
			нов(alerts); tabPanels[5] := alerts;

			нцДля i := 0 до NbrOfTabs-1 делай
				tabPanels[i].alignment.Set(WMComponents.AlignClient);
				tabPanels[i].visible.Set(ложь);
				tabList[i] := tabs.NewTab(); tabs.AddTab(tabList[i]);
				если ~WMPerfMonComponents.UseSkinColors то tabs.SetTabColor(tabList[i], WMGraphics.Black); всё;
				просей i из
					(* Note: Characters used for caption must be allowed in XML attributes *)
					|0: caption := Strings.NewString("CPU/Memory");
					|1: caption := Strings.NewString("Network");
					|2: caption := Strings.NewString("System");
					|3: caption := Strings.NewString("Processes");
					|4: caption := Strings.NewString("Plugins");
					|5: caption := Strings.NewString("Alerts");
				иначе
					caption := Strings.NewString("Unnamed");
				всё;
				tabs.SetTabCaption(tabList[i], caption);
				tabs.SetTabData(tabList[i], tabPanels[i]);
				tabPanel.AddContent(tabPanels[i]);
			кц;

			curTabPanel := cpuMemory; curTab := tabList[0]; curTabPanel.visible.Set(истина);
			возврат panel
		кон CreateForm;

		проц TabSelected(sender, data : динамическиТипизированныйУкль);
		перем tab : WMTabComponents.Tab;
		нач
			если (data # НУЛЬ) и (data суть WMTabComponents.Tab) то
				DisableUpdate;
				tab := data(WMTabComponents.Tab);
				если (tab.data # НУЛЬ) и (tab.data суть WMComponents.VisualComponent) то
					curTabPanel.visible.Set(ложь);
					curTab := tab;
					curTabPanel := tab.data(WMComponents.VisualComponent);
					curTabPanel.visible.Set(истина);
					tabPanel.Reset(сам, НУЛЬ);
					tabPanel.AlignSubComponents;
				всё;
				EnableUpdate;
				tabPanel.Invalidate;
				curTabPanel.Invalidate;
			всё;
		кон TabSelected;

		проц SelectTabByName(конст name : массив из симв8);
		перем i : цел32;
		нач
			нц
				если i >= длинаМассива(tabList) то прервиЦикл; всё;
				если (tabList[i] # НУЛЬ) и (Strings.Match(name, tabList[i].caption^)) то прервиЦикл; всё;
				увел(i);
			кц;
			если i < длинаМассива(tabList) то tabs.Select(tabList[i]); TabSelected(сам, tabList[i]); всё;
		кон SelectTabByName;

		проц {перекрыта}Handle*(перем x: WMMessages.Message);
		перем elem, data : WMRestorable.XmlElement;
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) то
				если x.ext суть WMRestorable.Storage то
					нов(data); data.SetName("Data");
					нов(elem); elem.SetName("Configuration"); data.AddContent(elem);
					WMRestorable.StoreLongint(elem, "SampleInterval", WMPerfMonPlugins.updater.sampleInterval);
					WMRestorable.StoreLongint(elem, "SampleBufferSize", WMPerfMonPlugins.updater.sampleBufferSize);
					WMRestorable.StoreLongint(elem, "ScreenInterval", WMPerfMonPlugins.updater.screenInterval);
					WMRestorable.StoreString(elem, "CurrentTab", curTab.caption^);
					нов(elem); elem.SetName("Size"); data.AddContent(elem);
					WMRestorable.StoreSize(elem, "Width", width);
					WMRestorable.StoreSize(elem, "Height", height);
					x.ext(WMRestorable.Storage).Add("WMPerfMon", "WMPerfMon.Restore", сам, data);
				аесли x.ext суть KillerMsg то
					Close;
				иначе Handle^(x)
				всё
			иначе Handle^(x)
			всё
		кон Handle;

		проц {перекрыта}Close*;
		нач
			Close^;
			DecCount;
		кон Close;

		проц &New*(c : WMRestorable.Context);
		перем
			vc : WMComponents.VisualComponent;
			configuration, size : WMRestorable.XmlElement;
			curTabName : массив 32 из симв8;
			si, sbs, scri : цел32;
			scale : булево;
		нач
			vc := CreateForm(); scaling := ложь; scale := ложь;
			если c # НУЛЬ то
				width := c.r - c.l; height :=  c.b - c.t;
				size := WMRestorable.GetElement(c, "Data\Size");
				если size # НУЛЬ то
					WMRestorable.LoadSize(size, "Width", width);
					WMRestorable.LoadSize(size, "Height", height);
					если (width < MinWidth) или (height < MinHeight) то
						scale := истина;
					всё;
				всё;
			иначе
				width := DefaultWidth; height := DefaultHeight;
			всё;

			Init(width, height, ложь);
			SetContent(vc);
			SetTitle(Strings.NewString("Performance Monitor 2.0"));
			SetIcon(WMGraphics.LoadImage("WMIcons.tar://WMPerfMon.png", истина));

			если c # НУЛЬ то
				WMRestorable.AddByContext(сам, c);
				configuration := WMRestorable.GetElement(c, "Data\Configuration");
				если configuration # НУЛЬ то
					WMRestorable.LoadLongint(configuration, "SampleInterval", si);
					WMRestorable.LoadLongint(configuration, "SampleBufferSize", sbs);
					WMRestorable.LoadLongint(configuration, "ScreenInterval", scri);
					WMRestorable.LoadString(configuration, "CurrentTab", curTabName);
				всё;
				если (si # 0) и (sbs # 0) и (scri # 0) то
					WMPerfMonPlugins.updater.SetIntervals(si, sbs, scri);
				иначе
					ЛогЯдра.пСтроку8("WMPerfMon: Could not restore config data."); ЛогЯдра.пВК_ПС;
				всё;
				если curTabName # "" то SelectTabByName(curTabName); всё;
				если scale то Resized(c.r - c.l, c.b - c.t); всё;
			иначе
				WMWindowManager.DefaultAddWindow(сам);
				SelectTabByName("CPU/Memory");
			всё;
			IncCount;
		кон New;

	кон Window;

перем
	nofWindows : цел32;

проц Open*;
перем w : Window;
нач
	нов(w, НУЛЬ);
кон Open;

проц Restore*(context : WMRestorable.Context);
перем w : Window;
нач
	нов(w, context);
кон Restore;

проц IncCount;
нач {единолично}
	увел(nofWindows);
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows);
кон DecCount;

проц Cleanup;
перем die : KillerMsg; msg : WMMessages.Message; m : WMWindowManager.WindowManager;
нач {единолично}
	нов(die); msg.ext := die; msg.msgType := WMMessages.MsgExt;
	m := WMWindowManager.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
кон WMPerfMon.

WMPerfMon.Open ~ 	System.Free WMPerfMon ~
