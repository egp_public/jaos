модуль EFISimpleFS; (** AUTHOR "Matthias Frei"; PURPOSE "EFI Simple FS Protocol"; *)

использует
	EFI, EFIFileProtocol, НИЗКОУР;

конст
	Revision* = 00010000H;

перем
	GUID- : EFI.GUID;

тип Protocol*= укль на ProtocolDescription;

тип SFSOpenVolume* = проц{WINAPI}(This : Protocol; перем Root : EFIFileProtocol.Protocol):EFI.Status;
тип ProtocolDescription *= запись(EFI.ProtocolDescription)
	Revision-:EFI.Int64;
	OpenVolume-: SFSOpenVolume
кон;

нач
	GUID.Data1 := -69B1A4DEH; (* 964E5B22H;*)
	GUID.Data2 := 6459H;
	GUID.Data3 := 11D2H;
	GUID.Data4[0] := -72H; (*8EH;*)
	GUID.Data4[1] := 39H;
	GUID.Data4[2] := 00H;
	GUID.Data4[3] := -60H; (*0A0H;*)
	GUID.Data4[4] := -37H; (*0C9H;*)
	GUID.Data4[5] := 69H;
	GUID.Data4[6] := 72H;
	GUID.Data4[7] := 3BH;
кон EFISimpleFS.
