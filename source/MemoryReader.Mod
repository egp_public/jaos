(** AUTHOR "rg"; PURPOSE "Memory reader"; *)

модуль MemoryReader;

использует НИЗКОУР, Потоки;

конст
	DefaultReaderSize = 64;

тип
	Reader* = окласс (Потоки.Чтец)
		перем
			memoryAdr: адресВПамяти;
			available : размерМЗ;

		проц Receive(перем buf: массив из симв8; ofs, size, min : размерМЗ; перем len: размерМЗ; перем res: целМЗ);
		нач
			утв(ofs+size <= длинаМассива(buf));
			если available # 0 то
				если size >= available то len := available
				иначе len := size всё;
				НИЗКОУР.копируйПамять(memoryAdr, адресОт(buf[0])+ofs, len);
				увел(memoryAdr, len);
				умень(available, len);
				res := Потоки.Успех
			иначе
				len := 0;
				res := Потоки.КонецФайла
			всё
		кон Receive;

		проц &InitMemoryReader*(memoryAdr: адресВПамяти; length : размерМЗ);
		нач
			сам.memoryAdr := memoryAdr;
			available := length;
			новЧтец(Receive, DefaultReaderSize);
		кон InitMemoryReader;
	кон Reader;

кон MemoryReader.
