модуль TextConverter; (** AUTHOR "negelef"; PURPOSE "automated text format convertion"; *)

использует Commands, Потоки, Diagnostics, Files, Texts, TextUtilities;

тип Converter = проц (text : Texts.Text; конст filename : массив из симв8; перем res : целМЗ);

проц Convert (diagnostics: Diagnostics.Diagnostics; list: Потоки.Чтец; converter: Converter);
перем text: Texts.Text; filename: Files.FileName; format: цел32; res: целМЗ;
нач
	нцПока list.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках (filename) делай
		нов (text);
		TextUtilities.LoadAuto (text, filename, format, res);
		если res = 0 то
			converter (text, filename, res);
			если res = 0 то
				diagnostics.Information (filename, Потоки.НевернаяПозицияВПотоке, "successfully converted");
			иначе
				diagnostics.Information (filename, Потоки.НевернаяПозицияВПотоке, "failed to store");
			всё
		иначе
			diagnostics.Error (filename, Потоки.НевернаяПозицияВПотоке, "failed to load");
		всё;
	кц;
кон Convert;

(* converts the provided list of text files into the oberon format *)
проц Oberon* (context: Commands.Context);
перем diagnostics: Diagnostics.StreamDiagnostics;
нач
	нов (diagnostics, context.error);
	Convert (diagnostics, context.arg, TextUtilities.StoreOberonText);
кон Oberon;

проц UTF8*(context : Commands.Context);
перем diagnostics: Diagnostics.StreamDiagnostics;
нач
	нов (diagnostics, context.error);
	Convert (diagnostics, context.arg, TextUtilities.ExportUTF8);
кон UTF8;

кон TextConverter.
