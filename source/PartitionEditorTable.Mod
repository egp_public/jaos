модуль PartitionEditorTable; (** AUTHOR "staubesv"; PURPOSE "Partition Table Abstraction"; *)

использует
	ЛогЯдра, Plugins, Disks;

конст
	Ok* = Disks.Ok;
	DeviceNotFound* = 98;
	BlocksizeNotSupported* = 99;
	NoSignature* = 100;

	PartitionTableOffset = 01BEH;
	EntrySize = 16; (* bytes *)

	BlockSize = 512;

	(* procedure Changed: changeType parameter encoding *)
	SizeChanged* = 1;
	StartLbaChanged* = 2;
	StartChsChanged* = 3;
	EndLbaChanged* = 4;
	EndChsChanged* = 5;

тип

	Buffer* = массив BlockSize из симв8;

	Block* = запись
		lba* : цел32;
		cylinder*, head*, sector* : цел32;
	кон;

	(** Datastructure representing a slot of a partition table *)
	Partition* = запись
		flag* : симв8;
		type* : цел32;
		start*, end* : Block; (* note: end.lba is not stored in the partition table *)
		size* : цел32;
	кон;

	PartitionTable* = массив 4 из Partition;

	DiskGeometry = запись
		cylinders, headsPerCylinder, sectorsPerTrack : цел32;
	кон;

(* get a named device, necessary for ReadBlock and WriteBlock *)
проц GetDevice(конст devicename : массив из симв8) : Disks.Device;
перем plugin : Plugins.Plugin;
нач
	plugin := Disks.registry.Get(devicename);
	если (plugin # НУЛЬ) и (plugin суть Disks.Device) то
		возврат plugin (Disks.Device);
	иначе
		возврат НУЛЬ;
	всё;
кон GetDevice;

(* Get the disk geometry of a named device *)
проц GetDiskGeometry(конст devicename : массив из симв8; перем diskGeometry : DiskGeometry; перем res : целМЗ);
перем device : Disks.Device; geometry : Disks.GetGeometryMsg; ignore : целМЗ;
нач
	device := GetDevice(devicename);
	если (device # НУЛЬ) то
		device.Open(res);
		если (res = Disks.Ok) то
			device.Handle(geometry, res);
			если (res = Disks.Ok) то
				diskGeometry.cylinders := geometry.cyls;
				diskGeometry.headsPerCylinder := geometry.hds;
				diskGeometry.sectorsPerTrack := geometry.spt;
			всё;
			device.Close(ignore);
		всё;
	иначе
		res := DeviceNotFound;
	всё;
кон GetDiskGeometry;

(* read a block from device with name devicename. Returns with res = Disks.Ok, if successful *)
проц ReadBlock*(конст devicename : массив из симв8;  block : цел32; перем buffer: Buffer; перем res: целМЗ);
перем device : Disks.Device; ignore : целМЗ;
нач
	device := GetDevice(devicename);
	если (device # НУЛЬ) то
		если (device.blockSize = BlockSize) то
			device.Open(res);
			если (res = Ok) то
				device.Transfer(Disks.Read, block, 1, buffer, 0, res);
				device.Close(ignore);
			всё;
		иначе
			res := BlocksizeNotSupported;
		всё;
	иначе
		res := DeviceNotFound;
	всё;
кон ReadBlock;

(* write a block to device with name devicename. Returns with res = Disks.Ok, if successful*)
проц WriteBlock*(конст devicename : массив из симв8; block : цел32; перем buffer: Buffer; перем res: целМЗ);
перем device : Disks.Device; ignore : целМЗ;
нач
	device := GetDevice(devicename);
	если (device # НУЛЬ) то
		если (device.blockSize = BlockSize) то
			device.Open(res);
			если (res = Ok) то
				device.Transfer(Disks.Write, block, 1, buffer, 0, res);
				device.Close(ignore);
			всё;
		иначе
			res := BlocksizeNotSupported;
		всё;
	иначе
		res := DeviceNotFound;
	всё;
кон WriteBlock;

проц HasSignature*(конст buffer : Buffer) : булево;
нач
	возврат (buffer[510] = 055X) и (buffer[511] = 0AAX);
кон HasSignature;

проц WriteSignature(перем buffer : Buffer);
нач
	buffer[510] := 055X;
	buffer[511] := 0AAX;
кон WriteSignature;

проц Get4(конст buffer : массив из симв8;  offset : цел32): цел32;
нач
	возврат кодСимв8(buffer[offset]) + арифмСдвиг(кодСимв8(buffer[offset+1]), 8) +
		арифмСдвиг(кодСимв8(buffer[offset+2]), 16) + арифмСдвиг(кодСимв8(buffer[offset+3]), 24)
кон Get4;

проц Put4(перем buffer : массив из симв8; value, offset : цел32);
перем i : цел32;
нач
	нцДля i := 0 до 3 делай
		buffer[offset + i] := симв8ИзКода(value остОтДеленияНа 256); value := value DIV 256;
	кц;
кон Put4;

(**	This procedure is called by the Partition Editor when you press the Load button. It should load the block number <block> on the
	device <devicename> and extract the partition table inside if any *)
проц LoadPartitionTable*(конст devicename : массив из симв8;  block : цел32; перем res : целМЗ) : PartitionTable;
перем pt : PartitionTable; buffer: Buffer;
нач
	Clear(pt);
	ReadBlock(devicename,block,buffer,res);
	если res = Disks.Ok то
		pt := ParseBuffer(buffer,res);
	всё;
	возврат pt;
кон LoadPartitionTable;

проц ParseBuffer*(конст buffer : Buffer; перем res : целМЗ) : PartitionTable;
перем pt : PartitionTable; entry, offset : цел32;

	проц ParseCHS(конст buffer : Buffer; offset : цел32; перем cylinder, head, sector : цел32);
	нач
		head := кодСимв8(buffer[offset]);
		sector := кодСимв8(buffer[offset+1]) остОтДеленияНа 64;
		cylinder := кодСимв8(buffer[offset+2]);
		cylinder := cylinder + ((кодСимв8(buffer[offset+1]) DIV 64) * 256)
	кон ParseCHS;

нач
	если HasSignature(buffer) то
		res := Ok;
		нцДля entry := 0 до 3 делай
			offset := PartitionTableOffset + entry * EntrySize;
			pt[entry].flag := buffer[offset + 0];
			ParseCHS(buffer, offset + 1, pt[entry].start.cylinder, pt[entry].start.head, pt[entry].start.sector);
			pt[entry].type := кодСимв8(buffer[offset + 4]);
			ParseCHS(buffer, offset + 5, pt[entry].end.cylinder, pt[entry].end.head, pt[entry].end.sector);
			pt[entry].start.lba := Get4(buffer, offset + 8);
			pt[entry].size := Get4(buffer, offset + 12);
			(* fixup: LBA of end sector *)
			если (pt[entry].size > 0) то
				pt[entry].end.lba := pt[entry].start.lba + pt[entry].size - 1;
			иначе
				pt[entry].end.lba := 0;
			всё;
		кц;
	иначе
		res := NoSignature;
	всё;
	возврат pt;
кон ParseBuffer;

(** 	This procedure is called by the Partition Editor when you press the Store button. It shall encode the given partition table <pt>  into a
	512 bytes block and store this block on the named device at block number <block> *)
проц StorePartitionTable*(конст devicename : массив из симв8; block : цел32; конст pt : PartitionTable; перем res : целМЗ);
перем buffer : Buffer;
нач
	ReadBlock(devicename,block,buffer,res);
	если res = Disks.Ok то
		WriteToBuffer(pt,buffer);
		WriteBlock(devicename,block,buffer,res);
	всё;
кон StorePartitionTable;

проц WriteToBuffer*(конст pt : PartitionTable; перем buffer : Buffer);
перем entry, offset : цел32;

	проц WriteChs(перем buffer : Buffer; offset, cylinder, head, sector : цел32);
	нач
		buffer[offset] := симв8ИзКода(head);
		buffer[offset + 1] := симв8ИзКода((sector остОтДеленияНа 64) + (cylinder DIV 64) * 64);
		buffer[offset + 2] := симв8ИзКода(cylinder);
	кон WriteChs;

нач
	WriteSignature(buffer);
	нцДля entry := 0 до 3 делай
		offset := PartitionTableOffset + entry * EntrySize;
		buffer[offset + 0] := pt[entry].flag;
		WriteChs(buffer, offset + 1, pt[entry].start.cylinder, pt[entry].start.head, pt[entry].start.sector);
		buffer[offset + 4] := симв8ИзКода(pt[entry].type);
		WriteChs(buffer, offset + 5, pt[entry].end.cylinder, pt[entry].end.head, pt[entry].end.sector);
		Put4(buffer, pt[entry].start.lba, offset + 8);
		Put4(buffer, pt[entry].size, offset + 12);
	кц;
кон WriteToBuffer;

проц Lba2Chs(lba : цел32; перем c, h, s : цел32; geometry : DiskGeometry);
перем temp : цел32;
нач
	c := lba DIV (geometry.headsPerCylinder * geometry.sectorsPerTrack);
	temp := lba остОтДеленияНа (geometry.headsPerCylinder * geometry.sectorsPerTrack);
	h := temp DIV geometry.sectorsPerTrack;
	s := temp остОтДеленияНа geometry.sectorsPerTrack + 1;
кон Lba2Chs;

проц Chs2Lba(c, h, s : цел32; перем lba : цел32; geometry : DiskGeometry);
нач
	lba := ((c * geometry.headsPerCylinder + h) * geometry.sectorsPerTrack) + s - 1;
кон Chs2Lba;

(** 	This procedure is called by the Partition Editor when the user presses the enter key on an editor component.
	Dependent of the changeType, we should now fixup all other entries of the provide partition table entry, e.g.
	if the start LBA changed, we should adjust the start CHS and the partition size.
	The Partition Editor will visualize changes we do to the Partition record *)
проц Changed*(changeType : цел32; перем partition : Partition; конст devicename : массив из симв8; перем res : целМЗ);
перем diskGeometry : DiskGeometry; geometry : булево;
нач
	GetDiskGeometry(devicename, diskGeometry, res);
	если (res = Ok) то
		geometry := истина;
		ЛогЯдра.пСтроку8(devicename); ЛогЯдра.пСтроку8(": CHS ");
		ЛогЯдра.пЦел64(diskGeometry.cylinders, 0); ЛогЯдра.пСтроку8(" x "); ЛогЯдра.пЦел64(diskGeometry.headsPerCylinder, 0); ЛогЯдра.пСтроку8(" x ");
		ЛогЯдра.пЦел64(diskGeometry.sectorsPerTrack, 0); ЛогЯдра.пВК_ПС;
	иначе
		(* if res = Disks.Unsupported, we could try to fake a disk geometry here... *)
		geometry := ложь;
	всё;
	если (changeType = StartLbaChanged) или (changeType = EndLbaChanged) то
		partition.size := partition.end.lba - partition.start.lba + 1;
		если geometry то
			Lba2Chs(partition.start.lba, partition.start.cylinder, partition.start.head, partition.start.sector, diskGeometry);
			Lba2Chs(partition.end.lba, partition.end.cylinder, partition.end.head, partition.end.sector, diskGeometry);
		всё;
	аесли (changeType = SizeChanged) то
		partition.end.lba := partition.start.lba + partition.size - 1;
		если geometry то
			Lba2Chs(partition.end.lba, partition.end.cylinder, partition.end.head, partition.end.sector, diskGeometry);
		всё;
	аесли (changeType = StartChsChanged) или (changeType = EndChsChanged)то
		если geometry то
			Chs2Lba(partition.start.cylinder, partition.start.head, partition.start.sector, partition.start.lba, diskGeometry);
			Chs2Lba(partition.end.cylinder, partition.end.head, partition.end.sector, partition.end.lba, diskGeometry);
			partition.size := partition.end.lba - partition.start.lba + 1;
		всё;
	всё;
	res := Ok;
кон Changed;

(** Set all entries the partition table <partitionTable>  to zero, needed by editor component. Can also be used here. *)
проц Clear*(перем partitionTable : PartitionTable);
перем i : цел32;

	проц ClearBlock(перем block : Block);
	нач
		block.lba := 0;
		block.cylinder := 0; block.head := 0; block.sector := 0;
	кон ClearBlock;

нач
	нцДля i := 0 до длинаМассива(partitionTable)-1 делай
		partitionTable[i].type := 0;
		partitionTable[i].flag := 0X;
		ClearBlock(partitionTable[i].start);
		ClearBlock(partitionTable[i].end);
	кц;
кон Clear;

кон PartitionEditorTable.
