; -*- Encoding: utf-8; system  :trob -*- 

(named-readtables:in-readtable :buddens-readtable-a)

(in-package :trob)

(defparameter *директория-яос* #P"C:/ob/jaos/")
(defparameter *конфигурации* 
  '("RPiC" "Linux64" "Bios32C" "Linux64C" "Bios32"  
     "ZynqC" "Zynq" "Win32" "Win64"))

(defun dump-lexem (l o)
  (format o "~A" (lexem-string l)))

(defun это-лисповый-список-лексем-ли (pathname)
  (with-open-file (in pathname :direction :input)
    (string= ";;;" (budden-tools:subseq1 (read-line in) 0 3))))

(defun merge-files (destination &rest pathnames)
  (perga-implementation:perga
   (let sources nil)
   (dolist (pathname pathnames)
     (format t "~&Читаю ~S~%" pathname)
     (cond
      ((это-лисповый-список-лексем-ли pathname)
       (with-open-file (in pathname :direction :input)
         (push (read in) sources)))
      (t
       (push (read-file-into-string pathname) sources))))
   (let output-source
     (merge-in-memory sources))
   (etypecase output-source
     (string 
      (budden-tools:save-string-to-file output-source destination))
     (cons
      (with-open-file (ou destination :direction :output
                          :if-exists :supersede
                          :if-does-not-exist :create)
        (mapc (lambda (l) (dump-lexem l ou)) output-source))))
   (values)))


(defun canonicalize-lexem (l)
  (format nil "~A~A" 
          (if (lexem-ne-otkl-preproc l) "1" "0")
          (lexem-string l)))

(defun compare-and-canon-lexems (l1 l2)
  (string= (canonicalize-lexem l1)
           (canonicalize-lexem l2)))

(defun merge-in-memory (sources)
  "Либо все они должны быть списками - это случай, когда в файле есть директивы условной компиляции. Тогда сливаем строки умным алгоритмом. Либо все должны быть
строками, причём идентичными - это случай, когда директив нет. Тогда проверяем, что они одинаковы"
  (perga
   (let first-one (car sources))
   (cond
    ((and first-one (stringp first-one))
     (assert (every (lambda (y) (equal y first-one))
                    (cdr sources))
             ()
             "Все исходные тексты должны совпадать, но они отличаются. Вот начало первого: ~S" 
             (budden-tools:subseq1 first-one 0 200))
     (car sources))
    (t
     (let ss (copy-list sources))
     (let res nil)
     (loop
       (let cur (mapcar 'car ss))
       (unless (first cur)
         (return (nreverse res)))
       (dotimes (i (length sources))
         (pop (elt ss i)))
       ;(print cur)
       (let different-variants
         (DEFPACKAGE-BUDDEN:GROUP-SIMILAR-ITEMS
             cur
             :test 'compare-and-canon-lexems))
       (case (length different-variants)
         (0 (error "WTF?"))
         (1 (push (first (first different-variants)) res))
         (2 
          (let good
            (find-if #'lexem-ne-otkl-preproc different-variants :key 'first))
          (cond
           (good
            (push (first good) res))
           (t
            (push `(inactive-block-mismatch ,@different-variants) res))))
         (t
          (push `(active-block-mismatch ,@different-variants) res))))))))
   
(defun собери-все-переводы-файлов (корень конфигурации)
  (perga
   (let все-имена-файлов-с-повторами nil)
   (dolist (к конфигурации)
     (let к-дир (str++ корень "perevod/p/" к "/ru"))
     (assert (probe-file к-дир))
     (budden-tools:map-dir
      (lambda (x) 
        (push (namestring (budden-tools:name-and-type-of-a-file x)) все-имена-файлов-с-повторами))
      к-дир
      :subdirs :skip))
   (remove-duplicates все-имена-файлов-с-повторами :test 'string=)))

(defun слей-переводы-файлов (корень конфигурации &key только-показать-план)
  "Корневая директория ЯОС должна заканчиваться на /; конфигурации - это поддиректории. Результат заливается прямо в source"
  (perga
   (let все-переводы-файлов (собери-все-переводы-файлов корень конфигурации))
   (flet задание-для-исходного-файла (fn)
     (let что-будем-сливать nil)
     (dolist (к конфигурации)
       (let имя-файла (str++ корень "perevod/p/" к "/ru/"
                             (budden-tools:name-and-type-of-a-file fn)))
       (print имя-файла)
       (when (probe-file имя-файла)
         (push имя-файла что-будем-сливать)))
     (when что-будем-сливать
       (let куда (str++ корень "source/" fn))
       `(,куда ,@что-будем-сливать)))
   (dolist (ф все-переводы-файлов)
     (let задание (задание-для-исходного-файла ф))
     (if только-показать-план
         (print задание)
         (apply #'merge-files задание)))))


(defun индекс-кирилличности-внутр (корень-яос-или-список-файлов писец)
  (perga
   (let всего-к 0)
   (let всего-б 0)
   (flet индекс-кирилличности-файла (фи)
     (let стр (ignore-errors (budden-tools:read-file-into-string фи)))
     (cond
      (стр
       (let в-файле-к (count-if 'russian-budden-tools:cyrillic-char-p стр))
       (let в-файле-а (count-if 'alpha-char-p стр))
       (let в-файле-доля (/ в-файле-к (+ 1e-10 в-файле-а)))
       (format писец  "~&~A:~A/~A ~~= ~A" фи в-файле-к в-файле-а в-файле-доля)
       (incf всего-к в-файле-к)
       (incf всего-б в-файле-а))
      (t
       (format писец "~&~A:ошибка чтения" фи))))
   (etypecase корень-яос-или-список-файлов
     ((or string pathname)
      (dolist (поддир '("source" "док"))
        (let поддир-полн (budden-tools:str++ корень-яос-или-список-файлов поддир))
        (budden-tools:map-dir 
         #'индекс-кирилличности-файла
         поддир-полн
         :file-test 
         (lambda (f)
           (member (pathname-type f) '("Mod" "XML" "ярм" "Tool" "md") :test 'string=))
         :subdirs :recurse)))
     (list
      (mapc #'индекс-кирилличности-файла корень-яос-или-список-файлов)))

   (format писец  "~&Всего:~A/~A ~~= ~A~%" всего-к всего-б (/ всего-к (+ 1e-10 всего-б)))))


(defun индекс-кирилличности (корень-яос-или-список-файлов куда-выводить &key)
  (perga
   (etypecase куда-выводить
    (stream
     (индекс-кирилличности-внутр корень-яос-или-список-файлов куда-выводить))
    ((or string pathname)
     (:@ with-open-file (писец куда-выводить :direction :output :if-does-not-exist :create :if-exists :supersede))
     (индекс-кирилличности-внутр корень-яос-или-список-файлов писец)))))
     

(defun выведи-файл-в-консоль (ф)
  (perga
   (:@ with-open-file (п ф :direction :input :external-format '(:utf-8 :replacement #\?)))
   (loop
     (let строка (read-line п nil nil))
     (unless строка
       (return-from выведи-файл-в-консоль t))
     (format t "~&~A~%" строка))))

(defun туды-или-сюды (так-куда-ж)
  (perga
   (пересоздай-директории-переводов-для-конфигураций)
   (let файл-вывода (budden-tools:str++ *директория-яос* "perevod/" так-куда-ж ".out"))
   (let файл-успеха (str++ файл-вывода ".успех"))
   (when (probe-file файл-успеха)
     (delete-file файл-успеха))
   (unwind-protect
       (perga
        (let процесс
          (sb-ext:run-program (uiop:native-namestring (budden-tools:str++ *директория-яос* "perevod/" так-куда-ж ".cmd")) nil :wait t 
                              :output файл-вывода :if-output-exists :supersede))
        (let код-возврата (sb-ext:process-exit-code процесс))
        (if (/= 0 код-возврата)
            (return-from туды-или-сюды код-возврата)))
     (выведи-файл-в-консоль файл-вывода))
   (cond
    ((probe-file файл-успеха)
     (delete-file файл-успеха)
     0)
    (t
     (format t "~&Нет файла успеха ~S - видимо, в ЯОС что-то пошло не так~%" файл-успеха)
     (return-from туды-или-сюды 4)))))


(defun пересоздай-директории-переводов-для-конфигураций ()
  (perga
   (dolist (к *конфигурации*)
     (let имя (budden-tools:str++ *директория-яос* "perevod/p/" к "/"))
     (when (probe-file имя)
       (uiop:delete-directory-tree (pathname имя) :validate (constantly t))))
   (dolist (к *конфигурации*)
     (let имя-ру (budden-tools:str++ *директория-яос* "perevod/p/" к "/ru/"))
     (ensure-directories-exist имя-ру))))

(defun с-русского-на-английский-и-обратно (&key (с-русского t) (на-русский t) (суффикс-файла "всё") (сливать t))
  (perga
   (let имя-файла-индекса-кирилличности
     (budden-tools:str++ *директория-яос* "perevod/индекс-кирилличности.txt"))
   (let конфигурации 
     (cond 
      ((string= суффикс-файла "всё") *конфигурации*) 
      ((string= суффикс-файла "win32") '("win32"))
      (t (error "неверный суффикс-файла"))))
   (when с-русского
     (let код-сюды (туды-или-сюды (str++ "na-angl" "." суффикс-файла)))
     (unless (= код-сюды 0)
       (return-from с-русского-на-английский-и-обратно nil))
     (unless сливать
       (warn "Сказано не сливать - превращаю только в английский")
       (return-from с-русского-на-английский-и-обратно nil))
     (слей-переводы-файлов "C:/ob/jaos/" конфигурации)
     (установи-пэк-англоязычный-режим t))

   (when на-русский
     (let код-туды (туды-или-сюды (str++ "na-rus" "." суффикс-файла)))
     (unless (= код-туды 0)
       (return-from с-русского-на-английский-и-обратно nil))
     (unless сливать
       (warn "Сказано не сливать - превращаю только в русский")
       (return-from с-русского-на-английский-и-обратно nil))
     (слей-переводы-файлов "C:/ob/jaos/" конфигурации)
     (установи-пэк-англоязычный-режим nil)

     (индекс-кирилличности (budden-tools:str++ *директория-яос* "source")
                          имя-файла-индекса-кирилличности)
     (выведи-файл-в-консоль имя-файла-индекса-кирилличности))
   t))


(defun установи-пэк-англоязычный-режим (англоязычный)
  (perga
   (let имя-файла "C:/ob/jaos/source/ПереводыЭлементовКода.ярм")
   (let старое-содержимое-файла
     (read-file-into-string имя-файла))
   (let русская-ложь "(*АнглоязычныйРежим>*)ложь(*<АнглоязычныйРежим*)")
   (let русская-истина "(*АнглоязычныйРежим>*)истина(*<АнглоязычныйРежим*)")
   (let английская-ложь "(*АнглоязычныйРежим>*)FALSE(*<АнглоязычныйРежим*)")
   (let английская-истина "(*АнглоязычныйРежим>*)TRUE(*<АнглоязычныйРежим*)")
   (unless
    (or (search русская-ложь старое-содержимое-файла)
        (search русская-истина старое-содержимое-файла)
        (search английская-ложь старое-содержимое-файла)
        (search английская-истина старое-содержимое-файла))
    (cerror "Продолжить, несмотря на это"
            "Не найдены маркированные вхождения для замены значения константы АнглоязычныйРежим"))
   (cond
    (англоязычный 
     (editor-budden-tools:replace-string-in-file
      имя-файла русская-ложь русская-истина)
     (editor-budden-tools:replace-string-in-file
      имя-файла английская-ложь английская-истина))
    (t (editor-budden-tools:replace-string-in-file
        имя-файла русская-истина русская-ложь)
       (editor-budden-tools:replace-string-in-file
        имя-файла английская-истина английская-ложь)))
   (values)))
     

(defun допереведи ()
  (perga
   (let код-туды (туды-или-сюды "na-russkij-1"))
   (unless (= код-туды 0)
     (return-from допереведи nil))
   (слей-переводы-файлов "C:/ob/jaos/" '("Win32"))
   t))


(defun туды-сюды (&key (na-rus t) (na-angl t))
  (perga
   (let имя-файла-индекса-кирилличности
     (budden-tools:str++ *директория-яос* "perevod/индекс-кирилличности.txt"))

   (when na-rus
     (let код-туды (туды-или-сюды "na-rus"))
     (unless (= код-туды 0)
       (return-from туды-сюды nil))
     (слей-переводы-файлов "C:/ob/jaos/" *конфигурации*)

     (индекс-кирилличности (budden-tools:str++ *директория-яос* "source")
                          имя-файла-индекса-кирилличности))
   (when na-angl
     (let код-сюды (туды-или-сюды "na-angl"))
     (unless (= код-сюды 0)
       (return-from туды-сюды nil))
     (слей-переводы-файлов "C:/ob/jaos/" *конфигурации*))

   (when (and na-rus na-angl)
     (выведи-файл-в-консоль имя-файла-индекса-кирилличности))
   t))


(defun напиши-скрипт-растенения-файлов (список-имён-файлов)
  (perga
   (:@ with-open-file (писец "C:/ob/jaos/source/СкриптРастенения.Tool" :direction :output :if-exists :supersede))
   (format писец "System.DoCommands~%")
   (dolist (имя-файла список-имён-файлов)
     (format писец
             """
LisCompiler.Compile --генерируйПереименованияДляРастенения ~A ~~
FSTools.DeleteFiles C:/ob/jaos/perevod/p/a/ru/* ~~
ЗагрузиПереводыЭлементовКода.ИзФайлаПереименованияИдентификаторов ~~
СборщикВыпускаЯОС.Докомпилируй --скомпилируй --показываяВыводКомпилятора
  --опцииКомпилятора='--переименуйИдентификаторы --кластьПереводыВ="C:/ob/jaos/perevod/p/a/ru"' Win32 ~A ~~
FSTools.CopyFiles --overwrite C:/ob/jaos/perevod/p/a/ru/*.* => C:/ob/jaos/source/*.* ~~
""" имя-файла имя-файла))
   (format писец "~%~~~~")
   ))

#|
(progn
  (trob:merge-files #P"C:/ob/jaos/win32/work/РуТестЛиб.ярм" #P"C:/ob/jaos/perevod/nano1/ru/РуТестЛиб.ярм" #P"C:/ob/jaos/perevod/nano2/ru/РуТестЛиб.ярм")
  (trob:merge-files #P"C:/ob/jaos/win32/work/РуТест.ярм" #P"C:/ob/jaos/perevod/nano1/ru/РуТест.ярм" #P"C:/ob/jaos/perevod/nano2/ru/РуТест.ярм"))

(trob:merge-files #P"C:/ob/jaos/win32/work/ПошаговыйОтладчик.ярм" #P"C:/ob/jaos/perevod/nejjtralqnye/ПошаговыйОтладчик.ярм")
|#
