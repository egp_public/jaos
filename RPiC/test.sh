#!/bin/bash 

set -xeuo pipefail 

SCRIPT_PATH=$(cd $(dirname $0) && cd .. && pwd);
export JAOS_ROOT=`readlink -f $SCRIPT_PATH`


expect << DONE

  spawn $JAOS_ROOT/../qemu0/arm-softmmu/qemu-system-arm -M raspi2 -serial mon:stdio -kernel $JAOS_ROOT/NewRPiC/kernel7.img -smp 4 

  expect_before {
    timeout { puts {Таймаут (ЯОС не загрузилась)}; exit 2 }
    eof     { puts {Неожиданный конец файла (QEMU упала?)}; exit 1 }
  }

  expect "Kernel: Initialized and started."
  puts ""
  puts {Тест сборки RPiC успешен}

DONE

