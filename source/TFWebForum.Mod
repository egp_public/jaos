модуль TFWebForum; (** AUTHOR "TF"; PURPOSE "CGI based forum system"; *)

использует
	Dates, Strings,
	XML, XMLObjects, XMLScanner, XMLParser,
	Commands, Files, Потоки, IP, Kernel, ЛогЯдра,
	WebHTTP, WebCGI, HTTPSupport;

конст
(*	MaxAuthor = 16;	*)
	ForumConfigFile = "WebForums.dat";

тип
	String = Strings.String;

	HTMLWriter= окласс
	перем w* : Потоки.Писарь;

		проц &New*(w : Потоки.Писарь);
		нач сам.w := w;
		кон New;

		проц Head*(конст title : массив из симв8);
		нач
			w.пСтроку8('<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>');
			w.пСтроку8(title);
			w.пСтроку8("</title></head>");
			w.пСтроку8("<body>");
		кон Head;

		проц Br*;
		нач
			w.пСтроку8("<br/>");
		кон Br;

		проц Nbsp*;
		нач
			w.пСтроку8("&nbsp;");
		кон Nbsp;

		проц InputText*(конст name : массив из симв8; value : String);
		нач
			w.пСтроку8('<input type="text" name="'); w.пСтроку8(name);	w.пСтроку8('" ');
			если value # НУЛЬ то w.пСтроку8('value="'); HTMLString(value^); w.пСтроку8('" ') всё;
			w.пСтроку8('/>');
		кон InputText;

		проц Hide*(конст name, value : массив из симв8);
		нач
			w.пСтроку8('<input type="hidden" name="'); w.пСтроку8(name);	w.пСтроку8('" ');
			w.пСтроку8('value="'); HTMLString(value); w.пСтроку8('" ');
			w.пСтроку8('/>');
		кон Hide;

		проц BeginOptionField*(конст name, value: массив из симв8);
		нач
			w.пСтроку8('<select  name="'); w.пСтроку8(name); w.пСтроку8('" ');
			если value # "" то w.пСтроку8(' value="'); w.пСтроку8(value); w.пСтроку8('"')	всё;
			w.пСтроку8('>');
		кон BeginOptionField;

		проц Option*(конст text : массив из симв8);
		нач
			w.пСтроку8('<option>'); HTMLString(text); w.пСтроку8('</option>');
		кон Option;

		проц EndOptionField*;
		нач
			w.пСтроку8('</select>');
		кон EndOptionField;


		проц Submit(конст text : массив из симв8);
		нач
			w.пСтроку8('<input type="submit" value="');
			w.пСтроку8(text);
			w.пСтроку8('" />');
		кон Submit;

		проц InputArea*(конст name : массив из симв8; value : String);
		нач
			w.пСтроку8('<textarea cols="80" rows="10" name="'); w.пСтроку8(name);	w.пСтроку8('"> ');
			если value # НУЛЬ то TAHTMLString(value^); всё;
			w.пСтроку8('</textarea>');
		кон InputArea;

		проц TextLink*(конст text, target : массив из симв8);
		нач
			w.пСтроку8('<a href="'); w.пСтроку8(target); w.пСтроку8('">'); w.пСтроку8(text); w.пСтроку8("</a>")
		кон TextLink;

		проц Tail*;
		нач
			w.пСтроку8("</body></html>");
		кон Tail;

		проц TAHTMLString(конст s : массив из симв8);
		перем i : цел32;
		нач
			i := 0;
			нцПока s[i] # 0X делай
				просей s[i] из
					|"<" : w.пСтроку8("&lt;");
					|">" : w.пСтроку8("&gt;");
					|"&" : w.пСтроку8("&amp;");
					|'"' : w.пСтроку8("&quot;");
				иначе w.пСимв8(s[i])
				всё;
				увел(i)
			кц
		кон TAHTMLString;

		проц HTMLString(конст s : массив из симв8);
		перем i : цел32;
		нач
			i := 0;
			нцПока s[i] # 0X делай
				просей s[i] из
					|"<" : w.пСтроку8("&lt;");
					|">" : w.пСтроку8("&gt;");
					|"&" : w.пСтроку8("&amp;");
					|'"' : w.пСтроку8("&quot;");
					|0DX : w.пСтроку8("<br/>");
				иначе w.пСимв8(s[i])
				всё;
				увел(i)
			кц
		кон HTMLString;

(*		PROCEDURE URIString(VAR s : ARRAY OF CHAR);
		VAR i : SIGNED32;
		BEGIN
			i := 0;
			WHILE s[i] # 0X DO
				IF uriLiteral[ORD(s[i])] THEN w.Char(s[i])
				ELSE w.Char("%"); w.Hex(ORD(s[i]), -2)
				END;
				INC(i)
			END
		END URIString;
*)

	кон HTMLWriter;

	EntryInfo = запись
		subject, id, datetime, author : String;
		entry : XML.Element;
		level : цел32;
	кон;

	EntryList = укль на массив из EntryInfo;

	Forum= окласс
	перем doc : XML.Document;
		forum : XML.Element;
		errors : булево;
		entryList : EntryList;
		nofEntries : цел32;
		title, editor, password : Strings.String;
		filename : массив 128 из симв8;

		проц &Create*;
		нач
			нов(doc);
			нов(forum);
			title:= empty; editor := empty; password := empty;
			forum.SetName("Forum");
			doc.AddContent(forum);
		кон Create;

		проц SetTitle(конст title : массив из симв8);
		нач
			сам.title := Strings.NewString(title);
			forum.SetAttributeValue("title", title);
		кон SetTitle;

		проц SetEditor(конст editor, password : массив из симв8);
		нач
			сам.editor := Strings.NewString(editor);
			forum.SetAttributeValue("editor", editor);
			сам.password := Strings.NewString(password);
			forum.SetAttributeValue("password", password);
		кон SetEditor;

		проц Fail(pos, line, row: Потоки.ТипМестоВПотоке; конст msg: массив из симв8);
		нач
			errors := истина;
			ЛогЯдра.пСтроку8("Version load failed : "); ЛогЯдра.пСтроку8("pos= "); ЛогЯдра.пЦел64(pos, 0); ЛогЯдра.пСтроку8("msg= "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС;
		кон Fail;

		проц Load(конст filename : массив из симв8) : булево;
		перем s : XMLScanner.Scanner;
			p : XMLParser.Parser;
			d : XML.Document;
			f : Files.File;
			r : Files.Reader;
		нач {единолично}
			f := Files.Old(filename); копируйСтрокуДо0(filename, сам.filename);
			ЛогЯдра.пСтроку8("loading filename= "); ЛогЯдра.пСтроку8(filename); ЛогЯдра.пВК_ПС;
			если f = НУЛЬ то возврат ложь всё;
			Files.OpenReader(r, f, 0);
			нов(s, r); нов(p, s); p.reportError := Fail;
			errors := ложь;
			d := p.Parse();
			если errors то возврат ложь всё;
			doc := d;
			forum := doc.GetRoot();
			title := forum.GetAttributeValue("title");
			если title = НУЛЬ то title := empty всё;

			editor := forum.GetAttributeValue("editor");
			если editor = НУЛЬ то editor := empty всё;

			password := forum.GetAttributeValue("password");
			если password = НУЛЬ то password := empty всё;
			возврат истина
		кон Load;

		проц StoreInternal(конст filename : массив из симв8);
		перем f : Files.File;
			w : Files.Writer;
		нач
			f := Files.New(filename);
			Files.OpenWriter(w, f, 0);
			doc.Write(w, НУЛЬ, 0);
			w.ПротолкниБуферВПоток;
			Files.Register(f);
			f.Update
		кон StoreInternal;

		проц Store(конст filename : массив из симв8);
		нач {единолично}
			StoreInternal(filename)
		кон Store;

		проц AddEntryToParent*(parent, entry : XML.Element);
		перем id, t : цел32;
			s : String;
			ids : массив 10 из симв8;
			contents : XMLObjects.Enumerator;
			content : динамическиТипизированныйУкль;
		нач {единолично}
			утв((parent # НУЛЬ) и (entry # НУЛЬ));
			entryList := НУЛЬ; nofEntries := 0;
			id := -1;
			contents := parent.GetContents();
			нцПока contents.HasMoreElements() делай
				content := contents.GetNext();
				если content суть XML.Element то
					s := content(XML.Element).GetName();
					если (s # НУЛЬ) и (s^ = "Entry") то
						s := content(XML.Element).GetAttributeValue("id");
						если s # НУЛЬ то
							Strings.StrToInt(s^, t);
							если t > id то id := t всё;
						всё
					всё
				всё;
			кц;
			Strings.IntToStr(id + 1, ids);
			entry.SetAttributeValue("id", ids);
			parent.AddContent(entry);
		кон AddEntryToParent;

		проц FindElement*(конст path : массив из симв8) : XML.Element;
		перем i, j, l : размерМЗ;
			id : массив 32 из симв8;
			s : String;
			e, next : XML.Element;
			contents : XMLObjects.Enumerator;
			content : динамическиТипизированныйУкль;
		нач {единолично}
			i := 0; j := 0; l := Strings.Length(path);
			e := forum;
			если forum = НУЛЬ то возврат НУЛЬ всё;
			нцПока j < l делай
				j := Strings.IndexOfByte("/", i, path);
				если j < 0 то j := l всё;
				Strings.Copy(path, i, j - i, id);
				i := j + 1;

				contents := e.GetContents();
				next := НУЛЬ;
				нцПока contents.HasMoreElements() и (next = НУЛЬ) делай
					content := contents.GetNext();
					если content суть XML.Element то
						s := content(XML.Element).GetName();
						если (s # НУЛЬ) и (s^ = "Entry") то
							s := content(XML.Element).GetAttributeValue("id");
							если s # НУЛЬ то
								если s^ = id то next := content(XML.Element) всё;
							всё;
						всё
					всё
				кц;
				если next = НУЛЬ то возврат НУЛЬ всё;
				e := next;
			кц;
			возврат e
		кон FindElement;

		проц GetEntryPath(e : XML.Element; перем path : массив из симв8);
		перем s : String; p : XML.Element;
		нач
			p := e.GetParent();
			если (p # НУЛЬ) и (p # forum) то GetEntryPath(p, path) всё;
			если (e.GetParent() # forum) то Strings.Append(path, "/") всё;
			s := e.GetAttributeValue("id");
			если s # НУЛЬ то
				Strings.Append(path, s^);
			всё;
		кон GetEntryPath;

		проц AddEntryToList(e : XML.Element);
		перем subject, author, email, datetime, ip, text : String;
			path : массив 512 из симв8;
			new : EntryList; i : цел32;
			t : XML.Element;
		нач
			если entryList = НУЛЬ то нов(entryList, 1024) всё;
			если nofEntries >= длинаМассива(entryList) то
				нов(new, длинаМассива(entryList) * 2);
				нцДля i := 0 до nofEntries - 1 делай new[i] := entryList[i] кц;
				entryList := new
			всё;
			ReadEntry(e, subject, author, email, datetime, ip, text);
			GetEntryPath(e, path);
			entryList[nofEntries].subject := subject;
			entryList[nofEntries].author := author;
			entryList[nofEntries].datetime := datetime;
			entryList[nofEntries].id := Strings.NewString(path);
			entryList[nofEntries].level := 0;
			entryList[nofEntries].entry := e;
			t := e;
			нцПока (t.GetParent() # НУЛЬ) и (t.GetParent() # forum) делай увел(entryList[nofEntries].level); t := t.GetParent() кц;
			увел(nofEntries)
		кон AddEntryToList;

		проц  Traverse (c : XML. Content; data: динамическиТипизированныйУкль);
		перем name : String;
		нач
			если (c # НУЛЬ) и (c суть XML.Element) то
				name := c(XML.Element).GetName();
				если (name # НУЛЬ) и (name^ = "Entry") то
					AddEntryToList(c(XML.Element));
				всё
			всё;
		кон Traverse;

		проц GetSubjectList*(перем e : EntryList; перем nof : цел32);
		нач {единолично}
			если entryList = НУЛЬ то forum.Traverse(Traverse, НУЛЬ) всё;
			e := entryList;
			nof := nofEntries
		кон GetSubjectList;

		проц AddEntry*(конст path : массив из симв8; entry : XML.Element);
		перем parent : XML.Element;
			f : Files.File;
			w : Files.Writer;
			s : массив 100 из симв8;
		нач
			утв(entry # НУЛЬ);
			parent := FindElement(path);
			если parent # НУЛЬ то
				AddEntryToParent(parent, entry);
				если filename # "" то Store(filename) всё;
			иначе
				ЛогЯдра.пСтроку8("Lost entry stored in LostForumEntries.txt"); ЛогЯдра.пВК_ПС;
				нач {единолично}
					f := Files.Old("LostForumEntries.txt");
					если f = НУЛЬ то
						f := Files.New("LostForumEntries.txt");
					всё;
					Files.OpenWriter(w, f, f.Length());
					w.пВК_ПС;
					Strings.FormatDateTime("@ yyyy.mm.dd hh.nn.ss", Dates.Now(), s);
					w.пСтроку8("Was not stored in "); w.пСтроку8(filename); w.пСтроку8(s); w.пВК_ПС;
					entry.Write(w, НУЛЬ, 0);
					w.ПротолкниБуферВПоток;
					Files.Register(f);
					f.Update;
				кон;
			всё
		кон AddEntry;

		проц DeleteEntry*(конст path : массив из симв8);
		перем entry, parent : XML.Element;
			f : Files.File;
			w : Files.Writer;
			s : массив 100 из симв8;
		нач
			entry := FindElement(path);
			утв(entry # НУЛЬ);
			parent := entry.GetParent();
			если parent # НУЛЬ то
				parent.RemoveContent(entry);
				если filename # "" то Store(filename) всё;
				ЛогЯдра.пСтроку8("deleted entry stored in DeletedEntries.txt"); ЛогЯдра.пВК_ПС;
				нач {единолично}
					entryList := НУЛЬ; nofEntries := 0; (* kill the cached list *)

					f := Files.Old("DeletedEntries.txt");
					если f = НУЛЬ то
						f := Files.New("DeletedEntries.txt");
					всё;
					Files.OpenWriter(w, f, f.Length());
					w.пВК_ПС;
					Strings.FormatDateTime("@ yyyy.mm.dd hh.nn.ss", Dates.Now(), s);
					w.пСтроку8("Deleted from "); w.пСтроку8(filename); w.пСтроку8(s); w.пВК_ПС;
					entry.Write(w, НУЛЬ, 0);
					w.ПротолкниБуферВПоток;
					Files.Register(f);
					f.Update;
				кон;
			всё
		кон DeleteEntry;

		проц EditEntry(parent: XML.Element; subject, author, email, datetime, ip, text : String);

			проц Set(конст name : массив из симв8; value : String);
			перем e : XML.Element;
				c : XML.CDataSect;
			нач
				e := GetSubElementByType(parent, name); если e # НУЛЬ то parent.RemoveContent(e) всё;
				нов(e);
				e.SetName(name); parent.AddContent(e);
				нов(c); c.SetStr(value^); e.AddContent(c);
			кон Set;

		нач
			entryList := НУЛЬ; nofEntries := 0;
			нач {единолично}
				Set("Subject", subject);
				Set("Author", author);
				Set("Email", email);
				Set("DateTime", datetime);
				Set("IP", ip);
				Set("Text", text);
			кон;
			если filename # "" то Store(filename) всё;
		кон EditEntry;

	кон Forum;

	ForumInfo = запись
		id : массив 256 из симв8;
		fileName : массив 256 из симв8;
		content : Forum;
	кон;

	ForumList = укль на массив из ForumInfo;

перем
	uriLiteral : массив 256 из булево;
	empty : String;
	forumList : ForumList;
	nofForum : цел32;

проц GetSubElementByType*(parent: XML.Element; конст type : массив из симв8): XML.Element;
перем enum: XMLObjects.Enumerator; p: динамическиТипизированныйУкль; e: XML.Element; s: XML.String;
нач
	enum := parent.GetContents();
	нцПока enum.HasMoreElements() делай
		p := enum.GetNext();
		если p суть XML.Element то
			e := p(XML.Element); s := e.GetName();
			если (s # НУЛЬ) и (s^ = type) то	(* correct element name *)
				возврат e
			всё
		всё
	кц;
	возврат НУЛЬ
кон GetSubElementByType;

проц MakeEntry*(subject, author, email, datetime, ip, text : String): XML.Element;
перем r, e : XML.Element;
	c : XML.CDataSect;
нач
	нов(r);
	r.SetName("Entry");

	нов(e); e.SetName("Subject"); r.AddContent(e);
	нов(c); c.SetStr(subject^); e.AddContent(c);

	нов(e); e.SetName("Author"); r.AddContent(e);
	нов(c); c.SetStr(author^); e.AddContent(c);

	нов(e); e.SetName("Email"); r.AddContent(e);
	нов(c); c.SetStr(email^); e.AddContent(c);

	нов(e); e.SetName("DateTime"); r.AddContent(e);
	нов(c); c.SetStr(datetime^); e.AddContent(c);

	нов(e); e.SetName("IP"); r.AddContent(e);
	нов(c); c.SetStr(ip^); e.AddContent(c);

	нов(e); e.SetName("Text"); r.AddContent(e);
	нов(c); c.SetStr(text^); e.AddContent(c);

	возврат r
кон MakeEntry;


проц PostingToHTML*(w : Потоки.Писарь; h : HTMLWriter; subject, author, email, datetime, ip, text : String);
нач
	w.пСтроку8('<table border="0" bgcolor="#F0F0F0">'); w.пВК_ПС;
	w.пСтроку8('<tr><td>');
	w.пСтроку8("Subject : "); w.пСтроку8("<b>"); h.HTMLString(subject^);w.пСтроку8("</b>");
	w.пСтроку8('</td></tr>'); w.пВК_ПС;

	w.пСтроку8('<tr><td>');
	w.пСтроку8("Author : "); h.HTMLString(author^); h.Br;
	w.пСтроку8('</td></tr>'); w.пВК_ПС;

	w.пСтроку8('<tr><td>');
	w.пСтроку8("Email : ");h.HTMLString(email^); h.Br;
	w.пСтроку8('</td></tr>'); w.пВК_ПС;

	w.пСтроку8('<tr><td>');
	w.пСтроку8("Date : ");h.HTMLString(datetime^); h.Br;
	w.пСтроку8('</td></tr>'); w.пВК_ПС;
	w.пСтроку8("</table>");

	w.пСтроку8('<table border="1" width="100%" cellpadding="0" cellspacing="0" bordercolor="#111111" bgcolor="#CCFFFF"><tr><td>');
	h.HTMLString(text^); h.Br;
	w.пСтроку8("</td></tr></table>");
кон PostingToHTML;

проц ReadEntry*(entry : XML.Element; перем subject, author, email, datetime, ip, text : String);
перем
	enum: XMLObjects.Enumerator; obj : динамическиТипизированныйУкль;
	e: XML.Element; str : String;

	проц GetCDataContent(e : XML.Element) : String;
	перем en : XMLObjects.Enumerator;
		p : динамическиТипизированныйУкль;
	нач
		en := e.GetContents();
		p := en.GetNext();
		если p # НУЛЬ то
			если p суть XML.CDataSect то
				возврат p(XML.CDataSect).GetStr()
			всё
		всё;
		возврат НУЛЬ
	кон GetCDataContent;

нач
	subject := empty; author := empty; email := empty; datetime := empty; ip := empty; text := empty;
	enum := entry.GetContents();
	нцПока enum.HasMoreElements() делай
		obj := enum.GetNext();
		если obj суть XML.Element то
			e := obj(XML.Element); str := e.GetName();
			если str^ = "Subject" то subject := GetCDataContent(e) всё;
			если str^ = "Author" то author := GetCDataContent(e) всё;
			если str^ = "Email" то email := GetCDataContent(e) всё;
			если str^ = "DateTime" то datetime := GetCDataContent(e) всё;
			если str^ = "IP" то ip := GetCDataContent(e) всё;
			если str^ = "Text" то text := GetCDataContent(e) всё;
		всё
	кц;
кон ReadEntry;

проц ListLink(перем forumID, link : массив из симв8);
нач
	копируйСтрокуДо0("Forum?forum=", link);
	Strings.Append(link, forumID);
	Strings.Append(link, "&action=List")
кон ListLink;

проц ShowLink(перем forumID, entryID, link : массив из симв8);
нач
	копируйСтрокуДо0("Forum?forum=", link);
	Strings.Append(link, forumID);
	Strings.Append(link, "&action=Show&entry=");
	Strings.Append(link, entryID);
кон ShowLink;

проц ReplyLink(перем forumID, entryID, link : массив из симв8);
нач
	копируйСтрокуДо0("Forum?forum=", link);
	Strings.Append(link, forumID);
	Strings.Append(link, "&action=Reply");
	если entryID # "" то
		Strings.Append(link, "&entry=");
		Strings.Append(link, entryID)
	всё
кон ReplyLink;

проц PublishPostLink(перем forumID, entryID, link : массив из симв8);
нач
	копируйСтрокуДо0("Forum?forum=", link);
	Strings.Append(link, forumID);
	Strings.Append(link, "&action=Publish");
	если entryID # "" то
		Strings.Append(link, "&entry=");
		Strings.Append(link, entryID)
	всё
кон PublishPostLink;


проц DeletePostLink(перем forumID, entryID, link : массив из симв8);
нач
	копируйСтрокуДо0("Forum?forum=", link);
	Strings.Append(link, forumID);
	Strings.Append(link, "&action=PublishDelete");
	если entryID # "" то
		Strings.Append(link, "&entry=");
		Strings.Append(link, entryID)
	всё
кон DeletePostLink;

проц GetParentLink(перем forumID, entryID, link : массив из симв8) : булево;
перем p : размерМЗ;
	parentID : массив 512 из симв8;
нач
	p := Strings.LastIndexOfByte2("/", entryID);
	если p > 0 то
		Strings.Copy(entryID, 0, p, parentID);
		ShowLink(forumID, parentID, link);
		возврат истина
	иначе возврат ложь
	всё;
кон GetParentLink;

проц List*(forum : Forum; forumID : массив из симв8; context: WebCGI.CGIContext);
перем
	w : Потоки.Писарь;
	chunker : WebHTTP.ChunkedOutStream;
	h : HTMLWriter;
	entryList : EntryList;
	nof, i, j : цел32;
	link : массив 256 из симв8;
	e : массив 2 из симв8;
нач
	forum.GetSubjectList(entryList, nof);
	(* reply *)
	нов(chunker, w, context.w, context.request.header, context.reply);
	context.reply.statuscode := WebHTTP.OK;
	context.reply.contenttype := "text/html; charset=UTF-8";
	WebHTTP.SendResponseHeader(context.reply, context.w);

	нов(h, w);
	h.Head(forum.title^);
	w.пСтроку8("<H1>");
	h.HTMLString(forum.title^);
	w.пСтроку8("</H1>"); w.пВК_ПС;

	нцДля i := 0 до nof - 1 делай
		нцДля j := 0 до entryList[i].level - 1 делай h.Nbsp; h.Nbsp кц;
		ShowLink(forumID, entryList[i].id^, link);
		h.TextLink(entryList[i].subject^, link);
		w.пСтроку8("<i> (");
		w.пСтроку8(entryList[i].author^);
		w.пСтроку8( " @ ");
		w.пСтроку8(entryList[i].datetime^);
		w.пСтроку8(")</i>");
		h.Br;
	кц;
	e := "";
	h.Br;
	ReplyLink(forumID, e, link);
	h.TextLink("Write new message", link);

	h.Tail;
	w.пВК_ПС; w.ПротолкниБуферВПоток;
	chunker.Close;
кон List;

проц Show*(forum : Forum; forumID, entryID : массив из симв8; context: WebCGI.CGIContext);
перем
	w : Потоки.Писарь;
	chunker : WebHTTP.ChunkedOutStream;
	h : HTMLWriter;
	nof, i, j, thisIndent : цел32;
	entry : XML.Element;
	firstReply : булево;
	link, title : массив 256 из симв8;
	subject, author, email, datetime, ip, text : String;
	entries : EntryList;
нач
	(* reply *)
	нов(chunker, w, context.w, context.request.header, context.reply);
	context.reply.statuscode := WebHTTP.OK;
	context.reply.contenttype := "text/html; charset=UTF-8";
	WebHTTP.SendResponseHeader(context.reply, context.w);

	нов(h, w);

	entry := forum.FindElement(entryID);
	если entry # НУЛЬ то
		forum.GetSubjectList(entries, nof);

		ReadEntry(entry, subject, author, email, datetime, ip, text);
		копируйСтрокуДо0(forum.title^, title);
		Strings.Append(title, " - "); Strings.Append(title, subject^);
		h.Head(title);

		ListLink(forumID, link);
		h.TextLink("List", link); h.Nbsp;

		если GetParentLink(forumID, entryID, link) то h.TextLink("Parent", link) всё;
		h.Br;
		i := 0; нцПока (entryID # entries[i].id^) и (i < nof) делай увел(i) кц;

		если i > 0 то
			ShowLink(forumID, entries[i -1].id^, link);
			h.TextLink("Previous", link)
		всё;

		если i < nof - 1 то
			h.Nbsp;
			ShowLink(forumID, entries[i + 1].id^, link);
			h.TextLink("Next", link)
		всё;

		h.Br;

		PostingToHTML(w, h, subject, author, email, datetime, ip, text);

		h.Br;

		firstReply := истина;
		нцДля i := 0 до nof - 1 делай
			если Strings.StartsWith2(entryID, entries[i].id^) то
				если (entryID = entries[i].id^) то
					thisIndent := entries[i].level;
				иначе
					если firstReply то w.пСтроку8("<b>Replies</b>"); h.Br; firstReply := ложь всё;
					нцДля j := 0 до entries[i].level - thisIndent - 1 делай h.Nbsp; h.Nbsp кц;
					ShowLink(forumID, entries[i].id^, link);
					h.TextLink(entries[i].subject^, link);
					w.пСтроку8("<i> (");
					w.пСтроку8(entries[i].author^);
					w.пСтроку8( " @ ");
					w.пСтроку8(entries[i].datetime^);
					w.пСтроку8(")</i>");
					h.Br;
				всё
			всё
		кц;
		h.Br;
		ReplyLink(forumID, entryID, link);
		h.TextLink("Write a new reply", link); h.Br;

	иначе
		ListLink(forumID, link);
		h.TextLink("list", link); h.Nbsp;
		link := "entry not found";
		h.HTMLString(link);
	всё;

	h.Tail;
	w.пВК_ПС; w.ПротолкниБуферВПоток;
	chunker.Close;
кон Show;

проц QueryPost(forum : Forum; forumID, entryID : массив из симв8; context: WebCGI.CGIContext);
перем
	w : Потоки.Писарь;
	chunker : WebHTTP.ChunkedOutStream;
	h : HTMLWriter;
	entry : XML.Element;
	link, s : массив 256 из симв8;
	subject, author, email, datetime, ip, text : String;
нач
	(* reply *)
	нов(chunker, w, context.w, context.request.header, context.reply);
	context.reply.statuscode := WebHTTP.OK;
	context.reply.contenttype := "text/html; charset=UTF-8";
	WebHTTP.SendResponseHeader(context.reply, context.w);

	нов(h, w);

	entry := forum.FindElement(entryID);
	если (entry # НУЛЬ) и (entry # forum.forum) то
		ReadEntry(entry, subject, author, email, datetime, ip, text);
		s := "Reply to ";
		Strings.Append(s, subject^);
		h.Head(s);

		ListLink(forumID, link);
		h.TextLink("list", link); h.Nbsp;

		если GetParentLink(forumID, entryID, link) то h.TextLink("parent", link); h.Br всё;
		h.Br;

		PostingToHTML(w, h, subject, author, email, datetime, ip, text);

	иначе
		h.Head("Create a new thread");
		w.пСтроку8("Create a new thread"); h.Br;
	всё;
	PublishPostLink(forumID, entryID, link);
	w.пСтроку8('<form action="');w.пСтроку8(link); w.пСтроку8('"method="POST" accept-charset="UTF-8" >'); w.пВК_ПС;
	h.Br; w.пСтроку8("<hr/>");  w.пВК_ПС;

	w.пСтроку8('Subject : '); h.InputText("subject", subject); h.Br; w.пВК_ПС;
	w.пСтроку8("Author : "); h.InputText("author", empty);  h.Br; w.пВК_ПС;
	w.пСтроку8("Email : "); h.InputText("email", empty); w.пСтроку8("<i>optional</i>"); h.Br; w.пВК_ПС;
	w.пСтроку8("Text : "); h.InputArea("text", empty); h.Br; w.пВК_ПС;
	h.Submit("Post");

	w.пСтроку8('</form>');

	h.Tail;
	w.пВК_ПС; w.ПротолкниБуферВПоток;
	chunker.Close;
кон QueryPost;

проц QueryEditPost(forum : Forum; forumID, entryID : массив из симв8; context: WebCGI.CGIContext);
перем
	w : Потоки.Писарь;
	chunker : WebHTTP.ChunkedOutStream;
	h : HTMLWriter;
	entry : XML.Element;
	link, s : массив 256 из симв8;
	subject, author, email, datetime, ip, text : String;
нач
	(* reply *)
	нов(chunker, w, context.w, context.request.header, context.reply);
	context.reply.statuscode := WebHTTP.OK;
	context.reply.contenttype := "text/html; charset=UTF-8";
	WebHTTP.SendResponseHeader(context.reply, context.w);

	нов(h, w);

	entry := forum.FindElement(entryID);
	если (entry # НУЛЬ) и (entry # forum.forum) то
		ReadEntry(entry, subject, author, email, datetime, ip, text);
		s := "Edit ";
		Strings.Append(s, subject^);
		h.Head(s);

		ListLink(forumID, link);
		h.TextLink("list", link); h.Nbsp;

		если GetParentLink(forumID, entryID, link) то h.TextLink("parent", link); h.Br всё;
		h.Br;

		PostingToHTML(w, h, subject, author, email, datetime, ip, text);


		PublishPostLink(forumID, entryID, link);
		w.пСтроку8('<form action="');w.пСтроку8(link); w.пСтроку8('"method="POST" accept-charset="UTF-8" >'); w.пВК_ПС;
		h.Br; w.пСтроку8("<hr/>");  w.пВК_ПС;

		w.пСтроку8("<b>Accreditiation:</b><br/>");
		w.пСтроку8('Editor : '); h.InputText("editor", НУЛЬ); w.пСтроку8('Authorization : '); h.InputText("password", НУЛЬ);
		h.Br;

		w.пСтроку8('Subject : '); h.InputText("subject", subject); h.Br; w.пВК_ПС;
		w.пСтроку8("Author : "); h.InputText("author", author);  h.Br; w.пВК_ПС;
		w.пСтроку8("Email : "); h.InputText("email", email); w.пСтроку8("<i>optional</i>"); h.Br; w.пВК_ПС;
		w.пСтроку8("Text : "); h.InputArea("text", text); h.Br; w.пВК_ПС;
		h.Hide("ip", ip^);
		h.Hide("datetime", datetime^);
		h.Hide("replace", "true");
		h.Submit("Edit");

		w.пСтроку8('</form>');

		h.Tail;
	всё;
	w.пВК_ПС; w.ПротолкниБуферВПоток;
	chunker.Close;
кон QueryEditPost;

проц QueryDeletePost(forum : Forum; forumID, entryID : массив из симв8; context: WebCGI.CGIContext);
перем
	w : Потоки.Писарь;
	chunker : WebHTTP.ChunkedOutStream;
	h : HTMLWriter;
	entry : XML.Element;
	link, s : массив 256 из симв8;
	subject, author, email, datetime, ip, text : String;
нач
	(* reply *)
	нов(chunker, w, context.w, context.request.header, context.reply);
	context.reply.statuscode := WebHTTP.OK;
	context.reply.contenttype := "text/html; charset=UTF-8";
	WebHTTP.SendResponseHeader(context.reply, context.w);

	нов(h, w);

	entry := forum.FindElement(entryID);
	если (entry # НУЛЬ) и (entry # forum.forum) то
		ReadEntry(entry, subject, author, email, datetime, ip, text);
		s := "Delete ";
		Strings.Append(s, subject^);
		h.Head(s);

		ListLink(forumID, link);
		h.TextLink("list", link); h.Nbsp;

		если GetParentLink(forumID, entryID, link) то h.TextLink("parent", link); h.Br всё;
		h.Br;

		PostingToHTML(w, h, subject, author, email, datetime, ip, text);

	иначе
	всё;
	DeletePostLink(forumID, entryID, link);
	w.пСтроку8('<form action="');w.пСтроку8(link); w.пСтроку8('"method="POST" accept-charset="UTF-8" >'); w.пВК_ПС;
	h.Br; w.пСтроку8("<hr/>");  w.пВК_ПС;

	w.пСтроку8("<b>Accreditiation:</b><br/>");
	w.пСтроку8('Editor : '); h.InputText("editor", НУЛЬ); w.пСтроку8('Authorization : '); h.InputText("password", НУЛЬ);
	h.Submit("Delete");

	w.пСтроку8('</form>');

	h.Tail;
	w.пВК_ПС; w.ПротолкниБуферВПоток;
	chunker.Close;
кон QueryDeletePost;

проц PublishPost(forum : Forum; forumID, entryID : массив из симв8; context: WebCGI.CGIContext);
перем
	w : Потоки.Писарь;
	chunker : WebHTTP.ChunkedOutStream;
	h : HTMLWriter;
	entry, parent : XML.Element;
	link, s, editor, password : массив 256 из симв8;
	subject, author, email, datetime, ip, text : String;
	var: HTTPSupport.HTTPVariable;
	replace : булево;
нач
	(* reply *)
	нов(chunker, w, context.w, context.request.header, context.reply);
	context.reply.statuscode := WebHTTP.OK;
	context.reply.contenttype := "text/html; charset=UTF-8";
	WebHTTP.SendResponseHeader(context.reply, context.w);

	нов(h, w);

	var := context.request.GetVariableByName("replace");
	если (var # НУЛЬ) и (var.value # "") то replace := var.value = "true"
	иначе replace := ложь
	всё;

	если replace то ЛогЯдра.пСтроку8("Replace entry") иначе ЛогЯдра.пСтроку8("New Entry"); ЛогЯдра.пВК_ПС;  всё;

	var := context.request.GetVariableByName("subject");
	если (var # НУЛЬ) и (var.value # "") то subject := Strings.NewString(var.value)
	иначе subject := Strings.NewString("anonymous");
	всё;

	var := context.request.GetVariableByName("author");
	если (var # НУЛЬ) и (var.value # "") то author := Strings.NewString(var.value)
	иначе author := Strings.NewString("anonymous");
	всё;

	var := context.request.GetVariableByName("email");
	если (var # НУЛЬ) и (var.value # "") то email := Strings.NewString(var.value)
	иначе email:= Strings.NewString("");
	всё;

	var := context.request.GetVariableByName("text");
	если (var # НУЛЬ) и (var.value # "") то text := Strings.NewString(var.value)
	иначе text := Strings.NewString("");
	всё;

	IP.AdrToStr(context.request.header.fadr, s);
	ip := Strings.NewString(s);


	Strings.FormatDateTime("yyyy.mm.dd hh.nn.ss", Dates.Now(), s);
	datetime := Strings.NewString(s);

	если ~replace то
		entry := MakeEntry(subject, author, email, datetime, ip, text);
		forum.AddEntry(entryID, entry);

		parent := forum.FindElement(entryID);
		если parent # НУЛЬ то
			h.Head(subject^);
			ListLink(forumID, link); h.TextLink("list", link); h.Nbsp;
			ShowLink(forumID, entryID, link); h.TextLink("parent", link); h.Br;
		иначе h.Head("New thread created");
			ListLink(forumID, link); h.TextLink("list", link); h.Nbsp;
		всё;

	иначе
		h.Head(subject^);
		var := context.request.GetVariableByName("editor");
		если (var # НУЛЬ) то копируйСтрокуДо0(var.value, editor) всё;

		var := context.request.GetVariableByName("password");
		если (var # НУЛЬ) то копируйСтрокуДо0(var.value, password) всё;

		если (editor = forum.editor^) и (password = forum.password^) то
			ListLink(forumID, link); h.TextLink("list", link); h.Nbsp;
			entry := forum.FindElement(entryID);
			если entry # НУЛЬ то
				forum.EditEntry(entry, subject, author, email, datetime, ip, text)
			всё;
		иначе
			w.пСтроку8("<h1>Your accredition was not accepted.</h1>"); w.пВК_ПС;
		всё
	всё;

	PostingToHTML(w, h, subject, author, email, datetime, ip, text);
	h.Br;
	h.Tail;
	w.пВК_ПС; w.ПротолкниБуферВПоток;
	chunker.Close;
кон PublishPost;

проц DeletePost(forum : Forum; forumID, entryID : массив из симв8; context: WebCGI.CGIContext);
перем
	w : Потоки.Писарь;
	chunker : WebHTTP.ChunkedOutStream;
	h : HTMLWriter;
	link, editor, password : массив 256 из симв8;
	var: HTTPSupport.HTTPVariable;
нач
	(* reply *)
	нов(chunker, w, context.w, context.request.header, context.reply);
	context.reply.statuscode := WebHTTP.OK;
	context.reply.contenttype := "text/html; charset=UTF-8";
	WebHTTP.SendResponseHeader(context.reply, context.w);

	нов(h, w);

	h.Head("Deleting Post");
	var := context.request.GetVariableByName("editor");
	если (var # НУЛЬ) то копируйСтрокуДо0(var.value, editor) всё;

	var := context.request.GetVariableByName("password");
	если (var # НУЛЬ) то копируйСтрокуДо0(var.value, password) всё;

	если (editor = forum.editor^) и (password = forum.password^) то
		ListLink(forumID, link); h.TextLink("list", link); h.Nbsp;
		forum.DeleteEntry(entryID);
		w.пСтроку8("Entry deleted.");
	иначе
		w.пСтроку8("<h1>Your accredition was not accepted.</h1>"); w.пВК_ПС;
	всё;
	h.Tail;
	w.пВК_ПС; w.ПротолкниБуферВПоток;
	chunker.Close;
кон DeletePost;

проц Access*(context : WebCGI.CGIContext);
перем
	r : HTTPSupport.HTTPRequest;
	var: HTTPSupport.HTTPVariable;

	action, forumID, entry : массив 32 из симв8;
	forum : Forum;

	w : Потоки.Писарь;
	chunker : WebHTTP.ChunkedOutStream;
	defaultAction : булево;
	milliTimer : Kernel.MilliTimer;
нач
	Kernel.SetTimer(milliTimer, 0);
	r := context.request;
	defaultAction := истина;
	var := r.GetVariableByName("action");
	если var # НУЛЬ то копируйСтрокуДо0(var.value, action); defaultAction := ложь всё;

	var := r.GetVariableByName("forum");
	если var # НУЛЬ то копируйСтрокуДо0(var.value, forumID) всё;

	var := r.GetVariableByName("entry");
	если var # НУЛЬ то копируйСтрокуДо0(var.value, entry) всё;

	forum := GetForum(forumID);

	если forum = НУЛЬ то
		нов(chunker, w, context.w, context.request.header, context.reply);
		context.reply.statuscode := WebHTTP.NotFound;
		WebHTTP.SendResponseHeader(context.reply, context.w);
		w.пСтроку8("<html><head><title>Forum</title></head>");
		w.пСтроку8("<body>");
		w.пСтроку8("Forum not found"); w.пВК_ПС;
		w.пСтроку8("</body></html>");
		w.пВК_ПС; w.ПротолкниБуферВПоток;
		chunker.Close
	иначе
		если action = "Show" то Show(forum, forumID, entry, context);
		аесли defaultAction или (action = "List") то List(forum, forumID, context);
		аесли action = "Reply" то QueryPost(forum, forumID, entry, context)
		аесли action = "Publish" то PublishPost(forum, forumID, entry, context)
		аесли action = "Edit" то QueryEditPost(forum, forumID, entry, context)
		аесли action = "Delete" то QueryDeletePost(forum, forumID, entry, context)
		аесли action = "PublishDelete" то DeletePost(forum, forumID, entry, context)
		иначе
			нов(chunker, w, context.w, context.request.header, context.reply);
			context.reply.statuscode := WebHTTP.NotFound;
			WebHTTP.SendResponseHeader(context.reply, context.w);
			w.пСтроку8("<html><head><title>Forum</title></head>");
			w.пСтроку8("<body>");
			w.пСтроку8("Illegal forum request"); w.пВК_ПС;
			w.пСтроку8("</body></html>");
			w.пВК_ПС; w.ПротолкниБуферВПоток;
			chunker.Close
		всё
	всё;
	ЛогЯдра.пСтроку8("Forum request handled in "); ЛогЯдра.пЦел64(Kernel.Elapsed(milliTimer), 0); ЛогЯдра.пСтроку8("ms"); ЛогЯдра.пВК_ПС;
кон Access;

проц InitURILiterals;
перем i : цел32;
нач
	нцДля i := 0 до 255 делай uriLiteral[i] := ложь кц;
	нцДля i :=  61H до  7AH делай uriLiteral[i] := истина кц;(* RFC2396 lowalpha *)
	нцДля i :=  41H до 5AH делай uriLiteral[i] := истина кц;(* RFC2396 upalpha *)
	нцДля i := 30H до 39H делай uriLiteral[i] := истина кц; (* RFC2396 digit *)
	uriLiteral[2DH] := истина; (* - *)
	uriLiteral[5FH] := истина; (* underscore *)
	uriLiteral[2EH] := истина; (* . *)
	uriLiteral[21H] := истина; (* ! *)
	uriLiteral[7EH] := истина; (* ~ *)
	uriLiteral[2AH] := истина; (* * *)
	uriLiteral[27H] := истина; (* ' *)
	uriLiteral[28H] := истина; (* ( *)
	uriLiteral[29H] := истина;  (* ) *)
кон InitURILiterals;

проц AddForum(конст id,fileName : массив из симв8);
перем new : ForumList;
	i : цел32;
нач
	если nofForum >= длинаМассива(forumList) то
		нов(new, длинаМассива(forumList) * 2);
		нцДля i := 0 до nofForum - 1 делай new[i] := forumList[i] кц;
		forumList := new
	всё;
	копируйСтрокуДо0(id, forumList[nofForum].id);
	копируйСтрокуДо0(fileName, forumList[nofForum].fileName);
	увел(nofForum)
кон AddForum;

проц GetForumInternal(конст id : массив из симв8) : Forum;
перем i : цел32; result : Forum;
нач
	i := 0;
	нцПока (i < nofForum) и (result = НУЛЬ)  делай
		если forumList[i].id = id то
			если forumList[i].content = НУЛЬ то
				нов(forumList[i].content);
				если forumList[i].content.Load(forumList[i].fileName) то
					ЛогЯдра.пСтроку8(forumList[i].id); ЛогЯдра.пСтроку8(" loaded from "); ЛогЯдра.пСтроку8(forumList[i].fileName); ЛогЯдра.пВК_ПС;
				иначе
					ЛогЯдра.пСтроку8(forumList[i].id); ЛогЯдра.пСтроку8("FAILED loading  from "); ЛогЯдра.пСтроку8(forumList[i].fileName); ЛогЯдра.пВК_ПС;
				всё;
			всё;
			result := forumList[i].content
		всё;
		увел(i)
	кц;
	возврат result
кон GetForumInternal;

проц GetForum(конст id : массив из симв8) : Forum;
нач {единолично}
	возврат GetForumInternal(id)
кон GetForum;

проц LoadForumList;
перем f : Files.File;
	r : Files.Reader;
	id, fileName : массив 128 из симв8;
нач {единолично}
	f := Files.Old(ForumConfigFile);
	если f # НУЛЬ то
		Files.OpenReader(r, f, 0);
		нцПока r.кодВозвратаПоследнейОперации = 0 делай
			r.чЦепочкуСимв8ДоБелогоПоля(id); r.ПропустиБелоеПоле;
			r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fileName);
			если r.кодВозвратаПоследнейОперации = 0 то AddForum(id, fileName) всё;
			r.ПропустиДоКонцаСтрокиТекстаВключительно;
		кц
	всё;
кон LoadForumList;

проц StoreForumList;
перем f : Files.File;
	w : Files.Writer;
	i : цел32;
нач {единолично}
	f := Files.New(ForumConfigFile);
	Files.OpenWriter(w, f, 0);
	нцДля i := 0 до nofForum - 1 делай
		w.пСтроку8(forumList[i].id); w.пСтроку8(' "'); w.пСтроку8(forumList[i].fileName); w.пСтроку8('"'); w.пВК_ПС
	кц;
	w.ПротолкниБуферВПоток;
	Files.Register(f)
кон StoreForumList;

проц CreateForum*(context : Commands.Context);
перем
	id, fileName, title, user, password : массив 128 из симв8;
	forum : Forum;
нач
	context.arg.чЦепочкуСимв8ДоБелогоПоля(id); context.arg.ПропустиБелоеПоле(); context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fileName); context.arg.ПропустиБелоеПоле(); context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(title);
	context.arg.ПропустиБелоеПоле(); context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(user); context.arg.ПропустиБелоеПоле(); context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(password);
	нач{единолично}
		forum := GetForumInternal(id);
		если forum # НУЛЬ то
			context.error.пСтроку8("Forum already exists"); context.error.пВК_ПС; возврат;
		иначе
			AddForum(id, fileName);
			forum := GetForumInternal(id);
			forum.SetTitle(title);
			forum.SetEditor(user, password);
			forum.Store(fileName);
		всё
	кон;
	StoreForumList;
кон CreateForum;

нач
	empty := Strings.NewString("");
	нов(forumList, 128); nofForum := 0;
	LoadForumList;
	InitURILiterals;
кон TFWebForum.

System.Free TFWebForum ~
TFWebForum.CreateForum RFWde ForumRFWde.XML "Raily for Windows (Deutsch)" rfwuser rfwpassword ~
TFWebForum.CreateForum RFWfr ForumRFWfr.XML "Raily for Windows (Francais)" rfwuser rfwpassword ~
TFWebForum.CreateForum RFWen ForumRFWen.XML "Raily for Windows (English)" rfwuser rfwpassword ~
TFWebForum.CreateForum PCFrey ForumPCFrey.XML "PC - Forum" rfwuser rfwpassword ~

TFWebForum.CreateForum BluebottleFeatures ForumBluebottle.XML "Forum Bluebottlerum" user password ~


WebHTTPServerTools.Start ~
WebCGI.Install ~
WebCGI.RegisterCGI Forum TFWebForum.Access~
WebCGI.ListCGI ~

