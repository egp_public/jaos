(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль NbrRat;   (** AUTHOR "adf"; PURPOSE "Defines a base fixed-point Rational type for scientific computing. "; *)

использует Потоки, NbrInt, NbrInt64;

тип
	Rational* = запись
		n, d: NbrInt64.Integer
	кон;

перем
	MinNbr-, MaxNbr-: Rational;

	(* Local Procedures for Rational Numbers. *)
	проц CommonDenominator( num, denom: NbrInt64.Integer;  перем cd: NbrInt64.Integer );
	(* Algorithm from:  P. Henrici, "A Subroutine for Computations with
			Rational Numbers," J. ACM, Vol. 3, No. 1, 1956, pp. 6-9. *)
	перем absNum, absDenom, prev, current, next: NbrInt64.Integer;
	нач
		если (num > NbrInt64.MinNbr) и (num # 0) то
			absNum := NbrInt64.Abs( num );  absDenom := NbrInt64.Abs( denom );
			если absNum > absDenom то current := absNum;  next := absDenom иначе current := absDenom;  next := absNum всё;
			нцДо  (* The Euclidean algorithm. *)
				prev := current;  current := next;  next := prev остОтДеленияНа current
			кцПри next = 0;
			cd := current
		иначе cd := 1
		всё
	кон CommonDenominator;

	проц Simplify( перем x: Rational );
	перем cd, n, d: NbrInt64.Integer;
	нач
		n := x.n;  d := x.d;
		если n # 0 то
			если (d < 0) и (d > NbrInt64.MinNbr) и (n > NbrInt64.MinNbr) то n := -n;  d := -d всё;
			CommonDenominator( n, d, cd );
			если cd = 1 то x.n := n;  x.d := d иначе x.n := n DIV cd;  x.d := d DIV cd всё
		иначе x.n := 0;  x.d := 1
		всё
	кон Simplify;

(** Monadic Operator *)
	операция "-"*( x: Rational ): Rational;
	перем n: Rational;
	нач
		n.n := -x.n;  n.d := x.d;  возврат n
	кон "-";

	(** Dyadic Operators *)
(** Type Conversion *)
	операция ":="*( перем l: Rational;  r: NbrInt.Integer );
	нач
		l.n := r;  l.d := 1
	кон ":=";

(** Comparison Operators *)
	операция "="*( l, r: Rational ): булево;
	нач
		возврат (l.n = r.n) и (l.d = r.d)
	кон "=";

	операция "="*( l: Rational;  r: NbrInt.Integer ): булево;
	нач
		возврат (l.n = r) и (l.d = 1)
	кон "=";

	операция "="*( l: NbrInt.Integer;  r: Rational ): булево;
	нач
		возврат (l = r.n) и (r.d = 1)
	кон "=";

	операция "#"*( l, r: Rational ): булево;
	нач
		возврат (l.n # r.n) или (l.d # r.d)
	кон "#";

	операция "#"*( l: Rational;  r: NbrInt.Integer ): булево;
	нач
		возврат (l.n # r) или (l.d # 1)
	кон "#";

	операция "#"*( l: NbrInt.Integer;  r: Rational ): булево;
	нач
		возврат (l # r.n) или (r.d # 1)
	кон "#";

	операция "<"*( l, r: Rational ): булево;
	перем a, b: NbrInt64.Integer;
	нач
		a := l.n DIV l.d;  b := r.n DIV r.d;
		если a < b то возврат истина
		аесли a > b то возврат ложь
		иначе
			если (l.n остОтДеленияНа l.d) < (r.n остОтДеленияНа r.d) то возврат истина иначе возврат ложь всё
		всё
	кон "<";

	операция "<"*( l: Rational;  r: NbrInt.Integer ): булево;
	нач
		если (l.n DIV l.d) < r то возврат истина иначе возврат ложь всё
	кон "<";

	операция "<"*( l: NbrInt.Integer;  r: Rational ): булево;
	перем b: NbrInt64.Integer;
	нач
		b := r.n DIV r.d;
		если l < b то возврат истина
		аесли l > b то возврат ложь
		иначе
			если (r.n остОтДеленияНа r.d > 0) и (r.n > 0) то возврат истина иначе возврат ложь всё
		всё
	кон "<";

	операция ">"*( l, r: Rational ): булево;
	перем a, b: NbrInt64.Integer;
	нач
		a := l.n DIV l.d;  b := r.n DIV r.d;
		если a > b то возврат истина
		аесли a < b то возврат ложь
		иначе
			если (l.n остОтДеленияНа l.d) > (r.n остОтДеленияНа r.d) то возврат истина иначе возврат ложь всё
		всё
	кон ">";

	операция ">"*( l: Rational;  r: NbrInt.Integer ): булево;
	перем a: NbrInt64.Integer;
	нач
		a := l.n DIV l.d;
		если a > r то возврат истина
		аесли a < r то возврат ложь
		иначе
			если (l.n остОтДеленияНа l.d) > 0 то возврат истина иначе возврат ложь всё
		всё
	кон ">";

	операция ">"*( l: NbrInt.Integer;  r: Rational ): булево;
	нач
		если l > (r.n DIV r.d) то возврат истина иначе возврат ложь всё
	кон ">";

	операция "<="*( l, r: Rational ): булево;
	нач
		возврат ~(l > r)
	кон "<=";

	операция "<="*( l: Rational;  r: NbrInt.Integer ): булево;
	нач
		возврат ~(l > r)
	кон "<=";

	операция "<="*( l: NbrInt.Integer;  r: Rational ): булево;
	нач
		возврат ~(l > r)
	кон "<=";

	операция ">="*( l, r: Rational ): булево;
	нач
		возврат ~(l < r)
	кон ">=";

	операция ">="*( l: Rational;  r: NbrInt.Integer ): булево;
	нач
		возврат ~(l < r)
	кон ">=";

	операция ">="*( l: NbrInt.Integer;  r: Rational ): булево;
	нач
		возврат ~(l < r)
	кон ">=";

(** Arithmetic *)
	операция "+"*( l, r: Rational ): Rational;
	перем cd, ld, rd: NbrInt64.Integer;  sum: Rational;
	нач
		CommonDenominator( l.d, r.d, cd );  ld := l.d DIV cd;  rd := r.d DIV cd;  sum.n := l.n * rd + r.n * ld;  sum.d := l.d * rd;
		Simplify( sum );  возврат sum
	кон "+";

	операция "+"*( l: Rational;  r: NbrInt.Integer ): Rational;
	перем sum: Rational;
	нач
		sum.n := l.n + r * l.d;  sum.d := l.d;  Simplify( sum );  возврат sum
	кон "+";

	операция "+"*( l: NbrInt.Integer;  r: Rational ): Rational;
	перем sum: Rational;
	нач
		sum.n := l * r.d + r.n;  sum.d := r.d;  Simplify( sum );  возврат sum
	кон "+";

	операция "-"*( l, r: Rational ): Rational;
	перем cd, ld, rd: NbrInt64.Integer;  diff: Rational;
	нач
		CommonDenominator( l.d, r.d, cd );  ld := l.d DIV cd;  rd := r.d DIV cd;  diff.n := l.n * rd - r.n * ld;  diff.d := l.d * rd;
		Simplify( diff );  возврат diff
	кон "-";

	операция "-"*( l: Rational;  r: NbrInt.Integer ): Rational;
	перем diff: Rational;
	нач
		diff.n := l.n - r * l.d;  diff.d := l.d;  Simplify( diff );  возврат diff
	кон "-";

	операция "-"*( l: NbrInt.Integer;  r: Rational ): Rational;
	перем diff: Rational;
	нач
		diff.n := l * r.d - r.n;  diff.d := r.d;  Simplify( diff );  возврат diff
	кон "-";

	операция "*"*( l, r: Rational ): Rational;
	перем cd, ld, ln, rd, rn: NbrInt64.Integer;  prod: Rational;
	нач
		CommonDenominator( l.n, r.d, cd );  ln := l.n DIV cd;  ld := r.d DIV cd;  CommonDenominator( r.n, l.d, cd );
		rn := r.n DIV cd;  rd := l.d DIV cd;  prod.n := ln * rn;  prod.d := ld * rd;  Simplify( prod );  возврат prod
	кон "*";

	операция "*"*( l: Rational;  r: NbrInt.Integer ): Rational;
	перем cd, int: NbrInt64.Integer;  prod: Rational;
	нач
		int := r;  CommonDenominator( l.d, int, cd );  prod.n := l.n * (int DIV cd);  prod.d := l.d DIV cd;  Simplify( prod );
		возврат prod
	кон "*";

	операция "*"*( l: NbrInt.Integer;  r: Rational ): Rational;
	перем cd, int: NbrInt64.Integer;  prod: Rational;
	нач
		int := l;  CommonDenominator( int, r.d, cd );  prod.n := (int DIV cd) * r.n;  prod.d := r.d DIV cd;  Simplify( prod );
		возврат prod
	кон "*";

	операция "/"*( l, r: Rational ): Rational;
	перем cd, ld, ln, rd, rn: NbrInt64.Integer;  div: Rational;
	нач
		CommonDenominator( l.n, r.n, cd );  ln := l.n DIV cd;  ld := r.n DIV cd;  CommonDenominator( r.d, l.d, cd );
		rn := r.d DIV cd;  rd := l.d DIV cd;  div.n := ln * rn;  div.d := ld * rd;  Simplify( div );  возврат div
	кон "/";

	операция "/"*( l: Rational;  r: NbrInt.Integer ): Rational;
	перем cd, int: NbrInt64.Integer;  div: Rational;
	нач
		int := r;  CommonDenominator( l.n, int, cd );  div.n := l.n DIV cd;  div.d := l.d * (int DIV cd);  Simplify( div );  возврат div
	кон "/";

	операция "/"*( l: NbrInt.Integer;  r: Rational ): Rational;
	перем cd, int: NbrInt64.Integer;  div: Rational;
	нач
		int := l;  CommonDenominator( int, r.n, cd );  div.n := (int DIV cd) * r.d;  div.d := r.n DIV cd;  Simplify( div );  возврат div
	кон "/";

(** Additional Functions and Procedures *)
	проц Abs*( x: Rational ): Rational;
	перем abs: Rational;
	нач
		abs.n := NbrInt64.Abs( x.n );  abs.d := x.d;  возврат abs
	кон Abs;

	проц Frac*( x: Rational ): Rational;
	перем frac: Rational;
	нач
		frac.n := x.n остОтДеленияНа x.d;  frac.d := x.d;  возврат frac
	кон Frac;

	проц Max*( x1, x2: Rational ): Rational;
	нач
		если x1 > x2 то возврат x1 иначе возврат x2 всё
	кон Max;

	проц Min*( x1, x2: Rational ): Rational;
	нач
		если x1 < x2 то возврат x1 иначе возврат x2 всё
	кон Min;

	проц Sign*( x: Rational ): NbrInt.Integer;
	перем sign: NbrInt.Integer;
	нач
		если x.n < 0 то sign := -1
		аесли x.n = 0 то sign := 0
		иначе sign := 1
		всё;
		возврат sign
	кон Sign;

	(** String conversions. *)
(** Admissible characters include: {" ", "-", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "/", ","}. LEN(string) >= 53 *)
	проц StringToRat*( string: массив из симв8;  перем x: Rational );
	перем i, j: NbrInt.Integer;
		numerString, denomString: массив 32 из симв8;
	нач
		i := 0;
		(* Read in the numerator string. *)
		нцПока (string[i] # 0X) и (string[i] # "/") делай numerString[i] := string[i];  NbrInt.Inc( i ) кц;
		numerString[i] := 0X;
		если string[i] = "/" то
			(* A division sign is present. Read in the denominator string. *)
			j := 0;
			нцДо NbrInt.Inc( i );  denomString[j] := string[i];  NbrInt.Inc( j ) кцПри string[i] = 0X
		иначе
			(* Read in an integer that is to be treated as a rational. *)
			копируйСтрокуДо0( "1", denomString )
		всё;
		NbrInt64.StringToInt( numerString, x.n );  NbrInt64.StringToInt( denomString, x.d );  Simplify( x )
	кон StringToRat;

	проц RatToString*( x: Rational;  перем string: массив из симв8 );
	перем i, k: NbrInt.Integer;
		numerString, denomString: массив 32 из симв8;
	нач
		NbrInt64.IntToString( x.n, numerString );  NbrInt64.IntToString( x.d, denomString );  i := 0;
		нцДо string[i] := numerString[i];  NbrInt.Inc( i ) кцПри numerString[i] = 0X;
		k := 0;  string[i] := "/";  NbrInt.Inc( i );
		нцДо string[i] := denomString[k];  NbrInt.Inc( i );  NbrInt.Inc( k ) кцПри denomString[k] = 0X;
		string[i] := 0X
	кон RatToString;

(** Persistence: file IO *)
	проц Load*( R: Потоки.Чтец;  перем x: Rational );
	нач
		NbrInt64.Load( R, x.n );  NbrInt64.Load( R, x.d )
	кон Load;

	проц Store*( W: Потоки.Писарь;  x: Rational );
	нач
		NbrInt64.Store( W, x.n );  NbrInt64.Store( W, x.d )
	кон Store;

(** The following procedures are useful, but their use exposes the implemented type size, and therefore,
	any module that imports NbrRat and that uses any of these procedures will have to be rewritten if the
	base type size for defining rational numbers is changed. *)
	проц Numer*( x: Rational ): NbrInt64.Integer;
	нач
		возврат x.n
	кон Numer;

	проц Denom*( x: Rational ): NbrInt64.Integer;
	нач
		возврат x.d
	кон Denom;

	проц Int*( x: Rational ): NbrInt64.Integer;
	нач
		возврат x.n DIV x.d
	кон Int;

	проц Round*( x: Rational ): NbrInt64.Integer;
	перем int, twoN: NbrInt64.Integer;  frac: Rational;
	нач
		int := x.n DIV x.d;  frac := Frac( x );  twoN := 2 * frac.n;
		если twoN > frac.d то NbrInt64.Inc( int )
		аесли twoN = frac.d то
			если int >= 0 то NbrInt64.Inc( int ) всё
		иначе  (* round = int *)
		всё;
		возврат int
	кон Round;

	проц Floor*( x: Rational ): NbrInt64.Integer;
	нач
		возврат Int( x )
	кон Floor;

	проц Ceiling*( x: Rational ): NbrInt64.Integer;
	перем int: NbrInt64.Integer;
	нач
		int := Int( x );  NbrInt64.Inc( int );  возврат int
	кон Ceiling;

	проц Set*( numerator, denominator: NbrInt64.Integer;  перем x: Rational );
	нач
		x.n := numerator;  x.d := denominator;  Simplify( x );
	кон Set;

нач
	MinNbr.n := NbrInt64.MinNbr;  MinNbr.d := NbrInt64.Long( 1 );  MaxNbr.n := NbrInt64.MaxNbr;
	MaxNbr.d := NbrInt64.Long( 1 )
кон NbrRat.
