модуль UsbTools; (** AUTHOR "staubesv"; PURPOSE "USB Tools"; *)
(**
 * Usage:
 *
 *	UsbTools.Mount prefix alias [volpar] ["|" fspar] ~ 				Starts a thread that waits for the disk device to become accessible and then mounts it.
 *																	(non-blocking)
 *																	Example: UsbTools.Mount USBDrive AosFS USB0#1 ~
 *
 *
 *	UsbTools.BlockingMount prefix alias [volpar] ["|" fspar] ~ 		Waits for the disk device to become accessible and then mounts it. Should not be used by a user.
 *																	(blocking)
 *																	Example: UsbTools.BlockingMount USBDrive AosFS USB0#1 ~
 *
 *	UsbTools.AwaitAndExecute devicename commandlist ~		Starts a thread that waits for the disk device to become accessible and then executes the commands.
 *																	(non-blocking)
 *																	Example: UsbTools.AwaitAndExecute USB0 FSTools.Mount USBDrive AosFS USB0#1+PET.Open USBDrive:/PET.Mod ~
 *
 *	UsbTools.AwaitAndExecute0 devicename commandlist ~		Waits for the disk device to become accessible and then executes the commands. Not for users.
 *																	(blocking)
 *																	Example: UsbTools.AwaitAndExecute0 USB0 FSTools.Mount USBDrive AosFS USB0#1~
 *
 *	System.Free UsbTools ~
 *
 * History:
 *
 *	28.06.2006	First release (staubesv)
 *)

использует
	ЛогЯдра, Commands, Plugins, Disks, FSTools;

конст

	CommandSeparator = "+";

(** Starts a thread that first waits until the device to be mounted becomes accessible and then mounts it *)
проц Mount*(context : Commands.Context); (** prefix alias [volpar] ["|" fspar] ~ *)
перем msg : массив 8 из симв8; res : целМЗ;
нач
	Commands.Activate("UsbTools.BlockingMount", context, {}, res, msg); (* ignore result *)
кон Mount;

(** Waits for the device to be mounted and then try to mount it *)
проц BlockingMount*(context : Commands.Context); (** prefix alias [volpar] ["|" fspar] ~ *)
перем
	plugin : Plugins.Plugin;
	devicename, string : массив 128 из симв8;
нач
	(* Skip the first two parameters *)
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string);
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string);

	(* read dev#part string *)
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string);

	если ExtractDevicename(string, devicename) то

		если (context.arg.можноЛиПерейтиКМестуВПотоке¿()) то
			context.arg.ПерейдиКМестуВПотоке(0);

			ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("UsbTools: Waiting for USB device "); ЛогЯдра.пСтроку8(devicename); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			plugin := Disks.registry.Await(devicename);
			ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("UsbTools: Device "); ЛогЯдра.пСтроку8(devicename); ЛогЯдра.пСтроку8(" connected."); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;

			FSTools.Mount(context);
		иначе context.error.пСтроку8("UsbTools.BlockingMount: Argument stream expected to support SetPos."); context.error.пВК_ПС;
		всё;
	иначе context.error.пСтроку8("UsbTools.BlockingMount: Could not extract device name"); context.error.пВК_ПС;
	всё;
кон BlockingMount;

(** Waits for the specified disk device and then execute the commands *)
проц AwaitAndExecute*(context : Commands.Context); (** devicename commandlist ~ *)
перем msg : массив 8 из симв8; res : целМЗ;
нач
	Commands.Activate("UsbTools.AwaitAndExecute0", context, {}, res, msg); (* ignore result *)
кон AwaitAndExecute;

(** Waits for the specified disk device and then execute the commands. Note: Does NOT mount the device. *)
проц AwaitAndExecute0*(context : Commands.Context); (** devicename commandlist ~ *)
перем
	devicename, msg : массив 128 из симв8;
	commandList : укль на массив из симв8;
	plugin : Plugins.Plugin;
	i, size, len : размерМЗ; res : целМЗ;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(devicename);

	ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("UsbTools: Waiting for USB device "); ЛогЯдра.пСтроку8(devicename); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
	plugin := Disks.registry.Await(devicename);
	ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("UsbTools: Device "); ЛогЯдра.пСтроку8(devicename); ЛогЯдра.пСтроку8(" connected."); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;

	(* read and copy command list *)
	context.arg.ПропустиБелоеПоле;
	size := context.arg.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество();
	если size > 0 то
		нов(commandList, size + 1);
		context.arg.чБайты(commandList^, 0, size, len);
		нцДля i := 0 до size делай если commandList[i] = CommandSeparator то commandList[i] := ";"; всё; кц;

		commandList^[size] := 0X;

		Commands.Call(commandList^, {}, res, msg);
		если res # Commands.Ok то
			ЛогЯдра.ЗахватВЕдиноличноеПользование;
			ЛогЯдра.пСтроку8("UsbTools: AwaitAndExecute: Command execution error, res: "); ЛогЯдра.пЦел64(res, 0);
			ЛогЯдра.пСтроку8(" ("); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пСтроку8(")");
			ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
		всё;
	всё;
кон AwaitAndExecute0;

проц ExtractDevicename(конст devpart : массив из симв8; перем devicename : массив из симв8) : булево;
перем i : размерМЗ;
нач
	нцПока (i < длинаМассива(devpart)) и (devpart[i] # "#") и (i < длинаМассива(devicename)) делай
		devicename[i] := devpart[i];
		увел(i);
	кц;
	возврат (i < длинаМассива(devpart)) и (devpart[i] = "#");
кон ExtractDevicename;

кон UsbTools.

UsbTools.Mount USB AosFS USB0#1 ~		System.Free UsbTools ~
