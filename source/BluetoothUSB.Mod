модуль BluetoothUSB; (** AUTHOR "staubesv"; PURPOSE "HCI USB Transport Layer"; *)
(**
 *
 * History:
 *
 *	01.12.2005 Cleanup (staubesv)
 *)

использует ЛогЯдра, Потоки, Plugins, Bluetooth, UsbBluetooth, Usb, Usbdi;

тип

	UsbTransportLayer* = окласс(Bluetooth.TransportLayer)
	перем
		driver : UsbBluetooth.BluetoothDriver;
		TraceReceive*, TraceSend*: булево;

		event : Bluetooth.EventPacket; (* for EventHandler, must be global *)

		eventExpectedParams : цел32; (* expected length of parameters in bytes *)
		eventParamOffset : цел32;

		acl: Bluetooth.ACLPacket;
		aclExpectedParams : цел32; (* expected length of parameters in bytes *)
		aclParamOffset : цел32;

		проц &{перекрыта}Init*(name: массив из симв8; sender: Потоки.Делегат˛реализующийЗаписьВПоток; receiver: Потоки.Делегат˛реализующийЧтениеИзПотока);
		перем plugin : Plugins.Plugin;
		нач
			Init^(name,НУЛЬ,НУЛЬ);
			TraceSend := ложь; TraceReceive := ложь;
			plugin := Usb.usbDrivers.Get(name);
			если plugin = НУЛЬ то
				ЛогЯдра.пСтроку8("UsbBluetooth: "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(" no found."); ЛогЯдра.пВК_ПС;
			иначе
				driver:=plugin(UsbBluetooth.BluetoothDriver);
				driver.SetEventHandler(EventHandler);
				driver.SetAclHandler(ReadACL);
			всё;
		кон Init;

		проц Init2*(name: массив из симв8): булево;
		перем plugin : Plugins.Plugin;
		нач
			(* provisorisch *)
			TraceSend:=истина; TraceReceive:=истина;
			plugin := Usb.usbDrivers.Get(name);
			если plugin=НУЛЬ то
				ЛогЯдра.пСтроку8("UsbBluetooth: "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(" no found."); ЛогЯдра.пВК_ПС;
				возврат ложь;
			всё;
			driver := plugin(UsbBluetooth.BluetoothDriver);
			driver.SetEventHandler(EventHandler);
			driver.SetAclHandler(ReadACL);
			возврат истина;
		кон Init2;

		проц {перекрыта}Close*;
		кон Close;

		(** receives HCI event packet fragments (each fragment 16 bytes)  and enters them into the event queue *)
		проц EventHandler(packet : Usbdi.Buffer; actLen : размерМЗ);
		перем
			i : размерМЗ;
			eventQueue : Bluetooth.Queue;

			проц DeliverPacket;
			нач
				утв((event#НУЛЬ));
				если TraceReceive то ЛогЯдра.пСтроку8("UsbBluetooth: EventHandler: Packet added to event queue.");
				ЛогЯдра.пВК_ПС; ЛогЯдра.пВК_ПС;
				всё;
				eventQueue := sink[Bluetooth.Event];
				eventQueue.Add(event);
				eventParamOffset:=0; eventExpectedParams:=0;
				event:=НУЛЬ;
			кон DeliverPacket;

		нач
			если TraceReceive то
				ЛогЯдра.пСтроку8("UsbBluetooth: ") ; ЛогЯдра.пСтроку8(driver.name);
				ЛогЯдра.пСтроку8(": EventHandler: Incoming: "); ЛогЯдра.пЦел64(actLen, 0); ЛогЯдра.пСтроку8(" Byte(s): ");
				ЛогЯдра.пВК_ПС;
			всё;
			если event=НУЛЬ то (* should be the beginning of a new event packet *)

				если TraceReceive то
					ЛогЯдра.пСтроку8("New packet: "); ShowEvent(кодСимв8(packet[0]));
					ЛогЯдра.пСтроку8(", "); ЛогЯдра.пЦел64(кодСимв8(packet[1]), 0); ЛогЯдра.пСтроку8(" Byte(s) params:  ");
					нцДля i := 0 до actLen-1 делай ЛогЯдра.п16ричное(кодСимв8(packet[i]), -2); ЛогЯдра.пСимв8(" ") кц;
					(* if the packet contains status information, display it as text ... *)
					i := кодСимв8(packet[0]);
					если (i = 01H) или (i=03H) или ((i >= 05H) и (i<=0DH)) или (i=0FH) или (i=12H) или (i=14H) или (i=1CH) или (i=1DH) то
						ЛогЯдра.пСтроку8("Status : "); ShowErrorCode(кодСимв8(packet[2]));
					всё;
					ЛогЯдра.пВК_ПС;
				всё;
				нов(event);
				event.code:=packet[0];
				event.paramLen := кодСимв8(packet[1]);
				утв(event.paramLen < Bluetooth.MaxEventParamLen);
				если event.paramLen>14 то (* there will be more fragments of this event packet *)
					eventExpectedParams := event.paramLen-14 ;
					утв(actLen=16);
					нцДля i:=0 до 13 делай event.params[i] := packet[2+i]; кц;
					eventParamOffset := 16;

				иначе (* cool! parameters fit into this packet *)
					утв(actLen=2+event.paramLen);
					нцДля i:=0 до event.paramLen-1 делай event.params[i]:=packet[2+i]; кц;
					DeliverPacket;
				всё;

			иначе (* next fragment of packet *)

				если TraceReceive то
					ЛогЯдра.пСтроку8("Fragment: "); нцДля i := 0 до длинаМассива(packet)-1 делай ЛогЯдра.п16ричное(кодСимв8(packet[i]), -2); ЛогЯдра.пСимв8(" ") кц;ЛогЯдра.пВК_ПС;
				всё;
				если eventExpectedParams <= 16 то (* fits in this packet *)
					утв(actLen=eventExpectedParams);
					нцДля i:=0 до eventExpectedParams-1 делай event.params[eventParamOffset+i]:=packet[i];кц;
					DeliverPacket;
				иначе (* there will be at least on more packet *)
					утв(actLen=16);
					eventExpectedParams:=eventExpectedParams-16;
					eventParamOffset:=eventParamOffset+16;
					нцДля i:=0 до 15 делай event.params[eventParamOffset+i]:=packet[i]; кц;
				всё;
			всё;
		кон EventHandler;

		проц ReadACL(packet : Usbdi.Buffer; actLen : размерМЗ);
		перем
			queue: Bluetooth.Queue;
			i: размерМЗ; len: цел32;

			проц DeliverPacket;
			нач
				утв(acl#НУЛЬ);
				queue := sink[Bluetooth.ACL];
				queue.Add(acl);
				aclParamOffset:=0; aclExpectedParams:=0;
				acl:=НУЛЬ;
			кон DeliverPacket;

		нач
			если TraceReceive то ЛогЯдра.пСтроку8("UsbBluetooth: Device "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(" receives ACL: "); всё;
			если acl=НУЛЬ то (* should be the beginning of a new acl packet *)

				нов(acl);
				len := кодСимв8(packet[0]) + кодСимв8(packet[1])*100H;
				acl.handle := len остОтДеленияНа 1000H;
				acl.PB := (len DIV 1000H) остОтДеленияНа 4;
				acl.BC := (len DIV 4000H) остОтДеленияНа 4;
				acl.len := кодСимв8(packet[2]) + кодСимв8(packet[3])*100H;
				утв(acl.len <= Bluetooth.MaxACLDataLen);
				если TraceReceive то
					ЛогЯдра.пСтроку8("New Packet: "); ЛогЯдра.пЦел64(acl.len, 0); ЛогЯдра.пСтроку8(" Byte(s): ");
					нцДля i:=0 до actLen-1 делай ЛогЯдра.п16ричное(кодСимв8(packet[i]),-2); ЛогЯдра.пСимв8(" "); кц;
					ЛогЯдра.пВК_ПС;
				всё;

				если acl.len>60 то (* there will be more fragments of this ACL packet *)
					aclExpectedParams := acl.len-60 ;
					утв(actLen=64);
					нцДля i:=0 до 59 делай acl.data[i] := packet[4+i]; кц;
					aclParamOffset := 64;

				иначе (* cool. parameters fit into this packet *)
					утв(actLen=4+acl.len);
					нцДля i:=0 до acl.len-1 делай acl.data[i]:=packet[4+i]; кц;
					DeliverPacket;
				всё;

			иначе (* next fragment of packet *)

				если TraceReceive то
					ЛогЯдра.пСтроку8("Fragment: "); нцДля i:=0 до actLen-1 делай ЛогЯдра.п16ричное(кодСимв8(packet[i]),-2); ЛогЯдра.пСимв8(" "); кц;
					ЛогЯдра.пВК_ПС;
				всё;
				если aclExpectedParams <= 64 то (* fits in this packet *)
					утв(actLen=aclExpectedParams);
					нцДля i:=0 до aclExpectedParams-1 делай acl.data[aclParamOffset+i]:=packet[i];кц;
					DeliverPacket;
				иначе (* there will be at least on more packet *)
					утв(actLen=64);
					нцДля i:=0 до 63 делай acl.data[aclParamOffset+i]:=packet[i]; кц;
					aclExpectedParams:=aclExpectedParams-64;
					aclParamOffset:=aclParamOffset+64;
				всё;
			всё;
		кон ReadACL;

		проц {перекрыта}Send*(type: цел32; перем data: массив из симв8; ofs, len: цел32; перем res: целМЗ);
		перем  i: цел32;
		нач {единолично}
			если TraceSend то
				ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("UsbBluetooth: Send: "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(": ");
				ЛогЯдра.п16ричное(type, -2); ЛогЯдра.пСимв8(" ");
				нцДля i := 0 до len-1 делай ЛогЯдра.п16ричное(кодСимв8(data[ofs+i]), -2); ЛогЯдра.пСимв8(" ") кц;
				ЛогЯдра.пВК_ПС;
			всё;
			просей type из
				| Bluetooth.Command:   driver.SendCommand(data, ofs, len, res);
				| Bluetooth.ACL: driver.SendACL(data, ofs, len, res);
				(* Bluetooth.Event cannot be send to the host controller; Bluetooth.SCO would require isochronous USB transfers, which
				are not yet implemented *)
			иначе
				если TraceSend то ЛогЯдра.пСтроку8("wrong packet type"); ЛогЯдра.пВК_ПС; всё;
				res := Bluetooth.ErrInvalidParameters;
			всё;
		кон Send;

		проц {перекрыта}Send1H*(type: цел32; перем hdr: массив из симв8; hdrlen: цел32; перем data: массив из симв8; ofs, len: цел32; перем res: целМЗ);
		перем  i: цел32; buffer : укль на массив из симв8; bufferLen : цел32;
		нач
			если TraceSend то
				ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("UsbBluetooth: "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(": Send1H:");
				ЛогЯдра.пСтроку8(" HdrLen: "); ЛогЯдра.пЦел64(hdrlen, 0); ЛогЯдра.пСтроку8(" DataLen: "); ЛогЯдра.пЦел64(len, 0);
				ЛогЯдра.пСтроку8(" DataOfs: "); ЛогЯдра.пЦел64(ofs, 0); ЛогЯдра.пСтроку8(" Type: "); ЛогЯдра.п16ричное(type, -2);
				ЛогЯдра.пСтроку8(" Hdr: "); нцДля i := 0 до hdrlen-1 делай ЛогЯдра.п16ричное(кодСимв8(hdr[i]), -2); ЛогЯдра.пСимв8(" ") кц;
				ЛогЯдра.пСтроку8(" Data: "); нцДля i := 0 до len-1 делай ЛогЯдра.п16ричное(кодСимв8(data[ofs+i]), -2); ЛогЯдра.пСимв8(" ") кц;
				ЛогЯдра.пВК_ПС
			всё;
			bufferLen := hdrlen + len;
			нов(buffer,bufferLen);

			нцДля i:=0 до hdrlen-1 делай buffer[i]:=hdr[i]; кц;
			нцДля i:=0 до len-1 делай buffer[i+hdrlen]:=data[ofs+i]; кц;

			просей type из
				| Bluetooth.Command:   driver.SendCommand(buffer^, 0, bufferLen, res);
				| Bluetooth.ACL: driver.SendACL(buffer^, 0, bufferLen, res);
				(* Bluetooth.Event cannot be send to the host controller; Bluetooth.SCO would require isochronous USB transfers, which
				are not yet implemented *)
			иначе
				если TraceSend то ЛогЯдра.пСтроку8("wrong packet type"); ЛогЯдра.пВК_ПС; всё;
				res:=Bluetooth.ErrInvalidParameters;
			всё;
		кон Send1H;

		проц {перекрыта}Send2H*(type: цел32; перем hdr1: массив из симв8; hdr1len: цел32;
				перем hdr2: массив из симв8; hdr2len: цел32; перем data: массив из симв8; ofs, len: цел32; перем res: целМЗ);
		перем
			i: цел32;
			buffer : укль на массив из симв8;
			bufferLen : цел32;
		нач
			если TraceSend то
				ЛогЯдра.пСтроку8("UsbBluetooth: Send2H: "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(": ");
				ЛогЯдра.п16ричное(type, -2); ЛогЯдра.пСимв8(" ");
				нцДля i := 0 до hdr1len-1 делай ЛогЯдра.п16ричное(кодСимв8(hdr1[i]), -2); ЛогЯдра.пСимв8(" ") кц;
				нцДля i := 0 до hdr2len-1 делай ЛогЯдра.п16ричное(кодСимв8(hdr2[i]), -2); ЛогЯдра.пСимв8(" ") кц;
				нцДля i := 0 до len-1 делай ЛогЯдра.п16ричное(кодСимв8(data[ofs+i]), -2); ЛогЯдра.пСимв8(" ") кц;
				ЛогЯдра.пВК_ПС
			всё;
			bufferLen:=hdr1len+hdr2len+len;
			нов(buffer,bufferLen);

			нцДля i:=0 до hdr1len-1 делай buffer[i]:=hdr1[i]; кц;
			нцДля i:=0 до hdr2len-1 делай buffer[hdr1len+i]:=hdr2[i]; кц;
			нцДля i:=0 до len-1 делай buffer[i+hdr1len+hdr2len]:=data[ofs+i]; кц;

			просей type из
				| Bluetooth.Command:   driver.SendCommand(buffer^, 0, bufferLen, res);
				| Bluetooth.ACL: driver.SendACL(buffer^, 0, bufferLen, res);
				(* Bluetooth.Event cannot be send to the host controller; Bluetooth.SCO would require isochronous USB transfers, which
				are not yet implemented *)
			иначе
				если TraceSend то ЛогЯдра.пСтроку8("wrong packet type"); ЛогЯдра.пВК_ПС; всё;
				res := Bluetooth.ErrInvalidParameters;
			всё;
		кон Send2H;

	кон UsbTransportLayer;

проц ShowEvent(event : цел32);
нач
	просей event из
		 01H: ЛогЯдра.пСтроку8("Inquiry Compete");
		|02H: ЛогЯдра.пСтроку8("Inquiry Result");
		|03H: ЛогЯдра.пСтроку8("Connection Complete");
		|04H: ЛогЯдра.пСтроку8("Connection Request");
		|05H: ЛогЯдра.пСтроку8("Disconnection Complete");
		|06H: ЛогЯдра.пСтроку8("Authentication Complete");
		|07H: ЛогЯдра.пСтроку8("Remote Name Request Complete");
		|08H: ЛогЯдра.пСтроку8("Encryption Change");
		|09H: ЛогЯдра.пСтроку8("Change Connection Link Key Complete");
		|0AH: ЛогЯдра.пСтроку8("Master Link Key Complete");
		|0BH: ЛогЯдра.пСтроку8("Read Remote Supported Features Complete");
		|0CH: ЛогЯдра.пСтроку8("Read Remote Version Information Complete");
		|0DH: ЛогЯдра.пСтроку8("QoS Setup Complete");
		|0EH: ЛогЯдра.пСтроку8("Command Complete");
		|0FH: ЛогЯдра.пСтроку8("Command Status");
		|10H: ЛогЯдра.пСтроку8("Hardware Error");
		|11H: ЛогЯдра.пСтроку8("Flush Occured");
		|12H: ЛогЯдра.пСтроку8("Role Change");
		|13H: ЛогЯдра.пСтроку8("Number Of Completed Packets");
		|14H: ЛогЯдра.пСтроку8("Mode Change");
		|15H: ЛогЯдра.пСтроку8("Return Link Keys");
		|16H: ЛогЯдра.пСтроку8("PIN Code Request");
		|17H: ЛогЯдра.пСтроку8("Link Key Request");
		|18H: ЛогЯдра.пСтроку8("Link Key Notification");
		|19H: ЛогЯдра.пСтроку8("Loopback Command");
		|1AH: ЛогЯдра.пСтроку8("Data Buffer Overflow");
		|1BH: ЛогЯдра.пСтроку8("Max Slots Change");
		|1CH: ЛогЯдра.пСтроку8("Read Clock Offset Complete");
		|1DH: ЛогЯдра.пСтроку8("Connection Packet Type Changed");
		|1EH: ЛогЯдра.пСтроку8("QoS Violation");
		|1FH: ЛогЯдра.пСтроку8("Page Scan Mode Change");
		|20H: ЛогЯдра.пСтроку8("Page Scan Repetition Mode Change");
		|0FEH: ЛогЯдра.пСтроку8("Bluetooth Logo Testing");
		|0FFH: ЛогЯдра.пСтроку8("Vendor-specific");
	иначе
		ЛогЯдра.пСтроку8("Unkown");
	всё;
кон ShowEvent;

проц ShowErrorCode(errorcode : целМЗ);
нач
	просей errorcode из
		00H: ЛогЯдра.пСтроку8("OK");
		| 01H: ЛогЯдра.пСтроку8("Unknown HCI Command");
		| 02H: ЛогЯдра.пСтроку8("No Connection");
		| 03H: ЛогЯдра.пСтроку8("Hardware Failure");
		| 04H: ЛогЯдра.пСтроку8("Page Timeout");
		| 05H: ЛогЯдра.пСтроку8("Authentication Failure");
		| 06H: ЛогЯдра.пСтроку8("Key Missing");
		| 07H: ЛогЯдра.пСтроку8("Memory Full");
		| 08H: ЛогЯдра.пСтроку8("Connection Timeout");
		| 09H: ЛогЯдра.пСтроку8("Max Number Of Connections");
		| 0AH: ЛогЯдра.пСтроку8("Max Number Of SCO Connection To A Device");
		| 0BH: ЛогЯдра.пСтроку8("ACL Connection Already Exists");
		| 0CH: ЛогЯдра.пСтроку8("Command Disallowed");
		| 0DH: ЛогЯдра.пСтроку8("Host Rejected due to limited resources");
		| 0EH: ЛогЯдра.пСтроку8("Host Rejected due to security reasons");
		| 0FH: ЛогЯдра.пСтроку8("Host Rejected (Remote Device is personal device)");
		| 10H: ЛогЯдра.пСтроку8("Host Timeout");
		| 11H: ЛогЯдра.пСтроку8("Unsupported Feature or Parameter Value");
		| 12H: ЛогЯдра.пСтроку8("Invalid HCI Command Parameters");
		| 13H: ЛогЯдра.пСтроку8("Other End Terminated Connection (User ended connection)");
		| 14H: ЛогЯдра.пСтроку8("Other End Terminated Connection (Low Resources)");
		| 15H: ЛогЯдра.пСтроку8("Other End Terminated Connection (About to Power Off)");
		| 16H: ЛогЯдра.пСтроку8("Connection Terminated by Local Host");
		| 17H: ЛогЯдра.пСтроку8("Repeated Attempts");
		| 18H: ЛогЯдра.пСтроку8("Pairing Not Allowd");
		| 19H: ЛогЯдра.пСтроку8("Unknown LMP PDU");
		| 1AH: ЛогЯдра.пСтроку8("Unsupported Remote Feature");
		| 1BH: ЛогЯдра.пСтроку8("SCO Offset Rejected");
		| 1CH: ЛогЯдра.пСтроку8("SCO Interval Rejected");
		| 1DH: ЛогЯдра.пСтроку8("SCO Airmode Rejected");
		| 1EH: ЛогЯдра.пСтроку8("Invalid LMP Parameters");
		| 1FH: ЛогЯдра.пСтроку8("Unspecified Error");
		| 20H: ЛогЯдра.пСтроку8("Unsupported LMP Parameter Value");
		| 21H: ЛогЯдра.пСтроку8("Role Change Not Allowed");
		| 22H: ЛогЯдра.пСтроку8("LMP Response Timeout");
		| 23H: ЛогЯдра.пСтроку8("LMP Error Transaction Collision");
		| 24H: ЛогЯдра.пСтроку8("LMP PDU Not Allowed");
		| 25H: ЛогЯдра.пСтроку8("Encryption Mode Not Acceptable");
		| 26H: ЛогЯдра.пСтроку8("Unit Key Used");
		| 27H: ЛогЯдра.пСтроку8("QoS is Not Supported");
		| 28H: ЛогЯдра.пСтроку8("Instant Passed");
		| 29H: ЛогЯдра.пСтроку8("Pairing with Unit Key Not Supported");
		| 2AH..0FFH: ЛогЯдра.пСтроку8("Reserved for Future Use");
	иначе
		ЛогЯдра.пСтроку8("Unknown");
	всё;
кон ShowErrorCode;

кон BluetoothUSB.

