(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль NbrRe;   (** AUTHOR "adf"; PURPOSE "Defines a base Real type for scientific computing."; *)

(* To change to 64-bit reals, change all code that is in light red. *)

использует Потоки, NbrInt, NbrInt8, NbrInt16, NbrInt32, NbrInt64, NbrRat, NbrRe32;

(** This module hides the type size of the real implemented.  This makes it a straightforward process for
	the user to change this one module, and in doing so, change the type size for all reals without having
	to change any modules that import NbrRe, at least in principle.  That was one of our design goals. *)

конст
	E* = NbrRe32.E;  Pi* = NbrRe32.Pi;

тип
	Real* = NbrRe32.Real;

перем
	MinNbr-, MaxNbr-,
	(** Machine epsilon, i.e., e. *)
	Epsilon-: Real;
	(** Machine radix, or base number. *)
	Radix-: NbrInt.Integer;

	(** Type Conversions *)
	проц RatToRe( n: NbrRat.Rational ): Real;
	перем den, frac, int, num: NbrInt64.Integer;  denomR, intR, fracR, r: Real;
	нач
		num := NbrRat.Numer( n );  den := NbrRat.Denom( n );  denomR := den;
		если num > 0 то int := num DIV den;  intR := int;  frac := num остОтДеленияНа den;  fracR := frac;  r := intR + fracR / denomR
		аесли num < 0 то int := -num DIV den;  intR := int;  frac := -num остОтДеленияНа den;  fracR := frac;  r := -intR - fracR / denomR
		иначе r := 0
		всё;
		возврат r
	кон RatToRe;

	операция ":="*( перем l: Real;  r: NbrRat.Rational );
	нач
		l := RatToRe( r )
	кон ":=";

	проц ReToRat*( x: Real ): NbrRat.Rational;
	(* Algorithm from:  Spanier and Oldham, An Atlas of Functions,
			Hemisphere Publishing Corp., Washington DC, 1987, pg. 666. *)
	перем rat: NbrRat.Rational;  dd, denom, nn, num, r: NbrInt64.Integer;  sign: NbrInt32.Integer;
		abs, error, errorLast, ratio, tol, y: Real;

		проц Swap( перем a, b: NbrInt64.Integer );
		перем temp: NbrInt64.Integer;
		нач
			temp := a;  a := b;  b := temp
		кон Swap;

	нач
		y := 1 / (Epsilon * Epsilon);  tol := 1;  error := MaxNbr;
		нцПока y > 10 делай y := y / 10;  tol := tol / 10 кц;
		если x < 0 то abs := -x;  sign := -1
		аесли x = 0 то rat := 0;  возврат rat
		иначе abs := x;  sign := 1
		всё;
		num := NbrRe32.LEntier( abs );  denom := 1;
		если Frac( abs ) = 0 то num := sign * num;  NbrRat.Set( num, denom, rat );  возврат rat всё;
		nn := num + 1;  dd := denom;  ratio := (nn - abs * dd) / (abs * denom - num);
		если ratio < 1 то Swap( num, nn );  Swap( denom, dd ) всё;
		нцДо
			если ratio < 1 то ratio := 1 / ratio всё;
			r := NbrRe32.LEntier( ratio );  nn := nn + num * r;  dd := dd + denom * r;  num := num + nn;  denom := denom + dd;
			ratio := (nn - abs * dd) / (abs * denom - num);
			если ratio < 1 то Swap( num, nn );  Swap( denom, dd ) всё;
			errorLast := error;  error := Abs( 1 - num / (abs * denom) )
		кцПри (error < tol) или (errorLast < error);
		num := sign * num;  NbrRat.Set( num, denom, rat );  возврат rat
	кон ReToRat;

(** Comparison Operators *)
	операция "="*( l: Real;  r: NbrRat.Rational ): булево;
	нач
		возврат l = RatToRe( r )
	кон "=";

	операция "="*( l: NbrRat.Rational;  r: Real ): булево;
	нач
		возврат RatToRe( l ) = r
	кон "=";

	операция "#"*( l: Real;  r: NbrRat.Rational ): булево;
	нач
		возврат l # RatToRe( r )
	кон "#";

	операция "#"*( l: NbrRat.Rational;  r: Real ): булево;
	нач
		возврат RatToRe( l ) # r
	кон "#";

	операция "<"*( l: Real;  r: NbrRat.Rational ): булево;
	нач
		возврат l < RatToRe( r )
	кон "<";

	операция "<"*( l: NbrRat.Rational;  r: Real ): булево;
	нач
		возврат RatToRe( l ) < r
	кон "<";

	операция ">"*( l: Real;  r: NbrRat.Rational ): булево;
	нач
		возврат l > RatToRe( r )
	кон ">";

	операция ">"*( l: NbrRat.Rational;  r: Real ): булево;
	нач
		возврат RatToRe( l ) > r
	кон ">";

	операция "<="*( l: Real;  r: NbrRat.Rational ): булево;
	нач
		возврат l <= RatToRe( r )
	кон "<=";

	операция "<="*( l: NbrRat.Rational;  r: Real ): булево;
	нач
		возврат RatToRe( l ) <= r
	кон "<=";

	операция ">="*( l: Real;  r: NbrRat.Rational ): булево;
	нач
		возврат l >= RatToRe( r )
	кон ">=";

	операция ">="*( l: NbrRat.Rational;  r: Real ): булево;
	нач
		возврат RatToRe( l ) >= r
	кон ">=";

(** Arithmetic *)
	операция "+"*( l: Real;  r: NbrRat.Rational ): Real;
	нач
		возврат l + RatToRe( r )
	кон "+";

	операция "+"*( l: NbrRat.Rational;  r: Real ): Real;
	нач
		возврат RatToRe( l ) + r
	кон "+";

	операция "-"*( l: Real;  r: NbrRat.Rational ): Real;
	нач
		возврат l - RatToRe( r )
	кон "-";

	операция "-"*( l: NbrRat.Rational;  r: Real ): Real;
	нач
		возврат RatToRe( l ) - r
	кон "-";

	операция "*"*( l: Real;  r: NbrRat.Rational ): Real;
	нач
		возврат l * RatToRe( r )
	кон "*";

	операция "*"*( l: NbrRat.Rational;  r: Real ): Real;
	нач
		возврат RatToRe( l ) * r
	кон "*";

	операция "/"*( l: Real;  r: NbrRat.Rational ): Real;
	нач
		возврат l / RatToRe( r )
	кон "/";

	операция "/"*( l: NbrRat.Rational;  r: Real ): Real;
	нач
		возврат RatToRe( l ) / r
	кон "/";

(** Basic Functions*)
	проц Abs*( x: Real ): Real;
	нач
		возврат NbrRe32.Abs( x )
	кон Abs;

	проц Entier*( x: Real ): NbrInt.Integer;
	нач
		возврат NbrRe32.Entier( x )
	кон Entier;

	проц Max*( x1, x2: Real ): Real;
	нач
		возврат NbrRe32.Max( x1, x2 )
	кон Max;

	проц Min*( x1, x2: Real ): Real;
	нач
		возврат NbrRe32.Min( x1, x2 )
	кон Min;

	проц Sign*( x: Real ): NbrInt.Integer;
	перем sInt: NbrInt8.Integer;  lInt: NbrInt32.Integer;
	нач
		sInt := NbrRe32.Sign( x );  lInt := NbrInt32.Long( NbrInt16.Long( sInt ) );  возврат lInt
	кон Sign;

	проц Int*( x: Real ): NbrInt.Integer;
	нач
		возврат NbrRe32.Int( x )
	кон Int;

	проц Frac*( x: Real ): Real;
	нач
		возврат NbrRe32.Frac( x )
	кон Frac;

	проц Round*( x: Real ): NbrInt.Integer;
	нач
		возврат NbrRe32.Round( x )
	кон Round;

	проц Floor*( x: Real ): NbrInt.Integer;
	нач
		возврат NbrRe32.Floor( x )
	кон Floor;

	проц Ceiling*( x: Real ): NbrInt.Integer;
	нач
		возврат NbrRe32.Ceiling( x )
	кон Ceiling;

(** Functions based on:  real = mantissa * (radix ^ exponent) *)
	проц Mantissa*( x: Real ): Real;
	нач
		возврат NbrRe32.Mantissa( x )
	кон Mantissa;

	проц Exponent*( x: Real ): NbrInt.Integer;
	перем exp: NbrInt16.Integer;
	нач
		exp := NbrRe32.Exponent( x );  возврат NbrInt32.Long( exp )
	кон Exponent;

	проц Re*( mantissa: Real;  exponent: NbrInt.Integer ): Real;
	перем exp: NbrInt16.Integer;
	нач
		exp := NbrInt32.Short( exponent );  возврат NbrRe32.Re( mantissa, exp )
	кон Re;

(** The basic Math functions. *)

	проц Sqrt*( x: Real ): Real;
	нач
		возврат NbrRe32.Sqrt( x )
	кон Sqrt;

	проц Sin*( x: Real ): Real;
	нач
		возврат NbrRe32.Sin( x )
	кон Sin;

	проц Cos*( x: Real ): Real;
	нач
		возврат NbrRe32.Cos( x )
	кон Cos;

	проц ArcTan*( x: Real ): Real;
	нач
		возврат NbrRe32.ArcTan( x )
	кон ArcTan;

	проц Exp*( x: Real ): Real;
	нач
		возврат NbrRe32.Exp( x )
	кон Exp;

	проц Ln*( x: Real ): Real;
	нач
		возврат NbrRe32.Ln( x )
	кон Ln;

(** String conversions. LEN(string) >= significantFigures + 6 *)
	проц StringToRe*( string: массив из симв8;  перем x: Real );
	нач
		NbrRe32.StringToRe( string, x )
	кон StringToRe;

	проц ReToString*( x: Real;  significantFigures: NbrInt.Integer;  перем string: массив из симв8 );
	перем sigFig: NbrInt8.Integer;
	нач
		sigFig := NbrInt16.Short( NbrInt32.Short( significantFigures ) );  NbrRe32.ReToString( x, sigFig, string )
	кон ReToString;

(** Persistence: file IO *)
	проц Load*( R: Потоки.Чтец;  перем x: Real );
	нач
		NbrRe32.Load( R, x )
	кон Load;

	проц Store*( W: Потоки.Писарь;  x: Real );
	нач
		NbrRe32.Store( W, x )
	кон Store;

нач
	MinNbr := NbrRe32.MinNbr;  MaxNbr := NbrRe32.MaxNbr;  Epsilon := NbrRe32.Epsilon;
	Radix := NbrInt32.Long( NbrInt16.Long( NbrRe32.Radix ) )
кон NbrRe.
