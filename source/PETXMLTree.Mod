модуль PETXMLTree; (** AUTHOR "TF/staubesv"; PURPOSE "XML Structure Viewer for PET"; *)

использует
	Modules, Потоки, Diagnostics, CompilerInterface, Strings, Texts, PETTrees, WMTrees,
	XML, XMLObjects, XMLScanner, XMLParser, UTF8Strings;

тип

	Tree* = окласс(PETTrees.Tree)
	перем
		diagnostics : Diagnostics.Diagnostics;
		log : Потоки.Писарь;
		hasErrors : булево;

		проц &{перекрыта}Init*;
		нач
			Init^;
			diagnostics := НУЛЬ;
			log := НУЛЬ;
			hasErrors := ложь;
		кон Init;

		проц AddSubNode(node : PETTrees.TreeNode; xml : XML.Element );
		перем
			en : XMLObjects.Enumerator; newNode : PETTrees.TreeNode;
			p : динамическиТипизированныйУкль; s, t, c : Strings.String;
		нач
			нов(newNode);
			tree.AddChildNode(node, newNode);
			SetNodeInfo(newNode, xml.GetPos());

			s := xml.GetName();
			t := xml.GetAttributeValue("name");
			если (t # НУЛЬ) то
				нов(c,Strings.Length(s^) + Strings.Length(t^) + 1 + 4);
				c[0] := 0X;
				если (s # НУЛЬ) то
					Strings.Append(c^,s^);
					Strings.Append(c^,': ');
				всё;
				Strings.Append(c^,'"');
				Strings.Append(c^,t^);
				Strings.Append(c^,'"');
			иначе
				c := s;
			всё;

			если (c # НУЛЬ) то tree.SetNodeCaption(newNode, c) всё;

			en := xml.GetContents();
			нцПока en.HasMoreElements() делай
				p := en.GetNext();
				если p суть XML.Element то
					AddSubNode(newNode, p(XML.Element));
				всё
			кц;
		кон AddSubNode;

		проц SetDocument(xml : XML.Element);
		перем en : XMLObjects.Enumerator; p : динамическиТипизированныйУкль; string : Strings.String; node : PETTrees.TreeNode;
		нач
			нов(node);
			tree.Acquire;
			tree.SetRoot(node);
			tree.SetNodeState(node, {WMTrees.NodeAlwaysExpanded});

			если xml # НУЛЬ то
				string := xml.GetName();
				если (string = НУЛЬ) то
					tree.SetNodeCaption(node, Strings.NewString("Document"));
				иначе
					tree.SetNodeCaption(node, string);
				всё;
				SetNodeInfo(node, xml.GetPos());
				en := xml.GetContents();

				нцПока en.HasMoreElements() делай
					p := en.GetNext();
					если p суть XML.Element то
						AddSubNode(node, p(XML.Element));
					всё
				кц
			иначе
				tree.SetNodeCaption(node, Strings.NewString("No Document"));
			всё;
			tree.Release
		кон SetDocument;

		проц Error(pos, line, row: Потоки.ТипМестоВПотоке; конст msg: массив из симв8);
		перем diagnostics : Diagnostics.Diagnostics; log : Потоки.Писарь;
		нач
			diagnostics := сам.diagnostics;
			log := сам.log;
			hasErrors := истина;
		кон Error;

		проц {перекрыта}AddNodes*(parent : PETTrees.TreeNode; diagnostics : Diagnostics.Diagnostics; log : Потоки.Писарь);
		перем r : Потоки.ЧтецИзСтроки;
			scanner : XMLScanner.Scanner;
			parser : XMLParser.Parser;
			doc : XML.Document;
			tr : Texts.TextReader; ch : Texts.Char32; i, p : размерМЗ; resstr : массив 7 из симв8;
			s : Strings.String;
			text : Texts.Text; out : Потоки.Писарь; ob : Strings.Buffer; hasErrors : булево;
		нач
			AddNodes^(parent, diagnostics, log);
			hasErrors := ложь;
			text := editor.text;
			text.AcquireRead;
			нов(ob, (text.GetLength() * 3 DIV 2)); (* heuristic to avoid growing in most cases *)
			out := ob.GetWriter();

			нов(tr, text);
			нцДля i := 0 до text.GetLength() - 1 делай
				tr.ReadCh(ch); p := 0;
				если (ch > Texts.Симв32Нулевой) и UTF8Strings.EncodeChar(ch, resstr, p) то out.пСтроку8(resstr) всё
			кц;
			out.ПротолкниБуферВПоток;
			text.ReleaseRead;

			нов(r, ob.GetLength() + 1);
			s := ob.GetString();
			r.ПримиСрезСтроки8ВСтрокуДляЧтения(s^, 0, ob.GetLength());
			нов(scanner, r); scanner.reportError := Error;
			нов(parser, scanner); parser.reportError := Error;
			doc := parser.Parse();
			если hasErrors то SetTitle("XML Structure (ERRORS)");
			иначе
				SetTitle("XML Structure");
			всё;
			если doc # НУЛЬ то
				SetDocument(doc.GetRoot())
			всё;
		кон AddNodes;

		проц SetNodeInfo(node : PETTrees.TreeNode; position : Потоки.ТипМестоВПотоке);
		нач
			если (position >= 0) то
				нов(node.pos, editor.text);
				node.pos.SetPosition(position(размерМЗ));
			иначе
				node.pos := НУЛЬ;
			всё;
		кон SetNodeInfo;

	кон Tree;

тип

	ErrorReporter = окласс
	перем
		diagnostics : Diagnostics.Diagnostics;
		hasErrors : булево;

		проц ReportError(pos, line, row : Потоки.ТипМестоВПотоке; конст msg : массив из симв8);
		нач
			diagnostics.Error("PET", pos, msg);
			hasErrors := истина;
		кон ReportError;

		проц &Init(diagnostics : Diagnostics.Diagnostics);
		нач
			утв(diagnostics # НУЛЬ);
			сам.diagnostics := diagnostics;
			hasErrors := ложь;
		кон Init;

	кон ErrorReporter;

проц ParseText(
	text : Texts.Text; конст source: массив из симв8; pos: цел32; конст pc,opt: массив из симв8;
	log: Потоки.Писарь; diagnostics : Diagnostics.Diagnostics; перем error: булево);
перем
	r : Потоки.ЧтецИзСтроки;
	scanner : XMLScanner.Scanner;
	parser : XMLParser.Parser;
	doc : XML.Document;
	tr : Texts.TextReader; ch : Texts.Char32; i, p : размерМЗ; resstr : массив 7 из симв8;
	s : Strings.String;
	out : Потоки.Писарь; ob : Strings.Buffer;
	errors : ErrorReporter;
нач
	утв((text # НУЛЬ) и (diagnostics # НУЛЬ));
	text.AcquireRead;
	нов(ob, (text.GetLength() * 3 DIV 2)); (* heuristic to avoid growing in most cases *)
	out := ob.GetWriter();

	нов(tr, text);
	нцДля i := 0 до text.GetLength() - 1 делай
		tr.ReadCh(ch); p := 0;
		если (ch > Texts.Симв32Нулевой) и UTF8Strings.EncodeChar(ch, resstr, p) то out.пСтроку8(resstr) всё
	кц;
	out.ПротолкниБуферВПоток;
	text.ReleaseRead;

	нов(r, ob.GetLength() + 1);
	s := ob.GetString();
	r.ПримиСрезСтроки8ВСтрокуДляЧтения(s^, 0, ob.GetLength());
	нов(errors, diagnostics);
	нов(scanner, r); scanner.reportError := errors.ReportError;
	нов(parser, scanner); parser.reportError := errors.ReportError;
	error := errors.hasErrors;
	doc := parser.Parse();
	если (log # НУЛЬ) то
		если error то log.пСтроку8("XML Parser reports errors"); иначе log.пСтроку8("XML Parser: OK"); всё;
		log.ПротолкниБуферВПоток;
	всё;
кон ParseText;

проц GenXMLTree*() : PETTrees.Tree;
перем tree : Tree;
нач
	нов(tree); возврат tree;
кон GenXMLTree;

проц Cleanup;
нач
	CompilerInterface.Unregister("XML");
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	CompilerInterface.Register("XML", "XML Parser", "XML", ParseText);
кон PETXMLTree.
