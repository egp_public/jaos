## Виды отладочной печати

* При возникновении исключения появляется красный экран, а также возникает в рабочей директории файл Trap*.txt 
* Модуль Trace - печатает в консоль (если она показана)
* Модуль KernelLog - печатает в консоль и на окошко
* Подробная печать - печатает в поток (в т.ч. может печатать и в KernelLog) структуры данных рекурсивно

## Подробная печать

Подробная печать реализована в модулях PodrobnajaPechatq.Mod и r.Mod. 

Нужно смотреть коммиты от https://gitlab.com/budden/jaos/commit/1464dfe95c48fb378b05cc25bfdc5a4e2b66cbd8 и позднее. 
Файлы нужно откомпилировать перед использованием. 

### Печать текущих локальных переменных
``` 
IMPORT PodrobnajaPechatq; 
...
VAR kw : Streams.Writer;
BEGIN
 Streams.OpenWriter( kw, KernelLog.Send );
 PodrobnajaPechatq.TraceBack(kw, 1, 1);
 kw.Update; (* Возможно, что это необязательно *)
```

### Печать переменной типа ANY или OBJECT

``` 
IMPORT PodrobnajaPechatq;
...
PodrobnajaPechatq.Pech(perem or value);
...
```

### Показ состояния модуля:

``` 
VAR kw : Streams.Writer;
BEGIN
 Streams.OpenWriter( kw, KernelLog.Send );
 PodrobnajaPechatq.ModuleState(kw, m); 
 kw.Update; (* Возможно, что это необязательно *)
```
Или с помощью команды:

``` 
r.ПокажиСостояниеМодуля TFTypeSys~
```


### Печать куска памяти в 16-ричном виде

``` 
WriteValueHexWords(писатель, адрес, число-16-разрядных-слов)
```

Также есть KernelLog.Memory. 

## См. также 

Другие статьи в пункте "отладка" и [Обработка TRAP-ов](обработка-trap-ов.md)

