(** AUTHOR "Yaroslav Romanchenko"; PURPOSE "Base Generic Collections"; *)

(*

	Module implements the base generic collection objects:
		
		Queue - simple (L)ast (I)n (F)irst (O)ut stack;
		
		DEQue - Double Ended Queue or (F)irst (I)n (F)irst (O)ut stack;
		
		Vector - simple linear container with possibilities of out of order elements' insertions and removal.
			Also sorting and searching of elements are supported.
	
	Parameters of this module are:
		
		T¿ - any type;
		
		Less¿ - procedure, that returns result of "less than" comparison,
			with signature "(l, r: T¿): BOOLEAN". Less¿ procedure are present
			mostly for Vector's sorting and searching functionality.
		
*)


(** Имя модуля было GenericCollections(TYPE T¿, CONST Less¿), причём Less¿ была процедурой. 
  Поскольку мы хотим догнать и перегнать, у нас Less¿ будет метапроцедурой, т.е. мы сможем
  использовать операцию "<" без вызова процедуры и посмотрим, насколько быстрее получится.
  
  
  Однако наш механизм более тяжеловесный и мы не можем (пока) генерировать целые неявные модули.
  Заголовок модуля придётся создать пользователю. Можно пытаться увидеть в этом и плюсы. 

*)

конст
	(* Initial size of containers *)
	INITIAL_SIZE* = 16;

тип

	(** Abstract container *)
	Container* = окласс

		проц Count*(): размерМЗ;
		нач СТОП(301)
		кон Count;
		
		проц Empty*(): булево;
		нач СТОП(301)
		кон Empty;
		
		проц Reset*();
		нач СТОП(301)
		кон Reset;
		
	кон Container;
	
	PArrayT* = укль на массив из T¿;

	(** Abstract Linear Container, based on dynamic array of T¿ *)
	LinearContainer* = окласс(Container)
	перем
		m_iSize : размерМЗ;
		m_pArray : PArrayT;
		
		проц Grow();
		перем
			pOldArray : PArrayT;
			iOldSize, i : размерМЗ;
		нач
			pOldArray := m_pArray;
			iOldSize := m_iSize;
			m_iSize := m_iSize * 2;
			NewStorage;
			нцДля i := 0 до iOldSize - 1 делай
				m_pArray[i] := pOldArray[i]
			кц
		кон Grow;
		
		проц &New*();
		нач
			m_iSize := INITIAL_SIZE;
			NewStorage;
			Reset
		кон New;
		
		(** Allocation of new storage of size set in m_iSize *)
		проц NewStorage();
		нач
			нов(m_pArray, m_iSize)
		кон NewStorage;
		
	кон LinearContainer;
	
	(** Abstract stack *)
	Stack* = окласс(LinearContainer)

		проц Pop*(перем t: T¿): булево;
		нач СТОП(301)
		кон Pop;

		проц Push*(конст t: T¿);
		нач СТОП(301)
		кон Push;

		проц Top*(перем t: T¿): булево;
		нач СТОП(301)
		кон Top;
		
	кон Stack;

	(** Simple Queue or LIFO stack *)
	Queue* = окласс(Stack)
	перем
		m_iPos: размерМЗ;

		проц Reset*();
		нач
			m_iPos := 0
		кон Reset;

		проц Count*(): размерМЗ;
		нач
			возврат m_iPos
		кон Count;
		
		проц Empty*(): булево;
		нач
			возврат m_iPos = 0
		кон Empty;
		
		проц Pop*(перем t: T¿): булево;
		перем
			bOk: булево;
		нач
			bOk := m_iPos # 0;
			если bOk то
				умень(m_iPos); t := m_pArray[m_iPos]
			всё;
			возврат bOk
		кон Pop;

		проц Push*(конст t: T¿);
		нач
			если m_iPos = m_iSize то Grow всё;
			m_pArray[m_iPos] := t;
			увел(m_iPos)
		кон Push;

		проц Top*(перем t: T¿): булево;
		перем
			bOk: булево;
		нач
			bOk := m_iPos # 0;
			если bOk то t := m_pArray[m_iPos - 1] всё;
			возврат bOk
		кон Top;

	кон Queue;
	
	(** Double Ended Queue or FIFO stack *)
	DEQue* = окласс(Stack)
	перем
		m_iHead, m_iTail: размерМЗ;

		проц Reset*();
		нач
			m_iHead := 0;
			m_iTail := 0
		кон Reset;

		проц Count*(): размерМЗ;
		нач
			возврат m_iTail - m_iHead
		кон Count;
		
		проц Empty*(): булево;
		нач
			возврат m_iTail - m_iHead = 0
		кон Empty;

		проц Grow();
		перем
			iCount, i: размерМЗ;
		нач
			iCount := m_iTail - m_iHead;
			если iCount = 0 то
				(* DEQue is empty, simply reset... *)
				m_iHead := 0; m_iTail := 0
			аесли iCount * 2 > m_iSize то
				(* Must grow, call default implementation *)
				Grow^
			иначе
				(* may for now simply shift,
					to move free space to the end *)
				нцДля i := 0 до iCount - 1 делай
					m_pArray[i] := m_pArray[m_iHead + i]
				кц;
				m_iHead := 0; m_iTail := iCount
			всё
		кон Grow;

		проц Pop*(перем t: T¿): булево;
		перем
			bOk: булево;
		нач
			bOk := m_iHead # m_iTail;
			если m_iHead # m_iTail то
				t := m_pArray[m_iHead]; увел(m_iHead)
			всё;
			возврат bOk
		кон Pop;
		
		проц Push*(конст t: T¿);
		нач
			если m_iTail = m_iSize то Grow всё;
			m_pArray[m_iTail] := t;
			увел(m_iTail)
		кон Push;

		проц Top*(перем t: T¿): булево;
		перем
			bOk: булево;
		нач
			bOk := m_iHead # m_iTail;
			если bOk то t := m_pArray[m_iHead] всё;
			возврат bOk
		кон Top;

	кон DEQue;

	Vector* = окласс(LinearContainer)
	перем
		m_iPos: размерМЗ;
		f_bSorted: булево;
		
		проц Add*(конст t: T¿): размерМЗ;
		перем
			pos: размерМЗ;
		нач
			(* Append as to unsorted array *)
			f_bSorted := ложь;
			если m_iPos = m_iSize то Grow всё;
			pos := m_iPos;
			m_pArray[pos] := t;
			увел(m_iPos);
			возврат pos
		кон Add;

		проц Reset*();
		нач
			f_bSorted := ложь;
			m_iPos := 0
		кон Reset;

		проц Count*(): размерМЗ;
		нач
			возврат m_iPos
		кон Count;
		
		проц Empty*(): булево;
		нач
			возврат m_iPos = 0
		кон Empty;
		
		проц FindBinary(конст t: T¿; перем bFound: булево): размерМЗ;
		перем
			lower, middle, upper: размерМЗ;
		нач
			bFound := ложь;
			если m_iPos = 0 то возврат 0 всё;
			lower := 0;
			upper := m_iPos - 1;
			нцПока lower <= upper делай
				middle := lower + (upper - lower) DIV 2;
				если Less¿(m_pArray[middle], t) то
					lower := middle + 1
				аесли Less¿(t, m_pArray[middle]) то
					upper := middle - 1
				иначе
					bFound := истина;
					возврат middle
				всё;
			кц;
			если lower <= upper то
				возврат upper
			иначе
				возврат lower
			всё;
		кон FindBinary;

		проц FindSequentially(конст t: T¿): размерМЗ;
		перем
			i: размерМЗ;
		нач
			i := 0;
			нцПока i < m_iPos делай
				если ~Less¿(m_pArray[i], t) и ~Less¿(t, m_pArray[i]) то
					возврат i
				всё;
				увел(i)
			кц;
			возврат -1
		кон FindSequentially;
		
		проц Get*(pos: размерМЗ; перем t: T¿);
		нач
			утв((pos >= 0) и (pos < m_iPos), 101);
			t := m_pArray[pos]
		кон Get;

		проц GetSorted*(): булево;
		нач
			возврат f_bSorted
		кон GetSorted;
		
		проц IndexOf*(конст t: T¿): размерМЗ;
		перем
			pos: размерМЗ;
			bFound: булево;
		нач
			если f_bSorted то
				pos := FindBinary(t, bFound);
				если bFound то
					возврат pos
				иначе
					возврат -1
				всё
			иначе
				возврат FindSequentially(t)
			всё
		кон IndexOf;

		проц Insert*(pos: размерМЗ; конст t: T¿);
		перем
			iSrc: размерМЗ;
		нач
			утв((pos >= 0) и (pos < m_iPos), 101);
			(* Insert as to unsorted array *)
			f_bSorted := ложь;
			если m_iPos = m_iSize то
				Grow
			всё;
			(* Shift content of array *)
			iSrc := m_iPos - 1;
			нцПока iSrc >= pos делай
				m_pArray[iSrc + 1] := m_pArray[iSrc];
				умень(iSrc)
			кц;
			(* Put t at pos position *)
			m_pArray[pos] := t;
			увел(m_iPos)
		кон Insert;
		
		проц QuickSort(l, r: размерМЗ);
		перем
			i, j, x: размерМЗ;
			t: T¿;
		нач
			нцДо
				i := l;
				j := r;
				x := l + (r - l) DIV 2;
				нцДо
					нцПока Less¿(m_pArray[i], m_pArray[x]) делай
						увел(i)
					кц;
					нцПока Less¿(m_pArray[x], m_pArray[j]) делай
						умень(j)
					кц;
					если i <= j то
						(* Swap i'th and j'th element *)
						t := m_pArray[i];
						m_pArray[i] := m_pArray[j];
						m_pArray[j] := t;
						если x = i то
							x := j
						аесли x = j то
							x := i
						всё;
						увел(i);
						умень(j)
					всё
				кцПри i > j;
				если l < j то
					QuickSort(l, j)
				всё;
				l := i
			кцПри i >= r
		кон QuickSort;

		проц Remove*(index: размерМЗ);
		перем
			iDst: размерМЗ;
		нач
			утв((index >= 0) и (index < m_iPos), 101);
			(* Shift content of array *)
			iDst := index;
			нцПока iDst < m_iPos - 1 делай
				m_pArray[iDst] := m_pArray[iDst + 1];
				увел(iDst)
			кц;
			умень(m_iPos)
		кон Remove;

		проц Set*(index: размерМЗ; конст t: T¿);
		нач
			утв((index >= 0) и (index < m_iPos), 101);
			m_pArray[index] := t
		кон Set;

		проц SetSorted*(bValue: булево);
		нач
			если f_bSorted = bValue то
				возврат
			всё;
			f_bSorted := bValue;
			если ~f_bSorted или (m_iPos = 0) то
				возврат
			всё;
			QuickSort(0, m_iPos - 1)
		кон SetSorted;

	кон Vector;
	
	
(* Поскольку заголовок модуля не включается в шаблон, конец модуля тоже не будем включать 
	
BEGIN
		
END GenericCollections.

*)

