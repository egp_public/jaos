модуль PCARMRegisters;	(** be  **)

использует PCM, PCOARM, ЛогЯдра;

конст
	INTERNALERROR = 100;

	CheckRegisterSize = истина;	(* temporary *)
	TraceDetail = ложь; 	(* careful ! generates A LOT of output ! *)

	Constants* = 0;		  (* constants reused *)
	MemoryStack* = 1;		(* memory access with base register FP reused *)
	MemoryAbsolute* =2; (* memory access with PC-relative addressing *)
	MemoryAll* = 3;		(* all memory access reused. WARNING: may produce false results when used together with memory mapped I/O !!! *)

	FP = PCOARM.FP;
	SP = PCOARM.SP;
	PC = PCOARM.PC;

тип
	Content* = окласс
		перем
			next: Content;

		проц Equals(c: Content): булево;
		нач возврат ложь
		кон Equals;
	кон Content;

	MemoryContent* = окласс(Content)
		перем
			baseReg-,offset-, size-: цел32;

		проц &Init*(baseReg, offset, size: цел32);
		нач сам.baseReg := baseReg; сам.offset := offset; сам.size := size
		кон Init;

		(* returns TRUE iff two MemoryContents are equal *)
		проц {перекрыта}Equals(c: Content): булево;
		нач
			если (c суть MemoryContent) то
				просейТип c: MemoryContent делай
					если bimboTrace и ((baseReg = c.baseReg) и (offset = c.offset) и (~CheckRegisterSize или (size = c.size))) то
						ЛогЯдра.пСтроку8("RegisterManager: Different size: size = "); ЛогЯдра.пЦел64(size, 0); ЛогЯдра.пСтроку8("; c.size = "); ЛогЯдра.пЦел64(c.size, 0); ЛогЯдра.пВК_ПС;
					всё;
					возврат (baseReg = c.baseReg) и (offset = c.offset) и (~CheckRegisterSize или (size = c.size))
				всё
			иначе
				возврат ложь
			всё
		кон Equals;

		(* returns TRUE iff two MemoryContents overlapp *)
		проц Overlapps(c: Content): булево;
		нач
			если (c суть MemoryContent) то
				просейТип c: MemoryContent делай
					возврат (baseReg = c.baseReg) и ((offset+size >  c.offset) и (c.offset+c.size > offset))
				всё
			иначе
				возврат ложь
			всё
		кон Overlapps;
	кон MemoryContent;

	PCRelMemContent* = окласс(MemoryContent)
		перем
			pc-: цел32;

		проц &{перекрыта}Init*(pc, offset, size: цел32);
		нач Init^(PC, offset, size); сам.pc := pc
		кон Init;

		проц {перекрыта}Equals(c: Content): булево;
		нач
			если (c суть PCRelMemContent) то
				просейТип c: PCRelMemContent делай
					если bimboTrace и (pc+offset = c.pc+c.offset) и (~CheckRegisterSize или (size = c.size)) то
						ЛогЯдра.пСтроку8("RegisterManager: Different size: size = "); ЛогЯдра.пЦел64(size, 0); ЛогЯдра.пСтроку8("; c.size = "); ЛогЯдра.пЦел64(c.size, 0); ЛогЯдра.пВК_ПС;
					всё;
					возврат (pc+offset = c.pc+c.offset) и (~CheckRegisterSize или (size = c.size))
				всё
			иначе
				возврат ложь
			всё
		кон Equals;
	кон PCRelMemContent;

	Address* = окласс(Content)
		перем
			adr: PCM.Attribute;
			offset: цел32;

		проц &Init*(adr: PCM.Attribute; offset: цел32);
		нач сам.adr := adr; сам.offset := offset
		кон Init;

		проц {перекрыта}Equals(c: Content): булево;
		перем b: булево;
		нач
			если TraceDetail то
				ЛогЯдра.пСтроку8("Address.Equals: ");
				b := (c суть Address);
				если ~b то ЛогЯдра.пСтроку8("no adr")
				иначе
					b := (adr = c(Address).adr);
						если ~b то ЛогЯдра.пСтроку8("not same adr")
					иначе
						b := (offset = c(Address).offset);
						если ~b то ЛогЯдра.пСтроку8("not same offset")
						иначе ЛогЯдра.пСтроку8("equal!")
						всё
					всё
				всё;
				ЛогЯдра.пВК_ПС
			всё;

			возврат (c суть Address) и (adr = c(Address).adr) и (offset = c(Address).offset)
		кон Equals;
	кон Address;

	ConstantContent* = окласс(Content)
	кон ConstantContent;

	IntConstant* = окласс(ConstantContent)
		перем
			v-: цел32;

		проц &Init*(value: цел32);
		нач v := value
		кон Init;

		проц {перекрыта}Equals(c: Content): булево;
		нач
			возврат (c суть IntConstant) и (v = c(IntConstant).v)
		кон Equals;
	кон IntConstant;

	RealConstant* = окласс(ConstantContent)
		перем
			v-: вещ32;

		проц &Init*(value: вещ32);
		нач v := value
		кон Init;

		проц {перекрыта}Equals(c: Content): булево;
		нач
			возврат (c суть RealConstant) и (v = c(RealConstant).v)
		кон Equals;
	кон RealConstant;

	LongRealConstant* = окласс(ConstantContent)
		перем
			v-: вещ64;

		проц &Init*(value: вещ64);
		нач v := value
		кон Init;

		проц {перекрыта}Equals(c: Content): булево;
		нач
			возврат (c суть LongRealConstant) и (v = c(LongRealConstant).v)
		кон Equals;
	кон LongRealConstant;

	Register* = окласс
		перем
			id-: цел32; (** register number *)
			free-: цел32;
			value: Content;
			memory: Content;
			prevLRU, nextLRU: Register;

		проц &Init*(ID: цел32);
		нач id := ID
		кон Init;

		(* AddContent - add a content for this register. ConstantContents are replaced, MemoryContents added to a list *)
		проц AddContent(c: Content);
		перем m: Content;
		нач
			если (c # НУЛЬ) то
				если (c суть ConstantContent) то
					если TraceDetail то
						ЛогЯдра.пСимв8("R"); ЛогЯдра.пЦел64(id, 0); ЛогЯдра.пСтроку8(": adding constant content... ");
					всё;
					value := c; memory := НУЛЬ
				иначе
					если TraceDetail то
						ЛогЯдра.пСимв8("R"); ЛогЯдра.пЦел64(id, 0); ЛогЯдра.пСтроку8(": adding memory content...");
					всё;
					c.next := memory; memory := c
				всё;
				если TraceDetail то
					если (value # НУЛЬ) и (value суть IntConstant) то ЛогЯдра.пЦел64(value(IntConstant).v, 0); ЛогЯдра.пСтроку8(", ") всё;
					m := memory;
					нцПока (m # НУЛЬ) делай
						если (m суть MemoryContent) то ЛогЯдра.пСимв8("["); ЛогЯдра.пЦел64(m(MemoryContent).baseReg, 0); ЛогЯдра.пСимв8(",");
							ЛогЯдра.пЦел64(m(MemoryContent).offset, 0); ЛогЯдра.пСтроку8("], ")
						иначе ЛогЯдра.пСтроку8("huga?, ")
						всё;
						m := m.next
					кц;
					ЛогЯдра.пВК_ПС
				всё
			всё
		кон AddContent;

		(* RemoveContent - removes a content from this register.
			If the parameter is a memory content, the list is searched for this content. If it is found, it's deleted from the list. If the content
			is not found in the list, the whole memory content is cleared. If r = NIL, all contents are cleared
		*)
		проц RemoveContent(r: Content);
		перем p,c: Content;
		нач
			если (r = НУЛЬ) то
				value := НУЛЬ; memory := НУЛЬ;
				если TraceDetail то
					ЛогЯдра.пСимв8("R"); ЛогЯдра.пЦел64(id, 0); ЛогЯдра.пСтроку8(": content cleared"); ЛогЯдра.пВК_ПС
				всё
			аесли (r суть ConstantContent) то (* nothing *)
			иначе
				p := НУЛЬ; c := memory;
				нцПока (c # НУЛЬ) и (c суть MemoryContent) и ~c(MemoryContent).Overlapps(r) делай p := c; c := c.next кц;
				если (c # НУЛЬ) то
					если TraceDetail то
						ЛогЯдра.пСимв8("R"); ЛогЯдра.пЦел64(id, 0); ЛогЯдра.пСтроку8(": memory content removed"); ЛогЯдра.пВК_ПС
					всё;
					если (p = НУЛЬ) то memory := c.next
					иначе p.next := c.next
					всё
				всё
			всё
		кон RemoveContent;

		(* Equals - *)
		проц Equals(c: Content): булево;
		перем m: Content;
		нач
			если (c # НУЛЬ) то
				если (c суть ConstantContent) и (value # НУЛЬ) то
					возврат value.Equals(c)
				аесли (memory # НУЛЬ) то
					m := memory;
					нцПока (m # НУЛЬ) и ~m.Equals(c) делай m := m.next кц;
					возврат m # НУЛЬ
				всё
			всё;
			возврат ложь
		кон Equals;
	кон Register;

	ARMRegisters* = окласс
		перем
			registers: укль на массив из Register;
			nofRegs: цел32;
			reuse: мнвоНаБитахМЗ;
			lru: Register;

		проц &Init*(nofRegs: цел32; reuseFlags: мнвоНаБитахМЗ);
		перем i: цел32;
		нач
			утв(nofRegs < матМаксимум(мнвоНаБитахМЗ));
			нов(registers, nofRegs); сам.nofRegs := nofRegs;
			нцДля i := 0 до nofRegs-1 делай нов(registers[i], i) кц;
			нцДля i := 0 до nofRegs-1 делай
				registers[i].prevLRU := registers[(nofRegs+i-1) остОтДеленияНа nofRegs];
				registers[i].nextLRU := registers[(i+1) остОтДеленияНа nofRegs]
			кц;
			lru := registers[0];
			reuse := reuseFlags
		кон Init;

		проц ReuseType(c: Content): булево;
		нач
			возврат (c # НУЛЬ) и
				((((c суть ConstantContent) и (Constants в reuse)) или
				((c суть MemoryContent) и
					 ((MemoryAll в reuse) или
					((MemoryStack в reuse) и (c(MemoryContent).baseReg = FP))))) или
				((c суть Address) и (MemoryAbsolute в reuse)))
		кон ReuseType;

		проц AllocDestReg*(useCount: цел32): цел32;
		перем dummy: булево;
		нач возврат AllocReg(НУЛЬ, dummy, useCount)
		кон AllocDestReg;

		проц AllocReg*(content: Content; перем contentValid: булево; useCount: цел32): цел32;
		перем reg, i: цел32; r: Register;
		нач
			contentValid := ложь;
			reg := -1;
			если ReuseType(content) то (* try to reuse register *)
				нцПока (i < nofRegs) и ~contentValid делай
					если (registers[i].free =  0) то
						если (registers[i].Equals(content)) то
							reg := i; contentValid := истина
						всё
					всё;
					увел(i)
				кц
			всё;
			если (reg = -1) то
				r := lru;
				нцПока (r # lru.prevLRU) и (r.free > 0) делай r := r.nextLRU кц;
				если (r.free = 0) то reg := r.id
				иначе (* not enough registers *)
					PCM.Error(215, PCM.InvalidPosition, "Not enough registers.");
					СТОП(матМаксимум(цел16));
					СТОП(INTERNALERROR)
				всё;
				если (content # НУЛЬ) то Invalidate(content); r.AddContent(content)
				иначе r.RemoveContent(НУЛЬ)
				всё
			всё;
			InAllocReg(reg, useCount);
			возврат reg
		кон AllocReg;

		проц AllocSpecialReg*(reg: цел32; content: Content; useCount: цел32);
		нач {единолично}
			утв((0 <= reg) и (reg < nofRegs));
			если (registers[reg].free # 0) то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("ERROR in AllocSpecialReg: register is not free (use count: ");
				ЛогЯдра.пЦел64(registers[reg].free, 0); ЛогЯдра.пСимв8(")");
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё;
			если (content # НУЛЬ) то Invalidate(content); registers[reg].AddContent(content)
			иначе registers[reg].RemoveContent(НУЛЬ)
			всё;
			InAllocReg(reg, useCount)
		кон AllocSpecialReg;

		проц InAllocReg(reg, useCount: цел32);
		перем r: Register;
		нач
			утв(registers[reg].free = 0);
			r := registers[reg];
			r.free := useCount;
			r.prevLRU.nextLRU := r.nextLRU; r.nextLRU.prevLRU := r.prevLRU;
			если (lru = r) то lru := r.nextLRU всё;
			r.prevLRU := lru.prevLRU; lru.prevLRU.nextLRU := r;
			r.nextLRU := lru; lru.prevLRU := r
		кон InAllocReg;

		проц FixRegisterUse*(reg, deltaUse: цел32);
		нач {единолично}
			утв((0 <= reg) и (reg < nofRegs) и (registers[reg].free + deltaUse >= 0));
			увел(registers[reg].free, deltaUse)
		кон FixRegisterUse;

		проц FreeReg*(reg: цел32);
		нач {единолично}
			если ~SpecialReg(reg) то
				утв((0 <= reg) и (reg < nofRegs) и (registers[reg].free > 0));
				умень(registers[reg].free)
			всё
		кон FreeReg;

		проц FreeAll*;
		перем i: цел32;
		нач {единолично}
			нцДля i := 0 до nofRegs-1 делай registers[i].free := 0 кц
		кон FreeAll;

		проц SetRegisterContent*(reg: цел32; content: Content);
		перем r: Register;
		нач { единолично }
			если (0 <= reg) и (reg < nofRegs) то	(* ignore invalid registerers (CG may want to set the register content of the SP register) *)
				r := registers[reg];
				r.RemoveContent(НУЛЬ);
				если (content # НУЛЬ) то r.AddContent(content) всё;
				r.prevLRU.nextLRU := r.nextLRU; r.nextLRU.prevLRU := r.prevLRU;
				если (lru = r) то lru := r.nextLRU всё;
				r.prevLRU := lru.prevLRU; lru.prevLRU.nextLRU := r;
				r.nextLRU := lru; lru.prevLRU := r
			всё
		кон SetRegisterContent;

		проц AddRegisterContent*(reg: цел32; content: Content);
		перем r: Register;
		нач { единолично }
			если (content # НУЛЬ) то
				утв((0 <= reg) и (reg < nofRegs));
				r := registers[reg];
				Invalidate(content); r.AddContent(content);
				r.prevLRU.nextLRU := r.nextLRU; r.nextLRU.prevLRU := r.prevLRU;
				если (lru = r) то lru := r.nextLRU всё;
				r.prevLRU := lru.prevLRU; lru.prevLRU.nextLRU := r;
				r.nextLRU := lru; lru.prevLRU := r
			всё
		кон AddRegisterContent;

		проц Invalidate*(content: Content);
		перем i: цел32;
		нач
			если (content # НУЛЬ) и (content суть MemoryContent) то
				нцДля i := 0 до nofRegs-1 делай
					registers[i].RemoveContent(content)
				кц
			всё
		кон Invalidate;

		проц InvalidateAll*;
		перем i: цел32;
		нач
			нцДля i := 0 до nofRegs-1 делай registers[i].RemoveContent(НУЛЬ) кц
		кон InvalidateAll;

		проц GetReg*(reg: цел32): Register;
		нач
			утв((0 <= reg) и (reg < nofRegs));
			возврат registers[reg]
		кон GetReg;

		проц GetUsedRegisterSet*(): мнвоНаБитахМЗ;
		перем r: мнвоНаБитахМЗ; i: цел32;
		нач
			нцДля i := 0 до nofRegs-1 делай
				если (registers[i].free > 0) то включиВоМнвоНаБитах(r, i) всё
			кц;
			возврат r
		кон GetUsedRegisterSet;

		проц IsRegisterFree*(reg: цел32): булево;
		нач
			утв((0 <= reg) и (reg < nofRegs));
			возврат registers[reg].free = 0
		кон IsRegisterFree;

		проц GetRegisterUseCount*(reg: цел32): цел32;
		нач
			утв((0 <= reg) и (reg < nofRegs));
			возврат registers[reg].free
		кон GetRegisterUseCount;

	кон ARMRegisters;

перем bimboTrace*: булево;

проц SpecialReg(r: цел32): булево;
нач возврат (r = PCOARM.SP) или (r = PCOARM.FP) или (r = PCOARM.LR) или (r = PCOARM.PC)
кон SpecialReg;

проц NewMemContent*(pc, rBase, offset, size: цел32): MemoryContent;
перем c: MemoryContent;
нач
	если (size = 0) то СТОП(матМаксимум(цел16)) всё;
	если (rBase = PC) то возврат NewPCRelMemContent(pc, offset, size)
	иначе нов(c, rBase, offset, size); возврат c
	всё
кон NewMemContent;

проц NewPCRelMemContent*(pc, offset, size: цел32): PCRelMemContent;
перем c: PCRelMemContent;
нач 	если (size = 0) то СТОП(матМаксимум(цел16)) всё;
нов(c, pc, offset, size); возврат c
кон NewPCRelMemContent;

проц NewMemAddress*(adr: PCM.Attribute; offset: цел32): Address;
перем c: Address;
нач нов(c, adr, offset); возврат c
кон NewMemAddress;

проц NewIntConst*(v: цел32): IntConstant;
перем c: IntConstant;
нач нов(c, v); возврат c
кон NewIntConst;

проц NewRealConst*(v: вещ32): RealConstant;
перем c: RealConstant;
нач нов(c, v); возврат c
кон NewRealConst;

проц NewLongRealConst*(v: вещ64): LongRealConstant;
перем c: LongRealConstant;
нач нов(c, v); возврат c
кон NewLongRealConst;


кон PCARMRegisters.
