модуль WMPerfMonPluginExample; (** AUTHOR "staubesv"; PURPOSE "Performance Monitor plugin example"; *)
(**
 * History:
 *
 *	27.02.2007	First release (staubesv)
 *)

использует
	WMPerfMonPlugins, Modules, Commands;

конст
	ModuleName = "WMPerfMonPluginExample";

тип

	Example= окласс(WMPerfMonPlugins.Plugin)

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		перем ds : WMPerfMonPlugins.DatasetDescriptor;
		нач
			p.name := "Example"; p.description := "Performance Monitor plugin example";
			p.modulename := ModuleName;
			p.autoMin := ложь; p.autoMax := истина; p.minDigits := 7;

			нов(ds, 3);
			ds[0].name := "Nvalue1";
			ds[1].name := "Nvalue2";
			ds[2].name := "Nvalue3";
			p.datasetDescriptor := ds;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		нач
			dataset[0] := Nvalue1;
			dataset[1] := Nvalue2;
			dataset[2] := Nvalue3;
		кон UpdateDataset;

	кон Example;

перем
	Nvalue1, Nvalue2, Nvalue3 : цел32;

проц SetValues*(context : Commands.Context); (** Nvalue1 Nvalue2 Nvalue3 ~ *)
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(Nvalue1, ложь);
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(Nvalue2, ложь);
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(Nvalue3, ложь);
кон SetValues;

проц Install*; (** ~ *)
кон Install;

проц InitPlugin;
перем par : WMPerfMonPlugins.Parameter; plugin : Example;
нач
	нов(par); нов(plugin, par);
кон InitPlugin;

проц Cleanup;
нач
	WMPerfMonPlugins.updater.RemoveByModuleName(ModuleName);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	InitPlugin;
кон WMPerfMonPluginExample.

WMPerfMonPluginExample.Install ~   System.Free WMPerfMonPluginExample ~

WMPerfMonPluginExample.SetValues 112 1 9 ~

WMPerfMonPluginExample.SetValues 10 1 1 ~

WMPerfMonPluginExample.SetValues 1 1 1 ~

WMPerfMonPluginExample.SetValues -1 -11 1 ~

WMPerfMonPluginExample.SetValues 1 1 100 ~
