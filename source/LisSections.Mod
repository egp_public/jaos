модуль LisSections; (** AUTHOR "fof"; PURPOSE "support for code sections and references"; *)

использует SyntaxTree := LisSyntaxTree,Потоки,Global := LisGlobal,Formats := LisFormats, Basic := FoxBasic, Strings, ObjectFile;

конст
	(* section categories *)
	EntryCodeSection*=ObjectFile.EntryCode;
	ExitCodeSection*=ObjectFile.ExitCode;
	InitCodeSection*=ObjectFile.InitCode;
	BodyCodeSection*=ObjectFile.BodyCode;
	CodeSection*=ObjectFile.Code;
	VarSection*=ObjectFile.Data;
	ConstSection*=ObjectFile.Const;
	InlineCodeSection*=10;
	UnknownSectionType*= 11;

	LineCommentStart*="; ";

	(* gensam *)
	UnknownSize* = -1;
	UndefinedFinalPosition* = -1;

тип
	Identifier*=ObjectFile.Identifier;
	SectionName*= ObjectFile.SegmentedName;

	Section*=окласс
	перем
		name-: SectionName; (* name of this section (globally unique-name derived from symbol name) *)
		type-: цел8; (* CodeSection, InlineCodeSection, ...  *)

		fixed-: булево; (* whether the position of the section is fixed, as opposed to being restricted by an alignment *)
		positionOrAlignment-: размерМЗ; (* the alignment OR the position *)
		fingerprint-: Basic.Fingerprint; (* fingerprint of the corresponding syntax tree node *)
		bitsPerUnit-: цел32; (* the unit size given in bits *)

		symbol-: SyntaxTree.ОбъявлениеИменованнойСущности; (* corresponding symbol in AST *)
		offset-: размерМЗ;

		(* for linking *)
		isReachable-: булево;

		проц & InitSection*(l0type: цел8; конст n: ObjectFile.SegmentedName; l1symbol: SyntaxTree.ОбъявлениеИменованнойСущности);
		нач
			name := n;
			сам.symbol := l1symbol;
			сам.type := l0type;
			offset := 0;
			fixed := ложь;
			positionOrAlignment := 1;
			fingerprint := 0;
			bitsPerUnit := UnknownSize;
		кон InitSection;

		проц IsCode*(): булево;
		нач
			возврат type в {EntryCodeSection .. CodeSection};
		кон IsCode;

		проц SetOffset*(l2offset: размерМЗ);
		нач сам.offset := l2offset;
		кон SetOffset;

		проц SetReachability*(l3isReachable: булево);
		нач сам.isReachable := l3isReachable
		кон SetReachability;

		проц SetBitsPerUnit*(l4bitsPerUnit: цел32);
		нач сам.bitsPerUnit := l4bitsPerUnit
		кон SetBitsPerUnit;

		проц IsAligned*(): булево;
		нач возврат ~fixed и (positionOrAlignment > 1)
		кон IsAligned;

		проц SetPositionOrAlignment*(isFixed: булево; l5positionOrAlignment: размерМЗ);
		нач
			сам.fixed := isFixed;
			сам.positionOrAlignment := l5positionOrAlignment
		кон SetPositionOrAlignment;

		проц GetSize*(): цел32;
		нач возврат UnknownSize
		кон GetSize;

		проц SetFingerprint*(l6fingerprint: Basic.Fingerprint);
		нач сам.fingerprint := l6fingerprint
		кон SetFingerprint;

		(** change the type of a section **)
		проц SetType*(l7type: цел8);
		нач сам.type := l7type
		кон SetType;

		проц Dump*(w: Потоки.Писарь);
		нач
			w.пСтроку8(".");
			просей type из
			| EntryCodeSection: w.пСтроку8("entrycode")
			| ExitCodeSection: w.пСтроку8("exitcode")
			| CodeSection: w.пСтроку8("code")
			| BodyCodeSection: w.пСтроку8("bodycode")
			| InlineCodeSection: w.пСтроку8("inlinecode")
			| VarSection: w.пСтроку8("var");
			| ConstSection: w.пСтроку8("const");
			| InitCodeSection: w.пСтроку8("initcode");
			иначе
				w.пСтроку8("UNDEFINED")
			всё;
			w.пСтроку8(" ");
			DumpName(w);

			(* positional restrictions *)
			если fixed то
				w.пСтроку8(" fixed="); w.пЦел64(positionOrAlignment, 0)
			аесли positionOrAlignment > 1 то
				w.пСтроку8(" aligned="); w.пЦел64(positionOrAlignment, 0)
			всё;

			если fingerprint # 0 то w.пСтроку8(" fingerprint="); w.п16ричное(fingerprint, 0) всё;

			если bitsPerUnit # UnknownSize то w.пСтроку8(" unit="); w.пЦел64(bitsPerUnit, 0) всё;

			(* note: this information is actually redundant *)
			если GetSize() # UnknownSize то w.пСтроку8(" size="); w.пЦел64(GetSize(), 0) всё;

			w.ПротолкниБуферВПоток
		кон Dump;

		проц WriteRaw*(w: Потоки.Писарь);
		нач
			w.пЦел16_мз(type);
			Basic.WriteSegmentedName(w,name);
			w.пБулево_мз(fixed);
			w.пЦел32_мз(positionOrAlignment(цел32));
			w.пЦел64_мз(fingerprint);
			w.пЦел32_мз(bitsPerUnit);
		кон WriteRaw;

		проц DumpName*(w: Потоки.Писарь);
		нач
			Basic.WriteSegmentedName(w,name);
		кон DumpName;

	кон Section;

	CommentStr* = укль на массив из симв8;
	Comment* = окласс
		перем str-: CommentStr; strLen: размерМЗ; pos-: размерМЗ; nextComment-: Comment;

		проц &Init*(l8pos: размерМЗ);
		нач
			сам.pos := l8pos;
			нов(str,32); strLen := 0;
			str[0] := 0X;
		кон Init;

		проц Append(конст buf: массив из симв8;  ofs, len: размерМЗ);

			проц Resize(newLen: размерМЗ);
			перем new: CommentStr; i: размерМЗ;
			нач
				нов(new,newLen);
				нцДля i := 0 до strLen-1 делай
					new[i] := str[i]
				кц;
				str := new
			кон Resize;

		нач
			увел(len,ofs);
			утв(длинаМассива(buf) >= len);
			нцПока (ofs < len) и (buf[ofs] # 0X) делай
				если длинаМассива(str) <= strLen то Resize(2*strLen) всё;
				str[strLen] := buf[ofs];
				увел(ofs); увел(strLen);
			кц;
			если длинаМассива(str) <= strLen то Resize(2*strLen) всё;
			str[strLen] := 0X;
		кон Append;

		проц Dump*(w: Потоки.Писарь);
		перем i: размерМЗ; ch: симв8; newln: булево;
		нач
			если w суть Basic.Writer то w(Basic.Writer).BeginComment; w(Basic.Writer).IncIndent; всё;
			w.пСтроку8("; ");
			i := 0; ch := str[i]; newln := ложь;
			нцПока(ch#0X) делай
				если (ch = 0DX) или (ch = 0AX) то newln := истина
				иначе
					если newln то w.пВК_ПС; w.пСтроку8(LineCommentStart); newln := ложь;  всё;
					w.пСимв8(ch);
				всё;
				увел(i); ch := str[i];
			кц;
			если w суть Basic.Writer то w(Basic.Writer).EndComment; w(Basic.Writer).DecIndent;всё;
			(*w.Update;*)
		кон Dump;

	кон Comment;

	GetPCProcedure=проц{делегат}(): размерМЗ;

	CommentWriter*= окласс (Потоки.Писарь)
	перем
		firstComment-,lastComment-: Comment; comments-: цел32;
		getPC: GetPCProcedure;

		проц AppendToLine*( конст buf: массив из симв8;  ofs, len: размерМЗ; propagate: булево;  перем res: целМЗ );
		перем pos: размерМЗ;
		нач
			если len = 0 то возврат всё;
			pos := getPC();
			если lastComment = НУЛЬ то
				нов(lastComment,pos); firstComment := lastComment;
			аесли (lastComment.pos # pos) то
				нов(lastComment.nextComment,pos);
				lastComment := lastComment.nextComment;
			всё;
			lastComment.Append(buf,ofs,len)
		кон AppendToLine;

		проц {перекрыта}пВК_ПС*;
		нач
			пВК_ПС^;
			(*Update;*)
		кон пВК_ПС;

		проц {перекрыта}Сбрось*;
		нач
			firstComment := НУЛЬ; lastComment := НУЛЬ; comments := 0;
			Сбрось^;
		кон Сбрось;

		проц & InitCommentWriter*(l9getPC: GetPCProcedure);
		нач
			сам.getPC := l9getPC;
			новПисарь(AppendToLine,256);
			firstComment := НУЛЬ; lastComment := НУЛЬ; comments := 0;
		кон InitCommentWriter;

	кон CommentWriter;

	SectionLookup = окласс(Basic.HashTable); (* SyntaxTree.Symbol _> Symbol *)
	перем

		проц GetSection(symbol: SyntaxTree.ОбъявлениеИменованнойСущности):Section;
		перем p: динамическиТипизированныйУкль;
		нач
			p := Get(symbol);
			если p # НУЛЬ то
				утв(p(Section).symbol = symbol);
				возврат p(Section);
			иначе
				возврат НУЛЬ
			всё;
		кон GetSection;

		проц PutSection(symbol: SyntaxTree.ОбъявлениеИменованнойСущности; section: Section);
		нач
			Put(symbol,section);
		кон PutSection;

	кон SectionLookup;

	SectionNameLookup = окласс(Basic.HashTableSegmentedName); (* SyntaxTree.Symbol _> Symbol *)

		проц GetSection(конст name: Basic.SegmentedName):Section;
		перем p: динамическиТипизированныйУкль;
		нач
			p := Get(name);
			если p # НУЛЬ то возврат p(Section) иначе возврат НУЛЬ всё;
		кон GetSection;

		проц PutSection(конст name:Basic.SegmentedName; section: Section);
		нач
			Put(name, section);
		кон PutSection;

	кон SectionNameLookup;

	(** a list of sections
	note: a section may be part of multiple lists in this implementation
	**)
	SectionList* = окласс(Basic.List)
	перем
		lookup: SectionLookup;
		lookupName: SectionNameLookup;

		проц & InitListOfSections*;
		нач
			нов(lookup, 128);
			нов(lookupName, 128);
			InitList(128) (* initializer of general list *)
		кон InitListOfSections;

		проц GetSection*(index: размерМЗ): Section;
		перем
			any: динамическиТипизированныйУкль;
		нач
			any := Get(index);
			возврат any(Section)
		кон GetSection;

		проц SetSection*(index: размерМЗ; section: Section);
		нач
			Set(index, section)
		кон SetSection;

		(* note: this procedure cannot be called "Add" as it was the case in the old section list implementation *)
		проц AddSection*(section: Section);
		нач
			(* assert that the section is not already present *)
			утв((FindBySymbol(section.symbol) = НУЛЬ) и (FindByName(section.name) = НУЛЬ));

			если section.symbol # НУЛЬ то (* special case, may not be added to lookup list *)
				lookup.PutSection(section.symbol, section)
			всё;
			если section.name[0] >= 0 то
				lookupName.PutSection(section.name, section);
			всё;
			Add(section)
		кон AddSection;

		(** finds a section with a certain AST symbol **)
		проц FindBySymbol*(конст symbol: SyntaxTree.ОбъявлениеИменованнойСущности): Section;
		нач
			если symbol = НУЛЬ то
				возврат НУЛЬ
			иначе
				возврат lookup.GetSection(symbol)
			всё
		кон FindBySymbol;

		(** finds a section with a certain name **)
		проц FindByName*(конст name: Basic.SegmentedName): Section;
		нач
			возврат lookupName.GetSection(name)
		кон FindByName;

		проц Dump*(w: Потоки.Писарь);
		перем
			i: размерМЗ;
			section: Section;
		нач
			нцДля i := 0 до Length() - 1 делай
				section := GetSection(i);
				section.Dump(w); w.пВК_ПС
			кц;
		кон Dump;

		проц WriteRaw*(w: Потоки.Писарь);
		перем
			i: размерМЗ;
			section: Section;
		нач
			нцДля i := 0 до Length() - 1 делай
				section := GetSection(i);
				section.WriteRaw(w);
			кц;
		кон WriteRaw;

	кон SectionList;

	NameEntry = укль на запись
		name: SyntaxTree.СтрокаИдентификатора;
	кон;

	(* TODO: efficient implementation using hash table *)
	NameList* = окласс(Basic.List)
		проц AddName*(конст moduleName: массив из симв8);
		перем entry: NameEntry;
		нач
			нов(entry);
			копируйСтрокуДо0(moduleName, entry.name);
			Add(entry)
		кон AddName;

		проц GetName*(index: размерМЗ): SyntaxTree.СтрокаИдентификатора;
		перем any: динамическиТипизированныйУкль;
		нач
			any := Get(index);
			утв(any суть NameEntry);
			возврат any(NameEntry).name
		кон GetName;

		проц ContainsName*(name: SyntaxTree.СтрокаИдентификатора): булево;
		перем i: размерМЗ;
		нач
			нцДля i := 0 до Length() - 1 делай
				если name = GetName(i) то возврат истина всё
			кц;
			возврат ложь
		кон ContainsName;
	кон NameList;

	(** output of (intermediate) code generation **)
	Module* = окласс (Formats.GeneratedModule)
	перем
		allSections-: SectionList;
		importedSections-: SectionList; (* necessary for binary object file format, for reference to symbol *)

		platformName-: SyntaxTree.СтрокаИдентификатора;
		imports-: NameList;
		exports-: NameList;

		проц & {перекрыта}Init*(l10module: SyntaxTree.ДеревоМодуля; l11system: Global.System);
		нач
			Init^(l10module,l11system);
			нов(allSections);
			нов(importedSections);
			нов(imports, 128);
			нов(exports, 128);
		кон Init;

		(*
		PROCEDURE SetSections*(sections: SectionList);
		BEGIN SELF.allSections := sections
		END SetSections;
		*)

		проц SetImports*(l12imports: NameList);
		нач сам.imports := l12imports
		кон SetImports;

		проц SetPlatformName*(конст l13platformName: массив из симв8);
		нач копируйСтрокуДо0(l13platformName, сам.platformName)
		кон SetPlatformName;

		проц {перекрыта}Dump*(w: Потоки.Писарь);
		перем
			dump: Basic.Writer;
			name: SyntaxTree.СтрокаИдентификатора;
			i: размерМЗ;
		нач
			dump := Basic.GetWriter(w);

			(* dump module directive *)
			dump.пСтроку8(".module ");
			dump.пСтроку8(moduleName); dump.пВК_ПС;
			dump.пВК_ПС;

			(* dump platform directive *)
			если platformName # "" то
				dump.пСтроку8(".platform ");
				dump.пСтроку8(platformName); dump.пВК_ПС;
				dump.пВК_ПС
			всё;

			(* dump imports directive *)
			если imports.Length() > 0 то
				dump.пСтроку8(".imports ");
				нцДля i := 0 до imports.Length() - 1 делай
					если i # 0 то dump.пСтроку8(", ") всё;
					name := imports.GetName(i);
					если name = "" то
						dump.пСтроку8("<import failed>")
					иначе
						dump.пСтроку8(name)
					всё
				кц;
				dump.пВК_ПС; dump.пВК_ПС
			всё;

			(* dump imports directive *)
			если exports.Length() > 0 то
				dump.пСтроку8(".exports ");
				нцДля i := 0 до exports.Length() - 1 делай
					если i # 0 то dump.пСтроку8(", ") всё;
					name := exports.GetName(i);
					если name = "" то
						dump.пСтроку8("<export failed>")
					иначе
						dump.пСтроку8(name)
					всё
				кц;
				dump.пВК_ПС; dump.пВК_ПС
			всё;


			(* dump all sections *)
			allSections.Dump(w)
		кон Dump;

	кон Module;

	проц DumpFiltered*(w: Потоки.Писарь; module: Module; конст filter: массив из симв8);
	перем
		i: размерМЗ;
		section: Section;
		name: ObjectFile.SectionName;
	нач
		нцДля i := 0 до module.allSections.Length() - 1 делай
			section := module.allSections.GetSection(i);
			ObjectFile.SegmentedNameToString(section.name,name);
			если Strings.Match(filter, name) то section.Dump(w); w.пВК_ПС; всё
		кц
	кон DumpFiltered;

	проц NewCommentWriter*(getPC: GetPCProcedure): CommentWriter;
	перем c: CommentWriter;
	нач
		нов(c,getPC); возврат c
	кон NewCommentWriter;

кон LisSections.
