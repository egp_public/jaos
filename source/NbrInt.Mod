(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль NbrInt;   (** AUTHOR "adf"; PURPOSE "Defines a base Integer type for scientific computing."; *)

использует Потоки, NbrInt8, NbrInt16, NbrInt32;

(** This module hides the type size of the integer implemented.  This makes it a straightforward process for
	the user to change this one module, and in doing so, change the type size for all integers without having
	to change any modules that import NbrInt, at least in principle.  That was one of our design goals. *)

тип
	Integer* = NbrInt32.Integer;

перем
	MinNbr-, MaxNbr-: Integer;

	(** Basic Functions*)
	проц Abs*( i: Integer ): Integer;   (** Except for Abs( MinNbr ) = MinNbr, because -MinNbr is out of range. *)
	нач
		возврат NbrInt32.Abs( i )
	кон Abs;

	проц Dec*( перем i: Integer );   (** Except for Dec( MinNbr ), because MinNbr-1 is out of range. *)
	нач
		NbrInt32.Dec( i )
	кон Dec;

	проц Inc*( перем i: Integer );   (** Except for Inc( MaxNbr ), because MaxNbr+1 is out of range. *)
	нач
		NbrInt32.Inc( i )
	кон Inc;

	проц Odd*( i: Integer ): булево;
	нач
		возврат NbrInt32.Odd( i )
	кон Odd;

	проц Max*( x1, x2: Integer ): Integer;
	нач
		возврат NbrInt32.Max( x1, x2 )
	кон Max;

	проц Min*( x1, x2: Integer ): Integer;
	нач
		возврат NbrInt32.Min( x1, x2 )
	кон Min;

	проц Sign*( x: Integer ): Integer;
	перем sign: NbrInt8.Integer;
	нач
		sign := NbrInt32.Sign( x );  возврат NbrInt32.Long( NbrInt16.Long( sign ) )
	кон Sign;

(** String conversions.  LEN(string) >= 15 *)
	проц StringToInt*( string: массив из симв8;  перем x: Integer );
	нач
		NbrInt32.StringToInt( string, x )
	кон StringToInt;

	проц IntToString*( x: Integer;  перем string: массив из симв8 );
	нач
		NbrInt32.IntToString( x, string )
	кон IntToString;

(** Persistence: file IO *)
	проц Load*( R: Потоки.Чтец;  перем x: Integer );
	нач
		NbrInt32.Load( R, x )
	кон Load;

	проц Store*( W: Потоки.Писарь;  x: Integer );
	нач
		NbrInt32.Store( W, x )
	кон Store;

нач
	MinNbr := NbrInt32.MinNbr;  MaxNbr := NbrInt32.MaxNbr
кон NbrInt.
