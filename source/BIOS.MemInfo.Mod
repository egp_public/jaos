(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль MemInfo; (** AUTHOR "pjm"; PURPOSE "Memory mapping information"; *)

использует НИЗКОУР, ЛогЯдра, ЭВМ;

конст
	SoftInt = ЭВМ.SoftInt;

		(* standard multipliers *)
	K = 1024;  M = 100000H;	(* 1K, 1M *)

	PS = 4096;
	RS = 4*M;	(* region covered by a page table in bytes *)
	PTEs = RS DIV PS;	(* number of page table/directory entries *)

перем
	kernelPD: адресВПамяти;
	msrlow, msrhigh: мнвоНаБитахМЗ;

проц GetCR3(перем state: ЭВМ.СостояниеПроцессора);
машКод
#если I386 то
	MOV EAX, CR3
	MOV [kernelPD], EAX
#аесли AMD64 то
	MOV RAX, CR3
	MOV RBX, kernelPD
	MOV [RBX], RAX
#иначе
	unimplemented
#кон
кон GetCR3;

проц -DoSoftInt(eax: цел32);
машКод
#если I386 то
	POP EAX
	INT SoftInt
#аесли AMD64 то
	POP RAX
	INT SoftInt
#иначе
	unimplemented
#кон
кон DoSoftInt;

(* Display mapped ranges. *)

проц DisplayMap*;
перем i, j: цел32; pt: цел32; virt, phys, virt0, phys0, size: цел32;

	проц Page;
	нач
		если (phys = phys0+size) и (virt = virt0+size) то
			увел(size, PS)
		иначе
			если size # 0 то
				ЛогЯдра.п16ричное(virt0, 9); ЛогЯдра.п16ричное(phys0, 9);
				ЛогЯдра.пОбъёмПамятиСРазмерностью(size, 8, "B"); ЛогЯдра.пВК_ПС
			всё;
			virt0 := virt;  phys0 := phys;  size := PS
		всё
	кон Page;

нач
	ЭВМ.InstallHandler(GetCR3, SoftInt);	(* ignore race *)
	DoSoftInt(0);
	ЭВМ.RemoveHandler(GetCR3, SoftInt);
	ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСимв8(0EX); ЛогЯдра.пВК_ПС;
	ЛогЯдра.пСтроку8(" Virtual  Physical Size");  ЛогЯдра.пВК_ПС;
	virt := 0;  virt0 := 0;  phys0 := 0;  size := 0;
	нцДля i := 0 до PTEs-1 делай
		НИЗКОУР.прочтиОбъектПоАдресу(kernelPD + размер16_от(адресВПамяти)*i, pt);
		если нечётноеЛи¿(pt) то	(* present *)
			pt := pt - pt остОтДеленияНа PS;
			нцДля j := 0 до PTEs-1 делай
				НИЗКОУР.прочтиОбъектПоАдресу(pt, phys);
				если нечётноеЛи¿(phys) то
					умень(phys, phys остОтДеленияНа PS);
					Page
				всё;
				увел(pt, 4);  увел(virt, 4*K)
			кц
		иначе
			увел(virt, 4*M)
		всё
	кц;
	virt := -1;  Page;
	ЛогЯдра.пСимв8(0FX); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
кон DisplayMap;

проц Write64(s: массив из симв8; low, high: мнвоНаБитахМЗ);
нач
	ЛогЯдра.пСтроку8(s);  ЛогЯдра.пСимв8("=");
	ЛогЯдра.п16ричное(НИЗКОУР.подмениТипЗначения(цел32, high), 8);
	ЛогЯдра.п16ричное(НИЗКОУР.подмениТипЗначения(цел32, low), 9)
кон Write64;

проц Bits(s: массив из симв8; x: мнвоНаБитахМЗ; ofs, n: цел32);
нач
	ЛогЯдра.пСтроку8(s); ЛогЯдра.пСимв8("="); ЛогЯдра.пМнвоНаБитахМЗКакБиты(x, ofs, n)
кон Bits;

проц -RealReadMSR(msr: длинноеЦелМЗ; перем low, high: мнвоНаБитахМЗ);
машКод
#если I386 то
	POP EDI
	POP ESI
	POP ECX
	RDMSR
	MOV [ESI], EAX
	MOV [EDI], EDX
#аесли AMD64 то
	POP RDI
	POP RSI
	POP RCX
	RDMSR
	MOV [RSI], EAX
	MOV [RDI], EDX
#иначе
	unimplemented
#кон
кон RealReadMSR;

проц IntReadMSR(перем state: ЭВМ.СостояниеПроцессора);
нач
#если I386 то
	RealReadMSR(state.EAX, msrlow, msrhigh);
#аесли AMD64 то
	RealReadMSR(state.RAX, msrlow, msrhigh);
#иначе
	unimplemented
#кон
кон IntReadMSR;

проц ReadMSR(msr: цел32; перем low, high: мнвоНаБитахМЗ);
нач
	ЭВМ.InstallHandler(IntReadMSR, SoftInt);	(* ignore race *)
	DoSoftInt(msr);
	ЭВМ.RemoveHandler(IntReadMSR, SoftInt);
	low := msrlow; high := msrhigh
кон ReadMSR;

проц DisplayMTTR*;
перем version, i, j, k, vcnt: цел32; features, low, high, mask: мнвоНаБитахМЗ; vendor: ЭВМ.Поставщик;
нач
	ЛогЯдра.ЗахватВЕдиноличноеПользование;
(*	Machine.CPUID(vendor, version, features);
	KernelLog.String("CPU: ");  KernelLog.Int(ASH(version, -8) MOD 16, 1);
	KernelLog.Char(".");  KernelLog.Int(ASH(version, -4) MOD 16, 1);
	KernelLog.Char(".");  KernelLog.Int(version MOD 16, 1);
	Bits(", features", features, 0, 32);
	KernelLog.String(", vendor ");  KernelLog.String(vendor);
	KernelLog.Ln; *)
	features := ЭВМ.свойстваПроцессора_1;
	если 5 в features то	(* MSR supported *)
		если 12 в features то	(* MTTR supported *)
			ReadMSR(0FEH, low, high);
			vcnt := НИЗКОУР.подмениТипЗначения(цел32, low) остОтДеленияНа 256;
			ЛогЯдра.пСтроку8("VCNT="); ЛогЯдра.пЦел64(vcnt, 1);
			Bits(", FIX", low, 8, 1); Bits(", WC", low, 10, 1);
			ЛогЯдра.пВК_ПС;
			если 8 в low то
				ReadMSR(2FFH, low, high);  Write64("DefType", low, high);  ЛогЯдра.пВК_ПС;
				ReadMSR(250H, low, high);  Write64("Fix64k", low, high);  ЛогЯдра.пВК_ПС;
				ReadMSR(258H, low, high);  Write64("Fix16k", low, high);  ЛогЯдра.пВК_ПС;
				ReadMSR(259H, low, high);  Write64("Fix16k", low, high);  ЛогЯдра.пВК_ПС;
				ReadMSR(268H, low, high);  Write64("Fix4k", low, high);  ЛогЯдра.пВК_ПС;
				ReadMSR(269H, low, high);  Write64("Fix4k", low, high);  ЛогЯдра.пВК_ПС;
				ReadMSR(26AH, low, high);  Write64("Fix4k", low, high);  ЛогЯдра.пВК_ПС;
				ReadMSR(26BH, low, high);  Write64("Fix4k", low, high);  ЛогЯдра.пВК_ПС;
				ReadMSR(26CH, low, high);  Write64("Fix4k", low, high);  ЛогЯдра.пВК_ПС;
				ReadMSR(26DH, low, high);  Write64("Fix4k", low, high);  ЛогЯдра.пВК_ПС;
				ReadMSR(26EH, low, high);  Write64("Fix4k", low, high);  ЛогЯдра.пВК_ПС;
				ReadMSR(26FH, low, high);  Write64("Fix4k", low, high);  ЛогЯдра.пВК_ПС;
				нцДля i := 0 до vcnt-1 делай
					ЛогЯдра.пЦел64(i, 1);
					ReadMSR(200H+2*i, low, high);  Write64(" base", low, high);
					ReadMSR(200H+2*i+1, low, high);  Write64(", mask", low, high);
					если 11 в low то	(* valid *)
						mask := логСдвиг(low, -12);
						нцДля j := 0 до 3 делай
							если j в high то включиВоМнвоНаБитах(mask, 20+j) всё
						кц;
						j := 0;  нцПока (j # 32) и ~(j в mask) делай увел(j) кц;
						k := 31;  нцПока (k # -1) и ~(k в mask) делай умень(k) кц;
						если (k = 23) и (k >= j) и (mask = {j..k}) то
							ЛогЯдра.пСтроку8(", ");  ЛогЯдра.пОбъёмПамятиСРазмерностью(НИЗКОУР.подмениТипЗначения(цел32, {j})*4*1024, 1, "B")
						иначе
							ЛогЯдра.пСтроку8(" discon=");  ЛогЯдра.п16ричное(НИЗКОУР.подмениТипЗначения(цел32, mask), 8)
						всё
					всё;
					ЛогЯдра.пВК_ПС
				кц
			иначе
				ЛогЯдра.пСтроку8("Fixed range registers not supported");  ЛогЯдра.пВК_ПС
			всё
		иначе
			ЛогЯдра.пСтроку8("MTTR not supported");  ЛогЯдра.пВК_ПС
		всё
	иначе
		ЛогЯдра.пСтроку8("MSR not supported");  ЛогЯдра.пВК_ПС
	всё;
	ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
кон DisplayMTTR;

(*
PROCEDURE IntSetCache(VAR state: Machine.State);
VAR res: INTEGER;
BEGIN
	Machine.SetLocalCacheProperties(0FA800000H, 800000H, Machine.WC, res);
	KernelLog.Enter; KernelLog.String("SetCache "); KernelLog.Int(res, 1); KernelLog.Exit
END IntSetCache;

PROCEDURE Test*;
BEGIN
	Machine.InstallHandler(IntSetCache, SoftInt);	(* ignore race *)
	DoSoftInt(0);
	Machine.RemoveHandler(IntSetCache, SoftInt)
END Test;

PROCEDURE -SetMTTRphysBase(n: SIGNED32; high, low: SIGNED32);
CODE {SYSTEM.Pentium, SYSTEM.Privileged}
	POP EAX
	POP EDX
	POP ECX
	SHL ECX, 1
	ADD ECX, 200H	; MTTRphysBase0
	WRMSR
END SetMTTRphysBase;

PROCEDURE -SetMTTRphysMask(n: SIGNED32; high, low: SIGNED32);
CODE {SYSTEM.Pentium, SYSTEM.Privileged}
	POP EAX
	POP EDX
	POP ECX
	SHL ECX, 1
	ADD ECX, 201H	; MTTRphysMask0
	WRMSR
END SetMTTRphysMask;

(*
1 000000000H 4GB WB
0 0F0000000H 128MB UC
4 0F8000000H 64MB UC
- 0FC000000H 32MB WB (implicit)
3 0FE000000H 32MB UC
2 100000000H 256MB WB

WB 0MB-2048MB 2048MB
WB 2048MB-3072MB 1024MB
WB 3072MB-3584MB 512MB
WB 3584MB-3840MB 256MB
WC 4032MB-4064MB 32MB
WB 4096MB-4352MB 256MB
*)

PROCEDURE IntSetCache2(VAR state: Machine.State);
BEGIN
	SetMTTRphysBase(3, 0, 0FE000000H);
	SetMTTRphysMask(3, 0FH, 0FE000800H);
	SetMTTRphysBase(4, 0, 0F8000000H);
	SetMTTRphysMask(4, 0FH, 0FC000800H);
	SetMTTRphysBase(0, 0, 0F0000000H);
	SetMTTRphysMask(0, 0FH, 0F8000800H)
END IntSetCache2;

PROCEDURE Test2*;
BEGIN
	Machine.InstallHandler(IntSetCache2, SoftInt);	(* ignore race *)
	DoSoftInt(0);
	Machine.RemoveHandler(IntSetCache2, SoftInt)
END Test2;

PROCEDURE Test3*;
VAR res: SIGNED32;
BEGIN
	Processors.GlobalSetCacheProperties(0FA800000H, 800000H, Machine.WC, res);
	KernelLog.Enter; KernelLog.String("SetCache "); KernelLog.Int(res, 1); KernelLog.Exit
END Test3;
*)

кон MemInfo.

System.Free MemInfo ~

MemInfo.DisplayMap
MemInfo.DisplayMTTR

MemInfo.Test
MemInfo.Test2
MemInfo.Test3

DisplayTests.Mod

PCITools.Scan
