(* ETH Oberon, Copyright 2001 ETH Zuerich Institut fuer Computersysteme, ETH Zentrum, CH-8092 Zuerich.
Refer to the "General ETH Oberon System Source License" contract available at: http://www.oberon.ethz.ch/ *)

модуль GfxImages; (** non-portable *)	(* eos  **)
(** AUTHOR "eos"; PURPOSE "Gfx raster image transformations"; *)

	(*
		24.05.2000 - adapted to new Raster module
	*)

	использует
		НИЗКОУР, Raster, GfxMatrix;


	(**
		Image transformations are decomposed into a series of one-dimensional shift and scale transforms. These
		are delegated to a filter object provided by the caller. The caller controls visual quality and execution time
		by selecting a filter which complies with its demands.
	**)

	тип
		Image* = Raster.Image;

		ShiftProc* = проц (перем filter: Raster.Mode; src, dst: Image; sadr: адресВПамяти; sbit: размерМЗ; dadr: адресВПамяти; dbit, len: размерМЗ; t: вещ32);
		ScaleProc* = проц (перем filter: Raster.Mode; src, dst: Image; sadr: адресВПамяти; sbit: размерМЗ; dadr: адресВПамяти; dbit, len: размерМЗ; xy, dxy: вещ32);

		(** transformation filter **)
		Filter* = запись (Raster.Mode)
			hshift*, vshift*: ShiftProc;	(** procedures for shifting rows and columns **)
			hscale*, vscale*: ScaleProc;	(** procedures for scaling rows and columns **)
		кон;


	перем
		PreCache, Cache: Image;	(* caches for image transformations *)
		hshift*, vshift*: ShiftProc;

	(**--- Filters ---**)

	(** predefined filter procedures using box filter (i.e. no filtering): fast and ugly **)

	проц HShift* (перем filter: Raster.Mode; src, dst: Image; sadr: адресВПамяти; sbit: размерМЗ; dadr: адресВПамяти; dbit, len: размерМЗ; tx: вещ32);
	нач
		если tx >= 0.5 то
			dbit := dbit + dst.fmt.bpp; увел(dadr, dbit DIV 8); dbit := dbit остОтДеленияНа 8;
			умень(len)
		всё;
		Raster.Bind(filter, src.fmt, dst.fmt);
		filter.transfer(filter, sadr, sbit, dadr, dbit, len)
	кон HShift;

	проц VShift* (перем filter: Raster.Mode; src, dst: Image; sadr: адресВПамяти; sbit: размерМЗ; dadr: адресВПамяти; dbit, len: размерМЗ; ty: вещ32);
	нач
		если ty >= 0.5 то
			увел(dadr, dst.bpr);
			умень(len)
		всё;
		Raster.Bind(filter, src.fmt, dst.fmt);
		нцПока len > 0 делай
			filter.transfer(filter, sadr, sbit, dadr, dbit, 1);
			увел(sadr, src.bpr); увел(dadr, dst.bpr);
			умень(len)
		кц
	кон VShift;

	проц HScale* (перем filter: Raster.Mode; src, dst: Image; sadr: адресВПамяти; sbit: размерМЗ; dadr: адресВПамяти; dbit, dlen: размерМЗ; x, dx: вещ32);
		перем i0, i1: размерМЗ;
	нач
		Raster.Bind(filter, src.fmt, dst.fmt);
		i0 := 0;
		нцПока dlen > 0 делай
			i1 := округлиВниз(x);
			если i0 < i1 то
				если i1 >= src.width то i1 := src.width-1 всё;
				sbit := sbit + (i1 - i0) * src.fmt.bpp; увел(sadr, sbit DIV 8); sbit := sbit остОтДеленияНа 8;
				i0 := i1
			всё;
			filter.transfer(filter, sadr, sbit, dadr, dbit, 1);
			dbit := dbit + dst.fmt.bpp; увел(dadr, dbit DIV 8); dbit := dbit остОтДеленияНа 8;
			x := x + dx; умень(dlen)
		кц
	кон HScale;

	проц VScale* (перем filter: Raster.Mode; src, dst: Image; sadr: адресВПамяти; sbit: размерМЗ; dadr: адресВПамяти; dbit, dlen: размерМЗ; y, dy: вещ32);
		перем j0, j1: размерМЗ;
	нач
		Raster.Bind(filter, src.fmt, dst.fmt);
		j0 := 0;
		нцПока dlen > 0 делай
			j1 := округлиВниз(y);
			если j0 < j1 то
				если j1 >= src.height то j1 := src.height-1 всё;
				увел(sadr, (j1 - j0) * src.bpr);
				j0 := j1
			всё;
			filter.transfer(filter, sadr, sbit, dadr, dbit, 1);
			увел(dadr, dst.bpr);
			y := y + dy; умень(dlen)
		кц
	кон VScale;

	(** predefined filter procedures for linearly filtered transformations: slow and less ugly **)

	проц LinearHShift* (перем filter: Raster.Mode; src, dst: Image; sadr: адресВПамяти; sbit: размерМЗ; dadr: адресВПамяти; dbit, len: размерМЗ; tx: вещ32);
		конст r = Raster.r; g = Raster.g; b = Raster.b; a = Raster.a;
		перем w0, w1, sinc, dinc, i, red, green, blue, alpha: размерМЗ; da: адресВПамяти; spix, dpix: Raster.Pixel;
	нач
		w0 := округлиВниз(1000H*tx + 0.5); w1 := 1000H-w0;
		если (w0 < 10H) или (w1 < 10H) то
			HShift(filter, src, dst, sadr, sbit, dadr, dbit, len, tx)
		иначе
			Raster.Bind(filter, Raster.PixelFormat, dst.fmt);
			sinc := src.fmt.bpp; dinc := dst.fmt.bpp; da := dadr;
			src.fmt.unpack(src.fmt, sadr, sbit, spix);
			нцДля i := 0 до 3 делай dpix[i] := симв8ИзКода(w1 * кодСимв8(spix[i]) DIV 1000H) кц;
			filter.transfer(filter, адресОт(dpix[0]), 0, dadr, dbit, 1);
			увел(dbit, dinc); увел(dadr, dbit DIV 8); dbit := dbit остОтДеленияНа 8;
			умень(len);
			нцПока len > 0 делай
				red := w0 * кодСимв8(spix[r]); green := w0 * кодСимв8(spix[g]); blue := w0 * кодСимв8(spix[b]); alpha := w0 * кодСимв8(spix[a]);
				увел(sbit, sinc); увел(sadr, sbit DIV 8); sbit := sbit остОтДеленияНа 8;
				src.fmt.unpack(src.fmt, sadr, sbit, spix);
				dpix[r] := симв8ИзКода((red + w1 * кодСимв8(spix[r])) DIV 1000H);
				dpix[g] := симв8ИзКода((green + w1 * кодСимв8(spix[g])) DIV 1000H);
				dpix[b] := симв8ИзКода((blue + w1 * кодСимв8(spix[b])) DIV 1000H);
				dpix[a] := симв8ИзКода((alpha + w1 * кодСимв8(spix[a])) DIV 1000H);
				filter.transfer(filter, адресОт(dpix[0]), 0, dadr, dbit, 1);
				увел(dbit, dinc); увел(dadr, dbit DIV 8); dbit := dbit остОтДеленияНа 8;
				умень(len)
			кц;
			если (da - dst.adr) DIV dst.bpr = (dadr - dst.adr) DIV dst.bpr то
				нцДля i := 0 до 3 делай dpix[i] := симв8ИзКода(w0 * кодСимв8(spix[i]) DIV 1000H) кц;
				filter.transfer(filter, адресОт(dpix[0]), 0, dadr, dbit, 1)
			всё
		всё
	кон LinearHShift;

	проц LinearVShift* (перем filter: Raster.Mode; src, dst: Image; sadr: адресВПамяти; sbit: размерМЗ; dadr: адресВПамяти; dbit, len: размерМЗ; ty: вещ32);
		конст r = Raster.r; g = Raster.g; b = Raster.b; a = Raster.a;
		перем w0, w1, i, red, green, blue, alpha: размерМЗ; spix, dpix: Raster.Pixel;
	нач
		w0 := округлиВниз(1000H*ty + 0.5); w1 := 1000H-w0;
		если (w0 < 10H) или (w1 < 10H) то
			VShift(filter, src, dst, sadr, sbit, dadr, dbit, len, ty)
		иначе
			Raster.Bind(filter, Raster.PixelFormat, dst.fmt);
			src.fmt.unpack(src.fmt, sadr, sbit, spix);
			нцДля i := 0 до 3 делай dpix[i] := симв8ИзКода(w1 * кодСимв8(spix[i]) DIV 1000H) кц;
			filter.transfer(filter, адресОт(dpix[0]), 0, dadr, dbit, 1);
			увел(dadr, dst.bpr);
			умень(len);
			нцПока len > 0 делай
				red := w0 * кодСимв8(spix[r]); green := w0 * кодСимв8(spix[g]); blue := w0 * кодСимв8(spix[b]); alpha := w0 * кодСимв8(spix[a]);
				увел(sadr, src.bpr);
				src.fmt.unpack(src.fmt, sadr, sbit, spix);
				dpix[r] := симв8ИзКода((red + w1 * кодСимв8(spix[r])) DIV 1000H);
				dpix[g] := симв8ИзКода((green + w1 * кодСимв8(spix[g])) DIV 1000H);
				dpix[b] := симв8ИзКода((blue + w1 * кодСимв8(spix[b])) DIV 1000H);
				dpix[a] := симв8ИзКода((alpha + w1 * кодСимв8(spix[a])) DIV 1000H);
				filter.transfer(filter, адресОт(dpix[0]), 0, dadr, dbit, 1);
				увел(dadr, dst.bpr);
				умень(len)
			кц;
			если (dst.adr < dadr) и (dadr < dst.adr + dst.height * dst.bpr) то
				нцДля i := 0 до 3 делай dpix[i] := симв8ИзКода(w0 * кодСимв8(spix[i]) DIV 1000H) кц;
				filter.transfer(filter, адресОт(dpix[0]), 0, dadr, dbit, 1)
			всё
		всё
	кон LinearVShift;

	проц LinearHScale* (перем filter: Raster.Mode; src, dst: Image; sadr: адресВПамяти; sbit: размерМЗ; dadr: адресВПамяти; dbit, dlen: размерМЗ; x, dx: вещ32);
		перем i0, i1,  w1, w0, j: размерМЗ; spix: массив 2 из Raster.Pixel; dpix: Raster.Pixel;
	нач
		Raster.Bind(filter, Raster.PixelFormat, dst.fmt);
		x := x+0.5;	(* displace sample position to midpoint between candidate pixels *)
		i0 := 0;
		src.fmt.unpack(src.fmt, sadr, sbit, spix[0]); spix[1] := spix[0];
		нцПока dlen > 0 делай
			i1 := округлиВниз(x);
			если i1 > i0 то
				увел(i0);
				если i0 >= src.width то
					spix[0] := spix[1]
				аесли i1 = i0 то
					spix[0] := spix[1];
					sbit := sbit + src.fmt.bpp; увел(sadr, sbit DIV 8); sbit := sbit остОтДеленияНа 8;
					src.fmt.unpack(src.fmt, sadr, sbit, spix[1])
				аесли i1 < src.width то
					sbit := sbit + (i1 - i0) * src.fmt.bpp; увел(sadr, sbit DIV 8); sbit := sbit остОтДеленияНа 8;
					src.fmt.unpack(src.fmt, sadr, sbit, spix[0]);
					sbit := sbit + src.fmt.bpp; увел(sadr, sbit DIV 8); sbit := sbit остОтДеленияНа 8;
					src.fmt.unpack(src.fmt, sadr, sbit, spix[1])
				иначе
					sbit := sbit + (src.width - i0) * src.fmt.bpp; увел(sadr, sbit DIV 8); sbit := sbit остОтДеленияНа 8;
					src.fmt.unpack(src.fmt, sadr, sbit, spix[0]); spix[1] := spix[0]
				всё;
				i0 := i1
			всё;
			w1 := округлиВниз(1000H*(x - i1)); w0 := 1000H-w1;
			нцДля j := 0 до 3 делай
				dpix[j] := Raster.Clamp[200H + (кодСимв8(spix[0, j]) * w0 + кодСимв8(spix[1, j]) * w1) DIV 1000H]
			кц;
			filter.transfer(filter, адресОт(dpix[0]), 0, dadr, dbit, 1);
			dbit := dbit + dst.fmt.bpp; увел(dadr, dbit DIV 8); dbit := dbit остОтДеленияНа 8;
			x := x + dx; умень(dlen)
		кц
	кон LinearHScale;

	проц LinearVScale* (перем filter: Raster.Mode; src, dst: Image; sadr: адресВПамяти; sbit: размерМЗ; dadr: адресВПамяти; dbit, dlen: размерМЗ; y, dy: вещ32);
		перем j0, j1, w1, w0, j: размерМЗ; spix: массив 2 из Raster.Pixel; dpix: Raster.Pixel;
	нач
		Raster.Bind(filter, Raster.PixelFormat, dst.fmt);
		y := y+0.5;	(* displace sample position to midpoint between candidate pixels *)
		j0 := 0;
		src.fmt.unpack(src.fmt, sadr, sbit, spix[0]); spix[1] := spix[0];
		нцПока dlen > 0 делай
			j1 := округлиВниз(y);
			если j1 > j0 то
				увел(j0);
				если j0 >= src.height то
					spix[0] := spix[1]
				аесли j1 = j0 то
					spix[0] := spix[1];
					увел(sadr, src.bpr);
					src.fmt.unpack(src.fmt, sadr, sbit, spix[1])
				аесли j1 < src.height то
					увел(sadr, (j1 - j0) * src.bpr);
					src.fmt.unpack(src.fmt, sadr, sbit, spix[0]);
					увел(sadr, src.bpr);
					src.fmt.unpack(src.fmt, sadr, sbit, spix[1])
				иначе
					увел(sadr, src.bpr);
					src.fmt.unpack(src.fmt, sadr, sbit, spix[0]); spix[1] := spix[0]
				всё;
				j0 := j1
			всё;
			w1 := округлиВниз(1000H*(y - j1)); w0 := 1000H-w1;
			нцДля j := 0 до 3 делай
				dpix[j] := Raster.Clamp[200H + (кодСимв8(spix[0, j]) * w0 + кодСимв8(spix[1, j]) * w1) DIV 1000H]
			кц;
			filter.transfer(filter, адресОт(dpix[0]), 0, dadr, dbit, 1);
			увел(dadr, dst.bpr);
			y := y + dy; умень(dlen)
		кц
	кон LinearVScale;

	(** initialize filter with compositing operation and transformation procedures **)
	проц InitFilter* (перем filter: Filter; op: цел8; hshift, vshift: ShiftProc; hscale, vscale: ScaleProc);
	нач
		Raster.InitMode(filter, op);
		filter.hshift := hshift; filter.vshift := vshift;
		filter.hscale := hscale; filter.vscale := vscale
	кон InitFilter;

	(* get temporary pixel format image for storing intermediate images *)
	проц GetTempImage (перем img, cache: Raster.Image; w, h: размерМЗ);
		перем size: размерМЗ;
	нач
		size := w * h;
		если (size >= 10000H) или (cache = НУЛЬ) то
			нов(img)
		иначе
			img := cache; cache := НУЛЬ
		всё;
		Raster.Create(img, w, h, Raster.PixelFormat)
	кон GetTempImage;

	проц FreeTempImage (перем img, cache: Raster.Image);
	нач
		если img.width * img.height < 10000H то
			cache := img
		всё
	кон FreeTempImage;

	(* depending on matrix elements, transpose/mirror image to avoid bottleneck problems *)
	проц Preprocess (перем src: Raster.Image; перем m: GfxMatrix.Matrix; перем filter: Filter);
		конст
			r = Raster.r; g = Raster.g; b = Raster.b;
		перем
			dst: Raster.Image; mode: Raster.Mode; dinc, sinc, h, w, sbit: размерМЗ;
			dadr, sadr, sa, da: адресВПамяти;
			mat: GfxMatrix.Matrix; t: вещ32;
	нач
		если матМодуль(m[0, 0] * m[1, 1]) >= матМодуль(m[0, 1] * m[1, 0]) то	(* no need to swap rows and columns *)
			если (m[0, 0] <= 0) или (m[1, 1] <= 0) то
				GetTempImage(dst, PreCache, src.width, src.height);
				Raster.InitModeColor(mode, Raster.srcCopy, кодСимв8(filter.col[r]), кодСимв8(filter.col[g]), кодСимв8(filter.col[b]));
				Raster.Bind(mode, src.fmt, dst.fmt);
				если m[0, 0] >= 0 то dadr := dst.adr; dinc := 4
				иначе dadr := dst.adr + 4*(dst.width-1); dinc := -4
				всё;
				если m[1, 1] >= 0 то sadr := src.adr; sinc := src.bpr
				иначе sadr := src.adr + (src.height-1) * src.bpr; sinc := -src.bpr
				всё;
				h := 0;
				нцПока h < src.height делай
					w := 0; sa := sadr; sbit := 0; da := dadr;
					нцПока w < src.width делай
						mode.transfer(mode, sa, sbit, da, 0, 1);
						sbit := sbit + src.fmt.bpp; увел(sa, sbit DIV 8); sbit := sbit остОтДеленияНа 8;
						увел(da, dinc); увел(w)
					кц;
					увел(sadr, sinc); увел(dadr, dst.bpr); увел(h)
				кц;
				если m[0, 0] < 0 то
					GfxMatrix.Init(mat, -1, 0, 0, 1, w, 0);
					GfxMatrix.Concat(mat, m, m)
				всё;
				если m[1, 1] < 0 то
					GfxMatrix.Init(mat, 1, 0, 0, -1, 0, h);
					GfxMatrix.Concat(mat, m, m)
				всё;
				src := dst;
				FreeTempImage(dst, PreCache)	(* reuse allocated image in next call *)
			всё
		иначе	(* need to transpose *)
			t := m[0, 0]; m[0, 0] := m[1, 0]; m[1, 0] := t;
			t := m[0, 1]; m[0, 1] := m[1, 1]; m[1, 1] := t;
			GetTempImage(dst, PreCache, src.height, src.width);
			Raster.InitModeColor(mode, Raster.srcCopy, кодСимв8(filter.col[r]), кодСимв8(filter.col[g]), кодСимв8(filter.col[b]));
			Raster.Bind(mode, src.fmt, dst.fmt);
			если m[0, 0] <= 0 то dadr := dst.adr; dinc := dst.bpr
			иначе dadr := dst.adr + (dst.height-1) * dst.bpr; dinc := -dst.bpr
			всё;
			если m[1, 1] <= 0 то sadr := src.adr; sinc := src.bpr
			иначе sadr := src.adr + (src.height-1) * src.bpr; sinc := -src.bpr
			всё;
			h := 0;
			нцПока h < src.height делай
				w := 0; sa := sadr; sbit := 0; da := dadr;
				нцПока w < src.width делай
					mode.transfer(mode, sa, sbit, da, 0, 1);
					sbit := sbit + src.fmt.bpp; увел(sa, sbit DIV 8); sbit := sbit остОтДеленияНа 8;
					увел(da, dinc); увел(w)
				кц;
				увел(sadr, sinc); увел(dadr, 4); увел(h)
			кц;
			если m[0, 0] < 0 то
				GfxMatrix.Init(mat, -1, 0, 0, 1, dst.width, 0);
				GfxMatrix.Concat(mat, m, m)
			всё;
			если m[1, 1] < 0 то
				GfxMatrix.Init(mat, 1, 0, 0, -1, 0, dst.height);
				GfxMatrix.Concat(mat, m, m)
			всё;
			src := dst;
			FreeTempImage(dst, PreCache)
		всё
	кон Preprocess;

	(* shift source row by fractional amount *)
	проц SkewRow (src, dst: Image; si, sj, w, di, dj: размерМЗ; tx: вещ32; перем filter: Filter);
		перем sbit, dbit: размерМЗ; sadr, dadr: адресВПамяти;
	нач
		утв((0.0 <= tx) и (tx <= 1.0), 100);	(* rounding problem if using tx < 1.0 *)
		если si < 0 то увел(w, si); умень(di, si); si := 0 всё;
		если si + w > src.width то w := src.width - si всё;
		если w > 0 то
			sbit := si * src.fmt.bpp; sadr := src.adr + sj * src.bpr + sbit DIV 8; sbit := sbit остОтДеленияНа 8;
			dbit := di * dst.fmt.bpp; dadr := dst.adr + dj * dst.bpr + dbit DIV 8; dbit := dbit остОтДеленияНа 8;
			filter.hshift(filter, src, dst, sadr, sbit, dadr, dbit, w, tx)
		всё
	кон SkewRow;

	(* shear rectangle in source image horizontally; clip to destination boundary *)
	проц SkewRows (src, dst: Image; si, sj, w, h, dj: размерМЗ; x, dx: вещ32; перем filter: Filter);
		перем j, di, n: размерМЗ;
	нач
		j := 0;
		если dj < 0 то
			j := -dj;
			если j >= h то возврат всё
		всё;
		если dj + h > dst.height то
			h := dst.height - dj;
			если h <= 0 то возврат всё
		всё;

		если dx > 0 то
			если x + h * dx >= dst.width то
				h := -округлиВниз((x - dst.width)/dx)
			всё;
			x := x + j * dx;
			если x + w < 0 то
				n := -округлиВниз((x + w)/dx);
				увел(j, n); x := x + n * dx
			всё;
			если x < 0 то
				n := j - округлиВниз(x/dx);
				если n > h то n := h всё;
				нцПока j < n делай
					di := округлиВниз(x);
					если di + w > dst.width то w := dst.width всё;
					SkewRow(src, dst, si - di, sj + j, di + w, 0, dj + j, x - di, filter);
					увел(j); x := x + dx
				кц
			всё;
			нцПока j < h делай
				di := округлиВниз(x);
				если di + w > dst.width то w := dst.width - di всё;
				SkewRow(src, dst, si, sj + j, w, di, dj + j, x - di, filter);
				увел(j); x := x + dx
			кц

		аесли dx < 0 то
			если x + w + h * dx < 0 то
				h := -округлиВниз((x + w)/dx)
			всё;
			x := x + j * dx;
			если x >= dst.width то
				n := округлиВниз((dst.width - x)/dx) + 1;
				увел(j, n); x := x + n * dx
			всё;
			n := j - округлиВниз(x/dx);	(* row at which x drops below zero *)
			если n > h то n := h всё;
			нцПока j < n делай
				di := округлиВниз(x);
				если di + w < dst.width то
					SkewRow(src, dst, si, sj + j, w, di, dj + j, x - di, filter)
				иначе
					SkewRow(src, dst, si, sj + j, dst.width - di, di, dj + j, x - di, filter)
				всё;
				увел(j); x := x + dx
			кц;
			нцПока j < h делай
				di := округлиВниз(x);
				если di + w < dst.width то
					SkewRow(src, dst, si - di, sj + j, di + w, 0, dj + j, x - di, filter)
				иначе
					SkewRow(src, dst, si - di, sj + j, dst.width, 0, dj + j, x - di, filter)
				всё;
				увел(j); x := x + dx
			кц

		аесли x < 0 то
			di := округлиВниз(x);
			если di + w > dst.width то
				si := si - di; x := x - di;
				нцПока j < h делай
					SkewRow(src, dst, si, sj + j, dst.width, 0, dj + j, x, filter);
					увел(j)
				кц
			аесли di + w >= 0 то
				si := si - di; w := w + di; x := x - di;
				нцПока j < h делай
					SkewRow(src, dst, si, sj + j, w, 0, dj + j, x, filter);
					увел(j)
				кц
			всё

		аесли x < dst.width то
			di := округлиВниз(x); x := x - di;
			если di + w > dst.width то w := dst.width - di всё;
			нцПока j < h делай
				SkewRow(src, dst, si, sj + j, w, di, dj + j, x, filter);
				увел(j)
			кц
		всё
	кон SkewRows;

	(* shift source column by fractional amount *)
	проц SkewCol (src, dst: Image; si, sj, h, di, dj: размерМЗ; ty: вещ32; перем filter: Filter);
		перем sbit, dbit: размерМЗ; sadr, dadr: адресВПамяти;
	нач
		утв((0.0 <= ty) и (ty <= 1.0), 100);	(* rounding problem with ty < 1.0 *)
		если sj < 0 то увел(h, sj); умень(dj, sj); sj := 0 всё;
		если sj + h > src.height то h := src.height - sj всё;
		если h > 0 то
			sbit := si * src.fmt.bpp; sadr := src.adr + sj * src.bpr + sbit DIV 8; sbit := sbit остОтДеленияНа 8;
			dbit := di * dst.fmt.bpp; dadr := dst.adr + dj * dst.bpr + dbit DIV 8; dbit := dbit остОтДеленияНа 8;
			filter.vshift(filter, src, dst, sadr, sbit, dadr, dbit, h, ty)
		всё
	кон SkewCol;

	(* shear rectangle in source image vertically; clip to destination boundary *)
	проц SkewCols (src, dst: Image; si, sj, w, h, di: размерМЗ; y, dy: вещ32; перем filter: Filter);
		перем i, dj, n: размерМЗ;
	нач
		i := 0;
		если di < 0 то
			i := -di;
			если i >= w то возврат всё
		всё;
		если di + w > dst.width то
			w := dst.width - di;
			если w <= 0 то возврат всё
		всё;

		если dy > 0 то
			если y + w * dy >= dst.height то
				w := -округлиВниз((y - dst.height)/dy)
			всё;
			y := y + i * dy;
			если y + h < 0 то
				n := -округлиВниз((y + h)/dy);
				увел(i, n); y := y + n * dy
			всё;
			если y < 0 то
				n := i - округлиВниз(y/dy);
				если n > w то n := w всё;
				нцПока i < n делай
					dj := округлиВниз(y);
					если dj + h > dst.height то h := dst.height всё;
					SkewCol(src, dst, si + i, sj - dj, dj + h, di + i, 0, y - dj, filter);
					увел(i); y := y + dy
				кц
			всё;
			нцПока i < w делай
				dj := округлиВниз(y);
				если dj + h > dst.height то h := dst.height - dj всё;
				SkewCol(src, dst, si + i, sj, h, di + i, dj, y - dj, filter);
				увел(i); y := y + dy
			кц

		аесли dy < 0 то
			если y + h + w * dy < 0 то
				w := -округлиВниз((y + h)/dy)
			всё;
			y := y + i * dy;
			если y >= dst.height то
				n := округлиВниз((dst.height - y)/dy) + 1;
				увел(i, n); y := y + n * dy
			всё;
			n := i - округлиВниз(y/dy);	(* column at which y drops below zero *)
			если n > w то n := w всё;
			нцПока i < n делай
				dj := округлиВниз(y);
				если dj + h < dst.height то
					SkewCol(src, dst, si + i, sj, h, di + i, dj, y - dj, filter)
				иначе
					SkewCol(src, dst, si + i, sj, dst.height - dj, di + i, dj, y - dj, filter)
				всё;
				увел(i); y := y + dy
			кц;
			нцПока i < w делай
				dj := округлиВниз(y);
				если dj + h < dst.height то
					SkewCol(src, dst, si + i, sj - dj, h + dj, di + i, 0, y - dj, filter)
				иначе
					SkewCol(src, dst, si + i, sj - dj, dst.height, di + i, 0, y - dj, filter)
				всё;
				увел(i); y := y + dy
			кц

		аесли y < 0 то
			dj := округлиВниз(y);
			если dj + h > dst.height то
				sj := sj - dj; y := y - dj;
				нцПока i < w делай
					SkewCol(src, dst, si + i, sj, dst.height, di + i, 0, y, filter);
					увел(i)
				кц
			аесли dj + h >= 0 то
				sj := sj - dj; h := h + dj; y := y - dj;
				нцПока i < w делай
					SkewCol(src, dst, si + i, sj, h, di + i, 0, y, filter);
					увел(i)
				кц
			всё

		аесли y < dst.height то
			dj := округлиВниз(y); y := y - dj;
			если dj + h > dst.height то h := dst.height - di всё;
			нцПока i < w делай
				SkewCol(src, dst, si + i, sj, h, di + i, dj, y, filter);
				увел(i)
			кц
		всё
	кон SkewCols;

	(** render translated image on destination **)
	проц Translate* (src, dst: Image; tx, ty: вещ32; перем filter: Filter);
		перем ti, tj, i, j, w, h: размерМЗ; tmp: Image;
	нач
		ti := округлиВниз(tx); tx := tx - ti;
		tj := округлиВниз(ty); ty := ty - tj;
		если tx < 0.01 то
			SkewCols(src, dst, 0, 0, src.width, src.height, ti, tj + ty, 0, filter)
		аесли ty < 0.01 то
			SkewRows(src, dst, 0, 0, src.width, src.height, tj, ti + tx, 0, filter)
		иначе
			i := 0; j := 0; w := src.width; h := src.height;
			если ti < 0 то i := -ti; увел(w, ti) всё;
			если ti + w >= dst.width то w := dst.width - ti - 1 всё;
			если tj < 0 то j := -tj; увел(h, tj) всё;
			если tj + h >= dst.height то h := dst.height - tj - 1 всё;
			GetTempImage(tmp, Cache, w, h+1);
			SkewCols(src, tmp, i, j, w, h, 0, ty, 0, filter);
			SkewRows(tmp, dst, 0, 0, tmp.width, tmp.height, tj, ti + tx, 0, filter);
			FreeTempImage(tmp, Cache)
		всё
	кон Translate;

	(** render scaled image on destination **)
	проц Scale* (src, dst: Image; sx, sy, tx, ty: вещ32; перем filter: Filter);
		перем xl, xr, yb, yt, w, h, sbit, dbit, i: размерМЗ; sadr, dadr: адресВПамяти; dy, y, dx, x: вещ32; tmp: Image;
	нач
		утв((sx > 0) и (sy > 0), 100);
		xl := округлиВниз(tx); xr := -округлиВниз(-(tx + sx * src.width));
		если xl < 0 то xl := 0 всё;
		если xr > dst.width то
			xr := dst.width;
			если xr <= xl то возврат всё;
		всё;
		yb := округлиВниз(ty); yt := -округлиВниз(-(ty + sy * src.height));
		если yb < 0 то yb := 0 всё;
		если yt > dst.height то
			yt := dst.height;
			если yt <= yb то возврат всё
		всё;
		w := xr - xl; h := yt - yb;

		если матМодуль(w - src.width) < 1 то
			dy := 1.0/sy; y := (0.5 - (ty - округлиВниз(ty))) * dy;
			sadr := src.adr; sbit := 0;
			dbit := xl * dst.fmt.bpp; dadr := dst.adr + yb * dst.bpr + dbit DIV 8; dbit := dbit остОтДеленияНа 8;
			i := 0;
			нцПока i < src.width делай
				filter.vscale(filter, src, dst, sadr, sbit, dadr, dbit, h, y, dy);
				sbit := sbit + src.fmt.bpp; увел(sadr, sbit DIV 8); sbit := sbit остОтДеленияНа 8;
				dbit := dbit + dst.fmt.bpp; увел(dadr, dbit DIV 8); dbit := dbit остОтДеленияНа 8;
				увел(i)
			кц

		аесли матМодуль(h - src.height) < 1 то
			dx := 1.0/sx; x := (0.5 - (tx - округлиВниз(tx))) * dx;
			sadr := src.adr; sbit := 0;
			dbit := xl * dst.fmt.bpp; dadr := dst.adr + yb * dst.bpr + dbit DIV 8; dbit := dbit остОтДеленияНа 8;
			i := 0;
			нцПока i < src.height делай
				filter.hscale(filter, src, dst, sadr, sbit, dadr, dbit, w, x, dx);
				увел(sadr, src.bpr); увел(dadr, dst.bpr);
				увел(i)
			кц

		иначе
			GetTempImage(tmp, Cache, src.width, h);
			dy := 1.0/sy; y := (0.5 - (ty - округлиВниз(ty))) * dy;
			sadr := src.adr; sbit := 0; dadr := tmp.adr; dbit := 0;
			i := 0;
			нцПока i < src.width делай
				filter.vscale(filter, src, tmp, sadr, sbit, dadr, dbit, h, y, dy);
				sbit := sbit + src.fmt.bpp; увел(sadr, sbit DIV 8); sbit := sbit остОтДеленияНа 8;
				dbit := dbit + tmp.fmt.bpp; увел(dadr, dbit DIV 8); dbit := dbit остОтДеленияНа 8;
				увел(i)
			кц;
			dx := 1.0/sx; x := (0.5 - (tx - округлиВниз(tx))) * dx;
			sadr := tmp.adr; sbit := 0;
			dbit := xl * dst.fmt.bpp; dadr := dst.adr + yb * dst.bpr + dbit DIV 8; dbit := dbit остОтДеленияНа 8;
			i := 0;
			нцПока i < h делай
				filter.hscale(filter, tmp, dst, sadr, sbit, dadr, dbit, w, x, dx);
				увел(sadr, tmp.bpr); увел(dadr, dst.bpr);
				увел(i)
			кц;
			FreeTempImage(tmp, Cache)
		всё
	кон Scale;

	(** render rotated image on destination **)
	проц Rotate* (src, dst: Image; sin, cos, tx, ty: вещ32; перем filter: Filter);
		перем m: GfxMatrix.Matrix; tan, htan, wsin, hcos, x, y: вещ32; wmax, h, iy, sw, sh: размерМЗ; tmp: Image;
	нач
		утв(матМодуль(sin * sin + cos * cos - 1) < 0.0001, 100);
		m[0, 0] := cos; m[0, 1] := sin; m[1, 0] := -sin; m[1, 1] := cos; m[2, 0] := tx; m[2, 1] := ty;
		Preprocess(src, m, filter);
		cos := m[0, 0]; sin := m[0, 1]; tx := m[2, 0]; ty := m[2, 1];
		tan := sin/(1.0 + cos);	(* identity for tan(phi/2); 1/2 SQRT(3) <= cos <= 1 *)
		sw := src.width; sh := src.height;
		htan := матМодуль(tan) * sh;
		wsin := матМодуль(sin) * sw;
		hcos := cos * sh;
		wmax := sw + округлиВниз(htan) + 1;	(* width of intermediate image *)
		h := округлиВниз(wsin + hcos) + 2;	(* second extra pixel for ty - tj *)
		GetTempImage(tmp, Cache, wmax, h + sh);	(* stack two intermediate images on top of each other *)
		если sin >= 0 то
			x := htan; tx := tx - x; y := hcos - sh
		иначе
			x := 0; y := wsin; tx := tx + wsin * tan; ty := ty - y
		всё;
		iy := округлиВниз(ty); y := y + (ty - iy);
		SkewRows(src, tmp, 0, 0, sw, sh, h, x, -tan, filter);	(* first pass: skew horizontal scanlines *)
		SkewCols(tmp, tmp, 0, h, wmax, sh, 0, y, sin, filter);	(* second pass: skew vertical scanlines *)
		SkewRows(tmp, dst, 0, 0, wmax, h, iy, tx, -tan, filter);	(* third pass: skew horizontal scanlines *)
		FreeTempImage(tmp, Cache)
	кон Rotate;

	(** render horizontally sheared image on destination **)
	проц ShearRows* (src, dst: Image; sx, tx: вещ32; перем filter: Filter);
	нач
		SkewRows(src, dst, 0, 0, src.width, src.height, 0, tx, sx, filter)
	кон ShearRows;

	(** render vertically sheared image on destination **)
	проц ShearCols* (src, dst: Image; sy, ty: вещ32; перем filter: Filter);
	нач
		SkewCols(src, dst, 0, 0, src.width, src.height, 0, ty, sy, filter)
	кон ShearCols;

	(** render affinely transformed image on destination **)
	проц Transform* (src, dst: Image; m: GfxMatrix.Matrix; перем filter: Filter);
		конст eps = 0.003;
		перем det, s, dx, x: вещ32; iy, w, h, ix: размерМЗ; tmp: Image;
	нач
		Preprocess(src, m, filter);
		если (матМодуль(m[0, 0]) >= eps) и (матМодуль(m[1, 1]) >= eps) то	(* matrix isn't singular *)
			если (матМодуль(m[0, 1]) < eps) и (матМодуль(m[1, 0]) < eps) то	(* no rotation or shear *)
				если (матМодуль(m[0, 0]-1) < eps) и (матМодуль(m[1, 1]-1) < eps) то	(* not even scaled *)
					Translate(src, dst, m[2, 0], m[2, 1], filter)
				иначе
					Scale(src, dst, m[0, 0], m[1, 1], m[2, 0], m[2, 1], filter)
				всё
			иначе
				det := m[0, 0] * m[1, 1] - m[0, 1] * m[1, 0];
				если матМодуль(det) >= eps то
					если (матМодуль(det-1) < eps) и (матМодуль(m[0, 0] - m[1, 1]) < eps) и (матМодуль(m[0, 1] + m[1, 0]) < eps) то
						Rotate(src, dst, m[0, 1], m[0, 0], m[2, 0], m[2, 1], filter)
					аесли матМодуль(m[0, 1]) < eps то	(* horizontal shear *)
						iy := округлиВниз(m[2, 1]);
						если матМодуль(det-1) >= eps то	(* scaled *)
							w := округлиВниз(m[0, 0] * src.width)+1;
							h := округлиВниз(m[1, 1] * src.height)+1;
							GetTempImage(tmp, Cache, w, h);
							Scale(src, tmp, m[0, 0], m[1, 1], 0, m[2, 1] - iy, filter);
							SkewRows(tmp, dst, 0, 0, tmp.width, tmp.height, iy, m[2, 0], m[1, 0]/m[1, 1], filter);
							FreeTempImage(tmp, Cache)
						аесли m[2, 1] - iy < eps то	(* integer translation *)
							SkewRows(src, dst, 0, 0, src.width, src.height, iy, m[2, 0], m[1, 0], filter)
						иначе
							GetTempImage(tmp, Cache, src.width, src.height+1);
							Translate(src, tmp, 0, m[2, 1] - iy, filter);
							SkewRows(tmp, dst, 0, 0, tmp.width, tmp.height, iy, m[2, 0], m[1, 0], filter);
							FreeTempImage(tmp, Cache)
						всё
					аесли матМодуль(m[1, 0]) < eps то	(* vertical shear *)
						ix := округлиВниз(m[2, 0]);
						если матМодуль(det-1) >= eps то	(* scaled *)
							w := округлиВниз(m[0, 0] * src.width)+1;
							h := округлиВниз(m[1, 1] * src.height)+1;
							GetTempImage(tmp, Cache, w, h);
							Scale(src, tmp, m[0, 0], m[1, 1], m[2, 0] - ix, 0, filter);
							SkewCols(tmp, dst, 0, 0, tmp.width, tmp.height, ix, m[2, 1], m[0, 1]/m[0, 0], filter);
							FreeTempImage(tmp, Cache)
						аесли m[2, 0] - ix < eps то	(* integer translation *)
							SkewCols(src, dst, 0, 0, src.width, src.height, ix, m[2, 1], m[0, 1], filter)
						иначе
							GetTempImage(tmp, Cache, src.width+1, src.height);
							Translate(src, tmp, m[2, 0] - ix, 0, filter);
							SkewRows(tmp, dst, 0, 0, tmp.width, tmp.height, ix, m[2, 1], m[0, 1], filter);
							FreeTempImage(tmp, Cache)
						всё
					иначе
						(*
							use the following identity:
								[ a b ]	[ a         0       ] [        1           0 ] [ 1 b/a ]
								[ c d ] = [ 0 (ad-bc)/a ] [ ca/(ad-bc) 1 ]  [ 0   1   ]
						*)
						s := det/m[0, 0];
						w := округлиВниз(m[0, 0] * src.width)+1;
						h := округлиВниз(s * src.height)+1;
						dx := m[1, 0]/s; x := (h-1) * матМодуль(dx) + 2;
						GetTempImage(tmp, Cache, w - 2*округлиВниз(-x) + 1, 2*h);
						Scale(src, tmp, m[0, 0], s, x, h, filter);
						ix := округлиВниз(m[2, 0]);
						SkewRows(tmp, tmp, 0, h, tmp.width, h, 0, m[2, 0] - ix, dx, filter);
						w := округлиВниз(x);
						если dx >= 0 то
							SkewCols(tmp, dst, w, 0, tmp.width - w, h, ix, m[2, 1], m[0, 1]/m[0, 0], filter)
						иначе
							s := m[0, 1]/m[0, 0];
							SkewCols(tmp, dst, 0, 0, tmp.width - w, h, ix - w, m[2, 1] - w * s, s, filter)
						всё;
						FreeTempImage(tmp, Cache)
					всё
				всё
			всё
		всё
	кон Transform;

	(** uses nearest-neighbor resampling (box filter); bad aliasing when downscaling **)
	проц InitNoFilter*(перем filter: Filter);
	нач
		InitFilter(filter, Raster.srcOverDst, HShift, VShift, HScale, VScale)
	кон InitNoFilter;

	(** uses linear interpolation (triangle filter); blurry when upscaling **)
	проц InitLinearFilter*(перем filter: Filter);
	нач
		InitFilter(filter, Raster.srcOverDst, LinearHShift, LinearVShift, LinearHScale, LinearVScale);
	кон InitLinearFilter;

нач
	hshift := HShift; vshift := VShift
кон GfxImages.
