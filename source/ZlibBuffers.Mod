(* ETH Oberon, Copyright 2001 ETH Zuerich Institut fuer Computersysteme, ETH Zentrum, CH-8092 Zuerich.
Refer to the "General ETH Oberon System Source License" contract available at: http://www.oberon.ethz.ch/ *)

модуль ZlibBuffers;	(** Stefan Walthert  **)

(*	2002.04.03	g.f.	procedure Drain fixed for SPARC	*)

использует
	НИЗКОУР;
		(*
			should be portable even if SYSTEM is imported:
			- PUT and GET only with byte sized operands
			- no overlapping MOVEs (unless malignant client passes buffer memory to buffer operations)
		*)

тип
	(** input/output buffer **)
	Buffer* = запись
		avail-: размерМЗ;	(** number of bytes that can be produced/consumed **)
		size-: размерМЗ;	(** total number of bytes in buffer memory **)
		totalOut-, totalIn-: размерМЗ;	(** total number of bytes produced/consumed **)
		next: адресВПамяти;	(* address of next byte to produce/consume **)
		adr: адресВПамяти;	(* buffer memory *)
	кон;


(** set buf.totalIn and buf.totalOut to zero **)
проц Reset*(перем buf: Buffer);
нач
	buf.totalIn := 0; buf.totalOut := 0
кон Reset;

(** initialize buffer on memory in client space **)
проц Init* (перем buf: Buffer; конст mem: массив из симв8; offset, size, avail: размерМЗ);
нач
	утв((0 <= offset) и (0 < size) и (offset + size <= длинаМассива(mem)), 100);
	утв((0 <= avail) и (avail <= size),101);
	buf.avail := avail; buf.size := size; buf.adr := адресОт(mem[offset]); buf.next := buf.adr;
кон Init;

(** read byte from (input) buffer **)
проц Read* (перем buf: Buffer; перем ch: симв8);
нач
	утв(buf.avail > 0, 100);
	НИЗКОУР.прочтиОбъектПоАдресу(buf.next, ch);
	увел(buf.next); умень(buf.avail); увел(buf.totalIn)
кон Read;

(** read len bytes from (input) buffer **)
проц ReadBytes* (перем buf: Buffer; перем dst: массив из симв8; offset, len: размерМЗ);
нач
	утв((0 <= offset) и (0 < len) и (offset + len <= длинаМассива(dst)) и (len <= buf.avail), 100);
	НИЗКОУР.копируйПамять(buf.next, адресОт(dst[offset]), len);
	 увел(buf.next, len); умень(buf.avail, len); увел(buf.totalIn, len)
кон ReadBytes;

(** write byte into (output) buffer **)
проц Write* (перем buf: Buffer; ch: симв8);
нач
	утв(buf.avail > 0, 100);
	НИЗКОУР.запишиОбъектПоАдресу(buf.next, ch);
	увел(buf.next); умень(buf.avail); увел(buf.totalOut)
кон Write;

(** write len bytes into (output) buffer **)
проц WriteBytes* (перем buf: Buffer; конст src: массив из симв8; offset, len: размерМЗ);
нач
	утв((0 <= offset) и (0 < len) и (offset + len <= длинаМассива(src)) и (len <= buf.avail), 100);
	НИЗКОУР.копируйПамять(адресОт(src[offset]), buf.next, len);
	увел(buf.next, len); умень(buf.avail, len); увел(buf.totalOut, len)
кон WriteBytes;

(** rewind previously empty input buffer to first position after it has been filled with new input **)
проц Rewind* (перем buf: Buffer; avail: размерМЗ);
нач
	утв(buf.avail = 0, 100);
	утв((0 <= avail) и (avail <= buf.size), 101);
	buf.next := buf.adr; buf.avail := avail
кон Rewind;

(** move position of next read for -offset bytes **)
проц Reread* (перем buf: Buffer; offset: размерМЗ);
нач
	утв((0 <= offset) и (buf.avail + offset <= buf.size), 101);
	умень(buf.next, offset); увел(buf.avail, offset)
кон Reread;

(** restart writing at starting position of output buffer after it has been emptied **)
проц Rewrite* (перем buf: Buffer);
нач
	buf.next := buf.adr; buf.avail := buf.size
кон Rewrite;

(** fill input buffer with new bytes to consume **)
проц Fill* (перем buf: Buffer; конст src: массив из симв8; offset, size: размерМЗ);
нач
	утв((0 <= offset) и (0 < size) и (offset + size <= длинаМассива(src)), 100);
	утв(buf.avail + size <= buf.size, 101);
	если buf.avail # 0 то
		НИЗКОУР.копируйПамять(buf.next, buf.adr, buf.avail)
	всё;
	buf.next := buf.adr + buf.avail;
	НИЗКОУР.копируйПамять(адресОт(src[offset]), buf.next, size);
	увел(buf.avail, size)
кон Fill;

(** extract bytes from output buffer to make room for new bytes **)
проц Drain* (перем buf: Buffer; перем dst: массив из симв8; offset, size: размерМЗ);
перем i, n: размерМЗ; s, d: адресВПамяти; c: симв8;
нач
	утв((0 <= offset) и (0 < size) и (offset + size <= длинаМассива(dst)), 100);
	утв(buf.avail + size <= buf.size, 101);	(* can't consume more than is in buffer *)
	НИЗКОУР.копируйПамять(buf.adr, адресОт(dst[offset]), size);
	(*SYSTEM.MOVE(buf.adr + size, buf.adr, buf.size - buf.avail - size);   overlapping moves don't work on SPARC !*)
	n := buf.size - buf.avail - size;  s := buf.adr + size;  d := buf.adr;
	нцДля i := 0 до n - 1 делай НИЗКОУР.прочтиОбъектПоАдресу( s + i, c );  НИЗКОУР.запишиОбъектПоАдресу( d + i, c ) кц;
	увел(buf.avail, size); умень(buf.next, size);
кон Drain;

кон ZlibBuffers.
