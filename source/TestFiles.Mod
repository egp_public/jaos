модуль TestFiles; (** AUTHOR "staubesv"; PURPOSE "Files  tester"; *)

использует Commands, Options, Random, Strings, TestSuite, Потоки, Files;

конст
	Integer = 0;
	Boolean = 1;
	Char = 2;
	String = 3;

тип

	Tester = окласс (TestSuite.Tester)
	перем
		log: Потоки.Писарь;
		file : Files.File;
		rider : Files.Rider;
		reader : Files.Reader;
		path : массив 512 из симв8;
		lastCommandWasComment : булево;

		readerBytesLength : размерМЗ; (* last results from Reader.Bytes(... VAR len) *)
		byte : симв8; (* last byte read *)
		buffer : Strings.String; (* last bytes read *)

		проц &InitTester *(log: Потоки.Писарь; конст path : массив из симв8);
		нач
			утв(log # НУЛЬ);
			сам.log := log;
			копируйСтрокуДо0(path, сам.path);
			Strings.TrimWS(сам.path);
			Reset;
		кон InitTester;

		проц Reset;
		нач
			file := НУЛЬ;
			rider.eof := ложь;
			rider.res := 0;
			rider.apos := 0;
			rider.bpos := 0;
			rider.hint := НУЛЬ;
			rider.file := НУЛЬ;
			rider.fs := НУЛЬ;
			reader := НУЛЬ;
			readerBytesLength := 0;
			byte := 0X;
			buffer := НУЛЬ;
			lastCommandWasComment := ложь;
		кон Reset;

		проц OpenFile(конст filename : массив из симв8; перем error : булево);
		перем fullname : Files.FileName;
		нач
			копируйСтрокуДо0(path, fullname); Strings.Append(fullname, filename);
			log.пСтроку8("Files.Old '"); log.пСтроку8(fullname); log.пСтроку8("' ... "); log.ПротолкниБуферВПоток;
			file := Files.Old(fullname);
			если (file # НУЛЬ) то
				Files.OpenReader(reader, file, 0);
				log.пСтроку8("Ok");
			иначе
				error := истина;
				log.пСтроку8("Failed");
			всё;
		кон OpenFile;

		проц CreateFile(конст filename : массив из симв8; перем error : булево);
		перем fullname : Files.FileName;
		нач
			копируйСтрокуДо0(path, fullname); Strings.Append(fullname, filename);
			log.пСтроку8("Files.New '"); log.пСтроку8(fullname); log.пСтроку8("' ... "); log.ПротолкниБуферВПоток;
			file := Files.New(fullname);
			если (file # НУЛЬ) то
				Files.OpenReader(reader, file, 0);
				log.пСтроку8("Ok");
			иначе
				error := истина;
				log.пСтроку8("Failed.");
			всё;
		кон CreateFile;

		проц DeleteFile(конст filename : массив из симв8; перем error : булево);
		перем fullname : Files.FileName; res : целМЗ;
		нач
			копируйСтрокуДо0(path, fullname); Strings.Append(fullname, filename);
			log.пСтроку8("DELETE '"); log.пСтроку8(fullname); log.пСтроку8("' ... "); log.ПротолкниБуферВПоток;
			Files.Delete(fullname, res);
			если (res = Files.Ok) то
				log.пСтроку8("Ok");
			иначе
				error := истина;
				log.пСтроку8("File not found");
			всё;
		кон DeleteFile;

		проц RegisterFile(перем error : булево);
		нач
			log.пСтроку8("Register current file ... "); log.ПротолкниБуферВПоток;
			если CheckOpen(error) то
				Files.Register(file);
				log.пСтроку8("Ok");
			иначе
				log.пСтроку8("Failed");
			всё;
		кон RegisterFile;

		проц CheckOpen(перем error : булево) : булево;
		нач
			если (file = НУЛЬ) то
				log.пСтроку8("Error: no open file");
				error := истина;
			иначе
				error := ложь;
			всё;
			возврат ~error;
		кон CheckOpen;

		проц FileSet(position : цел32; перем error : булево);
		нач
			log.пСтроку8("File.Pos := "); log.пЦел64(position, 0); log.пСтроку8(" ... "); log.ПротолкниБуферВПоток;
			если CheckOpen(error) то
				file.Set(rider, position);
				log.пСтроку8("Ok");
			всё;
		кон FileSet;

		проц GetInteger(r : Потоки.Чтец; перем integer : цел32) : булево;
		нач
			r.ПропустиБелоеПоле; r.чЦел32(integer, ложь);
			если (r.кодВозвратаПоследнейОперации # Потоки.Успех) то
				log.пВК_ПС; log.пСтроку8("Parse error: Expected integer value, pos = "); log.пЦел64(r.МестоВПотоке(), 0); log.пВК_ПС;
			всё;
			возврат (r.кодВозвратаПоследнейОперации = Потоки.Успех);
		кон GetInteger;

		проц GetString(r : Потоки.Чтец; перем string : Strings.String) : булево;
		перем value : массив 1024 из симв8;
		нач
			r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(value);
			если (r.кодВозвратаПоследнейОперации # Потоки.Успех) то
				string := НУЛЬ;
				log.пВК_ПС; log.пСтроку8("Parse error: Expected string value, pos = "); log.пЦел64(r.МестоВПотоке(), 0); log.пВК_ПС;
			иначе
				string := Strings.NewString(value);
			всё;
			возврат (r.кодВозвратаПоследнейОперации = Потоки.Успех);
		кон GetString;

		проц GetBoolean(r : Потоки.Чтец; перем boolean : булево) : булево;
		перем string : массив 8 из симв8;
		нач
			r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string);
			если (string = "TRUE") то boolean := истина;
			аесли (string = "FALSE") то boolean := ложь;
			иначе
				log.пВК_ПС; log.пСтроку8("Parse error : Expected boolean value, pos = "); log.пЦел64(r.МестоВПотоке(), 0); log.пВК_ПС;
			всё;
			возврат (string = "TRUE") или (string = "FALSE");
		кон GetBoolean;

		проц GetChar(r : Потоки.Чтец; перем char : симв8) : булево;
		нач
			r.ПропустиБелоеПоле; r.чСимв8(char);
			если (r.кодВозвратаПоследнейОперации # Потоки.Успех) то
				log.пВК_ПС; log.пСтроку8("Parse error: Expected character value, pos = "); log.пЦел64(r.МестоВПотоке(), 0); log.пВК_ПС;
			всё;
			возврат (r.кодВозвратаПоследнейОперации = Потоки.Успех);
		кон GetChar;

		проц GetIndex(конст string : массив из симв8; перем index : цел32) : булево;
		перем variable : массив 64 из симв8;
		нач
			утв(Strings.Match("buffer*", string));
			копируйСтрокуДо0(string, variable);
			Strings.Delete(variable, 0, 6);
			если (variable[0] = "[") и (variable[Strings.Length(variable)-1] = "]") то
				Strings.Delete(variable, 0, 1);
				variable[Strings.Length(variable)-1] := 0X;
				если IsNumber(variable) то
					Strings.StrToInt(variable, index);
					возврат истина;
				иначе
					log.пСтроку8("Parse error: Expected integer as index in "); log.пСтроку8(string);
				всё;
			иначе
				log.пСтроку8("Parse error: Expected index brackets [] in "); log.пСтроку8(string);
			всё;
			возврат ложь;
		кон GetIndex;

		проц Assert(r : Потоки.Чтец; перем error, parseError : булево);
		перем variable : массив 32 из симв8; index : цел32;

			проц CheckInteger(конст name : массив из симв8;  variable : цел64; r : Потоки.Чтец; перем error, parseError : булево);
			перем operator : массив 4 из симв8; value : цел32;
			нач
				r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(operator);
				если GetInteger(r, value) то
					если (operator = "=") то error := variable # value;
					аесли (operator = "#") то error := variable = value;
					аесли (operator = "<") то error := variable >= value;
					аесли (operator = "<=") то error := variable > value;
					аесли (operator = ">") то error := variable <= value;
					аесли (operator = ">=") то error := variable < value;
					иначе
						parseError := истина;
						log.пСтроку8("Parse error: Unsupported integer operator: "); log.пСтроку8(operator);
						log.пСтроку8(", pos = "); log.пЦел64(r.МестоВПотоке(), 0);
					всё;
				иначе
					parseError := истина;
				всё;
				если ~parseError то
					log.пСтроку8("ASSERT "); log.пСтроку8(name); log.пСимв8(" "); log.пСтроку8(operator); log.пСимв8(" ");  log.пЦел64(value, 0); log.пСтроку8(" ... ");
					если ~error то log.пСтроку8("Ok"); иначе log.пСтроку8("Failed ("); log.пЦел64(variable, 0); log.пСтроку8(")");  всё;
				всё;
			кон CheckInteger;

			проц CheckBoolean(конст name : массив из симв8; variable : булево; r : Потоки.Чтец; перем error, parseError : булево);
			перем operator : массив 4из симв8; value : булево;
			нач
				r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(operator);
				если GetBoolean(r, value) то
					если (operator = "=") то
						error := variable # value;
					аесли (operator = "#") то
						error := variable = value;
					иначе
						parseError := истина;
						log.пСтроку8("Parse error: Unsupported boolean operator: "); log.пСтроку8(operator);
						log.пСтроку8(", pos = "); log.пЦел64(r.МестоВПотоке(), 0);
					всё;
				иначе
					parseError := истина;
				всё;
				если ~parseError то
					log.пСтроку8("ASSERT "); log.пСтроку8(name); log.пСимв8(" "); log.пСтроку8(operator); log.пСимв8(" ");
					если value то log.пСтроку8("TRUE"); иначе log.пСтроку8("FALSE"); всё; log.пСтроку8(" ... ");
					если ~error то log.пСтроку8("Ok"); иначе log.пСтроку8("Failed"); всё;
				всё;
			кон CheckBoolean;

			проц CheckChar(конст name : массив из симв8; variable : симв8; r : Потоки.Чтец; перем error, parseError : булево);
			перем operator : массив 4 из симв8; value : симв8;
			нач
				r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(operator);
				если GetChar(r, value) то
					если (operator = "=") то error := variable # value;
					аесли (operator = "#") то error := variable = value;
					иначе
						parseError := истина;
						log.пСтроку8("Parse error: Unsupported character operator: "); log.пСтроку8(operator);
					всё;
				иначе
					parseError := истина;
				всё;
				если ~parseError то
					log.пСтроку8("ASSERT "); log.пСтроку8(name); log.пСимв8(" "); log.пСтроку8(operator); log.пСимв8(" ");
					log.пЦел64(кодСимв8(value), 0); log.пСтроку8(" ... ");
					если ~error то log.пСтроку8("Ok"); иначе log.пСтроку8("Failed ("); log.пЦел64(кодСимв8(variable), 0); log.пСтроку8(")"); всё;
				всё;
			кон CheckChar;

			проц CheckString(конст name : массив из симв8; конст variable : Strings.String; r : Потоки.Чтец; перем error, parseError : булево);
			перем operator : массив 4 из симв8; value : Strings.String; i : цел32;
			нач
				r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(operator);
				если GetString(r, value) то
					утв(value # НУЛЬ);
					если (operator = "=") то
						error := ~StringsAreEqual(value, buffer);
					аесли (operator = "#") то
						error := StringsAreEqual(value, buffer);
					иначе
						parseError := истина;
						log.пСтроку8("Parse error: Unsupported string operator: "); log.пСтроку8(operator);
					всё;
				иначе
					parseError := истина;
				всё;
				если ~parseError то
					log.пСтроку8("ASSERT "); log.пСтроку8(name); log.пСимв8(" "); log.пСтроку8(operator); log.пСимв8(" ");
					i := 0;
					нцПока (i < длинаМассива(value)) и (i < 20) и (value[i] # 0X) делай log.пСимв8(value[i]); увел(i); кц;
					log.пСтроку8(" ... ");
					если ~error то log.пСтроку8("Ok"); иначе log.пСтроку8("Failed"); всё;
				всё;
			кон CheckString;

		нач
			variable := "";
			r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(variable);
			если (variable # "") то
				если (variable = "rider.res") то
					CheckInteger(variable, rider.res, r, error, parseError);
				аесли (variable = "rider.eof") то
					CheckBoolean(variable, rider.eof, r, error, parseError);
				аесли (variable = "reader.res") то
					CheckInteger(variable, reader.кодВозвратаПоследнейОперации, r, error, parseError);
				аесли (variable = "byte") то
					CheckChar(variable, byte, r, error, parseError);
				аесли (variable = "readerBytesLength") то
					CheckInteger(variable, readerBytesLength, r, error, parseError);
				аесли (variable = "buffer") то
					CheckString(variable, buffer, r, error, parseError);
				аесли Strings.Match("buffer*", variable) то
					если GetIndex(variable, index) то
						если (buffer # НУЛЬ) и (index < длинаМассива(buffer)) то
							CheckChar(variable, buffer[index], r, error, parseError);
						иначе
							error := истина;
						всё;
					иначе
						parseError := истина;
					всё;
				аесли (variable = "File.Length()") и CheckOpen(error) то
					CheckInteger(variable, file.Length(), r, error, parseError);
				аесли (variable = "File.Pos()") и CheckOpen(error) то
					CheckInteger(variable, file.Pos(rider), r, error, parseError);
				аесли (variable = "Reader.Available()") и CheckOpen(error) то
					CheckInteger(variable, reader.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество(), r, error, parseError);
				аесли (variable = "Reader.Pos()") и CheckOpen(error) то
					CheckInteger(variable, reader.МестоВПотоке(), r, error, parseError);
				иначе
					parseError := истина;
					log.пСтроку8("Parse error: Unknown variable "); log.пСтроку8(variable);
					log.пСтроку8(", pos = "); log.пЦел64(r.МестоВПотоке(), 0);
				всё;
			иначе
				parseError := истина;
				log.пСтроку8("Parse error: Expected variable name in ASSERT, pos = "); log.пЦел64(r.МестоВПотоке(), 0);
			всё;
		кон Assert;

		проц Set(r : Потоки.Чтец; перем error, parseError : булево);
		перем
			variable, to : массив 32 из симв8;
			charValue : симв8; integerValue : цел32;  booleanValue : булево; stringValue : Strings.String;
			set, index : цел32; i: размерМЗ;

		нач
			r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(variable);
			если (variable = "rider.res") или (variable = "rider.eof") или (variable = "byte") или (variable = "readerBytesLength") или
				(variable = "buffer") или Strings.Match("buffer*", variable) то
				r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(to);
				если (to = "TO") то
					если (variable = "rider.res") то
						set := Integer;
						если GetInteger(r, integerValue) то rider.res := integerValue; иначе parseError := истина; всё;
					аесли (variable = "rider.eof") то
						set := Boolean;
						если GetBoolean(r, booleanValue) то rider.eof := booleanValue; иначе parseError := истина; всё;
					аесли (variable = "reader.res") то
						set := Integer;
						если GetInteger(r, integerValue) то reader.кодВозвратаПоследнейОперации := integerValue; иначе parseError := истина; всё;
					аесли (variable = "byte") то
						set := Char;
						если GetChar(r, charValue) то byte := charValue; иначе parseError := истина; всё;
					аесли (variable = "readerBytesLength") то
						set := Integer;
						если GetInteger(r, integerValue) то readerBytesLength := integerValue; иначе parseError := истина; всё;
					аесли (variable = "buffer") то
						set := String;
						если GetString(r, stringValue) то
							утв((stringValue # НУЛЬ) и (stringValue[длинаМассива(stringValue)-1] = 0X));
							если (buffer # НУЛЬ) и (длинаМассива(buffer) >= длинаМассива(stringValue)-1) то
								нцДля i := 0 до длинаМассива(stringValue)-2 делай (* don-t include 0X *)
									buffer[i] := stringValue[i];
								кц;
							иначе
								error := истина;
							всё;
						иначе
							parseError := истина;
						всё;
					аесли Strings.Match("buffer*", variable) то
						если GetIndex(variable, index) то
							если (buffer # НУЛЬ) и (index < длинаМассива(buffer)) то
								если GetChar(r, charValue) то
									buffer[index] := charValue;
								иначе
									parseError := истина;
								всё;
							иначе
								error := истина;
							всё;
						иначе
							parseError := истина;
						всё;
					всё;
					если ~parseError то
						log.пСтроку8("SET "); log.пСтроку8(variable); log.пСтроку8(" to ");
						если (set = Boolean) то
							если booleanValue то log.пСтроку8("TRUE"); иначе log.пСтроку8("FALSE"); всё;
						аесли (set = Integer) то
							log.пЦел64(integerValue, 0);
						аесли (set = Char) то
							log.пЦел64(кодСимв8(charValue), 0);
						аесли (set = String) то
							если (stringValue # НУЛЬ) то
								i := 0;
								нцПока (i < длинаМассива(stringValue)) и (i < 20) и (stringValue[i] # 0X) делай
									log.пСимв8(stringValue[i]); увел(i);
								кц;
								если (i < длинаМассива(stringValue)) и (stringValue[i] # 0X) то
									log.пСтроку8(" ... ");
								всё;
							иначе
								log.пСтроку8("NIL");
							всё;
						всё;
						если ~error то log.пСтроку8(" ... Ok"); иначе log.пСтроку8(" ... Failed"); всё;
					всё;
				иначе
					parseError := истина;
					log.пСтроку8("Parse error: Expected TO, pos = "); log.пЦел64(r.МестоВПотоке(), 0);
				всё;
			иначе
				parseError := истина;
				log.пСтроку8("Parse error: Unknown variable in SET: "); log.пСтроку8(variable); log.пЦел64(r.МестоВПотоке(), 0);
			всё;
		кон Set;

		проц FileReadByte(перем error : булево);
		нач
			если CheckOpen(error) то
				log.пСтроку8("File.Read ... "); log.ПротолкниБуферВПоток;
				file.Read(rider, byte);
				log.пСтроку8("value="); log.пЦел64(кодСимв8(byte), 0); log.пСтроку8(", Ok");
			всё;
		кон FileReadByte;

		проц FileWriteByte(перем error : булево);
		нач
			если CheckOpen(error) то
				log.пСтроку8("File.Write: "); log.пЦел64(кодСимв8(byte), 0); log.пСтроку8(" ... "); log.ПротолкниБуферВПоток;
				file.Write(rider, byte);
				log.пСтроку8("Ok");
			всё;
		кон FileWriteByte;

		проц FileReadBytes(offset, length : цел32; перем error : булево);
		нач
			если CheckOpen(error) то
				log.пСтроку8("File.ReadBytes "); log.пЦел64(offset, 0); log.пСтроку8(" "); log.пЦел64(length, 0); log.пСтроку8(" ... "); log.ПротолкниБуферВПоток;
				file.ReadBytes(rider, buffer^, offset, length);
				log.пСтроку8("Ok");
			всё;
		кон FileReadBytes;

		проц FileWriteBytes(offset, length : цел32; перем error : булево);
		нач
			если CheckOpen(error) то
				log.пСтроку8("File.WriteBytes "); log.пЦел64(offset, 0); log.пСтроку8(" "); log.пЦел64(length, 0); log.пСтроку8(" ... "); log.ПротолкниБуферВПоток;
				file.WriteBytes(rider, buffer^, offset, length);
				log.пСтроку8("Ok");
			всё;
		кон FileWriteBytes;

		проц ReaderGet(перем error : булево);
		нач
			если CheckOpen(error) то
				log.пСтроку8("Reader.Get ... "); log.ПротолкниБуферВПоток;
				byte := reader.чИДайСимв8();
				log.пСтроку8("value="); log.пЦел64(кодСимв8(byte), 0); log.пСтроку8(", Ok");
			всё;
		кон ReaderGet;

		проц ReaderBytes(offset, length : цел32; перем error : булево);
		нач
			если CheckOpen(error) то
				log.пСтроку8("Reader.Bytes "); log.пЦел64(offset, 0); log.пСтроку8(" "); log.пЦел64(length, 0); log.пСтроку8("... "); log.ПротолкниБуферВПоток;
				reader.чБайты(buffer^, offset, length, readerBytesLength);
				log.пЦел64(readerBytesLength, 0); log.пСтроку8(" bytes read, Ok");
			всё;
		кон ReaderBytes;

		проц ReaderSetPos(position : цел32; перем error : булево);
		нач
			если CheckOpen(error) то
				log.пСтроку8("Reader.SetPos "); log.пЦел64(position, 0); log.пСтроку8(" ... "); log.ПротолкниБуферВПоток;
				reader.ПерейдиКМестуВПотоке(position);
				log.пСтроку8("Ok");
			всё;
		кон ReaderSetPos;

		проц InitBuffer(length : цел32);

			проц Clear;
			перем i : размерМЗ;
			нач
				нцДля i := 0 до длинаМассива(buffer)-1 делай
					buffer[i] := 0X;
				кц;
			кон Clear;

		нач
			log.пСтроку8("INITBUFFER "); log.пЦел64(length, 0); log.пСтроку8(" ... "); log.ПротолкниБуферВПоток;
			если (length = 0) то
				buffer := НУЛЬ;
			аесли (buffer # НУЛЬ) и (длинаМассива(buffer) = length) то
				Clear;
			иначе
				нов(buffer, length);
				Clear;
			всё;
			log.пСтроку8("Ok");
		кон InitBuffer;

		проц CallCommand(r : Потоки.Чтец; перем error, parseError : булево);
		перем
			context : Commands.Context;
			arg : Потоки.ЧтецИзСтроки;
			commandStr : массив 128 из симв8; parameterStr : Strings.String;
			msg : массив 128 из симв8;
			pos : размерМЗ; res : целМЗ;
		нач
			нов(parameterStr, 4096); Strings.Truncate(parameterStr^, 0);
			r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(commandStr);
			r.чСтроку8ДоКонцаСтрокиТекстаВключительно(parameterStr^);
			Strings.TrimWS(parameterStr^);
			pos := Strings.Pos("<path>", parameterStr^);
			нцПока (pos >= 0) делай
				Strings.Delete(parameterStr^, pos, Strings.Length("<path>"));
				Strings.Insert(path, parameterStr^, pos);
				pos := Strings.Pos("<path>", parameterStr^);
			кц;

			log.пСтроку8("CALL '"); log.пСтроку8(parameterStr^); log.пСтроку8("' ... "); log.ПротолкниБуферВПоток;
			если (commandStr # "") то
				нов(arg, Strings.Length(parameterStr^));
				arg.ПримиСрезСтроки8ВСтрокуДляЧтения(parameterStr^, 0, Strings.Length(parameterStr^));
				нов(context, НУЛЬ, arg, log, log, сам);
				Commands.Activate(commandStr, context, {Commands.Wait}, res, msg);
				если (res = Commands.Ok) то
					log.пСтроку8("Ok");
				иначе
					error := истина;
					log.пСтроку8("Failed, "); log.пСтроку8(msg); log.пСтроку8(", res: "); log.пЦел64(res, 0);
				всё;
			иначе
				parseError := истина;
				log.пСтроку8("Parse error: Expected argument for CALL");
			всё;
		кон CallCommand;

		проц ProcessCommand(r : Потоки.Чтец; перем error, parseError, finished : булево);
		перем
			filename : Files.FileName;
			string : массив 64 из симв8;
			offset, length, position : цел32;
		нач
			error := ложь; parseError := ложь;
			если ~r.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string) то finished := истина; возврат; иначе finished := ложь; всё;
			если ~lastCommandWasComment то log.пСтроку8("    "); иначе lastCommandWasComment := ложь; всё;
			если (string = "Files.Old") то (* Files.Old <filename> *)
				r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename);
				OpenFile(filename, error);
			аесли (string = "Files.New") то (* Files.New <filename> *)
				r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename);
				CreateFile(filename, error);
			аесли (string = "DELETE") то (* DELETE <filename> (does not cause error) *)
				r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename);
				DeleteFile(filename, error); error := ложь;
			аесли (string = "REGISTER") то (* REGISTER *)
				RegisterFile(error);
			аесли (string = "File.Set") и GetInteger(r, position) то (* File.Set <position> *)
				FileSet(position, error);
			аесли (string = "SET") то (* SET variable TO value *)
				Set(r, error, parseError);
			аесли (string = "ASSERT") то (* ASSERT <variable> <operator> <value> *)
				Assert(r, error, parseError);
			аесли (string = "File.Read")  то (* File.Read *)
				FileReadByte(error);
			аесли (string = "File.ReadBytes") и GetInteger(r, offset) и GetInteger(r, length) то (* File.ReadBytes offset length *)
				FileReadBytes(offset, length, error);
			аесли (string = "File.Write") то (* File.Write *)
				FileWriteByte(error);
			аесли (string = "File.WriteBytes") и GetInteger(r, offset) и GetInteger(r, length) то (* File.WriteBytes offset length *)
				FileWriteBytes(offset, length, error);
			аесли (string = "Reader.Get") то (* Reader.Get *)
				ReaderGet(error);
			аесли (string = "Reader.Bytes") и GetInteger(r, offset) и GetInteger(r, length) то (* Reader.Bytes offset length *)
				ReaderBytes(offset, length, error);
			аесли (string = "Reader.SetPos") и GetInteger(r, position) то (* Reader.SetPosition position *)
				ReaderSetPos(position, error);
			аесли (string = "INITBUFFER") и GetInteger(r, length) то (* INITBUFFER length *)
				InitBuffer(length);
			аесли (string = "CALL") то
				CallCommand(r, error, parseError);
			аесли (string[0] = "#") то (* comment *)
				lastCommandWasComment := истина;
				r.ПропустиДоКонцаСтрокиТекстаВключительно;
			иначе
				log.пВК_ПС; log.пСтроку8("Parse Error: Expected command, but found: "); log.пСтроку8(string); log.пСтроку8(", pos = "); log.пЦел64(r.МестоВПотоке(), 0); log.пВК_ПС;
				parseError := истина;
			всё;
			если ~parseError и (string[0] # "#") то log.пВК_ПС; log.ПротолкниБуферВПоток; всё;
		кон ProcessCommand;

		проц {перекрыта}Handle*(r: Потоки.Чтец; pos : Потоки.ТипМестоВПотоке; конст name: массив из симв8; testType: TestSuite.TestType): целМЗ;
		перем result: цел16; error, parseError, finished, trapped: булево;
		нач
			trapped := истина;
			Reset;
			error := ложь; parseError := ложь;
			result := TestSuite.Failure;
			log.пСтроку8 ("testing: "); log.пСтроку8 (name); log.ПротолкниБуферВПоток; log.пВК_ПС;
			нцДо
				ProcessCommand(r, error, parseError, finished);
				log.ПротолкниБуферВПоток;
			кцПри (error или parseError или finished);

			trapped := ложь;
			если parseError то result := TestSuite.Failure;
			аесли error  то result := TestSuite.Negative;
			иначе result := TestSuite.Positive;
			всё;
		выходя
			если trapped то log.пСтроку8("TRAP"); всё;
			log.пВК_ПС; log.ПротолкниБуферВПоток;
			возврат result;
		кон Handle;

	кон Tester;

(* string is 0X terminated, buffer is NOT 0X terminated *)
проц StringsAreEqual(string, buffer : Strings.String) : булево;
перем i : цел32;
нач
	если (string # НУЛЬ) и (buffer # НУЛЬ) и (string[длинаМассива(string)-1] = 0X) и (длинаМассива(string) = длинаМассива(buffer) + 1) то
		i := 0;
		нцПока (i < длинаМассива(string) - 1) и (string[i] = buffer[i]) делай увел(i); кц;
		возврат (i >= длинаМассива(string) - 1);
	всё;
	возврат ложь;
кон StringsAreEqual;

проц IsNumber(конст string : массив из симв8) : булево;
перем i : размерМЗ;
нач
	если ("0" <= string[0]) и (string[0] <= "9") то
		нцДля i := 1 до Strings.Length(string)-1 делай
			если (string[i] < "0") и ("9" < string[i]) то
				возврат ложь;
			всё;
		кц;
		возврат истина;
	всё;
	возврат ложь;
кон IsNumber;

проц Test*(context : Commands.Context); (** [Options] TestFile(input) [TestResultFile] ~ *)
перем options : Options.Options; tester: Tester; report: TestSuite.StreamReport; path : Files.FileName;
нач
	нов(options);
	options.Add("p", "path", Options.String);
	если options.Parse(context.arg, context.error) то
		если ~options.GetString("path", path) то path := ""; всё;
		нов(tester, context.out, path);
		нов(report, context.out);
		TestSuite.Drive(context, tester);
		tester.Print(report);
	всё;
кон Test;

проц RandomReadWrite*(context : Commands.Context); (** path filename seed nofRuns ~ *)
перем
	path, filename, fullname : Files.FileName;
	seed, run, nofRuns, position, lastPosition, temp, lastPercent : цел32;
	length0: Files.Size;
	char, lastChar, ch : симв8;
	file : Files.File; rider : Files.Rider;
	random : Random.Generator;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(path);
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename);
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(seed, ложь);
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(nofRuns, ложь);
	копируйСтрокуДо0(path, fullname); Strings.Append(fullname, filename);
	context.out.пСтроку8("    RandomReadWrite on file '"); context.out.пСтроку8(fullname); context.out.пСтроку8("' (");
	context.out.пЦел64(nofRuns, 0); context.out.пСтроку8(" runs)' ... ");
	context.out.ПротолкниБуферВПоток;
	file := Files.Old(fullname);
	если (file # НУЛЬ) то
		нов(random);
		random.InitSeed(seed);
		length0 := file.Length();
		run := 0;
		lastChar := 0X; lastPosition := 0; lastPercent := 0;
		context.out.пСтроку8("0% "); context.out.ПротолкниБуферВПоток;
		нцПока (run <= nofRuns) делай
			если (100 * run DIV nofRuns >= lastPercent + 10) то
				lastPercent := lastPercent + 10;
				context.out.пЦел64(lastPercent, 0); context.out.пСтроку8("% "); context.out.ПротолкниБуферВПоток;
			всё;
			position := random.Dice(цел32(length0));
			temp := random.Dice(255) + 1;
			char := симв8ИзКода(temp);
			file.Set(rider, position);
			утв(file.Pos(rider) = position);
			file.Write(rider, char);
			утв(rider.res = 0);
			утв(rider.eof = ложь);
			утв(file.Length() = length0);
			если (lastChar # 0X) то
				file.Set(rider, lastPosition);
				утв(file.Pos(rider) = lastPosition);
				file.Read(rider, ch);
				утв(rider.res = 0);
				утв(rider.eof = ложь);
				утв(file.Length() = length0);
				утв(ch = lastChar);
			всё;
			lastChar := char;
			lastPosition := position;
			увел(run);
		кц;
		context.out.пСтроку8("Ok"); context.out.пВК_ПС;
	иначе
		context.out.пСтроку8("Failed (File not found)"); context.out.пВК_ПС; context.out.ПротолкниБуферВПоток;
		СТОП(99);
	всё;
кон RandomReadWrite;

проц CreateTestFiles*(context : Commands.Context); (** [path] ~ *)
перем path : Files.FileName; i : цел32;

	проц CreateFile(конст path, filename : массив из симв8; size : цел32);
	перем file : Files.File; rider : Files.Rider; fullname : Files.FileName;
	нач
		копируйСтрокуДо0(path, fullname); Strings.Append(fullname, filename);
		context.out.пСтроку8("Create test file '"); context.out.пСтроку8(fullname); context.out.пСтроку8("' ... ");
		context.out.ПротолкниБуферВПоток;
		file := Files.New(fullname);
		если (file # НУЛЬ) то
			file.Set(rider, 0);
			нцДля i := 1 до size делай
				если (i остОтДеленияНа 2 = 0) то file.Write(rider, 1X) иначе file.Write(rider, 0FFX); всё;
			кц;
			Files.Register(file);
			context.out.пСтроку8("Ok");
		иначе
			context.out.пСтроку8("Failed");
		всё;
		context.out.пВК_ПС;
	кон CreateFile;

нач
	path := ""; context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(path);
	CreateFile(path, "TestFile0.Bin", 0);
	CreateFile(path, "TestFile4096.Bin", 4096);
	CreateFile(path, "TestFile8192.Bin", 8192);
кон CreateTestFiles;

кон TestFiles.

TestFiles.CreateTestFiles ~

System.Free TestFiles TestSuite~

WMUtilities.Call TestFiles.Test Files.Test ~	 Verbose testing mode
WMUtilities.Call TestFiles.Test Files.Test Files.Test.tmp ~ Regression testing mode

System.DoCommands

	VirtualDisks.InstallRamdisk TEST 163840 ~
	Partitions.WriteMBR TEST#0 OBEMBR.Bin ~

	System.Show AosFS File System ~ System.Ln ~

	Partitions.Create TEST#1 76 9999 ~
	Partitions.Format TEST#1 AosFS ~
	Partitions.Mount TEST#1 AosFS TEST ~

	WMUtilities.Call --blocking TestFiles.Test --path="TEST:" Files.Test ~

	FSTools.Unmount TEST ~

	System.Show FatFS File System ~ System.Ln ~

	Partitions.ChangeType TEST#1 76 12 ~
	Partitions.Format TEST#1 FatFS ~
	Partitions.Mount TEST#1 FatFS TEST ~

	WMUtilities.Call --blocking TestFiles.Test --path="TEST:" Files.Test ~

	FSTools.Unmount TEST ~

	System.Show SSFS File System ~ System.Ln ~

	SSFS.Format TEST#1 ~
	SSFS.Mount TEST TEST#1 ~

	WMUtilities.Call --blocking TestFiles.Test --path="TEST:" Files.Test ~

	SSFS.Unmount TEST ~

	VirtualDisks.Uninstall TEST ~
~~
