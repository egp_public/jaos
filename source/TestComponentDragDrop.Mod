модуль TestComponentDragDrop;	(** AUTHOR "TF"; PURPOSE "Testing Component Drag & Drop"; *)

использует
	ЛогЯдра, Modules, WMStandardComponents,
	WMComponents, WMGraphics, WMDialogs,
	WM := WMWindowManager;

тип
	TestComponent = окласс(WMComponents.VisualComponent)
		проц {перекрыта}PointerDown*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
		нач
			если 0 в keys то
				если StartDrag(НУЛЬ, WMGraphics.LoadImage("Bart.Pict", истина), 0,0,DragAccepted, DragRejected) то ЛогЯдра.пСтроку8("DraggingStarted")
				иначе ЛогЯдра.пСтроку8("Drag could not be started")
				всё;
			иначе
				ToWMCoordinates(x, y, x, y);
				ЛогЯдра.пЦел64(x, 5); ЛогЯдра.пСтроку8(", "); ЛогЯдра.пЦел64(y, 4); ЛогЯдра.пВК_ПС;
				ЛогЯдра.пЦел64(WMDialogs.MessageXY(WMDialogs.TInformation, x, y, "Hello", "This should always be where you clicked the panel", {WMDialogs.ResOk}), 5);
			всё
		кон PointerDown;

		проц {перекрыта}DragOver*(x, y : размерМЗ; dragInfo : WM.DragInfo);
		нач
			ЛогЯдра.пСтроку8("Drag Over @ "); ЛогЯдра.пЦел64(x, 4); ЛогЯдра.пЦел64(y, 4); ЛогЯдра.пВК_ПС
		кон DragOver;

		проц {перекрыта}DragDropped*(x, y : размерМЗ; dragInfo : WM.DragInfo);
		нач
			ЛогЯдра.пСтроку8("Drag dropped @ "); ЛогЯдра.пЦел64(x, 4); ЛогЯдра.пЦел64(y, 4); ЛогЯдра.пВК_ПС;
			если dragInfo.sender = сам то ConfirmDrag(ложь, dragInfo)
			иначе ConfirmDrag(истина, dragInfo)
			всё
		кон DragDropped;

		проц DragAccepted(sender, data : динамическиТипизированныйУкль);
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(DragAccepted, sender, data)
			иначе
				ЛогЯдра.пСтроку8("Accepted"); ЛогЯдра.пВК_ПС
			всё;
		кон DragAccepted;

		проц DragRejected(sender, data : динамическиТипизированныйУкль);
		нач
			если ~IsCallFromSequencer() то sequencer.ScheduleEvent(DragRejected, sender, data)
			иначе
				ЛогЯдра.пСтроку8("Rejected"); ЛогЯдра.пВК_ПС
			всё;
		кон DragRejected;

	кон TestComponent;

	Window = окласс (WMComponents.FormWindow)
	перем
		panel : WMStandardComponents.Panel;
		testComponent : TestComponent;

		проц &New*;
		нач
			(* add a panel *)
			нов(panel); panel.bounds.SetExtents(640, 420); panel.fillColor.Set(WMGraphics.RGBAToColor(255, 255, 255, 255));

			(* add a test component *)
			нов(testComponent); testComponent.alignment.Set(WMComponents.AlignTop);
			 testComponent.bounds.SetExtents(200, 20); testComponent.fillColor.Set(0FFFFH);
			panel.AddContent(testComponent);

			(* add a test component *)
			нов(testComponent); testComponent.alignment.Set(WMComponents.AlignTop);
			testComponent.bounds.SetExtents(200, 20); testComponent.fillColor.Set(0FF00FFH);
			panel.AddContent(testComponent);

			(* create the form window with panel size *)
			Init(panel.bounds.GetWidth(), panel.bounds.GetHeight(), ложь);
			SetContent(panel);

			manager := WM.GetDefaultManager();
			SetTitle(WM.NewString("Drag drop panels"));
			(* open the window *)
			manager.Add(100, 100, сам, {WM.FlagFrame})
		кон New;

		проц {перекрыта}Close*;
		нач
			Close^;
			window := НУЛЬ
		кон Close;

	кон Window;

перем window : Window;

проц Open*;
нач
	если window = НУЛЬ то нов(window)
	иначе ЛогЯдра.пСтроку8("Already open"); ЛогЯдра.пВК_ПС
	всё;
кон Open;

проц Cleanup;
нач
	если window # НУЛЬ то window.Close всё
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон TestComponentDragDrop.

TestComponentDragDrop.Open ~
System.Free TestComponentDragDrop ~

