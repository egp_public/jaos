модуль UsbNetwork; (** AUTHOR "staubesv"; PURPOSE "USB network device driver framework"; *)
(**
 * History:
 *
 *	03.11.2006	First release (staubesv)
 *)

использует
	ЛогЯдра, Usbdi, Commands, Network, Plugins;

конст

	Ok* = 0;
	Error* = 1;
	Unsupported* = 2;

	MinEthernetFrameSize* = 60;
	MaxEthernetFrameSize* = 1514;
	EthernetHeaderSize* = 14;

	Name = "UsbNet#";
	Description = "USB Link Device";

	ModuleName = "UsbNetwork";

	AddressSize = 6;

	RegisterAtNetwork = истина;

тип

	UsbLinkDevice = окласс (Network.LinkDevice)
	перем
		controller : UsbNetworkController;

		проц {перекрыта}Linked(): цел32;
		нач
			возврат controller.linkStatus;
		кон Linked;

		проц {перекрыта}DoSend(dst: Network.LinkAdr; type: цел32; конст l3hdr, l4hdr, data: массив из симв8;  h3len, h4len, dofs, dlen: цел32);
		нач
			controller.SendFrame(dst, type, l3hdr, l4hdr, data, h3len, h4len, dofs, dlen);
		кон DoSend;

		проц {перекрыта}Finalize(connected: булево);
		нач
			controller.Finalize;
			Finalize^(connected);
		кон Finalize;

		проц Diag;
		нач
			Show(" Diagnostics:"); ЛогЯдра.пВК_ПС;
			если controller # НУЛЬ то controller.Diag;
			иначе ЛогЯдра.пСтроку8("No controller available."); ЛогЯдра.пВК_ПС;
			всё;
		кон Diag;

		проц Show*(конст string : массив из симв8);
		нач
			ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(" ("); ЛогЯдра.пСтроку8(desc); ЛогЯдра.пСтроку8("): "); ЛогЯдра.пСтроку8(string);
		кон Show;

	кон UsbLinkDevice;

тип

	UsbNetworkController* = окласс (Usbdi.Driver)
	перем
		bulkInPipe-, bulkOutPipe-, interruptInPipe- : Usbdi.Pipe;

		rxBuffer-, interruptInBuffer- : Usbdi.BufferPtr;

		linkDevice- : UsbLinkDevice;
		linkStatus* : цел32;

		(** Interface to be implemented by actual network controller driver *)

		проц SendFrame*(dst: Network.LinkAdr; type: цел32; конст l3hdr, l4hdr, data: массив из симв8;  h3len, h4len, dofs, dlen: цел32); (* abstract *)
		нач {единолично} СТОП(301); кон SendFrame;

		проц GetLinkAddress*(перем linkAddress : Network.LinkAdr; перем res : целМЗ); (* abstract *)
		нач СТОП(301); кон GetLinkAddress;

		проц SetLinkAddress*(linkAddress : Network.LinkAdr; перем res : целМЗ); (* abstract *)
		нач СТОП(301); кон SetLinkAddress;

		проц HandleInterrupt*(status : Usbdi.Status; actLen : размерМЗ); (* abstract *)
		нач СТОП(301); кон HandleInterrupt;

		проц HandleBulkIn*(status : Usbdi.Status; actLen : размерМЗ); (* abstract *)
		нач СТОП(301); кон HandleBulkIn;

		проц InitController*(перем rxBuffer : Usbdi.BufferPtr) : булево; (* abstract *)
		нач СТОП(301); кон InitController;

		проц Finalize*; (* abstract *)
		нач СТОП(301); кон Finalize;

		проц Diag*;
		нач
			ЛогЯдра.пСтроку8("Diagnostics of "); ЛогЯдра.пСтроку8(name);
			ЛогЯдра.пСтроку8(" ("); ЛогЯдра.пСтроку8(desc); ЛогЯдра.пСтроку8(")"); ЛогЯдра.пВК_ПС;
		кон Diag;

		проц InitLinkDevice() : булево;
		перем name : Plugins.Name; nofDevices, i : цел32; res : целМЗ;
		нач
			нов(linkDevice, Network.TypeEthernet, 1000, AddressSize);

			linkDevice.controller := сам;

			nofDevices := GetNofDevices();
			name := Name;
			i := 0; нцПока name[i] # 0X делай увел(i) кц;
			если nofDevices > 9 то
				name[i] := симв8ИзКода(кодСимв8("A") + nofDevices - 10);
			иначе
				name[i] := симв8ИзКода(кодСимв8("0") + nofDevices);
			всё;
			name[i+1] := 0X;
			linkDevice.SetName(name);
			linkDevice.desc := Description;

			(* Set ethernet broadcast address: FF-FF-FF-FF-FF-FF *)
			нцДля i := 0 до 5 делай linkDevice.broadcast[i] := 0FFX; кц;

			GetLinkAddress(linkDevice.local, res);
			если res # Ok то
				Show("Could not get link address, res: "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;
				возврат ложь;
			всё;

			если ~RegisterAtNetwork то возврат истина; всё;

			Network.registry.Add(linkDevice, res);
			утв(res = Plugins.Ok);
			IncNofDevices;

			возврат истина;
		кон InitLinkDevice;

		проц GetPipes(перем bulkInPipe, bulkOutPipe, interruptInPipe : Usbdi.Pipe);
		перем i : размерМЗ; bulkInEndpoint, bulkOutEndpoint, interruptInEndpoint : цел32;
		нач
			нцДля i := 0 до длинаМассива(interface.endpoints)-1 делай
				если interface.endpoints[i].type = Usbdi.BulkIn то
					bulkInEndpoint := interface.endpoints[i].bEndpointAddress;
				аесли interface.endpoints[i].type = Usbdi.BulkOut то
					bulkOutEndpoint := interface.endpoints[i].bEndpointAddress;
				аесли interface.endpoints[i].type = Usbdi.InterruptIn то
					interruptInEndpoint := interface.endpoints[i].bEndpointAddress;
				всё;
			кц;
			если bulkInEndpoint # 0 то bulkInPipe := device.GetPipe(bulkInEndpoint); всё;
			если bulkOutEndpoint # 0 то bulkOutPipe := device.GetPipe(bulkOutEndpoint); всё;
			если interruptInEndpoint # 0 то interruptInPipe := device.GetPipe(interruptInEndpoint); всё;
		кон GetPipes;

		проц {перекрыта}Connect*() : булево;
		перем status : Usbdi.Status;
		нач
			linkStatus := Network.LinkUnknown;

			GetPipes(bulkInPipe, bulkOutPipe, interruptInPipe);

			если (bulkInPipe = НУЛЬ) или (bulkOutPipe = НУЛЬ) или (interruptInPipe = НУЛЬ) то
				Show("Device endpoints not found."); ЛогЯдра.пВК_ПС;
				возврат ложь;
			всё;

			если ~InitController(rxBuffer) то
				Show("Controller initialization failed."); ЛогЯдра.пВК_ПС;
				возврат ложь;
			всё;

			bulkInPipe.SetTimeout(0);
			bulkInPipe.SetCompletionHandler(HandleBulkIn);

			status := bulkInPipe.Transfer(bulkInPipe.maxPacketSize, 0, rxBuffer^); (* ignore status *)

			(* setup status pipe *)
			нов(interruptInBuffer, interruptInPipe.maxPacketSize);

			interruptInPipe.SetTimeout(0);
			interruptInPipe.SetCompletionHandler(HandleInterrupt);

			status := interruptInPipe.Transfer(interruptInPipe.maxPacketSize, 0, interruptInBuffer^); (* ignore status *)

			если ~InitLinkDevice() то
				Show("Link device initialization failed."); ЛогЯдра.пВК_ПС;
				возврат ложь;
			всё;

			возврат истина;
		кон Connect;

		проц {перекрыта}Disconnect*;
		нач
			если ~RegisterAtNetwork то возврат; всё;
			Network.registry.Remove(linkDevice);
		кон Disconnect;

	кон UsbNetworkController;

перем
	nofDevices : цел32;

(** Show diagnostics of the specified link device *)
проц Diag*(context : Commands.Context); (** linkdevice ~ *)
перем plugin : Plugins.Plugin; name : массив 128 из симв8;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name);
	plugin := Network.registry.Get(name);
	если plugin # НУЛЬ то
		если plugin суть UsbLinkDevice то
			plugin(UsbLinkDevice).Diag;
		иначе
			context.out.пСтроку8("Link device "); context.out.пСтроку8(name); context.out.пСтроку8(" is not a USB link device."); context.out.пВК_ПС;
		всё;
	иначе
		context.out.пСтроку8("Link device "); context.out.пСтроку8(name); context.out.пСтроку8(" not found."); context.out.пВК_ПС;
	всё;
кон Diag;

проц IncNofDevices;
нач {единолично}
	увел(nofDevices);
кон IncNofDevices;

проц GetNofDevices() : цел32;
нач {единолично}
	возврат nofDevices;
кон GetNofDevices;

проц Show(конст string : массив из симв8);
нач
	ЛогЯдра.пСтроку8(ModuleName); ЛогЯдра.пСтроку8(": "); ЛогЯдра.пСтроку8(string);
кон Show;

кон UsbNetwork.

UsbNetwork.Diag UsbNet#0 ~
UsbNetwork.Diag UsbNet#1 ~
UsbNetwork.Diag UsbNet#2 ~

System.Free UsbNetwork ~
