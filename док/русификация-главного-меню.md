Главное меню содержится в модуле [../source/MainMenu.Mod](../source/MainMenu.Mod)

Меню системы описано в файлах [../source/MenuPage00.xml](../source/MenuPage00.xml), MenuPage10.xml и т.п. 

Нетрудно поправить эти файлы и перевести названия пунктов меню (а также добавить свои)

Некоторые пункты меню названы ссылками, например, `::StartMenu:ButtonTitles:STutorial`

Эти пункты находятся в StartMenu.rep, к-рый является tar-архивом и называтеся в терминологии A2 "репозиториям". Они не ищутся полнотекстовым поиском в VSCodium, но Midnight Commander их находит. 

по всей вероятности, выход на StartMenu.rep происходит через название ::StartMenu. 

В репозитории есть файлы названий для разных языков. Для включения русского нужно в конфигурации написать 
```
<Config>

<!-- Localization -->
	<Section name="Local">
		<!-- ISO 936-1 language code -->
		<Setting name="Language" value="ru"/>
```
