модуль WMPerfMonPluginHTTPServer; (** AUTHOR "staubesv"; PURPOSE "Performance Monitor plugin for HTTP server statistics"; *)
(**
 * History:
 *
 *	27.02.2007	First release (staubesv)
 *)

использует
	WMPerfMonPlugins, WebHTTPServer, Modules;

конст
	ModuleName = "WMPerfMonPluginHTTPServer";

тип

	HTTPStats= окласс(WMPerfMonPlugins.Plugin)

		проц {перекрыта}Init*(p : WMPerfMonPlugins.Parameter);
		перем ds : WMPerfMonPlugins.DatasetDescriptor;
		нач
			p.name := "WebHTTPServer";
			p.description := "HTTP server statistics";
			p.modulename := ModuleName;
			p.autoMin := ложь; p.autoMax := истина; p.minDigits := 7;

			нов(ds, 3);
			ds[0].name := "nofRequests";
			ds[1].name := "requestsPerMinute";
			ds[2].name := "nofConnects";
			p.datasetDescriptor := ds;
		кон Init;

		проц {перекрыта}UpdateDataset*;
		нач
			dataset[0] := WebHTTPServer.nofRequests;
			dataset[1] := WebHTTPServer.requestsPerMinute;
			dataset[2] := WebHTTPServer.nofConnects;
		кон UpdateDataset;

	кон HTTPStats;

проц Install*;
кон Install;

проц InitPlugin;
перем par : WMPerfMonPlugins.Parameter; stats : HTTPStats;
нач
	нов(par); нов(stats, par);
кон InitPlugin;

проц Cleanup;
нач
	WMPerfMonPlugins.updater.RemoveByModuleName(ModuleName);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	InitPlugin;
кон WMPerfMonPluginHTTPServer.

WMPerfMonPluginHTTPServer.Install ~   System.Free WMPerfMonPluginHTTPServer ~
