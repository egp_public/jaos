модуль EFI; (** AUTHOR "Matthias Frei"; PURPOSE "Provides (low-level) API to the (Unified) Extensible Firmware Interface"; *)

использует НИЗКОУР;

конст
	(* Status Codes *)
	(* Success *)
	Success* = 0;
	(* Warnings *)
	WarnUnknownGlyph *= 1;
	WarnDeleteFailure *= 2;
	WarnWriteFailure	*= 3;
	WarnBufferTooSmall *= 4;
	(* Errors*)
	Error* = цел32(80000000H); (* == 80000000H *)
	ErrLoadError* = Error + 1;
	ErrInvalidParameter* = Error + 2;
	ErrUnsupported* = Error + 3;
	ErrBadBufferSize* = Error + 4;
	ErrBufferTooSmall* = Error + 5;
	(* ... TODO ... *)
	ErrNotFound* = Error + 14;
	(* ... TODO ... *)
	ErrEndOfFile* = Error + 31;
	ErrInvalidLanguage* = Error + 32;

	(* BSAllocatePages allocation types *)
	AllocateAnyPage* = 0;
	AllocateMaxAddress* = 1;
	AllocateAddress* = 2;
	MaxAllocateType* = 3;

	(* Memory Types (for BSAllocatePages and MemoryDescriptor.Type)*)
	MTReserved* = 0;
	MTLoaderCode* = 1;
	MTLoaderData* = 2;
	MTBootServicesCode* = 3;
	MTBootServicesData* = 4;
	MTRuntimeServicesCode* = 5;
	MTRuntimeServicesData* = 6;
	MTConventionalMemory*= 7;
	MTUnusableMemory* = 8;
	MTACPIReclaimMemory* = 9;
	MTACPIMemoryNVSM* = 10;
	MTMemoryMappedIO* = 11;
	MTMemoryMappedIOPortSpace* = 12;
	MTPalCode* = 13;
	MTMaxMemoryType* = 14;

	(* Page Size in bytes *)
	PageSize* = 1000H; (* = 4KB *)

	(* BSLocateHandle search types *)
	AllHandles* = 0; (* the 'Protocol' and 'SearchLey' argument are ignored and all handles in the system are returned *)
	ByRegistryNotify* = 1; (* 'SearchKey' supplies the Registration value returned by RegisterProtocolNotify(). 'Protocol' is ignored *)
	ByProtocol* =2; (* return all handles that support 'Protocol' the 'SearchKey' argument is ignored *)

	(* Task Priority Levels *)
	ApplicationTPL* = 4;
	CallbackTPL* = 8;
	NotifiyTPL* = 16;
	HighLevelTPL* = 31;

(* various type definitions *)
тип Char8* = цел8;
тип Char16* = цел16;
тип Boolean* = булево;

тип Int* = размерМЗ; (* size of Int architecture dependant (e.g. 32bit on i386, 64bit on ia64) *)
тип Int8* = цел8; (* Int8, Int16, Int32, Int64 NOT architecture dependant *)
тип Int16* = цел16;
тип Int32* = цел32;
тип Int64* = цел64;
тип Address*= адресВПамяти;

тип Status* = Int;
тип TPL* = Int;
тип LBA* = Int64;
тип Pointer* = Address;
тип VirtualAddress* = Int64; (* sic! *)
тип PhysicalAddress* = Int64; (* sic! *)
тип Handle* = Pointer;
тип Event* = Pointer;

тип Protocol* = укль на ProtocolDescription;
тип ProtocolDescription* = запись кон;

тип GUID* = запись
	Data1*: Int32;
	Data2*: Int16;
	Data3*: Int16;
	Data4*: массив 8 из Int8;
кон;

тип
	MemoryDescriptorPointer*= укль на MemoryDescriptor;
	MemoryDescriptor* = запись
		Type- : Int32;
		pad-: Int32;
		PhysicalStart-: PhysicalAddress;
		VirtualStart-: VirtualAddress;
	(*	PhysicalStart-: Int64;
		VirtualStart-: Int64;	*)
		NumberOfPages-: Int64;
		Attribute-: Int64;
кон;

тип Time* = запись
	Year-: Int16; (* 1998 - 20XX *)
	Month-: Int8; (* 1 - 12 *)
	Day-: Int8; (* 1 - 31 *)
	Hour-: Int8; (* 0-23 *)
	Minute-: Int8; (* 0 - 59 *)
	Second-: Int8; (* 0 - 59 *)
	Pad1 : Int8;
	Nanosecond-: Int32; (* 0 - 999,999,999 *)
	TimeZone- : Int8; (* -1440 - 1440 or 2047 (= unspecified) *)
	Daylight-: Int8; (* 1 : EFITimeAdjustDaylight, 2 : EFITimeInDaylight *)
	Pad2 : Int8;
кон;

(* efi generic table header *)
тип TableHeader* = запись
	Signature-: Int64;
	Revision-: Int32;
	HeaderSize-: Int32;
	CRC32-: Int32;
	Reserved-: Int32;
кон;

(* main EFI tables *)
тип
SystemTable* = запись
	Hdr-: TableHeader;
	FirmwareVendor-{неОтслСборщиком}: укль на массив из Char16;
	FirmwareRevision-: Int32;
	ConsoleInHandle* : Handle;
	ConIn* {неОтслСборщиком}: укль на SimpleInputInterface;
	ConsoleOutHandle*: Handle;
	ConOut* {неОтслСборщиком}: укль на SimpleTextOutputInterface;
	StandardErrorHandle*: Handle;
	StdErr* {неОтслСборщиком}: укль на SimpleTextOutputInterface;
	RS- {неОтслСборщиком}: укль на RuntimeServices;
	BS* {неОтслСборщиком}: укль на BootServices;
	NumberOfTableEntries-: Int;
	ConfigurationTable-{неОтслСборщиком}: укль на массив 2048 из ConfigurationTableEntry; (* access to ACPI, SMBIOS, ... *)
кон;

(* BootServices function type declarations *)
тип DevicePath* = запись
	Type-: Int8;
	SubType-: Int8;
	Length-: массив 2 из Int8;
кон;
тип BSDummyType* = проц{WINAPI};
(* task priority functions *)
тип BSTPLRaise* = проц{WINAPI}(NewTpl : TPL) : TPL;
тип BSTPLRestore* = проц{WINAPI}(oldTpl : TPL);
(* Memory functions *)
тип BSAllocatePages* = проц{WINAPI}(Type : Int; MemoryType : Int; Pages : Int; перем Memory : PhysicalAddress) : Status;
тип BSFreePages* = проц{WINAPI}(Memory : PhysicalAddress; Pages : Int) : Status;
тип BSGetMemoryMap* = проц{WINAPI}(перем MemoryMapSize : Int; перем MemoryMap : массив из MemoryDescriptor; перем MapKey, DescriptorSize : Int; перем DescriptorVersion : Int32) : Status;
(* protocol handler functions *)
тип BSProtocolInstall* = проц{WINAPI}(перем Handle : Handle; конст Protocol : GUID; InterfaceType : Int; Interface : Protocol) : Status;
тип BSProtocolReinstall* = проц{WINAPI}(Handle : Handle; конст Protocol : GUID; OldInterface, NewInterface : Protocol) : Status;
тип BSProtocolUninstall* = проц{WINAPI}(Handle : Handle; конст Protocol : GUID; Interface : Protocol) : Status;
тип BSProtocolHandle* = проц{WINAPI}(Handle : Handle; конст Protocol : GUID; перем Interface : Protocol) : Status;
тип BSProtocolRegisterNotify* = проц{WINAPI}(конст Protocol : GUID; Event : Event; перем Registration : Pointer) : Status;
тип BSLocateHandle* = проц{WINAPI}(SearchType : Int; конст Protocol : GUID; SearchKey : Pointer; перем BufferSize : Int; перем Buffer : массив из Handle): Status;
тип BSLocateDevicePath* = проц{WINAPI}(конст Protocol : GUID; перем DevicePath : DevicePath; перем Device : Handle) : Status;
тип BSInstallConfigurationTable* = проц{WINAPI}(конст Protocol : GUID; Table : Pointer) : Status;
(* image functions *)
тип BSImageLoad* = проц{WINAPI}(BootPolicy : Boolean; ParentImageHandle : Handle; конст FilePath : DevicePath; SourceSize : Int; SourceBuffer : Pointer; перем ImageHandle : Handle) : Status;
тип BSImageStart* = проц{WINAPI}(ImageHandle : Handle; перем ExitDataSize : Int; перем ExitData : массив из Char16) : Status;
тип BSImageExit* = проц{WINAPI}(ImageHandle : Handle; ExitStatus : Status; ExitDataSize : Int; перем ExitData : массив из Char16) : Status;
тип BSImageUnload* = проц{WINAPI}(ImageHandle : Handle) : Status;
тип BSExitBootServices* = проц{WINAPI}(ImageHandle : Handle; MapKey : Int) : Status;
(* misc. functions *)
тип BSGetNextMonotonicCount* = проц{WINAPI}(перем Count : Int64);
тип BSStall* = проц{WINAPI}(Microseconds : Int);

тип BootServices* = запись
	Hdr- : TableHeader;
	RaiseTPL-: BSTPLRaise;
	RestoreTPL-: BSTPLRestore;
	(* memory functions *)
	AllocatePages-: BSAllocatePages;
	FreePages-: BSFreePages;
	GetMemoryMap-: BSGetMemoryMap;
	AllocatePool-: BSDummyType;
	FreePool-: BSDummyType;
	(* event & timer functions *)
	CreateEvent-: BSDummyType;
	SetTimer-: BSDummyType;
	WaitForEvent-: BSDummyType;
	SignaleEvent-: BSDummyType;
	CloseEvent-: BSDummyType;
	CheckEvent-: BSDummyType;
	(* protocol handler functions *)
	InstallProtocolInterface-: BSProtocolInstall;
	ReinstallProtocolInterface-: BSProtocolReinstall;
	UninstallProtocolInterface-: BSProtocolUninstall;
	HandleProtocol-: BSProtocolHandle;
	PCHandleProtocol-: BSProtocolHandle;
	RegisterProtocolNotify-: BSProtocolRegisterNotify;
	LocateHandle-: BSLocateHandle;
	LocateDevicePath-: BSLocateDevicePath;
	InstallConfigurationTable-: BSDummyType;
	(* image functions *)
	LoadImage-: BSImageLoad;
	StartImage-: BSImageStart;
	Exit-: BSImageExit;
	UnloadImage-: BSImageUnload;
	ExitBootServices-: BSExitBootServices;
	(* misc *)
	GetNextMonotinicCount-: BSGetNextMonotonicCount;
	Stall-: BSStall;
	SetWatchdogTimer-: BSDummyType;
кон;

(* RuntimeService function type declarations *)
тип RSDummyType* = проц{WINAPI};

тип RuntimeServices* = запись
	Hdr- : TableHeader;
	(* time services *)
	GetTime-: RSDummyType;
	SetTime-: RSDummyType;
	GetWakeupTime-: RSDummyType;
	SetWakeupTime-: RSDummyType;
	(* virtual memory services *)
	SetVirtualAddressMap-: RSDummyType;
	ConvertPointer-: RSDummyType;
	(* varible services *)
	GetVariable-: RSDummyType;
	GetNextVariable-: RSDummyType;
	SetVariable-: RSDummyType;
	(* misc *)
	GetNextHighMonotonicCount-: RSDummyType;
	ResetSystem -: RSDummyType;
кон;

тип ConfigurationTableEntry* = запись
	VendorGuid-: GUID;
	VendorTable-: адресВПамяти; (* virtual OR physical address - see EFI spec. Chapter 4.6 *)
кон;

(* basic EFI protocols ( = function tables to communicate with EFI services ) *)

(* Simple Input Interface Protocol *)
тип InputKey* = запись
	ScanCode-: Int16;
	UnicodeChar-: Char16;
кон;
тип InputReset* = проц {WINAPI} (This: укль на SimpleInputInterface; ExtendedVerification: Boolean): Status;
тип InputReadKey* = проц {WINAPI} (This: укль на SimpleInputInterface; перем key: InputKey): Status;

тип SimpleInputInterface* = запись(ProtocolDescription)
	Reset-: InputReset;
	ReadKey-: InputReadKey;
	WaitForEvent- : Event;
кон;

(* Simple Text Output Interface Protocol *)

тип SimpleTextOutputInterfaceMode* = запись
	MaxMode-: Int32;
	(* current settings: *)
	Mode-: Int32;
	Attribute-: Int32;
	CursorColumn-: Int32;
	CursorRow-: Int32;
	CursorVisible-: Boolean;
кон;

тип TextReset* = проц {WINAPI} (This: укль на SimpleTextOutputInterface; ExtendedVerification: Boolean): Status;
тип TextString* = проц {WINAPI} (This: укль на SimpleTextOutputInterface;  конст String: массив из Char16): Status;
тип TextTestString* = проц {WINAPI} (This: укль на SimpleTextOutputInterface; конст String: массив из Char16): Status;
тип TextQueryMode* = проц {WINAPI} (This: укль на SimpleTextOutputInterface; ModeNumber: Int; перем Columns, Rows: Int): Status;
тип TextSetMode* = проц {WINAPI} (This: укль на SimpleTextOutputInterface; ModeNumber: Int): Status;
тип TextSetAttribute* = проц {WINAPI} (This: укль на SimpleTextOutputInterface; Attribute: Int): Status;
тип TextClearScreen* = проц {WINAPI} (This: укль на SimpleTextOutputInterface): Status;
тип TextSetCursorPos* = проц {WINAPI} (This: укль на SimpleTextOutputInterface; Column, Row : Int): Status;
тип TextEnableCursor* = проц {WINAPI} (This : укль на SimpleTextOutputInterface; Visible : Boolean): Status;

тип SimpleTextOutputInterface* = запись(ProtocolDescription)
	Reset-: TextReset;
	OutputString-: TextString;
	TestString-: TextTestString;
	QueryMode-: TextQueryMode;
	SetMode-: TextSetMode;
	SetAttribute-: TextSetAttribute;
	ClearScreen-: TextClearScreen;
	SetCursorPosition-: TextSetAttribute;
	EnableCursor-: TextEnableCursor;
	Mode-{неОтслСборщиком}: укль на SimpleTextOutputInterfaceMode;
кон;

перем
	imageHandle-: Handle;
	table- {неОтслСборщиком}: укль на SystemTable;

кон EFI.
