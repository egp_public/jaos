модуль FoxTextualSymbolFile; (** AUTHOR "fof & fn"; PURPOSE "Oberon Compiler: Symbolfile in- and output"; *)
(* (c) fof ETH Zürich, 2008 *)

использует
	D := Debugging, Basic := FoxBasic,  Scanner := FoxScanner, SyntaxTree := FoxSyntaxTree, Global := FoxGlobal, Formats := FoxFormats, Files,Потоки,
	Printout := FoxPrintout,Parser:= FoxParser , SemanticChecker := FoxSemanticChecker, InterfaceComparison := FoxInterfaceComparison, Options;

конст Trace = ложь;

тип
	TextualSymbolFile = окласс (Formats.SymbolFileFormat)
	перем extension: Basic.FileName; noRedefinition, noModification: булево;

		проц {перекрыта}Import*(конст moduleFileName: массив из симв8; importCache: SyntaxTree.ВнутренняяОбластьВидимостиМодуля; вхАнглоязычныеИмена: булево): SyntaxTree.ДеревоМодуля;
		перем fileName: Files.FileName; module: SyntaxTree.ДеревоМодуля; reader: Потоки.Чтец; scanner: Scanner.Лексер; parser: Parser.Parser;
			checker: SemanticChecker.Checker;
		нач
			Basic.Concat(fileName,path,moduleFileName,extension);
			если Trace то D.Ln; D.Str("importing "); D.Str(fileName); D.Ln; D.Update;  всё;
			module := НУЛЬ;
			reader := Basic.GetFileReader(fileName);
			если reader # НУЛЬ то
				scanner := Scanner.NewScanner(fileName, reader, reader.МестоВПотоке(), diagnostics);
				если ~scanner.былаЛиОшибкаРазбора то
					parser := Parser.NewParser(scanner,diagnostics, "");
					module := parser.Module();
					если parser.error то module := НУЛЬ всё;
				всё;
			всё;
			если (module # НУЛЬ) и ~(SyntaxTree.Осмыслен в module.стадииОсмысления) и (module.параметрыШаблона = НУЛЬ) то
				(*! should rather be done by importer *)
				(* на самом деле 4-й параметр должен быть проброшен из компилятора, а мы берём его из динамики, т.к. далеко брать из компилятора. 
				*)
				checker := SemanticChecker.NewChecker(diagnostics,ложь,истина,вхАнглоязычныеИмена,system,сам,importCache,"");
				checker.Module(module); (* semantic check *)
				если checker.error то module := НУЛЬ всё;
			всё;

			возврат module
		кон Import;

		проц {перекрыта}Export*(module: SyntaxTree.ДеревоМодуля; importCache: SyntaxTree.ВнутренняяОбластьВидимостиМодуля; вхАнглийскиеИмена:булево): булево;
		перем moduleName,fileName: Basic.FileName; writer: Files.Writer; file: Files.File; printer: Printout.Printer; result: булево;flags: мнвоНаБитахМЗ;
		нач
			Global.ModuleFileName(module.имя,module.контекстA2,moduleName,вхАнглийскиеИмена);
			Basic.Concat(fileName,path,moduleName,extension);
			если Trace то D.Ln; D.Str("exporting"); D.Str(fileName); D.Ln; D.Update;  всё;

			file := Files.New(fileName);
			если file = НУЛЬ то
				Basic.Error(diagnostics, module.имяФайлаИсходника, Basic.invalidPosition,  "could not open export file for writing");
				result := ложь;
			иначе

				flags := {};

				если module.параметрыШаблона = НУЛЬ то
					InterfaceComparison.CompareThis(module,сам,diagnostics,importCache,flags);
				всё;

				если noRedefinition или noModification то
					если (InterfaceComparison.Redefined в flags) то
						Basic.Error(diagnostics, module.имяФайлаИсходника, Basic.invalidPosition, " no redefinition of symbol file allowed");
						возврат ложь;
					всё;
				всё;
				если noModification то
					если (InterfaceComparison.Extended в flags) то
						Basic.Error(diagnostics, module.имяФайлаИсходника,Basic.invalidPosition,  " no extension of symbol file allowed");
						возврат ложь;
					всё;
				всё;

				нов(writer,file,0);
				если module.параметрыШаблона = НУЛЬ то
					printer := Printout.NewPrinter(writer, Printout.SymbolFile,ложь,ложь,module.вариантПеревода);
				иначе
					printer := Printout.NewPrinter(writer, Printout.SourceCode, ложь,ложь,module.вариантПеревода);
				всё;
				printer.Module(module);
				writer.ПротолкниБуферВПоток();
				Files.Register(file);
				result := истина;
			всё;
			возврат result
		кон Export;

		проц {перекрыта}DefineOptions*(options: Options.Options);
		нач
			options.Add(0X,"symbolFileExtension",Options.String);
			options.Add(0X,"noRedefinition",Options.Flag);
			options.Add(0X,"noModification",Options.Flag);
		кон DefineOptions;

		проц {перекрыта}GetOptions*(options: Options.Options);
		нач
			если ~options.GetString("symbolFileExtension",extension) то extension := ".Sym" всё;
			noRedefinition := options.GetFlag("noRedefinition");
			noModification := options.GetFlag("noModification");
		кон GetOptions;

	кон TextualSymbolFile;

	проц Get*(): Formats.SymbolFileFormat;
	перем symbolFile: TextualSymbolFile;
	нач
		нов(symbolFile); возврат symbolFile
	кон Get;

кон FoxTextualSymbolFile.
