модуль WMFileManager;		(** AUTHOR "ug"; PURPOSE  "file manager"; *)

использует ЛогЯдра, Commands, Files, Modules, WMGraphics, WMProperties,
	WMEditors, WMSystemComponents, WMComponents, WMStandardComponents, WMMessages, WMRestorable, Strings,
	WM := WMWindowManager;

конст
	WindowWidth = 500; WindowHeight = 400; FileNameLength = Files.NameLength;

тип
	KillerMsg = окласс
	кон KillerMsg;

	FileListPanel* = окласс (WMComponents.VisualComponent)
		перем pathProp*, filterProp : WMProperties.StringProperty;
			filterEdit : WMEditors.Editor;
			list* : WMSystemComponents.FileList;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrFileListPanel);
			(* new properties *)
			нов(pathProp, FileListPathProt, НУЛЬ, НУЛЬ); properties.Add(pathProp);
			нов(filterProp, FileListFilterProt, НУЛЬ, НУЛЬ); properties.Add(filterProp);

			нов(filterEdit); filterEdit.alignment.Set(WMComponents.AlignTop);
			filterEdit.bounds.SetHeight(25); AddContent(filterEdit);
			filterEdit.text.onTextChanged.Add(TextChanged);
			filterEdit.multiLine.Set(ложь);
			filterEdit.tv.showBorder.Set(истина);
			filterEdit.tv.textAlignV.Set(WMGraphics.AlignCenter);

			нов(list);
			list.alignment.Set(WMComponents.AlignClient);
			AddContent(list)
		кон Init;

		проц TextChanged(sender, data : динамическиТипизированныйУкль);
		перем str : массив FileNameLength (* was 128*)  из симв8;
		нач
			filterEdit.GetAsString(str);
			filterProp.Set(Strings.NewString(str))
		кон TextChanged;

		проц {перекрыта}PropertyChanged*(sender, data : динамическиТипизированныйУкль);
		нач
			если data = pathProp то
				list.StartNewPath(pathProp.Get())
			аесли (data = filterProp) то
				list.StartNewFilter(filterProp.Get())
			иначе
				PropertyChanged^(sender, data)
			всё;
		кон PropertyChanged;

	кон FileListPanel;

	Window* = окласс (WMComponents.FormWindow)
	перем label: WMStandardComponents.Label;
		tree : WMSystemComponents.DirectoryTree;
		flistPanel : FileListPanel;
		vol, cap, free, files : WMStandardComponents.Label;

		проц CreateForm(): WMComponents.VisualComponent;
		перем
			sidePanel, panel, status : WMStandardComponents.Panel;
			toolbar: WMStandardComponents.Panel;
			button : WMStandardComponents.Button;
			resizerH : WMStandardComponents.Resizer;

		нач
			нов(panel); panel.alignment.Set(WMComponents.AlignClient); panel.fillColor.Set(цел32(0FFFFFFFFH)); panel.takesFocus.Set(истина);
			нов(toolbar); toolbar.fillColor.Set(цел32(0FFFFFFFFH)); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);
			нов(button); button.alignment.Set(WMComponents.AlignLeft); button.caption.SetAOC("Refresh FileSystems");
			toolbar.AddContent(button); button.bounds.SetWidth(150); button.onClick.Add(RefreshHandler);
			нов(button); button.alignment.Set(WMComponents.AlignLeft); button.caption.SetAOC("Toggle Properties");
			toolbar.AddContent(button); button.bounds.SetWidth(150); button.onClick.Add(TogglePropsHandler);
			нов(status); status.alignment.Set(WMComponents.AlignBottom); status.bounds.SetHeight(20);
			panel.AddContent(status); status.fillColor.Set(цел32(0CCCCCCFFH));
			нов(label); label.bounds.SetWidth(100); label.caption.SetAOC(" Volume: "); label.alignment.Set(WMComponents.AlignLeft);
			status.AddContent(label);
			нов(vol); vol.bounds.SetWidth(100); vol.caption.SetAOC("-"); vol.alignment.Set(WMComponents.AlignLeft);
			status.AddContent(vol);
			нов(label); label.bounds.SetWidth(100); label.caption.SetAOC(" Size: "); label.alignment.Set(WMComponents.AlignLeft);
			status.AddContent(label);
			нов(cap); cap.bounds.SetWidth(100); cap.caption.SetAOC("-"); cap.alignment.Set(WMComponents.AlignLeft);
			status.AddContent(cap);
			нов(label); label.bounds.SetWidth(100); label.caption.SetAOC(" Free: "); label.alignment.Set(WMComponents.AlignLeft);
			status.AddContent(label);
			нов(free); free.bounds.SetWidth(100); free.caption.SetAOC("-"); free.alignment.Set(WMComponents.AlignLeft);
			status.AddContent(free);
			нов(label); label.bounds.SetWidth(100); label.caption.SetAOC(" Files: "); label.alignment.Set(WMComponents.AlignLeft);
			status.AddContent(label);
			нов(files); files.bounds.SetWidth(100); files.caption.SetAOC("-"); files.alignment.Set(WMComponents.AlignLeft);
			status.AddContent(files);

			нов(sidePanel); sidePanel.alignment.Set(WMComponents.AlignLeft); sidePanel.bounds.SetWidth(200);
			нов(resizerH); resizerH.alignment.Set(WMComponents.AlignRight); resizerH.bounds.SetWidth(4);
			sidePanel.AddContent(resizerH);
			нов(tree); tree.alignment.Set(WMComponents.AlignClient);
			sidePanel.AddContent(tree);
			panel.AddContent(sidePanel);

			нов(flistPanel); flistPanel.alignment.Set(WMComponents.AlignClient);
			panel.AddContent(flistPanel);

			возврат panel
		кон CreateForm;

		проц &New*(c : WMRestorable.Context);
		перем vc : WMComponents.VisualComponent; s : Strings.String;
		нач
			IncCount;
			vc := CreateForm();

			tree.onPathChanged.Add(PathChanged);

			если (c # НУЛЬ) то
				Init(c.r - c.l, c.b - c.t, ложь);
			иначе
				Init(WindowWidth, WindowHeight, ложь);
			всё;

			SetContent(vc);
			SetTitle(Strings.NewString("File Manager"));
			SetIcon(WMGraphics.LoadImage("WMIcons.tar://WMFileManager.png", истина));

			если c # НУЛЬ то
				(* restore the desktop *)
				WMRestorable.AddByContext(сам, c);
			иначе
				WM.DefaultAddWindow(сам);
			всё;

			s := tree.currentPath.Get();
		кон New;

		проц PathChanged(sender, data : динамическиТипизированныйУкль);
		нач
			flistPanel.pathProp.Set(tree.currentPath.Get());
			UpdateStatus(tree.currentPath.Get());
		кон PathChanged;

		проц UpdateStatus(alias : Strings.String);
		перем sfree, total: цел32; nfiles: размерМЗ; i: размерМЗ; fs: Files.FileSystem; ft: Files.FileSystemTable;
			temp : массив 32 из симв8;
		нач
			Files.GetList(ft);
			если ft # НУЛЬ то
				нцДля i := 0 до длинаМассива(ft)-1 делай
					fs := ft[i];
					копируйСтрокуДо0(fs.prefix, temp);
					Strings.Append(temp, ":");
					если (alias^ = temp) то
						если fs.vol # НУЛЬ то
							vol.caption.SetAOC(alias^);
							sfree := округлиВниз(fs.vol.Available()/1024.0D0 * fs.vol.blockSize);
							total := округлиВниз(fs.vol.size/1024.0D0 * fs.vol.blockSize);
							Strings.IntToStr(total, temp); Strings.Append(temp, " KB");
							cap.caption.SetAOC(temp);
							Strings.IntToStr(sfree, temp); Strings.Append(temp, " KB");
							free.caption.SetAOC(temp);
							nfiles := flistPanel.list.GetNofFiles();
							Strings.IntToStr(nfiles, temp);
							files.caption.SetAOC(temp);
						всё;
					всё;
				кц
			всё;
		кон UpdateStatus;

		проц {перекрыта}Close*;
		нач
			Close^;
			DecCount;
		кон Close;

		проц RefreshHandler(sender, data: динамическиТипизированныйУкль);
		нач
			tree.Refresh;
		кон RefreshHandler;

		проц TogglePropsHandler(sender, data: динамическиТипизированныйУкль);
		нач
			flistPanel.list.ToggleProps;
		кон TogglePropsHandler;

		проц {перекрыта}Handle*(перем x: WMMessages.Message);
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) и (x.ext суть KillerMsg) то Close
			аесли (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) и (x.ext суть WMRestorable.Storage) то
				x.ext(WMRestorable.Storage).Add("FileManager", "WMFileManager.Restore", сам, НУЛЬ)
			иначе Handle^(x)
			всё
		кон Handle;

	кон Window;

перем
	nofWindows : цел32;
	FileListPathProt : WMProperties.StringProperty;
	FileListFilterProt : WMProperties.StringProperty;

	StrFileListPanel : Strings.String;
	
(* 06-09-2021 kia штатный метод выглядел так
проц Open*;
перем win : Window;
тело
	нов(win, НУЛЬ);
кн Open;
*)

проц Restore*(context : WMRestorable.Context);
перем win : Window;
нач
	нов(win, context)
кон Restore;

проц InitStrings;
нач
	StrFileListPanel := Strings.NewString("FileListPanel");
кон InitStrings;

проц InitPrototypes;
нач
	нов(FileListPathProt, НУЛЬ, Strings.NewString("Path"), Strings.NewString("contains the displayed path"));
	нов(FileListFilterProt, НУЛЬ, Strings.NewString("Filter"), Strings.NewString("display filename filter"));
кон InitPrototypes;

проц IncCount;
нач {единолично}
	увел(nofWindows);
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows);
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WM.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WM.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0)
кон Cleanup;

(* ####################################################################
	Мод открытия окна менеджера с указанным путём. 
	Используется публичный метод Refresh, модифицированный под сброс дерева в определённый путь.
	- путь вида C:\Dev\MyDir или с:/Dev/MyDir
	- 06-09-2021 kia: имена каталогов без пробелов
*)

проц Open* (context: Commands.Context);
тип
	Filename = Files.FileName; (* после отладки перевести в FileNameUCS32 *)
перем 
	win: Window;
	путь: Filename;
	normalizedPath: Strings.String;

	(* 03-01-2021 kia: сервисная процедура подтверждения наличного пути. Регистронезависимо! *)
	проц СуществуетЛи (path : Filename): булево;
	перем fullName: Files.FileName; flags: мнвоНаБитахМЗ;
	нач
		flags := {};
	возврат Files.Exists(path, fullName, flags);
	кон СуществуетЛи;

	(* имя диска поднять в верхний регистр, возможные бэкслэши заменить на слэши *)
	проц НормализоватьПуть (конст исходнаяСтрока : массив из симв8): Strings.String;
	перем диск, имя, новаяСтрока: Filename; slashPath: Strings.String; i: размерМЗ;
	нач
		Files.SplitName(исходнаяСтрока, диск, имя);
		Strings.UpperCase(диск);
		slashPath := Strings.NewString(имя);
		нцДля i := 0 до длинаМассива(slashPath)-1 делай
			если slashPath[i] = "\" то slashPath[i] := Files.PathDelimiter всё;
		кц;
		Files.JoinName(диск, slashPath^, новаяСтрока);
	возврат Strings.NewString(новаяСтрока);
	кон НормализоватьПуть;

нач
	нов(win, НУЛЬ);
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(путь) то
		если СуществуетЛи(путь) то
			normalizedPath := НормализоватьПуть(путь);
			(* сброс дерева в указанный путь *)
			win.tree.currentPath.Set(normalizedPath);
			win.tree.Refresh;
			(* принудительное обновление списка файлов *)
			win.flistPanel.list.Acquire;
			win.flistPanel.list.StartNewPath(normalizedPath);
			win.flistPanel.list.Release;
		иначе
			context.out.пСтроку8("WMFileManager: задан неверный путь "); context.out.пСтроку8(путь); context.out.пВК_ПС;
		всё
	всё;
кон Open;

(* #################################################################### *)

нач
	InitStrings;
	InitPrototypes;
	Modules.InstallTermHandler(Cleanup);
кон WMFileManager.

(*
System.Free WMFileManager WMSystemComponents ~
WMFileManager.Open ~
WMFileManager.Open r:\1\2\3 ~
*)



