модуль SSHPackets;  (* GF 10.12.2020 *)

использует Strings, Log := ЛогЯдра, B := CryptoBigNumbers, U := CryptoUtils;

конст				
	Disconn* = 1X;  Ignore* = 2X;  Unimpl* = 3X;  Debug* = 4X;
	ServiceRequest* = 5X;  ServiceAccept* = 6X;
	
	KEXInit* = 14X;  NewKeys* = 15X;  DHInit* = 1EX;  DHReply* = 1FX;   
	GEXInit* = 20X;  GEXReply* = 21X;  GEXRequest* = 22X;  GEXGroup* = 1FX;  
	
	UserauthRequest* = 32X;  UserauthFailure* = 33X;
	UserauthSuccess* = 34X;  UserauthBanner* = 35X;
	UserauthPkOk* = 3CX;
	
	GlobalRequest* = 50X;  RequestSuccess* = 51X;  RequestFailure* = 52X;  
	ChannelOpen* = 5AX;  OpenConfirm* = 5BX;  OpenFailure* = 5CX;  
	WindAdjust* = 5DX;  Data* = 5EX;  ExtData* = 5FX;  
	
	ChannelEOF* = 60X;  ChannelClose* = 61X;
	ChannelRequest* = 62X;  ChannelSuccess* = 63X;  ChannelFailure* = 64X;


тип	
	Packet* = окласс (** SSH Packet *)
		перем
			buf-: укль на массив из симв8;
			type-: симв8;
			len-: размерМЗ;	(* packet size *)
			pos-: размерМЗ;	(* read position *)
			sent: булево;
			
			проц &Init*( ptype: симв8; bufsize: размерМЗ );
			нач
				нов( buf, bufsize );
				type := ptype; buf[0] := ptype;  len := 1;
				sent := истина
			кон Init;
			
			(*===============================================================*)
			
			проц AppInteger* ( v: размерМЗ );
			нач
				buf[len] := симв8ИзКода( v DIV 1000000H остОтДеленияНа 100H );
				buf[len+1] := симв8ИзКода( v DIV 10000H остОтДеленияНа 100H );
				buf[len+2] := симв8ИзКода( v DIV 100H остОтДеленияНа 100H );
				buf[len+3] := симв8ИзКода( v остОтДеленияНа 100H );
				увел( len, 4 )
			кон AppInteger;
			
			проц AppChar*( c: симв8 );
			нач
				buf[len] := c;  увел( len )
			кон AppChar;
			
			проц AppString*( конст str: массив из симв8 );
			перем slen, i: размерМЗ; 
			нач
				slen := Strings.Length( str );
				AppInteger( slen );
				нцДля i := 0 до slen-1 делай  buf[len+i] := str[i]  кц;
				увел( len, slen )
			кон AppString;

			проц AppArray*( конст arr: массив из симв8;  pos, arlen: размерМЗ );
			перем i: размерМЗ;
			нач
				AppInteger( цел32(arlen) );
				нцДля i := 0 до arlen-1 делай  buf[len+i] := arr[pos+i]  кц;
				увел( len, arlen )
			кон AppArray;
			
			проц AppBigNumber*( b: B.BigNumber );
			нач
				U.PutBigNumber( buf^, len, b );
			кон AppBigNumber;
			
			(*================================================================*)
			
			проц SetPos*( p: размерМЗ );
			нач
				pos := p
			кон SetPos;
			
			проц IncPos*( i: целМЗ );
			нач
				увел( pos, i )
			кон IncPos;
			
			проц GetInteger*(): цел32;
			перем i: цел32;
			нач
				i :=	арифмСдвиг( цел32( кодСимв8( buf[pos] ) ), 24 ) +
						арифмСдвиг( цел32( кодСимв8( buf[pos + 1] ) ), 16 ) +
						арифмСдвиг( цел32( кодСимв8( buf[pos + 2] ) ), 8 ) +
						цел32( кодСимв8( buf[pos + 3] ) );
				увел( pos, 4 );
				возврат i
			кон GetInteger;
			
			проц GetChar*(): симв8;
			перем c: симв8; p: размерМЗ;
			нач 
				p := pos;
				c := buf[pos];  увел( pos );  возврат c
			кон GetChar;
			
			проц GetBlobInfo*( перем start, size: размерМЗ );
			перем l: цел32;
			нач
				start := pos;
				l := GetInteger( );
				увел( pos, l );
				size := l + 4
			кон GetBlobInfo;
			
			проц GetString*( перем str: массив из симв8 );
			перем i, slen: цел32;
			нач
				slen := GetInteger();
				нцДля i := 0 до slen - 1 делай  str[i] := buf[pos + i]  кц;
				str[slen] := 0X;  увел( pos, slen )
			кон GetString;
			
			проц GetArray*( перем arr: массив из симв8; перем arlen: размерМЗ );
			перем i: размерМЗ;
			нач
				arlen := GetInteger();
				нцДля i := 0 до arlen - 1 делай  arr[i] := buf[pos + i]  кц;
				увел( pos, arlen )
			кон GetArray;
			
			проц GetBigNumber*( ): B.BigNumber;
			перем b: B.BigNumber;  l: цел32;
			нач
				l := GetInteger( );
				B.AssignBin( b, buf^, pos, l );
				увел( pos, l );
				возврат b
			кон GetBigNumber;
			
			(*================================================================*)
			
			
			проц Show*;
			перем i, j, d: размерМЗ;  c: симв8;
			нач
				если sent то  Log.пСтроку8( "Sent: " )  иначе  Log.пСтроку8( "Got: " )  всё;
				Log.п16ричное( кодСимв8( type ), -2 ); Log.пСтроку8( "X(" );  Log.пЦел64( кодСимв8( type ), 0 );  Log.пСтроку8( "), len=" ); Log.пЦел64( len, 0 ); 
				SetPos( 1 );
				если (type = 50X) или (type = 5DX) или (type = 5EX) то
					i := GetInteger( );  
					Log.пСтроку8( ", " );  Log.пЦел64( i, 0 );
				всё;
				Log.пВК_ПС; Log.пСтроку8( "      " ); i := 0;
				нцДо
					c := buf[i];  увел( i );
					Log.п16ричное( кодСимв8(c), -2 );  Log.пСимв8( ' ' );
					если i остОтДеленияНа 32 = 0 то  
						Log.пСтроку8( "   " );
						j := i - 32;
						нцДо
							c := buf[j];  увел( j );
							если (c < ' ') или (c > '~') то  Log.пСимв8( '.' )  иначе  Log.пСимв8( c )  всё
						кцПри j = i;
						Log.пВК_ПС;  Log.пСтроку8( "      " )  
					всё
				кцПри i >= len;
				d := i остОтДеленияНа 32;
				если d # 0 то
					Log.пСтроку8( "   " );
					j := i - d;
					нцДо
						c := buf[j];  увел( j );
						если (c < ' ') или (c > '~') то  Log.пСимв8( '.' )  иначе  Log.пСимв8( c )  всё
					кцПри j = i;
				всё;		
				Log.пВК_ПС;
			кон Show;

	кон Packet;
	
перем
	emptyPacket-: Packet; (* marks end of data from remote host *)
	
	
	
	проц MakeReceivedPacket*( конст trbuf: массив из симв8;  len: размерМЗ ): Packet;
	перем p: Packet; i: размерМЗ;
	нач
		нов( p, 0X, len ); 
		нцДля i := 0 до len - 1 делай  p.buf[i] := trbuf[i+5]  кц;
		p.type := p.buf[0];
		p.len := len;
		p.pos := 1;
		p.sent := ложь;
		возврат p
	кон MakeReceivedPacket;
	

нач
	нов( emptyPacket, Data, 16 );	 
		emptyPacket.sent := ложь;
		emptyPacket.pos := 5;
кон SSHPackets.
