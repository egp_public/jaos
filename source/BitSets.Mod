модуль BitSets;	(** AUTHOR "negelef"; PURPOSE "generic bit container"; *)

использует НИЗКОУР;

конст Elements = матМаксимум (мнвоНаБитахМЗ) - матМинимум (мнвоНаБитахМЗ) + 1;

тип Bits = размерМЗ;
тип Data = укль на массив из мнвоНаБитахМЗ;

тип BitSet* = окласс

	перем size: Bits;
	перем data: Data;

	проц & InitBitSet* (size: Bits);
	нач сам.size := size; Resize (size);
	кон InitBitSet;

	проц Zero*;
	перем i: Bits;
	нач нцДля i := 0 до длинаМассива(data)-1 делай data[i] := {} кц;
	кон Zero;

	проц Resize* (size: Bits);
	перем newData: Data; i: Bits;
	нач
		утв (size >= 0);
		сам.size := size;
		size := матМаксимум (size - 1, 0) DIV Elements + 1;
		если data # НУЛЬ то
			если size <= длинаМассива (data) то возврат; всё;
			size := матМаксимум (size, длинаМассива (data) * 2);
		всё;
		нов (newData, size);
		если data # НУЛЬ то
			нцДля i := 0 до длинаМассива (data) - 1 делай newData[i] := data[i]; кц;
		всё;
		data := newData;
	кон Resize;

	проц GetSize* (): размерМЗ;
	нач возврат size;
	кон GetSize;

	проц SetBit* (pos: Bits; value: булево);
	нач
		утв (pos >= 0); утв (pos < size);
		если value то
			включиВоМнвоНаБитах (data[pos DIV Elements], pos остОтДеленияНа Elements);
		иначе
			исключиИзМнваНаБитах (data[pos DIV Elements], pos остОтДеленияНа Elements);
		всё;
	кон SetBit;

	проц GetBit* (pos: Bits): булево;
	нач
		утв (pos >= 0); утв (pos < size);
		возврат pos остОтДеленияНа Elements в data[pos DIV Elements];
	кон GetBit;

	проц SetBits* (startPos, bits: Bits; value: цел64);
	перем adr: адресВПамяти;
	нач
		утв (startPos >= 0); утв (startPos+bits <= size);
		если (bits = 8) и (startPos остОтДеленияНа 8 = 0) то
			adr := операцияАдресОт data[0] + startPos DIV 8;
			НИЗКОУР.запишиОбъектПоАдресу(adr, симв8ИзКода(value));
		иначе
			нцПока bits > 0 делай
				SetBit (startPos, нечётноеЛи¿ (value)); value := value DIV 2;
				увел(startPos); умень(bits)
			кц;
			нцПока bits < 0 делай
				SetBit (startPos, нечётноеЛи¿ (value)); value := value DIV 2;
				умень(startPos); увел(bits)
			кц;
		всё;
	кон SetBits;

	проц SetBytes*(startPos, bytes: размерМЗ; конст values: массив из симв8);
	перем adr: адресВПамяти;
	нач
		утв (startPos >= 0); утв (startPos+8*bytes <= size); утв(startPos остОтДеленияНа 8 = 0);
		adr := операцияАдресОт data[0] + startPos DIV 8;
		НИЗКОУР.копируйПамять(операцияАдресОт values[0], adr, bytes);
	кон SetBytes;

	проц GetBits* (startPos, bits: Bits): целМЗ;
	перем value: целМЗ; adr: адресВПамяти;
	нач
		утв (startPos >= 0); утв (startPos+bits <= size);
		если (bits = 8) и (startPos остОтДеленияНа 8 =0) то
			adr := операцияАдресОт data[0] + startPos DIV 8;
			value := НИЗКОУР.прочти8битПоАдресу(adr)
		иначе
			увел (startPos, bits); value := 0;
			нцПока bits > 0 делай
				value := value*2; умень (startPos); умень (bits);
				если GetBit (startPos) то увел (value) всё;
			кц;
			нцПока bits < 0 делай
				value := value*2; увел (startPos); увел (bits);
				если GetBit (startPos) то увел (value) всё;
			кц;
		всё;
		возврат value;
	кон GetBits;

	проц CopyTo*(address: адресВПамяти; bits: Bits);
	нач
		утв(bits остОтДеленияНа 8 = 0);
		НИЗКОУР.копируйПамять(операцияАдресОт data[0], address, bits DIV 8);
	кон CopyTo;

кон BitSet;

проц CopyBits* (source: BitSet; sourcePos: Bits; dest: BitSet; destPos, count: Bits);
нач
	утв (count >= 0);
	если sourcePos остОтДеленияНа Elements = destPos остОтДеленияНа Elements то
		нцПока (count # 0) и (sourcePos остОтДеленияНа Elements # 0) делай
			dest.SetBit (destPos, source.GetBit (sourcePos));
			увел (sourcePos); увел (destPos); умень (count);
		кц;
		нцПока (count >= Elements) делай
			dest.data[destPos DIV Elements] := source.data[sourcePos DIV Elements];
			увел(sourcePos,Elements); увел(destPos,Elements); умень(count,Elements);
		кц;
		нцПока count # 0 делай
			dest.SetBit (destPos, source.GetBit (sourcePos));
			увел (sourcePos); увел (destPos); умень (count);
		кц;
	иначе
		нцПока count # 0 делай
			dest.SetBit (destPos, source.GetBit (sourcePos));
			увел (sourcePos); увел (destPos); умень (count);
		кц;
	всё;
кон CopyBits;

кон BitSets.
