модуль UsbKeyboard; (** AUTHOR "cplattner; staubesv"; PURPOSE "Bluebottle USB Keyboard Driver" *) (** non-portable **)
(**
 * Bluebottle USB Keyboard Driver (HID boot protocol)
 *
 * Usage:
 *
 *	UsbKeyboard.Install ~ loads this driver		System.Free UsbKeyboard ~ unloads it
 *
 *	UsbKeyboard.SetLayout dev file ~ sets the keyboard layout
 * 	UsbKeyboard.SetLayout UsbKeyboard00 KeyCH.Bin ~ sets the CH keyboard layout, for example
 *
 * References:
 *
 *	Device Class Definition for Human Interface Devices (HID), version 1.11
 *	HID Usage Tables, version 1.11
 *
 *	References are available at http://www.usb.org
 *
 * History:
 *
 *	30.09.2000 	cp first release
 *	18.10.2000 	cp fix size of interrupt endpoint and add warning message if keyboard fails
 *	27.02.2006	Correct handling for modifier keys (also generate event if only a modifier key is pressed) (staubesv)
 *	01.03.2006	Added SetLayout & KeyboardDriver.SetLayout (staubesv)
 *	22.01.2007	Splitted up Keyboard Driver for HID compatibility (ottigerm)
 *	26.06.2014	Basic port to ARM (tmartiel)
 *)

использует НИЗКОУР, ЭВМ, Files, Inputs, Commands, ЛогЯдра, Потоки, Plugins, Modules, Usb, Usbdi, UsbHid;

конст

	Name = "UsbKeyboard";
	Description = "USB Keyboard Driver";
	Priority = 10;

	NumLock* = 0;
	CapsLock* = 1;
	ScrollLock* = 2;
	(* Compose & kana not yet implemented *)
	Compose = 3;
	Kana = 4;

	(* If you press a key and hold it down, the following will happen:					*)
	(* 1. A Inputs.KeyboardMsg is sent											*)
	(* 2. No further messages are sent until the period KeyDeadTime expires			*)
	(* 3. Further messages are sent with the interval KeyDeadTimeRepeat				*)
	(*																				*)
	(* A release event is sent when you release the key.								*)
	(* The values KeyDeadTime and KeyDeadTimeRepeat are set in milliseconds. 		*)
	(*																				*)
	KeyDeadTime* = 100;
	KeyDeadTimeRepeat* = 0;  (* 10 <= value < infinity  && value mod 10 = 0 *)

	TraceKeys* = ложь; (* Displays scan code of pressed key on KernelLog if TRUE *)
	Debug* = истина;

тип

	Key* = запись
		ch* : симв8;
		keysym* : цел32;
		counter* : цел32;
		repeat* : булево;
		updated* : булево;
	кон;

тип

	KeyboardBase*=окласс
	перем
		msg*, lastMsg : Inputs.KeyboardMsg;
		lastFlags : мнвоНаБитахМЗ;

		numKeyVal : цел32;
		deadKey* : цел32;
		dkHack* : цел32;  (* deadKey value should persist Release events ... *)

		(* Status of NumLock,ScrollLock,CapsLock,Compose & Kana *)
		leds*, lastLeds* : мнвоНаБитахМЗ;
		ledBuffer* : Usbdi.BufferPtr;

		keyboardFileTable : укль на массив из симв8;
		keytable* : адресВПамяти; (* used as pointer to keyboardFileTable[0] *)

		keyDeadTime*, keyDeadTimeRepeat* : цел32;


		проц HandleKey*(c : симв8);
		перем k : цел32;
		нач
			(* map USB Usage ID to keysym: Only non-alphanumeric keys are mapped by Keysym()  *)
			msg.keysym := KeySym(c, leds);
			если TraceKeys то ЛогЯдра.пСтроку8("USB Usage ID: "); ЛогЯдра.п16ричное(кодСимв8(c), -3); всё;
			(* map USB Usage ID to Oberon key code *)
			НИЗКОУР.прочтиОбъектПоАдресу(UsbScanTab() + кодСимв8(c), c);
			если TraceKeys то ЛогЯдра.пСтроку8(" -> Oberon key code: "); ЛогЯдра.п16ричное(кодСимв8(c), -3) всё;

			если c = симв8ИзКода(58) то  leds := leds / {CapsLock};
			аесли c = симв8ИзКода(69) то leds := leds / {NumLock};
			аесли c = симв8ИзКода(70) то leds := leds / {ScrollLock};
			иначе
				k := Translate(msg.flags, leds, c, keytable, deadKey, numKeyVal);
				если TraceKeys то ЛогЯдра.пСтроку8(" translated into: "); ЛогЯдра.пСимв8(симв8ИзКода(k)); всё;
				(* if c is an ASCII character, then map c to keysym *)
				если (k  >= 1) и (k  <= 126) и (msg.keysym = Inputs.KsNil)  то msg.keysym := k; всё;
				если k >= 0 то msg.ch := симв8ИзКода(k) иначе msg.ch := 0X всё;
				если TraceKeys то
					ЛогЯдра.пСтроку8(" Aos Keysym: "); если msg.keysym = Inputs.KsNil то ЛогЯдра.пСтроку8("No Key"); иначе ЛогЯдра.п16ричное(msg.keysym, 9); всё;
					ЛогЯдра.пВК_ПС; ShowFlags(msg.flags, leds); ЛогЯдра.пВК_ПС;
				всё;
				(* build up message for this event *)
				если (msg.flags # lastMsg.flags) или (msg.ch # 0X) или (msg.keysym # Inputs.KsNil) то
					Inputs.keyboard.Handle(msg);
				всё;
				lastMsg := msg;
			всё;
		кон HandleKey;

		проц HandleModifiers*(flags : мнвоНаБитахМЗ);
		перем i : цел32;
		нач
			если flags # lastFlags то
				msg.flags := {}; msg.ch := 0X; msg.keysym := Inputs.KsNil;
				нцДля i := 0 до матМаксимум(мнвоНаБитахМЗ) делай
					если (i в flags) и ~(i в lastFlags) то (* modifier key pressed for the first time *)
						msg.flags := мнвоНаБитахМЗ({i}); msg.keysym := GetModifierKeysym(i);
						Inputs.keyboard.Handle(msg);
					аесли ~(i в flags) и (i в lastFlags) то (* modifier key released *)
						msg.flags := {Inputs.Release}; msg.keysym := GetModifierKeysym(i);
						Inputs.keyboard.Handle(msg);
					всё;
				кц;
			всё;
			lastFlags := flags;
		кон HandleModifiers;

		проц TableFromFile*(конст name: массив из симв8): адресВПамяти;
		перем f: Files.File; r: Files.Rider; len: размерМЗ;
		нач
			ЛогЯдра.пСтроку8("UsbKeyboard: "); ЛогЯдра.пСтроку8(" Loading layout "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пВК_ПС;
			f := Files.Old(name);
			если f # НУЛЬ то
				len := f.Length()(размерМЗ);
				если len остОтДеленияНа 4 = 0 то
					нов(keyboardFileTable, len+1);
					f.Set(r, 0); f.ReadBytes(r, keyboardFileTable^, 0, len);
					если r.res = 0 то
						keyboardFileTable[len] := 0FFX;
						возврат адресОт(keyboardFileTable[0])
					аесли Debug то ЛогЯдра.пСтроку8("UsbKeyboard: TableFromFile: Error: res="); ЛогЯдра.пЦел64(r.res, 1); ЛогЯдра.пВК_ПС;
					всё
				аесли Debug то ЛогЯдра.пСтроку8("UsbKeyboard: TableFromFile: Error: len="); ЛогЯдра.пЦел64(len, 1); ЛогЯдра.пВК_ПС;
				всё
			аесли Debug то ЛогЯдра.пСтроку8("UsbKeyboard: TableFromFile: Error: File not found."); ЛогЯдра.пВК_ПС;
			всё;
			возврат -1;
		кон TableFromFile;

		проц SetLayout*(конст name : массив из симв8);
		перем adr : адресВПамяти;
		нач
			если name = "KeyUS.Bin" то adr := TableUS();
			иначе adr := TableFromFile(name);
			всё;
			если adr = -1 то (* Leave the current setting *)
			иначе НИЗКОУР.запишиОбъектПоАдресу(адресОт(keytable), adr);
			всё;
		кон SetLayout;

	кон KeyboardBase;

	KeyboardDriver = окласс (UsbHid.HidDriver)
	перем
		pipe : Usbdi.Pipe;

		(* buffer[0] : modifier byte					*)
		(* buffer[1] : reserved						*)
		(* buffer[2]-buffer[7] : 6 one byte key codes  	*)
		buffer : Usbdi.BufferPtr;

		base : KeyboardBase;

		(*for keeping the pressed keys in mind*)
		pressed* : массив 6 из Key;

		проц &Init*;
		нач
			нов(base);
		кон Init;

		проц EventHandler(status : Usbdi.Status; actLen : цел32);
		перем
			i, j : цел32;
			c : симв8;
			modifiers, flags : мнвоНаБитахМЗ;
			res : булево;
			tempPressed : массив 6 из Key;
			found, kill : булево;
		нач
			если (status=Usbdi.Ok) или ((status = Usbdi.ShortPacket) и (actLen >= 8)) то

				(* evaluate modifier keys *)
				base.msg.flags := {};
				modifiers := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, buffer[0]);
				если modifiers * {0} # {} то включиВоМнвоНаБитах(base.msg.flags, Inputs.LeftCtrl) всё;
				если modifiers * {1} # {} то включиВоМнвоНаБитах(base.msg.flags, Inputs.LeftShift) всё;
				если modifiers * {2} # {} то включиВоМнвоНаБитах(base.msg.flags, Inputs.LeftAlt) всё;
				если modifiers * {3} # {} то включиВоМнвоНаБитах(base.msg.flags, Inputs.LeftMeta) всё;
				если modifiers * {4} # {} то включиВоМнвоНаБитах(base.msg.flags, Inputs.RightCtrl) всё;
				если modifiers * {5} # {} то включиВоМнвоНаБитах(base.msg.flags, Inputs.RightShift) всё;
				если modifiers * {6} # {} то включиВоМнвоНаБитах(base.msg.flags, Inputs.RightAlt) всё;
				если modifiers * {7} # {} то включиВоМнвоНаБитах(base.msg.flags, Inputs.RightMeta) всё;
				flags := base.msg.flags;

				(* evaluate the six keycodes *)
				нцДля i := 2 до 7 делай
					c := buffer[i];
					если c # симв8ИзКода(0) то (* buffer[i] contains key code *)

						(* check whether the key is pressed for the first time, is still being pressed or has been released *)
						нцДля j := 0 до 5 делай

							если pressed[j].ch = c то (* key is still pressed *)
								found := истина;
								pressed[j].updated := истина;

								tempPressed[i-2].counter := pressed[j].counter + 1;
								tempPressed[i-2].ch := pressed[j].ch;
								tempPressed[i-2].keysym := pressed[j].keysym;
								tempPressed[i-2].updated := ложь;
								tempPressed[i-2].repeat := pressed[j].repeat;

								если pressed[j].repeat то
									если (base.keyDeadTimeRepeat # 0) и (tempPressed[i-2].counter остОтДеленияНа base.keyDeadTimeRepeat # 0) то (* don't send key event *) kill := истина; всё;
								иначе
									если tempPressed[i-2].counter остОтДеленияНа base.keyDeadTime # 0 то (* don't send key event *)
										kill := истина;
									иначе
										tempPressed[i-2].repeat := истина;
									всё;
								всё;
							всё;
					    	кц;
					 всё;

					если ~found то (* the key has not been pressed down before *)
						tempPressed[i-2].ch := c;
						tempPressed[i-2].repeat := ложь;
						tempPressed[i-2].updated := ложь;
						tempPressed[i-2].counter := 1;
					всё;

				    (* kill : Key is pressed but do not generate key event this time -> repeat rate ... *)
				    если (c # симв8ИзКода(0)) и ~kill то
				    	base.HandleKey(c);
				    	tempPressed[i-2].keysym := base.msg.keysym; (* base.msg.keysym asigned by HandleKey() ... *)
				    всё;
				кц; (* FOR LOOP *)

				(* update pressed array. generate keyboard.base.msg's for released keys *)
				нцДля i := 0 до 5 делай
					если (pressed[i].updated = ложь) и (pressed[i].ch # симв8ИзКода(0)) то (* this key has been released *)
						base.msg.flags := {};
						включиВоМнвоНаБитах(base.msg.flags, Inputs.Release);
						base.msg.ch := pressed[i].ch;
						base.msg.keysym := pressed[i].keysym;
						base.dkHack := base.deadKey;  (* value of deadKey should persist the key release event *)
						base.HandleKey(c);
						base.deadKey := base.dkHack;
					всё;
					pressed[i].counter := tempPressed[i].counter;
					pressed[i].ch := tempPressed[i].ch;
					pressed[i].keysym := tempPressed[i].keysym;
					pressed[i].repeat := tempPressed[i].repeat;
					pressed[i].updated := ложь;
				кц;

				(* Generate events for modifiers *)
				base.HandleModifiers(flags);

				(* update status of the LEDs  of the keyboad if necessary *)
				если base.lastLeds # base.leds то (* LED status has changed *)
					base.ledBuffer[0] := НИЗКОУР.подмениТипЗначения(симв8, base.leds); base.lastLeds := base.leds;
					res := SetReport(UsbHid.ReportOutput, 0, base.ledBuffer, 1); (* ignore res *)
				всё;
				status := pipe.Transfer(pipe.maxPacketSize, 0, buffer);
			иначе
				если Debug то ЛогЯдра.пСтроку8("UsbKeyboard: Error. Disabling keyboard "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пВК_ПС; всё;
			всё;
		кон EventHandler;

		проц {перекрыта}Connect*(): булево;
		перем status : Usbdi.Status; endpoint: цел32; i: адресВПамяти; k : массив 32 из симв8;
		нач
			если ~SetProtocol(0) то
				если Debug то ЛогЯдра.пСтроку8("UsbKeyboard: Error: Cannot set keyboard into boot protocol mode."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь
			всё;

			если ~SetIdle(0,10) то
				если Debug то ЛогЯдра.пСтроку8("UsbKeyboard: Error: Cannot set idle the keyboard."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь
			всё;

			endpoint := НИЗКОУР.подмениТипЗначения(цел32, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, interface.endpoints[0].bEndpointAddress) * {0,1,2,3,7});

			pipe := device.GetPipe(endpoint);
			если pipe = НУЛЬ то
				если Debug то ЛогЯдра.пСтроку8("UsbKeyboard: Could not get pipe."); ЛогЯдра.пВК_ПС; всё;
				возврат ложь;
			всё;

			(* Get  *)
			ЭВМ.ДайЗначениеКлючаКонфигурацииЯОС("Keyboard", k);
			i := -1;
			если k # "" то i := base.TableFromFile(k); всё;
			если i = -1 то (* Fallback to default *) i := TableUS(); всё;
			НИЗКОУР.запишиОбъектПоАдресу(адресОт(base.keytable), i);

			(* Apply Numlock boot up state *)
			ЭВМ.ДайЗначениеКлючаКонфигурацииЯОС("NumLock", k);
			если k[0] = "1" то включиВоМнвоНаБитах(base.leds, NumLock) всё;

			base.keyDeadTime := KeyDeadTime DIV 10;
			base.keyDeadTimeRepeat := KeyDeadTimeRepeat DIV 10;

			нов(base.ledBuffer, 1);

			нов(buffer, pipe.maxPacketSize);
			pipe.SetTimeout(0);
			pipe.SetCompletionHandler(EventHandler);
			status := pipe.Transfer(pipe.maxPacketSize, 0, buffer); (* ignore status *)

			возврат истина;
		кон Connect;

		проц {перекрыта}Disconnect*;
		нач
			ЛогЯдра.пСтроку8("UsbKeyboard: USB Keyboard disconnected."); ЛогЯдра.пВК_ПС;
		кон Disconnect;

	кон KeyboardDriver;

перем

(* Translation table format:
 *
 *	table = { scancode unshifted-code shifted-code flags }  0FFX .
 *	scancode = <scancode byte from keyboard, bit 7 set for "grey" extended keys>
 *	unshifted-code = <CHAR produced by this scancode, without shift>
 *	shifted-code = <CHAR produced by this scancode, with shift>
 *	flags = <bit-mapped flag byte indicating special behaviour>
 *
 *	flag bit	function
 *		0	01	DeadKey: Set dead key flag according to translated key code (1-7)
 *		1	02	NumLock: if set, the state of NumLock will reverse the action of shift (for num keypad)
 *		2	04	CapsLock: if set, the state of CapsLock will reverse the action of shift (for alpha keys)
 *		3	08	LAlt:  \ the state of these two flags in the table and the current state of the two...
 *		4	10	RAlt: / ...Alt keys must match exactly, otherwise the search is continued.
 *		5	20	\
 *		6	40	 >  dead key number (0-7), must match current dead key flag
 *		7	80	/
 *
 *	The table is scanned sequentially (speed not critical).  Ctrl-Break, Ctrl-F10 and Ctrl-Alt-Del
 *	are always defined and are not in the table.   The control keys are also always defined.
 *)

(* TableUS - US keyboard translation table (dead keys: ^=1, '=2, `=3, ~=4, "=5) *)
проц TableUS*(): адресВПамяти;
машКод
	; Return address of first table element
	ADD	R0, PC, #Base-8-$
	MOV	SP, FP
	LDR	FP, [SP], #4
	LDR	LR, [SP], #4
	BX		LR
	; Table
Base:
		; alphabet
	DB 1EH, 61H, 41H, 4H,		30H, 62H, 42H, 4H,		2EH, 63H, 43H, 4H,		20H, 64H, 44H, 4H
	DB 12H, 65H, 45H, 4H,		21H, 66H, 46H, 4H,		22H, 67H, 47H, 4H,		23H, 68H, 48H, 4H
	DB 17H, 69H, 49H, 4H,		24H, 6AH, 4AH, 4H,	25H, 6BH, 4BH, 4H,	26H, 6CH, 4CH, 4H
	DB 32H, 6DH, 4DH, 4H,	31H, 6EH, 4EH, 4H,	18H, 6FH, 4FH, 4H,		19H, 70H, 50H, 4H
	DB 10H, 71H, 51H, 4H,		13H, 72H, 52H, 4H,		1FH, 73H, 53H, 4H,		14H, 74H, 54H, 4H
	DB 16H, 75H, 55H, 4H,		2FH, 76H, 56H, 4H,		11H, 77H, 57H, 4H,		2DH, 78H, 58H, 4H
	DB 15H, 79H, 59H, 4H,		2CH, 7AH, 5AH, 4H

		; dead keys (LAlt & RAlt)
	DB 07H, 0FFH, 1H, 9H,		28H, 2H, 5H, 9H,	29H, 3H, 4H, 9H
	DB 07H, 0FFH, 1H, 11H,	28H, 2H, 5H, 11H,	29H, 3H, 4H, 11H

		; numbers at top
	DB 0BH, 30H, 29H, 0H,		02H, 31H, 21H, 0H,		03H, 32H, 40H, 0H,		04H, 33H, 23H, 0H
	DB 05H, 34H, 24H, 0H,		06H, 35H, 25H, 0H,		07H, 36H, 5EH, 0H,		08H, 37H, 26H, 0H
	DB 09H, 38H, 2AH, 0H,		0AH, 39H, 28H, 0H
		; symbol keys
	DB 28H, 27H, 22H, 0H,		33H, 2CH, 3CH, 0H,	0CH, 2DH, 5FH, 0H,	34H, 2EH, 3EH, 0H
	DB 35H, 2FH, 3FH, 0H,		27H, 3BH, 3AH, 0H,	0DH, 3DH, 2BH, 0H,	1AH, 5BH, 7BH, 0H
	DB 2BH, 5CH, 7CH, 0H,	1BH, 5DH, 7DH, 0H,	29H, 60H, 7EH, 0H
		; control keys
	DB 0EH, 7FH, 7FH, 0H ; backspace
	DB 0FH, 09H, 09H, 0H ; tab
	DB 1CH, 0DH, 0DH, 0H ; enter
	DB 39H, 20H, 20H, 0H ; space
	DB 01H, 0FEH, 1BH, 0H ; esc
		; keypad
	DB 4FH, 0A9H, 31H, 2H ; end/1
	DB 50H, 0C2H, 32H, 2H ; down/2
	DB 51H, 0A3H, 33H, 2H ; pgdn/3
	DB 4BH, 0C4H, 34H, 2H ; left/4
	DB 4CH, 0FFH, 35H, 2H ; center/5
	DB 4DH, 0C3H, 36H, 2H ; right/6
	DB 47H, 0A8H, 37H, 2H ; home/7
	DB 48H, 0C1H, 38H, 2H ; up/8
	DB 49H, 0A2H, 39H, 2H ; pgup/9
	DB 52H, 0A0H, 30H, 2H ; insert/0
	DB 53H, 0A1H, 2EH, 2H ; del/.
		; gray keys
	DB 4AH, 2DH, 2DH, 0H ; gray -
	DB 4EH, 2BH, 2BH, 0H ; gray +
	DB 0B5H, 2FH, 2FH, 0H ; gray /
	DB 37H, 2AH, 2AH, 0H ; gray *
	DB 0D0H, 0C2H, 0C2H, 0H ; gray down
	DB 0CBH, 0C4H, 0C4H, 0H ; gray left
	DB 0CDH, 0C3H, 0C3H, 0H ; gray right
	DB 0C8H, 0C1H, 0C1H, 0H ; gray up
	DB 09CH, 0DH, 0DH, 0H ; gray enter
	DB 0D2H, 0A0H, 0A0H, 0H ; gray ins
	DB 0D3H, 0A1H, 0A1H, 0H ; gray del
	DB 0C9H, 0A2H, 0A2H, 0H ; gray pgup
	DB 0D1H, 0A3H, 0A3H, 0H ; gray pgdn
	DB 0C7H, 0A8H, 0A8H, 0H ; gray home
	DB 0CFH, 0A9H, 0A9H, 0H ; gray end
		; function keys
	DB 3BH, 0A4H, 0FFH, 0H ; F1
	DB 3CH, 0A5H, 0FFH, 0H ; F2
	DB 3DH, 1BH, 0FFH, 0H ; F3
	DB 3EH, 0A7H, 0FFH, 0H ; F4
	DB 3FH, 0F5H, 0FFH, 0H ; F5
	DB 40H, 0F6H, 0FFH, 0H ; F6
	DB 41H, 0F7H, 0FFH, 0H ; F7
	DB 42H, 0F8H, 0FFH, 0H ; F8
	DB 43H, 0F9H, 0FFH, 0H ; F9
	DB 44H, 0FAH, 0FFH, 0H ; F10
	DB 57H, 0FBH, 0FFH, 0H ; F11
	DB 58H, 0FCH, 0FFH, 0H ; F12
	DB 0FFH
кон TableUS;

(* maps USB usage ID's to Oberon character code *)
проц UsbScanTab*() : адресВПамяти;
машКод
	; Return address of first table element
	ADD	R0, PC, #Base-8-$
	MOV	SP, FP
	LDR	FP, [SP], #4
	LDR	LR, [SP], #4
	BX		LR
	; Table
Base:
	DB 000, 000, 000, 000, 030, 048, 046, 032, 018, 033, 034, 035, 023, 036, 037, 038
	DB 050, 049, 024, 025, 016, 019, 031, 020, 022, 047, 017, 045, 021 ,044, 002, 003
	DB 004, 005, 006, 007, 008, 009, 010, 011, 028, 001, 014, 015 ,057, 012, 013, 026
	DB 027, 043, 043, 039, 040, 041, 051, 052, 053, 058, 059, 060, 061, 062, 063, 064
	DB 065, 066, 067, 068, 087, 088, 099, 070, 119, 210, 199, 201, 211, 207, 209, 205
	DB 203, 208, 200, 069, 181, 055, 074, 078, 156, 079, 080, 081, 075, 076, 077, 071
	DB 072, 073, 082, 083, 086, 127, 116, 117, 085, 089, 090, 091, 092, 093, 094, 095
	DB 120, 121, 122, 123, 134, 138, 130, 132, 128, 129, 131, 137, 133, 135, 136, 113
	DB 115, 114, 000, 000, 000, 000, 000, 124, 000, 000, 000, 000, 000, 000, 000, 000
	DB 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000
	DB 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000
	DB 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000
	DB 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000
	DB 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000, 000
	DB 029, 042, 056, 125, 097, 054, 100, 126, 164, 166, 165, 163, 161, 115, 114, 113
	DB 150, 158, 159, 128, 136, 177, 178, 176, 142, 152, 173, 140, 000, 000, 000, 000
кон UsbScanTab;

(* Maps USB key code to X11 keysym (/usr/include/X11/keysymdef.h). *)
проц KeySym*(перем ch : симв8; перем leds : мнвоНаБитахМЗ): цел32;
перем res: цел32;
нач
	просей ch из
		028X: res := Inputs.KsReturn (* Return *)
	    	|029X: res := Inputs.KsEscape (* Escape *)
	    	|02AX: res := Inputs.KsBackSpace (* Delete (Backspace) *)
	   	|02BX: res := Inputs.KsTab (* Tab *)
		|03AX: res := Inputs.KsF1 (* f1 *)
		|03BX: res := Inputs.KsF2 (* f2 *)
		|03CX: res := Inputs.KsF3 (* f3 *)
		|03DX: res := Inputs.KsF4 (* f4 *)
		|03EX: res := Inputs.KsF5 (* f5 *)
		|03FX: res := Inputs.KsF6 (* f6 *)
		|040X: res := Inputs.KsF7 (* f7 *)
		|041X: res := Inputs.KsF8 (* f8 *)
		|042X: res := Inputs.KsF9 (* f9 *)
		|043X: res := Inputs.KsF10 (* f10 *)
		|044X: res := Inputs.KsF11 (* f11 *)
		|045X: res := Inputs.KsF12 (* f12 *)
		|046X: res := Inputs.KsPrint (* Printscreen *)
		|047X: res := Inputs.KsScrollLock (* ScrollLock *)
		|048X: res := Inputs.KsPause (* Pause *)
		|049X: res := Inputs.KsInsert (* insert *)
		|04AX: res := Inputs.KsHome (* home *)
		|04BX: res := Inputs.KsPageUp (* pgup *)
		|04CX: res := Inputs.KsDelete (* delete *)
		|04DX: res := Inputs.KsEnd (* end *)
		|04EX: res := Inputs.KsPageDown (* pgdn *)
		|04FX: res := Inputs.KsRight (* right *)
		|050X: res := Inputs.KsLeft (* left *)
		|051X: res := Inputs.KsDown (* down *)
		|052X: res := Inputs.KsUp (* up *)
		|053X: res := Inputs.KsNumLock; (* Keypad NumLock *)
		|054X: res := Inputs.KsKPDivide (* Keypad / *)
		|055X: res := Inputs.KsKPMultiply (* Keypad * *)
		|056X: res := Inputs.KsKPSubtract (* Keypad - *)
		|057X: res := Inputs.KsKPAdd (* Keypad + *)
		|058X: res := Inputs.KsReturn (* Keypad Enter: Should be KsKPEnter *)
		|059X: если ~(NumLock в leds) то res := Inputs.KsEnd; иначе res := Inputs.KsNil всё;  (* Keypad 1 and End *)
		|05AX: если ~(NumLock в leds) то res := Inputs.KsDown; иначе res := Inputs.KsNil всё;  (* Keypad 2 and Down Arrow *)
		|05BX: если ~(NumLock в leds) то res := Inputs.KsPageDown; иначе res := Inputs.KsNil всё;  (* Keypad 3 and PageDown *)
		|05CX: если ~(NumLock в leds) то res := Inputs.KsLeft; иначе res := Inputs.KsNil всё;  (* Keypad 4 and Left Arrow *)
		|05DX: если ~(NumLock в leds) то ch := 0X; res := Inputs.KsNil; иначе res := Inputs.KsNil всё; (* don't report key event !! *)
		|05EX: если ~(NumLock в leds) то res := Inputs.KsRight; иначе res := Inputs.KsNil всё;  (* Keypad 6 and Right Arrow *)
		|05FX: если ~(NumLock в leds) то res := Inputs.KsHome; иначе res := Inputs.KsNil всё;  (* Keypad 7 and Home *)
		|060X: если ~(NumLock в leds) то res := Inputs.KsUp; иначе res := Inputs.KsNil всё;  (* Keypad 8 and Up Arrow *)
		|061X: если ~(NumLock в leds) то res := Inputs.KsPageUp; иначе res := Inputs.KsNil всё;  (* Keypad 9 and Page Up *)
		|062X: если ~(NumLock в leds) то res := Inputs.KsInsert; иначе res := Inputs.KsNil всё;  (* Keypad 0 and Insert *)
		|063X: если ~(NumLock в leds) то res := Inputs.KsDelete; иначе res := Inputs.KsNil всё;  (* Keypad . and Delete *)
		|067X:  если ~(NumLock в leds) то ch := 028X; res := Inputs.KsKPEnter; иначе res := Inputs.KsNil всё;  (* Keypad =; remap to KpEnter *)
		|0B0X: ch := 0X; res := Inputs.KsNil;  (* Keypad 00; don't map *)
		|0B1X: ch := 0X; res := Inputs.KsNil;  (* Keypad 000; don't map *)
		|09AX: res := Inputs.KsSysReq (* SysReq / Attention *)
		|0E0X: res := Inputs.KsControlL (* Left Control *)
		|0E1X: res := Inputs.KsShiftL (* Left Shift *)
		|0E2X: res := Inputs.KsAltL (* Left Alt *)
		|0E3X: res := Inputs.KsMetaL (* Left GUI *)
		|0E4X: res := Inputs.KsControlR (* Right Control *)
		|0E5X: res := Inputs.KsShiftR (* Right Shift *)
		|0E6X: res := Inputs.KsAltR (* Right Alt *)
		|0E7X: res := Inputs.KsMetaR (* Right GUI *)
		|076X: res := Inputs.KsMenu (* Windows Menu *)
		|0FFX: res := Inputs.KsBreak (* Break *)
	иначе
		(* if res=Inputs.KsNil, the KeySym will be assigned later (see HandleKey) *)
		res := Inputs.KsNil (* no key *)
	всё;
	возврат res
кон KeySym;

проц GetModifierKeysym(modifier : цел32) : цел32;
перем res : цел32;
нач
	просей modifier из
		|Inputs.LeftCtrl: res := Inputs.KsControlL;
		|Inputs.LeftShift: res := Inputs.KsShiftL;
		|Inputs.LeftAlt: res := Inputs.KsAltL;
		|Inputs.LeftMeta: res := Inputs.KsMetaL;
		|Inputs.RightCtrl: res := Inputs.KsControlR;
		|Inputs.RightShift: res := Inputs.KsShiftR;
		|Inputs.RightAlt: res := Inputs.KsAltR;
		|Inputs.RightMeta: res := Inputs.KsMetaR;
	иначе
		res := Inputs.KsNil;
	всё;
	возврат res;
кон GetModifierKeysym;

(* Translate - Translate scan code "c" to key. *)
проц Translate(flags, leds: мнвоНаБитахМЗ;  c: симв8; keyboardTable : адресВПамяти; перем keyboardDeadKey, keyboardKeyVal : цел32): цел32;
конст
	(* The flags stored in the keytable are not the same as the ones defined in Inputs.
		The parameter flags and leds use the Inputs constants.
		The constants below are for the use of the flags stored in the keytable (variable s) *)
	OScrollLock = 0;
	ONumLock = 1;
	OCapsLock = 2;
	LAlt = 3;
	RAlt = 4;
	LCtrl = 5;
	RCtrl = 6;
	LShift = 7;
	RShift = 8;
	GreyEsc = 9;
	LMeta = 13;
	RMeta = 14;
	Alt = {LAlt, RAlt};
	Ctrl = {LCtrl, RCtrl};
	Shift = {LShift, RShift};
	DeadKey = 0;
перем
	a: адресВПамяти;
	s1: симв8;
	s : мнвоНаБитахМЗ;
	k: цел16;
	dkn: цел8;
нач
	если (c = 46X) и (flags * Inputs.Ctrl # {}) то возврат -2 всё;  (* Ctrl-Break - break *)
	если (c = 44X) и (flags * Inputs.Ctrl # {}) то возврат 0FFH всё;	(* Ctrl-F10 - exit *)
	если (c = 53X) и (flags * Inputs.Ctrl # {}) и (flags * Inputs.Alt # {}) то возврат 0FFH всё; (* Ctrl-Alt-Del - exit *)

	a := keyboardTable;

	(* this loop linearly searches the keytable for an entry for the character c *)
	нц
		НИЗКОУР.прочтиОбъектПоАдресу(a, s1);

		если s1 = 0FFX то (* end of table -> unmapped key *)

			(* reset key and dead key state *)
			k := -1;  keyboardDeadKey := 0;  прервиЦикл;

		аесли s1 = c то (* found scan code in table *)

			k := 0;

			НИЗКОУР.прочтиОбъектПоАдресу(a+3, НИЗКОУР.подмениТипЗначения(симв8, s)); (* flags from table *)
			dkn := устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(НИЗКОУР.подмениТипЗначения(цел32, логСдвиг(s * {5..7}, -5))));

			s := s * {DeadKey, ONumLock, OCapsLock, LAlt, RAlt, LCtrl, RCtrl};

			если ((s * Alt = логСдвиг(flags * Inputs.Alt,-2)) или (ONumLock в s) или (s1>03BX))  и (dkn = keyboardDeadKey) то	(* Alt & dead keys match exactly *)

				(* check if shift pressed *)
				если flags * Inputs.Shift # {} то включиВоМнвоНаБитах(s, LShift) всё;

				(* handle CapsLock *)
				если (OCapsLock в s) и (CapsLock в leds) то s := s / {LShift} всё;

				(* handle NumLock *)
				если ONumLock в s то
					если flags * Inputs.Alt # {} то включиВоМнвоНаБитах(s, LShift)
					аесли NumLock в leds то s := s / {LShift}
					всё
				всё;

				(* get key code *)
				если LShift в s то НИЗКОУР.прочтиОбъектПоАдресу(a+2, НИЗКОУР.подмениТипЗначения(симв8, k))	(* shifted value *)
				иначе НИЗКОУР.прочтиОбъектПоАдресу(a+1, НИЗКОУР.подмениТипЗначения(симв8, k))	(* unshifted value *)
				всё;

				если (DeadKey в s) и (k <= 7) то (* dead key *)
					keyboardDeadKey := устарПреобразуйКБолееУзкомуЦел(k);  k := -1	(* set new dead key state *)
				аесли k = 0FFH то	(* unmapped key *)
					k := -1;  keyboardDeadKey := 0	(* reset dead key state *)
				иначе	(* mapped key *)
					если flags * Inputs.Ctrl # {} то
						если ((k >= 64) и (k <= 95)) или ((k >= 97) и (k <= 122)) то
							k := устарПреобразуйКБолееУзкомуЦел(НИЗКОУР.подмениТипЗначения(цел32, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, адресВПамяти(k)) * {0..4}))	(* control *)
						аесли k = 13 то	(* Ctrl-Enter *)
							k := 10
						всё
					всё;
					если flags * Inputs.Alt # {} то	(* Alt-keypad *)
						если (k >= кодСимв8("0")) и (k <= кодСимв8("9")) и (NumLock в s) то	(* keypad num *)
							если keyboardKeyVal = -1 то keyboardKeyVal := k-кодСимв8("0")
							иначе keyboardKeyVal := (10*keyboardKeyVal + (k-кодСимв8("0"))) остОтДеленияНа 1000;
							всё;
							k := -1
						всё
					всё;
					keyboardDeadKey := 0	(* reset dead key state *)
				всё;
				прервиЦикл
			всё
		всё;
		увел(a, 4)
	кц; (* LOOP *)
	возврат k
кон Translate;

(* Displays textual representation of the set flags to KernelLog *)
проц ShowFlags(flags, leds : мнвоНаБитахМЗ);
нач
	ЛогЯдра.пСтроку8("Flags: ");
	если Inputs.LeftAlt в flags то ЛогЯдра.пСтроку8("[Left Alt]"); всё;
	если Inputs.RightAlt в flags то ЛогЯдра.пСтроку8("[Right Alt]"); всё;
	если Inputs.LeftCtrl в flags то ЛогЯдра.пСтроку8("[Left Ctrl]"); всё;
	если Inputs.RightCtrl в flags то ЛогЯдра.пСтроку8("[Rigth Ctrl]"); всё;
	если Inputs.LeftShift в flags то ЛогЯдра.пСтроку8("[Left Shift]"); всё;
	если Inputs.RightShift в flags то ЛогЯдра.пСтроку8("[Right Shift]"); всё;
	если Inputs.LeftMeta в flags то ЛогЯдра.пСтроку8("[Left Meta]"); всё;
	если Inputs.RightMeta в flags то ЛогЯдра.пСтроку8("[Rigth Meta]"); всё;
	если Inputs.Release в flags то ЛогЯдра.пСтроку8("[Released]"); всё;
	если ScrollLock в leds то ЛогЯдра.пСтроку8("[ScrollLock]"); всё;
	если NumLock в leds то ЛогЯдра.пСтроку8("[NumLock]"); всё;
	если CapsLock в leds то ЛогЯдра.пСтроку8("[CapsLock]"); всё;
	если Compose в leds то ЛогЯдра.пСтроку8("[Compose]"); всё;
	если Kana в leds то ЛогЯдра.пСтроку8("[Kana]"); всё;
кон ShowFlags;

проц Probe(dev : Usbdi.UsbDevice; if : Usbdi.InterfaceDescriptor) : Usbdi.Driver;
перем driver : KeyboardDriver;
нач
	если if.bInterfaceClass # 3 то возврат НУЛЬ всё; (* HID class *)
	если if.bInterfaceSubClass # 1 то возврат НУЛЬ всё; (* Boot protocol subclass *)
	если if.bInterfaceProtocol # 1 то возврат НУЛЬ всё; (* Keyboard *)
	если if.bNumEndpoints # 1 то возврат НУЛЬ всё;
	ЛогЯдра.пСтроку8("UsbKeyboard: USB Keyboard found."); ЛогЯдра.пВК_ПС;
	нов(driver);
	возврат driver;
кон Probe;

проц SetLayout*(context : Commands.Context); (** dev file ~ *)
перем
	string : массив 64 из симв8;
	plugin : Plugins.Plugin; kd : KeyboardDriver;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string) то
		plugin := Usb.usbDrivers.Get(string);
		если plugin # НУЛЬ то
			если plugin суть KeyboardDriver то
				kd := plugin (KeyboardDriver);
			иначе context.error.пСтроку8("UsbKeyboard: Device "); context.error.пСтроку8(string); context.error.пСтроку8(" is not a keyboard."); context.error.пВК_ПС;
			всё;
		иначе context.error.пСтроку8("UsbKeyboard: Device "); context.error.пСтроку8(string); context.error.пСтроку8(" not found."); context.error.пВК_ПС;
		всё;
	иначе context.error.пСтроку8("UsbKeyboard: Expected <dev> parameter."); context.error.пВК_ПС;
	всё;
	если kd # НУЛЬ то
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(string) то
			kd.base.SetLayout(string);
			context.out.пСтроку8("Layout set to "); context.out.пСтроку8(string); context.out.пВК_ПС;
		всё;
	всё;
кон SetLayout;

проц Install*;
кон Install;

проц Cleanup;
нач
	Usbdi.drivers.Remove(Name);
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	Usbdi.drivers.Add(Probe, Name, Description, Priority);
кон UsbKeyboard.

UsbKeyboard.Install ~ System.Free UsbKeyboard ~

UsbKeyboard.SetLayout UsbKeyboard00 KeyBE.Bin ~
UsbKeyboard.SetLayout UsbKeyboard00 KeyCA.Bin ~
UsbKeyboard.SetLayout UsbKeyboard00 KeyCH.Bin ~
UsbKeyboard.SetLayout UsbKeyboard00 KeyD.Bin ~
UsbKeyboard.SetLayout UsbKeyboard00 KeyDV.Bin ~
UsbKeyboard.SetLayout UsbKeyboard00 KeyFR.Bin ~
UsbKeyboard.SetLayout UsbKeyboard00 KeyIT.Bin ~
UsbKeyboard.SetLayout UsbKeyboard00 KeyN.Bin ~
UsbKeyboard.SetLayout UsbKeyboard00 KeyPL.Bin ~
UsbKeyboard.SetLayout UsbKeyboard00 KeySF.Bin ~
UsbKeyboard.SetLayout UsbKeyboard00 KeyTR.Bin ~
UsbKeyboard.SetLayout UsbKeyboard00 KeyUK.Bin ~
UsbKeyboard.SetLayout UsbKeyboard00 KeyUS.Bin ~

WMKeyCode.Open ~	System.Free WMKeyCode ~
