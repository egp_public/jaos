(* Runtime support for ACPI *)
(* Copyright (C) Florian Negele *)

модуль ACPI;

конст HPETSignature* = 54455048H;
конст RDSPSignature* = 2052545020445352H;

тип Address* = запись
	value: цел32;
кон;

тип Header* = запись
	signature-: цел32;
	length-: цел32;
	revision-: цел8;
	checksum-: цел8;
	oemID-: массив 6 из симв8;
	oemTableID-: массив 8 из симв8;
	oemRevision-: цел32;
	creatorID-: цел32;
	creatorRevision-: цел32;
кон;

тип HPET* = запись (Header)
	eventTimerBlockID-: цел32;
	baseAddress-: запись
		addressSpaceID-: цел8;
		registerBitWidth-: цел8;
		registerBitOffset-: цел8;
		reserved-: цел8;
		addressLow-: Address;
		addressHigh-: Address;
	кон;
	hpetNumber-: цел8;
	mainCounterMinimum-: цел16;
	oemAttribute-: цел8;
кон;

тип RDSP* = запись
	signature-: цел64;
	checksum-: цел8;
	oemID-: массив 6 из симв8;
	revision-: цел8;
	rsdtAddress-: Address;
кон;

тип RSDT* = запись (Header)
	entry-: массив 1024 из Address;
кон;

перем rdsp- {неОтслСборщиком} := НУЛЬ: укль {опасныйДоступКПамяти} на RDSP;

проц Checksum (address: адресВПамяти; size: размерМЗ): цел8;
перем checksum := 0: цел8; block {неОтслСборщиком}: укль {опасныйДоступКПамяти} на массив из цел8;
нач {безКооперации, безОбычныхДинПроверок} block := address; нцПока size # 0 делай умень (size); увел (checksum, block[size]) кц; возврат checksum;
кон Checksum;

проц Convert- (конст address: Address): адресВПамяти;
перем result: адресВПамяти; masked {неОтслСборщиком}: укль {опасныйДоступКПамяти} на запись value: мнвоНаБитахМЗ кон;
нач {безКооперации, безОбычныхДинПроверок}
	result := address.value; masked := операцияАдресОт result;
	masked.value := masked.value * {0 .. размерОт цел32 * 8 - 1};
	возврат result;
кон Convert;

проц GetTable- (signature: цел32): адресВПамяти;
перем address: адресВПамяти; rsdt {неОтслСборщиком}: укль {опасныйДоступКПамяти} на RSDT; header {неОтслСборщиком}: укль {опасныйДоступКПамяти} на Header; i: размерМЗ;
нач {безКооперации, безОбычныхДинПроверок}
	утв (rdsp # НУЛЬ);
	address := Convert (rdsp.rsdtAddress); rsdt := address;
	утв (Checksum (rsdt, rsdt.length) = 0);
	нцДля i := 0 до (rsdt.length - 36) DIV 4 делай
		address := Convert (rsdt.entry[i]); header := address;
		если (header.signature = signature) и (Checksum (header, header.length) = 0) то возврат header всё;
	кц;
	возврат НУЛЬ;
кон GetTable;

проц Initialize-;
тип BiosEBDA = укль {опасныйДоступКПамяти} на запись val: цел16 кон;
перем address := 0E0000H: адресВПамяти; size := 020000H: размерМЗ; biosEBDA {неОтслСборщиком}: BiosEBDA;

нач {безКооперации, безОбычныхДинПроверок}
	нцПока size # 0 делай
		rdsp := address;
		если (rdsp.signature = RDSPSignature) и (Checksum (address, 20) = 0) то возврат всё;
		увел (address, 16); умень (size, 16);
	кц;

	biosEBDA := 40EH;
	(* try Extended Bios Data Area EBDA *)
	address := адресВПамяти(biosEBDA.val) остОтДеленияНа 10000H * 16;
	size := 1024;
	нцПока size # 0 делай
		rdsp := address;
		если (rdsp.signature = RDSPSignature) и (Checksum (address, 20) = 0) то возврат всё;
		увел (address, 16); умень (size, 16);
	кц;
	rdsp := НУЛЬ;
кон Initialize;


кон ACPI.
