модуль CDRecordLib;

использует НИЗКОУР, ЛогЯдра, Ata := ATADisks, Utils := CDRecordUtils;

конст
	TrackLimit = 99;
	ResOk = 0; ResErr = 1;
	Trace = ложь;

	Ignore* = 0H;

	(* Mode Pages *)

	(* Page Codes *)
	MPCapabilities* = 2AH;
	MPWriteParameters* = 5H;

	(* Page Control *)
	MPCurrent* = 0H;
	MPChangeable* = 1H;
	MPDefault* = 2H;
	MPSaved* = 3H;

	MPPSBit* = 7;

	(* Capabilities *)
	MPCCdrBit* = 0;
	MPCCdRwBit* = 1;
	MPCBufeBit* = 7;
	MPCMultisessionBit* = 5;

	(* Loading Mechanism *)
	LMTMask* = {5..7}; LMTOfs* = 5;
	LMTCaddy* = 0;
	LMTTray* = 1;
	LMTPopUp * = 2;


	(* Write Parameters *)
	MPWBufeBit* = 6;
	MPWTestWriteBit* = 4;

	MPWWriteTypeMask* = {0..3}; MPWWriteTypeOfs* = 0;
	WTPacket* = 0H;
	WTTao* = 1H;
	WTSao* = 2H;
	WTRaw* = 3H;
	WTLayerJump* = 4H;

	MPWTrackModeMask* = {0..3}; MPWTrackModeOfs* = 0; (* ECMA 130. p. 20 Control Nibble *)
	(* Track Mode is the q channel control nibble. See below *)

	MPWDataBlockMask* = {0..3}; MPWDataBlockOfs* = 0;
	DBRaw* = 0H; (* 2352 *)
	DBIsoMode1* = 8H;
	DBIsoMode2* = 9H;

	(* multisession *)
	MPWMultisessionMask* = {6,7}; MPWMultisessionOfs* = 6;
	MSNoNextSessNoB0* = 0H;
	MSNoNextSessB0* = 1H;
	MSNextSessB0* = 3H;

	(* Features *)
	FMorphing* = 2H;
	FMastering* = 2EH;

	FAll* = 0H;
	FCurrent* = 1H;
	FOne* = 2H;

	(* CD Mastering *)
	FDMSaoBit* = 5;

	(* ReadTrackInformation *)
	TRAdrType0 = 0H;
	TRAdrType1 = 1H;
	TRAdrType2 = 2H;
	TRAdrType3 = 3H;

	TRInvisible* = 0FFH;

	(* Read Toc *)

	TCFormatToc* = 0H;
	TCFormatSessionInfo* = 1H;
	TCFormatFullToc* = 2H;
	TCFormatPMA* = 3H;
	TCFormatATIP* = 4H;
	TCFormatCDText* = 5H;

	(* track descriptor *)
	(* Control is the q channel control nibble. see below *)
	TCControlMask* = {0..3}; TCControlOfs* = 0; (* control nibble *)

	(* ATIP *)
	ATCdRwBit* = 6;
	ATA1ValidBit* = 2; ATA2ValidBit* = 1; ATA3ValidBit* = 0;

	ATSubTypeMask* = {3..5}; ATSubTypeOfs* = 3;
	(* cdr subtypes orange book *)
	ATCdrNormal*= 0H;
	ATCdrHighSpeed* =1H;

	(* cdrw subtypes *)
	ATCdRwStandardSpeed* = 0H;
	ATCdRwHighSpeed* = 1H;
	ATCdRwUltraHighSpeed* = 2H;
	ATCdRwUltraHighSpeedPlus* = 3H;

	ATRefSpeedMask* ={0..2}; ATRefSpeedOfs*=0;
	(* A1 / A2 / A3 Values *)
	ATCLVHighMask* = {0..3}; ATCLVHighOfs* = 0;
	ATCLVLowMask* = {4..6}; ATCLVLowOfs* = 4;

	(* disc information block *)
	(* Data Type *)
	DTDiscInfoBlock* =  0H;
	DTAssignedTrack* = 1H;

	DIBErasableBit* = 4;

	(*Disc Status *)
	DIBDiscStatusMask* = {0,1}; DIBDiscStatusOfs* = 0;
	DSEmpty* = 0H;
	DSAppendable* = 1H;
	DSComplete* = 2H;
	DSOtherStatus* =4H;

	(* Last Session Status *)
	DIBSessionStatusMask* = {2, 3}; DIBSessionStatusOfs*= 2;
	LSSEmpty* = 0H;
	LSSIncomplete* = 1H;
	LSSComplete* = 3H;

	(* disc types *)
	DTCdDACdRom* = 0H;
	DTCdI* = 10H;
	DTCdRomXA* = 20H;
	DTUndefined* = 0FFH;

	(* Cue Sheet *)
	(* Ctl/Adr *)
	CTLMask* = {4..7}; CTLOfs* = 4;
	(* CTL is the q channel control nibble. see below *)

	ADRMask* = {0..3}; ADROfs* = 0;
	ADRTno* = 1H;
	ADRCatalogCode* = 2H;
	ADRIsrcCode* =2H;

	(* main data form *)
	DFMLeadin* = 1H;
	DFMLeadout* = 1H;
	DFMDigitalAudio* = 0H;
	DFMCdRomMode1* = 10H;
	DFMMask* = {0..5}; DFMOfs* = 0;

	(* Close Session *)
	(* Close Function *)
	CFTrack* = 1H;
	CFSession* = 2H;

	(* q Channel Control nibble *)
	QCWithPreEmphasis* = 0;
	QCCopyPermitted* = 1;
	QCDataTrack* = 2;
	QC4ChannelAudio* = 3;

	(* Blank Disc*)
	BDEntire* = 0H;
	BDQuick* = 1H;
	BDTrack* = 2H; (* optional *)

	(* get performance *)
	PTypeWriteSpeed* = 3H;

	(* Read CD *)
	(* sector type *)
	STAny* = 0;
	STCdDa* = 1;
	STMode1* = 2;
	STMode2* = 3;
	STMode2Form1* = 4;
	STMode2Form2*= 5;

	(* header *)
	HNone*= НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, арифмСдвиг(0, 5));
	HOnlyHeader* = НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, арифмСдвиг(1, 5));
	HOnlySubHeader* = НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, арифмСдвиг(2, 5));
	HAllHeaders* = НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, арифмСдвиг(3, 5));

	(* error flags *)
	EFNone* = НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, арифмСдвиг(0,1));
	EFC2* = НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, арифмСдвиг(1, 1));
	EFC2Block *= НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, арифмСдвиг(2, 1));

	EDC* = {3};
	Sync*= {7};
	UserData* = {4};

	(* Sub-Channel *)
	SCNoData* = 0;
	SCRaw* = 1;

тип
	(* Read Toc *)

	TocDescriptor* = запись
		Reserved1: симв8;
		Byte1*: симв8;
		TNO*: симв8;
		Reserved2: симв8;
		TrackStartAdr*: массив 4 из симв8;
	кон;

	TocHeader* = запись
		DataLength*: массив 2 из симв8;
		FirstTrackNo*: симв8;
		LastTrackNo*: симв8;
	кон;

	SessionInfo* = запись
		DataLength*: массив 2 из симв8; (* 0Ah *)
		FirstComplSess*: симв8;
		LastComplSess*: симв8;
		Reserved1: симв8;
		Byte1*: симв8;
		FirstTNOLastSess*: симв8;
		Reserved2: симв8;
		StartAdrFirstTrack*: массив 4 из симв8;
	кон;

	FullTocDescriptor* = запись
		SessionNo*: симв8;
		AdrCtrl*: симв8;
		TNO*: симв8;
		Point*: симв8;
		Min*: симв8;
		Sec*: симв8;
		Frame*: симв8;
		Zero*: симв8;
		PMin*: симв8;
		PSec*: симв8;
		PFrame*: симв8;
	кон;

	FullTocHeader* = запись
		DataLength: массив 2 из симв8;
		FirstComplSessNo*, LastComplSessNo*: симв8;
	кон;

	PmaDescriptor* = запись
		Reserved: симв8;
		AdrCtrl*: симв8;
		TNO*: симв8;
		Point*: симв8;
		Min*: симв8;
		Sec*: симв8;
		Frame*: симв8;
		Zero*: симв8;
		PMin*: симв8;
		PSec*: симв8;
		PFrame*: симв8;
	кон;

	PmaHeader* = запись
		DataLength*: массив 2 из симв8;
		Reserved1: массив 2 из симв8;
	кон;

	ATIPHeader* = PmaHeader;

	ATIPDescriptor* = запись
		Byte0*, Byte1*, Byte2*: симв8;
		Reserved1: симв8;
		LeadInMin*, LeadInSec*, LeadInFrame*: симв8;
		Reserved2: симв8;
		LastLeadOutMin*, LastLeadOutSec*, LastLeadOutFrame*: симв8;
		Reserved3: симв8;
		A1Values*: массив 3 из симв8;
		Reserved4: симв8;
		A2Values*: массив 3 из симв8;
		Reserved5: симв8;
		A3Values*: массив 3 из симв8;
		Reserved6: симв8;
	кон;
	ATIPDescriptorPtr* = укль на ATIPDescriptor;

	(* Read Track Information *)

	TrackInfo* = запись
		InfoLength*: массив 2 из симв8;
		TrackNo*: симв8;
		SessionNo*: симв8;
		Reserved1*: симв8;
		Byte5*, Byte6*, Byte7*: симв8;
		StartAdr*: массив 4 из симв8;
		NextWriteAdr*: массив 4 из симв8;
		FreeBlocks*: массив 4 из симв8;
		PacketSize*: массив 4 из симв8;
		Size*: массив 4 из симв8;
	кон;

	(* Read Disc Information *)

	DiscInfo* = запись
		DataLength*: массив 2 из симв8;
		Byte2*: симв8;
		NoFirstTrack*: симв8;
		NofSessions*: симв8;
		FirstTNOLastSess*: симв8;
		LastTNOLastSess*: симв8;
		Byte7*: симв8;
		DiscType*: симв8;
		Reserved1*: массив 3 из симв8; (* session/tracks <= 99 *)
		DiscIdent*: массив 4 из симв8;
		LeadInLastSess*: массив 4 из симв8;
		LastLeadOut*: массив 4 из симв8;
		BarCode*: массив 8 из симв8;
	кон;

	(* mode pages *)

	ModeHeader* = запись
		DataLength*: массив 2 из симв8;
		Obsolete*: симв8;
		Reserved*: массив 3 из симв8;
		DescrLength*: массив 2 из симв8;
	кон;

	CapabilityPage* = запись
		Header*: ModeHeader;
		Byte0*: симв8;
		Length*: симв8;
		Byte2*, Byte3*, Byte4*, Byte5*, Byte6*, Byte7*: симв8;
		MaxReadSpeed*: массив 2 из симв8; (* obsolete *)
		NoVolumeLevels*: массив 2 из симв8;
		BufferSize*: массив 2 из симв8;
		CurReadSpeed*: массив 2 из симв8; (* obsolete *)
		Obsolete*: симв8;
		Byte17*: симв8;
		MaxWriteSpeed*: массив 2 из симв8; (* obsolete*)
		CurWriteSpeed1*: массив 2 из симв8; (* obsolete *)
		CopyManagement*: массив 2 из симв8;
		Reserved1*: массив 3 из симв8;
		Byte27*: симв8;
		CurWriteSpeed2*: массив 2 из симв8;
		NofWriteDescriptors*: массив 2 из симв8;
	кон;
	CapabilityPagePtr* = укль на CapabilityPage;

	SpeedDescriptor* = запись
		Reserved: симв8;
		Byte1*: симв8;
		WriteSpeed*: массив 2 из симв8; (* kbytes/s*)
	кон;
	SpeedDescriptorPtr* = укль на SpeedDescriptor;

	(* Write Parameters Mode Page *)

	WriteParameterPage* = запись
		Header*: ModeHeader;
		Byte0*: симв8;
		Length*: симв8;
		Byte2*, Byte3*, Byte4*: симв8;
		LinkSize*: симв8;
		Reserved1*: симв8;
		Byte7*: симв8;
		SessionFormat*: симв8;
		Reserved2*: симв8;
		PacketSize*: массив 4 из симв8;
		PauseLength*: массив 2 из симв8;
		CatalogNo*: массив 16 из симв8;
		ISRC*: массив 16 из симв8;
		SubHeader*: массив 4 из симв8;
	кон;

	(* Get Configuration *)

	FeatureHeader = запись
		DataLength: массив 4 из симв8;
		Reserved1: симв8;
		Reserved2: симв8;
		CurProfile: массив 2 из симв8;
	кон;

	MasteringFeature* =  запись
		Header: FeatureHeader;
		FeatureCode*: массив 2 из симв8;
		Byte2*: симв8;
		AdditionalLength*: симв8;
		Byte4*: симв8;
		MaxCueSheetLen*: массив 3 из симв8;
	кон;

	(* Read Buffer Capacity *)

	BufferCapacity* = запись
		DataLength*: массив 2 из симв8;
		Reserved1*: симв8;
		Reserved2*: симв8;
		BufferLength*: массив 4 из симв8;
		BlankLength*: массив 4 из симв8;
	кон;

	(* Get Performance *)

	WriteSpeedHeader* = запись
		DataLength*: массив 4 из симв8;
		Reserved*: массив 4 из симв8;
	кон;

	WriteSpeedDescr* = запись
		Byte0*: симв8;
		Reserved: массив 3 из симв8;
		EndLba*: массив 4 из симв8;
		ReadSpeed*: массив 4 из симв8;
		WriteSpeed*: массив 4 из симв8;
	кон;
	WriteSpeedDescrPtr* = укль на WriteSpeedDescr;

проц GetNextAddress*(dev: Ata.DeviceATAPI; перем adr: цел32): целМЗ;
перем
	info: TrackInfo;
	res: целМЗ;
нач
	res := ReadTrackInformation(dev, ложь, TRAdrType1, TRInvisible, адресОт(info), размер16_от(TrackInfo));
	если res = ResOk то
		adr := Utils.ConvertBE32Int(info.NextWriteAdr);
	всё;
	возврат res;
кон GetNextAddress;

проц ReadSessionInfo*(dev: Ata.DeviceATAPI; перем info: SessionInfo): целМЗ;
нач
	возврат ReadToc(dev, ложь, TCFormatSessionInfo, 0, адресОт(info), размер16_от(SessionInfo));
кон ReadSessionInfo;

проц GetTrackDescriptor*(dev: Ata.DeviceATAPI; tno: цел32; перем toc: TocDescriptor): целМЗ;
перем
	buf: укль на массив из симв8;
	res: целМЗ;
нач
	нов(buf, размер16_от(TocHeader) + размер16_от(TocDescriptor));
	res := ReadToc(dev, ложь, TCFormatToc, tno, адресОт(buf^), длинаМассива(buf)(цел32));
	если res = ResOk то
		НИЗКОУР.копируйПамять(адресОт(buf^) + размер16_от(TocHeader), адресОт(toc), размер16_от(TocDescriptor));
	всё;
	возврат res;
кон GetTrackDescriptor;

проц SetField*(перем byte: симв8; mask: мнвоНаБитахМЗ; ofs, value: цел32);
нач
	byte := НИЗКОУР.подмениТипЗначения(симв8, (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, byte) * (-mask)) + НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, логСдвиг(value, ofs)));
кон SetField;

проц GetField*(byte: симв8; mask: мнвоНаБитахМЗ; ofs: цел32) : цел32;
нач
	возврат логСдвиг(НИЗКОУР.подмениТипЗначения(цел32, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, byte) * mask), -ofs);
кон GetField;

проц SetBit*(перем byte: симв8; bit: цел8);
нач
	утв(bit < 8);
	byte := НИЗКОУР.подмениТипЗначения(симв8, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, byte) + {bit});
кон SetBit;

проц CheckBit*(byte: симв8; bit: цел8): булево;
нач
	возврат bit в НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, byte);
кон CheckBit;

проц ClearBit*(перем byte: симв8; bit: цел8);
нач
	утв(bit < 8);
	byte := НИЗКОУР.подмениТипЗначения(симв8, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, byte) *(-{bit}));
кон ClearBit;


проц CLVToSpeed*(clv: цел32): цел32;
перем
	speed: цел32;
нач
	просей clv из
		1: speed := 2;
		| 2: speed := 4;
		| 3: speed := 6;
		| 4: speed := 8;
		иначе speed := 0;
	всё;
	возврат speed;
кон CLVToSpeed;

проц CLVToHighSpeed*(clv: цел32): цел32;
перем
	speed: цел32;
нач
	просей clv из
		1: speed := 2;
		| 2: speed := 4;
		| 3: speed := 6;
		| 4: speed := 10;
		иначе speed := 0;
	всё;
	возврат speed;
кон CLVToHighSpeed;


проц CLVToUltraHighSpeed*(clv: цел32): цел32;
перем
	speed: цел32;
нач
	просей clv из
		1: speed := 2;
		| 2: speed := 4;
		| 3: speed := 8;
		| 6: speed := 16;
		| 8: speed := 24;
		| 9: speed := 32;
		| 10: speed := 40;
		| 11: speed := 48;
		иначе speed := 0;
	всё;
	возврат speed;
кон CLVToUltraHighSpeed;

проц SetAllocationLength(перем command: Ata.CommandPacket; length: цел32);
нач
	command.packet[7] := симв8ИзКода(арифмСдвиг(length, -8) остОтДеленияНа 100H);
	command.packet[8] := симв8ИзКода(length остОтДеленияНа 100H)
кон SetAllocationLength;


(* additional ATAPI commands *)

проц ModeSense*(dev: Ata.DeviceATAPI; pageControl, pageCode: цел32; adr: адресВПамяти; len: цел32) : целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(5AH);
	command.packet[1] := 8X;

	SetField(command.packet[2], {6, 7}, 6, pageControl);
	SetField(command.packet[2], {0..5}, 0, pageCode);

	SetAllocationLength(command, len);

	command.protocol := Ata.Protocol_PacketPIO;
	command.read := истина;
	command.bufAdr := adr;
	command.size := len;

	res := dev.controller.ExecuteCommand(command, Ata.ATAPITimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" mode sense"); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон ModeSense;

проц Blank*(dev: Ata.DeviceATAPI; immediate: булево; type, tno: цел32): целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(0A1H);
	command.packet[1] := симв8ИзКода(type);
	если immediate то
		SetBit(command.packet[1], 4);
	всё;
	command.packet[2] := симв8ИзКода(арифмСдвиг(tno, -24) остОтДеленияНа 100H);
	command.packet[3] := симв8ИзКода(арифмСдвиг(tno, -16)  остОтДеленияНа 100H);
	command.packet[4] := симв8ИзКода(арифмСдвиг(tno, -8) остОтДеленияНа 100H);
	command.packet[5] := симв8ИзКода(tno остОтДеленияНа 100H);
	command.protocol:= Ata.Protocol_PacketPIO;

	res := dev.controller.ExecuteCommand(command, Ata.ATAPITimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" blank"); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон Blank;

проц SendOPCInformation*(dev: Ata.DeviceATAPI; doOPC: булево): целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(54H);
	если doOPC то
		SetBit(command.packet[1], 0);
	всё;
	SetAllocationLength(command, 0);
	command.protocol := Ata.Protocol_PacketPIO;
	command.read := ложь;
	command.bufAdr := 0;
	command.size := 0;

	res := dev.controller.ExecuteCommand(command, 10*Ata.ATAPITimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" send opc information"); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон SendOPCInformation;

проц GetConfiguration*(dev: Ata.DeviceATAPI; rt, start: цел32; adr: адресВПамяти; len: цел32): целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(46H);

	SetField(command.packet[1], {0,1}, 0, rt);

	command.packet[2] := симв8ИзКода(арифмСдвиг(start, -8) остОтДеленияНа 100H);
	command.packet[3] := симв8ИзКода(start остОтДеленияНа 100H);

	SetAllocationLength(command, len);
	command.protocol := Ata.Protocol_PacketPIO;
	command.read := истина;
	command.bufAdr := adr;
	command.size := len;

	res := dev.controller.ExecuteCommand(command, Ata.ATAPITimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" get configuration"); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон GetConfiguration;

проц SynchronizeCache*(dev: Ata.DeviceATAPI; immediate: булево): целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(35H);
	если immediate то
		SetBit(command.packet[1], 1);
	всё;
	command.protocol:= Ata.Protocol_PacketPIO;

	res := dev.controller.ExecuteCommand(command, Ata.ATAPITimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" synchronize cache "); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон SynchronizeCache;

проц ModeSelect*(dev: Ata.DeviceATAPI;  save: булево; adr: адресВПамяти; len: цел32): целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(55H);
	SetBit(command.packet[1], 4);
	если save то
		SetBit(command.packet[1], 0);
	всё;
	SetAllocationLength(command, len);

	command.protocol := Ata.Protocol_PacketPIO;
	command.read := ложь;
	command.bufAdr := adr;
	command.size := len;

	res := dev.controller.ExecuteCommand(command, Ata.ATAPITimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" mode select"); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон ModeSelect;

проц ReadDiscInformation*(dev: Ata.DeviceATAPI; dataType: цел32; adr: адресВПамяти; len: цел32): целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(51H);
	SetField(command.packet[1], {0..2}, 0, dataType);

	SetAllocationLength(command, len);

	command.protocol := Ata.Protocol_PacketPIO;
	command.read := истина;
	command.bufAdr := adr;
	command.size := len;

	res := dev.controller.ExecuteCommand(command, Ata.ATAPITimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" read disc information "); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон ReadDiscInformation;

проц ReadToc*(dev: Ata.DeviceATAPI; msf: булево; format, tno:  цел32; adr: адресВПамяти; len: цел32): целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(43H);
	если msf то
		SetBit(command.packet[1], 1);
	всё;
	SetField(command.packet[2], {0..3}, 0, format);
	command.packet[6] := симв8ИзКода(tno);

	SetAllocationLength(command, len);

	command.protocol := Ata.Protocol_PacketPIO;
	command.read := истина;
	command.bufAdr := adr;
	command.size := len;

	res := dev.controller.ExecuteCommand(command, Ata.ATAPITimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" read toc "); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон ReadToc;

проц Verify*(dev: Ata.DeviceATAPI; lba, length: цел32): целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(2FH);
	command.packet[2] := симв8ИзКода(арифмСдвиг(lba, -24) остОтДеленияНа 100H);
	command.packet[3] := симв8ИзКода(арифмСдвиг(lba, -16) остОтДеленияНа 100H);
	command.packet[4] := симв8ИзКода(арифмСдвиг(lba, -8) остОтДеленияНа 100H);
	command.packet[5] := симв8ИзКода(lba остОтДеленияНа 100H);

	command.packet[7] := симв8ИзКода(арифмСдвиг(length, -8) остОтДеленияНа 100H);
	command.packet[8] := симв8ИзКода(length остОтДеленияНа 100H);

	command.protocol := Ata.Protocol_PacketPIO;

	res := dev.controller.ExecuteCommand(command, Ata.IOTimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" verify"); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон Verify;

проц ReadCD*(dev: Ata.DeviceATAPI; lba, length, adr, size, type, subChannel: цел32; flags: мнвоНаБитахМЗ; dma: булево): целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(0BEH);

	SetField(command.packet[1], {2..4}, 2, type);

	command.packet[2] := симв8ИзКода(арифмСдвиг(lba, -24) остОтДеленияНа 100H);
	command.packet[3] := симв8ИзКода(арифмСдвиг(lba, -16) остОтДеленияНа 100H);
	command.packet[4] := симв8ИзКода(арифмСдвиг(lba, -8) остОтДеленияНа 100H);
	command.packet[5] := симв8ИзКода(lba остОтДеленияНа 100H);

	command.packet[7] := симв8ИзКода(арифмСдвиг(length, -16) остОтДеленияНа 100H);
	command.packet[7] := симв8ИзКода(арифмСдвиг(length, -8) остОтДеленияНа 100H);
	command.packet[8] := симв8ИзКода(length остОтДеленияНа 100H);

	command.packet[9] := НИЗКОУР.подмениТипЗначения(симв8, flags);

	SetField(command.packet[10], {0..2}, 0, subChannel);

	command.protocol := Ata.Protocol_PacketPIO;
	command.read := истина;
	command.bufAdr := adr;
	command.size := size;
	command.count := length;

	если dma то
		command.protocol := Ata.Protocol_PacketDMA;
		включиВоМнвоНаБитах(command.features, Ata.ATAPI_DMA);
	иначе
		command.protocol := Ata.Protocol_PacketPIO;
	всё;

	res := dev.controller.ExecuteCommand(command, Ata.IOTimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" read cd"); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон ReadCD;

проц GetPerformance*(dev: Ata.DeviceATAPI; type, dataType, lba, maxDescr: цел32; adr: адресВПамяти; len: цел32): целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(0ACH);

	SetField(command.packet[1], {0..4}, 0, dataType);

	command.packet[2] := симв8ИзКода(арифмСдвиг(lba, -24) остОтДеленияНа 100H);
	command.packet[3] := симв8ИзКода(арифмСдвиг(lba, -16)  остОтДеленияНа 100H);
	command.packet[4] := симв8ИзКода(арифмСдвиг(lba, -8) остОтДеленияНа 100H);
	command.packet[5] := симв8ИзКода(lba остОтДеленияНа 100H);

	command.packet[8] := симв8ИзКода(арифмСдвиг(maxDescr, -8) остОтДеленияНа 100H);
	command.packet[9] := симв8ИзКода(maxDescr остОтДеленияНа 100H);

	command.packet[10] := симв8ИзКода(type);

	command.protocol := Ata.Protocol_PacketPIO;
	command.read := истина;
	command.bufAdr := adr;
	command.size := len;

	res := dev.controller.ExecuteCommand(command, Ata.ATAPITimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" get performance "); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон GetPerformance;

проц ReadTrackInformation*(dev: Ata.DeviceATAPI; appendable: булево; adrType, adrnr: цел32; adr: адресВПамяти; len: цел32): целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(52H);
	SetField(command.packet[1], {0,1}, 0, adrType);
	если appendable то
		SetBit(command.packet[1], 2);
	всё;
	command.packet[2] := симв8ИзКода(арифмСдвиг(adrnr, -24) остОтДеленияНа 100H);
	command.packet[3] := симв8ИзКода(арифмСдвиг(adrnr, -16)  остОтДеленияНа 100H);
	command.packet[4] := симв8ИзКода(арифмСдвиг(adrnr, -8) остОтДеленияНа 100H);
	command.packet[5] := симв8ИзКода(adrnr остОтДеленияНа 100H);

	SetAllocationLength(command, len);

	command.protocol := Ata.Protocol_PacketPIO;
	command.read := истина;
	command.bufAdr := adr;
	command.size := len;

	res := dev.controller.ExecuteCommand(command, Ata.ATAPITimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" read track information "); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон ReadTrackInformation;

проц CloseTrackSess*(dev: Ata.DeviceATAPI; immediate: булево; func, trackNr: цел32): целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(5BH);
	если immediate то
		SetBit(command.packet[1], 0);
	всё;
	SetField(command.packet[2], {0..2}, 0, func);

	command.packet[4] := симв8ИзКода(арифмСдвиг(trackNr, -8) остОтДеленияНа 100H);
	command.packet[5] := симв8ИзКода(trackNr остОтДеленияНа 100H);

	command.protocol := Ata.Protocol_PacketPIO;

	res := dev.controller.ExecuteCommand(command, Ata.ATAPITimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" close track / Session"); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон CloseTrackSess;

проц ReadBufferCapacity*(dev: Ata.DeviceATAPI; block: булево; adr: адресВПамяти; len: цел32): целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(5CH);
	если block то
		SetBit(command.packet[1], 0);
	всё;

	SetAllocationLength(command, 12);

	command.protocol := Ata.Protocol_PacketPIO;
	command.read := истина;
	command.bufAdr := adr;
	command.size := len;

	res := dev.controller.ExecuteCommand(command, Ata.ATAPITimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" read buffer capacity "); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон ReadBufferCapacity;

проц SetCDSpeed*(dev: Ata.DeviceATAPI; readSpeed, writeSpeed, rotControl: цел32): целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(0BBH);

	SetField(command.packet[1], {0,1}, 0, rotControl);

	command.packet[2] := симв8ИзКода(арифмСдвиг(readSpeed, -8) остОтДеленияНа 100H);
	command.packet[3] := симв8ИзКода(readSpeed остОтДеленияНа 100H);

	command.packet[4] := симв8ИзКода(арифмСдвиг(writeSpeed, -8) остОтДеленияНа 100H);
	command.packet[5] := симв8ИзКода(writeSpeed остОтДеленияНа 100H);

	command.protocol := Ata.Protocol_PacketPIO;

	res := dev.controller.ExecuteCommand(command, Ata.ATAPITimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" set cd speed "); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон SetCDSpeed;

проц SendCueSheet*(dev: Ata.DeviceATAPI; adr: адресВПамяти; len: цел32): целМЗ;
перем
	command: Ata.CommandPacket;
	res: целМЗ;
	status: мнвоНаБитахМЗ;
нач
	command := dev.NewCommandPacket(5DH);

	command.packet[6] := симв8ИзКода(арифмСдвиг(len, -16) остОтДеленияНа 100H);
	command.packet[7] := симв8ИзКода(арифмСдвиг(len, -8) остОтДеленияНа 100H);
	command.packet[8] := симв8ИзКода(len остОтДеленияНа 100H);

	command.protocol := Ata.Protocol_PacketPIO;
	command.read := ложь;
	command.bufAdr := adr;
	command.size :=len;
	res := dev.controller.ExecuteCommand(command, 4*Ata.ATAPITimeout, status);
	если Trace то
		ЛогЯдра.пСтроку8(dev.name); ЛогЯдра.пСтроку8(" send cue sheet"); ЛогЯдра.пВК_ПС;
	всё;
	возврат res;
кон SendCueSheet;

кон CDRecordLib.

CDRecordLib.test~

