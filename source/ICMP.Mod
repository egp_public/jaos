модуль ICMP; (** AUTHOR "mvt"; PURPOSE "ICMP protocol"; *)

(*
	ICMPv4 Header

	00	08	type
	01	08	code
	02	16	checksum of icmp header and data
	04	--	contents

	ICMPv4 Echo Request/Reply Packet

	00	08	type = 8 (request) or type = 0 (reply)
	01	08	code = 0
	02	16	checksum of icmp header and data
	04	16	identifier
	06	16	sequence number
	08	--	optional data

	Notes:
	o Bit numbers above are Intel bit order.
	o Avoid use of SET because of PPC bit numbering issues.
	o Always access fields as 8-, 16- or 32-bit values and use DIV, MOD, ASH, ODD for bit access.
*)

использует НИЗКОУР, ЭВМ, Modules, ЛогЯдра, IP, IPv6, Network;

конст
	(* DEBUG *)
	DEBUG = ложь;

	(** Error codes *)
	Ok* = 0;
	AlreadyInstalled* = 3501;
	NeverInstalled* = 3502;

	(* ICMP type exported *)
	ICMPDstUnreachable* = 1;

	(** ICMPv4 types *)
	TypeEchoReplyv4* = 0;
	TypeDstUnreachablev4 = 3;
	TypeSourceQuenchv4 = 4;
	TypeRedirectv4 = 5;
	TypeEchoRequestv4* = 8;
	TypeTimeExceededv4* = 11;

	(* ICMPv6 error types *)
	TypeDstUnreachablev6 = 1;
	TypePacketTooBigv6 = 2;
	TypeTimeExceededv6* = 3;
	TypeParamProbv6 = 4;

	(* ICMPv6 informal messages types *)
	TypeEchoRequestv6* = 128;
	TypeEchoReplyv6* = 129;

	(* Neighbor Discovery *)
	TypeNeighborSolicitation = 135;
	TypeNeighborAdvertisement = 136;
	TypeRouterSolicitation = 133;
	TypeRouterAdvertisement = 134;

	IPTypeICMPv4 = 1; (* ICMPv4 type code for IP packets *)
	IPTypeICMPv6 = 58; (* ICMPv6 type code for IP packets *)
	ICMPHdrLen = IP.ICMPHdrLen; (* length of ICMP header *)

	MaxPseudoHdrLen = 40;  (* IPv6: 40 *)
	PrefixOptHdrLen = 4;	(* 4 * 8-byte block *)
	MTUOptHdrLen = 1;		(* 1 * 8-byte block *)
	RtrAdvHdrLen = 12;		(* Router advertisement header length *)
	TimeExcHdrLen = 4;		(* ICMPv6 time exceeded header length *)
	ParamExcHdrLen = 4;	(* ICMPv6 parameter problem header length *)

тип
	Receiver* = проц {делегат} (int: IP.Interface; type, code: цел32; fip, lip: IP.Adr; buffer: Network.Buffer);

перем
	receivers: массив 256 из Receiver; (* registered receivers - array position is ICMP packet type *)

	(* Statistic variables *)
	NICMPRcvTotal-, NICMPTooSmall-, NICMPBadChecksum-, NICMPNoReceiver-, NICMPDelivered-,
	NICMPEchoRequest-, NICMPSend-: цел32;

	res: целМЗ;

(* Receive an ICMP (v4 and v6) packet *)
проц Input(int: IP.Interface; type: цел32; fip, lip: IP.Adr; buffer: Network.Buffer);
перем
	code: цел32;
	receiver: Receiver;
	checksumOk: булево;
	sum: цел32;
	pseudoHdrLen: цел32;
	pseudoHdr: массив MaxPseudoHdrLen из симв8; (* pseudo header for calculating checksum *)
	reassembledLength: цел32;
	fragmentBuffer: Network.Buffer;

нач
	если DEBUG то
		утв ((type = IPTypeICMPv6) или (type = IPTypeICMPv4));
	всё;

	ЭВМ.атомарноУвел(NICMPRcvTotal);
	если buffer.len >= ICMPHdrLen то
		(* Checksum calculation of ICMPv4 and ICMPv6 is different! In ICMPv6 another pseudo header is used *)
		если int.protocol = IP.IPv4 то
			checksumOk := IP.Checksum2(buffer.data, buffer.ofs, buffer.len, 0) = 0;
		аесли int.protocol = IP.IPv6 то
			(* Get checksum from header *)
			sum := Network.GetNet2(buffer.data, buffer.ofs + 2);

			если sum # 0 то
				(* calculate checksum *)
				(* set pseudo header *)
				reassembledLength := 0;
				fragmentBuffer := buffer;
				нцПока fragmentBuffer # НУЛЬ делай
					увел(reassembledLength, fragmentBuffer.len);
					fragmentBuffer := fragmentBuffer.nextFragment;
				кц;

				pseudoHdrLen := int.WritePseudoHeader(pseudoHdr, fip, lip, IPTypeICMPv6, reassembledLength);
				sum := IP.Checksum1(pseudoHdr, 0, pseudoHdrLen, 0);

				если buffer.nextFragment # НУЛЬ то
					(* fragmented packets *)
					fragmentBuffer := buffer;
					нцПока fragmentBuffer.nextFragment # НУЛЬ делай
						sum := IP.Checksum1(fragmentBuffer.data, fragmentBuffer.ofs, fragmentBuffer.len, sum);
						fragmentBuffer := fragmentBuffer.nextFragment;
					кц;

					sum := IP.Checksum2(fragmentBuffer.data, fragmentBuffer.ofs, fragmentBuffer.len, sum);
				иначе
					sum := IP.Checksum2(buffer.data, buffer.ofs, buffer.len, sum);
				всё;
			всё;
			checksumOk := sum = 0;
		иначе
			если DEBUG то
				утв(истина);
			всё;
			(* interface with unknown protocol *)
			checksumOk := ложь;
		всё;

		если checksumOk то
			type := кодСимв8(buffer.data[buffer.ofs]);
			code := кодСимв8(buffer.data[buffer.ofs+1]);
			receiver := receivers[type];

			если receiver # НУЛЬ то
				(* do receiver upcall *)
				buffer.l4ofs := buffer.ofs;
				увел(buffer.ofs, ICMPHdrLen);
				умень(buffer.len, ICMPHdrLen);
				receiver(int, type, code, fip, lip, buffer);
				ЭВМ.атомарноУвел(NICMPDelivered);
				(* Exit here w/o returning buffer because it is passed to a receiver *)
				возврат;
			иначе
				ЭВМ.атомарноУвел(NICMPNoReceiver);
			всё;
		иначе
			ЭВМ.атомарноУвел(NICMPBadChecksum);
		всё;
	иначе
		ЭВМ.атомарноУвел(NICMPTooSmall);
	всё;
	(* Exit and return buffer here because it is no longer used *)
	Network.ReturnBuffer(buffer);
кон Input;


(** Send an ICMP packet. The variables "type" and "code" must conatin the ICMP type and code information.
	interface can be set to send the ICMP message on a specific interface otherwise it is automatically slected. *)
проц Send*(interface: IP.Interface; fip: IP.Adr; перем data: массив из симв8; ofs, len, type, code, TTL: цел32);
перем
	hdr: массив ICMPHdrLen из симв8;
	pseudoHdrLen: цел32;
	pseudoHdr: массив MaxPseudoHdrLen из симв8; (* pseudo header for calculating checksum *)
	sum: цел32;

нач
	(* IF no interface was given choose one *)
	если interface = НУЛЬ то
		interface := IP.InterfaceByDstIP(fip);
	всё;

	если interface # НУЛЬ то
		ЭВМ.атомарноУвел(NICMPSend);
		(* Set ICMP header *)
		hdr[0] := симв8ИзКода(type);
		hdr[1] := симв8ИзКода(code);
		если fip.usedProtocol = IP.IPv4 то
			Network.Put2(hdr, 2, IP.Checksum2(data, ofs, len, IP.Checksum1(hdr, 0, 2, 0)));
			interface.Send(IPTypeICMPv4, fip, hdr, data, ICMPHdrLen, ofs, len, TTL);
		аесли fip.usedProtocol = IP.IPv6 то
			(* Use pseudo header for checksum calculation *)
			(* set pseudo header *)
			pseudoHdrLen := interface.WritePseudoHeader(pseudoHdr, interface.localAdr, fip, IPTypeICMPv6, len+ICMPHdrLen);

			sum := IP.Checksum1(pseudoHdr, 0, pseudoHdrLen, 0);
			sum := IP.Checksum1(hdr, 0, ICMPHdrLen, sum);
			sum := IP.Checksum2(data, ofs, len, sum);
			Network.Put2(hdr, 2, sum); (* checksum := sum *)
			interface.Send(IPTypeICMPv6, fip, hdr, data, ICMPHdrLen, ofs, len, TTL);
		всё;
	всё;
кон Send;


(** Send an ICMP packet. The variables "type" and "code" must conatin the ICMP type and code information.
	interface must be set. The ICMPv6 packet is directly send without cache lookups etc. *)
проц SendDirectly*(interface: IPv6.Interface; linkDst: Network.LinkAdr; fip: IP.Adr; перем data: массив из симв8; ofs, len, type, code, TTL: цел32);
перем
	hdr: массив ICMPHdrLen из симв8;
	pseudoHdrLen: цел32;
	pseudoHdr: массив MaxPseudoHdrLen из симв8; (* pseudo header for calculating checksum *)
	sum: цел32;

нач
	если DEBUG то
		утв (interface # НУЛЬ);
		утв (fip.usedProtocol = IP.IPv6);
	всё;

	если interface # НУЛЬ то
		ЭВМ.атомарноУвел(NICMPSend);
		(* Set ICMP header *)
		hdr[0] := симв8ИзКода(type);
		hdr[1] := симв8ИзКода(code);
		(* Use pseudo header for checksum calculation *)
		(* set pseudo header *)
		pseudoHdrLen := interface.WritePseudoHeader(pseudoHdr, interface.localAdr, fip, IPTypeICMPv6, len+ICMPHdrLen);

		sum := IP.Checksum1(pseudoHdr, 0, pseudoHdrLen, 0);
		sum := IP.Checksum1(hdr, 0, ICMPHdrLen, sum);
		sum := IP.Checksum2(data, ofs, len, sum);
		Network.Put2(hdr, 2, sum); (* checksum := sum *)

		interface.SendDirectly(linkDst, IPTypeICMPv6, fip, hdr, data, ICMPHdrLen, ofs, len, TTL);
	всё;
кон SendDirectly;


(** Install a receiver for this type *)
проц InstallReceiver*(type: цел32; r: Receiver; перем res: целМЗ);
нач {единолично}
	утв(r # НУЛЬ);
	утв((type >=0) и (type <= 255));
	если receivers[type] # НУЛЬ то
		res := AlreadyInstalled;
	иначе
		receivers[type] := r;
		res := Ok;
	всё;
кон InstallReceiver;


(** Remove the currently installed receiver for this type *)
проц RemoveReceiver*(type: цел32; перем res: целМЗ);
нач {единолично}
	утв((type >=0) и (type <= 255));
	если receivers[type] = НУЛЬ то
		res := NeverInstalled;
	иначе
		res := Ok;
		receivers[type] := НУЛЬ;
	всё;
кон RemoveReceiver;


(** Standard receiver that replies echo requests *)
проц ReplyEcho*(int: IP.Interface; type, code: цел32; fip, lip: IP.Adr; buffer: Network.Buffer);
перем
	longData: укль на массив из симв8;
	fragmentBuffer: Network.Buffer;
	fragmentLen: цел32;
	i: цел32;

нач
	ЭВМ.атомарноУвел(NICMPEchoRequest);
	если ~int.IsBroadcast(lip) то
		если fip.usedProtocol = IP.IPv4 то
			Send(int, fip, buffer.data, buffer.ofs, buffer.len, TypeEchoReplyv4, 0, IP.MaxTTL);

		аесли fip.usedProtocol = IP.IPv6 то
			если buffer.nextFragment = НУЛЬ то
				Send(int, fip, buffer.data, buffer.ofs, buffer.len, TypeEchoReplyv6, 0, IP.MaxTTL);
			иначе
				(* packet is fragmented *)
				нов(longData, IPv6.MaxFragPacketSize);
				fragmentBuffer := buffer;
				fragmentLen := 0;
				нцПока fragmentBuffer # НУЛЬ делай
					нцДля i := 0 до fragmentBuffer.len - 1 делай
						longData^[fragmentLen + i] := fragmentBuffer.data[fragmentBuffer.ofs + i];
					кц;
					увел(fragmentLen, fragmentBuffer.len);

					fragmentBuffer := fragmentBuffer.nextFragment;
				кц;

				Send(int, fip, longData^, 0, fragmentLen, TypeEchoReplyv6, 0, IP.MaxTTL);
			всё;
		иначе
			если DEBUG то
				утв(истина);
			всё;
			(* Unknown protocol *)
		всё
	всё;
	Network.ReturnBuffer(buffer);
кон ReplyEcho;


(* Send a ICMP message *)
проц SendICMP* (type: цел32; fip: IP.Adr; buffer: Network.Buffer);
перем
	IPHdrLen: цел32; (* length of IP header to copy *)
	icmpMsg: массив 72 из симв8; (* unused (4) + MaxIPHdrLen (60) + UDPHdrLen (8) *)

нач {единолично}
	просей type из
		ICMPDstUnreachable:
			IPHdrLen := buffer.ofs - buffer.l3ofs;
			Network.Put4(icmpMsg, 0, 0); (* unused *)
			(* 8: first 8 bytes of the original datagram-s dataIP header UDP header *)
			Network.Copy(buffer.data, icmpMsg, buffer.l3ofs, 4, IPHdrLen + 8);
			Send(НУЛЬ, fip, icmpMsg, 0, 4+IPHdrLen+8, TypeDstUnreachablev4, 3, IP.MaxTTL);
		иначе
			если DEBUG то
				утв(истина);
			всё;
	всё;
кон SendICMP;


(* Send a Neighbor Advertisement message *)
проц SendNeighborAdvertisement*(interface: IPv6.Interface; linkDst: Network.LinkAdr; dstAdr: IP.Adr; solicited: булево);
перем
	nsData: массив IPv6.NeighborHdrLen + IPv6.LLAdrOptionLen из симв8;
	nsDataLen: цел32;
	i: цел32;
	flagsSet: мнвоНаБитахМЗ;

нач
	flagsSet := {};

	(* routerFlag *)
	если interface.isRouter то
		flagsSet := flagsSet + {31};
	всё;

	(* solicited flag *)
	если solicited то
		flagsSet := flagsSet + {30};
	всё;

	(* override flag is true *)
	flagsSet := flagsSet + {29};

	Network.PutNet4(nsData, 0, НИЗКОУР.подмениТипЗначения(цел32, flagsSet));	(* flags & reserved *)

	нцДля i := 0 до 3 делай		(* Target address 16 byte *)
		Network.Put4(nsData, 4+(i*4), Network.Get4(interface.localAdr.ipv6Adr, i * 4));
	кц;

	если ~IP.IsNilAdr(dstAdr) то
		(* Add a Source Link-Layer Address option *)
		nsDataLen := IPv6.NeighborHdrLen + IPv6.LLAdrOptionLen;
		nsData[20] := 2X;	(* Type = 2 *)
		nsData[21] := 1X;	(* Length = 1 : Ethernet MAC Address (6bytes) *)
		нцДля i := 0 до 5 делай	(* Link-Layer Address *)
			nsData[22+i] := interface.dev.local[i];
		кц;
	иначе
		nsDataLen := 20;
	всё;

	(* Send packet directly without Neighbor Cache lookup etc. *)
	если solicited то
		SendDirectly (interface, linkDst, dstAdr, nsData, 0, nsDataLen, TypeNeighborAdvertisement, 0, IP.MaxTTL);
	иначе
		SendDirectly (interface, IPv6.linkMulticastAllNodesAdr, dstAdr, nsData, 0, nsDataLen, TypeNeighborAdvertisement, 0, IP.MaxTTL);
	всё;
кон SendNeighborAdvertisement;


(* Send a Neighbor Solicitation message *)
проц SendNeighborSolicitation*(interface: IPv6.Interface; linkDst: Network.LinkAdr; dstAdr: IP.Adr; multicast: булево);
перем
	nsData: массив IPv6.NeighborHdrLen + IPv6.LLAdrOptionLen из симв8;
	nsDataLen: цел32;
	solicitedNodeDstAdr: IP.Adr;
	i: цел32;

нач
	Network.Put4(nsData, 0, 0);	(* Reserved *)

	нцДля i := 0 до 3 делай		(* Target address 16 byte *)
		Network.Put4(nsData, 4+(i*4), Network.Get4(dstAdr.ipv6Adr, i * 4));
	кц;

	если ~IP.IsNilAdr(dstAdr) то
		(* Add a Source Link-Layer Address option *)
		nsDataLen := IPv6.NeighborHdrLen + IPv6.LLAdrOptionLen;
		nsData[20] := 1X;	(* Type = 1 *)
		nsData[21] := 1X;	(* Length = 1 : Ethernet MAC Address (6bytes) *)
		нцДля i := 0 до 5 делай	(* Link-Layer Address *)
			nsData[22+i] := interface.dev.local[i];
		кц;
	иначе
		nsDataLen := 20;
	всё;

	(* Send packet directly without Neighbor Cache lookup etc. *)
	если multicast то
		solicitedNodeDstAdr := IPv6.linkLocalMulticastNodeAdr;
		нцДля i := 13 до 15 делай
			solicitedNodeDstAdr.ipv6Adr[i] := dstAdr.ipv6Adr[i];
		кц;
		solicitedNodeDstAdr.ipv6Adr[11] := 1X;
		solicitedNodeDstAdr.ipv6Adr[12] := 0FFX;
		SendDirectly (interface, linkDst, solicitedNodeDstAdr, nsData, 0, nsDataLen, TypeNeighborSolicitation, 0, IP.MaxTTL);
	иначе
		SendDirectly (interface, linkDst, dstAdr, nsData, 0, nsDataLen, TypeNeighborSolicitation, 0, IP.MaxTTL);
	всё;
кон SendNeighborSolicitation;


(* Send a Router Solicitation message *)
проц SendRouterSolicitation(interface: IPv6.Interface);
перем
	rsData: массив IPv6.RouterSolHdrLen + IPv6.LLAdrOptionLen из симв8;
	rsDataLen: цел32;
	i: цел32;

нач
	Network.Put4(rsData, 0, 0); (* Reserved *)

	(* Add a source link-layer option *)
	rsDataLen := IPv6.RouterSolHdrLen + IPv6.LLAdrOptionLen;
	rsData[4] := 1X;	(* Type = 1 *)
	rsData[5] := 1X; (* Length = 1: Ethernet MAC Address (6 bytes) *)
	нцДля i := 0 до 5 делай	(* Link-Layer Address *)
		rsData[6+i] := interface.dev.local[i];
	кц;

	SendDirectly (interface, IPv6.linkMulticastAllRoutersAdr, IPv6.linkLocalMulticastRouterAdr, rsData, 0, rsDataLen, TypeRouterSolicitation, 0, IP.MaxTTL);
кон SendRouterSolicitation;


(* Send a Router Advertisement message *)
проц SendRouterAdvertisement(interface: IPv6.Interface; dstAdr: IP.Adr; dstLinkAdr: Network.LinkAdr; routerConfig: IPv6.RouterConfig);
перем
	raData: укль на массив из симв8;
	raDataLen: цел32;
	nbrOfPrefixes: цел32;
	prefixConfigItem: IPv6.PrefixConfig;
	flags: мнвоНаБитахМЗ;
	offset: цел32;
	i: цел32;

нач
	(* Count number of prefix options *)
	nbrOfPrefixes := 0;
	prefixConfigItem := routerConfig.Prefixes;
	нцПока prefixConfigItem # НУЛЬ делай
		prefixConfigItem := prefixConfigItem.next;
		увел(nbrOfPrefixes);
	кц;

	увел(raDataLen, nbrOfPrefixes * 8 * PrefixOptHdrLen);	(* prefix options header len is written in number of 8-bytes block *)

	(* Source link-layer address option *)
	увел(raDataLen, IPv6.LLAdrOptionLen);

	(* MTU option *)
	если routerConfig.LinkMTU # 0 то
		увел(raDataLen, MTUOptHdrLen * 8);
	всё;

	увел(raDataLen, RtrAdvHdrLen);

	нов(raData, raDataLen);

	(* Fill packet *)
	raData^[0] := симв8ИзКода(routerConfig.CurrentHopLimit);	(* Current hop limit *)

	(* Managed address configuration, other stateful configuration and home agent flag
	    Home flag is always zero. *)
	flags := {};
	если routerConfig.ManagedAddressConfig то
		flags := flags + {7};
	всё;
	если routerConfig.OtherStatefulConfig то
		flags := flags + {6};
	всё;

	raData^[1] := НИЗКОУР.подмениТипЗначения(симв8, flags);

	Network.PutNet2(raData^, 2, routerConfig.Lifetime);
	Network.PutNet4(raData^, 4, routerConfig.ReachableTime);
	Network.PutNet4(raData^, 8, routerConfig.RetransTimer);

	offset := 12;

	(* Add a source link-layer option *)
	raData^[offset] := 1X;	(* Type = 1 *)
	raData^[offset + 1] := 1X; (* Length = 1: Ethernet MAC Address (6 bytes) *)
	нцДля i := 0 до 5 делай	(* Link-Layer Address *)
		raData^[offset + 2 + i] := interface.dev.local[i];
	кц;
	увел(offset, IPv6.LLAdrOptionLen);

	(* LinkMTU option *)
	если routerConfig.LinkMTU # 0 то
		raData^[offset] := 5X;
		raData^[offset+1] := 1X;
		Network.Put2(raData^, offset + 2, 0);
		Network.PutNet4(raData^, offset + 4, routerConfig.LinkMTU);

		увел(offset, 8);
	всё;

	(* Prefixes *)
	prefixConfigItem := routerConfig.Prefixes;
	нцПока prefixConfigItem # НУЛЬ делай
		raData^[offset] := 3X;
		raData^[offset + 1] := 4X;
		raData^[offset + 2] := симв8ИзКода(prefixConfigItem.Prefix.data);

		(* flags *)
		flags := {};
		если prefixConfigItem.OnLink то
			flags := flags + {7};
		всё;
		если prefixConfigItem.Autonomous то
			flags := flags + {6};
		всё;
		(* router address flag is always zero: Mobility support is disabled *)
		если prefixConfigItem.IsSitePrefix то
			flags := flags + {4};
		всё;
		raData^[offset + 3] := НИЗКОУР.подмениТипЗначения(симв8, flags);

		Network.PutNet4(raData^, offset + 4, prefixConfigItem.ValidLifetime);
		Network.PutNet4(raData^, offset + 8, prefixConfigItem.PreferredLifetime);
		Network.Put4(raData^, offset + 12, 0);

		если prefixConfigItem.IsSitePrefix то
			raData^[offset + 15] := симв8ИзКода(prefixConfigItem.Prefix.data);
		всё;

		нцДля i := 0 до 15 делай
			raData^[offset + 16 + i] := prefixConfigItem.Prefix.ipv6Adr[i];
		кц;

		увел(offset, 8 * PrefixOptHdrLen);
		prefixConfigItem := prefixConfigItem.next;
	кц;
	SendDirectly (interface, dstLinkAdr, dstAdr, raData^, 0, raDataLen, TypeRouterAdvertisement, 0, IP.MaxTTL);
кон SendRouterAdvertisement;


(** Send a ICMPv6 time exceeded message *)
проц SendICMPv6TimeExceeded(interface: IPv6.Interface; discardedPacket: Network.Buffer; srcAdr: IP.Adr; code: цел32);
перем
	(* Max size of a ICMPv6 time exceeded packet including portion of discarded packet is 1280 *)
	teData: массив TimeExcHdrLen + 1280 - IPv6.MaxIPHdrLen - ICMPHdrLen из симв8;
	teDataLen: цел32;
	i: цел32;

нач
	Network.Put4(teData, 0, 0); (* Unused *)

	(* add portion of discarded packet *)
	teDataLen := матМинимум(TimeExcHdrLen + 1280 - IPv6.MaxIPHdrLen - ICMPHdrLen, TimeExcHdrLen + discardedPacket.len);
	нцДля i := TimeExcHdrLen до teDataLen - 1 делай
		teData[i] := discardedPacket.data[i - TimeExcHdrLen];
	кц;

	Send(interface, srcAdr, teData, 0, teDataLen, TypeTimeExceededv6, code, interface.curHopLimit);
кон SendICMPv6TimeExceeded;


(** Send a ICMPv6 parameter problem message *)
проц SendICMPv6ParamProb(interface: IPv6.Interface; discardedPacket: Network.Buffer; srcAdr: IP.Adr; probPointer: цел32; code: цел32);
перем
	(* Max size of a ICMPv6 parameter problem packet including portion of discarded packet is 1280 *)
	ppData: массив ParamExcHdrLen + 1280 - IPv6.MaxIPHdrLen - ICMPHdrLen из симв8;
	ppDataLen: цел32;
	i: цел32;

нач
	Network.Put4(ppData, 0, probPointer);

	(* add portion of discarded packet *)
	ppDataLen := матМинимум(ParamExcHdrLen + 1280 - IPv6.MaxIPHdrLen - ICMPHdrLen, ParamExcHdrLen + discardedPacket.len);
	нцДля i := ParamExcHdrLen до ppDataLen - 1 делай
		ppData[i] := discardedPacket.data[i - ParamExcHdrLen];
	кц;

	Send(interface, srcAdr, ppData, 0, ppDataLen, TypeParamProbv6, code, interface.curHopLimit);
кон SendICMPv6ParamProb;

(* Receive a neighbor Soliciation message *)
проц ReceiveNeighborSolicitation (interface: IP.Interface; type, code: цел32; srcAdr, dstAdr: IP.Adr; buffer: Network.Buffer);
перем
	ipv6Interface: IPv6.Interface;

нач
	если interface суть IPv6.Interface то
		(* Only IPv6 *)
		ipv6Interface := interface (IPv6.Interface);
		ipv6Interface.ReceiveNeighborSolicitation(srcAdr, dstAdr, buffer);

	иначе
		если DEBUG то
			утв(истина);
		всё;
		Network.ReturnBuffer(buffer);
	всё;
кон ReceiveNeighborSolicitation;


(* Receive a neighbor Advertisement message *)
проц ReceiveNeighborAdvertisement (interface: IP.Interface; type, code: цел32; srcAdr, dstAdr: IP.Adr; buffer: Network.Buffer);
перем
	ipv6Interface: IPv6.Interface;

нач
	если interface суть IPv6.Interface то
		(* Only IPv6 *)
		ipv6Interface := interface (IPv6.Interface);
		ipv6Interface.ReceiveNeighborAdvertisement (srcAdr, dstAdr, buffer);

	иначе
		если DEBUG то
			утв(истина);
		всё;
		Network.ReturnBuffer(buffer);
	всё;
кон ReceiveNeighborAdvertisement;


(* Receive a router solicitation message *)
проц ReceiveRouterSolicitation(interface: IP.Interface; type, code: цел32; srcAdr, dstAdr: IP.Adr; buffer: Network.Buffer);
перем
	ipv6Interface: IPv6.Interface;

нач
	если interface суть IPv6.Interface то
		(* Only IPv6 *)
		ipv6Interface := interface (IPv6.Interface);
		ipv6Interface.ReceiveRouterSolicitation();
	иначе
		если DEBUG то
			утв(истина);
		всё;
	всё;

ЛогЯдра.ЗахватВЕдиноличноеПользование;ЛогЯдра.пВК_ПС; ЛогЯдра.пСтроку8("************************");ЛогЯдра.пВК_ПС;
ЛогЯдра.пСтроку8("Received a router advertisement");
ЛогЯдра.пСтроку8("");
ЛогЯдра.пВК_ПС; ЛогЯдра.пСтроку8("************************");ЛогЯдра.пВК_ПС;ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;

	Network.ReturnBuffer(buffer);
кон ReceiveRouterSolicitation;


(* Receive a router advertisement message *)
проц ReceiveRouterAdvertisement (interface: IP.Interface; type, code: цел32; srcAdr, dstAdr: IP.Adr; buffer: Network.Buffer);
перем
	ipv6Interface: IPv6.Interface;

нач
	если interface суть IPv6.Interface то
		(* Only IPv6 *)
		ipv6Interface := interface (IPv6.Interface);
		ipv6Interface.ReceiveRouterAdvertisement(srcAdr, buffer);
	иначе
		если DEBUG то
			утв(истина);
		всё;
		Network.ReturnBuffer(buffer);
	всё;
кон ReceiveRouterAdvertisement;


(* Receive a packet too big message *)
проц ReceivePacketTooBig (interface: IP.Interface; type, code: цел32; srcAdr, dstAdr: IP.Adr; buffer: Network.Buffer);
перем
	ipv6Interface: IPv6.Interface;

нач
	если interface суть IPv6.Interface то
		(* Only IPv6 *)
		ipv6Interface := interface(IPv6.Interface);
		ipv6Interface.ReceivePacketTooBig(srcAdr, buffer);
	иначе
		если DEBUG то
			утв(истина);
		всё;
		Network.ReturnBuffer(buffer);
	всё;
кон ReceivePacketTooBig;


(** Reads Source or Target Link-Layer address option. Buffer.ofs has to be set to the Type byte*)
проц LinkLayerAdrOption (перем buffer: Network.Buffer; перем linkAdr: Network.LinkAdr);
перем
	i: цел32;

нач
	если DEBUG то
		(* Type is Source or Target Link *)
		утв ((buffer.data[buffer.ofs] = 1X) или (buffer.data[buffer.ofs] = 2X));
	всё;

	нцДля i := 0 до 5 делай
		linkAdr[i] := buffer.data[buffer.ofs + i + 2];
	кц;
	linkAdr[6] := 0X;
	linkAdr[7] := 0X;

	умень(buffer.len, 8 * кодСимв8(buffer.data[buffer.ofs + 1]));
	увел(buffer.ofs, 8 * кодСимв8(buffer.data[buffer.ofs + 1]));
кон LinkLayerAdrOption;


(* Reads ICMP prefix information option *)
проц PrefixInfoOption(перем buffer: Network.Buffer;
							   перем onLink: булево;
							   перем autonomous: булево;
							   перем routerAddress: булево;
							   перем sitePrefix: булево;
							   перем validLifetime: цел32;
							   перем preferredLifetime: цел32;
							   перем sitePrefixLength: цел32;
							   перем prefix: IP.Adr);
перем
	flags: мнвоНаБитахМЗ;
	i: цел32;

нач
	prefix.data := кодСимв8(buffer.data[buffer.ofs + 2]);

	flags := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, buffer.data[buffer.ofs + 3]);

	onLink := 7 в flags;
	autonomous := 6 в flags;
	routerAddress := 5 в flags;
	sitePrefix := 4 в flags;

	validLifetime := Network.GetNet4(buffer.data, buffer.ofs + 4);
	preferredLifetime := Network.GetNet4(buffer.data, buffer.ofs + 8);
	sitePrefixLength := кодСимв8(buffer.data[buffer.ofs + 15]);

	prefix.usedProtocol := IP.IPv6;
	нцДля i := 0 до 15 делай
		prefix.ipv6Adr[i] := buffer.data[buffer.ofs + 16 + i]
	кц;

	умень(buffer.len, 8 * кодСимв8(buffer.data[buffer.ofs + 1]));
	увел(buffer.ofs, 8 * кодСимв8(buffer.data[buffer.ofs + 1]));	(* Length field times 8 bytes*)
кон PrefixInfoOption;


(* Reads ICMP redirect header option *)
проц RedirectHdrOption(перем buffer: Network.Buffer);
нач
	умень(buffer.len, 8 * кодСимв8(buffer.data[buffer.ofs + 1]));
	увел(buffer.ofs, 8 * кодСимв8(buffer.data[buffer.ofs + 1]));	(* Length field times 8 bytes*)
кон RedirectHdrOption;


(* Reads ICMP MTU option *)
проц MTUOption(перем buffer: Network.Buffer; перем MTU: цел32);
нач
	MTU := Network.GetNet4(buffer.data, buffer.ofs + 4);

	умень(buffer.len, 8 * кодСимв8(buffer.data[buffer.ofs + 1]));
	увел(buffer.ofs, 8 * кодСимв8(buffer.data[buffer.ofs + 1]));	(* Length field times 8 bytes*)
кон MTUOption;


(* Reads ICMP advertisement interval option *)
проц AdvIntervalOption(перем buffer: Network.Buffer);
нач
	умень(buffer.len, 8 * кодСимв8(buffer.data[buffer.ofs + 1]));
	увел(buffer.ofs, 8 * кодСимв8(buffer.data[buffer.ofs + 1]));	(* Length field times 8 bytes*)
кон AdvIntervalOption;


(* Reads ICMP home agent information option *)
проц HomeAgentInfoOption(перем buffer: Network.Buffer);
нач
	умень(buffer.len, 8 * кодСимв8(buffer.data[buffer.ofs + 1]));
	увел(buffer.ofs, 8 * кодСимв8(buffer.data[buffer.ofs + 1]));	(* Length field times 8 bytes*)
кон HomeAgentInfoOption;


(* Reads ICMP route information option *)
проц RouteInfoOption(перем buffer: Network.Buffer);
нач
	умень(buffer.len, 8 * кодСимв8(buffer.data[buffer.ofs + 1]));
	увел(buffer.ofs, 8 * кодСимв8(buffer.data[buffer.ofs + 1]));	(* Length field times 8 bytes*)
кон RouteInfoOption;


проц Cleanup;
перем
	res: целМЗ;

нач
	IPv6.sendNeighborSolicitation := НУЛЬ;
	IPv6.sendNeighborAdvertisement := НУЛЬ;
	IPv6.sendRouterSolicitation := НУЛЬ;
	IPv6.sendRouterAdvertisement := НУЛЬ;
	IPv6.icmpLinkLayerAdrOption := НУЛЬ;
	IPv6.icmpPrefixInfoOption := НУЛЬ;
	IPv6.icmpRedirectHdrOption := НУЛЬ;
	IPv6.icmpMTUOption := НУЛЬ;
	IPv6.icmpAdvIntervalOption := НУЛЬ;
	IPv6.icmpHomeAgentInfoOption := НУЛЬ;
	IPv6.icmpRouteInfoOption := НУЛЬ;

	(* Remove ICMP receivers *)
	RemoveReceiver(TypeEchoRequestv4, res);
	если DEBUG то утв (res = Ok) всё;
	RemoveReceiver(TypeEchoRequestv6, res);
	если DEBUG то утв (res = Ok) всё;
	RemoveReceiver(TypeNeighborSolicitation, res);
	если DEBUG то утв (res = Ok) всё;
	RemoveReceiver(TypeNeighborAdvertisement, res);
	если DEBUG то утв (res = Ok) всё;
	RemoveReceiver(TypeRouterAdvertisement, res);
	если DEBUG то утв (res = Ok) всё;
	RemoveReceiver(TypeRouterSolicitation, res);
	если DEBUG то утв (res = Ok) всё;
	RemoveReceiver(TypePacketTooBigv6, res);
	если DEBUG то утв (res = Ok) всё;

	(* Remove IP receivers *)
	IP.RemoveReceiver(IPTypeICMPv4);
	IP.RemoveReceiver(IPTypeICMPv6);
кон Cleanup;


проц InitDelegates*;
нач
	(* set delegates in IPv6 *)
	IPv6.sendNeighborSolicitation := SendNeighborSolicitation;
	IPv6.sendNeighborAdvertisement := SendNeighborAdvertisement;
	IPv6.sendRouterAdvertisement := SendRouterAdvertisement;
	IPv6.sendRouterSolicitation := SendRouterSolicitation;
	IPv6.sendICMPv6TimeExceeded := SendICMPv6TimeExceeded;
	IPv6.sendICMPv6ParamProb := SendICMPv6ParamProb;
	IPv6.icmpLinkLayerAdrOption := LinkLayerAdrOption;
	IPv6.icmpPrefixInfoOption := PrefixInfoOption;
	IPv6.icmpRedirectHdrOption := RedirectHdrOption;
	IPv6.icmpMTUOption := MTUOption;
	IPv6.icmpAdvIntervalOption := AdvIntervalOption;
	IPv6.icmpHomeAgentInfoOption := HomeAgentInfoOption;
	IPv6.icmpRouteInfoOption := RouteInfoOption;
кон InitDelegates;


нач
	если (IP.EchoReply) то
		(* install internal echoRequest receiver *)
		InstallReceiver(TypeEchoRequestv4, ReplyEcho, res);
		если DEBUG то утв (res = Ok) всё;
		InstallReceiver(TypeEchoRequestv6, ReplyEcho, res);
		если DEBUG то  утв (res = Ok) всё;

		(* Install neighbor discovery reiceivers *)
		InstallReceiver(TypeNeighborSolicitation, ReceiveNeighborSolicitation, res);
		если DEBUG то утв (res = Ok) всё;
		InstallReceiver(TypeNeighborAdvertisement, ReceiveNeighborAdvertisement, res);
		если DEBUG то утв (res = Ok) всё;

		(* Router Advertisement *)
		InstallReceiver(TypeRouterAdvertisement, ReceiveRouterAdvertisement, res);
		если DEBUG то утв (res = Ok) всё;

		(* Router Solicitation *)
		InstallReceiver(TypeRouterSolicitation, ReceiveRouterSolicitation, res);
		если DEBUG то утв (res = Ok) всё;

		(* Packet too big *)
		InstallReceiver(TypePacketTooBigv6, ReceivePacketTooBig, res);
		если DEBUG то утв (res = Ok) всё;
	всё;
	IP.InstallReceiver(IPTypeICMPv4, Input);
	IP.InstallReceiver(IPTypeICMPv6, Input);
	Modules.InstallTermHandler(Cleanup);
кон ICMP.

(*
History:
21.10.2003	mvt	Created and moved the ICMP impelementation from the IP module to this one.
26.10.2003	mvt	Adapted to new design of IP.
02.05.2005	eb	IPv6 (Neighbor Discovery / EchoRequest / EchoReply
*)
