модуль UDP;   (* AUTHOR "fof"; PURPOSE "UDP protocol for Winsock"; *)

использует НИЗКОУР, IP, (* AosWinsock, *) WSock32, ЛогЯдра;

конст
	(* error codes. *)
	Ok* = 0;  AddressInUse* = 3501;  Timeout* = 3502;  BufferOverflow* = 3503;  AlreadyBlocked* = 3504;
	PortInUse* = 3501;

	unknown = 1;  IPAdrLen = 4;
	NilPort* = 0;  (* Dan 31.01.05 *)

	SendBufSize = 65536*8; (* send buffer size in bytes *)
	RecvBufSize = 65536*16; (* receive buffer size in bytes *)

тип

тип
	Socket* = окласс  (* stores the state of a UDP communication endpoint. *)
	перем sock: WSock32.Socket;

		(* Initialize the Socket (only use once per instance).
	Opens a socket which is dedicated to datagram services. lport is registered to receive datagrams
	from any port and any host. *)
		проц & Open*( lport: цел32;  перем res: целМЗ );
		перем
			sadr: WSock32.sockaddrIn;
			err: цел32; winRes: цел32;
			bufSize, len: цел32;
		нач
			(* IF ~AosWinsock.ready THEN RETURN END;  *)
			sock := WSock32.socket( WSock32.PFINet, WSock32.SockDGram, WSock32.IPProtoUDP );
			если sock # WSock32.InvalidSocket то
				sadr.sinFamily := WSock32.PFINet;  sadr.sinAddr := 0;
				если lport # NilPort то sadr.sinPort := WSock32.htons( устарПреобразуйКБолееУзкомуЦел( lport ) ) иначе sadr.sinPort := 0 всё;
				winRes := WSock32.bind( sock, sadr, размер16_от( WSock32.sockaddrIn ) );
				если winRes # 0 то
					err := WSock32.WSAGetLastError(); (*  AosWinsock.DispError( err ); *)  SockFinalizer( сам );  res := unknown;
				иначе (*  Kernel.RegisterObject( SELF, SockFinalizer, FALSE ); *)  res := Ok
				всё
			иначе res := unknown;
			всё;

			bufSize := SendBufSize; len := размер16_от(цел32);
			err := WSock32.setsockopt(sock,WSock32.SOLSocket,WSock32.SOSndBuf,bufSize,len);
			если (err # 0) и trace то
				ЛогЯдра.пСтроку8( "UDP.Open : failed to set send buffer size, WSock32 error code " ); ЛогЯдра.пЦел64(err,0); ЛогЯдра.пВК_ПС;
			всё;
			bufSize := RecvBufSize;
			err := WSock32.setsockopt(sock,WSock32.SOLSocket,WSock32.SORcvBuf,bufSize,размер16_от(цел32));
			если (err # 0) и trace то
				ЛогЯдра.пСтроку8( "UDP.Open : failed to set receive buffer size, WSock32 error code " ); ЛогЯдра.пЦел64(err,0); ЛогЯдра.пВК_ПС;
			всё;

			если trace то
				ЛогЯдра.пСтроку8( "UDP.Open : " );  ЛогЯдра.пЦел64( lport, 1 );  ЛогЯдра.пСтроку8( "(" );  ЛогЯдра.пЦел64( res, 1 );
				ЛогЯдра.пСтроку8( ")" );  ЛогЯдра.пВК_ПС;
			всё;
		кон Open;

	(* Send a UDP datagram to the foreign address specified by "fip" and "lport".  The data is in "data[ofs..ofs+len-1]".
					 In case of concurrent sends the datagrams are serialized.
	Sends len bytes of data (beginning at pos in buf) to the host specified by remIP and remPort. *)

		проц Send*( fip: IP.Adr;  fport: цел32;  конст data: массив из симв8;  ofs, len: размерМЗ; перем res: целМЗ );
		перем sadr: WSock32.sockaddrIn;  err: цел32; winRes: цел32;
		нач
			утв ( длинаМассива( data ) >= (ofs + len) );
			если (fip.usedProtocol = IP.IPv4) то
				НИЗКОУР.копируйПамять( адресОт( fip ), адресОт( sadr.sinAddr ), IPAdrLen );
				sadr.sinFamily := WSock32.PFINet;  sadr.sinPort := WSock32.htons( устарПреобразуйКБолееУзкомуЦел( fport ) );
				winRes := WSock32.sendto( sock, data[ofs], len(целМЗ), {}, sadr, размер16_от( WSock32.sockaddrIn ) );
				(* account that sendto returns number of bytes sent to the socket (Alexey) *)
				если winRes = len то res := Ok; иначе err := WSock32.WSAGetLastError(); res := unknown;  всё;
			иначе res := unknown;
			всё;
			если trace то
				если (fip.usedProtocol = IP.IPv4) то
					ЛогЯдра.пСтроку8( "UDP.Send : " );  ЛогЯдра.пЦел64( fip.ipv4Adr, 1 );  ЛогЯдра.пСтроку8( " , " );  ЛогЯдра.пЦел64( fport, 1 );
					ЛогЯдра.пСтроку8( "(" );  ЛогЯдра.пЦел64( res, 1 );  ЛогЯдра.пСтроку8( ")" );  ЛогЯдра.пВК_ПС;
				иначе
					ЛогЯдра.пСтроку8("UDP.Send : Error, only works with IPv4 addresses!"); ЛогЯдра.пВК_ПС;
				всё;
			всё;
		кон Send;

	(* Receive a UDP datagram.  If none is available, wait up to the specified timeout for one to arrive.  Only one thread
					is allowed to wait for a datagram.  "data[ofs..ofs+size-1]" is the data buffer to hold the returned datagram.
					 "ms" is a timeout value in milliseconds, or 0 for an indefinite wait.  On return, "fip" and "lport" hold
					the foreign address.  "len" returns the actual datagram size and "data[ofs..ofs+len-1]" returns the data.
					 "res" returns "Timeout" in case of a timeout and "BufferOverflow" if the received datagram was too big,
					in which case "len" is the actual datagram size negated and the data is undefined.  "res" returns "AlreadyBlocked"
					if another thread is already blocked on this Socket.
	Stores an entire datagram in buf beginning at pos. On success (S.res = done), remIP and remPort indicate the sender, len indicate the length of valid data. *)

		проц Receive*( перем data: массив из симв8;  ofs, size: размерМЗ; ms: цел32;  перем fip: IP.Adr;
										   перем fport: цел32; перем len: размерМЗ; перем res: целМЗ );
		перем sadr: WSock32.sockaddrIn;  err: цел32;  l: цел32; winRes: цел32;
		ret: цел32;  fdset: WSock32.FDSet; avail: булево; time: WSock32.TimeVal;
		нач
			утв ( ofs+size <= длинаМассива( data ) );
			l := размер16_от( WSock32.sockaddrIn );

			если ms=-1 то (* do, as if data was available to invoke blocking call of recvfrom *)
				avail := истина;
			иначе (* handle timeout *)
				ret := WSock32.ioctlsocket( sock, WSock32.FIONRead, winRes );
				если ret # 0 то (* error *)
					err := WSock32.WSAGetLastError(); res := unknown; avail := ложь;
				иначе  (* no error *)
					avail := winRes > 0;
					если ~avail то  (* nothing available yet *)
						fdset.fdcount := 1;  fdset.socket[0] := sock;
						если ms = 0 то
							time := НУЛЬ
						иначе
							нов(time);
							time.sec := ms DIV 1000; time.musec := 1000* (ms остОтДеленияНа 1000);
						всё;
						ret := WSock32.select( 0, fdset, НУЛЬ , НУЛЬ , time );
						avail := ret = 1;
						если  ~avail то (* still nothing available *)
							len := 0; res := Timeout
						всё;
					всё;
				всё;
			всё;

			если avail то
				len := WSock32.recvfrom( sock, data[ofs], size(целМЗ), {}, sadr, l );
				если len < 0 то err := WSock32.WSAGetLastError(); (*  AosWinsock.DispError( err ); *)  res := unknown;
				иначе res := Ok;
				всё;
			всё;

			fport := WSock32.ntohs( sadr.sinPort );
			НИЗКОУР.копируйПамять( адресОт( sadr.sinAddr ), адресОт( fip ), IPAdrLen );

			(*? Problem - Windows XP does not fill in the Fip.UseProtocol field for IPv4 packets ! done manually here*)
			если fip.ipv4Adr # 0 то fip.usedProtocol := IP.IPv4 всё;

			если trace то
				если (fip.usedProtocol = IP.IPv4) то
					ЛогЯдра.пСтроку8( "UDP.Receive : " );  ЛогЯдра.пЦел64( fip.ipv4Adr, 1 );  ЛогЯдра.пСтроку8( " , " );  ЛогЯдра.пЦел64( fport, 1 );
					ЛогЯдра.пСтроку8( "(" );  ЛогЯдра.пЦел64( res, 1 );  ЛогЯдра.пСтроку8( ")" );  ЛогЯдра.пВК_ПС;
				иначе
					ЛогЯдра.пСтроку8("UDP.Receive : Warning, received UDP packet from non-IPv4 source!"); ЛогЯдра.пВК_ПС;
				всё;
			всё;

		кон Receive;

	(* Close the Socket, freeing its address for re-use. *)
		проц Close*;
		нач
			SockFinalizer( сам );
			если trace то ЛогЯдра.пСтроку8( "UDP.Close" );  ЛогЯдра.пВК_ПС;  всё;
		кон Close;
	(*
		(** Returns the size of the first available datagram on the socket, otherwise <= 0. *)
	PROCEDURE Available*(  ): SIGNED32;
	VAR avail: SIGNED32;
	BEGIN
		WSock32.ioctlsocket( sock, WSock32.FIONRead, avail );  RETURN avail
	END Available;
*)

	кон Socket;

перем
	trace: булево;

	проц SockFinalizer( S: динамическиТипизированныйУкль );
	перем ret: цел32;																			(* Dan 10.11.05 *)	нач
		просейТип S: Socket делай
			если S.sock # WSock32.InvalidSocket то  ret:= WSock32.closesocket( S.sock );  S.sock := WSock32.InvalidSocket всё
		всё
	кон SockFinalizer;

	проц ToggleTrace*;
	нач
		trace := ~trace;
		если trace то ЛогЯдра.пСтроку8( "UDP: tracing ON" );  ЛогЯдра.пВК_ПС;
		иначе ЛогЯдра.пСтроку8( "UDP: tracing OFF" );  ЛогЯдра.пВК_ПС
		всё;
	кон ToggleTrace;

кон UDP.
